<?php 

// echo 'Begin Assessment Results page.<br />';

include_once('../includes/header.php');

getPage('Assessment Results', $connection);

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

include('../php/assessment_results.php');

include('../php/system_log_page_access.php'); 

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php'); 

?>

<script type="text/javascript" src="../js/assessment.js?_version=<?php echo VERSION?>"></script>
<script type="text/javascript" src="../js/ajax_discussion_forum.min.js?_version=<?php echo VERSION?>"></script>