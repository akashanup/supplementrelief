<?php 

// echo 'Begin Blog page.<br />';
// Calling Page: Menu
// Calling Script: show_project_program_blogs.php
// https://supplementrelief.com/show_blog_posts/?ppca_id=274

include_once('../includes/header.php');

getPage('Blog', $connection);

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

include('../php/show_blogs.php');

include('../php/system_log_page_access.php');  

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php'); 

?>

