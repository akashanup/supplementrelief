<?php 

// echo 'Begin Contact Confirmation page.<br />';

include_once('../includes/header.php');

getPage('Contact Confirmation', $connection);

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

if (strlen($_SESSION['message']) > 0) { 
	?>
	<br />
	<div class="row">		
		<div class="small-12 columns">
				<?php 
					echo $_SESSION['message'];
					$_SESSION['message'] = null;	
				?>		
		</div>
	</div>
	<br />
	<?php
}
	
?>

<!-- Google Code for Whiteshark Media Contact Form Conversion Page -->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 879537895;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "yh28CNP22WcQ592yowM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/879537895/?label=yh28CNP22WcQ592yowM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<?php

// include('../php/contact_confirmation.php');

include('../php/system_log_page_access.php'); 

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php'); 

?>