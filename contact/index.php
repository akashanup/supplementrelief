<?php 

// echo 'Begin Contact page.<br />';
// die;

include_once('../includes/header.php');

getPage('Contact', $connection);

if ($_SESSION['page']['use_authorization']) { include_once('../includes/authorization.php'); }

include_once('../includes/page_header.php');

if ($_SESSION['page']['use_menu']) { include_once('../includes/menu.php'); }

if ($_SESSION['page']['use_title_bar']) { include_once('../includes/off_page_canvas_1.php'); }

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

include_once('../php/contact.php');

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

if ($_SESSION['page']['use_title_bar']) { include_once('../includes/off_page_canvas_2.php'); }

// Begin rest of body content for page.

if ($_SESSION['page']['use_footer']) { include_once('../includes/page_footer.php'); }

if ($_SESSION['page']['use_disclaimer']) { include_once('../includes/disclaimer.php'); }

include_once('../includes/foundation_footer.php'); 

if ($_SESSION['page']['use_system_log']) { include_once('../php/system_log_page_access.php'); }

?>

<script type="text/javascript" src="../js/contact.js"></script>
