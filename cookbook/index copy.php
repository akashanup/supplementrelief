<?php 
 
// echo 'Begin Cookbook page.<br />';
 
include_once('../includes/header.php');
  
getPage('Cookbook', $connection);
 
include_once('../includes/authorization.php');
 
include('../includes/page_header.php');
 
include('../includes/menu.php');
 
include('../includes/off_page_canvas_1.php');
 
// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 
 
include('../php/show_project_program_resource.php'); 
 
// include('../php/show_project_program_resource.php'); 
 
include('../php/system_log_page_access.php'); 
 
// End body content that will appear inside of the Off Canvas <section class="main-section"> -->
 
include('../includes/off_page_canvas_2.php');
 
// Begin rest of body content for page.
 
include('../includes/page_footer.php');
 
include('../includes/disclaimer.php');
 
include('../includes/foundation_footer.php'); 
 
?>
 
<script>
$(document).ready(function() {
	
	// These events will fire when the user changes a value in these fields.
	
    $('#keyword').change(function(){
        $('#recipe_search_form').submit();
    });
  
    $('#order_by_recipe').change(function(){
        $('#recipe_search_form').submit();
    });
     
    $('#number_servings').change(function(){
        $('#recipe_search_form').submit();
    }); 
     
    $('#time_to_prepare').change(function(){
        $('#recipe_search_form').submit();
    }); 
     
    $('#limit_by_chef').change(function(){
        $('#recipe_search_form').submit();
    });
     
    $('#recipe_search_form').find('input[type=checkbox]').on('click',function(){
        $('#recipe_search_form').submit();
    });
         
});
</script>
 
<!-- <script src="../js/f5tabfix.js"></script> -->