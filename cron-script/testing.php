<?php
	//Following are	the commands used for compression.
    //ffmpeg -i akash.wmv -s 480x320 -c:v libx264 akash_wmv.mp4

    //ffmpeg -i aaaa.webm -s 480x320 -c:a copy akash_output.mp4

    //Check	whether	this script is already running or not. If yes then exit	this script.

	define('DB_SERVER', 'localhost');
    define("DB_USER", "root");
    define("DB_PASS", "akashanup"); 
    define("DB_NAME", "rtsadven_development");
    	
    //Making connection to DB
	$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
	
	$sapi_type = php_sapi_name();
	if (substr($sapi_type, 0, 3) == 'cli')
	//if(true) 
	{
		
		$log_file_path 	=	'/home/my-errors.log';

		if(mysqli_connect_errno()) {
		    die(error_log((date('m/d/y h:i:s:m')." : Database connection failed: " . mysqli_connect_error() . " (" . mysqli_connect_errno() . ")\n"), 3, $log_file_path));
		}
		compressVideo('discussion_threads', $log_file_path);
		compressVideo('discussion_replies', $log_file_path);
	}
	else
	{
		error_log((date('m/d/y h:i:s:m')." : Not Allowed!\n"), 3, $log_file_path);
	}

	function compressVideo($table, $log_file_path)
	{
		global $connection;
		//fetching files with video_status = 0. This indicates uncompressed files.
		$query 	= 	'SELECT file_video_url FROM ' . $table . ' WHERE video_status = '. 0 . ' LIMIT 10';
		$result = 	mysqli_query($connection, $query);
		$totalUncompressedVideos = mysqli_num_rows($result); 

		if ($totalUncompressedVideos)  
		{
		    $dir 					=	'/var/www/html/media/videos/user/';
		    $ffmpeg 				=	'ffmpeg -i ';
		    $resize					=	'320x240';//480x320
		    $resizeCmd				=	' -s ' . $resize;
		    $copyCmd 				=	' -c:v libx264 ';

		    $deleteCmd 				=	'rm ';
		    
		    $changePermissionCmd 	= 	'chmod 777 ';

		    $givePrmisnUsrGrpCmd 	=	'chown -R akash:akash ';

			try
			{
			    while($row 	= 	mysqli_fetch_assoc($result)) 
			    {
			    	$video_path 		=	$row['file_video_url'];
			    	$file_video_url 	=	substr($video_path, 0, (strrpos($video_path, '/')+1));

			    	$video_name 		=	substr($video_path, (strrpos($video_path, '/')+1));
			    	$current_video_path	=	$dir . $video_name;
			        
			        //changing the file permission so that it can be deleted after compression
			        $systemChangePermissionCmd 	= 	$changePermissionCmd . $dir . $video_name;
			        system($systemChangePermissionCmd);

			 		//changing the extension of output file
			        $video_name_length  = 	strrpos($video_name,'.');
			    	$video_name 		=	substr($video_name, 0, $video_name_length) . '.mp4';
			        $cmp_video_name 	= 	'cmprs_' . $video_name;
			        $cmp_video_path 	= 	$dir . $cmp_video_name;

			        $file_video_url 	= 	$file_video_url . $cmp_video_name;

			        //updating the video_status to 1 which indicates that compression will start for this file
			        $query = 'UPDATE ' . $table . ' SET video_status = '. 1 .'  WHERE file_video_url = "' . $video_path .'"';
				
					if (mysqli_query($connection, $query) != TRUE) 
					{
					    die(error_log( (date('m/d/y h:i:s:m')." : Error:- $query.\n"), 3, $log_file_path));
					}
					else
					{
						error_log((date('m/d/y h:i:s:m'). " : Compressing - $file_video_url.\n"), 3, $log_file_path);
					}

			        $systemCall			=	$ffmpeg . $current_video_path . $resizeCmd . $copyCmd . $cmp_video_path; 
			        //error_log((date('m/d/y h:i:s:m')." : $systemCall.\n"), 3, $log_file_path);
			        system($systemCall);
			        
			        //giving permission to akash user
			        $systemGivePermissionToUsrGrp 	=	$givePrmisnUsrGrpCmd . $cmp_video_path;
			        system($systemGivePermissionToUsrGrp);
			        
			        //updating the video_status to 2 which indicates that compression is finished for the file
			        $query = 'UPDATE ' . $table . ' SET video_status = '. 2 .' , file_video_url = "'. $file_video_url . '"  WHERE file_video_url = "' . $video_path .'"';
				
					if (mysqli_query($connection, $query) != TRUE) 
					{
					    die(error_log((date('m/d/y h:i:s:m')." : Error:- $query.\n"), 3, $log_file_path));
					}
					else
					{
						error_log((date('m/d/y h:i:s:m'). " : Compressed - $file_video_url.\n"), 3, $log_file_path);
					}

					//removing the uncompressed video
			        $deleteSystemCall 	=	$deleteCmd . $current_video_path;
			        system($deleteSystemCall);
			    }
			}
			catch(Exception $e)
			{
				error_log((date('m/d/y h:i:s:m')." : $e.\n"), 3, $log_file_path);
			}
		}
		else
		{
			error_log((date('m/d/y h:i:s:m')." : Nothing to compress in $table.\n"), 3, "/home/my-errors.log");
		}
	}
?>