<?php 

// echo 'Begin Customer Order Lookup page.<br />';

include_once('../includes/header.php');

getPage('Customer Order Lookup', $connection);

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

show_session_message();

include('../php/customer_order_lookup.php'); 

include('../php/system_log_page_access.php'); 

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php'); 

?>

