delimiter $$
drop function if exists getPrice$$
create function getPrice(p_usageid int,
							p_qty int, 
							p_bpuo_id int, 
                            p_project int,
                            p_price_type varchar(30))
returns decimal(11,2)
begin
   declare p_price decimal(11,2);
   declare p_prev_id int;
  
  
  searching : LOOP
     set p_price = (select getPrice2(p_usageid,p_qty,p_bpuo_id,p_project,p_price_type) as p);
     
     if p_price is null then
        set p_prev_id = p_bpuo_id;
        -- getting the new parent
		set p_bpuo_id = (select opt.brand_product_usage_option_parent_id 
							FROM brand_product_usage_options opt
							WHERE opt.id = p_prev_id);
						
        if p_bpuo_id is null then 
			leave searching;
        end if;
     else -- price found
		leave searching;
     end if;
  
  END LOOP searching;
 
  return p_price;

end
$$delimiter ;
