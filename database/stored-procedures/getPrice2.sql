delimiter $$
drop function if exists getPrice2$$
create function getPrice2(p_usageid int,
							p_qty int, 
							p_bpuo_id int, 
                            p_project int,
                            p_price_type varchar(30))
returns decimal(11,2)
begin
   declare p_price decimal(11,2);
  
  set p_price = (
   SELECT price -- price_type_code, price,quantity_low_range ,quantity_high_range 
		FROM  project_brand_product_usage_prices
		WHERE 
        bpuo_id = p_bpuo_id
        AND  brand_product_usage_id = p_usageid
		AND pbpu_project_id = p_project
		AND price_type_code = p_price_type
        
		AND
		((quantity_low_range <= p_qty AND (quantity_high_range >= p_qty OR quantity_high_range IS NULL))) 
        
		limit 1
    ) ;

   -- getting the default price
   if p_price is null then
	  set p_price = (
	   SELECT price 
			FROM  project_brand_product_usage_prices
			WHERE 
			bpuo_id = p_bpuo_id
			AND  brand_product_usage_id = p_usageid
			AND pbpu_project_id = p_project
			AND price_type_code = p_price_type
			
			AND
			  (quantity_low_range IS NULL AND quantity_high_range IS NULL) 
			 
		limit 1
		) ;
	
   end if;
   
  
   return p_price;
end$$
delimiter ;
