<?php 

// echo 'Begin Education Development page.<br />';
// die;

// You can get here by:
// 1. Choosing a Topic from the Course Outline
// 2. Choosing Education from the Main Menu

include_once('../includes/header.php');

// Gets the Session Page ID in context of the Application so that SOZO, SupplementRelief and other deployments to different URLs can share the same code base.
getPage('Wellness Education', $connection);

if ($_SESSION['page']['use_authorization']) { include_once('../includes/authorization.php'); }

include('../includes/page_header.php');

if ($_SESSION['page']['use_menu']) { include_once('../includes/menu.php'); }

// include('../php/education_prefix.php');

// User cannot access this page if we do not know the Course ID. If no Course ID they got here somehow probably from some older code. Redirect them to the Select Program Enrollment page.		
if (!isset($_SESSION['enrollment']['course_id'])) {
	// echo 'Session Course ID is not set. Redirect to Select Program Enrollment.';
	$_SESSION['message_type'] = 'alert-box alert radius';				
	$_SESSION['message'] = '<p>Please select a <strong>Program Enrollment</strong> from the listing before proceeding.</p>';		
	header("location: ../select-program-enrollment/");	
	exit();	
}

// Course
if (isset($_GET['c_id'])) {
	$course_id = $_GET['c_id'];
	$_SESSION['enrollment']['course_id'] = $_GET['c_id'];
	
 } else {
 	$course_id = $_SESSION['enrollment']['course_id']; // Previously viewed a Course Topic but went somewhere else on the site i.e. Blog and then selected the Education menu.	
 	
} 

// Topic
if (isset($_GET['t_id'])) {
	$topic_id = $_GET['t_id'];
	$_SESSION['enrollment']['topic_id'] = $_GET['t_id'];
		
}  else {

	if (strlen($_SESSION['enrollment']['topic_id']) > 0) {
		// Means user is returning to Education page from somewhere other than using the Course Outline navigation which explicitly sets the Topic ID in the URL parameter. Also implies that the Topic ID was set earlier in the SESSION and stored as a SESSION variable for this very purpose so that the user is returned to the correct Topic whenever they leave the Education page and then return later.
		$topic_id = $_SESSION['enrollment']['topic_id'];
	} else {
		// User has navigated to this page without using the Course Outline at some previous point to set the $_SESSION['enrollment']['topic_id'] so send them to the User Dashboard to Select a Topic from the Course Outline before going to the Education page. Don't think this path is possible but is here for insurance.
		// User message
		$_SESSION['message_type'] = 'alert-box warning radius';				
		$_SESSION['message'] = '<p>Please select a <strong>Topic</strong> from the <strong>Course Outline</strong> before accessing the Education page.</p>';		
		header("location: ../member-dashboard/");
		exit();
	}

}

// Web Page
if (isset($_GET['wp_id'])) {
	$web_page_id = $_GET['wp_id'];
	$_SESSION['enrollment']['web_page_id'] = $_GET['wp_id'];
		
}  else {
	 // Means user is returning to Education page from somewhere other than using the Course Outline navigation which explicitly sets the Web Page ID in the URL parameter. Also implies that the Web Page ID was set earlier in the SESSION and stored as a SESSION variable for this very purpose so that the user is returned to the correct Web Page whenever they leave the Education page and then return later.
	 $web_page_id = $_SESSION['enrollment']['web_page_id'];
}

// Get the Education Course Title for the chosen Course ID.
$queryCourseTitle  = 'SELECT title FROM content_assets WHERE id = '.$course_id.'  LIMIT 1';

// echo $queryCourseTitle . '<br /><hr />';

$result_content_asset_course_title = mysqli_query($connection, $queryCourseTitle);

if (!$result_content_asset_course_title) {
	show_mysqli_error_message($queryCourseTitle, $connection);
	die;
}

while($c = mysqli_fetch_assoc($result_content_asset_course_title)) {
	$course_title = cleanEncoding($c['title']);
	// echo 'Course Title: '.$course_title.'<br />';
}

mysqli_free_result($result_content_asset_course_title);

// Get the Education Web Page Title	
$queryWebPageTitle  = "SELECT title FROM content_assets WHERE id = '$web_page_id' LIMIT 1";

// echo $queryWebPageTitle . '<br/><hr />';

$result_web_page_title = mysqli_query($connection, $queryWebPageTitle);

if (!$result_web_page_title) {
	show_mysqli_error_message($queryWebPageTitle, $connection);
	die;
}

while($r = mysqli_fetch_assoc($result_web_page_title)) {
	$web_page_title = cleanEncoding($r['title']);
}

mysqli_free_result($result_web_page_title);

// show_session();

if ($_SESSION['page']['use_title_bar']) { include_once('../includes/off_page_canvas_1.php'); }
// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

// show_session();
// show_session_message();

?>

<div class="row">

	<div class="small-12 medium-4 columns">
	
		<!-- Education Sidebar -->
	
		<?php
			include('../php/course_outline_query.php');		
			include('../php/topic_query.php');
			include('../php/content_asset_usages_query.php');					
			include('../php/education_sidebar.php'); 
			include('../php/topic_resources.php');
			include('../php/topic_products.php');
			include('../php/topic_assessments.php'); 
        ?>
		
	</div>
	
	<div class="small-12 medium-8 columns">
	
		<!-- Education Tabs: Watch, Read, Challenge, Resources, Quiz, Discuss -->
				
		<?php include('../php/education_tabs.php');	?>	
		
	</div>
</div>
		
<?php 
	
// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

if ($_SESSION['page']['use_title_bar']) { include_once('../includes/off_page_canvas_2.php'); }

// Begin rest of body content for page.

if ($_SESSION['page']['use_footer']) { include_once('../includes/page_footer.php'); }

if ($_SESSION['page']['use_disclaimer']) { include_once('../includes/disclaimer.php'); }

include_once('../includes/foundation_footer.php'); 

if ($_SESSION['page']['use_system_log']) { include_once('../php/system_log_page_access.php'); }

?>

<!-- Why is jquery-migrate being used here? -->
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="../js/slick.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.autoplay').slick({
		  arrows: true,
		  autoplay: true,
		  autoplaySpeed: 4000,		
		  dots: true,
		  fade: true,
		  focusOnSelect: true,
		  pauseOnHover: true,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  speed: 300,
		  swipe: true,
		});
	});
</script>
<script type="text/javascript" src="../js/ajax_discussion_forum.min.js"></script> 
