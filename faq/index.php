<?php 

// echo 'Begin FAQ page.<br />';

/*
if (!isset($_SESSION['enrollment']['project_program_id'])) {
	// echo 'Session Course ID is not set';
	$_SESSION['message_type'] = 'alert-box alert radius';				
	$_SESSION['message'] = '<p>Please select a <strong>Program Enrollment</strong> from the listing before proceeding to the FAQ page.</p>';		
	header("location: ../select-program-enrollment/");	
	// header("location: ../member-dashboard/");
	exit();	
}
*/

include_once('../includes/header.php');

// $_SESSION['page']['page_id'] = 19;
getPage('Frequently Asked Questions', $connection);

if ($_SESSION['page']['use_authorization']) { include_once('../includes/authorization.php'); }

include('../includes/page_header.php');

if ($_SESSION['page']['use_menu']) { include_once('../includes/menu.php'); }

if ($_SESSION['page']['use_title_bar']) { include_once('../includes/off_page_canvas_1.php'); }
// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

if (!empty($global_page_content_summary)) { echo $global_page_content_summary; }

if (!empty($global_page_content)) { echo $global_page_content; }

include('../php/show_project_program_faqs.php');

include('../php/system_log_page_access.php');  

// End body content that will appear inside of the Off Canvas <section class="main-section">
if ($_SESSION['page']['use_title_bar']) { include_once('../includes/off_page_canvas_2.php'); }
// Begin rest of body content for page.

if ($_SESSION['page']['use_footer']) { include_once('../includes/page_footer.php'); }

if ($_SESSION['page']['use_disclaimer']) { include_once('../includes/disclaimer.php'); }

include_once('../includes/foundation_footer.php'); 

if ($_SESSION['page']['use_system_log']) { include_once('php/system_log_page_access.php'); }

?>