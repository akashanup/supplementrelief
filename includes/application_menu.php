<?php
	
	// show_array($_SESSION);
	
	// Query the Application Menu Options
	$queryApplicationMenu = 'SELECT 
		afe.id,
		afe.application_id, 
		afe.project_program_id, 
		afe.functional_element_id, 
		afe.functional_element_type_code, 
		afe.seq AS menu_item_seq, 
		afe.name AS menu_item_name,
		afe.relative_url, 
		afe.active,  
		fet.name AS functional_element_type_name  
		FROM application_functional_elements afe  
		LEFT JOIN functional_element_types fet ON afe.functional_element_type_code = fet.code 
		WHERE afe.application_id = '.$_SESSION['application']['application_id'].' 
		AND afe.active = 1 
		AND afe.effective_date <= CURRENT_DATE  
		AND (afe.end_date is NULL or afe.end_date >= CURRENT_DATE)  
		ORDER BY afe.seq ASC';
	
	// echo $queryApplicationMenu;
	// die();
	
	$resultApplicationMenu = mysqli_query($connection, $queryApplicationMenu);
	
	if (!$resultApplicationMenu) {
		show_mysqli_error_message($queryApplicationMenu, $connection);
		die;
	}
	
	$total_menu_items = mysqli_num_rows($resultApplicationMenu);
		
	echo 'Total Menu Items: '.$total_menu_items.'<br />';
	
	while($r = mysqli_fetch_assoc($resultApplicationMenu)) { 
		
		show_array($r);
	    
	}
	
	mysqli_free_result($resultApplicationMenu);
	
?>
