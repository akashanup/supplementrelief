<?php
	
// assessment_functions.php
// located in /includes/

function gradeMCSA ($connection, $assessment_item_id = '', $assessment_item_option_id = '') { 
	
	// called from function gradeAssessmentResponse
	
	// Query the Assessment Item Option		
	$queryAssessmentItemOption = 'SELECT 
		aio.is_correct, 
		aio.feedback_text AS option_feedback_text   
		FROM assessment_item_options aio 
		WHERE aio.id = "'.$assessment_item_option_id.'" 
		LIMIT 1';
				
	// echo $queryAssessmentItemOption;
	// die();
	
	// Execute the query
	$result_assessment_item_option = mysqli_query($connection, $queryAssessmentItemOption);
	
	if (!$result_assessment_item_option) {
		show_mysqli_error_message($queryAssessmentItemOption, $connection);
		die;
	}
	
	$aio = mysqli_fetch_assoc($result_assessment_item_option);
	
	$_SESSION['assessment']['assessment_item']['option_feedback_text'] .= $aio['option_feedback_text'];
	
	// show_array($aio);
	
	if ($aio['is_correct']) {
		$response = TRUE;
		
	} else {
		$response = FALSE;
	}
	
	mysqli_free_result($result_assessment_item_option); 
	
	// echo 'Response: '.$response.'<br>';
	
	return $response;
	
}

function gradeMCMA ($connection, $assessment_session_id = '', $assessment_item_id = '') { 
	
	// called from function gradeAssessmentResponse
	
	// Query the Assessment Session Responses for the Assessment Item
	$queryAssessmentItemSessionResponses = 'SELECT 
		asr.assessment_item_id, 
		asr.assessment_item_option_id   
		FROM assessment_session_responses asr 
		WHERE asr.assessment_session_id = "'.$assessment_session_id.'" 
		AND asr.assessment_item_id = "'.$assessment_item_id.'"
		ORDER BY asr.created_timestamp ASC';
				
	// echo $queryAssessmentItemSessionResponses;
	// die();
	
	// Execute the query
	$result_assessment_item_session_response = mysqli_query($connection, $queryAssessmentItemSessionResponses);
	
	if (!$result_assessment_item_session_response) {
		show_mysqli_error_message($queryAssessmentItemSessionResponses, $connection);
		die;
	}
	
	$count_correct_answers = 0;
	$count_incorrect_answers = 0;
	
	while($asr = mysqli_fetch_assoc($result_assessment_item_session_response)) {
	
		// show_array($asr);
		
		$assessment_item_option_id = $asr['assessment_item_option_id'];
		
		// Query the Assessment Item Option to see if the response provided is correct
		$queryAssessmentItemOption = 'SELECT 
			aio.is_correct, 
			aio.feedback_text AS option_feedback_text     
			FROM assessment_item_options aio  
			WHERE aio.id = "'.$assessment_item_option_id.'" 
			LIMIT 1';
					
		// echo $queryAssessmentItemOption;
		// die();
		
		// Execute the query
		$result_assessment_item_option = mysqli_query($connection, $queryAssessmentItemOption);
		
		if (!$result_assessment_item_option) {
			show_mysqli_error_message($queryAssessmentItemOption, $connection);
			die;
		} 

		$aio = mysqli_fetch_assoc($result_assessment_item_option);
		
		$_SESSION['assessment']['assessment_item']['option_feedback_text'] .= $aio['option_feedback_text'];

		if ($aio['is_correct']) {
			$count_correct_answers++;
			
		} else {
			$count_incorrect_answers++;
		}
		
		mysqli_free_result($result_assessment_item_option); 
		
		// echo 'Response: '.$response.'<br>';

	}
	
	mysqli_free_result($result_assessment_item_session_response); 
	
	// Now see if they ommitted any correct responses.	
	$queryAssessmentItemOptions = 'SELECT 
	aio.id, 
	aio.is_correct, 
	aio.feedback_text AS option_feedback_text    
	FROM assessment_item_options aio  
	WHERE aio.assessment_item_id = "'.$assessment_item_id.'" 
	AND is_correct = 1';
					
	// echo $queryAssessmentItemOptions;
	// die();
		
	// Execute the query
	$result_assessment_item_option = mysqli_query($connection, $queryAssessmentItemOptions);
	
	if (!$result_assessment_item_option) {
		show_mysqli_error_message($queryAssessmentItemOptions, $connection);
		die;
	}
	
	$count_missing_answers = 0; 

	while($aio = mysqli_fetch_assoc($result_assessment_item_option)) { 
	
		// Now see if the user provided this correct response	
		// show_array($aio);
		
		// $_SESSION['assessment']['assessment_item']['option_feedback_text'] .= $aio['option_feedback_text'];

		$assessment_item_option_id = $aio['id'];
		
		$queryAssessmentSessionCorrectResponse = 'SELECT 
		asr.id 
		FROM assessment_session_responses asr   
		WHERE asr.assessment_session_id = "'.$assessment_session_id.'" 
		AND asr.assessment_item_option_id = "'.$assessment_item_option_id.'" 
		LIMIT 1';
		
		// echo $queryAssessmentSessionCorrectResponse;
		// die();
		
		$result_assessment_session_correct_response = mysqli_query($connection, $queryAssessmentSessionCorrectResponse);

		if (!$result_assessment_session_correct_response) {
			show_mysqli_error_message($queryAssessmentSessionCorrectResponse, $connection);
			die;
		} 
		
		$total_records = mysqli_num_rows($result_assessment_session_correct_response);
		
		// $asr = mysqli_fetch_assoc($result_assessment_session_response);
		
		if ($total_records == 0) {
			// a correct response was not found
			$count_missing_answers++;	
		} else {
			// echo 'asr response found<br>';
		}

		mysqli_free_result($result_assessment_session_correct_response); 

	}
	
	if ($count_incorrect_answers > 0 OR $count_missing_answers > 0) {
		$response = FALSE;
	} else {
		$response = TRUE;
	}
	
	/*
	echo 'Counts: <br>
	Correct Answers: '.$count_correct_answers.'<br>
	Incorrect Answers: '.$count_incorrect_answers.'<br>
	Missing Answers: '.$count_missing_answers.'<br>';
	*/
	
	// die;
	return $response;

}

function gradeTF ($connection, $assessment_item_id = '', $assessment_item_option_id = '') { 
	
	// called from function gradeAssessmentResponse
	
	// Query the Assessment Item Option		
	$queryAssessmentItemOption = 'SELECT 
		aio.is_correct, 
		aio.feedback_text AS option_feedback_text   
		FROM assessment_item_options aio 
		WHERE aio.id = "'.$assessment_item_option_id.'" 
		LIMIT 1';
				
	// echo $queryAssessmentItemOption;
	// die();
	
	// Execute the query
	$result_assessment_item_option = mysqli_query($connection, $queryAssessmentItemOption);
	
	if (!$result_assessment_item_option) {
		show_mysqli_error_message($queryAssessmentItemOption, $connection);
		die;
	}
	
	$aio = mysqli_fetch_assoc($result_assessment_item_option);
	
	$_SESSION['assessment']['assessment_item']['option_feedback_text'] .= $aio['option_feedback_text'];
	
	// show_array($aio);
	
	if ($aio['is_correct']) {
		$response = TRUE;
		
	} else {
		$response = FALSE;
	}
		
	mysqli_free_result($result_assessment_item_option); 
	
	// echo 'Response: '.$response.'<br>';
	
	return $response;
	
}

function createAssessmentSessionResponse ($connection, $assessment_session_id = '', $assessment_id = '', $assessment_item_id = '', $assessment_item_option_id = '', $text_response = '') {
	
	$created_timestamp = date('Y/m/d H:i:s');
			
	$queryInsert = '
	INSERT INTO assessment_session_responses (created_timestamp, assessment_session_id, assessment_id, assessment_item_id, assessment_item_option_id, text_response) 
	VALUES ( 
	"'.$created_timestamp.'", 
	"'.$assessment_session_id.'",
	"'.$assessment_id.'",
	"'.$assessment_item_id.'",
	"'.$assessment_item_option_id.'",
	'.no_value_null_check($text_response).')';
			
	// echo $queryInsert;	
	// die();
	
	$result_insert = mysqli_query($connection, $queryInsert);
			
	if (!$result_insert) {
		show_mysqli_error_message($queryInsert, $connection);
		die;
	}

	// $last_insert_id = mysqli_insert_id($connection); 		
					
}

function gradeAssessmentResponse ($connection, $assessment_session_id = '', $assessment_id = '', $assessment_item_id = '', $assessment_item_type_code = '', $assessment_item_option_id = '', $text_response = '') { 
	
	// show_array($_SESSION['assessment']['assessment_item']);
	
	// echo 'Inside of gradeAssessmentResponse<br>';
	// echo 'assessment item type code: '.$assessment_item_type_code.'<br>';
	
	$response = FALSE;
	
	switch ($assessment_item_type_code) {		
	
    	case 'MCSA':
    	   	// echo $assessment_item_type_code.' processing TBD.<br />';
		   	$response = gradeMCSA($connection, $assessment_item_id, $assessment_item_option_id);     		
		   	break;
		   	
		case 'MCMA':
    	   	// echo $assessment_item_type_code.' processing TBD.<br />';
		   	$response = gradeMCMA($connection, $assessment_session_id, $assessment_item_id);     		
		   	break;
		   	
		case 'TF':
    	   	// echo $assessment_item_type_code.' processing TBD.<br />';
		   	// $response = gradeTF($connection, $assessment_item_id, $assessment_item_option_id);
		   	$response = gradeTF($connection, $assessment_item_id, $assessment_item_option_id);     		
		   	break;
		   	
	}
	
	?>
	
	<!-- <br /> -->
	
	<div class="row" id="assessment-item-feedback">		
		<div class="small-12 columns">
			
			<div class="panel">
			
			<?php
			
			if ($response) { 
				
				// Response is correct
				$_SESSION['assessment']['user_correct_responses']++;
				
				?>
				<h3><i class="fa fa-check fa-2x icon-green" aria-hidden="true"></i>&nbsp;Correct</h3>
				<p><?php echo $_SESSION['assessment']['assessment_item']['option_feedback_text']; ?></p>
				<p><?php echo $_SESSION['assessment']['assessment_item']['correct_feedback']; ?></p>
				<?php		
				
			} else {
				
				// Response is incorrect
				$_SESSION['assessment']['user_incorrect_responses']++;
				?>
				<h3><i class="fa fa-times fa-2x icon-red" aria-hidden="true"></i>&nbsp;Incorrect</h3>
				<p><?php echo $_SESSION['assessment']['assessment_item']['option_feedback_text']; ?></p>
				<p><?php echo $_SESSION['assessment']['assessment_item']['incorrect_feedback']; ?></p>
				<?php		
				
			}
						
			if ($_SESSION['assessment']['items_to_complete'] > 0) {
				
				if ($_SESSION['assessment']['items_to_complete'] > 1) { 
				
					$button_text = 'Next Question';
					
				} else {
					
					$button_text = 'Last Question';
				}
				
				?>
				
				<!-- <input id="show-next-question" class="button large radius success" type="submit" value="<?php echo $button_text; ?>"> -->
				<div class="show-for-medium-up">
					<a href="../assessment/" class="button large radius success"><?php echo $button_text; ?></a>
				</div>
				
				<div class="show-for-small-only">
					<a href="../assessment/" class="button large radius success expand"><?php echo $button_text; ?></a>
				</div>						    	
				
				<?php
				
			} else {
				
				// items completed
				
				?>
				<a href="../assessment-results/" class="button large radius success">Show <?php echo $_SESSION['assessment']['type']; ?> Results</a>
				<?php
				
			}
			
			?>
			
			</div>
								
		</div>
	</div>

	<?php	
					
}
