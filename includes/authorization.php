<?php

// Determines if the User is allowed to access the page based upon the User's AND the Page's respective Access Level.
// echo 'Begin authorization.php<br /><hr />';
// die;

// $_SESSION['page']['page_id'] = 4;
// page_id is set at the beginning of each page that uses this script using the getPage() function.

$queryPageAuth = 'SELECT 
	pg.access_level 
	FROM pages pg 
	WHERE pg.id = "'.$_SESSION['page']['page_id'].'"   
	LIMIT 1';
			
// echo $queryPage . '<br /><hr />';

$pageAuth = mysqli_query($connection, $queryPageAuth);

if (!$pageAuth) {
	show_mysqli_error_message($queryPageAuth, $connection);
	die;
}

$r = mysqli_fetch_assoc($pageAuth);

// $_SESSION['page']['name'] = $r['name'];	
$_SESSION['page']['access_level'] = $r['access_level'];
	
mysqli_free_result($pageAuth);

// Determine the Page Access Level	(0 = Public, 1 = User, 10 = Admin)
if ($_SESSION['page']['access_level'] > 0) {
	// echo 'Secure page. Could be User, Admin or some other level TBD.<br />';
	// die;

	if(!$_SESSION['user']['user_id']) {
		// echo 'User not authenticated so send them to Login.<br />';
		// die;
        // echo 'Session User ID empty - no previous User Login.<br /><hr />';		
		// Store the Page they are trying to access. 
		$_SESSION['target_uri'] = 'https://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; // think this is current page, not the one they were trying to access
		
		// Log system event.	
		$event = 'ACCESS';
		$log_description = 'UNAUTHORIZED ACCESS to system without LOGIN for '.$_SESSION['page']['name'].'.';
		$flag = 1;	
		create_system_log($connection, $event, $_SESSION['page']['name'], $log_description, $flag);
	
		$_SESSION['message_type'] = 'alert-box alert radius'; // none (standard), alert, success, warning, info, secondary	
		$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Please <strong>LOGIN</strong> before accessing the page you are trying to access.</p>';
		header("location: ../login/");
		exit();
	}
	
	// Determine if the Logged In User has permission to access the Page.
	if($_SESSION['user']['access_level'] < $_SESSION['page']['access_level']) {
	
		// User does not have permission.
		echo 'User DOES NOT have access to this page.<br />';
		die;
		// Log system event.
		
		$event = 'ACCESS';
		$log_description = 'UNAUTHORIZED ROLE ACCESS to system for '.$_SESSION['page']['name'].' for '.$_SESSION['user']['username'].'.';	
		$flag = 1;	
		create_system_log($connection, $event, $_SESSION['page']['name'], $log_description, $flag);
	
		$_SESSION['message_type'] = 'alert-box alert radius'; 
		$_SESSION['message'] = '<p>You attempted to access a Page you do not have permission to access. This has been recorded.</p>';
		header("location: ../logout/");
		exit();	
	}	
	
}

// We have either:
// a. Secure Page AND the User has been validated to access it, or
// b. Public page
// echo 'User HAS access to this page.<br />';

?>