<?php 

// echo 'Begin core.php.<br />;
	
$ie9 = false;
if(preg_match('/(?i)msie 9/',$_SERVER['HTTP_USER_AGENT']))
{
	$ie9 = true;
}
	
// Open database connection
include('defines.php');

// Added by Ivan 8/18/15 to replace Adam's secure session code.
include_once('SecureSession.php');
session_start();

// Establish mysql and mysqli database connections
// $con = mysql_connect("192.163.252.247","rtsadven_admin","2013Admin");
$con = mysql_connect(DB_SERVER,DB_USER,DB_PASS);
mysql_select_db(DB_NAME, $con);

$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

if(mysqli_connect_errno()) {
    die("Database connection failed: " . 
         mysqli_connect_error() . 
         " (" . mysqli_connect_errno() . ")"
    );
}

//	Define Functions

function show_jay_session($string = '') {

    if ($_SESSION['user']['user_id'] == 2) {
        show_array($_SERVER); 
        show_array($_SESSION);  
    }
    
}

function googleRecaptcha($private_captcha_key, $captcha_response, $remote_ip, $return_url) {
	
	// Google Recaptcha server side integration		
	$googleURL = "https://www.google.com/recaptcha/api/siteverify?secret=" . $private_captcha_key . "&response=" . $captcha_response . "&remoteip=" . $remote_ip;
	
	// https://www.sitepoint.com/using-curl-for-remote-requests/
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $googleURL);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 0);
	
	// execute
	$googleResultJSON = curl_exec($ch);
	
	// free
	curl_close($ch);
	
	// Verify the Google result
	// Convert JSON return string to PHP array
	// http://www.dyn-web.com/tutorials/php-js/json/decode.php
	$googleResultPHPArray = json_decode($googleResultJSON, true);
		
	if ($googleResultPHPArray["success"] != true) {
		$_SESSION['message_type'] = 'alert-box alert radius';		
		$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Please let us know that <strong>you are NOT a ROBOT!</strong></p>';
		// $_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';

		/* special ajax here */
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$json 						=	[];
			$json['error']				=	true;
			$json['message_type']		=	$_SESSION['message_type'];
			$json['message']			=	$_SESSION['message'];
			$json 						=	json_encode($json);
			$_SESSION['message_type']	=	'';
			$_SESSION['message']		=	'';
			die($json);
		}
						
		header("location: ".$return_url);
		exit();	
	
	}
	
	return TRUE;
	
}

// Uploads user media files in Discussion Forums to server. 
function uploadMedia($postType, $mediaType, $mediaFileArray, $thread_title, $thread_text, $thread_reply_text) {
	
	// postType: thread, reply
	// mediaType: audio, document, image, video
	
	global $thread_title, $thread_text, $thread_reply_text;
	
	// Upload document file
	if ($mediaType == 'document') {
				
		$document_file_name = $mediaFileArray['name'];
		$document_file_type = $mediaFileArray['type'];
		$tmp_document_file_name = $mediaFileArray['tmp_name'];
		$document_file_error = $mediaFileArray['error'];
		$document_file_size = $mediaFileArray['size'];
								
		$document_file_extension = strtolower(substr($document_file_type, strpos($document_file_type, '/') +1));		
		$unique_filename = strtolower(substr($tmp_document_file_name, strpos($tmp_document_file_name, '/tmp/') +5)).'.'.$document_file_extension;	
		$document_file_url = 'https://'.user_media_domain.'/media/documents/user/'.$unique_filename;
		
		if ($document_file_extension == 'pdf' || $document_file_extension == 'txt') {
				
			$directory = user_media_upload_directory.'/documents/user/';			
			$destination = $directory.$unique_filename;	
												
			if (!move_uploaded_file($tmp_document_file_name, $destination)) {
				
				/* special ajax here */
				if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
					$json 					=	[];
					$json['error']			=	true;
					$json['message_type']	=	'alert-box warning radius';
					$json['message']		=	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Upload failed. Please contact System Administrator!</strong></p>';
					$json 					=	json_encode($json);
					die($json);
				}		
				echo '<span class="formErrorMessage">Upload failed. Please contact System Administrator!</span><br><hr>';

				die;	
				
			}
			
			return array($document_file_name, $document_file_url, $document_file_extension); 
			
		} else {
						
			if ($postType == 'thread') {	
				$_SESSION['temp']['thread_title'] = $thread_title;	// So user does not loose the Comment previously entered on the form	
				$_SESSION['temp']['thread_text'] = $thread_text;				
			} elseif ($postType == 'reply') {
				$_SESSION['temp']['thread_reply_text'] = $thread_reply_text;
			}
				
			$_SESSION['message_type'] = 'alert-box warning radius';				
			$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Document File Type</strong> must be <b>pdf or txt</b>. The file type you submitted was <b>'.$document_file_extension.'</b>. Please choose a Document with a supported File Type.</p>';
			$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#fndtn-discuss';
			
			/* special ajax here */
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				$json 						=	[];
				$json['error']				=	true;
				$json['message_type']		=	$_SESSION['message_type'];
				$json['message']			=	$_SESSION['message'];
				$json 						=	json_encode($json);
				$_SESSION['message_type']	=	'';
				$_SESSION['message']		=	'';
				die($json);
			}
			
			header("location: ".$_SESSION['target_uri']);
			exit();	
			
		}			
		
	}
	
	// Upload Image file	
	if ($mediaType == 'image') {
		
		$image_file_name = $mediaFileArray['name'];
		$image_file_type = $mediaFileArray['type'];
		$tmp_image_file_name = $mediaFileArray['tmp_name'];
		$image_file_error = $mediaFileArray['error'];
		$image_file_size = $mediaFileArray['size'];
										
		$image_file_extension = strtolower(substr($image_file_type, strpos($image_file_type, '/') +1));		
		$unique_filename = strtolower(substr($tmp_image_file_name, strpos($tmp_image_file_name, '/tmp/') +5)).'.'.$image_file_extension;	
		// $image_file_url = 'https://'.user_media_domain.'/media/images/user/'.$unique_filename;
				
		// Check for a valid image filetype.
		if ($image_file_extension == 'jpg' || $image_file_extension == 'jpeg' || $image_file_extension == 'png' || $image_file_extension == 'gif') {
		
			$directory = user_media_upload_directory.'/images/user/';		
			$destination = $directory.$unique_filename;	
			
			if (move_uploaded_file($tmp_image_file_name, $destination)) {
						
				// proportionally resize images to fit in max-width box parameter
				create_thumb($destination, 300);
				$image_file_url = 'https://'.user_media_domain.'/media/images/user/tn_'.$unique_filename;
														
			} else {
				
				/* special ajax here */
				if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
					$json 					=	[];
					$json['error']			=	true;
					$json['message_type']	=	'alert-box warning radius';
					$json['message']		=	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Upload failed. Please contact System Administrator!</strong></p>';
					$json 					=	json_encode($json);
					die($json);
				}

				echo '<span class="formErrorMessage">Upload failed. Please contact System Administrator!</span><br><hr>';
				die;	
				
			}
			
			return array($image_file_name, $image_file_url, $image_file_extension);
		
		} else {
		
			// Invalid Image File Type so provide user with error message
			
			if ($postType == 'thread') {	
				$_SESSION['temp']['thread_title'] = $thread_title;	// So user does not loose the Comment previously entered on the form	
				$_SESSION['temp']['thread_text'] = $thread_text;				
			} elseif ($postType == 'reply') {
				$_SESSION['temp']['thread_reply_text'] = $thread_reply_text;
			}			

			$_SESSION['message_type'] = 'alert-box warning radius';				
			$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Image File Type</strong> must be <b>jpg, jpeg, png or gif</b>. The file type you submitted was <b>'.$image_file_extension.'</b>. Please choose an Image with a supported File Type.</p>';
			$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#fndtn-discuss';							
			/* special ajax here */
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				$json 						=	[];
				$json['error']				=	true;
				$json['message_type']		=	$_SESSION['message_type'];
				$json['message']			=	$_SESSION['message'];
				$json 						=	json_encode($json);
				$_SESSION['message_type']	=	'';
				$_SESSION['message']		=	'';
				die($json);
			}

			header("location: ".$_SESSION['target_uri']);
			exit();	
			
		} 		
		 	
	}
	
	// Upload Video file
	if ($mediaType == 'video') {
																	
		$video_file_name = $mediaFileArray['name'];
		$video_file_type = $_FILES['video_file'] ['type'];
		$tmp_video_file_name = $_FILES['video_file'] ['tmp_name'];
		$video_file_error = $_FILES['video_file'] ['error'];
		$video_file_size = $_FILES['video_file'] ['size'];
		
		$video_file_extension = strtolower(substr($video_file_type, strpos($video_file_type, '/') +1));	

		// Check for a valid HTML5 video filetype.
		if ($video_file_extension != 'mp4' && $video_file_extension != 'webm') {
		
			// echo 'video_file_extension != mp4 or webm: '.$video_file_extension.'<br />';
			
			if ($postType == 'thread') {	
				$_SESSION['temp']['thread_title'] = $thread_title;	// So user does not loose the Comment previously entered on the form	
				$_SESSION['temp']['thread_text'] = $thread_text;				
			} elseif ($postType == 'reply') {
				$_SESSION['temp']['thread_reply_text'] = $thread_reply_text;
			}			
		
			$_SESSION['message_type'] = 'alert-box warning radius';				
			$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Video File Type</strong> must be <b>mp4</b> or <b>webm</b>. The file type you submitted was <b>'.$video_file_extension.'</b>. Please choose a Video with a supported File Type.</p>';
			$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#fndtn-discuss';						

			/* special ajax here */
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				$json 						=	[];
				$json['error']				=	true;
				$json['message_type']		=	$_SESSION['message_type'];
				$json['message']			=	$_SESSION['message'];
				$json 						=	json_encode($json);
				$_SESSION['message_type']	=	'';
				$_SESSION['message']		=	'';
				die($json);
			}

			header("location: ".$_SESSION['target_uri']);
			exit();
		}		
	
		// Check the size of the file uploaded.
		// This needs to be done in JS so that the user gets immediate feedback not having to wait for a large file to first upload (or time out) before it gets rejected for being too large.
		// The upload process needs to somehow be done in the background anyway because the user experience is too slow.
		// How does Facebook do it?

		// 1000000 = 1 MB
		// 5037394 = 5 MB
		// 10000000 = 10 MB
		if ($video_file_size > 30000000) {
			// echo 'File sized exceeded 30MB.<br />';
		    // File size is too large so provide user with error message.
		    
			if ($postType == 'thread') {	
				$_SESSION['temp']['thread_title'] = $thread_title;	// So user does not loose the Comment previously entered on the form	
				$_SESSION['temp']['thread_text'] = $thread_text;				
			} elseif ($postType == 'reply') {
				$_SESSION['temp']['thread_reply_text'] = $thread_reply_text;
			}			
		
		    $_SESSION['message_type'] = 'alert-box warning radius';				
		    $_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Video File Size</strong> must be less than 30MB. Please choose a smaller-size Video.</p>';
		    $_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#fndtn-discuss';							
		    /* special ajax here */
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				$json 						=	[];
				$json['error']				=	true;
				$json['message_type']		=	$_SESSION['message_type'];
				$json['message']			=	$_SESSION['message'];
				$json 						=	json_encode($json);
				$_SESSION['message_type']	=	'';
				$_SESSION['message']		=	'';
				die($json);
			}

		    header("location: ".$_SESSION['target_uri']);
		    exit();	
		}
		
		$unique_filename = strtolower(substr($tmp_video_file_name, strpos($tmp_video_file_name, '/tmp/') +5)).'.'.$video_file_extension;	
		$video_file_url = 'https://'.user_media_domain.'/media/videos/user/'.$unique_filename;
		$directory = user_media_upload_directory.'/videos/user/';			
		$destination = $directory.$unique_filename;
		
		/*try 
		{
			require_once('../vendor/autoload.php');
			$ffmpeg = FFMpeg\FFMpeg::create(array(
			    'ffmpeg.binaries'  => '../ffmpeg/bin/ffmpeg',
			    'ffprobe.binaries' => '../ffmpeg/bin/ffprobe',
			    'timeout'          => 3600, // The timeout for the underlying process
			    'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
			), $logger);
	    
			$video  = $ffmpeg->open($tmp_video_file_name);
			
			$video
			    ->filters()
			    ->resize(new FFMpeg\Coordinate\Dimension(320, 240))
			    ->synchronize();
			
			$video
			    ->save(new FFMpeg\Format\Video\WebM(), $destination);

			return array($video_file_name, $video_file_url, $video_file_extension);

		} 
		catch (Exception $e) 
		{
			
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				$json 					=	[];
				$json['error']			=	true;
				$json['message_type']	=	'alert-box warning radius';
				$json['message']		=	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Upload failed. Please contact System Administrator!</strong></p>';
				$json 					=	json_encode($json);
				die($json);
			}
			$_SESSION['message_type'] = 'alert-box warning radius';				
		    $_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Upload failed. Please contact System Administrator!</p>';
		    $_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#fndtn-discuss';							
		    
			header("location: ".$_SESSION['target_uri']);
		    exit();
		}*/

		if (!move_uploaded_file($tmp_video_file_name, $destination)) {
			
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				$json 					=	[];
				$json['error']			=	true;
				$json['message_type']	=	'alert-box warning radius';
				$json['message']		=	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Upload failed. Please contact System Administrator!</strong></p>';
				$json 					=	json_encode($json);
				die($json);
			}

			$_SESSION['message_type'] = 'alert-box warning radius';				
		    $_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Upload failed. Please contact System Administrator!</p>';
		    $_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#fndtn-discuss';							
		    
			header("location: ".$_SESSION['target_uri']);
		    exit();	
			
		} else {

			return array($video_file_name, $video_file_url, $video_file_extension);
			
		}
			 	
	} 
		
}

function getCustomerNumberOrders($connection, $order_status = '', $user_id = '', $order_created_timestamp = '', $last_name = '', $zip_code = '') {
    
    // Calculate Total # Orders for this Person
    $user_orders = 0;
    $revenue_order = array("1", "2", "3", "4");

    if (in_array($order_status, $revenue_order)) { 
    	// Revenue orders are processed for 1) Total Orders for that Customer, 2) Accounting Summary
    										
    	// #1 Total Customer Orders. Determine Total Orders for this Customer. Can be a Member or a Public User.
    	if ($user_id > 0) {
    		// Member Account
    		// echo $c['user_id'].'<br>';
    		
    		// Determine how many valid Orders this User has prior to and including this Order.	
    		$queryCustomerOrders = '
    			SELECT count(*) AS order_count 
    			FROM orders   
    			WHERE user_id = "'.$user_id.'"
    			AND created_timestamp <= "'.$order_created_timestamp.'"
    			AND order_status in (1,2,3,4)';
    													
    	} else {
    		// Public User (not a Member Account)
    		// echo 'Not a Member order<br>';
    		
    		// Determine how many valid Orders this User has prior to and including this Order.	
    		$queryCustomerOrders = '
    			SELECT count(*) AS order_count 
    			FROM orders   
    			WHERE last_name = "'.$last_name.'" 
    			AND zip_code = "'.$zip_code.'" 
    			AND created_timestamp <= "'.$order_created_timestamp.'"
    			AND order_status in (1,2,3,4)';
    		    		
    	}
    	
        // echo $queryCustomerOrders.'<br>';
        // die();
    			
    	// Execute the query
    	$result_order_count = mysqli_query($connection, $queryCustomerOrders);
    	
    	if (!$result_order_count) {
    		show_mysqli_error_message($queryCustomerOrders, $connection);
    		die;
    	}
    	
    	$oc = mysqli_fetch_assoc($result_order_count);
    	
    	$user_orders = $oc['order_count'];
    	
    	mysqli_free_result($result_order_count);	
    	
    	return $user_orders;
    	
    }
    
    return false;
    
}

function getBrowser($ub = '') 
// http://www.useragentstring.com/pages/useragentstring.php
{     
    if (strlen($ub) > 0) {
        $u_agent = $ub;   
    } else {
        $u_agent = $_SERVER['HTTP_USER_AGENT']; 			
    }
      
    $bname = 'Unknown';
    $platform = 'Unknown';
    $device_type = 'Computer';
    $version= "";

    // 1. Determine the platform.
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'Linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'Mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'Windows';
    }
    elseif (preg_match('/googlebot|bingbot/i', $u_agent)) {
        $platform = 'Bot';
    }
    
    // 2. Determine if a Robot, SmartPhone, Tablet or Computer device type.
    if(preg_match('/iphone/i',$u_agent) OR preg_match('/andriod/i',$u_agent)) 
    {   // What identifies other types of SmartPhones? Does andriod also identify a Tablet?
        $device_type = 'SmartPhone'; 
    } 
    elseif(preg_match('/ipad/i',$u_agent)) 
    {   // What identifies other types of Tablets?
        $device_type = 'Tablet'; 
    } 
    elseif (preg_match('/bingbot/i', $u_agent)) {
        $device_type = 'BingBot';
    }
    elseif (preg_match('/googlebot/i', $u_agent)) {
        $device_type = 'GoogleBot';
    }

    // 3. Determine other aspects of useragent for specific Browser and Version
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Trident/i',$u_agent)) 
    { // this condition is for IE11
        $bname = 'Internet Explorer'; 
        $ub = "rv"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    
    // 4a. Determine the correct Version number
    // Added "|:"
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
     ')[/|: ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

    // 4b. See how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        // We will have two since we are not using 'other' argument yet
        // See if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }

    // 4c. Check if we have a number
    if ($version==null || $version=="") {$version="?";}

    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'deviceType'  => $device_type,
        'pattern'    => $pattern
    );
} 

function createUserSessionLog($connection) {
    
	// Create User Session Log
	
	if( isset($_SESSION['application']['application_id']) && $_SESSION['application']['application_id'] > 0 ) $application_id = $_SESSION['application']['application_id'];
    else $application_id = public_application_id; // /includes/defines.php
    
    if( isset($_SESSION['enrollment']['project_id']) && $_SESSION['enrollment']['project_id'] > 0 ) $project_id = $_SESSION['enrollment']['project_id'];
    else $project_id = public_project_id; // /includes/defines.php
	
	$created_timestamp = date('Y/m/d H:i:s');
	
	$browser=getBrowser(); // No param is passed so will use $_SERVER['HTTP_USER_AGENT']. 
	// $ua=getBrowser();
	// $yourbrowser= "Your browser: " . $ua['name'] . " " . $ua['version'] . " on " .$ua['platform'] . " reports: <br >" . $ua['userAgent'];	
	
	$queryInsertUserSession = '
	INSERT INTO user_sessions (
    	application_id, 
    	project_id, 
		created_timestamp, 
		http_referer, 
		query_string, 
		remote_address, 
		http_user_agent, 
		browser_device_type, 
		browser_platform, 
		browser_name, 
		browser_version,  
		request_uri,
		page_id)
	VALUES (
    "'.$application_id.'",	 
    "'.$project_id.'", 	 
	"'.$created_timestamp.'",	 
	"'.safe_data($_SESSION['userStatistics']['HTTP_REFERER']).'",	
	"'.safe_data($_SESSION['userStatistics']['QUERY_STRING']).'",	
	"'.safe_data($_SESSION['userStatistics']['REMOTE_ADDR']).'",
	"'.safe_data($browser['userAgent']).'",
    "'.safe_data($browser['deviceType']).'",
	"'.safe_data($browser['platform']).'",
	"'.safe_data($browser['name']).'",
	"'.safe_data($browser['version']).'",
	"'.safe_data($_SESSION['userStatistics']['REQUEST_URI']).'",
    "'.safe_data($_SESSION['page']['page_id']).'")';							 
	
	// echo $queryInsertUserSession.'<br /><hr />';
	// die;
	
	$result_insert = mysqli_query($connection, $queryInsertUserSession);
	
	if (!$result_insert) {
		show_mysqli_error_message($queryInsertUserSession, $connection);
		die;
	}
	
	// Get the insert_id of the record just created in the database for the User Session ID
	$last_insert_id = mysqli_insert_id($connection);
	 
	$_SESSION['userStatistics']['session_id'] = $last_insert_id; // Used later when creating an Order to associate the Order with the User Session for Marketing.
	// $_SESSION['userStatistics']['userAgent'] = $browser['userAgent'];
	$_SESSION['userStatistics']['device_type'] = $browser['deviceType'];
	$_SESSION['userStatistics']['platform'] = $browser['platform'];
	$_SESSION['userStatistics']['browser'] = $browser['name'];
	$_SESSION['userStatistics']['browser_version'] = $browser['version'];
		
}

function getMarketingReferralURI() {
	    	    
    if (!isset($_SESSION['userStatistics'])) {
	    // Get sets for the first page only when a new User lands anywhere on the website.
	    $_SESSION['userStatistics']['HTTP_REFERER'] = $_SERVER['HTTP_REFERER']; // What URI did they come from?
	    $_SESSION['userStatistics']['QUERY_STRING'] = $_SERVER['QUERY_STRING']; // What query parameters did they come in with?
	    $_SESSION['userStatistics']['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR']; // What IP Address did they come from?
	    $_SESSION['userStatistics']['HTTP_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT']; // What browser are they using?	    
	    $_SESSION['userStatistics']['REQUEST_URI'] = $_SERVER['REQUEST_URI']; // What page did the request?
	    
	    return true;      
	    
    }
    
    return false;
    	
}

function updateUserSessionLogin($connection, $session_id, $user_id) { 
    
	// Update User Session Log with Login User ID
	$login_timestamp = date('Y/m/d H:i:s');

	$queryUpdateUserSession = '
	UPDATE user_sessions SET 
	user_id = "'.$user_id.'", 
	login_timestamp = "'.$login_timestamp.'" 
	WHERE id = "'.$session_id.'"';	
	
	// echo $queryUpdateUserSession;	
	// die();
		
	$result_update_user_session = mysqli_query($connection, $queryUpdateUserSession);
	
	if (!$result_update_user_session) {
		show_mysqli_error_message($queryUpdateUserSession, $connection);
		die;
	}
	
	// mysqli_free_result($result_update_user_session);

    return true;
    	
}

// Gets the correct Page ID for the Application. Used so SOZO, SupplementRelief and other applications (deployed to unique URLs) can share the same code base to minimize maintenance. Each unique Page has database attributes that determine it's behavior and content.
function getPage($page_name, $connection) {
	
	if (!isset($_SESSION['application']['application_id'])) {
		$_SESSION['application']['application_id'] = public_application_id; // /includes/defines.php
	}

	// Get the Page ID based upon the Application ID and Page Name.				
	$queryApplicationPage = 'SELECT 
		pg.id AS page_id 
		FROM pages pg
		WHERE pg.application_id = "'.$_SESSION['application']['application_id'].'" 
		AND pg.name = "'.$page_name.'"
		LIMIT 1';   
				
	// echo $queryApplicationPage . '<br /><hr />';
	
	$result_application_page = mysqli_query($connection, $queryApplicationPage);
	
	if (!$result_application_page) {
		show_mysqli_error_message($queryApplicationPage, $connection);
		die;
	}
	
	$r = mysqli_fetch_assoc($result_application_page);
	
    // show_array($r);
    $_SESSION['page']['page_id'] = $r['page_id'];
	
	mysqli_free_result($result_application_page);
	
	// $_SESSION['page']['page_id'] = $page_id;

	return $_SESSION['page']['page_id']; // Used in page_header.php

}

// Sanitize the Phone
function sanitizePhone($phone, $country_abbr) {

	if ( ( strlen($phone) > 0 AND $country_abbr == "US" ) ) { 					
		$ourNumber = preg_replace('/[^0-9,]+/i', '', $phone);		
		$phonePart1 = substr($ourNumber, 0, 3);
		$phonePart2 = substr($ourNumber, 3, 3);
		$phonePart3 = substr($ourNumber, 6, 4);		
		$formattedPhone = '('.$phonePart1.') '.$phonePart2.'-'.$phonePart3;		
		// echo $formattedPhone.' '.$phone.' '.$email.'<br>';		
		return $formattedPhone; // (555) 555-1212	
	} else {
		// No formatting for phone outside of USA and Canada
		return $phone;
	}	

}

// InitCap a string of data
function initCapData($string) {
	$lowercaseString = strtolower($string);
	$ucTitleString = ucwords($lowercaseString);
	return $ucTitleString; // Init Cap String
}

// Enforce a minimum String Length 
function minimumStringLength($string, $length) {
	
	if (strlen($string) < $length) {
		return false;				
	} else {
		return true;
	}
	
}	
			
// Cleaning variables that can be injected.
function sanitize($data) {
	$data = trim($data); // Trim leading/trailing whitespace
	$data = htmlspecialchars($data); // Prevent html/script injection
	$data = addslashes($data); // Escape quotes/slashes/nullchars
	$data = cleanEncoding($data); // Clean everything else
	
	return $data; // Return Squeaky-clean.
}

function cleanEncoding( $text, $type='standard' ){
    // determine the encoding before we touch it
    $encoding = mb_detect_encoding($text, 'UTF-8, ISO-8859-1');
    // The characters to output
    if ( $type=='standard' ){
        $outp_chr = array('...',          "'",            "'",            '"',            '"',            '•',            '-',            '-'); // run of the mill standard characters
    } elseif ( $type=='reference' ) {
        $outp_chr = array('&#8230;',      '&#8216;',      '&#8217;',      '&#8220;',      '&#8221;',      '&#8226;',      '&#8211;',      '&#8212;'); // decimal numerical character references
    }
    // The characters to replace (purposely indented for comparison)
        $utf8_chr = array("\xe2\x80\xa6", "\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", '\xe2\x80\xa2', "\xe2\x80\x93", "\xe2\x80\x94"); // UTF-8 hex characters
        $winc_chr = array(chr(133),       chr(145),       chr(146),       chr(147),       chr(148),       chr(149),       chr(150),       chr(151)); // ASCII characters (found in Windows-1252)
    // First, replace UTF-8 characters.
    $text = str_replace( $utf8_chr, $outp_chr, $text);
    // Next, replace Windows-1252 characters.
    $text = str_replace( $winc_chr, $outp_chr, $text);
    // even if the string seems to be UTF-8, we can't trust it, so convert it to UTF-8 anyway
    $text = mb_convert_encoding($text, 'UTF-8', $encoding);
    return $text;
}

function login($username, $password, $mysqli) {
    // Using prepared statements means that SQL injection is not possible. 
	// $user_role = USER_ROLE;
    if ($stmt = $mysqli->prepare("SELECT id, password_hash, salt, user_level, first_name, last_name, email, phone  
        FROM persons 
		WHERE username = ?
        LIMIT 1")) {
        $stmt->bind_param('s', $username);  // Bind parameters
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($user_id, $db_password, $salt, $user_level, $first_name, $last_name, $email, $phone);
        $stmt->fetch();
 
        // hash the password with the unique salt.
        $password = hash('sha512', $password . $salt);
        if ($stmt->num_rows == 1) { 
	        // Username found.
            // If the user exists we check if the account is locked
            // from too many login attempts 
			// $brute = checkbrute($user_id, $mysqli);
			$brute = 0;
	
            if ($brute > 5) {
				// Account is locked, user already notified
				// TODO: SET ERROR MESSAGE - LOGIN FAILED
				return false;
				
			} elseif ($brute == 5) {
				
				 // We record this attempt in the database
				$now = time();
				$mysqli->query("INSERT INTO login_attempts(user_id, time)
								VALUES ('$user_id', '$now')");
				$stmt->close();
			
                // Account is locked 
                // Send an email to user saying their account is locked
				
				// TODO: CHANGE TO CURRENT EMAIL PROCESS
				// $_SESSION['application'][no_reply_email_name],$_SESSION['application'][no_reply_email]
				$from = '"SupplementRelief" <support@supplementrelief.com>';
				$to = $email;
				$subject = 'Your Account is Locked';
				
				$rf = $user_role[0];
				
				$unlockcode = md5(uniqid(mt_rand(), true));
				
				$mysqli->query("UPDATE persons SET temp_password='$unlockcode' WHERE id='$user_id'");
                	$stmt->close();
				
				// TODO: CHANGE LOCATION OF THIS FILE (UPLOAD prau.php TO SERVER)
				// $unlocklink = 'http://wrightcheck.com/sozo/php/prau.php?si=' . $user_id . '&uc=' . $unlockcode;
				// ** change to dynamic path
				$unlocklink = 'https://supplementrelief.com/php/prau.php?si=' . $user_id . '&uc=' . $unlockcode;
				
				$body = 	"Because of 5 unsuccessful login attempts within 1 hour, your account has been locked for your protection.\n\n" . 
				"You will be able to login successfully in 1 hour (after " . date('g:i A T', strtotime('+1 hour')) . ").\n\n" . 
				"If you would like to have your account unlocked immediately, please click the following link:\n\n" . 
				$unlocklink;

				send_email_html($from, $to, $subject, $body, "");

                return false;
     
            } else {
								
                // Check if the password in the database matches
                // the password the user submitted.
                if ($db_password == $password) {
                    // Password is correct!
                    // Get the user-agent string of the user.
                    $user_browser = $_SERVER['HTTP_USER_AGENT'];
                    // XSS protection as we might print this value
                    $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                    $_SESSION['user']['user_id'] = $user_id;
                    // XSS protection as we might print this value
                    /* This code was removing the @ symbol from usernames that used email addresses and was causing problems. */
                    /*
                    $username = preg_replace("/[^a-zA-Z0-9_\-]+/", 
                                                                "", 
                                                                $username);
                    */                                
                    $_SESSION['user']['username'] = $username;
                    $_SESSION['user']['login_string'] = hash('sha512', $password . $user_browser);
                    // Where is $_SESSION['user']['login_string'] used? for what?          
					$_SESSION['user']['login_timestamp'] = date('m/d/y h:i A T');				
					$_SESSION['user']['first_name'] = $first_name;
					// $_SESSION['user']['full_name'] = $first_name . ' ' .$last_name;
					$_SESSION['user']['full_name'] = $first_name . ' ' .$last_name;
					// Decide where to get email and phone. simple: persons, advanced: person_contact_methods
					$_SESSION['user']['email'] = $email;
					$_SESSION['user']['phone'] = $phone;		
					$_SESSION['user']['access_level'] = $user_level;
													
					// Log System Event
					// echo 'About to Log System Login Event<br />';
									
					$event = 'LOGIN';
					$page = 'Login';
					$log_description = 'User LOGGED IN: '.$_SESSION['user']['username'].'.';
					$flag = 'NULL';	
					create_system_log($mysqli, $event, $page, $log_description, $flag);
                              
                    // Login successful.
                    $stmt->close();

                    return true;
                } else {
                    // Password not found.
                    // We record this attempt in the database.
                    $now = time();
                    $mysqli->query("INSERT INTO login_attempts(user_id, time)
                                    VALUES ('$user_id', '$now')");
                	$stmt->close();
			       $_SESSION['user']['login_status]'] = 'p';
			       return false; 	    
                }
            }
        } else {
            // Username not found. 
            $_SESSION['user']['login_status]'] = 'u';
            return false;
        }

    }
}
	
function checkbrute($user_id, $mysqli) {
    // Get timestamp of current time 
    $now = time();
 
    // All login attempts are counted from the past hour. 
    $valid_attempts = $now - (60 * 60);
 
    if ($stmt = $mysqli->prepare("SELECT time 
                             FROM login_attempts 
                             WHERE user_id = ? 
                            AND time > '$valid_attempts'")) {
        $stmt->bind_param('i', $user_id);
 
        // Execute the prepared query. 
        $stmt->execute();
        $stmt->store_result();
 
        // Return number of unsuccessful logins in past hour
        return $stmt->num_rows;
   
    }
}

// Purpose: Make sure the same person who logged in initially is the person logged in/requesting data
function login_check($mysqli) {
    // Check if all session variables are set 
    if (isset($_SESSION['user']['user_id'], 
                        $_SESSION['user']['username'], 
                        $_SESSION['user']['login_string'])) {
 
        $user_id = $_SESSION['user']['user_id'];
        $login_string = $_SESSION['user']['login_string'];
        $username = $_SESSION['user']['username'];
 
        // Get the user-agent string of the user.
        $user_browser = $_SERVER['HTTP_USER_AGENT'];
 
        if ($stmt = $mysqli->prepare("SELECT password_hash 
                                      FROM persons 
                                      WHERE id = ? LIMIT 1")) {
            // Bind "$user_id" to parameter. 
            $stmt->bind_param('i', $user_id);
            $stmt->execute();   // Execute the prepared query.
            $stmt->store_result();
 
            if ($stmt->num_rows == 1) {
                // If the user exists get variables from result.
                $stmt->bind_result($password);
                $stmt->fetch();
                $login_check = hash('sha512', $password . $user_browser);
 
                if ($login_check == $login_string) {
                    // Logged In!!!! 
                    return true;
                } else {
                    // Not logged in 
                    return false;
                }
            } else {
                // Not logged in 
                return false;
            }
        } else {
            // Not logged in 
            return false;
        }
    } else {
        // Not logged in 
        return false;
    }
}

function validate_login_credentials($username, $password, $mysqli) {

    if ($stmt = $mysqli->prepare("SELECT id, password_hash, salt, user_level, first_name, last_name, email, phone  
        FROM persons 
		WHERE username = ?
        LIMIT 1")) {
        $stmt->bind_param('s', $username);  // Bind parameters
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($user_id, $db_password, $salt, $user_level, $first_name, $last_name, $email, $phone);
        $stmt->fetch();
 
        // hash the password with the unique salt.
        $password = hash('sha512', $password . $salt);
        if ($stmt->num_rows == 1) {
	       if ($db_password == $password) { 
		       // valid Username and Password
		       return true;
	        } else {
	            // Password not found.
	            return false;
	        }
	    } else {
		   // Username not found
		   return false; 
	    }
	} else {
		echo 'Database query failed for validate_login_credentials';
		return false; 
	}
    
}

function esc_url($url) {
 
    if ('' == $url) {
        return $url;
    }
 
    $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);
 
    $strip = array('%0d', '%0a', '%0D', '%0A');
    $url = (string) $url;
 
    $count = 1;
    while ($count) {
        $url = str_replace($strip, '', $url, $count);
    }
 
    $url = str_replace(';//', '://', $url);
 
    $url = htmlentities($url);
 
    $url = str_replace('&amp;', '&#038;', $url);
    $url = str_replace("'", '&#039;', $url);
 
    if ($url[0] !== '/') {
        // We're only interested in relative links from $_SERVER['PHP_SELF']
        return '';
    } else {
        return $url;
    }
}


// Show any array i.e. $_SESSION, $_GET, $_POST
function show_array($array) {

	echo '
	<div class="row">
		<div class="small-12 columns">
			<br /><pre>';
			print_r($array);
	echo '</pre><br /></div></div>';
	
}

// Show MYSQLI Error Message
// ** change to dynamic path
function show_mysqli_error_message($query, $connection) {
	echo '
	<br />
	<div id="userMessage" class="row">		
		<div class="small-12 columns">				
			<div data-alert class="alert-box alert radius">
				<h2>Error Message:</h2>
				<h3>Query:</h3>
				<p>'.$query.'</p>
				<h3>System Response:</h3>';
				echo '<p>'.mysqli_error($connection).'</p>';
				echo '
				<h3>Next Step:</h3>
				<p>Please contact the System Administrator if you need help with this error message. Copy and paste the entire text on this page and email it to '.$_SESSION['application']['email'].'</p>				
				<a href="#" class="close">&times;</a>
		  </div>
		</div>
	</div>';
}

function no_null_date_check($value) {
	if(strlen($value) > 0) return date('m/d/y', strtotime($value));
	else return '';
}

function no_null_timestamp_check($value) {
	if(strlen($value) > 0) return date('m/d/y h:i A T', strtotime($value));
	else return '';
}	

// get_user_browser()
function get_user_browser() { 
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $ub = ''; 
    if(preg_match('/MSIE/i',$u_agent)) 
    { 
        $ub = "ie"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $ub = "firefox"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $ub = "safari"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $ub = "chrome"; 
    } 
    elseif(preg_match('/Flock/i',$u_agent)) 
    { 
        $ub = "flock"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $ub = "opera"; 
    } 
    
    return $ub; 
} 

// Create System Log
function create_system_log( $connection, $event = '', $page = '', $description = '', $flag = '' ) {
	
	switch ($page) {		
		case 'Blog Post':	    	
			if (!empty($_SESSION['blog_post']['blog_post_id'])) { $content_asset_id = $_SESSION['blog_post']['blog_post_id']; }
			break;
		case 'Education':	    	
			if (!empty($_SESSION['enrollment']['web_page_id'])) { $content_asset_id = $_SESSION['enrollment']['web_page_id']; }
			break;		  
		case 'Product':	    	
			if (!empty($_SESSION['brand_product_usage']['brand_product_usage_id'])) { $brand_product_usage_id = $_SESSION['brand_product_usage']['brand_product_usage_id']; }
			break;		  
		case 'Resource':	    	
			if (!empty($_SESSION['resource']['resource_id'])) { $content_asset_id = $_SESSION['resource']['resource_id']; }
			break;		  
		case 'Recipe':	    	
			if (!empty($_SESSION['recipe']['recipe_id'])) { $content_asset_id = $_SESSION['recipe']['recipe_id']; }
			break;		  
	}	
				
	if ($_SESSION['user']['access_level'] == 10) { $admin_user = 1;	}		
	
	$created_timestamp = date('Y-m-d H:i:s');
					    
    // Create System Log record.
	$queryInsertSystemLog = 
	'INSERT INTO system_logs (created, user_session_id, application_id, project_id, event_type_code, http_cookie, server_name, request_uri, http_referer, http_user_agent, query_string, remote_address, page_id, user_id, page, description, enrollment_id, project_program_id, content_asset_id, branded_product_id, admin_user, flag) VALUES (
	"'.$created_timestamp.'", 
	'.no_value_null_check($_SESSION['userStatistics']['session_id']).',
	'.no_value_null_check($_SESSION['application']['application_id']).',
	'.no_value_null_check($_SESSION['enrollment']['project_id']).',
	"'.$event.'",
	"'.$_SERVER['HTTP_COOKIE'].'",
	"'.$_SERVER['SERVER_NAME'].'",
	"'.$_SERVER['REQUEST_URI'].'",
	"'. (isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'').'",
	"'.$_SERVER['HTTP_USER_AGENT'].'",
	"'. (isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'').'",
	"'.$_SERVER['REMOTE_ADDR'].'",
	'.no_value_null_check($_SESSION['page']['page_id']).', 
	'.no_value_null_check($_SESSION['user']['user_id']).', 
	"'.$page.'",
	"'.$description.'",
	'.no_value_null_check($_SESSION['enrollment']['enrollment_id']).', 
	'.no_value_null_check($_SESSION['enrollment']['project_program_id']).', 
	'.no_value_null_check($content_asset_id).', 
	'.no_value_null_check($brand_product_usage_id).', 
	'.no_value_null_check($admin_user).', 	
	'.$flag.')';
	
	// echo $queryInsertSystemLog;	
	// die();

	$resultInsertSystemLog = mysqli_query($connection, $queryInsertSystemLog);
				
	if (!$resultInsertSystemLog) {
		show_mysqli_error_message($queryInsertSystemLog, $connection);
		die;
	}
	
	// $last_insert_system_log = mysqli_insert_id($connection); 		
	
	return $queryInsertSystemLog;
}
	
// Show SESSION Message and Type if present then clear the SESSION variables.
function show_session_message() {
	if (strlen($_SESSION['message']) > 0) { 
		echo '
		<!-- <br /> -->
		<div id="sessionMessage" class="row">		
			<div class="small-12 columns">				
				<div data-alert class="'.$_SESSION['message_type'].'">
					'.$_SESSION['message'].
					'<a href="#" class="close">&times;</a>
			  	</div>
			</div>
		</div>';
		$_SESSION['message_type'] = null;
		$_SESSION['message'] = null;		   
	}
}

// Show session variables
function show_session() {
	
	echo '
	<div class="row">
		<div class="small-12 columns">
			<br /><pre>';
			print_r($_SESSION);
	echo '</pre><br /></div></div>';
	
}
	
	
// Show Lorem Ipsum text. $count is the number of paragraphs to show.
function lorem_ipsum( $count = '' ) {

	$p1 = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel sodales enim. Cras id augue ac enim elementum semper quis sed turpis. Aliquam erat volutpat. In sed erat quis dolor feugiat placerat. Sed quis tincidunt purus. Morbi tempor felis vel consequat fringilla. Praesent eu bibendum turpis. Duis vel fermentum nibh, sit amet sodales augue. Pellentesque dictum ac diam vitae vehicula. Sed efficitur sollicitudin sollicitudin. Cras at risus varius, convallis velit eu, auctor velit. Fusce luctus mollis placerat. Quisque blandit lacus vestibulum orci fermentum, eu vulputate tellus efficitur.</p>';
	$p2 = '<p>Sed lacinia ex neque, sit amet hendrerit felis tempor ut. Quisque sodales mi ultrices vehicula consectetur. Aliquam viverra cursus lacus, eu ultrices turpis imperdiet ac. Nullam nunc orci, mollis ac bibendum eu, tincidunt in justo. Donec eget dignissim urna. Morbi eu magna ornare, consequat sem et, faucibus metus. Nullam a ligula dignissim, fringilla nunc et, mollis nibh. Aliquam erat volutpat. Mauris ornare cursus enim sed venenatis. Pellentesque cursus elementum tellus sodales suscipit. Cras blandit sem elit, et convallis orci eleifend a. Etiam libero augue, mattis nec tellus id, luctus vulputate metus. Nulla a dolor vel justo vestibulum laoreet vitae at sapien. Morbi sagittis lacus quis eros vestibulum, et congue quam venenatis.</p>';
	$p3 = '<p>Quisque nec ante libero. Vivamus ut elit a nunc condimentum blandit. Donec nec nulla finibus, efficitur arcu eu, vehicula erat. Sed sed neque sodales, dignissim ipsum vel, lacinia nisi. Vestibulum tempus mattis quam ac commodo. Etiam in luctus leo. Pellentesque tempus auctor urna, ut porta felis tristique eget. Aliquam suscipit dapibus laoreet. Aliquam eros neque, euismod id tincidunt ac, lobortis et nibh. Nulla vel nunc lacinia, ullamcorper velit ac, gravida arcu. Aliquam risus arcu, iaculis a viverra in, sagittis ut mi. Pellentesque a tellus neque. Sed in consequat massa. Etiam consectetur nunc dolor, in molestie dui rhoncus a.</p>';
	$p4 = '<p>Curabitur lectus sem, congue quis urna quis, convallis consectetur massa. Phasellus tristique efficitur venenatis. Maecenas fringilla lorem sed risus molestie, aliquet aliquam lacus consectetur. Nulla ullamcorper, dolor lobortis consectetur blandit, mi turpis ultrices nibh, vel scelerisque purus massa sit amet ante. Nulla interdum nibh vitae consequat pulvinar. Praesent a lacus eget nisl rutrum blandit. In molestie nulla purus, et accumsan dui commodo eget. Etiam in tempor magna, eget sodales tellus. Donec accumsan vel neque et suscipit. Morbi porta viverra commodo. Nunc in sodales quam. Vestibulum rhoncus nulla a lacus consectetur, et elementum velit consequat.</p>';
	$p5 = '<p>Phasellus congue leo nec augue consectetur pharetra. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis eleifend convallis est, non aliquam risus iaculis vitae. Aliquam fringilla lacus et laoreet rutrum. Sed nec lacus ultricies, sollicitudin neque ut, viverra augue. Mauris a facilisis purus, non aliquet velit. Nullam dapibus ultrices dolor, at ullamcorper sem sollicitudin quis. Praesent convallis lobortis eros sed scelerisque.</p>';
	
	$lorem_ipsum = '
	<div class="row">
		<div class="small-12 columns">';
			
	switch ($count) {
	
		case 1:
			$lorem_ipsum .= $p1;
			break;
			
		case 2:
			$lorem_ipsum .= $p1.$p2;
			break;
			
		case 3:
			$lorem_ipsum .= $p1.$p2.$p3;
			break;
			
		case 4:
			$lorem_ipsum .= $p1.$p2.$p3.$p4;
			break;
			
		case 5:
			$lorem_ipsum .= $p1.$p2.$p3.$p4.$p5;
			break;
		
		$lorem_ipsum .= $p1;
		
	}
	
	$lorem_ipsum .= '					
		</div>
	</div>';
	
	echo $lorem_ipsum;
	
    // return $lorem_ipsum; 
} 

/**
 * function creates a thumbnail image in the same directory with the prefix 'tn'
 * thumb should fit to the defined box (second parameter)
 * only bigger images are processed, while smaller images are just copied
 * function will resize PNG and GIF images, without losing their transparency
 *
 * @param string  $image1_path - full path to the image
 * @param integer $box         - box dimension
 */
 
function create_thumb($image1_path, $box=200){
    // get image size and type
    list($width1, $height1, $image1_type) = getimagesize($image1_path);
 
    // prepare thumb name in the same directory with prefix 'tn'
    $image2_path = dirname($image1_path) . '/tn_' .basename($image1_path);
 
    // make image smaller if doesn't fit to the box
    if ($width1 > $box || $height1 > $box){
        // set the largest dimension
        $width2 = $height2 = $box;
        // calculate smaller thumb dimension (proportional)
        if ($width1 < $height1) $width2  = round(($box / $height1) * $width1);
        else                    $height2 = round(($box / $width1) * $height1);
 
        // set image type, blending and set functions for gif, jpeg and png
        switch($image1_type){
            case IMAGETYPE_PNG:  $img = 'png';  $blending = false; break;
            case IMAGETYPE_GIF:  $img = 'gif';  $blending = true;  break;
            case IMAGETYPE_JPEG: $img = 'jpeg'; break;
        }
        $imagecreate = "imagecreatefrom$img";
        $imagesave   = "image$img";
 
        // initialize image from the file
        $image1 = $imagecreate($image1_path);
 
        // create a new true color image with dimensions $width2 and $height2
        $image2 = imagecreatetruecolor($width2, $height2);
 
        // preserve transparency for PNG and GIF images
        if ($img == 'png' || $img == 'gif'){
          // allocate a color for thumbnail
            $background = imagecolorallocate($image2, 0, 0, 0);
            // define a color as transparent
            imagecolortransparent($image2, $background);
            // set the blending mode for thumbnail
            imagealphablending($image2, $blending);
            // set the flag to save alpha channel
            imagesavealpha($image2, true);
        }
 
        // save thumbnail image to the file
        imagecopyresampled($image2, $image1, 0, 0, 0, 0, $width2, $height2, $width1, $height1);
        $imagesave($image2, $image2_path);
    }
    // else just copy the image
    else copy($image1_path, $image2_path);
}

	/**
		* Method Name : safe_data
		* Desc : Used to secure input data
		* @access public
		* @param String $param input data
		* @return string
	**/
	
	function safe_data($param)
	{
			return stripslashes(htmlspecialchars(trim($param)));
			
	}
	
	/**
		* Method Name : safe_sql_data
		* Desc : Used to escape input data for preventing sql injection
		* @access public
		* @param Object $connection Mysqli connection object
		* @param String $param input data
		* @return string
	**/
	
	function safe_sql_data($connection, $param)
	{
			return mysqli_real_escape_string($connection, stripslashes(htmlspecialchars(trim($param))));
			// Jay. What does mysqli_real_escape_string do? Should I use safe_sql_data on every user input value that can go into the database?
			
	}
		
	function no_value_null_check($value) {
		
		if(strlen($value) > 0) return '"'.$value.'"';
		else return 'NULL';
	
	}
	
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

				
	/*
	function ValidDate($string = '') {
	    $dt = $string;
	    $arr = explode("-", $dt);
	    
	    //print_r($arr);
	
	    If(@checkdate($arr[0], $arr[1], $arr[2])){
	        return true;
	    }else{
	        return false;
	    }
	}	
	*/		
	
	function valid_date($string = '') {
	    $dt = $string;
	    $arr = explode("/", $dt);
	    
	    //print_r($arr);
	
	    If(@checkdate($arr[0], $arr[1], $arr[2])){
	        if(strlen($arr[2]) == 4) return '1';
		else return '0';
	    }else{
	        return '0';
	    }
	}

	// Converts user formatted date input MM/DD/YYYY into YYYY-MM-DD into for SQL database date format.	
	function sql_date($date) {
		if(strlen($date) > 0) {
			$date_array = explode('/', $date);
			$sql_date = $date_array[2].'-'.$date_array[0].'-'.$date_array[1];
		}
		return @$sql_date;
	}
	
	// Converts database YYYY-MM-DD into MM/DD/YYYY for common user format.
	// used when getting a date value from the database and displaying on the page or within a user form field.
	function hdate($date) {
		if(strlen($date) > 0) {
			$date_array = explode('-', $date);
			$hdate = $date_array[1].'/'.$date_array[2].'/'.$date_array[0];
			// $hdate = $date_array[1].'/'.$date_array[2].'/'.substr($date_array[0], 2, 3);
		}
		return @$hdate;
	}	
		
	function convert_smart_quotes($string)
	{
		$search = array(
	//		'O',
	//		'f',
			'“',
			'”',
			'’',
			'’',
			'’',
			'…',
			'-',
			'–',
			'“',
			'”',
			'+',
			'/',
			'%');
	
		$replace = array(
	//		'Z',
	//		'*',
			'"',
			'"',
			"'",
			"'",
			"'",
			"...",
			'-',
			'_',
			'"',
			'"',
			'+',
			"/",
			"%");
	
		return str_replace($search, $replace, $string);
	}
			
	function check_email_address($email) {
		// First, we check that there's one @ symbol, and that the lengths are right
		if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
			// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			return false;
		}
		// Split it into sections to make life easier
		$email_array = explode("@", $email);
		$local_array = explode(".", $email_array[0]);
		for ($i = 0; $i < sizeof($local_array); $i++) {
			if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
				return false;
			}
		}    
		if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
			$domain_array = explode(".", $email_array[1]);
			if (sizeof($domain_array) < 2) {
				return false; // Not enough parts to domain
			}
			for ($i = 0; $i < sizeof($domain_array); $i++) {
				if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
					return false;
				}
			}
		}
		return true;
	}
	
	function doubleSalt($toHash,$val2) { 
		$str = str_split($toHash,(strlen($toHash)/2)+1); 
		$hash = hash('SHA512', $val2.$str[0].'!56RtS72*'.$str[1]); 
		return $hash; 
	}
	
	function double_salt($toHash,$val2) { 
		$str = str_split($toHash,(strlen($toHash)/2)+1); 
		$hash = hash('SHA512', $val2.$str[0].'!56RtS72*'.$str[1]); 
		return $hash; 
	}
	
	function send_email_html($from, $to, $subject, $message, $BCC) {
	
	// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	
	// Additional headers
		// $headers .= "To: $to \r\n";
		$headers .= "From: " .$from. "\r\n";
		$headers .= "Bcc: " .$BCC. "\r\n";
		// echo $headers;
		// die;
		mail($to, $subject, $message, $headers);
	}
	
	function generate_forum_post_email_notification($connection, $post_type, $post_id)
	{
    	
        if ($post_type == 'thread') {
            
        // Query the Discussion Thread
        $query = 'SELECT 
    	   	dt.title AS thread_title, 
    	   	dt.text AS thread_text, 
    	   	dt.file_image_url AS thread_image_url, 
            pj.name AS project_name, 
    	   	pp.name AS project_program_name, 
    	   	pe.first_name, 
    	   	pe.last_name, 
    	   	bpu.name AS brand_product_usage_name, 
    	   	cabp.title AS blog_post_title, 
    	    caedc.title AS education_course_title, 
    	    caedw.title AS education_web_page_title, 
    	    carp.title AS recipe_title,    
    	   	care.title AS resource_title     
    	   	FROM discussion_threads dt 
    	   	LEFT JOIN projects pj ON dt.project_id = pj.id 
    	   	LEFT JOIN project_programs pp ON dt.project_program_id = pp.id 
    	   	LEFT JOIN persons pe ON dt.created_by = pe.id 
    	   	LEFT JOIN brand_product_usages bpu ON dt.brand_product_usage_id = bpu.id 
    	   	LEFT JOIN content_assets cabp ON dt.blog_post_id = cabp.id 
    	   	LEFT JOIN content_assets caedc ON dt.course_id = caedc.id 
    	    LEFT JOIN content_assets caedw ON dt.web_page_id = caedw.id 
    	    LEFT JOIN content_assets carp ON dt.recipe_id = carp.id 
    	   	LEFT JOIN content_assets care ON dt.resource_id = care.id 
    	   	WHERE dt.id = '.$post_id.'';    
            
        } elseif ($post_type == 'reply') { 
                        
            // Query the Reply
            $query = 'SELECT 
            dr.text AS reply_text, 
            dr.file_image_url AS reply_image_url, 
    	   	dt.title as thread_title, 
    	    dt.file_image_url AS thread_image_url, 
    	   	dt.text AS thread_text, 
            pj.name AS project_name, 
    	   	pp.name AS project_program_name, 
    	   	pe.first_name, 
    	   	pe.last_name, 
    	   	bpu.name AS brand_product_usage_name, 
    	   	cabp.title AS blog_post_title, 
    	    caedc.title AS education_course_title, 
    	    caedw.title AS education_web_page_title, 
    	    carp.title AS recipe_title,    
    	   	care.title AS resource_title      
    	   	FROM discussion_replies dr 
    	   	JOIN discussion_threads dt ON dr.discussion_thread_id = dt.id 
            LEFT JOIN projects pj ON dt.project_id = pj.id 
    	   	LEFT JOIN project_programs pp ON dt.project_program_id = pp.id 
    	   	LEFT JOIN persons pe ON dt.created_by = pe.id 
    	   	LEFT JOIN brand_product_usages bpu ON dt.brand_product_usage_id = bpu.id 
    	   	LEFT JOIN content_assets cabp ON dt.blog_post_id = cabp.id 
    	    LEFT JOIN content_assets caedc ON dt.course_id = caedc.id 
    	    LEFT JOIN content_assets caedw ON dt.web_page_id = caedw.id 
    	    LEFT JOIN content_assets carp ON dt.recipe_id = carp.id 
    	   	LEFT JOIN content_assets care ON dt.resource_id = care.id 
    	   	WHERE dr.id = '.$post_id.'';      
            
        }
	  
	   // echo $query;
	   // die();
	   
	   $result = mysqli_query($connection, $query);
	   
	   if (!$result) {
	   	show_mysqli_error_message($query, $connection);
	   	die;
	   }
	      
	   $r = mysqli_fetch_assoc($result);
	   
	   if (strlen($r['project_name']) > 0) {
			$message .= 'Project: '.$r['project_name'].'<br>'; 		
	   }
	   	   	      
	   if (strlen($r['project_program_name']) > 0) {
			$message .= 'Program: '.$r['project_program_name'].'<br>';			
	   }
	   
	   if (strlen($r['first_name']) > 0) {
			$message .= 'Posted by: '.$r['first_name'].' '.$r['last_name'].'<br>';			
	   }
	   
	   if (strlen($r['blog_post_title']) > 0) {
			$message .= 'Blog Post: '.$r['blog_post_title'].'<br>';			
	   }
	   
	   if (strlen($r['brand_product_usage_name']) > 0) {
			$message .= 'Product: '.$r['brand_product_usage_name'].'<br>';			
	   }
	   
	   if (strlen($r['education_course_title']) > 0) {
			$message .= 'Education Course: '.$r['education_course_title'].'<br>';			
	   }
	   
	   if (strlen($r['education_web_page_title']) > 0) {
			$message .= 'Education Topic: '.$r['education_web_page_title'].'<br>';			
	   }

	   if (strlen($r['recipe_title']) > 0) { 
			$message .= 'Recipe: '.$r['recipe_title'].'<br><br>';			
	   }
	   
	   if (strlen($r['resource_title']) > 0) { 
			$message .= 'Resource: '.$r['resource_title'].'<br><br>';			
	   } 
	   
	   if (strlen($r['thread_title']) > 0) { 
			$message .= 'Post Title: '.$r['thread_title'].'<br>';			
	   } 

	   if ($post_type == 'thread') {
    	   
            $message .= '<hr>Post:<br>'.$r['thread_text']; 
            
            if (strlen($r['thread_image_url']) > 0) { 
    			$message .= '<br><br><img src="'.$r['thread_image_url'].'">';			
    	    } 
    	   
	   } elseif ($post_type == 'reply') {
    	   
    	 $message .= '<hr><br>Post:<br>'.$r['thread_text'].'<br><br>Reply:<br>'.$r['reply_text'];  
    	   
	   }
 	   	   
	   mysqli_free_result($result);
	   
	   return($message);
	   	        
	}
	
	function rand_passwd( $length = 8, $chars = 'abcdefghjkmnpqrstuvwxyABCDEFGHJKLMNPQRSTUVWXY3456789' ) {
    	return substr( str_shuffle( $chars ), 0, $length );
    }
        
    
    function clearEnrollmentSession() {   
    
	    unset($_SESSION['enrollment_username']); 
	    unset($_SESSION['enrollment_password']); 
	    unset($_SESSION['enrollment_ngs_benefits_number']);	    
	    unset($_SESSION['enrollment_email']);
	 	unset($_SESSION['enrollment_person_id']); 
	    unset($_SESSION['enrollment_display_name']); 
	    unset($_SESSION['enrollment_project_program_id']); 
	    unset($_SESSION['person_role_display']); 
	    unset($_SESSION['enrollment_ouf_id']); 
	    unset($_SESSION['role_eligible_program']); 
	    unset($_SESSION['enrollment_person_role_id']);  
	    unset($_SESSION['enrollment_project_program_name_selected']);
	    
	    unset($_SESSION['enrollment_first_name']);
	    unset($_SESSION['enrollment_last_name']);
	    unset($_SESSION['enrollment_address']);
	    unset($_SESSION['enrollment_city']);
	    unset($_SESSION['enrollment_state']);
	    unset($_SESSION['enrollment_postal_code']);
	    unset($_SESSION['enrollment_phone']);
	    unset($_SESSION['enrollment_birth_date']);
	    unset($_SESSION['enrollment_gender']);
	        
    }
    
	// Calculate Order Total for Retail and Cost
	function calculate_order_total($order_id, $project_id, $connection) {
    	
    	if ($project_id) {
        	   	
            // Query Projects for Cost Discount amount.
    		$queryProjects = '
    			SELECT 
    			numedica_cost_discount   
     			FROM projects    
    			WHERE id = "'.$project_id.'"';
    		
    		// echo $queryProjects;
    		// die();
			
    		$result_project = mysqli_query($connection, $queryProjects);
    		
    		if (!$result_project) {
    			show_mysqli_error_message($queryProjects, $connection);
    			die;
    		}
    			
    		while($r = mysqli_fetch_assoc($result_project)) {
    		
    			// show_array($r);
    			if ($r['numedica_cost_discount'] > 0) {
         			$cost_discount_percentage = ((100 - $r['numedica_cost_discount']) / 100);
    			} else {
        			$cost_discount_percentage = 1;
    			}
    			
            } 
            
            mysqli_free_result($result_project);
        	
    	} else {
        	$cost_discount_percentage = 1;
    	}
    	
		// Query Order Items
		$queryOrderItems = '
			SELECT 
			id AS order_item_id,  
			quantity 
			FROM order_items   
			WHERE orders_id = "'.$order_id.'"';
		
		// echo $queryOrderItems;
		// die();
		
			
		$result_order_item = mysqli_query($connection, $queryOrderItems);
		
		if (!$result_order_item) {
			show_mysqli_error_message($queryOrderItems, $connection);
			die;
		}
			
		while($r = mysqli_fetch_assoc($result_order_item)) {
		
			// show_array($r);
			
			$order_item_id = $r['order_item_id'];
			
			$queryOrderItemOptions = '
			SELECT 
			bpu.retail_price, 
			bpu.cost  	  
			FROM order_item_options oio
    			LEFT JOIN brand_product_usage_options bpuo ON 
    				(oio.brand_product_usage_id = bpuo.brand_product_usage_id AND oio.item_option_id = bpuo.id) 
    			LEFT JOIN brand_product_usages bpu ON bpuo.variant_brand_product_usage_id = bpu.id     
			WHERE oio.order_item_id = "'.$order_item_id.'" 
    			AND (bpuo.effective_date <= CURDATE() AND (bpuo.end_date is NULL or bpuo.end_date >= CURDATE() )) 
    			AND bpuo.variant_brand_product_usage_id is NOT NULL '; 
			/* GROUP BY oio.brand_product_usage_id'; */
		
			// echo $queryOrderItemOptions;
			// die();
				
			// Execute the query
			$result_order_item_option = mysqli_query($connection, $queryOrderItemOptions);
			
			$total_options = mysqli_num_rows($result_order_item_option);
			$count = 0;
	
			// echo 'Total Options with Cost: '.$total_options.'<br>';
					
			while($o = mysqli_fetch_assoc($result_order_item_option)) { 
    			
    			// show_array($o);
				
				$count++;
				
				if ($count == $total_options) {
					
					// show_array($o);
				
					// echo $o['brand_product_name'].' '.$o['order_option_type_code'].' '.$o['order_option_name'].'<br>';
					// echo 'Quantity: '.$r['quantity']. ' Cost: '.$o['cost']. ' = Total Cost: '.$total_product_cost.'<br>';
					
					$line_item_retail_amount = $r['quantity'] * $o['retail_price'];
					$total_order_retail_amount = $total_order_retail_amount + $line_item_retail_amount;	
					
					$line_item_cost_amount = $r['quantity'] * $o['cost'] * $cost_discount_percentage;
					$total_order_cost_amount = $total_order_cost_amount + $line_item_cost_amount;				
					
				}			
		
			}
			
			mysqli_free_result($result_order_item_option);
				
		}
		
		mysqli_free_result($result_order_item);
		
		$total_retail_amount = number_format((float)$total_order_retail_amount, 2, '.', '');
		$total_cost_amount = number_format((float)$total_order_cost_amount, 2, '.', ''); 
		
		// echo 'Retail Amount: '.$total_retail_amount.'<br>';
		// echo 'Cost Amount: '.$total_cost_amount.'<br>';
				
		// return $total_order_product_cost;
		// return number_format((float)$total_order_product_cost, 2, '.', ''); 
		return array($total_retail_amount, $total_cost_amount); 
	
	}
	
	function getProductImage($root_media_host_url,$feature_image_host_url,$feature_image_alt_text)
	{
		$feature_image 	= 	"<a href='javascript:void(0)' data-reveal-id='productImages' rel='self' 
								title='View Additional Product Images' >
								<img src=".$root_media_host_url.$feature_image_host_url." alt=".$feature_image_alt_text." />".
							"</a>";
		return $feature_image;
	}

	function getProductIngredients($resultQueryOptionSpecifications)
	{
		//$resultQueryOptionSpecifications 	= 	mysqli_query($connection, $query);
		$flag = false;
		while($specification = mysqli_fetch_assoc($resultQueryOptionSpecifications)) 
		{			
			if(!$flag)
			{
				$flag = true;
				$ingredients_header = '';
				$ingredients_table =
				'<table style="width:100%;">
					<thead>
						<tr>
						  <th align="left">Ingredient</th> 
						  <th align="left">Amount</th> 
						</tr>
					</thead>';		
				$ingredients_footer = '';					
			}	
			switch ($specification['specification_type_code']) 
			{
			
				case 'INGRH':
					
				    $ingredients_header .= '<div id="ingredientsHeader">'.$specification['value'].'</div>'; 
				    break;
				
				case 'INGR':

			        $ingredients_table .= '
			        <tr>
			        	<td>'
			        		.$specification['name'].'
			        	</td>
			        	<td>'
			        		.$specification['value'].'
			        	</td>
			        </tr>';        

			        break;
			        
			    case 'INGRF':
			    	
			        $ingredients_footer .= '<div id="ingredientsFooter">'.$specification['value'].'</div>';  
			            	                	        
			        break;    
			        
			} 
			
		}
		$ingredients_table .= '</table>';
		$ingredients = '<b>'.cleanEncoding($ingredients_header).'</b>'.
						'<div id="product-ingredients" >'.
							cleanEncoding($ingredients_table).
						'</div>'.
						cleanEncoding($ingredients_footer);
		return $ingredients;
	}	
	
	//Query for getting product's option
	function get_query_option_product($connection, $option_id, $bpu_id)
	{
		$queryOptionProduct =
			'SELECT
			bpu.name,
			bpu.sku,
			bpu.upc,
			bpu.description_html,
			bpu.benefits_html,
			bpu.directions_html,
			bpu.retail_price,
			prd.manufacturer,
			im.host_url AS feature_image_host_url,
			im.caption AS feature_image_caption,
			im.alt_text AS feature_image_alt_text

			FROM brand_product_usage_options bpuo

			LEFT JOIN brand_product_usages bpu ON bpuo.variant_brand_product_usage_id = bpu.id
			LEFT JOIN products prd ON bpu.product_id = prd.id
			LEFT JOIN images im
			ON (bpu.image_id = im.content_asset_id AND
			im.format = "JPG" AND
			im.size = "Product" AND
			im.usage_size = "Medium")

			WHERE bpuo.id = '.$option_id.
			' AND bpuo.brand_product_usage_id = '.$bpu_id.
			' AND variant_brand_product_usage_id IS NOT NULL
			LIMIT 1';

		$resultQueryOptionProduct 	= 	mysqli_query($connection, $queryOptionProduct);
		return $resultQueryOptionProduct;
	}

	//Query for getting product's option specifications
	function get_query_option_specifications($connection, $option_id, $bpu_id)
	{
		$queryOptionSpecifications = 
			'SELECT 
			bps.id, 
			bps.specification_type_code,  
			bps.name, 
			bps.value 
			FROM brand_product_specifications bps 
			LEFT JOIN brand_product_usages bpu
			ON bps.brand_product_usage_id = bpu.id
			LEFT JOIN brand_product_usage_options bpuo
			ON bpuo.variant_brand_product_usage_id = bpu.id
			WHERE bpuo.id = '.$option_id.
			' AND bpuo.brand_product_usage_id = '.$bpu_id.
			' AND bps.effective_date <= CURRENT_DATE 
			AND (bps.end_date is NULL or bps.end_date >= CURRENT_DATE) 
			ORDER BY bps.specification_type_code ASC, bps.seq ASC';
		
		$resultQueryOptionSpecifications 	= 	mysqli_query($connection, $queryOptionSpecifications);
		return $resultQueryOptionSpecifications;

	}
	//Query for getting product's option price
	function get_product_option_price($connection, $option_id, $bpu_id)
	{
		$queryPrice =
			'SELECT 
            getPrice(bpuo.brand_product_usage_id, 1, bpuo.id, '.$_SESSION['enrollment']['project_id'].',"'.USER_PRICE_TYPE.'") as price
			FROM brand_product_usage_options bpuo
			WHERE bpuo.id = '. $option_id .' AND bpuo.brand_product_usage_id = '. $bpu_id .' LIMIT 1';
		$resultPrice = mysqli_query($connection, $queryPrice);
		$price = mysqli_fetch_assoc($resultPrice);
		return $price['price'];
	}
    	  
  	function checkForBookmark($connection, $currentUrl)
  	{
  		$total_records = 0;

  		$queryExistingBookmark = 
		'SELECT	id, title, comments 
		FROM bookmarks 
		WHERE enrollment_id = '.$_SESSION['enrollment']['enrollment_id'].' 
		AND url = "'.$currentUrl.'"';

		$result_existing_bookmark = mysqli_query($connection, $queryExistingBookmark);

		if (!$result_existing_bookmark) {
			show_mysqli_error_message($queryExistingBookmark, $connection);
			die;
		}

		$total_records = mysqli_num_rows($result_existing_bookmark);	
		
		$arr = [];
		$arr[0] = $total_records;
		if($total_records)
		{	
			$result = mysqli_fetch_assoc($result_existing_bookmark);
			$arr[1] = $result['id'];
			$arr[2] = $result['title'];
			$arr[3] = $result['comments'];
		}

		return $arr;
  	} 	  	

  	function checkForAddress($connection, $user_id)
  	{
  		$queryForAddress 	=	'SELECT address_type_code FROM locations WHERE person_id = "'.$user_id.'" ORDER BY address_type_code';

  		$fetchQuery 		=	mysqli_query($connection, $queryForAddress);
  		$noOfAddress 		=	mysqli_num_rows($fetchQuery);
  		
  		if($noOfAddress == 1)
  		{
  			$result 		=	mysqli_fetch_assoc($fetchQuery);
  			$addressType 	=	$result['address_type_code'] == 'SHIP' ? 'BILL' : 'SHIP';
  			// Check for shipping address. If not found add it.
  			// Check for billing address. If not found add it.
  			addAddress($connection, $addressType);
  		}
  		else if(!$noOfAddress)
		{  	//Add shipping address
			addAddress($connection, 'SHIP');
  			//Add billing address
  			addAddress($connection, 'BILL');
		}
  	}

  	function addAddress($connection, $addressType)
  	{
  		$address 			=	'';
  		$city 				=	'';
  		$state_id			=	'';
  		$postal_code 		=	'';
  		$country_id 		=	'';
  		$created_by 		=	$_SESSION['user']['user_id'];

		// Set the Timestamp for the insert record audit trail.
		// date_default_timezone_set("America/Detroit");
		$created_timestamp = date('Y/m/d H:i:s');
	
  		switch ($addressType) 
  		{
  			case 'SHIP':
				$address 			=	$_SESSION['checkout']['shipping_address'];
		  		$city 				=	$_SESSION['checkout']['shipping_city'];
		  		$state_id			=	getStateId($connection, $_SESSION['checkout']['shipping_state']);
		  		$postal_code 		=	$_SESSION['checkout']['shipping_zip'];
		  		$country_id 		=	getCountryId($connection, $_SESSION['checkout']['shipping_country']);
  				break;
  			
  			case 'BILL':
  				$address 			=	$_SESSION['checkout']['address'];
		  		$city 				=	$_SESSION['checkout']['city'];
		  		$state_id			=	getStateId($connection, $_SESSION['checkout']['state']);
		  		$postal_code 		=	$_SESSION['checkout']['zip'];
		  		$country_id 		=	getCountryId($connection, $_SESSION['checkout']['country']);
  				break;
  		}  		

  		$queryInsertAddress = 'INSERT INTO addresses (address, city, state_id, postal_code, country_id, created_by, created_timestamp) VALUES (
  									"'.$address.'",
									"'.$city.'",
									"'.$state_id.'",
									"'.$postal_code.'", 
									"'.$country_id.'", 
									"'.$created_by.'",
									"'.$created_timestamp.'")';
		  		
		$resultInsertAddress = mysqli_query($connection, $queryInsertAddress);  
				
		if (!$resultInsertAddress) {
			show_mysqli_error_message($queryInsertAddress, $connection);
			die;
		}	

		$address_id = mysqli_insert_id($connection);

		$effective_date = date('m/d/y');
		$queryInsertLocation = '
			INSERT INTO locations (address_type_code, address_id, person_id, effective_date, created_by, created_timestamp) 
			VALUES (
			"'.$addressType.'",
			"'.$address_id.'",
			"'.$created_by.'",
			'.no_value_null_check(sql_date($effective_date)).', 
			"'.$created_by.'",		
			"'.$created_timestamp.'")';
			
		$resultInsertLocation = mysqli_query($connection, $queryInsertLocation);
		// echo $queryInsertLocation.'<br /><hr />';   
						
		if (!$resultInsertLocation) {
			show_mysqli_error_message($queryInsertLocation, $connection);
			die;
		}
		
		mysqli_free_result($resultInsertLocation);
		
 	}

  	function getStateId($connection, $state_abbr)
  	{
  		$queryGetId 	= 	'SELECT state_id from states WHERE state_abbr = "'.$state_abbr.'" LIMIT 1';
  		$fetchQuery 	=	mysqli_query($connection, $queryGetId);
  		$result 		=	mysqli_fetch_assoc($fetchQuery);
  		return $result['state_id'];
  	}

  	function getCountryId($connection, $country_abbr)
  	{
  		$queryGetId 	= 	'SELECT country_id from country WHERE country_abbr = "'.$country_abbr.'" LIMIT 1';
  		$fetchQuery 	=	mysqli_query($connection, $queryGetId);
  		$result 		=	mysqli_fetch_assoc($fetchQuery);
  		return $result['country_id'];
  	}

  	function checkForPhone($connection, $user_id)
  	{
  		$queryForPhone  	=	'SELECT phone FROM persons WHERE id = '.$user_id ;
  		$fetchQuery 		=	mysqli_query($connection, $queryForPhone);
  		$resultQuery 		=	mysqli_fetch_assoc($fetchQuery);
  		if($resultQuery['phone'] == null)
  		{
	  		
  			$phone = sanitizePhone($_SESSION['checkout']['phone'], $_SESSION['checkout']['country']);
	  		
  			$queryUpdatePhone 	=	'Update persons set phone = "'.$phone.'"';
   			$fetchQueryPhone 	=	mysqli_query($connection, $queryUpdatePhone);
  			if (!$fetchQueryPhone) {
				show_mysqli_error_message($queryUpdatePhone, $connection);
				die;
			}
			
			mysqli_free_result($fetchQueryPhone);
  		}
  	}

  	function productsPagination($json, $page, $last_page, $last_prev, $limit, $alpha_href, $keyword_href, $orderby_href, $new_item_href, $featured_item_href, $program_item_href, $pack_item_href, $sample_item_href, $education_item_href, $vegetable_capsule_href, $gluten_free_href, $vegetarian_href, $albion_minerals_href, $gos_id_href, $ingr_id_href, $json = '')
  	{
		if ($last_page > 6)
		{
			if ($page == 1)
			{
				$next  = $page + 1;
				$next1 = $page + 2;
				$next2 = $page + 3;
				$url = 
				"<ul class='pagination'>"."<li class='arrow unavailable'><a href=''>&laquo;</a></li>".
					"<li class='current'><a data-form='products' class='paginationAnchor' data-url='"."page=".$page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$page."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$next1."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next1."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$next2."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next2."</a></li>".
					"<li class='unavailable'><a href=''>&hellip;</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$last_prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_prev."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_page."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&raquo;"."</a></li>".
				"</ul>";
			}

			else if ($page == $last_page)
			{
				$first = 1;
				$first1 = $first + 1;
				$first2 = $first + 2;
				$first3 = $first + 3;
				$url= 
				"<ul class='pagination'>".
					"<li class='arrow'><a data-form='products' class='paginationAnchor' data-url='"."page=".$last_prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&laquo;</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$first."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$first1."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first1."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$first2."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first2."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$first3."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first3."</a></li>".
					"<li class='unavailable'><a href=''>&hellip;</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$last_prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_prev."</a></li>".
					"<li class='current'><a data-form='products' class='paginationAnchor' data-url='"."page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_page."</a></li>".
					"<li class='arrow unavailable'><a href='' >"."&raquo;"."</a></li>".
				"</ul>";
			}
			
			else if ($page == 2)
			{
				$next = $page + 1;
				$prev = $page - 1;
				$next1 = $page + 2;
				$url = 
				"<ul class='pagination'>".
					"<li class='arrow'><a data-form='products' class='paginationAnchor' data-url='"."page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&laquo;</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$prev."</a></li>".
					"<li class='current'><a data-form='products' class='paginationAnchor' data-url='"."page=".$page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$page."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$next1."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next1."</a></li>".
					"<li><a  href=''>&hellip;</a></li>"."<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$last_prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_prev."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_page."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&raquo;"."</a></li>".
				"</ul>";
				
			}
			
			elseif ($page > 2 && $page < ($last_prev - 1))
			{
				$next = $page + 1;
				$prev = $page - 1;
				$first = 1;
				$url=
				"<ul class='pagination'>".
					"<li class='arrow'><a data-form='products' class='paginationAnchor' data-url='"."page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&laquo;</a></l>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$first."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first."</a></li>".
					"<li class='unavailable'><a href=''>&hellip;</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$prev."</a></li>".
					"<li class='current'><a data-form='products' class='paginationAnchor' data-url='"."?page=".$page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$page."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next."</a></li>".
					"<li><a href=''>&hellip;</a></li>"."<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$last_prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_prev."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_page."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&raquo;"."</a></li>".
				"</ul>";
			}
			
			else if ($page == $last_prev)
			{
				$first = 1;
				$first1 = $first + 1;
				$first2 = $first + 2;
				$first3 = $first + 3;
				$prev = $page - 1;
				$url = 
				"<ul class='pagination'>".
					"<li class='arrow'><a data-form='products' class='paginationAnchor' data-url='"."page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&laquo;</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$first."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first."</a></li>".
					"<li class='unavailable'><a href=''>&hellip;</a></li>"."<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$prev."</a></li>".
					"<li class='current'><a data-form='products' class='paginationAnchor' data-url='"."page=".$last_prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_prev."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_page."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&raquo;"."</a></li>".
				"</ul>";
			}
			
			else if ($page == ($last_prev - 1))
			{
				$first = 1;
				$first1 = $first + 1;
				$first2 = $first + 2;
				$first3 = $first + 3;
				$prev = $page - 1;
				$next = $page + 1;
				$url= 
				"<ul class='pagination'>".
					"<li class='arrow'><a data-form='products' class='paginationAnchor' data-url='"."page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&laquo;</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$first."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first."</a></li>".
					"<li class='unavailable'><a href=''>&hellip;</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$prev."</a></li>".
					"<li class='current'><a data-form='products' class='paginationAnchor' data-url='"."page=".$page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$page."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_page."</a></li>".
					"<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&raquo;"."</a></li>".
				"</ul>";
			}
		} 
		elseif($limit == 'all')
		{
			$url="<ul class='pagination'>".
				"<li class='arrow unavailable'><a href=''>&laquo;</a></li>".
				"<li class='current'><a data-form='products' class='paginationAnchor' data-url='"."page=".$page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$page."</a></li>".
				"<li class='arrow unavailable'><a href=''>&laquo;</a></li>";
		}
		
		else		
		{
			$prev = $page - 1;
			$next = $page + 1;
			
			if ($page == 1)
			{
				$url="<ul class='pagination'>".
				"<li class='arrow unavailable'><a href=''>&laquo;</a></li>";
			}
			else
			{
				$url="<ul class='pagination'>".
				"<li class='arrow'><a data-form='products' class='paginationAnchor' data-url='"."page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&laquo;</a></li>";
			}
			
			for ($i=1 ; $i <= $last_page ; $i++)
			{
				if ($i == $page)
				{
					$url.="<li class='current'><a data-form='products' class='paginationAnchor' data-url='"."page=".$page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$page."</a></li>";
				}
				else
				{
					$url.="<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$i."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$i."</a></li>";
				}
			}
				
			if ($page == $last_page)
			{
				$url.="<li class='arrow unavailable'><a href='' >"."&raquo;"."</a></li></ul>";
			}
			else
			{
				$url.="<li><a data-form='products' class='paginationAnchor' data-url='"."page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&raquo;"."</a></li></ul>";
			}	
		}
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$json['pagination'] = $url;
			$json = json_encode($json);
			die($json);
		}
		else
		{
			return $url;
		}
  	}

?>
