<?php
	
// There is another defines.php for e-Commerce resources located in /store/includes/defines.php.
	
if (!defined("DB_USER")) {
	
	error_reporting(E_ERROR);
	// error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

	date_default_timezone_set("America/Chicago");
	// define("FONT_AWESOME_CSS", "//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css");
	define("FONT_AWESOME_CSS", "https://use.fontawesome.com/8a0137c895.js");

	define('user_media_domain', 'supplementrelief.com'); // Domain for User uploaded media
	define('user_media_upload_directory', '/home/supplementrelief/public_html/media/'); // Discussion Forum uploaded documents, images and videos

	define("STORE_FOLDER_NAME", "store"); // Sets the main path for all e-Commerce files needed.
	define("VERSION",'1');//Sets the version of custom JS and CSS files
    define('USER_PRICE_TYPE', 'RETL');//default price type
	define('public_application_id', '2'); // 2 = supplementrelief.com
	define('public_project_id', '1'); // 1 = SupplementRelief
	define('public_project_program_id', '23'); // SupplementRelief.com
	define('public_blog_id', '1299'); // SupplementRelief.com
	define('public_cookbook_id', '1280'); // SupplementRelief.com 
	define('public_geo_gos_id', '368'); // SupplementRelief.com 
	define('private_captcha_key', '6LeZlBsTAAAAAA1jB10kCOn8Aza_iWG4XuUfgIPd'); // SupplementRelief.com secret key
	define('public_captcha_key', '6LeZlBsTAAAAAMsmPQ3JFXZ9jXp-p-zFsTiDCSJ8'); // SupplementRelief.com public key
	// https://developers.google.com/recaptcha/
	// https://www.google.com/recaptcha/admin#site/320574617
	
	// Adam's defines for Account creation/management.
	define("ACCOUNT_STATUS_PRECONFIRM", "pl03//15");
	define("ACCOUNT_STATUS_ACTIVE", "nr15//15");
	define("ACCOUNT_STATUS_DELINQ", "de21//15");
	
	// define('public_environment', 'Development'); // Development Environment
	define('public_environment', 'Production'); // Production Environment
	
	switch ($_SERVER['HTTP_HOST']) {
    	
		// Jay
    	case 'localhost:8888':
			define('DB_SERVER', 'localhost');
			define("DB_USER", "root");
			define("DB_PASS", "root"); 
    		define("DB_NAME", "rtsadven_production");
            define("CUSTOMER_SERVICE_EMAIL",'jay.todtenbier@rtsadventures.com');
			define("CUSTOMER_SERVICE_EMAIL_FAIL",'jay.todtenbier@rtsadventures.com');
			
			// Search and replace all uses of $con with mysqli.
            $con = mysql_connect("localhost","root","root");
            mysql_select_db("rtsadven_production", $con);		
			
		    break;
    	
    	// Akash
        case 'local.supplementreliefdev.com':
            define('DB_SERVER', 'localhost');
            define("DB_USER", "root");
            define("DB_PASS", "akashanup"); 
            define("DB_NAME", "rtsadven_production");
            define("CUSTOMER_SERVICE_EMAIL",'akasha@mindfiresolutions.com');
            define("CUSTOMER_SERVICE_EMAIL_FAIL",'akasha@mindfiresolutions.com');
            
            // Search and replace all uses of $con with mysqli.
            $con = mysql_connect("localhost","root","akashanup");
            mysql_select_db("rtsadven_production", $con);       
            
            break;

    	// Don
    	case 'supplementrelief:8888':
		    define('DB_SERVER', '192.163.252.247');
			define("DB_USER", "rtsadven_testdev");
			define("DB_PASS", "2015Admin"); 
    		define("DB_NAME", "rtsadven_testdev");
    		define("CUSTOMER_SERVICE_EMAIL",'donw@dwdataconcepts.com');
    		define("CUSTOMER_SERVICE_EMAIL_FAIL",'donw@dwdataconcepts.com');
			break;
    	
    	// Ivan
    	case 'ivancp.dw04.pe':
    	case '192.168.10.120':
		    define('DB_SERVER', '192.163.252.247');
			define("DB_USER", "rtsadven_testdev");
			define("DB_PASS", "2015Admin"); 
    		define("DB_NAME", "rtsadven_testdev");
    		define("CUSTOMER_SERVICE_EMAIL",'ivancp@latindevelopers.com');
    		define("CUSTOMER_SERVICE_EMAIL_FAIL",'ivancp@nspsac.com');
		    break;

    	// Production
    	case 'supplementrelief.com':
    	
            if (public_environment == 'Development') {
                
    			define('DB_SERVER', 'localhost');
    			define("DB_USER", "rtsadven_develop");
    			define("DB_PASS", "giX%(&q6mwEZ"); 
        		define("DB_NAME", "rtsadven_development");
                define("CUSTOMER_SERVICE_EMAIL",'orders@supplementrelief.com');
    			define("CUSTOMER_SERVICE_EMAIL_FAIL",'orders@supplementrelief.com');
    			define("USER_STATISTICS_LOG_FILE", "userStatistics.log");             
                
            } elseif (public_environment == 'Production') {
                
                define('DB_SERVER', 'localhost');
    			define("DB_USER", "rtsadven_admin");
    			define("DB_PASS", "bvBg!XO%$8Jx");
        		define("DB_NAME", "rtsadven_production");
        		define("CUSTOMER_SERVICE_EMAIL",'orders@supplementrelief.com');
    			define("CUSTOMER_SERVICE_EMAIL_FAIL",'orders@supplementrelief.com');
    			define("USER_STATISTICS_LOG_FILE", "userStatistics.log");
    			                
            }       	
     			
            break;
    		    
    }
    
}    	

?>