<?php // echo 'Begin discussion_forum_header.php.<br />'; ?>
	
<div class="panel">
	<h3>What's on Your Mind?</h3>
	<p>Ask questions, share your thoughts and respond to what other have said.&nbsp;&nbsp;<span class="label">Post</span> a new <b>Comment</b> or <span class="label">Reply</span> to an existing one.</p>
	<p><a href="#" data-reveal-id="discussHelpModal">Help</a> for using the <b>Discussion Forum</b>.</p>
</div>
