<?php // echo 'Begin discussion_forum_help_modal.php.<br /'; ?>

<div id="discussHelpModal" class="reveal-modal" data-reveal>
		
	<h3><i class="fa fa-question-circle"></i> Discussion Forum Help</h3>
	
	<hr>
	
	<!-- <p class="lead">Be nice! This is your forum.</p> -->
	
	<p><b>Comments</b> are displayed in order of the last one posted so <b>the most recent Comment is at the top</b> and <b>older Comments are towards the bottom</b>. <b>Replies within a Comment</b> are displayed in reverse order with <b>the oldest Reply at the top</b> and <b>the most recent one at the bottom.</p> 
	
	<p>Each post identifies <i class="fa fa-user icon-red"></i> <b>who made the post</b> and the <i class="fa fa-clock-o icon-red"></i> <b>date and time</b> the post was made.</p>
	
	<p>Mouse over the icons <b>for tooltips</b> that explain what the data means.</p>
	
	<p>You will see the <i class="fa fa-ban icon-red"></i> <b>Ban</b> icon (Report Post as SPAM) immediately following the <b>Timestamp</b> of the post. Click this icon <b>if you feel strongly</b> that the content posted is not appropriate and <b>should be reviewed by the Forum Moderator</b>. You will be provided with a confirmation dialog to be sure you wish to submit this post for review. If submitted, the <b>Forum Moderator</b> will be notified to review the post and will determine what type of action to take.</p>
	
	<?php
	if ($_SESSION['enrollment']['discussion_forum_moderator']) {
		?>
		<p>If your Program Enrollment has <b>Forum Moderator</b> privileges, you will see the <b>Moderator</b> label immediately following the <b>Ban</b> icon. Then you will see <i class="fa fa-remove icon-red"></i> immediately to the right that allows the <b>Moderator</b> to delete the entire <b>Comment Thread</b> (including all <b>Replies</b>) or just an individual <b>Reply</b>.</p>
		<?php
	}
	?>
	
	<p>Click <i class="fa fa-times icon-red"></i> in the <b>upper right corner of this Help modal</b> or anywhere on the web page outside of the modal <b>to exit Help</b>.</p>
	
	<a class="close-reveal-modal">&#215;</a>
	
</div>