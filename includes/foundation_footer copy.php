<?php

// echo 'Begin Foundation Footer';

	if($ie9) 
	{
		echo '<script src="'.$_SESSION['file_directory_path_prefix'].'js/ie9.js"></script>';
	}
	 	
	echo '
		<script src="'.$_SESSION['file_directory_path_prefix'].'bower_components/foundation/js/vendor/jquery.js"></script>
		<script src="'.$_SESSION['file_directory_path_prefix'].'bower_components/foundation/js/foundation.min.js"></script>
		<!-- <script src="'.$_SESSION['file_directory_path_prefix'].'bower_components/foundation/js/foundation.topbar.js"></script> -->
		<!-- <script src="'.$_SESSION['file_directory_path_prefix'].'bower_components/foundation/js/foundation/foundation.dropdown.js"></script> -->
		<!-- <script src="'.$_SESSION['file_directory_path_prefix'].'bower_components/foundation/js/foundation/foundation.offcanvas.js"></script> -->
		<!-- <script src="'.$_SESSION['file_directory_path_prefix'].'bower_components/foundation/js/foundation/foundation.clearing.js"></script> -->
		<!-- <script src="'.$_SESSION['file_directory_path_prefix'].'bower_components/foundation/js/foundation/foundation.orbit.js"></script> -->
		<script type="text/javascript" src="'.$_SESSION['file_directory_path_prefix'].'js/jquery.scrollUp.min.js"></script>';
	  
	if ($_SESSION['page']['use_jquery_ui']) {
		echo '
			<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
			<script
			  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
			  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
			  crossorigin="anonymous">
			</script>';
	}
	    
	echo '<script src="'.$_SESSION['file_directory_path_prefix'].'js/app.js?_version='.VERSION.'"></script>';
	?>
	<?php
		if($ie9)
		{
			echo '
				<script type="text/javascript">
				$(document).ready(function() {
					var htopBar = 0
					var hHeader = 0
					if($(".top-bar").height() > 0)
					{
						htopBar = $(".top-bar").height();
					}
					if($("#e-commerce-products-header").outerHeight() > 0)
					{
						hHeader = $("#e-commerce-products-header").outerHeight();
					}
					var heightHeader =  htopBar + hHeader;
					heightHeader += "px";
					$("body").css("padding-top",heightHeader);
				});
				</script>
			';
			echo '<link rel="stylesheet" type="text/css" href="'.$_SESSION['file_directory_path_prefix'].'stylesheets/ie9_tabs.css">';
		}

		if(isset($_SESSION['enrollment']['enable_auto_logoff']) && ($_SESSION['enrollment']['enable_auto_logoff'] == TRUE))
		{
			$idleTime = isset($_SESSION['enrollment']['auto_logoff_time']) ? $_SESSION['enrollment']['auto_logoff_time'] : 60000;
			echo '<script type="text/javascript" src="'.$_SESSION['file_directory_path_prefix'].'js/jquery.idle.min.js"></script>';
	?>
			<script type="text/javascript">
				$(document).idle({
				  	onIdle: function(){
				  		if (<?php echo isset($_SESSION['user'])?>)
						window.location = '../logout/?auto_logoff=1';
				  	},
				  	idle: <?php echo $idleTime ;?>
				})
			</script>
	<?php
		}
	?>
  </body>
</html>