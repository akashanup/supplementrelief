<?php
	
ob_start(); 
	
/*
This function will turn output buffering on. While output buffering is active no output is sent from the script (other than headers), instead the output is stored in an internal buffer. The contents of this internal buffer may be copied into a string variable using ob_get_contents(). To output what is stored in the internal buffer, use ob_end_flush(). Alternatively, ob_end_clean() will silently discard the buffer contents.

$output = ob_get_contents(); // gives you whatever has been "saved" to the buffer since it was turned on with ob_start()
ob_end_clean() // or ob_flush(), which either stops saving things and discards whatever was saved, or stops saving and outputs it all at once, respectively.
*/
    
include('core.php'); // Opens database connection and establishes common functions.

?>