<?php
	
if (strlen($_SESSION['enrollment']['enrollment_id']) > 0)
{  
    if ($_SESSION['enrollment']['use_bookmarks'] > 0) {
	    // Set in /includes/page_header.php and /member-dashboard/index.php
    	include_once($_SESSION['file_directory_path_prefix'].'php/bookmarks.php');
    } 
}
    
?>

<!-- <div class="fixed contain-to-grid"> -->
<div class="fixed">
 
    <nav class="top-bar" data-topbar role="navigation">
         
        <ul class="title-area">
             
            <li class="name">
                <a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>" title="View SupplementRelief.com Home page"><img src="<?php echo $_SESSION['application']['root_media_host_url'].$_SESSION['application']['navigation_logo_url']; ?>" alt="<?php echo $_SESSION['application']['navigation_logo_alt_text']; ?>" /></a>
            </li>
            
            <li class="toggle-topbar menu-icon"><a href="#"><span><!--Menu--></span></a></li>
         
        </ul>
         
        <?php
        // Set the active Menu item
 
        if (strlen($_SESSION['file_directory_path_prefix']) == 0) { $home_active = 'active'; }
         
        $active = 'active';
        
        switch ($_SESSION['page']['page_id']) {     
            case '99':          
                $product_active = $active;
                break;
            case '98':          
                $product_active = $active;
                break;                      
            case '95':          
                $login_active = $active;
                break;
            case '107':         
                $member_dashboard_active = $active;
                break;
            case '157':         
                $program_description_active = $active;
                break;          
            case '120':         
                $register_account_active = $active;
                break;
            case '2':           
                $home_active = $active;
                break;
            case '93':          
                $education_active = $active;
                break;
            case '109':         
                $blog_posts_active = $active;
                break;
            case '106':         
                $blog_posts_active = $active;
                break;
            case '235':         
                $recipe_active = $active;
                break;
            case '100':         
                $recipe_active = $active;
                break;
            case '108':         
                $resources_active = $active;
                break;
            case '104':         
                $resources_active = $active;
                break;
            case '92':          
                $contact_active = $active;
                break;
        }   
                     
        ?>
     
        <section class="top-bar-section">
             
            <!-- Left Nav Section -->
            <ul class="left">
                                 
                <!-- show Database -->
                <?php list($menu_database_active, $menu_database_relative_url, $menu_database_hint_text, $menu_database_icon, $menu_database_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 17);

	                if ($menu_database_active == 1 && $_SESSION['user']['user_id'] == 2) { ?>
						<li class="">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_database_relative_url; ?>/" title="<?php echo $menu_database_hint_text; ?>"><i class="<?php echo $menu_database_icon; ?>"></i>&nbsp;&nbsp;<?php echo public_environment; ?></a>
						</li>
				<?php } ?>

                <!-- show Phone -->
                <?php list($menu_phone_active, $menu_phone_relative_url, $menu_phone_hint_text, $menu_phone_icon, $menu_phone_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 16);

	                if ($menu_phone_active == 1) { ?>
						<li class="<?php echo $contact_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_phone_relative_url; ?>/" title="<?php echo $menu_phone_hint_text; ?>"><i class="<?php echo $menu_phone_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_phone_title; ?></a>
						</li>
				<?php } ?>
				
				<!-- show Store -->
                <?php list($menu_store_active, $menu_store_relative_url, $menu_store_hint_text, $menu_store_icon, $menu_store_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 2);

	                if ($menu_store_active == 1) { ?>
						<li class="has-dropdown <?php echo $product_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_store_relative_url; ?>/" title="<?php echo $menu_store_hint_text; ?>"><i class="<?php echo $menu_store_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_store_title; ?></a>
							<ul class="dropdown">
								<li class="active"><a href="#" data-reveal-id="NuMedicaProductDirectory"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;&nbsp;NuMedica Supplement Directory</a></li>
                    		</ul>
						</li>
						<?php include_once($_SESSION['file_directory_path_prefix'].'includes/numedica-project-brand-product-link-index.php'); ?>
				<?php } ?>                
                                                         
                <?php    
                // Authenticated User and Page is NOT "Select Program"      
                if (isset($_SESSION['user']['user_id']) && strlen($_SESSION['user']['user_id']) > 0 && $_SESSION['page']['name'] != 'Select Program') {
                     
                    ?>
                    <!-- show User -->
                    <li class="<?php echo $member_dashboard_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>member-dashboard/" title="View Member Dashboard"><i class="fa fa-user icon-red"></i>&nbsp;&nbsp;<?php echo $_SESSION['user']['full_name']; ?></a></li>
                 
                    <!-- show current Program selected -->
                    <?php
                    list($menu_program_description_active, $menu_program_description_relative_url, $menu_program_description_hint_text, $menu_program_description_icon, $menu_program_description_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 11);

                    if ($menu_program_description_active == 1 && strlen($_SESSION['enrollment']['project_program_name']) > 0) {
                        ?>
                        <li class="<?php echo $program_description_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>project-program/?pp_id=<?php echo $_SESSION['enrollment']['project_program_id']; ?>" title="View Program Description"><i class="fa fa-book nav-icon-menu-color"></i>&nbsp;&nbsp;<?php echo $_SESSION['enrollment']['project_program_name']; ?></a></li>
                        <?php
                    }
					
					// Show the Education menu option
                    if ( (strlen($_SESSION['enrollment']['course_id']) > 0) && (strlen($_SESSION['enrollment']['topic_id']) > 0) ) {
	                    list($menu_education_description_active, $menu_education_description_relative_url, $menu_education_description_hint_text, $menu_education_description_icon, $menu_education_description_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 18);

		                if ($menu_education_description_active == 1) { ?>
							<li class="<?php echo $education_active; ?>">
								<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_education_description_relative_url; ?>/" title="<?php echo $menu_education_description_hint_text; ?>"><i class="<?php echo $menu_education_description_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_education_description_title; ?></a>
							</li>
						<?php } 
							
					}							
                    ?>
				
                    <!-- show Logout -->
                    <?php list($menu_logout_active, $menu_logout_relative_url, $menu_logout_hint_text, $menu_logout_icon, $menu_logout_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 7);

	                if ($menu_logout_active == 1) { ?>
						<li class="<?php echo $logout_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_logout_relative_url; ?>/" title="<?php echo $menu_logout_hint_text; ?>"><i class="<?php echo $menu_logout_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_logout_title; ?></a>
						</li>
					<?php }
              
                } elseif (strlen($_SESSION['user']['user_id']) < 1 && $_SESSION['page']['name'] != 'Login') {
                    // Public User (not logged in)
                    ?>
                                             
                    <!-- show Login -->
                    <?php list($menu_login_active, $menu_login_relative_url, $menu_login_hint_text, $menu_login_icon, $menu_login_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 6);

	                if ($menu_login_active == 1) { ?>
						<li class="<?php echo $login_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_login_relative_url; ?>/" title="<?php echo $menu_login_hint_text; ?>"><i class="<?php echo $menu_login_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_login_title; ?></a>
						</li>
					<?php } ?>
                     
                    <!-- show Featured Program Offering Registration -->
                    <?php list($menu_program_registration_active, $menu_program_registration_relative_url, $menu_program_registration_hint_text, $menu_program_registration_icon, $menu_program_registration_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 10);

	                if ($menu_program_registration_active == 1) { ?>
						<li class="<?php echo $register_account_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_program_registration_relative_url; ?>/" title="<?php echo $menu_program_registration_hint_text; ?>"><i class="<?php echo $menu_program_registration_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_program_registration_title; ?></a>
						</li>
					<?php }
              
                } elseif (isset($_SESSION['user']['user_id']) &&  strlen($_SESSION['user']['user_id']) > 0) {
                    // Authenticated User and Page is "Select Program"  
                    ?>
                     
                    <!-- show Logout -->
                    <?php list($menu_logout_active, $menu_logout_relative_url, $menu_logout_hint_text, $menu_logout_icon, $menu_logout_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 7);

	                if ($menu_logout_active == 1) { ?>
						<li class="<?php echo $logout_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_logout_relative_url; ?>/" title="<?php echo $menu_logout_hint_text; ?>"><i class="<?php echo $menu_logout_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_logout_title; ?></a>
						</li>
					<?php }

                } 
                ?>  
                               
            </ul>
             
            <!-- Right Nav Section -->
            <ul class="right">
             
                <li class="<?php echo $home_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>" title="View SupplementRelief.com Home page"><i class="fa fa-home nav-icon-menu-color"></i>&nbsp;&nbsp;Home</a></li>
         
                <?php
                // Determine if User is authenticated and has selected a Project Program Offering. Display appropriate Navigation based upon authenticiation.           
                if (isset($_SESSION['user']['user_id']) && isset($_SESSION['enrollment']['project_program_id']) && strlen($_SESSION['user']['user_id']) > 0 && strlen($_SESSION['enrollment']['project_program_id']) > 0) {
             
                    // Authenticated User and Project Program Offering so show all menu navigation options.
             
                    ?>
					<!-- show Blog -->
                    <?php list($menu_blog_active, $menu_blog_relative_url, $menu_blog_hint_text, $menu_blog_icon, $menu_blog_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 9);

	                if ($menu_blog_active == 1) { ?>
						<li class="<?php echo $blog_posts_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_blog_relative_url; ?>/" title="<?php echo $menu_blog_hint_text; ?>"><i class="<?php echo $menu_blog_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_blog_title; ?></a>
						</li>
					<?php } ?>
					
					<!-- show Recipes -->
                    <?php list($menu_recipes_active, $menu_recipes_relative_url, $menu_recipes_hint_text, $menu_recipes_icon, $menu_recipes_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 5);

	                if ($menu_recipes_active == 1) { ?>
						<li class="<?php echo $recipe_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_recipes_relative_url; ?>/" title="<?php echo $menu_recipes_hint_text; ?>"><i class="<?php echo $menu_recipes_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_recipes_title; ?></a>
						</li>
					<?php } ?>
                    
                    <!-- show Resources -->
                    <?php list($menu_resources_active, $menu_resources_relative_url, $menu_resources_hint_text, $menu_resources_icon, $menu_resources_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 4);

	                if ($menu_resources_active == 1) { ?>
						<li class="<?php echo $resources_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_resources_relative_url; ?>/" title="<?php echo $menu_resources_hint_text; ?>"><i class="<?php echo $menu_resources_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_resources_title; ?></a>
						</li>
					<?php }
                 
                } else {
             
                    // Don't know who the User is so just show Public pages.
                    ?>
                    
                    <!-- show Blog -->
                    <?php list($menu_blog_active, $menu_blog_relative_url, $menu_blog_hint_text, $menu_blog_icon, $menu_blog_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 9);

	                if ($menu_blog_active == 1) { ?>
						<li class="<?php echo $blog_posts_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_blog_relative_url; ?>/" title="<?php echo $menu_blog_hint_text; ?>"><i class="<?php echo $menu_blog_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_blog_title; ?></a>
						</li>
					<?php }                 
                     
                    list($menu_recipes_active, $menu_recipes_relative_url, $menu_recipes_hint_text, $menu_recipes_icon, $menu_recipes_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 5);
                                         
					if ($menu_recipes_active == 1) { ?>
						<li class="<?php echo $recipe_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_recipes_relative_url; ?>/" title="<?php echo $menu_recipes_hint_text; ?>"><i class="<?php echo $menu_recipes_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_recipes_title; ?></a>
						</li>
					<?php } ?>
					
					<!-- show Resources -->
                    <?php list($menu_resources_active, $menu_resources_relative_url, $menu_resources_hint_text, $menu_resources_icon, $menu_resources_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 4);

	                if ($menu_resources_active == 1) { ?>
						<li class="<?php echo $resources_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_resources_relative_url; ?>/" title="<?php echo $menu_resources_hint_text; ?>"><i class="<?php echo $menu_resources_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_resources_title; ?></a>
						</li>
					<?php } ?>
					
					<!-- show Contact -->
                    <?php list($menu_contact_active, $menu_contact_relative_url, $menu_contact_hint_text, $menu_contact_icon, $menu_contact_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 1);

	                if ($menu_contact_active == 1) { ?>
						<li class="<?php echo $contact_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_contact_relative_url; ?>/" title="<?php echo $menu_contact_hint_text; ?>"><i class="<?php echo $menu_contact_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_contact_title; ?></a>
						</li>
					<?php }

                    }   
                ?>
                             
            </ul>
             
        </section>
             
    </nav>
         
    <!-- </div> -->
         
    <?php    
    if ($_SESSION['page']['name'] != 'NuMedica Supplements' && $_SESSION['page']['name'] != 'Product') { 
        // Close the div to contain the "sticky" behavior.
        ?>
        </div>
        <?php
             
    } else {
        // echo 'Session Page Name is: '.$_SESSION['page_name'].'<br />';
        // Leave the div open because we need the e-Commerce header in the sticky div.
        // We close the div later on the Store (NuMedica) and Product page.
    }
    ?>
    