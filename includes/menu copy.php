<?php
	
if (strlen($_SESSION['enrollment']['enrollment_id']) > 0)
{  
    if ($_SESSION['enrollment']['use_bookmarks'] > 0) {
	    // Set in /includes/page_header.php and /member-dashboard/index.php
    	include_once($_SESSION['file_directory_path_prefix'].'php/bookmarks.php');
    } 
}
    
?>

<!-- <div class="fixed contain-to-grid"> -->
<div class="fixed">
 
    <nav class="top-bar" data-topbar role="navigation">
         
        <ul class="title-area">
             
            <li class="name">
                <a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>" title="View SupplementRelief.com Home page"><img src="<?php echo $_SESSION['application']['root_media_host_url'].$_SESSION['application']['navigation_logo_url']; ?>" alt="<?php echo $_SESSION['application']['navigation_logo_alt_text']; ?>" /></a>
            </li>
            
            <li class="toggle-topbar menu-icon"><a href="#"><span><!--Menu--></span></a></li>
         
        </ul>
         
        <?php
        // Set the active Menu item
 
        if (strlen($_SESSION['file_directory_path_prefix']) == 0) { $home_active = 'active'; }
         
        $active = 'active';
        
        switch ($_SESSION['page']['page_id']) {     
            case '99':          
                $product_active = $active;
                break;
            case '98':          
                $product_active = $active;
                break;                      
            case '95':          
                $login_active = $active;
                break;
            case '107':         
                $member_dashboard_active = $active;
                break;
            case '157':         
                $program_description_active = $active;
                break;          
            case '120':         
                $register_account_active = $active;
                break;
            case '2':           
                $home_active = $active;
                break;
            case '93':          
                $education_active = $active;
                break;
            case '109':         
                $blog_posts_active = $active;
                break;
            case '106':         
                $blog_posts_active = $active;
                break;
            case '235':         
                $recipe_active = $active;
                break;
            case '100':         
                $recipe_active = $active;
                break;
            case '108':         
                $resources_active = $active;
                break;
            case '104':         
                $resources_active = $active;
                break;
            case '92':          
                $contact_active = $active;
                break;
        }   
                     
        ?>
     
        <section class="top-bar-section">
             
            <!-- Left Nav Section -->
            <ul class="left">
                 
                <?php
	            /*
                if ($_SESSION['user']['user_id'] == 2) { 
                    ?>
                    <li><a href="#"><i class="fa fa-database icon-red"></i>&nbsp;&nbsp;<?php echo public_environment; ?></a></li>
                    <?php
                }
                */
                ?>
                
                <?php
	                // Function to display Phone Menu item based upon Application and Program Offering (if present) 
	                list($menu_phone_active, $menu_phone_relative_url, $menu_phone_hint_text, $menu_phone_icon, $menu_phone_title) = setMenuItem($connection, $_SESSION['enrollment']['project_program_id'], 'MMI', 16);

	                if ($menu_phone_active == 1) { 
		            	?>
						<li class="<?php echo $contact_active; ?>">
							<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?><?php echo $menu_phone_relative_url; ?>/" title="<?php echo $menu_phone_hint_text; ?>"><i class="<?php echo $menu_phone_icon; ?>"></i>&nbsp;&nbsp;<?php echo $menu_phone_title; ?></a>
						</li>
						<?php  
	                }
	            ?>
                 
                <!--
                <li class="<?php echo $contact_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>contact/" title="Contact SupplementRelief.com Customer Support"><i class="fa fa-phone nav-icon-menu-color" aria-hidden="true"></i>&nbsp;&nbsp;888.424.0032</a></li>
                -->           
                                  
                <li class="has-dropdown <?php echo $product_active; ?> ">
                    <a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>numedica/" title="Shop for NuMedica Supplements"><i class="fa fa-shopping-cart nav-icon-menu-color"></i>&nbsp;&nbsp;Shop NuMedica</a>
                    <ul class="dropdown">
                      <!-- <li><a href="#">First link in dropdown</a></li> -->
                      <li class="active"><a href="#" data-reveal-id="NuMedicaProductDirectory"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;&nbsp;NuMedica Supplement Directory</a></li>
                    </ul>
                </li>
                   
                <?php include_once($_SESSION['file_directory_path_prefix'].'includes/numedica-project-brand-product-link-index.php'); ?>
                                                         
                <?php    
                // Authenticated User and Page is NOT "Select Program"      
                if (isset($_SESSION['user']['user_id']) && strlen($_SESSION['user']['user_id']) > 0 && $_SESSION['page']['name'] != 'Select Program') {
                     
                    ?>
                    <!-- show User -->
                    <li class="<?php echo $member_dashboard_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>member-dashboard/" title="View Member Dashboard"><i class="fa fa-user icon-red"></i>&nbsp;&nbsp;<?php echo $_SESSION['user']['full_name']; ?></a></li>
                 
                    <!-- show current Program selected -->
                    <?php
                    if (strlen($_SESSION['enrollment']['project_program_name']) > 0) {
                        ?>
                        <li class="<?php echo $program_description_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>project-program/?pp_id=<?php echo $_SESSION['enrollment']['project_program_id']; ?>" title="View Program Description"><i class="fa fa-book nav-icon-menu-color"></i>&nbsp;&nbsp;<?php echo $_SESSION['enrollment']['project_program_name']; ?></a></li>
                        <?php
                    }
                    ?>
     
                    <!-- optionally promote a new program -->
                    <!--<li><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>program-registration/?pp_id=24" title="Register for the FREE Six Steps to Healthy Eating Cooking Program that begins November 3"><i class="fa fa-sign-in nav-icon-menu-color"></i>&nbsp;&nbsp;FREE Healthy Cooking Webinar Series</a></li>-->
                     
                    <!-- show Logout -->
                    <li><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>logout/" title="Logout"><i class="fa fa-unlock nav-icon-menu-color"></i>&nbsp;&nbsp;Logout</a></li>
                             
                    <?php 
              
                } elseif (strlen($_SESSION['user']['user_id']) < 1 && $_SESSION['page']['name'] != 'Login') {
                    // Public User (not logged in)
     
                    ?>
                         
                    <!-- show Login -->
                    <li class="<?php echo $login_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>login/" title="Login to Member Account"><i class="fa fa-lock nav-icon-menu-color"></i>&nbsp;&nbsp;Login</a></li>
                     
                    <!-- show Register for Member Account -->
                    <li class="<?php echo $register_account_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>register-member-account/" title="Learn about Membership Benefits"><i class="fa fa-user nav-icon-menu-color"></i>&nbsp;&nbsp;Member Benefits</a></li>
                     
                    <!-- optionally promote a new program -->
                    <!--<li><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>program_registration/?pp_id=24" title="Register for the FREE Six Steps to Healthy Eating Cooking Program that begins November 3"><i class="fa fa-sign-in nav-icon-menu-color"></i>&nbsp;&nbsp;FREE Healthy Cooking Webinar Series</a></li>-->
                                 
                    <?php 
              
                } elseif (isset($_SESSION['user']['user_id']) &&  strlen($_SESSION['user']['user_id']) > 0) {
                    // Authenticated User and Page is "Select Program"  
                    ?>
                     
                    <!-- show Logout -->
                    <li><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>logout/" title="Logout"><i class="fa fa-lock-in nav-icon-menu-color"></i>&nbsp;&nbsp;Logout</a></li>
                        
                    <?php                    
                } 
                ?>  
                               
            </ul>
             
            <!-- Right Nav Section -->
            <ul class="right">
             
                <li class="<?php echo $home_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>" title="View SupplementRelief.com Home page"><i class="fa fa-home nav-icon-menu-color"></i>&nbsp;&nbsp;Home</a></li>
         
                <?php
                // Determine if User is authenticated and has selected a Project Program Offering. Display appropriate Navigation based upon authenticiation.           
                if (isset($_SESSION['user']['user_id']) && isset($_SESSION['enrollment']['project_program_id']) && strlen($_SESSION['user']['user_id']) > 0 && strlen($_SESSION['enrollment']['project_program_id']) > 0) {
             
                    // Authenticated User and Project Program Offering so show all menu navigation options.
             
                    if (strlen($_SESSION['enrollment']['course_id']) > 0 ) {
                        // Show the Education menu option
                        ?>
                        <li class="<?php echo $education_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>education/" title="View Program Education"><i class="fa fa-graduation-cap nav-icon-menu-color"></i>&nbsp;&nbsp;Education</a></li>
                        <?php    
                    }
                    ?>
                     
                    <li class="<?php echo $blog_posts_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>your-healthy-life-concierge-blog/" title="View Wellness Blog"><i class="fa fa-comment nav-icon-menu-color"></i>&nbsp;&nbsp;Blog</a></li>
                     
                    <!--<li><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>calendar/" title="View Calendar"><i class="fa fa-calendar nav-icon-menu-color"></i>&nbsp;&nbsp;Calendar</a></li>-->
                     
                    <li class="<?php echo $recipe_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>healthy-living-whole-foods-cookbook/" title="View Healthy Living Whole Foods Cookbook Recipes"><i class="fa fa-cutlery nav-icon-menu-color"></i>&nbsp;&nbsp;Recipes</a></li>
                                     
                    <li class="<?php echo $resources_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>resources/" title="View Wellness Resources"><i class="fa fa-briefcase nav-icon-menu-color"></i>&nbsp;&nbsp;Wellness</a></li>
                                         
                    <!-- <li><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>news/" title="News"><i class="fa fa-bullhorn nav-icon-menu-color"></i>&nbsp;&nbsp;News</a></li> -->
                                         
                    <?php
                 
                } else {
             
                    // Don't know who the User is so just show Public pages.
                    ?>
                    <li class="<?php echo $blog_posts_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>your-healthy-life-concierge-blog/" title="View Wellness Blog"><i class="fa fa-comment nav-icon-menu-color"></i>&nbsp;&nbsp;Blog</a></li>
                     
                    <!-- <li><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>calendar/" title="View Calendar"><i class="fa fa-calendar nav-icon-menu-color"></i>&nbsp;&nbsp;Calendar</a></li> -->
                     
                    <li class="<?php echo $recipe_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>healthy-living-whole-foods-cookbook/" title="View Healthy Living Whole Foods Cookbook Recipes"><i class="fa fa-cutlery nav-icon-menu-color"></i>&nbsp;&nbsp;Recipes</a></li>
                     
                    <li class="<?php echo $resources_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>resources/" title="View Wellness Resources"><i class="fa fa-briefcase nav-icon-menu-color"></i>&nbsp;&nbsp;Wellness</a></li>
                                          
                    <li class="<?php echo $contact_active; ?>"><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>contact/" title="Contact SupplementRelief.com Customer Support"><i class="fa fa-envelope nav-icon-menu-color" aria-hidden="true"></i>&nbsp;&nbsp;Contact</a></li>                
                
                     <?php   
                }   
                ?>
                             
            </ul>
             
        </section>
             
    </nav>
         
    <!-- </div> -->
         
    <?php    
    if ($_SESSION['page']['name'] != 'NuMedica Supplements' && $_SESSION['page']['name'] != 'Product') { 
        // Close the div to contain the "sticky" behavior.
        ?>
        </div>
        <?php
             
    } else {
        // echo 'Session Page Name is: '.$_SESSION['page_name'].'<br />';
        // Leave the div open because we need the e-Commerce header in the sticky div.
        // We close the div later on the Store (NuMedica) and Product page.
    }
    ?>
    