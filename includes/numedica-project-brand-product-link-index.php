<?php
	
	// Query the Project Brand Products
	$query = 'SELECT 
		bpu.name,  
		url.url 
		FROM project_brand_product_usages pbpu 
		LEFT JOIN brand_product_usages bpu ON pbpu.brand_product_usage_id = bpu.id 
		LEFT JOIN url_usages urlu ON pbpu.id = urlu.pbpu_id 
		LEFT JOIN urls url ON urlu.url_id = url.id 
		WHERE pbpu.project_id = "'.$_SESSION['enrollment']['project_id'].'"   
		AND pbpu.active = 1 
		ORDER BY bpu.name ASC';
	
	// echo $query;
	// die();
	
	$result = mysqli_query($connection, $query);
	
	if (!$result) {
		show_mysqli_error_message($query, $connection);
		die;
	}
	
	// $total_records = mysqli_num_rows($result);	
	// echo 'Total Project Brand Products: '.$total_records.'<br />';
		
	while($r = mysqli_fetch_assoc($result)) {
		
		// show_array($r);
		// echo $r['name'].'<br>'.$r['url'].'<br>';
		// echo str_replace("NuMedica","",$r['name']).'<br>';
		
		$productLinks .= '
        <a href="'.$_SESSION['file_directory_path_prefix'].$r['url'].'/">'.str_replace("NuMedica","",$r['name']).'</a><br>';
	    
	}
	
	mysqli_free_result($result);
		
?>

<div id="NuMedicaProductDirectory" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<img src="https://cdn-manager.net/media/images/numedica-logo-landscape-medium.png" alt="NuMedica">
	<h2 id="modalTitle">Supplement Directory</h2>
	<p class="lead">Click the link below to be taken directly to the product page.</p>
	<?php echo $productLinks; ?>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>