<?php
	
	// Query the Project Brand Products
	$query = 'SELECT 
		bpu.name,  
		url.url 
		FROM project_brand_product_usages pbpu 
		LEFT JOIN brand_product_usages bpu ON pbpu.brand_product_usage_id = bpu.id 
		LEFT JOIN url_usages urlu ON pbpu.id = urlu.pbpu_id 
		LEFT JOIN urls url ON urlu.url_id = url.id 
		WHERE pbpu.project_id = "'.$_SESSION['enrollment']['project_id'].'"   
		AND pbpu.active = 1 
		ORDER BY bpu.name ASC';
	
	// echo $query;
	// die();
	
	$result = mysqli_query($connection, $query);
	
	if (!$result) {
		show_mysqli_error_message($query, $connection);
		die;
	}
	
	// $total_records = mysqli_num_rows($result);	
	// echo 'Total Project Brand Products: '.$total_records.'<br />';
	
	$productMenu = '
	<li class="has-dropdown">
		<a href="'.$_SESSION['file_directory_path_prefix'].'numedica/"><i class="fa fa-shopping-cart nav-icon-menu-color"></i>&nbsp;&nbsp;Shop NuMedica</a>
			<ul class="dropdown">';
	
	while($r = mysqli_fetch_assoc($result)) {
		
		// show_array($r);
		// echo $r['name'].'<br>'.$r['url'].'<br>';
		// echo str_replace("NuMedica","",$r['name']).'<br>';
		
		$productMenu .= '
        <li><a href="'.$_SESSION['file_directory_path_prefix'].$r['url'].'/">'.str_replace("NuMedica","",$r['name']).'</a></li>';
	    
	}
	
	mysqli_free_result($result);
	
	$productMenu .= '
		</ul>
	</li>';
	
	echo $productMenu;
	
?>