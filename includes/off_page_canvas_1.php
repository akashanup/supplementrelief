<?php

// Customize the Page Title displayed within the <section class="middle tab-bar-section"> for certain Pages.
if ($_SESSION['page']['page_id'] == 3 OR $_SESSION['page']['page_id'] == 93) {
	// Education page for SOZO and SupplementRelief
	GLOBAL $course_title;
	GLOBAL $web_page_title;
	$page_title = $course_title.' - '.$web_page_title;	
} else {
	if ($_SESSION['page']['page_id'] == 67 OR $_SESSION['page']['page_id'] == 98) {
		// Product page for SOZO and SupplementRelief
		GLOBAL $page_meta_title;
		// $page_title = ' - <b>Phone Orders (888) 424-0032</b>';
		$page_title = ': '.$page_meta_title;	
	} else {
		$page_title = null;
	}
}

?>

<div class="off-canvas-wrap" data-offcanvas>
  	<div class="inner-wrap">
    	<nav class="tab-bar">
	    	
			<section class="left-small">
		        <a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
      		</section>

	  		<section class="middle tab-bar-section">
		  		
			    <div class="show-for-medium-up">
				    <?php
					    if ($_SESSION['page']['page_id'] == 3 OR $_SESSION['page']['page_id'] == 93) { 
						    // Education page for SOZO and SupplementRelief
						    ?> 
						    <div>
							    <i class="<?php echo $_SESSION['page']['icon_snippet'];?> icon-white"></i>&nbsp;&nbsp;<b><?php echo $page_title; ?></b>
							</div> 
						<?php 
						} else {
							?> 
							<div>
								<i class="<?php echo $_SESSION['page']['icon_snippet']; ?> icon-white"></i>&nbsp;&nbsp;<b><?php echo $_SESSION['page']['name'].$page_title; ?></b>
							</div>
						<?php 
						} 
						?>
		       	</div>
		       	
				<div class="show-for-small-only">
		        	<div><i class="<?php echo $_SESSION['page']['icon_snippet'];?> icon-white"></i>&nbsp;&nbsp;<?php echo $_SESSION['page']['page_name']; ?></div>
			    </div>
			    
			</section>
    	</nav>
    	
	    <aside class="left-off-canvas-menu">
		    
	    	<ul class="off-canvas-list">
		    	
	        	<li><label>Tools</label></li>
	        	
	        	<?php
		        if ( (strlen($global_user_help_text) > 0) OR (strlen($_SESSION['page']['user_help_video_host_url_mp4']) ) > 0) {
			        ?>
			        <li><a href="#" data-reveal-id="pageHelpModal"><i class="fa fa-info nav-icon-color"></i>&nbsp;&nbsp;Page Help</a></li>
			        <?php
		        }	
		        ?>
	        	
				<li><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>faq/"><i class="fa fa-question nav-icon-color"></i>&nbsp;&nbsp;FAQ</a></li>
			
				<?php
				if (isset($_SESSION['user']['user_id']) && strlen($_SESSION['user']['user_id']) > 0) {
					?>			     
					<li><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>member-dashboard/"><i class="fa fa-user nav-icon-color"></i>&nbsp;&nbsp;Member Dashboard</a></li>
					<?php
				}
				?>     
				<li><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>contact/"><i class="fa fa-phone nav-icon-color"></i>&nbsp;&nbsp;Contact</a></li>
				<li><label>Login Information</label></li>		       
	      	</ul>
	      
		  	<br />
	      
		    <?php
		      
		    // Display the Project Logo for the Program Offering the User has selected.
		    if (isset($_SESSION['project_logo_strip_url']) &&  strlen($_SESSION['project_logo_strip_url']) > 0) {
				echo '
				<div class="row">
					<div class="small-12 columns">
						<div class="stripLogo"><img src="'.$_SESSION['project_logo_strip_url'].'"></div>
					</div>
				</div>';
			}
		      					
			// Display the Program Logo for the Program Offering the User has selected.          
			if (isset($_SESSION['program_logo_strip_url']) &&  strlen($_SESSION['program_logo_strip_url']) > 0) {
				echo '
				<div class="row">
					<div class="small-12 columns">
						<div class="stripLogo"><img src="'.$_SESSION['program_logo_strip_url'].'"></div>
					</div>
				</div>';					
			}	
			            
			echo '
			<div class="row">
				<div class="small-12 columns">     
					<div style="color:white;">
						<p>';
						if (isset($_SESSION['user']['full_name'])) { echo '<i class="fa fa-user"></i>&nbsp;&nbsp;'.$_SESSION['user']['full_name'].'<br /><br />'; }
						if (isset($_SESSION['user']['login_timestamp'])) { echo '<i class="fa fa-clock-o"></i>&nbsp;&nbsp;'.$_SESSION['user']['login_timestamp'].'<br /><br />'; }
						if (isset($_SESSION['enrollment']['project_name'])) { echo '<i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;'.$_SESSION['enrollment']['project_name'].'<br /><br />'; }
						if (isset($_SESSION['enrollment']['project_program_name'])) { echo '<i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;'.$_SESSION['enrollment']['project_program_name']; }
						
						// if (isset($_SESSION['project_program_name'])) { echo '<a href="#" title="Program selected"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;'.$_SESSION['project_program_name'].'</a><br />'; }
						// if (isset($_SESSION['user']['login_timestamp'])) { echo '<a href="#" title="Last Log In Timestamp"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;'.$_SESSION['user']['login_timestamp'].'</a>'; }
						echo 
						'</p>
					</div>
				</div>
			</div>';
			         
			?>     
	      
	    </aside>

		<section class="main-section">
		<!-- main content for off canvas starts here -->
