      		<!-- main content for off canvas ends here -->
      
	  	</section>

		<a class="exit-off-canvas"></a>

  	</div>
</div>

<!-- These variables are set in page_header.php. 
$global_user_help_text = $r['user_help_text'];
$_SESSION['page']['user_help_video_title'] = $r['user_help_video_title'];
$_SESSION['page']['user_help_video_host_url_mp4'] = $r['user_help_video_host_url_mp4'];
$_SESSION['page']['user_help_video_host_url_webm'] = $r['user_help_video_host_url_webm'];	
$_SESSION['page']['user_help_video_duration_minutes_seconds'] = $r['user_help_video_duration_minutes_seconds'];	
$_SESSION['page']['user_help_video_poster_image_url'] = $r['user_help_video_poster_image_url'];					
-->

<div id="pageHelpModal" class="reveal-modal medium" data-reveal="">

	<h1><?php echo $_SESSION['page']['name']; ?><small>&nbsp;Page Help</small></h1>
 
	<?php
  
  		if (strlen($global_user_help_text) > 0) {
  			echo $global_user_help_text;
  		}
  		
		if (strlen($_SESSION['page']['user_help_video_host_url_mp4']) > 0) { 
		
			if (strlen($_SESSION['page']['user_help_video_poster_image_url']) > 0) {
				$video_poster_url = $_SESSION['application']['root_media_host_url'].$_SESSION['page']['user_help_video_poster_image_url'];
			} else {
				$video_poster_url = 'https://placehold.it/640x360?text=no+image+provided';
			}
			
			?>
			<h3><?php echo $_SESSION['page']['user_help_video_title']; ?><small>&nbsp;Time: <?php echo $_SESSION['page']['user_help_video_duration_minutes_seconds']; ?></small></h3>
			
			<div class="flex-video widescreen" style="display: block;">
			    <video controls poster="<?php echo $video_poster_url; ?>"> 
			    	<source src="<?php echo $_SESSION['application']['root_media_host_url'].$_SESSION['page']['user_help_video_host_url_mp4']; ?>" type="video/mp4">
					<source src="<?php echo $_SESSION['application']['root_media_host_url'].$_SESSION['page']['user_help_video_host_url_webm']; ?>" type="video/webm">   
					Your browser does not support the video tag.
			    </video>    
			</div> 
			<?php
				 
		}
			
	?>	

	<a class="close-reveal-modal">&#215;</a>
	
</div>
<?php

// unset($_SESSION['page']['user_help_text']);
unset($_SESSION['page']['user_help_video_title']);
unset($_SESSION['page']['user_help_video_host_url_mp4']);
unset($_SESSION['page']['user_help_video_host_url_webm']);
unset($_SESSION['page']['user_help_video_duration_minutes_seconds']);
unset($_SESSION['page']['user_help_video_poster_image_url']);

?>