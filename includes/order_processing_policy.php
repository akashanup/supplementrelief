<?php
	
if (isset($_SESSION['enrollment']['project_id'])) {
	$project_id = $_SESSION['enrollment']['project_id'];
} else {
	$project_id = public_project_id; // store/includes/defines.php
}

?>

<!--googleoff: index-->
<div id="shippingHandling" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	
	<div class="panel">
		<h2><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp;&nbsp;Order Processing Policy</h2>
		<p>Please call <b><?php echo $_SESSION['application']['phone']; ?></b> or <a href="../contact/" title="Contact <?php echo $_SESSION['application']['name']; ?> Customer Service">email</a> if you have questions or need assistance.</p>
	</div>
	
	<?php
  
		// Query the Project Shipping & Handling Policy	
		$queryProject = 'SELECT 
			pj.payment_policy, 
			pj.email_shipping_policy, 
			pj.email_refund_policy 
			FROM projects pj 
			WHERE pj.id = "'.$project_id.'"';
			
		// echo $queryProject;
		// die();
			
		// Execute the query
		$result_project = mysqli_query($connection, $queryProject);
		
		if (!$result_project) {
			show_mysqli_error_message($queryProject, $connection);
			die;
		}
		
		while($r = mysqli_fetch_assoc($result_project)) {
		
			// show_array($r);
			
			echo '<h3>Payment</h3>';
			echo $r['payment_policy'].'<hr>';
			
			if ($_SESSION['application']['privacy_policy']) {
				echo '<h3>Privacy</h3>';
				echo $_SESSION['application']['privacy_policy'].'<hr>';
			}
			
			echo '<h3>Shipping & Handling</h3>';
			echo $r['email_shipping_policy'].'<hr>';
			
			echo '<h3>Returns and Refunds</h3>';
			echo $r['email_refund_policy'];
			
		}
		
		mysqli_free_result($result_project);
	
	?>
	
	<center><img src="https://cdn-manager.net/media/images/package-delivery-large-landscape.jpg" alt="orders are delivered to your door"></center>				

	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
  
</div>
<!--googleon: index-->
