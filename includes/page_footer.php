<?php	
if (strlen($_SESSION['file_directory_path_prefix']) > 0) { 
	// not at root level of project
	$breadcrumb = '
	<li><a href="'.$_SESSION['file_directory_path_prefix'].'"><i class="fa fa-home icon-home"></i>&nbsp;Home</a></li>
	<li class="current"><a href="./"><i class="'.$_SESSION['page']['icon_snippet'].' nav-icon-footer-color"></i>&nbsp;'.$_SESSION['page']['name'].'</a></li>';		
} else {
	// on Home page at root of project
	$breadcrumb = '
	<li class="current"><a href="'.$_SESSION['file_directory_path_prefix'].'"><i class="fa fa-home icon-home"></i>&nbsp;Home</a></li>';
}

include($_SESSION['file_directory_path_prefix'].'includes/order_processing_policy.php');
?>

<div id="footerContainer">
	
	<div class="row ">
		
		<div class="small-12 medium-6 columns">
			<ul class="breadcrumbs"><?php echo $breadcrumb; ?></ul>
		</div>
				
		<div class="small-12 medium-6 columns">		
			<?php // include($social_share_filename); ?>
			<a href="https://twitter.com/SupplementCare" title="SupplementRelief on Twitter" target="_blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;
			<a href="https://www.facebook.com/SupplementRelief/" title="SupplementRelief on Facebook" target="_blank"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;
			<a href="https://plus.google.com/b/110226800549100561400/" title="SupplementRelief on Google+" target="_blank"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;
			<a href="https://www.pinterest.com/supplementcare/" title="SupplementRelief on Pinterest" target="_blank"><i class="fa fa-pinterest-p fa-2x" aria-hidden="true"></i></a>	
		</div>
				
	</div>
	
	<div class="row">
		
		<span class="show-for-small-only"><br></span>
		<!-- BBB Logo -->	
		<div class="small-6 medium-2 columns BBBLogo">
			
			<?php echo $_SESSION['application']['bbb_seal_code']; ?>
									
		</div>
		
		<!-- Authorize.net Merchant Seal -->			
		<div class="small-6 medium-4 columns">
					
			<?php echo $_SESSION['application']['authorize_net_seal_code']; ?>
						
		</div>
		
		<!-- Company Information SmartPhones, Tablets, Laptops and Desktops -->
		<div class="small-12 medium-6 columns">
			<div id="copyrightFooter">
				
				<div itemscope itemtype="http://schema.org/PostalAddress">
					<span itemprop="name"><?php echo $_SESSION['application']['copyright_name']; ?></span><br>
					<span itemprop="streetAddress"><?php echo $_SESSION['application']['street_address']; ?></span><br>
					<span itemprop="addressLocality"><?php echo $_SESSION['application']['city']; ?></span>,&nbsp;
				    <span itemprop="addressRegion"><?php echo $_SESSION['application']['state']; ?></span>
				    <span itemprop="postalCode"><?php echo $_SESSION['application']['postal_code']; ?></span>&nbsp;-&nbsp;
				    <span itemprop="addressCountry">United States</span><br><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;
				    <span itemprop="telephone" content="<?php echo $_SESSION['application']['phone']; ?>"><?php echo $_SESSION['application']['phone']; ?></span>
				</div>
				
				<br>
				
				<i class="fa fa-envelope"></i>&nbsp;
								
				<a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>contact/" rel="self" title="Contact <?php echo $_SESSION['application']['copyright_name']; ?>">Contact</a>&nbsp;&nbsp;
				
				<i class="fa fa-user">&nbsp;&nbsp;</i><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>about/" rel="self" title="About <?php echo $_SESSION['application']['copyright_name']; ?>">About</a>&nbsp;&nbsp;
				<i class="fa fa-lock">&nbsp;&nbsp;</i><a href="#" data-reveal-id="footer-privacy-policy" title="Privacy Policy">Privacy</a>&nbsp;&nbsp;
				
				<i class="fa fa-truck">&nbsp;&nbsp;</i><a href="#" data-reveal-id="shippingHandling" title="Order Policy">Order Policy</a>&nbsp;&nbsp;
					
				<i class="fa fa-sitemap">&nbsp;&nbsp;</i><a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>sitemap/" rel="self" title="Sitemap directory of pages on SupplementRelief.com">Sitemap</a>
											
			</div>
								
		</div>
								
	</div>
		
</div>

<div id="footer-privacy-policy" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	
	<h3><i class="fa fa-lock" aria-hidden="true"></i>&nbsp; Privacy Policy</h3>
	
	<?php echo $_SESSION['application']['privacy_policy']; ?>
	
	<?php // echo $_SESSION['application']['bbb_seal_code']; ?>
			    
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
 
</div>