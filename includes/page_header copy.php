<?php
	
// Determine if this is the first page the User is landing on. If so determine what URI (and other User metrics) the User came from before coming to this website to be used for Marketing.

if ( getMarketingReferralURI() ) {	
	// This is currenly already being executed in url-traffic-manager/index.php so can likely be removed.
	createUserSessionLog($connection); // Log what we know about User into the database.	
}
	
if (empty($_SESSION['application']['application_id'])) { $_SESSION['application']['application_id'] = public_application_id; }

if (!$_SESSION['application']['application']) {		
	
	// Get the Application specific data to build the page. This data should persist for the entire user session.

	$queryApplication = 'SELECT 
		ap.name, 
		ap.domain, 
		ap.root_url, 
		ap.root_media_host_url, 
		ap.copyright_year, 
		ap.copyright_name, 
		ap.phone, 
		ap.email, 
		ap.street_address, 
		ap.city, 
		ap.state, 
		ap.postal_code, 
		ap.authorize_net_seal_code, 
		ap.bbb_seal_code, 
		ap.privacy_policy, 
		ap.disclaimer_policy,
		im.host_url as application_navigation_logo_url, 
		im.alt_text AS application_navigation_logo_alt_text    
		FROM applications ap 
		LEFT JOIN images im ON (ap.image_id = im.content_asset_id AND im.size = "Navigation")  
		WHERE ap.id = "'.$_SESSION['application']['application_id'].'"';   
				
	// echo $queryApplication . '<br /><hr />';
	
	$result_application = mysqli_query($connection, $queryApplication);
	
	if (!$result_application) {
		show_mysqli_error_message($queryApplication, $connection);
		die;
	}
	
	while($r = mysqli_fetch_assoc($result_application)) {
					
		// These variable values are used on every page once the session is initiated. They do not change during the session. 
		// They are set as _$_SESSION variables so that the database query does not have to be re-executed for every page.
		
		$_SESSION['application']['navigation_logo_url'] = $r['application_navigation_logo_url'];
		$_SESSION['application']['navigation_logo_alt_text'] = $r['application_navigation_logo_alt_text'];		
		$_SESSION['application']['name'] = $r['name'];
		$_SESSION['application']['domain'] = $r['domain'];
		$_SESSION['application']['root_url'] = $r['root_url'];
		$_SESSION['application']['root_media_host_url'] = $r['root_media_host_url'];
		$_SESSION['application']['copyright_year'] = $r['copyright_year'];
		$_SESSION['application']['copyright_name'] = $r['copyright_name'];
		$_SESSION['application']['phone'] = $r['phone'];
		$_SESSION['application']['email'] = $r['email'];
		$_SESSION['application']['street_address'] = $r['street_address'];
		$_SESSION['application']['city'] = $r['city'];
		$_SESSION['application']['state'] = $r['state'];
		$_SESSION['application']['postal_code'] = $r['postal_code'];
		
		$_SESSION['application']['authorize_net_seal_code'] = $r['authorize_net_seal_code'];
		$_SESSION['application']['bbb_seal_code'] = $r['bbb_seal_code'];
		$_SESSION['application']['privacy_policy'] = $r['privacy_policy'];
		$_SESSION['application']['disclaimer_policy'] = $r['disclaimer_policy'];
		
	}
	
	mysqli_free_result($result_application);
	
	$_SESSION['application']['application'] = true;
	
	if (!$_SESSION['enrollment']['project_id']) { $_SESSION['enrollment']['project_id'] = public_project_id; } 
	
	if (!$_SESSION['enrollment']['project_program_id']) { $_SESSION['enrollment']['project_program_id'] = public_project_program_id; } 

	if (!$_SESSION['enrollment']['user_price_type']) { $_SESSION['enrollment']['user_price_type'] = USER_PRICE_TYPE; }
	
}

// show_array($_SESSION);
			
// Get the Page specific data to build the page.	

$queryPage	= 'SELECT 
	pg.id AS page_id, 
	pg.core,
	pg.geo_location, 
	pg.use_authorization, 
	pg.use_disclaimer, 
	pg.ecommerce AS use_ecommerce, 
	pg.use_font_awesome, 
	pg.use_footer, 
	pg.use_google_recaptcha, 
	pg.use_jquery_ui, 
	pg.use_menu,  
	pg.promotions AS use_promotions, 
	pg.use_system_log, 
	pg.use_title_bar, 
	pg.name,
	pg.relative_url, 
	pg.canonical_url,
	pg.icon_snippet,  
	pg.access_level, 
	pg.styles, 
	pg.content_title AS page_content_title, 
	pg.content_summary AS page_content_summary,  
	pg.content AS page_content, 
	pg.meta_title, 
	pg.meta_description, 
	pg.meta_robots, 
	pg.meta_author,  
	pg.user_help AS user_help_text, 
	ca1.title as user_content_video_title, 
	vi1.host_url_mp4 AS user_content_video_host_url_mp4, 
	vi1.host_url_webm AS user_content_video_host_url_webm, 
	vi1.duration_minutes_seconds AS user_content_video_duration_minutes_seconds, 
	im1.host_url AS user_content_video_poster_image_url, 
	ca2.title as user_help_video_title, 
	vi2.host_url_mp4 AS user_help_video_host_url_mp4, 
	vi2.host_url_webm AS user_help_video_host_url_webm, 
	vi2.duration_minutes_seconds AS user_help_video_duration_minutes_seconds, 
	im2.host_url AS user_help_video_poster_image_url 

	FROM pages pg
	
	LEFT JOIN content_assets ca1 ON pg.content_video_id = ca1.id 
	LEFT JOIN videos vi1 ON ca1.id = vi1.content_asset_id 
	LEFT JOIN images im1 ON (vi1.image_id = im1.content_asset_id AND im1.size = "Video") 
 
	LEFT JOIN content_assets ca2 ON pg.user_help_video_id = ca2.id 
	LEFT JOIN videos vi2 ON ca2.id = vi2.content_asset_id 
	LEFT JOIN images im2 ON (vi2.image_id = im2.content_asset_id AND im2.size = "Video") 
	
	WHERE pg.id = "'.$_SESSION['page']['page_id'].'"';   
			
// echo $queryPage . '<br /><hr />';

$result_page = mysqli_query($connection, $queryPage);

if (!$result_page) {
	show_mysqli_error_message($queryPage, $connection);
	die;
}

while($r = mysqli_fetch_assoc($result_page)) {

	// show_array($r);
	
	// These variables are used in scripts that follow page.header.php:
	// menu.php
	// off_page_canvas_1.php
	// off_page_canvas_2.php
	
	$_SESSION['page']['core'] = $r['core'];
	$_SESSION['page']['ecommerce'] = $r['use_ecommerce'];
	$_SESSION['page']['geo_location'] = $r['geo_location'];
	$_SESSION['page']['promotions'] = $r['use_promotions'];
	
	$_SESSION['page']['use_authorization'] = $r['use_authorization'];
	$_SESSION['page']['use_disclaimer'] = $r['use_disclaimer'];
	$_SESSION['page']['use_font_awesome'] = $r['use_font_awesome'];
	$_SESSION['page']['use_footer'] = $r['use_footer'];
	$_SESSION['page']['use_google_recaptcha'] = $r['use_google_recaptcha'];
	$_SESSION['page']['use_jquery_ui'] = $r['use_jquery_ui'];
	$_SESSION['page']['use_menu'] = $r['use_menu'];
	$_SESSION['page']['use_system_log'] = $r['use_system_log'];
	$_SESSION['page']['use_title_bar'] = $r['use_title_bar'];
			
	$_SESSION['page']['name'] = $r['name'];	
	$_SESSION['page']['icon_snippet'] = $r['icon_snippet'];
	$_SESSION['page']['access_level'] = $r['access_level'];
	
	// Subject Matter text content authored for user display on the page.
	$styles = $r['styles'];	

	$global_page_content_title = $r['page_content_title'];	
	$global_page_content_summary = $r['page_content_summary'];
	$global_page_content = $r['page_content'];
	
	// Subject Matter video content authored for user display on the page.
	$_SESSION['page']['user_content_video_title'] = $r['user_content_video_title'];
	$_SESSION['page']['user_content_video_host_url_mp4'] = $r['user_content_video_host_url_mp4'];
	$_SESSION['page']['user_content_video_host_url_webm'] = $r['user_content_video_host_url_webm'];
	$_SESSION['page']['user_content_video_duration_minutes_seconds'] = $r['user_content_video_duration_minutes_seconds'];
	$_SESSION['page']['user_content_video_poster_image_url'] = $r['user_content_video_poster_image_url'];

	// User help text and video content authored for user display on the page.
	// $_SESSION['page']['user_help_text'] = $r['user_help_text'];
	$global_user_help_text = $r['user_help_text'];
	$_SESSION['page']['user_help_video_title'] = $r['user_help_video_title'];
	$_SESSION['page']['user_help_video_host_url_mp4'] = $r['user_help_video_host_url_mp4'];
	$_SESSION['page']['user_help_video_host_url_webm'] = $r['user_help_video_host_url_webm'];	
	$_SESSION['page']['user_help_video_duration_minutes_seconds'] = $r['user_help_video_duration_minutes_seconds'];	
	$_SESSION['page']['user_help_video_poster_image_url'] = $r['user_help_video_poster_image_url'];			
			
	// if ($_SESSION['user']['user_id'] == 2) {show_array($_SESSION);}
	
	// show_array($_SESSION);
	
	
	// Gets page metadata for the specific database content that is displayed on these reuseable page templates.
	// Pages: Product, Blog, Blog Post, Recipe, Resource, default for all other pages.
	if ($r['page_id'] == 67 OR $r['page_id'] == 98) {
		
		// 67 = Sozo Product page, 98 = SupplementRelief Product page
		// This needs to be a ( binary attribute product TINYINT(1) ) or ( page_type = list "Product" ) in the pages table that is stored in the $_SESSION['page'] array.
		// Apply this concept to all of the other page types below.
		
		// Product page (dynamic content) so get SEO Meta attributes from project_brand_product_usages for the specific Product being viewed.
		// http://supplementrelief.com/product/?p_id=1&bpu_id=291
		
		if (isset($_GET['p_id'])) { $project_id = $_GET['p_id']; }
		if (isset($_GET['bpu_id'])) { $bpu_id = $_GET['bpu_id']; }
		
		$queryProductMetaData = 'SELECT 
		pbpu.page_meta_title, 
		pbpu.page_meta_description,
		pbpu.page_canonical_url, 
		pbpu.page_meta_robots 
		FROM project_brand_product_usages pbpu 
		WHERE pbpu.project_id = '.$project_id.' 
		AND pbpu.brand_product_usage_id = '.$bpu_id.'  
		LIMIT 1';
		
		$result_product_meta_data = mysqli_query($connection, $queryProductMetaData);
		
		if (!$result_product_meta_data) {
			show_mysqli_error_message($queryProductMetaData, $connection);
			die;
		}
		
		while($p = mysqli_fetch_assoc($result_product_meta_data)) {
			
			// show_array($p);
			
			$page_meta_title = $p['page_meta_title'];
			$page_meta_description = strip_tags($p['page_meta_description']);
			$page_canonical_url = $p['page_canonical_url'];			
			$page_meta_robots = $p['page_meta_robots'];
			
		}
		
		mysqli_free_result($result_product_meta_data);		
		
	} elseif ($r['page_id'] == 71 OR $r['page_id'] == 109) {
		
		// Blog page (dynamic content) so get SEO Meta attributes from blogs for the specific Blog Post being viewed.
		// https://supplementrelief.com/show_blog_posts/?blog_id=1299		
		
		if (isset($_GET['blog_id'])) { $blog_id = $_GET['blog_id']; }

		// echo 'On Show Blogs page with Blog ID: '.$blog_id;
		
		$queryBlogMetaData	 = 'SELECT 
		bl.page_meta_title, 
		bl.page_meta_description,
		bl.page_canonical_url, 
		bl.page_meta_robots 
		FROM blogs bl 
		WHERE bl.content_asset_id = '.$blog_id.' 
		LIMIT 1';
		
		$result_blog_meta_data = mysqli_query($connection, $queryBlogMetaData);
		
		if (!$result_blog_meta_data) {
			show_mysqli_error_message($queryBlogMetaData, $connection);
			die;
		}
				
		while($p = mysqli_fetch_assoc($result_blog_meta_data)) {
			
			// show_array($p);
			
			$page_meta_title = $p['page_meta_title'];
			$page_meta_description = strip_tags($p['page_meta_description']);
			$page_canonical_url = $p['page_canonical_url'];
			$page_meta_robots = $p['page_meta_robots'];
			
		}
		
		mysqli_free_result($result_blog_meta_data);
		
	} elseif ($r['page_id'] == 14 OR $r['page_id'] == 106) {
		
		// Blog Post page (dynamic content) so get SEO Meta attributes from blog_posts for the specific Blog Post being viewed.
		// https://supplementrelief.com/show_blog_post/?bp_id=2213		
		
		if (isset($_GET['bp_id'])) { $blog_post_id = $_GET['bp_id']; }

		// echo 'On Show Blog Post page with Blog Post ID: '.$blog_post_id;
		
		$queryBlogPostMetaData	 = 'SELECT 
		bp.page_meta_title, 
		bp.page_meta_description,
		bp.page_canonical_url, 
		bp.page_meta_robots 
		FROM blog_posts bp 
		WHERE bp.content_asset_id = '.$blog_post_id.' 
		LIMIT 1';
		
		$result_blog_post_meta_data = mysqli_query($connection, $queryBlogPostMetaData);
		
		if (!$result_blog_post_meta_data) {
			show_mysqli_error_message($queryBlogPostMetaData, $connection);
			die;
		}
				
		while($p = mysqli_fetch_assoc($result_blog_post_meta_data)) {
			
			// show_array($p);
			
			$page_meta_title = $p['page_meta_title'];
			$page_meta_description = strip_tags($p['page_meta_description']);
			$page_canonical_url = $p['page_canonical_url'];
			$page_meta_robots = $p['page_meta_robots'];
			
		}
		
		mysqli_free_result($result_blog_post_meta_data);

	} elseif ($r['page_id'] == 50 OR $r['page_id'] == 100) {
		
		// Recipe page (dynamic content) so get SEO Meta attributes from blog_posts for the specific Blog Post being viewed.
		// https://supplementrelief.com/recipe/?ppca_id=276&ca_id=1280&re_id=1349		
		
		if (isset($_GET['re_id'])) { $recipe_id = $_GET['re_id']; }

		// echo 'On Recipe page with Recipe ID: '.$recipe_id;
		
		$queryRecipeMetaData = 'SELECT 
		re.page_meta_title, 
		re.page_meta_description,
		re.page_canonical_url, 
		re.page_meta_robots 
		FROM recipes re
		WHERE re.content_asset_id = '.$recipe_id.' 
		LIMIT 1';
		
		$result_recipe_meta_data = mysqli_query($connection, $queryRecipeMetaData);
		
		if (!$result_recipe_meta_data) {
			show_mysqli_error_message($queryRecipeMetaData, $connection);
			die;
		}
				
		while($p = mysqli_fetch_assoc($result_recipe_meta_data)) {
			
			// show_array($p);
			
			$page_meta_title = $p['page_meta_title'];
			$page_meta_description = strip_tags($p['page_meta_description']);
			$page_canonical_url = $p['page_canonical_url'];
			$page_meta_robots = $p['page_meta_robots'];
			
		}
		
		mysqli_free_result($result_recipe_meta_data);							

	} elseif ($r['page_id'] == 16 OR $r['page_id'] == 104 OR $r['page_id'] == 235) {
		
		// Resource page (dynamic content) so get SEO Meta attributes from blog_posts for the specific Blog Post being viewed.
		// https://supplementrelief.com/resource/?ppca_id=330&ca_id=2255		
		
		if (isset($_GET['ca_id'])) { $resource_id = $_GET['ca_id']; }

		// echo 'On Show Blog Post page with Blog Post ID: '.$blog_post_id;
		
		$queryResourceMetaData	 = 'SELECT 
		rk.page_meta_title, 
		rk.page_meta_description,
		rk.page_canonical_url, 
		rk.page_meta_robots 
		FROM resource_kits rk
		WHERE rk.content_asset_id = '.$resource_id.' 
		LIMIT 1';
		
		$result_resource_meta_data = mysqli_query($connection, $queryResourceMetaData);
		
		if (!$result_resource_meta_data) {
			show_mysqli_error_message($queryResourceMetaData, $connection);
			die;
		}
				
		while($p = mysqli_fetch_assoc($result_resource_meta_data)) {
			
			// show_array($p);
			
			$page_meta_title = $p['page_meta_title'];
			$page_meta_description = strip_tags($p['page_meta_description']);
			$page_canonical_url = $p['page_canonical_url'];
			$page_meta_robots = $p['page_meta_robots'];
			
		}
		
		mysqli_free_result($result_resource_meta_data);						
		
	} else {
		
		// Static Page for the Application so get SEO Meta attributes from the Page ID.		
	
		$page_meta_title = $r['meta_title'];
		$page_meta_description = $r['meta_description'];
		$page_canonical_url = $r['canonical_url']; // Jay added 2/6/16.
		$page_meta_robots = $r['meta_robots'];
		
		if ($r['page_id'] == 99) { 
    		// NuMedica Product page.
    		if ($_SERVER['QUERY_STRING']) { $page_meta_robots = 'noindex, follow'; }
    		// Prevent page from indexing (duplicate content) if URL query parameters are present.
		}
			
	}
	
	$page_meta_author = $r['meta_author'];
			
}

// show_array($_SESSION);	

mysqli_free_result($result_page);

// Set the relative path for the Page
if (!$url) { $url = $_SERVER['REQUEST_URI']; }

$parsedURL = explode("/", $url);
// show_array($parsedURL);
// die;
       
if (strlen($parsedURL[1]) > 0) {  
    $_SESSION['file_directory_path_prefix'] = '../';
} else {
	// At root (Home page)
	$_SESSION['file_directory_path_prefix'] = null;
}
 
/*     
if (strlen($parsedURL[2]) > 0) {  
    $_SESSION['file_directory_path_prefix'] = '../';
} else {
	// At root (Home page)
	$_SESSION['file_directory_path_prefix'] = '/';
}
*/

// show_array($_SESSION);

?><!DOCTYPE html>
<html class="no-js <?php echo $ie9?'lt-ie9':'';   ?>" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        
    <!--[if lt IE 9]>
		<script type="text/javascript">
		    window.location = "https://supplementrelief.com/legacy-ie-browser/";
		</script>
		<![endif]-->
 
    <title><?php echo $page_meta_title; ?></title>
    <meta name="description" content="<?php echo $page_meta_description; ?>"> 
    <?php
	    if(isset($page_canonical_url) && strlen($page_canonical_url) > 0) {
		    ?><link rel="canonical" href="<?php echo $page_canonical_url; ?>" /><?php	    
		}
	?>    
    <meta name="robots" content="<?php echo $page_meta_robots; ?>">
	<meta name="author" content="<?php echo $page_meta_author; ?>">
	<!-- Used for Pinterest - project-specific so should be an application database attribute and not hard coded. -->
	<meta name="p:domain_verify" content="382a11e7a755db1ef0c132cae0a56d0a"/>
	<!-- Minimy both of these css files when used in Production -->
    <link rel="stylesheet" href="<?php echo $_SESSION['file_directory_path_prefix']; ?>stylesheets/app.min.css?_version=<?php echo VERSION?>"/>
    <link rel="stylesheet" href="<?php echo $_SESSION['file_directory_path_prefix']; ?>stylesheets/supplementrelief.css?_version=<?php echo VERSION?>" />
    <!-- Slick can potentially be replaced using Foundation Orbit Slider and is not needed on every page so make it a Page attribute. -->   
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/> 
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css"/>  
    <script src="<?php echo FONT_AWESOME_CSS?>"></script>
    <!-- detects which HTML5 and CSS3 features your visitor's browser supports -->
    <script src="<?php echo $_SESSION['file_directory_path_prefix']; ?>bower_components/modernizr/modernizr.js"></script>
    <?php if ($_SESSION['page']['use_google_recaptcha']) { echo '<script src="https://www.google.com/recaptcha/api.js"></script>'; } ?>
	<?php		
    if (public_environment == 'Development') { 
	    // set in /includes/defines.php
	    // Do whatever is needed for Development environment 
    
    } elseif (public_environment == 'Production') { 
      	// Use search engine analytics tracking
	  	include($_SESSION['file_directory_path_prefix'].'includes/analyticstracking.php');
   	                       	    
	}

	if ($styles) {
		// Load the page-specific css
		echo '<style>'.$styles.'</style>';
	}
	
	?>    
  </head>
  
  <body class='antialiased'>
  