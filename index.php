<?php
    
include_once('includes/header.php');

getPage('Your Wellness Lifestyle Concierge', $connection);

if ($_SESSION['page']['use_authorization']) { include_once('includes/authorization.php'); }
	
include_once('includes/page_header.php');

if ($_SESSION['page']['use_menu']) { include_once('includes/menu.php'); }

if ($_SESSION['page']['use_title_bar']) { include_once('includes/off_page_canvas_1.php'); }
	
if (!empty($global_page_content_summary)) { echo $global_page_content_summary; }

if ($_SESSION['page']['promotions'] == 1) { include_once('php/show_project_program_promotions.php'); }

if (!empty($global_page_content)) { echo $global_page_content; }

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->
	 
if ($_SESSION['page']['use_title_bar']) { include_once('includes/off_page_canvas_2.php'); }

// Begin rest of body content for page.

if ($_SESSION['page']['use_footer']) { include_once('includes/page_footer.php'); }

if ($_SESSION['page']['use_disclaimer']) { include_once('includes/disclaimer.php'); }

include_once('includes/foundation_footer.php');

if ($_SESSION['page']['use_system_log']) { include_once('php/system_log_page_access.php'); }

if ($_SESSION['page']['promotions'] == 1) { 
	?>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
	      $(".promotion-slider").show();
		  $('.promotion-slider').slick({
			 autoplay: true,
			 autoplaySpeed: 7000,	
			 dots: true, 
			 slidesToShow: 1,
			 slidesToScroll: 1
		  });		  
		});
	</script>
	<?php
}

?>