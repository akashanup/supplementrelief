$(document).ready(function() {
    var user_id 				=	$('#loggedInUserUserId').val();
    var firstName 				=	$('#loggedInUserFirstName').val();
	firstName 					=	firstName ? firstName : 'Guest';
	var frmSelectingNumGlobal 	= 	'';
	var frmSelectingBtnGlobal 	=	'';

	$('#postCommentBtn').on('click', function(){
		frmSelectingBtnGlobal 	= 	$(this);
		frmSelectingNumGlobal	=	1;
		commentForm(frmSelectingBtnGlobal);
	});

	$(document).on('click', '.replyBtn', function(){		
		frmSelectingBtnGlobal 	= 	$(this);
		frmSelectingNumGlobal	=	2;
		replyForm(frmSelectingBtnGlobal);		
	});

	$(document).on('click','#btnHuman', function(){
		var captcha_input = $('#captcha_input').val();
		if(captcha_input == '')
		{
			$('#captchaFrm').find('.errorSpan').css('display','block');
		}
		else
		{
			data 				=	{};
			data.captcha_input 	=	captcha_input;
			$.ajax({
			    url 	: 	'../php/set_captcha.php',
			    type 	: 	'POST',
			    data 	: 	data,
			    dataType: 	'json',
			    success : 	function(response) {
			    	if(!response.status)
			    	{
			    		$('#captchaFrm').find('.errorSpan').css('display','block');
			    	}
			    	else
			    	{
			    		$('#captchaModal').foundation('reveal', 'close');	
			    		selectedForm(frmSelectingBtnGlobal, frmSelectingNumGlobal);
			    	}
				},
				error 	: 	function(response) {
					$('#captchaModal').foundation('reveal', 'close');
					generateErrorForRequestFailure();
				}
			});
		}
	})

	$(document).on('click','#btnRobot', function(){
		$('#captchaModal').foundation('reveal', 'close');
	})

	$(document).on('keypress paste', 'textarea', function(){
		clearAlertBox();
		clearErrorSpan();
	})

	$(document).on('click', '.moderatorCommentDelete', function(){
		var url 			= 	$(this).attr('data-url');
		$('#deleteBlogModal').find('#deleteBlogModalHeader').html('<p>Are you sure you want to <b>delete</b> this Comment? Select OK to DELETE.</p>');
		$('#deleteBlogModal').find('#deleteBlogYesBtn').attr('data-url', url);
		$('#deleteBlogModal').foundation('reveal','open');
	});

	$(document).on('click', '.headerCommentSpam', function(){
		var url 			= 	$(this).attr('data-url');
		$('#deleteBlogModal').find('#deleteBlogModalHeader').html('<p>Are you sure you want to <b>report</b> this Comment as SPAM? Select OK to REPORT.</p>');
		$('#deleteBlogModal').find('#deleteBlogYesBtn').attr('data-url', url);
		$('#deleteBlogModal').foundation('reveal','open');
	});

	$(document).on('click', '.moderatorReplyDelete', function(){
		var url 			= 	$(this).attr('data-url');
		$('#deleteBlogModal').find('#deleteBlogModalHeader').html('<p>Are you sure you want to <b>delete</b> this Discussion Reply? Select OK to DELETE.</p>');
		$('#deleteBlogModal').find('#deleteBlogYesBtn').attr('data-url', url);
		$('#deleteBlogModal').foundation('reveal','open');
	});

	$(document).on('click', '.headerReplySpam', function(){
		var url 			= 	$(this).attr('data-url');
		$('#deleteBlogModal').find('#deleteBlogModalHeader').html('<p>Are you sure you want to <b>report</b> this Reply as SPAM? Select OK to REPORT.</p>');
		$('#deleteBlogModal').find('#deleteBlogYesBtn').attr('data-url', url);
		$('#deleteBlogModal').foundation('reveal','open');
	});

	$(document).on('click', '#deleteBlogYesBtn', function(){
		var url 			= 	$(this).attr('data-url');
		window.location 	=	url;
	});

	$(document).on('click', '#deleteBlogNoBtn', function(){
		$('#deleteBlogModal').foundation('reveal', 'close');
	});

	$(document).on('click', '.closeAlertDiv', function(){
		$(this).parent('div').parent('div').parent('div').html('');
	});

	$(document).on('change', '.fileInput', function(){
		var thisFrm 		= 	$(this).closest('form');
		var switchValue 	=	parseInt($(this).attr('data-key'));
		showFileName($(this));
		disableOtherInputs(thisFrm, switchValue);
	});

	$(document).on('click', '#resetCodeBtn', function(){
		$('#imgCaptcha').attr('src','../../includes/captcha.php?_'+Math.random());
	});

	$(document).on('click', '.fileInputAnchor', function(){
		var fileInput 	=	$(this).siblings();
		fileInput.click();
	});

	$(document).on('click', '.resetBtn', function(){
		var thisBtn 	= 	$(this);
		resetFileInput(thisBtn);
	});

	var selectedForm 		=	function(thisBtn, switchValue)
	{
		switch(switchValue)
		{
			case 1:
				commentForm(thisBtn);
				break;
			case 2:
				replyForm(thisBtn);
				break;
		}
	}

	var commentForm 		=	function(thisBtn)
	{
		clearErrorSpan();
		clearAlertBox();
		if(isFormValid($('#commentForm')))
		{
			submitCommentForm(thisBtn);
		}
		else
		{
			$('#commentForm').find('.errorSpan').css('display','block');
		}
	}

	var replyForm 	 		=	function(thisBtn)
	{
		var thisFrm     =   thisBtn.closest('form');
		clearErrorSpan();
		clearAlertBox();
		if(isFormValid(thisFrm))
		{
			submitReplyForm(thisBtn, thisFrm);
		}
		else
		{
			thisFrm.find('.errorSpan').css('display','block');	
		}
	}	

	var submitCommentForm   =	function(thisBtn)
	{
		thisBtn.html('Post <i class="fa fa-refresh fa-spin fa-fw"></i>');
		thisBtn.attr('disabled',true);
		var form 		=	$('#commentForm');
		var ie 			= 	checkForIEVersion();
		if( ie == 9 )
		{
			$.ajax({
			    url: '../php/discussion_forum_crud.php',
			    type: 'POST',
			    dataType:'json',
			    success : 	function(response) {
					thisBtn.html('Post');
					thisBtn.attr('disabled',false);
			    	if(response.captcha_status == 0)
			    	{
			    		$('#imgCaptcha').attr('src','../../includes/captcha.php?_'+Math.random());
		    			$('#captchaModal').foundation('reveal', 'open');
			    	}
			    	else
			    	{
						form.submit();    	
					}
				},
				error 	: 	function(response) {
					thisBtn.html('Post');
					thisBtn.attr('disabled',false);
					generateErrorForRequestFailure();
				}
			});
		}
		else
		{	
			var formData 	= 	new FormData();
			formData.append('action', 'new_thread');
			formData.append('forum_type', $("#forum_type").val());
			formData.append('content_assest_id', $("#content_assest_id").val());
			// formData.append('use_google_recaptcha', '1');
			formData.append('comment_user_id', $("#comment_user_id").val());
			formData.append('comment_note', $('#comment_note').val());
			formData.append('comment_text', $('#comment_text').val());

			if(form.attr('data-id') == 'education')
			{
				formData.append('comment_title', $('#comment_title').val());
			}

			var documents 	=	form.find('.document_file');
			var images 		=	form.find('.image_file');
			var videos 		= 	form.find('.video_file');
			var audios 		= 	form.find('.audio_file');

			if(documents.val())
			{
				var file 	=	documents[0].files[0];
				var type 	=	'document';
				if(!validFile(file, documents, $('#errorResponse'), 0))
				{
					thisBtn.html('Post');
					thisBtn.attr('disabled',false);
					return;
				}
				formData.append('document_file', type);
			}
			else if(images.val())
			{	
				var file 	=	images[0].files[0];
				var type 	=	'image';
				if(!validFile(file, images, $('#errorResponse'), 1))
				{
					thisBtn.html('Post');
					thisBtn.attr('disabled',false);
					return;
				}
				formData.append('image_file', type);
			}
			else if(videos.val())
			{
				var file 	=	videos[0].files[0];
				var type 	=	'video';
				if(!validFile(file, videos, $('#errorResponse'), 2))
				{
					thisBtn.html('Post');
					thisBtn.attr('disabled',false);
					return;
				}
				formData.append('video_file', type);
			}
			else if(audios.val())
			{	
				var file 	=	audios[0].files[0];
				var type 	=	'audio';
				if(!validFile(file, audios, $('#errorResponse'), 3))
				{
					thisBtn.html('Post');
					thisBtn.attr('disabled',false);
					return;
				}
				formData.append('audio_file', type);
			}
			
			//formData.append('g-recaptcha-response', grecaptcha.getResponse());

			// alert(formData);

			$.ajax({
			    url: '../php/discussion_forum_crud.php',
			    data: formData,
			    type: 'POST',
			    // THIS MUST BE DONE FOR FILE UPLOADING
			    contentType: false,
			    processData: false,
			    dataType: 	'json',
			    success : 	function(response) {
					thisBtn.html('Post');
					thisBtn.attr('disabled',false);
			    	if(response.captcha_status == 0)
			    	{
			    		$('#imgCaptcha').attr('src','../../includes/captcha.php?_'+Math.random());
		    			$('#captchaModal').foundation('reveal', 'open');
			    	}
			    	else
			    	{
			    		resetFileInput(thisBtn);
				    	var makeResponse = generateResponse(response);
						//grecaptcha.reset();
				    	if(response.error)
				    	{
				    		$('#errorResponse').html(makeResponse);
				    	}
				    	else
				    	{
				    		var tempId	=	Math.floor(Math.random() * (9999999 - 100)) + 100;	
							$('#discussResponse').html(makeResponse);
							var makeBlog = generateBlog(tempId, response);
							$('#discussBlogResponse').prepend(makeBlog);
							$("#commentForm").trigger('reset');
							//$(document).foundation('abide', 'reflow');
							$('html, body').animate({
						        scrollTop: $("#postCommentBtn").offset().top
						    }, 1000);
						    if(typeof(file)!='undefined')
						    {
						    	uploadFile(tempId, file, type,  $('#errorResponse'), response.id, 'discussion_threads');
						    }
						}
					}
				},
				error 	: 	function(response) {
					thisBtn.html('Post');
					thisBtn.attr('disabled',false);
					resetFileInput(thisBtn);
					generateErrorForRequestFailure();
				}
			});
		}
	}

	var submitReplyForm 	=	function(thisBtn, thisFrm)
	{	
		thisBtn.html('Reply <i class="fa fa-refresh fa-spin fa-fw"></i>');
		thisBtn.attr('disabled',true);
		var ie 	=	checkForIEVersion();
		if( ie == 9 )
		{
			thisFrm.submit();
		}		
		else
		{
			var update 		= 	thisFrm.parent("fieldset");
			var replyDiv 	= 	update;//.parent("div");
			var formData 	= 	new FormData();
			formData.append('action', 'new_reply');
			formData.append('forum_type', $("#forum_type").val());
			formData.append('comment_reply_id', thisFrm.find('.comment_reply_id').val());
			formData.append('comment_reply_user_id', thisFrm.find('.comment_reply_user_id').val());
			formData.append('comment_note', thisFrm.find('.comment_note').val());
			var documents 	=	thisFrm.find('.document_file');
			var images 		=	thisFrm.find('.image_file');
			var videos 		= 	thisFrm.find('.video_file');
			var audios 		= 	thisFrm.find('.audio_file');
			if(documents.val())
			{
				var file 	=	documents[0].files[0];
				var type 	=	'document';
				if(!validFile(file, documents, replyDiv, 0))
				{
					thisBtn.html('Reply');
					thisBtn.attr('disabled',false);
					return;
				}
				formData.append('document_file', type);
			}
			else if(images.val())
			{	
				var file 	=	images[0].files[0];
				var type 	=	'image';
				if(!validFile(file, images, replyDiv, 1))
				{
					thisBtn.html('Reply');
					thisBtn.attr('disabled',false);
					return;
				}
				formData.append('image_file', type);
			}
			else if(videos.val())
			{
				var file 	=	videos[0].files[0];
				var type 	=	'video';
				if(!validFile(file, videos, replyDiv, 2))
				{
					thisBtn.html('Reply');
					thisBtn.attr('disabled',false);
					return;
				}
				formData.append('video_file', type);
			}
			else if(audios.val())
			{
				var file 	=	audios[0].files[0];
				var type 	=	'audio';
				if(!validFile(file, audios, replyDiv, 3))
				{
					thisBtn.html('Reply');
					thisBtn.attr('disabled',false);
					return;
				}
				formData.append('audio_file', type);
			}

			formData.append('comment_reply_text', thisFrm.find('.comment_reply_text').val());
			console.log(formData);
			$.ajax({
			    url: '../php/discussion_forum_crud.php',
			    data: formData,
			    type: 'POST',
			    // THIS MUST BE DONE FOR FILE UPLOADING
			    contentType: false,
			    processData: false,
			    dataType: 	'json',
			    success : 	function(response) {
				    thisBtn.html('Reply');
				    thisBtn.attr('disabled',false);
			    	if(response.captcha_status == 0)
			    	{
			    		$('#imgCaptcha').attr('src','../../includes/captcha.php?_'+Math.random());
		    			$('#captchaModal').foundation('reveal', 'open');
			    	}
			    	else
			    	{
					    resetFileInput(thisBtn);
			    		var tempId			=	Math.floor(Math.random() * (9999999 - 100)) + 100;	
						var makeResponse 	= 	generateResponse(response);
						if(response.error)
						{
							update.before('<hr>' + makeResponse);	
						}
						else
						{
							var makeReply 	 = 	generateReply(tempId, response);
							update.before('<hr>' + makeResponse + makeReply);
						}

						thisFrm.trigger('reset');
						if(typeof(file)!='undefined')
					    {
					    	uploadFile(tempId, file, type, replyDiv, response.id, 'discussion_replies');
					    }
					}

					/*$('html, body').animate({
				        scrollTop: thisBtn.offset().top
				    }, 1000);*/
				},
				error 	: 	function(response) {
					thisBtn.html('Reply');
				    thisBtn.attr('disabled',false);
				    resetFileInput(thisBtn);
					generateErrorForRequestFailure();
				}
			});
		}
	}

	//tempId is used to catch the that blog which is created dynamically by javascript
	var uploadFile 			=	function(tempId, file, type, errorDiv, postId, tableName)
	{
		var thisDiv 		=	$(document).find('#'+tempId);
		var progressBar		=	thisDiv.find('.progressBar');
		
		var formData 		= 	new FormData();
		formData.append('file', file);
		formData.append('type', type);
		formData.append('postId', postId);
		formData.append('tableName', tableName);

		$.ajax({
			xhr: function() {
		        var xhr = new window.XMLHttpRequest();
		        xhr.upload.addEventListener("progress", function(evt) {
		            if (evt.lengthComputable) {
		                var percentComplete = (evt.loaded / evt.total)*100;
		                percentComplete 	=	Math.ceil(percentComplete) + '%';
		                //Do something with upload progress here
		                progressBar.find('.meter').css('width',percentComplete);
		            }
		       }, false);
		       return xhr;
		    },
		    url: '../php/background_file_upload.php',
		    data: formData,
		    type: 'POST',
		    // THIS MUST BE DONE FOR FILE UPLOADING
		    contentType: false,
		    processData: false,
		    dataType: 	'json',
		    success : 	function(response) {
		    	progressBar.remove();
		    	if(response.upload == 1)
		    	{
		    		showUploadedFile(thisDiv, type, response.file_url);
		    	}
		    	else
		    	{
					var makeResponse 		= 	generateResponse(response);
					errorDiv.before(makeResponse);
		    		thisDiv.find('.fileImage').html('<img src="../img/uploadfailed.jpg">');
		    	}
		    	//Add progress bars
			},
			error 	: 	function(response) {
				progressBar.remove();
				thisDiv.find('.commentBodyImage').html('<img src="../img/uploadfailed.jpg">');
			}
		});
	}

	var showUploadedFile 	=	function(thisDiv, switchValue, file_url)
	{
		var html 			=	'';
		
		switch(switchValue)
		{
			case 'video':
				html 	= 	'<img src="../img/MediaWillBeAvailableSoon.jpg">';
				break;
			case 'audio':
				html	=	'<audio controls>'										+
							  '<source src="'+file_url+'" type="audio/mpeg">'		+
							  'Your browser does not support the audio element.'	+
							'</audio>';
				break;
			case 'image':
				html 	=	'<img src="'+ file_url +'">';
				break;
			case 'document':
				html 	=	'<a href="'+file_url+'" target="_blank" title="Preview document">'						+
	                    		'<img src="https://cdn-manager.net/media/images/document.jpg" alt="" width="150"/>'	+
	                    	'</a>';
				break;
		}
		var uploadDiv			=	thisDiv.find('.fileImage');
		uploadDiv.html(html);
	}


	var generateResponse 	=	function(response)
	{
		var html =	'';
		html	+= 	'<div class="row sessionMessage">'								+
						'<div class="small-12 columns">'							+
							'<div data-alert class="'+response.message_type+'">'	+
								response.message 									+
								'<button tabindex="0" class="close closeAlertDiv" aria-label="Close Alert">&times;</button>'						+
			  				'</div>'												+
						'</div>'													+
					'</div>';
		return html;
	}

	var generateBlog 		=	function(tempId, response)
	{
		var comment_moderator 	=	'';
		var commentHeader		=	'';
		var html =	'';
		html	+= 	'<div class="row" id="'+tempId+'">'					+
                    	'<div class="small 12-columns">'				+
                        	'<div class="blogPostCommentContainer">';
                		if($('#discussion_forum_moderator').val())
                		{
                			comment_moderator 	+=	'<span class="label">Moderator</span>&nbsp;'+
            										'<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Remove this Comment and all related Replies?">'							+
            											'<a href="#fndtn-discuss" data-url="../php/delete_discussion_thread.php?type=thread&dt_id='+response.id+'" class="moderatorCommentDelete" >'	   +
            											'<i class="fa fa-remove icon-red"></i>'							   +
            											'</a>'															   +
        											'</span><br />';
                		}
	                		commentHeader 		+=	'<div class="commentHeader">'										   +
	            										'<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Person who posted this Comment"><i class="fa fa-user icon-red"></i></span>&nbsp;'+firstName+'&nbsp; '				  +
	            										'<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Post Timestamp"><i class="fa fa-clock-o icon-red"></i></span> '+response.created_timestamp+'&nbsp;'								   +
	            										'<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Report Comment as SPAM"><a href="#fndtn-discuss" data-url="../php/report_discussion_forum_spam.php?forum_type='+response.forum_type+'&comment_type=comment&comment_id='+response.id+'" class="headerCommentSpam" ><i class="fa fa-ban icon-red"></i></a></span>&nbsp;'	  				  +
	            										comment_moderator 						+
	        										'</div>'									+
        											'<div id="threadPostHeaderTitle">'			+
            											'<h4>'+response.title+'</h4>'			+
        											'</div>';
						if(response.file_document_url)
						{
							html 				+=	'<div id="comment'+response.id+'" class="row collapse">'			+
										                '<div class="small-12 medium-8 columns">'						+
										                    '<div class="blogPostComment">'								+
										                    	commentHeader 											+ 
										                        '<div class="commentBodyText">'							+
										                        	'<p>'+response.thread_text+'</p>'					+
									                        	'</div>'												+
										                    '</div>'													+
										                '</div>'														+
										                '<div class="small-12 medium-4 columns">'               		+           
										                    '<div class="commentBodyImage">'							+
										                    	'<div class="row progressBar">'							+
										                 			'<div class="progress large-9 success round">'		+
																      '<span class="meter" style="width:0%"></span>'	+
																    '</div>'											+
										                 		'</div>'												+
										                 		'<div class="row fileImage">'							+
										                 			'<i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>'+
										                 		'</div>'												+
									                    	'</div>'													+
										                '</div>'														+
										            '</div>';
						}	

						else if(response.file_image_url)
						{
							html 				+=	'<div id="comment'+response.id+'" class="row collapse">'			+
										                '<div class="small-12 medium-8 columns">'						+
										                    '<div class="blogPostComment">'								+
										                    	commentHeader 											+ 
										                        '<div class="commentBodyText">'							+
										                        	'<p>'+response.thread_text+'</p>'					+
									                        	'</div>'												+
										                    '</div>'													+
										                '</div>'														+
										                '<div class="small-12 medium-4 columns">'               		+
										                    '<div class="commentBodyImage">'							+
									                    		'<div class="row progressBar">'							+
										                 			'<div class="progress large-9 success round">'		+
																      '<span class="meter" style="width:0%"></span>'	+
																    '</div>'											+
										                 		'</div>'												+
										                 		'<div class="row fileImage">'							+
										                 			'<i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>'+
										                 		'</div>'												+
									                    	'</div>'													+
										                '</div>'														+
										            '</div>';
						}

						else if(response.file_video_url)
						{
							html 				+=	'<div id="comment'+response.id+'" class="row collapse">'			+
										                '<div class="small-12 medium-8 columns">'						+
										                    '<div class="blogPostComment">'								+
										                    	commentHeader 											+ 
										                        '<div class="commentBodyText">'							+
										                        	'<p>'+response.thread_text+'</p>'					+
									                        	'</div>'												+
										                    '</div>'													+
										                '</div>'														+
										                '<div class="small-12 medium-4 columns">'               		+
										                 	'<div class="commentBodyVideo">'							+
										                 		'<div class="row progressBar">'							+
										                 			'<div class="progress large-9 success round">'		+
																      '<span class="meter" style="width:0%"></span>'	+
																    '</div>'											+
										                 		'</div>'												+
										                 		'<div class="row fileImage">'							+
										                 			'<i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>'+
										                 		'</div>'												+
										                    '</div>'													+
										                '</div>'														+
										            '</div>';
						}

						else if(response.file_audio_url)
						{
							html 				+=	'<div id="comment'+response.id+'" class="row collapse">'			+
										                '<div class="small-12 medium-8 columns">'						+
										                    '<div class="blogPostComment">'								+
										                    	commentHeader 											+ 
										                        '<div class="commentBodyText">'							+
										                        	'<p>'+response.thread_text+'</p>'					+
									                        	'</div>'												+
										                    '</div>'													+
										                '</div>'														+
										                '<div class="small-12 medium-4 columns">'               		+
										                 	'<div class="commentBodyAudio">'							+
										                 		'<div class="row progressBar">'							+
										                 			'<div class="progress large-9 success round">'		+
																      '<span class="meter" style="width:0%"></span>'	+
																    '</div>'											+
										                 		'</div>'												+
										                 		'<div class="row fileImage">'							+
										                 			'<i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>'+
										                 		'</div>'												+
										                    '</div>'													+
										                '</div>'														+
										            '</div>';
						}				

						else
						{
							html 				+=	'<div id="comment'+response.id+'" class="row collapse">'	+
										                '<div class="small-12 columns">'						+
										                    '<div class="blogPostComment">'						+
										                    	commentHeader 									+ 
										                        '<div class="commentBodyText">'					+
										                        	'<p>'+response.thread_text+'</p>'			+
									                        	'</div>'										+
										                    '</div>'											+
										                '</div>'												+
										            '</div>';
						}

						if($('#discussion_forum_reply_post').val())
						{
							html 				+=	'<div class="discussionReplyForm">'										+
										                '<fieldset>'														+
											                '<legend>Post Reply</legend>'									+
											                '<form autocomplete="off" class="replyForm">'		+
											                    '<input type="hidden" name="action" value="new_reply"/>'	+
											                    '<input type="hidden" name="forum_type" value="'+response.forum_type+'"/>'+
											                    '<input type="hidden" name="comment_reply_id" class="comment_reply_id" value="'+response.id+'"/>'  	  +
											                    '<input type="hidden" name="comment_reply_user_id" class="comment_reply_user_id" value="'+user_id+'"/> '   +
											                    '<input type="text" name="comment_note" class="comment_note" value=""/>'											 +
											                    '<div class="row collapse">'								+
											                      	'<div class="small-12 medium-8 columns">'				+
											                        	'<textarea required name="comment_reply_text" class="comment_reply_text" style="width: 100%" placeholder="Reply..."  />'+response.thread_reply_text+'</textarea>'				  +
											                        	'<span class="error errorSpan" style="display: none">Reply is required</span>'	+
											                      	'</div>'												+
											                        '<div class="small-12 medium-4 columns">'				+
											                        	'<button type="button" class="replyBtn button small radius" >Reply</button>&nbsp;'					+
											                        	'<input type="reset" class="button small radius resetBtn" value="Reset" >'+
											                       '</div>'													+
											                   '</div>'														+
											                   '<div class="row collapse">'									+
											                        '<div class="small-12 columns">';

											                            if ($('#discussion_forum_reply_post_audio').val())
											                            {   
	                            											html +=
	                            			 									'<div class="small-2 medium-2 large-1 columns">'					+
												                                	'<div class="right">'											+
												                                		'<a href="#fndtn-discuss" data-toggle="tooltip" title="Attach Audio file" class="fileInputAnchor audioAnchor">'						  					 +
																						'<i class="fa fa-file-audio-o fa-2x" aria-hidden="true"></i>'+
																						'</a>'														+
																						'<input type="file" data-key="3" name="audio_file" class="audio_file fileInput" style="display: none">'	   +
											                                		'</div>'														+
												                           		'</div>';      
											                            }

											                            if ($('#discussion_forum_reply_post_document').val())
											                            {    
																			html +=
																				'<div class="small-2 medium-2 large-1 columns">'					+
												                                	'<div class="right">'											+
												                                		'<a href="#fndtn-discuss" data-toggle="tooltip" title="Attach Document file" class="fileInputAnchor documentAnchor">'					  					+
												                                			'<i class="fa fa-file-text-o fa-2x" aria-hidden="true"></i>'												 +
											                                			'</a>'														+
											                                			'<input type="file" data-key="0" name="document_file" class="document_file fileInput" style="display: none">' +
											                                		'</div>'														+
												                           		'</div>';
											                            }                                   
													                                                         
											                            if ($('#discussion_forum_reply_post_image').val())
											                            {   
																			html += 									
																				'<div class="small-2 medium-2 large-1 columns">'					+
												                                	'<div class="right">'											+
													                                	'<a href="#fndtn-discuss" data-toggle="tooltip" title="Attach Image file" class="fileInputAnchor imageAnchor">'				 						     +
													                                		'<i class="fa fa-file-image-o fa-2x" aria-hidden="true"></i>'												 +
												                                		'</a>'														+
											                                			'<input type="file" data-key="1" name="image_file" class="image_file fileInput" style="display: none">'	   +
											                                		'</div>'														+
												                           		'</div>';      
											                            }
													                             
											                            if ($('#discussion_forum_reply_post_video').val())
											                            {   
	                            											html += 									
	                            												'<div class="small-2 medium-2 large-1 columns">'    				+
												                                	'<div class="right">'											+
												                                		'<a href="#fndtn-discuss" data-toggle="tooltip" title="Attach Video file" class="fileInputAnchor videoAnchor">'						  					 +
												                                			'<i class="fa fa-file-video-o fa-2x" aria-hidden="true"></i>'												 +
											                                			'</a>'														+
											                                			'<input type="file" data-key="2" name="video_file" class="video_file fileInput" style="display: none">'	   +
											                                		'</div>'														+
												                           		'</div>';      
											                            }										                           									                         
																			html +=	
																		'<div class="small-1 medium-1 large-1 end">'+
																		'</div>'		          				+
											                        '</div>'									+
											                        '<div class="row uploadedFileName">'		+
											                        '</div>'									+
									                           	'</div>'										+
											                '</form>'											+
										                '</fieldset>'											+
										            '</div>';
						}

		html 	+=			'</div>'																			+
						'</div>'																				+
					'</div>';
		return html;
	}

	var generateReply 		=	function(tempId, response)
	{
		var comment_reply_moderator 	=	'';
		var commentHeader 				=	'';
		var html 						=	'';
		if ($('#discussion_forum_moderator').val()) 
		{
        	comment_reply_moderator = '&nbsp;&nbsp;<span class="label">Moderator</span>&nbsp;<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Remove this Comment Reply?"><a href="#fndtn-discuss"  data-url="../php/delete_discussion_thread.php?type=reply&dr_id='+response.id+'" class="moderatorReplyDelete"><i class="fa fa-remove icon-red"></i></a></span><br />';       
        } 
        else 
        {
            comment_reply_moderator = '';
        }

		commentHeader 	+=	'<div class="commentHeader">'			+
          						'<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Person who posted this Comment Reply"><i class="fa fa-user icon-red"></i></span>&nbsp;'+firstName+'&nbsp'	+
      							'<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Date/Time this Comment Reply was posted">&nbsp;<i class="fa fa-clock-o icon-red"></i></span> '+response.created_timestamp+'&nbsp;'+
      							'<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Report Reply as SPAM"><a href="#fndtn-discuss" data-url="../php/report_discussion_forum_spam.php?forum_type='+response.forum_type+'&comment_type=reply&comment_id='+response.id+'" class="headerReplySpam"><i class="fa fa-ban icon-red"></i></a></span>&nbsp;'		+
          						comment_reply_moderator	+
        					'</div>';

		html 	+=	'<div class="row" id="'+tempId+'">'														+ 
                		'<div id="reply'+response.id+'" class="small-12 columns">'							+ 
                    		'<div class="topicCommentReply">';     
                  
                        if(response.file_document_url) 
                        {		
							html 	+=	'<div id="comment'+response.id+'" class="row collapse">'			+
											'<div class="small-12 medium-8 columns">'						+
												'<div class="topicReply">'									+
													commentHeader											+
													'<div class="commentBodyText">'							+
														'<p>'+response.thread_reply_text+'</p>'				+
													'</div>'												+	
												'</div>'													+
											'</div>'														+
											'<div class="small-12 medium-4 columns">'						+
												'<div class="topicReplyBodyImage">'							+	
													'<div class="row progressBar">'							+
							                 			'<div class="progress large-9 success round">'		+
													      '<span class="meter" style="width:0%"></span>'	+
													    '</div>'											+
							                 		'</div>'												+
							                 		'<div class="row fileImage">'							+
							                 			'<i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>'+
							                 		'</div>'												+
												'</div>'													+
											'</div>'														+
										'</div>';
							
						} 
						else if (response.file_image_url) 
						{
                         
                            html 	+=	'<div class="row collapse">'										+
			                            	'<div class="small-12 medium-8 columns">'						+
                                    			'<div class="topicReply">'									+
			                                        commentHeader											+
                                        			'<div class="commentBodyText">'							+
                                        				'<p>'+response.thread_reply_text+'</p>'				+
                                    				'</div>'												+
                                    			'</div>'													+
                                			'</div>'														+
			                                '<div class="small-12 medium-4 columns">'						+
			                                    '<div class="topicReplyBodyImage">'							+
			                                    	'<div class="row progressBar">'							+
							                 			'<div class="progress large-9 success round">'		+
													      '<span class="meter" style="width:0%"></span>'	+
													    '</div>'											+
							                 		'</div>'												+
							                 		'<div class="row fileImage">'							+
							                 			'<i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>'+
							                 		'</div>'												+
		                                    	'</div>'													+
			                                '</div>'														+
			                            '</div>';
                            
                        } 
                        else if (response.file_video_url) 
                        {  
	                           
				            html 	+=	'<div id="comment'+response.id+'" class="row collapse">'    		+
				                			'<div class="small-12 medium-8 columns">'						+               
				                    			'<div class="topicReply">'									+
				                        			commentHeader			   								+
							                        '<div class="commentBodyText">'							+
							                        '<p>'+response.thread_reply_text+'</p>'					+
							                        '</div>'												+
				                    			'</div>'													+
				                			'</div>'														+
				                			'<div class="small-12 medium-4 columns">'						+                   
				                    			'<div class="commentBodyVideo">'							+
					                    			'<div class="row progressBar">'							+
							                 			'<div class="progress large-9 success round">'		+
													      '<span class="meter" style="width:0%"></span>'	+
													    '</div>'											+
							                 		'</div>'												+
							                 		'<div class="row fileImage">'							+
							                 			'<i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>'+
							                 		'</div>'												+
				                    			'</div>'													+
			                				'</div>'														+
				            			'</div>';
                   
                        } 
                        else if (response.file_audio_url) 
                        {  
	                           
				            html 	+=	'<div id="comment'+response.id+'" class="row collapse">'    		+
				                			'<div class="small-12 medium-8 columns">'						+         
				                    			'<div class="topicReply">'									+
				                        			commentHeader			   								+
							                        '<div class="commentBodyText">'							+
							                        '<p>'+response.thread_reply_text+'</p>'					+
							                        '</div>'												+
				                    			'</div>'													+
				                			'</div>'														+
				                			'<div class="small-12 medium-4 columns">'						+                   
				                    			'<div class="commentBodyAudio">'							+
							                 		'<div class="row progressBar">'							+
							                 			'<div class="progress large-9 success round">'		+
													      '<span class="meter" style="width:0%"></span>'	+
													    '</div>'											+
							                 		'</div>'												+
							                 		'<div class="row fileImage">'							+
							                 			'<i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>'+
							                 		'</div>'												+
				                    			'</div>'											+
			                				'</div>'												+
				            			'</div>';
                   
                        } 
                        else 
                        {
                         
                            html 	+=	'<div class="row">'							+
                                			'<div class="small-12 columns">'		+
                                    			'<div class="topicReply">'			+     
                                        			commentHeader					+
                                        			'<div class="commentBodyText">'	+
                                        			'<p>'+response.thread_reply_text+'</p>' +
                                        			'</div>'						+
			                                    '</div>'							+
			                                '</div>'								+
			                            '</div>';
                             
                        }   
                         
        html  	+=			'</div>'	+
                		'</div>'		+
            		'</div>';
        return html;
	}

	var clearAlertBox 		=	function()
	{
		$('#discussResponse').html('');
		$(document).find('.sessionMessage').html('');
		$('#errorResponse').html('');
	}

	var isFormValid 		= 	function(thisFrm)
	{
		var textarea 		=	thisFrm.find('textarea').val();
		return textarea 	==  '' ? false : true;
	}

	var validFile 			=	function(thisFile, thisFileUpload, errorDiv, switchValue)
	{
		var thisSize 		=	thisFile.size;
		var thisType 		=	thisFile.type;
		var thisName 		=	thisFile.name;
		thisName 			=	thisName.toLowerCase();
		var thisExtnsn 		=	thisName.substr((thisName.lastIndexOf('.')+1));

		var fileSize 		=	getSize(switchValue);
		var fileType 		=	getType(switchValue);
		var check 			= 	validFileType(thisType, fileType);
		
		if(thisSize > fileSize)
		{
			var response 			=	getSizeErrorData(switchValue, thisSize)
			var makeResponse 		= 	generateResponse(response);
			errorDiv.before(makeResponse);
			return false;
		}
		else if(!validFileType(thisExtnsn, fileType))
		{
			var response 			=	getTypeErrorData(switchValue, thisExtnsn)
			var makeResponse 		= 	generateResponse(response);
			errorDiv.before(makeResponse);
			return false;
		}

		return true;
	}

	//checking for valid extension
	var validFileType 			=	function(thisExtnsn, fileType)
	{
		return	$.inArray(thisExtnsn,fileType) != -1 ? true : false;
	}
 
 	//valid sizes
	var getSize 				=	function(switchValue)
	{
		var thisSize 			=	'';
		switch(switchValue)
		{
			case 0:
				thisSize 		=	$('#max_document_size').val();
				break;
			case 1:
				thisSize 		=	$('#max_image_size').val();
				break;
			case 2:
				thisSize 		=	$('#max_video_size').val();
				break;
			case 3:
				thisSize 		=	$('#max_audio_size').val();
				break;
		}
		return thisSize;
	}

	//valid extensions
	var getType 				=	function(switchValue)
	{
		var thisType 			=	[];
		switch(switchValue)
		{
			case 0:
				thisType 		=	['txt', 'pdf', 'doc', 'docx', 'odt', 'rtf', 'htm', 'html', 'ods', 'xls', 'xlsx', 'csv', 'key', 'numbers', 'pages', 'odp', 'pps', 'ppt', 'pptx'];
				break;
			case 1:
				thisType 		=	['jpg', 'jpeg', 'png', 'gif'];
				break;
			case 2:
				thisType 		=	['mp4', 'webm', 'mkv', 'avi', 'flv', 'mov',  'wmv', 'm4v', 'mpeg'];
				break;
			case 3:
				thisType 		=	['mp3', 'mpeg'];
				break;
		}
		return thisType;
	}

	var getTypeErrorData		=	function(switchValue, thisType)
	{
		var response 			= 	{};
		var message_type 		=	'alert-box warning radius';
		var message 			=	'';
		switch(switchValue)
		{
			case 0:
				message 		=	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The file type you submitted was <b>'+thisType+'</b>. Please choose a Document with a supported File Type.</p><p>The <strong>Document File Type</strong> must be <b>csv</b>, <b>doc</b>, <b>docx</b>, <b>htm</b>, <b>html</b>, <b>key</b>, <b>numbers</b>, <b>odp</b>, <b>ods</b>, <b>odt</b>, <b>pages</b>, <b>pdf</b>, <b>pps</b>, <b>ppt</b>, <b>pptx</b>, <b>rtf</b>, <b>xls</b> or <b>xlsx</b>.</p>';
				break;
			case 1:
				message 		=	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The file type you submitted was <b>'+thisType+'</b>. Please choose an Image with a supported File Type.</p><p>The <strong>Image File Type</strong> must be <b>gif</b>, <b>jpg</b>, <b>jpeg</b> or <b>png</b>.</p>';
				break;
			case 2:
			message 			=	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The file type you submitted was <b>'+thisType+'</b>. Please choose a Video with a supported File Type.</p><p>The <strong>Video File Type</strong> must be <b>avi</b>, <b>flv</b>, <b>m4v</b>, <b>mkv</b>, <b>mov</b>, <b>mp4</b>, <b>mpeg</b>, <b>webm</b> or <b>wmv</b>.</p>';
				break;
			case 3:
			message 			=	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The file type you submitted was <b>'+thisType+'</b>. Please choose an Audio with a supported File Type.</p><p>The <strong>Audio File Type</strong> must be <b>mp3</b>.</p>';
				break;
		}

		response.message_type 	=	message_type;
		response.message 		=	message;
		return response;
	}

	var getSizeErrorData		=	function(switchValue, thisSize)
	{
		var fileSize			=	getSize(switchValue);
		
		var response 			= 	{};
		var message_type 		=	'alert-box warning radius';
		var message 			=	'';
		
		switch(switchValue)
		{
			case 0:
				message 		=	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Document File Size</strong> must be smaller then <b>'+fileSize+'</b> bytes. The size of the file you submitted was <b>'+thisSize+'</b> bytes. Please choose a Document with equal to or smaller then supported File Size.</p>';
				break;
			case 1:
				message 		=	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Image File Type</strong> must be smaller then <b>'+ fileSize +'</b> bytes. The size of the file  you submitted was <b>'+thisSize+'</b> bytes. Please choose an Image with equal to or smaller then supported File Size.</p>';
				break;
			case 2:
			message 			=	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Video File Type</strong> must be smaller then <b>'+fileSize+'</b> bytes. The size of the file you submitted was <b>'+thisSize+'</b> bytes. Please choose a Video with equal to or smaller then supported File Size.</p>';
				break;
			case 3:
			message 			=	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Audio File Type</strong> must be smaller then <b>'+fileSize+'</b> bytes. The size of the file you submitted was <b>'+thisSize+'</b> bytes. Please choose an Audio with equal to or smaller then supported File Size.</p>';
				break;
		}

		response.message_type 	=	message_type;
		response.message 		=	message;
		return response;
	}

	var disableOtherInputs 		=	function(thisFrm, switchValue)
	{
		switch(switchValue)
		{
			case 0:
				thisFrm.find('.image_file, .video_file, .audio_file').attr('disabled',true);
				thisFrm.find('.imageAnchor, .videoAnchor, .audioAnchor').css('color', '#D3D3D3').attr('disabled', true);
				break;
			
			case 1:
				thisFrm.find('.document_file, .video_file, .audio_file').attr('disabled',true);
				thisFrm.find('.documentAnchor, .videoAnchor, .audioAnchor').css('color', '#D3D3D3').attr('disabled', true);
				break;
			
			case 2:
				thisFrm.find('.image_file, .document_file, .audio_file').attr('disabled',true);
				thisFrm.find('.imageAnchor, .documentAnchor, .audioAnchor').css('color', '#D3D3D3').attr('disabled', true);
				break;
			
			case 3:
				thisFrm.find('.image_file, .document_file, .video_file').attr('disabled',true);
				thisFrm.find('.imageAnchor, .documentAnchor, .videoAnchor').css('color', '#D3D3D3').attr('disabled', true);
				break;
		}
	}

	var clearErrorSpan      	=   function()
	{
		$(document).find('.errorSpan').css('display','none');
	}

	var checkForIEVersion 		=	function() 
	{
	    var ua = window.navigator.userAgent;
	    var msie = ua.indexOf("MSIE ");
	    if (msie > 0) // If Internet Explorer, return version number
	    {
	        var version = (parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
	        return version;
	    }
	    return 0;
	}

	var resetFileInput 			=	function(thisBtn)
	{
		var thisFrm 			=	$(thisBtn).closest('form');
		var fileInputAnchor 	=	thisFrm.find('.fileInputAnchor');
		$.each(fileInputAnchor, function(i,v){
			$(v).css('color', '#61AFD1');
			$(v).attr('disabled', false);
		});

		var fileInput 			=	thisFrm.find('.fileInput');
		$.each(fileInput, function(i,v){
			$(v).attr('disabled', false);
			$(v).val('');
		});

		thisFrm.find('.uploadedFileName').html('');
	}

	var showFileName 		=	function(fileUpload)
	{
		var thisFileName	=	fileUpload.val().split('\\').pop();;
		var thisDiv 		=	fileUpload.closest('form').find('.uploadedFileName');
		thisDiv.html('<br>'+thisFileName);
	}

	var generateErrorForRequestFailure 	=	function()
	{
		var error 			=	{};
		error.message_type 	=	'alert-box warning radius';
		error.message 		=	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Request to the server <b>failed</b>.</p>';
		var makeResponse 	= 	generateResponse(error);
		$('#errorResponse').html(makeResponse);
	}
});