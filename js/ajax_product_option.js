$(document).ready(function() {
    $(document).on('change','.recurse_option',function(){
    	var data 		= 	{};
		data.option_id 	= 	$(this).val();
		data.bpu_id 	= 	$("#bpu_id").val();
		data            =   data;
		$.ajax({
			type 	: 	'POST',
			async 	:  	'false',
			url 	: 	'../php/brand_product_option_content_refresh.php',
			data 	: 	data,
			dataType: 	'json',
			success : 	function(result) {
				if(result.feature_image != '')
				{
					$("#productFeatureImage").html(result.feature_image);
				}
				if(result.description_html != '')
				{
					$("#product-description").html(result.description_html);
				}
				if(result.benefits_html != '')
				{
					$("#product-benefits").html(result.benefits_html);
				}
				if(result.directions_html != '')
				{
					$("#product-directions").html(result.directions_html);
				}
				if(result.ingredients != '')
				{
					$("#ingredients").html(result.ingredients);
				}
				if(result.project_brand_product_name != '')
				{
					$("#microdata_name").html(result.project_brand_product_name);
				}
				if(result.upc != '')
				{
					$("#microdata_gtin14").attr("content", result.upc);

				}
				if(result.sku != '')
				{
					$("#microdata_mpn").attr("content",result.sku);
					$("#microdata_sku").attr("content",result.sku);
				}
				if(result.retail_price != '')
				{
					$("#microdata_offer_price").text(result.retail_price);
					$("#productPrice" + result.group).find(".product-price").html("$" + result.retail_price);
				}
				if(result.manufacturer != '')
				{
					$("#microdata_manufacturer").attr("content",result.manufacturer);
				}
			},
			error 	: 	function(response) {
				console.log("Request to the server failed.");
			}
		});
	});
});
