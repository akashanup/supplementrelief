// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$(document).ready(function() {

  // ScrollUp https://github.com/markgoodyear/scrollup
  $.scrollUp({       
        scrollImg: true,
        scrollDistance: 100       
    });

  lazyLoadFunction();

  //This event will be fired when user will select any options from the view as dropdown
  $('#limitby').change(function(){
    //if seach field will be empty #20
    var functionName  =   $(this).attr('data-function');
    var form          =   $(this).closest("form");
    callSubmitForm(functionName, form);
  });
    
  //This event will be fired when user will select any options from the orderby dropdown
  $('#orderby').change(function(){
    var functionName  =   $(this).attr('data-function');
    var form          =   $(this).closest("form");
    callSubmitForm(functionName, form);
  });
    
  // This event will be fired when user will select any options from the Category dropdown
  $('#gos_id').change(function(){
    var form = $(this).closest("form");
    submitSearchProductForm(form);
  }); 
    
  // This event will be fired when user will select any options from the Ingredient dropdown
  $('#ingr_id').change(function(){
    var form = $(this).closest("form");
    submitSearchProductForm(form);
  });   
      
  //This event is fired when a button is pressed down. 
  $('#search').keypress (function (event)
  {
    var functionName  =   $(this).attr('data-function');
    var form          =   $(this).closest("form");
    var keycode       =   (event.keyCode ? event.keyCode : event.which);
    //if "ENTER" button is pressed,submit the form #22
    if (keycode == '13')
    {
      event.preventDefault();
      callSubmitForm(functionName, form);
    }
  });

  //This will be fired for change ,keyup and cut text event
  /*$("#search").on('change keyup cut', function(event) 
  {
    var functionName  =   $(this).attr('data-function');
    var form          = $(this).closest("form");
    var keycode       =   (event.keyCode ? event.keyCode : event.which);
    if (!this.value) {
      $('#search').removeAttr( "value");
      //document.form.submit();
      //$('#search-product-form').submit();
      callSubmitForm(functionName, form);
    }
    //!this.value ...
  });*/
  
  $("#productSearchButton").on("click", function(){
    var form = $(this).closest("form");
    submitSearchProductForm(form);
  });

  $("#searchBlogsBtn").on("click", function(){
    var form = $(this).closest("form");
    submitSearchBlogPostForm(form);
  });

  //this will be fired when user click on checkboxes
  $('#search-product-form').find('input[type=checkbox]').on('click',function( ){
    var form = $(this).closest("form");
    submitSearchProductForm(form);
  });

  //this will be fired when user click on checkboxes
  $('#search-blog-posts-form').find('input[type=checkbox]').on('click',function( ){
    var form = $(this).closest("form");
    submitSearchBlogPostForm(form);
  });

  $(".alphabetSearch").on("click", function(){
    $(".alphabetSearch").parent("dd").removeClass("active");
    $(this).parent("dd").addClass("active");
    var form = $(this).closest("form");
    form.find("#hiddenAlpha").val($(this).attr("data-alpha"));
    submitSearchProductForm(form);
  });

  $(document).on("click","#discontinuedProductsAnchor", function(){
    $('#discontinuedProducts').foundation('reveal','open');
  });

  $(document).on("click","#searchHelpAnchor", function(){
    $('#searchHelp').foundation('reveal','open');
  });

  // These events will fire when the user changes a value in these fields.
  $('#keyword').keypress(function(event){
    var txt       =   $("#keyword").val();
    var keycode   =   (event.keyCode ? event.keyCode : event.which);
    if(txt.length && keycode == '13')
    {
      event.preventDefault();
      submitSearchRecipeForm($('#recipe_search_form'));
    }
  });

  $('#searchRecipeBtn').on('click', function(){
    submitSearchRecipeForm($('#recipe_search_form'));
  });

  $('#order_by_recipe').change(function(){
    submitSearchRecipeForm($('#recipe_search_form'));
  });
   
  $('#number_servings').change(function(){
    submitSearchRecipeForm($('#recipe_search_form'));
  }); 
   
  $('#time_to_prepare').change(function(){
    submitSearchRecipeForm($('#recipe_search_form'));
  }); 
   
  $('#limit_by_chef').change(function(){
    submitSearchRecipeForm($('#recipe_search_form'));
  });
   
  $('#recipe_search_form').find('input[type=checkbox]').on('click',function(){
    submitSearchRecipeForm($('#recipe_search_form'));
  });

  $(document).on('click','.paginationAnchor', function(){
    var form = $(this).attr('data-form');
    var data = $(this).attr('data-url');
    
    switch(form)
    {
      case 'products':
        submitSearchProductForm($('#search-product-form'), data);
        break;
      case 'blogs':
        submitSearchBlogPostForm($('#search-blog-posts-form'), data);
        break;
    }
  });

$('#showAllBtn').on('click', function(){
  var limitby = $('#limitby').val();
  var orderby = $('#orderby').val();
  //var data    = 'limitby='+limitby+'&orderby='+orderby;
  var data    = 'alpha=&keyword=&limitby='+limitby+'&orderby='+orderby+'&gos_id=&ingr_id=';
  $('#search').val('');
  $('.alphabetSearch').parent().removeClass('active');
  var alpha = $('.alphabetSearch')[0];
  $(alpha).parent().addClass('active');
  $("#ingr_id").val($("#ingr_id option:first").val());
  $("#gos_id").val($("#gos_id option:first").val());
  $('#hiddenAlpha').val('');
  $('#search-product-form').find('input[type=checkbox]').attr('checked',false); 
  submitSearchProductForm($('#search-product-form'), data);
})

});

var lazyLoadFunction = function()
{ 
    var lazyImage       = $(document).find("img.lazy");
    var lazyImageParent = $(lazyImage).parent(".lazyDiv"); 
    $(lazyImage).height(lazyImageParent.height());
    $(lazyImage).width(lazyImageParent.width());
    $(lazyImage).lazyload();
}

//change address bar
function setLocation(data){
    try {
        window.history.pushState({"html": '',"pageTitle": 'Filter'},"Filter State", '?' + data);
        return false;
    } catch(e) {}
        location.hash = '#' + data;
}

var submitSearchProductForm = function(form, data)
{
  $.blockUI({ message: '<h3>Loading <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></h3>',baseZ: 1005 });
  if(data == undefined)
  {
    var data = form.serialize();
  }
  $.ajax({
      url: '../php/search_products.php',
      data: data,
      dataType:'json',
      type: 'GET',
      success :   function(response) {
        $.unblockUI();
        setLocation(data);
        //window.history.pushState({"html": '',"pageTitle": 'Filter'},"Filter State", '?' + data);
        $("#pagination-setup").html(response.pagination);
        $("#productsFound").html("<center>Products Found:&nbsp;&nbsp;"+response.total_products+"</center>");
        $("#products").html(response.html);
        lazyLoadFunction();
      },
      error   :   function(response) {
        $.unblockUI();
        $.blockUI({ message: '<h3>Request to the server failed.</h3>',timeout: 2000,baseZ: 1005 });
        console.log("Request to the server failed.");
      }
  });
}

var submitSearchBlogPostForm = function(form, data)
{
  $.blockUI({ message: '<h3>Loading <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></h3>',baseZ: 1005 });
  if(data == undefined)
  {
    var data = form.serialize();
  }
  $.ajax({
      url: '../php/show_blog_posts.php',
      data: data,
      type: 'GET',
      dataType:'json',
      success :   function(response) {
        $.unblockUI();
        setLocation(data);
        //window.history.pushState({"html": '',"pageTitle": 'Filter'},"Filter State", '?' + data);
        $("#pagination-setup").html(response.pagination);
        $("#noOfPosts").html('&nbsp;<i class="fa fa-binoculars"></i>&nbsp;&nbsp;'+response.total_records+'&nbsp;Posts')
        $("#blogsDiv").html(response.html);
        lazyLoadFunction();
      },
      error   :   function(response) {
        $.unblockUI();
        $.blockUI({ message: '<h3>Request to the server failed.</h3>',timeout: 2000,baseZ: 1005 });
        console.log("Request to the server failed.");
      }
  });
}

var callSubmitForm = function(formName, form)
{
  switch(formName)
  {
    case 'blogs':
      submitSearchBlogPostForm(form);
      break;
    case 'products':
      submitSearchProductForm(form);
      break;
  }
}

var submitSearchRecipeForm = function(form)
{
  $.blockUI({ message: '<h3>Loading <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></h3>',baseZ: 1005 });
  var formData = form.serialize();
  $.ajax({
      url: '../php/show_project_program_resource.php',
      data: formData,
      type: 'GET',
      dataType:'json',
      success :   function(response) {
        $.unblockUI();
        setLocation(formData);
        //window.history.pushState({"html": '',"pageTitle": 'Filter'},"Filter State", '?' + formData);
        $("#noOfRecipe").html('<i class="fa fa-search"></i>&nbsp;&nbsp;'+response.total_records+'<br /><br />')
        $("#recipeResultList").html(response.recipe_results_list);
        /*if(response.video_block.length)
        {
          $('#video_block').html(response.video_block);
        }
        if(response.resource_addendum_block.length)
        {
          if($('#resourceAddendum').length)
          {
            $('#resourceAddendum').html(response.resource_addendum_block);
          }
          else
          {
            $('#discuss').before(response.resource_addendum_block)
          }
        }
        if(response.product_addendum_block)
        {
          if($('#productAddendum').length)
          {
            $('#productAddendum').html(response.product_addendum_block);
          }
          else
          {
            $('#discuss').before(response.product_addendum_block)
          }
        }*/
        lazyLoadFunction();
      },
      error   :   function(response) {
        $.unblockUI();
        $.blockUI({ message: '<h3>Request to the server failed.</h3>',timeout: 2000,baseZ: 1005 });
        console.log("Request to the server failed.");
      }
  });
}