$(document).ready(function() { 
		
	$(".assessmentItemSubmit").click(function(){
		var myForm 							= 	$("#assessmentItemForm");		
		// Check for required Item Response value from user
		var itemType 						=	$('#assessmentItemTypeCode').val();
		var item_type_validation_feedback 	=	'';
		var assessmentResponse 				= 	false;
		if(myForm.find('.assessmentResponse').is(":checked"))
		{
			assessmentResponse = true;
		}

		if(!assessmentResponse)
		{
			switch(itemType)
			{
				case 'MCSA':
					item_type_validation_feedback = 'One option is required. Please choose!';
					break;
				case 'MCMA':
					item_type_validation_feedback = 'One or more options are required. Please choose!';
					break;
				case 'TF':
					item_type_validation_feedback = 'One option is required. Please choose!';
					break;
				case 'FIB':
					item_type_validation_feedback = 'A text response is required!';
					break;
			}
			var message_type 	= 	'alert-box warning radius';

			var message			= 	'<p><i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i>&nbsp;&nbsp;' + item_type_validation_feedback + '</p>';
			var html =	'';
			html	+= 	'<div class="row sessionMessage">'								+
							'<div class="small-12 columns">'							+
								'<div data-alert class="'+message_type+'">'	+ message 	+
									'<button tabindex="0" class="close closeAlertDiv" aria-label="Close Alert">&times;</button>'						+
				  				'</div>'												+
							'</div>'													+
						'</div>';
			$('#assessmentItemValidationFeedback').html('<br>' + html);
			$('html, body').animate({
		        scrollTop: myForm.offset().top
		    }, 1000);
		}
		else
		{
			var url 	= 	myForm.attr("action");
			var data 	= 	myForm.serialize();
			
			$.ajax({
				url: url,
				method: "POST",
				data: data,
				success: function(result) {
					$('#assessmentItemValidationFeedback').html('');
					$("#assessmentItemResponseFeedback").html(result);
					$("#assessmentItemSubmitButton").hide();
					$("input[type=radio]").attr('disabled', true);
					$("input[type=checkbox]").attr('disabled', true);
				},
				error: function(response) {
					alert("Something went wrong. Please try again later.");
				}
			});
		}
		return false;
	});

	$(document).on('click','.closeAlertDiv', function(){
		$('#assessmentItemValidationFeedback').html('');
	})
	
	/*
	$("#show-first-question").click(function(){
    	// $("#first-question").show();
    	$("#assessment-header").hide();
    	$("#assessment-item-form").show();
	});
	*/
	
	/*
	$("#show-next-question").click(function(){
    	// $("#first-question").show();
    	// $("#assessment-header").hide();
    	$("#assessment-item-feedback").hide();
    	// alert('Here i am');
    	// console.log('On item feedback about to show Next Question.');
    	$("#assessment-item-form").toggle();
	});
	*/
				
});