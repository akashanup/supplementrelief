$(document).ready(function() {
	$('#bookmarkBtn').on('click mouseover',function(){
		$('#bookmarkModal').foundation('reveal','open');
	});

	$(document).on('click', '#createBookmarkBtn', function(){
		var form 		= 	$(this).closest("form");
		var formData 	= 	form.serialize();
		$('#bookmarkResponseModal').foundation('reveal','open', {
		    url 	: 	'../php/bookmark_crud.php',
		    data    : 	formData,
		    async	: 	'false',
		    type 	: 	'POST',
		    success : 	function(response) {
				$('#bookmarkBtn').html('<i class="fa fa-bookmark fa-2x" aria-hidden="true" title="You already have a Bookmark for this page."></i>');
				$('#bookmarkModalBody').html('<a href="javascript:void(0)" id="editBookmarkLnk" title="Edit Bookmark for this page"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Edit Bookmark for this Page</a><br>');
			},
			error 	: 	function(response) {
				$.blockUI({ message: '<h3>Request to the server failed.</h3>',timeout: 2000,baseZ: 1005 });
				console.log("Request to the server failed.");
			}
		});
	});

	$(document).on('click', '#editBookmarkLnk', function(){
		var data 	=	{'key':'edit'};
		$('#editBookmark').foundation('reveal','open', {
		    url 	: 	'../php/bookmarks.php',
		    async	: 	'false',
		    data 	: 	data,
		    type 	: 	'POST',
		    success : 	function(response) {	
			},
			error 	: 	function(response) {
				$.blockUI({ message: '<h3>Request to the server failed.</h3>',timeout: 2000,baseZ: 1005 });
				console.log("Request to the server failed.");
			}
		});
	});

	$(document).on('click', '#updateBookmarkBtn', function(){
		var form 		= 	$(this).closest("form");
		var formData 	= 	form.serialize();
		$('#bookmarkResponseModal').foundation('reveal','open', {
		    url 	: 	'../php/bookmark_crud.php',
		    data    : 	formData,
		    async	: 	'false',
		    type 	: 	'POST',
		    success : 	function(response) {
				$('#bookmarkBtn').html('<i class="fa fa-bookmark fa-2x" aria-hidden="true" title="You already have a Bookmark for this page."></i>');
				$('#bookmarkModalBody').html('<a href="javascript:void(0)" id="editBookmarkLnk" title="Edit Bookmark for this page"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Edit Bookmark for this Page</a><br>');
			},
			error 	: 	function(response) {
				$.blockUI({ message: '<h3>Request to the server failed.</h3>',timeout: 2000,baseZ: 1005 });
				console.log("Request to the server failed.");
			}
		});
	});

	$(document).on('click', '#deleteBookmarkModalBtn', function(){
		var data 	=	{'key':'delete'};
		data.id 	=	$(this).attr('data-id');
		data.url 	=	$(this).attr('data-url');
		$('#deleteBookmark').foundation('reveal','open', {
		    url 	: 	'../php/bookmarks.php',
		    data    : 	data,
		    async	: 	'false',
		    type 	: 	'POST',
		    success : 	function(response) {
			},
			error 	: 	function(response) {
				$.blockUI({ message: '<h3>Request to the server failed.</h3>',timeout: 2000,baseZ: 1005 });
				console.log("Request to the server failed.");
			}
		});
	});

	$(document).on('click', '#deleteBookmarkNo', function(){
		$(this).closest('#deleteBookmarkForm').foundation('reveal','close');
	});

	$(document).on('click', '#deleteBookmarkYes', function(){
		var form 		= 	$(this).closest("form");
		var formData 	= 	form.serialize();
		var url 		=	form.find('#bookmark_url').val();
		var currentUrl  =   'https://'+window.location.hostname + window.location.pathname;

		$('#bookmarkResponseModal').foundation('reveal','open', {
		    url 	: 	'../php/bookmark_crud.php',
		    data    : 	formData,
		    async	: 	'false',
		    type 	: 	'POST',
		    success : 	function(response) {
		    	if(url == currentUrl)
				{
					$('#bookmarkBtn').html('<i class="fa fa-bookmark-o fa-2x" aria-hidden="true" title="You already have a Bookmark for this page."></i>');
					$('#bookmarkModalBody').html('<a href="" data-reveal-id="newBookmark" title="Bookmark this page"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Bookmark this Page</a><br>');
				}
			},
			error 	: 	function(response) {
				$.blockUI({ message: '<h3>Request to the server failed.</h3>',timeout: 2000,baseZ: 1005 });
				console.log("Request to the server failed.");
			}
		});
	});

	/*
		$(document).on('click', '.editBookmarkBtn', function(){
			var id 			=	$(this).attr('data-id');
			
			data 			=	{'id':id};
			$.ajax({
			    url 	: 	'../php/show_enrollment_bookmarks.php',
			    data 	: 	data,
			    type 	: 	'POST',
			    dataType: 	'json',
			    success : 	function(response) {	
			    	var data 	=	{'key':'edit'};
			    	data.id 		=	response.id;
					data.title 		=	response.title;
					data.comments   =   response.comments;
					data.page_name  = 	response.page_name;
					data.url 		=	response.url;
					$('#editBookmark').foundation('reveal','open', {
					    url 	: 	'../php/bookmarks.php',
					    async	: 	'false',
					    data 	: 	data,
					    type 	: 	'POST',
					    success : 	function(response) {	
						},
						error 	: 	function(response) {
							$.blockUI({ message: '<h3>Request to the server failed.</h3>',timeout: 2000,baseZ: 1005 });
							console.log("Request to the server failed.");
						}
					});
				},
				error 	: 	function(response) {
					$.blockUI({ message: '<h3>Request to the server failed.</h3>',timeout: 2000,baseZ: 1005 });
					console.log("Request to the server failed.");
				}
			});
		});
	*/




	$(document).on('click', '.editBookmarkBtn', function(){
		var id 			=	$(this).attr('data-id');
		data 			=	{'id':id};
		$('#editBookmark').foundation('reveal','open', {
		    url 	: 	'../php/show_enrollment_bookmarks.php',
		    async	: 	'false',
		    data 	: 	data,
		    type 	: 	'POST',
		    success : 	function(response) {	
			},
			error 	: 	function(response) {
				$.blockUI({ message: '<h3>Request to the server failed.</h3>',timeout: 2000,baseZ: 1005 });
				console.log("Request to the server failed.");
			}
		});
	});


	$(document).on('click', '#showBookmarksLnk', function(){
		var data 	=	{'key':'show'};
		$('#showBookmarks').foundation('reveal','open', {
		    url 	: 	'../php/show_enrollment_bookmarks.php',
		    async	: 	'false',
		    data 	: 	data,
		    type 	: 	'POST',
		    success : 	function(response) {	
			},
			error 	: 	function(response) {
				$.blockUI({ message: '<h3>Request to the server failed.</h3>',timeout: 2000,baseZ: 1005 });
				console.log("Request to the server failed.");
			}
		});
	});

});
