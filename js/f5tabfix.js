function openTab() {
    if(window.location.hash){
    	$('ul.tabs li a').each(function(){
			var hash = '#' + $(this).attr('href').split('#')[1] + "/";
			if(hash == window.location.hash){
				$(this).click();
			}
		});        
	} else {
		// open the first tab by default
        $('ul.tabs li:first-child a').click();
	}
}        
		
$(window).bind('hashchange', function() {
	openTab();
});
		
openTab();