var form_error = "";

/*
function testAndSubmit() {
	
	if (test_data()) {
		
		// Create a new element input, this will be our hashed password field. 
		var p = document.createElement("input");
	 	var form = document.getElementById("reg-form");
		
		// Add the new element to our form. 
		form.appendChild(p);
		p.name = "p";
		p.type = "hidden";
		p.value = hex_sha512(password.value);
	 
		// Make sure the plaintext password doesn't get sent. 
		document.getElementById("password").value = "";
		document.getElementById("confirmpwd").value = "";
		
		var client = new braintree.api.Client({clientToken: ct});
	
		client.tokenizeCard({
								number: document.getElementById('number').value, 
								expirationDate: document.getElementById('exp').value,
								postalCode: document.getElementById('zip').value
							}, function (err, nonce) {
								// Add the nonce to our form. 
								
								document.getElementById("payment_nonce").value = nonce;
												 
								// Make sure the plaintext card info doesn't get sent. 
								document.getElementById('number').value = "";
								document.getElementById('exp').value = "";
								
								if (err == null) {
									form.submit();
								} else {
									alert(err);
								}
						
		});
		
   		
		
	}
	
	
}

$(document).on('click', "#side-submit", function() {
	testAndSubmit();
});

$(document).on('click', "#side-next-personal", function() {
	if (test_personal( 	document.getElementById("reg-form"),
						document.getElementById("username"),
						document.getElementById("email"),
						document.getElementById("password"),
						document.getElementById("confirmpwd"), 
						document.getElementById("age"), 
						document.getElementById("sex")) ) {
		nextMenu($(this));				
	} else {
		alert(form_error);
		form_error = "";
		return false;	
	}
});

$(document).on('click', "#side-next-billing", function() {
	if (test_billing(   document.getElementById("fname").value, 
						document.getElementById("lname").value, 
						document.getElementById("street").value,
						document.getElementById("city").value,
						document.getElementById("state").value,
						document.getElementById("zip").value,
						document.getElementById("phone").value) ) {
		nextMenu($(this));				
	} else {
		alert(form_error);
		form_error = "";
		return false;	
	}
});
*/

function submitLoginKey(e) {
    if (e.keyCode == 13) {
	    
        var form = document.getElementById("login_form");
		var password = document.getElementById("password");
		
		e.preventDefault();
		e.stopPropagation();
        
		formhash(form, password);
    }
}

function submitLoginNow(evt) {
	var form = document.getElementById("login_form");
	var password = document.getElementById("password");
	
	evt.preventDefault();
	evt.stopPropagation();
	
	formhash(form, password);
	
}

function submitRegisterAccountNow(evt) {
	//console.log("Submitted");
	var form = document.getElementById("register_account_form");
	var password = document.getElementById("password");
	var confirmPassword = document.getElementById("confirm_password");
    
    evt.preventDefault();
	evt.stopPropagation();
	
    if (!testuserdata(form)) return false;
	
	// Make sure the plaintext password doesn't get sent. 
    confirmPassword.value = "";
    
	formhash(form, password);
	
}

function formhash(form, password) {
    // Create a new element input, this will be our hashed password field. 
    var p = document.createElement("input");
 
    // Add the new element to our form. 
    form.appendChild(p);
    p.name = "p";
    p.type = "hidden";
    p.value = hex_sha512(password.value);
 
    // Make sure the plaintext password doesn't get sent. 
    password.value = "";
 
    // Finally submit the form. 
    form.submit();
}

/* Modified version with Address verfication removed. */
function testuserdata(form) {
	// //console.log("Testing...")
    // Check each field has a value
    if (	/* form.first_name.value == ''			||
		    form.last_name.value == ''			|| */
		  	/* form.phone.value == ''			|| */
          	/* form.gender.value == '' 			|| */
          	form.username.value == ''   		||
		  	form.email.value == '' 	    		|| 
		  	form.password.value == ''			|| 
		  	form.confirm_password.value == ''		) {
 
        form_error = 'You must provide all the requested details. Please try again.';
        alert(form_error);
        return false;
    }
    
    if (form.username.length < 6) {
        form_error = 'Usernames must be at least 6 characters long.  Please try again.';
        alert(form_error);
        form.username.focus();
        return false;
    }
 
    // re = /^\w+$/;
    // re = /^[a-z0-9_@.]+$/;
    // re = /\S+@\S+\.\S+/;
    
    re = /^[a-zA-Z0-9.\-_$@*!]{6,30}$/;

    // re = /^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._@]+(?<![_.])$/; 
    if(!re.test(form.username.value)) { 
        form_error = "Username must contain only letters and numbers with these optional special characters: .\-_$@*! Please try again.";
        alert(form_error); 
        form.username.focus();
        return false;
    }
    
    /*
    var maleCheck = document.getElementById("genderMale");
    var femaleCheck = document.getElementById("genderFemale");
	
	if (maleCheck.checked !== true && femaleCheck.checked !== true) {
	    form_error = 'The gender you entered is invalid. Please try again.';
        alert(form_error); 
        return false;
    }
    */
      
    /*
    var re = /^([1-9]|[1-9][0-9]|[1-9][0-9][0-9])$/; 
    if (re.test(form.state.value) == false) {
        form_error = 'The state abbreviation you selected is not a valid state. Please try again';
        alert(form_error); 
        form.state.focus();
        return false;
    }
	
	re = /^\d{5}(-\d{4})?$/; 
    if (re.test(form.postal_code.value) == false) {
        form_error = 'The zip code you entered is not a valid US zip code. Please try again';
        alert(form_error); 
        form.postal_code.focus();
        return false;
    }
	*/
	
	/*
	re = /^\(?(\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{4})$/; 
    if (re.test(form.phone.value) == false) {
        form_error = 'The phone number you entered is not a valid US phone number. Please try again.';
        alert(form_error); 
        form.phone.focus();
        return false;
    }
    */
    
    re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i; 
	if(re.test(form.email.value) == false) {
		form_error = 'The email address you entered is not valid.';
        alert(form_error); 
		// TODO: flashError("#" + wForm + "PersonContactMethodDateErrorMessage", '<b>Email address</b> is invalid.');
		form.email.focus(); 
        return false;   
	}
	
	form_error = test_pass(form.password.value, form.confirm_password.value);
    
    if (form_error !== '') {
		alert(form_error); // TODO: Replace with standard error function (form_error contains error message string)
		form_error = "";
		return false;
	}

	return true;
	 
}

/* Original version that has Address verification.
	
function testuserdata(form) {
	//console.log("Testing...")
     // Check each field has a value
    if (	form.first_name.value == ''			||
		    form.last_name.value == ''			||
		    form.address.value == ''			||
          	form.city.value == '' 	    		|| 
		  	form.state.value == ''				||
		  	form.postal_code.value == ''		||
		  	form.phone.value == ''				||
          	form.gender.value == '' 			||
          	form.username.value == ''   		||
		  	form.email.value == '' 	    		|| 
		  	form.password.value == ''			|| 
		  	form.confirm_password.value == ''		) {
 
        form_error = 'You must provide all the requested details. Please try again';
        alert(form_error);
        return false;
    }
    
    if (form.username.length < 6) {
        form_error = 'Usernames must be at least 6 characters long.  Please try again';
        alert(form_error);
        form.username.focus();
        return false;
    }
 
    re = /^\w+$/; 
    if(!re.test(form.username.value)) { 
        form_error = "Username must contain only letters, numbers and underscores. Please try again";
        alert(form_error); 
        form.username.focus();
        return false;
    }
    
    var maleCheck = document.getElementById("genderMale");
    var femaleCheck = document.getElementById("genderFemale");
	
	if (maleCheck.checked !== true && femaleCheck.checked !== true) {
	    form_error = 'The gender you entered is invalid. Please try again';
        alert(form_error); 
        return false;
    }
      
    //var re = /^(AA|AE|AP|AL|AK|AS|AZ|AR|CA|CO|CT|DE|DC|FM|FL|GA|GU|HI|ID|IL|IN|IA|KS|KY|LA|ME|MH|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|MP|OH|OK|OR|PW|PA|PR|RI|SC|SD|TN|TX|UT|VT|VI|VA|WA|WV|WI|WY)$/; 
    
    var re = /^([1-9]|[1-9][0-9]|[1-9][0-9][0-9])$/; 
    if (re.test(form.state.value) == false) {
        form_error = 'The state abbreviation you selected is not a valid state. Please try again';
        alert(form_error); 
        form.state.focus();
        return false;
    }
	
	re = /^\d{5}(-\d{4})?$/; 
    if (re.test(form.postal_code.value) == false) {
        form_error = 'The zip code you entered is not a valid US zip code. Please try again';
        alert(form_error); 
        form.postal_code.focus();
        return false;
    }
	
	re = /^\(?(\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{4})$/; 
    if (re.test(form.phone.value) == false) {
        form_error = 'The phone number you entered is not a valid US phone number. Please try again';
        alert(form_error); 
        form.phone.focus();
        return false;
    }
    
    re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i; 
	if(re.test(form.email.value) == false) {
		form_error = 'The email address you entered is not valid.';
        alert(form_error); 
		// TODO: flashError("#" + wForm + "PersonContactMethodDateErrorMessage", '<b>Email address</b> is invalid.');
		form.email.focus(); 
        return false;   
	}
	
	form_error = test_pass(form.password.value, form.confirm_password.value);
    
    if (form_error !== '') {
		alert(form_error); // TODO: Replace with standard error function (form_error contains error message string)
		form_error = "";
		return false;
	}

	return true;
	 
}
*/

/*
function test_data() {
	
	if (test_personal( 	document.getElementById("reg-form"),
						document.getElementById("username"),
						document.getElementById("email"),
						document.getElementById("password"),
						document.getElementById("confirmpwd"), 
						document.getElementById("age"), 
						document.getElementById("sex")) &&
					
	test_billing(   document.getElementById("fname").value, 
					document.getElementById("lname").value, 
					document.getElementById("street").value,
					document.getElementById("city").value,
					document.getElementById("state").value,
					document.getElementById("zip").value,
					document.getElementById("phone").value )) {
						
    	return true;
	
	} else {
		
		alert(form_error);
		form_error = "";
		return false;
		
	}
	
}


function test_personal(form, uid, email, password, conf, age, sex ) {
     // Check each field has a value
    if (uid.value == ''         || 
          email.value == ''     || 
          password.value == ''  || 
          conf.value == '') {
 
        form_error = 'You must provide all the requested details. Please try again';
        return false;
    }
 	
    // Check the username
 
    re = /^\w+$/; 
    if(!re.test(form.username.value)) { 
        form_error = "Username must contain only letters, numbers and underscores. Please try again"; 
        form.username.focus();
        return false; 
    }
 	
	form_error = test_pass(password.value, conf.value);
	
	if (form_error !== '') {
		form.password.focus();
		return false;
	}
	
	if (sex.value !== "M" && sex.value !== "F") {
	    form_error = 'The sex you entered is invalid. Please try again';
        return false;
    }
	
	return true;
	
    
}
*/

function test_user_id(user_id) {
	var err = '';
	// Check that the User ID is sufficiently long (min 6 chars).
    if (user_id.length < 6) {
        err = 'User ID must be at least 6 characters long.  Please try again.';
        return err;
    }
        
    var re = /^[a-zA-Z0-9@._]+$/;	

    if(!re.test(user_id)) { 
        err = "User ID may contain only letters, numbers, dashes, underscores, periods and @. Please try again.";
        return err;
    }
 	
	return err;
}

function test_pass(password, conf) {
	var err = '';
	 // Check that the password is sufficiently long (min 6 chars)
    // The check is duplicated below, but this is included to give more
    // specific guidance to the user
    if (password.length < 6) {
        err = 'Passwords must be at least 6 characters long.  Please try again.';
        return err;
    }
 
    // At least one number, one lowercase and one uppercase letter 
    // At least six characters 
 
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
    if (!re.test(password)) {
        err = 'Passwords must contain at least one number, one lowercase and one uppercase letter.  Please try again.';
        return err;
    }
 
    // Check password and confirmation are the same
    if (password != conf) {
        err = 'Your password and confirmation do not match. Please try again.';
        return err;
	}
	
	return err;
}

function test_user_email(user_email) {
	
	var err = '';
	
	var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i; 

    if (!re.test(user_email)) {
        err = 'Email is not valid.  Please try again.';
        return err;
    }
 	
	return err;
}

/*
function test_billing(fname, lname, street, city, state, zip, phone) {
	
	if (fname == "" || lname == "") {
		form_error = 'Please enter a First and Last Name.';
        return false;
	}
	
	var re = /^(AA|AE|AP|AL|AK|AS|AZ|AR|CA|CO|CT|DE|DC|FM|FL|GA|GU|HI|ID|IL|IN|IA|KS|KY|LA|ME|MH|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|MP|OH|OK|OR|PW|PA|PR|RI|SC|SD|TN|TX|UT|VT|VI|VA|WA|WV|WI|WY)$/; 
    if (re.test(state) == false) {
        form_error = 'The state abbreviation you entered is not a valid US state. Please try again';
        return false;
    }
	
	re = /^\d{5}(-\d{4})?$/; 
    if (re.test(zip) == false) {
        form_error = 'The zip code you entered is not a valid US zip code. Please try again';
        return false;
    }
	
	re = /^\(?(\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{4})$/; 
    if (re.test(phone) == false) {
        form_error = 'The phone number you entered is not a valid US phone number. Please try again';
        return false;
    }
	
	return true;
	
}


function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

*/
