$(document).ready(function() {

	var typingTimer 		=	null;   //timer identifier
	var doneTypingInterval 	= 	1000;   //time in ms
	var listIndex 			=	-1;
	var noOfElements 		=	10;
	//on keyup, start the countdown

	$('.predictive_search :not(.productsNameLi)').on('focusout', function(){
		listIndex 			=	-1;
		removeList();
	});

	$('.predictive_search').on('keydown focusin click', function(e){
		var searchBox 		=	$(this);
		var searchText 		= 	searchBox.val();
		var key 			=	e.keyCode;

	    if(key == 38 || key == 40)
    	{
    		highLightLi(key, searchBox);
    		return;
    	}
	    if (typingTimer) 
	    {
	    	window.clearTimeout(typingTimer);  // Clear if already set     
			removeList();
	    }
	    if(searchText.length > 0)
	    {
	    	typingTimer 		= 	window.setTimeout(function(){
		    	doneTyping(searchBox, searchText)
		    }, doneTypingInterval);
	    }
	});

	$(document).on('click','.productsNameLi',function(){
		var thisValue = $(this).text();
		var predictive_search = $('.predictive_search');
		predictive_search.val(thisValue);

		var form = predictive_search.closest('form');
		var form_attr = form.attr('data-id');
		removeList();
		if(form_attr == 'product')
		{
			form.submit();
		}
		//$('.predictive_search').closest('form').submit();
		submitSearchProductForm(form);
	});

	//user is "finished typing," do something
	var doneTyping 		= function(searchBox, searchText) 
	{
	    var data 		=	{};
	    data.searchText =	searchText;
	    $.ajax({
		    url 		: 	'../php/get_numedica_product.php',
		    data 		: 	data,
		    type 		: 	'POST',
		    dataType 	: 	'json',
		    success 	: 	function(response) {
		    	listIndex 		= 	-1;
		    	noOfElements 	=	response.elements - 1;
		    	searchBox.closest('div').append(response.html);
		    	addHover();
			},
			error 	: 	function() {
				console.log("Request to the server failed.");
			}
		});
	}

	var removeList 	=	function()
	{
		$(document).find('#productsNameList').remove();
	}

	var addHover 	=	function()
	{
		$(document).find('.productsNameLi').hover(function(){
			$(this).css("background-color", "#61AFD1");
		},function(){
			$(this).css("background-color", "#fafafa");
		});
	}

	var highLightLi =	function(key,searchBox)
	{
		if(key == 40)
		{
			listIndex 	+=	1;
			if(listIndex > noOfElements)
			{
				listIndex = 0;	
			}
			
		}   
		else if(key == 38)
		{
			listIndex	-=	1;
			if(listIndex < 0)
			{
				listIndex = noOfElements;
			}
		}
		
		element = $(document).find('.productsNameLi')[listIndex];
		var listValue = $(element).text();
		searchBox.attr("data-value", listValue);
		$(element).css("background-color", "#61AFD1");
		$(element).siblings().css("background-color", " #fafafa");
	}	

	$(document).on('click', ':not(#productsNameList, .predictive_search)', function(){
		listIndex 			=	-1;
		removeList();
	})
});

