// Called from /member-dashboard/

// var maskedField = null;

$(document).ready(function() {
	
	// Creates a Date format mask for Effective and End Dates

	/*
	MaskedInput({
	  elm: document.getElementById("newPersonContactMethodEffectiveDate"),
	  format: "MM/DD/YYYY",
	  separator: ' \/:-()',
	  typeon: 'X_MDYhms'
	});
	

	MaskedInput({
	  elm: document.getElementById("newPersonContactMethodEndDate"),
	  format: "MM/DD/YYYY",
	  separator: ' \/:-()',
	  typeon: 'X_MDYhms'
	});
	
	
	MaskedInput({
	  elm: document.getElementById("editPersonContactMethodEffectiveDate"),
	  format: "MM/DD/YYYY",
	  separator: ' \/:-()',
	  typeon: 'X_MDYhms'
	});
	
	
	MaskedInput({
	  elm: document.getElementById("editPersonContactMethodEndDate"),
	  format: "MM/DD/YYYY",
	  separator: ' \/:-()',
	  typeon: 'X_MDYhms'
	});
	*/
	
});

/*
function maskField(ctrlId, maskFormat) {

	if (maskedField !== null) {maskedField.setEnabled(false);}
	
		maskedField = MaskedInput({
		elm: document.getElementById(ctrlId),
		format: maskFormat,
		separator: ' \/:-()',
		typeon: 'X_MDYhms'
	});

}
*/

// Client-Side Validation
/*
function checkMethod(wForm) {
	
	methodValue = $("#" + wForm + "PersonContactMethod").val();
	
	//console.log("methodValue = '" + methodValue + "'");

	valId = "#" + wForm + "PersonContactMethodValue";
	
	if ($(valId).val() == "") {
		flashError("#" + wForm + "PersonContactMethodDateErrorMessage", 'Value cannot be blank.');   
		return false; 
	}
	
	if (methodValue == "PHONE" || methodValue == "FAX") {
		//Validate Phone/Fax
		var number = $(valId).val();
		var re = /^\(?(\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{4})$/; 
		if(!re.test(number)) { 
			// Process Error
			flashError("#" + wForm + "PersonContactMethodDateErrorMessage", '<b>' + (methodValue == "PHONE" ? 'Phone ' : 'Fax ') + 'Number</b> is invalid.');   
		  return false; 
		}
		
	} else if (methodValue == "EMAIL") {
	
	// Validate Email
		var email = $(valId).val();
		//console.log("Validating: " + email)
		var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i; 
	  if(!re.test(email)) { 
	  	// Process Error
	  	flashError("#" + wForm + "PersonContactMethodDateErrorMessage", '<b>Email address</b> is invalid.');   
	  	return false; 
	  }
	  
	} else if (methodValue == "URL") {
	
	// Validate URL
		var link = $(valId).val();
		//console.log("Validating: " + link)
		var re = /(https?|ftp|file|ssh):\/\/(((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?/i; 
	  if(!re.test(link)) { 
	  	// Process Error
	  	flashError("#" + wForm + "PersonContactMethodDateErrorMessage", '<b>URL</b> is invalid.');   
	  	return false; 
	  }
	  
	}
	
	//console.log("Method Validation Passed");
	return true;
}
*/


// Validation to insure End Date (optional) is > Effective Date (required)
/*
function checkDates(wForm) {

	// //console.log('checkDates function executing.');
	var startDt=document.getElementById(wForm + "PersonContactMethodEffectiveDate").value;
	// //console.log(startDt);
		
    var endDt=document.getElementById(wForm + "PersonContactMethodEndDate").value;
    // //console.log(endDt);
    
    var re = /(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/; 
    if(!re.test(startDt)) { 
    	// Process Error
    	flashError("#" + wForm + "PersonContactMethodDateErrorMessage", '<b>Start Date</b> is required.'); 
      return false; 
    }
		
	var re = /(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/; 
	if(!re.test(endDt)) {
		$("#" + wForm + "PersonContactMethodEndDate").val("");
	}

    if (new Date(startDt).getTime() > new Date(endDt).getTime()) {
    
    	// //console.log('Effective Date: ', startDt, ' > End Date: ', endDt );
		flashError("#" + wForm + "PersonContactMethodDateErrorMessage", '<b>End Date (' + endDt + ') </b> must be later than <b>Effective Date (' + startDt + ') </b>.');   
		return false;
             
    }
    
    return true;
}
*/

/*
function flashError(formErrDiv, message) {
	$(formErrDiv).html('<div data-alert class="alert-box alert radius"><p>' + message + '</p></div>');   
	      
	// $("#newPersonContactMethodDateErrorMessage").appendTo("#error-here");
	// $("#error-div").show();
	
	$(formErrDiv).show().delay(6000).animate({
	      opacity: 0 }, 1500, "linear", function() {
	      	$(formErrDiv).hide().css("opacity", 1);
	      });
}


// New Contact Method form - Contact Method field (Select list)
// Sets the field mask and placeholder depending upon user value selected
$( "#newPersonContactMethod" ).change(function() {
	switchMethod("new", $(this));
});


// New Contact Method form - Contact Method field (Select list)
// Sets the field mask and placeholder depending upon user value selected
$( "#editPersonContactMethodSelect" ).change(function() {
	switchMethod("edit", $(this));
});


function switchMethod(wForm, ctrl) {
	//console.log("Changing " + wForm + " method");
		valId = wForm + "PersonContactMethodValue";
	
		$('#' + valId).prop("disabled", (ctrl.val == ""));
	
		if (ctrl.val() == "PHONE" || ctrl.val() == "FAX") {
			$('#' + valId).val("");
			maskField(valId, "(XXX) XXX-XXXX");
		} else if (ctrl.val() == "EMAIL") {
			if (maskedField !== null) { maskedField.setEnabled(false);}
			//$('#' + valId).attr("type", "email");
			$('#' + valId).attr("placeholder", "someone@domain.com");
			$('#' + valId).val("");
		} else if (ctrl.val() == "SKYPE") {
			if (maskedField !== null) { maskedField.setEnabled(false);}
			$('#' + valId).attr("placeholder", "Skype name");
			$('#' + valId).val("");
		} else if (ctrl.val() == "TWITTER") {
			if (maskedField !== null) { maskedField.setEnabled(false);}
			$('#' + valId).attr("placeholder", "Twitter handle");
			$('#' + valId).val("");
		} else if (ctrl.val() == "URL") {
			if (maskedField !== null) { maskedField.setEnabled(false);}
			$('#' + valId).attr("placeholder", "http://YourDomain.com");
			$('#' + valId).val("");
		} else {
			if (maskedField !== null) { maskedField.setEnabled(false);}
			$('#' + valId).val("");
		}
		
		$('#' + valId).select();
	
}


$( "#newPersonContactMethodEffectiveDate" ).change(function() {
	// //console.log('Effective Date on New Contact Method form changed.')
    checkDates("new");
});


$( "#newPersonContactMethodEndDate" ).change(function() {
		// //console.log('End Date on New Contact Method form changed.')
    checkDates("new");   
});


$( "#editPersonContactMethodEffectiveDate" ).change(function() {
		// //console.log('Effective Date on New Contact Method form changed.')
    checkDates("edit");
});


$( "#editPersonContactMethodEndDate" ).change(function() {
		// //console.log('End Date on New Contact Method form changed.')
    checkDates("edit");   
});

*/
  
// SOZO User Dashboard Contacts tab

// Triggered when User presses New Contact Method Button from Contacts tab 

/*
function newPersonContactMethod() {
	// //console.log('User selected New Contact Method button that displays New Contact Method form.');		
	$('#newPersonContactMethodLink').hide();
	$('#listPersonContactMethods').hide();
	$('#newPersonContactMethodForm').show();
}


$(".edit-button").click(function() {

	// //console.log('User selected New Contact Method button that displays New Contact Method form.');		
	$('#newPersonContactMethodLink').hide();
	$('#listPersonContactMethods').hide();
	$('#newPersonContactMethodForm').show();
	
	$('#contact_method_id_hidden_field').val($(this).data("value"));

});


// User selects Create button on new_person_contact_method_form.php.
$("#newPersonContactMethodSubmit").on("click", function () {
	// Make sure validations passed before submitting form for processing.
	if ( checkMethod("new") !== true ) { return; }
	if ( checkDates("new") !== true ) { return; }
	// Other validations for remaining Contact Methods?	i.e. valid EMAIL, URL?
	submitNow("#createPersonContactMethod");
} );


// User selects Create button on new_person_contact_method_form.php.
$("#editPersonContactMethodSubmit").on("click", function () {
	// Make sure validations passed before submitting form for processing.
	if ( checkMethod("edit") !== true ) { return; }
	if ( checkDates("edit") !== true ) { return; }
	// Other validations for remaining Contact Methods?	i.e. valid EMAIL, URL?
	submitNow("#updatePersonContactMethod");
} );


// Triggered when User presses Create button for the New Contact Method form 
function submitNow(wForm) {

	// alert('submitNow("#createPersonContactMethod");');
	var myForm = $(wForm);
	
	event.preventDefault();
	var url = myForm.attr("action");
	var data = myForm.serialize();
	// //console.log(url);
 	// //console.log(data);
 	
	$.post(url,data,function(response) {
	
		// //console.log("New Contact Method form submitted to server for processing.");
	
		if ( response !== "" ) {
			//console.log(response);
			// error condition
			return;
		}
		
		if (wForm == "#createPersonContactMethod") {
			$('#newPersonContactMethodLink').show();
			$('#newPersonContactMethodForm').hide();
		} else if (wForm == "#updatePersonContactMethod") {
			$('#editPersonContactMethodLink').show();
			$('#editPersonContactMethodForm').hide();
			
		}
		
		$('#listPersonContactMethods').show();		
		$('#listPersonContactMethods').load("../php/list_person_contact_methods.php");
	
	});
				
}


// Triggered when User presses the Cancel Button from the New Contact Method form
function newPersonContactMethodCancel() {
    $('#newPersonContactMethodForm').hide();   
		$('#newPersonContactMethodLink').show();
    $('#listPersonContactMethods').show();
}

*/


// SOZO User Dashboard Contacts tab
// Triggered when User presses Edit Button for list_person_contact_methods.php.
// Want to change the link that calls the Edit form from a button to a standard text hyperlink "Edit"

/*
$(document.body).on('submit','#editPersonContactMethod',function(evt) {
	evt.preventDefault();
	var url = $(this).attr("action");
	var data = $(this).serialize();
	//console.log(url);
	//console.log(data);
	$.post(url, data, function(response) {
  		//$('#editPersonContactMethodForm').html(response); 
  		
  		//console.log(response);
  		
  		//console.log("Contact Method Value: " + response.ContactMethodValue);
  		
  		$("#editContactMethodId").val(response.ContactMethodId);
  		
  		$("#editPersonContactMethodSelect > option").each(function() {
  			
  			if ($(this).val() == response.ContactMethodTypeCode) {
  			
  				$(this).prop("selected", true);
  				
  			} else {
  				
  				$(this).prop("selected", false);
  				
  			}
  			
  			//console.log($(this).val() + ": " + $(this).prop("selected")); 			
  		});
  		
  		$("#editPersonContactMethodValue").val(response.ContactMethodValue);
  		$("#editPersonContactMethodEffectiveDate").val(response.ContactMethodEffectiveDate);
  		$("#editPersonContactMethodEndDate").val(response.ContactMethodEndDate);
  		
			$('#editPersonContactMethodForm').show();
			$('#newPersonContactMethodLink').hide();		  	
  		$('#newPersonContactMethodForm').hide(); 		
  		$('#listPersonContactMethods').hide();
	}, "json");
});

*/

// Triggered when User presses Update Button on the Edit Contact Method form 
/*
$(document.body).on('submit','#updatePersonContactMethod',function(evt) {
	evt.preventDefault();
	var url = $(this).attr("action");
	var data = $(this).serialize();
 	//console.log(url);
 	//console.log(data);
 	  
	$.post(url,data,function(response) {
	
		// Error handling of response?
		$('#editPersonContactMethodForm').hide();
		$('#listPersonContactMethods').load("../php/list_person_contact_methods.php");
		$('#newPersonContactMethodLink').show();
		$('#listPersonContactMethods').show();	
				
		/*
		var a = $(response);
		alert(response);
			
		$('#newPersonContactMethodLink').show();
		$('#listPersonContactMethods').html(a.find('#listPersonContactMethods').html());
		$('#listPersonContactMethods').html;
		$('#newPersonContactMethodForm').hide();
		$('#listPersonContactMethods').show();
		
		alert(a.find('.success').length);
		
		if($('.success').length <= 0)
		      
			$('#userTabs').before('<div id="flash_message" class="alert-box success radius" data-alert>'+a.find('.success').html()+'</div>');
			$( "#flash_message" ).fadeOut( 3200, function() {
				// Animation complete.
							
		});
		*/	
		
	/* });	
				
});
*/

// Triggered when User presses No button on the Delete Confirmation Dialogue for Edit Contact Method form 
/*
$(document.body).on('click',"#deleteContactMethodNo",function(){
	// //console.log('User selected No on the Delete Confirmation Dialogue for the Edit Contact Method form.');		
	$('#person_contact_method_delete_confirmation_modal').foundation('reveal', 'close');
});

// Triggered when User presses Yes button on the Delete Confirmation Dialogue for Edit Contact Method form 
$(document.body).on('click',"#deleteContactMethodYes",function(){	
	// //console.log('User selected Yes on the Delete Confirmation Dialogue for the Edit Contact Method form.');		
	$('#updatePersonContactMethodAction').attr("value", "delete");
	$('#updatePersonContactMethod').submit();
	$('#person_contact_method_delete_confirmation_modal').foundation('reveal', 'close');
	$('#listPersonContactMethods').load("../php/list_person_contact_methods.php");	
	$('#newPersonContactMethodLink').show();
	$('#listPersonContactMethods').show();	 	
});


// Triggered when User presses the Cancel Button from the Edit Contact Method form
function editPersonContactMethodCancel() {
    $('#editPersonContactMethodForm').hide();   
		$('#newPersonContactMethodLink').show();
    $('#listPersonContactMethods').show();
}

*/
		
// Member Dashboard Addresses tab

// Triggered when User presses New Address button 
function newPersonLocation() {
	// console.log('User selected the New Address button.');
	$('#newPersonLocationLink').hide();
    $('#listPersonLocations').hide();
    $('#newPersonLocationForm').show();	  	    
}


// Triggered when User presses the Cancel Button from the New Address form
function newPersonLocationCancel() {
	// console.log('User selected the Cancel button on the New Address form.');
    $('#newPersonLocationForm').hide();   
	$('#newPersonLocationLink').show();
    $('#listPersonLocations').show();
}


// Triggered when User presses Edit Button for Show Addresses
$(document.body).on('submit','.edit_address',function(evt) { 
	// console.log('User selected the Edit button on an Address record.');
	evt.preventDefault();
	var url = $(this).attr("action");
	var data = $(this).serialize();
	// console.log(url); // debugging
 	// console.log(data); // debugging
	$.post(url,data,function(response) {
  		$('#editPersonLocationForm').html(''); 
  		$('#editPersonLocationForm').append(response); 
		$('#editPersonLocationForm').show();
  		$('#newPersonLocationLink').hide();
  		$('#listPersonLocations').hide();
	});
});

	
// Triggered when User presses Cancel Button for Edit Address form
$(document).on('click','#update_address_cancel',function() { 
	// console.log('User selected the Cancel button on an Edit Address form record.');
	$('#editPersonLocationForm').hide();
	$('#newPersonLocationLink').show();
	$('#listPersonLocations').show();
});	

// Triggered when all forms having class "update_address" are submitted. $(document.body) is used because it will work for dynamic content also.
$(document.body).on('submit','.update_address',function(evt) {
	// console.log('User selected the Update (Submit) button on an Edit Address form record.');
	evt.preventDefault();
	var url = $(this).attr("action");
	var data = $(this).serialize();
	// console.log(url);
 	// console.log(data);
	$.post(url,data,function(response) {
  		var a = $(response);
  		// alert(response);
  		$('#newPersonLocationLink').show();
  		$('#listPersonLocations').html(a.find('#listPersonLocations').html());
  		// $('#listPersonLocations').html;
		$('#editPersonLocationForm').hide();
		$('#listPersonLocations').show();
		
		// alert(a.find('.success').length);
		
		if(a.find('.success').length > 0)
        {	
			if($('.success').length <= 0)
            {
            	//$('#flash_message').remove();
            }
            
				// $('#userTabs').before('<div id="flash_message" class="alert-box success radius" data-alert>'+a.find('.success').html()+'</div>');
				// $( "#flash_message" ).fadeOut( 3200, function() {
					// Animation complete.
					
				// });
					
			} /*
            else
            {
                $('#flash_message').remove();
                $('#userWelcomeMessage').after('<div id="flash_message" class="alert-box success radius" data-alert>'+a.find('.success').html()+'</div>');
                $( "#flash_message" ).fadeOut( 3200, function() {
                        // Animation complete.
    
                      });
            } 
		}*/
	});
});


// It will be triggered when user will try to close success alert box div. Default close button will not close the div because foundation works for all contents when document is ready. It will not work for dynamic loaded content .So we need to close it manually.
$(document.body).on('click','.close',function() {
	$('.close').parent().remove();
	$('#addresses').addClass('active');
});

$(document).on('click',"#deleteAddressNo",function(){
	// console.log("User clicked No button to delete Address.");
	jQuery('#delete_confirmation_modal').foundation('reveal', 'close');
});

$(document).on('click',"#deleteAddressYes",function(){
	// console.log("User clicked Yes button to delete Address.");
    $('#update_address_action').attr("value", "delete");
	$('.update_address').submit();
	jQuery('#delete_confirmation_modal').foundation('reveal', 'close');	
});