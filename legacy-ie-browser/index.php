<?php

include_once('../includes/header.php');	
getPage('Legacy IE Browser', $connection);
include_once('../includes/authorization.php');
include('../php/system_log_page_access.php'); 

?>

<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
	    <title>Microsoft Internet Explorer Support</title>
	    <meta name="robots" content="noindex, nofollow">
	</head>
	
	<body>
		
		<h1>Microsoft Internet Explorer Version 8 and Older No Longer Supported</h1>
		
		<h3>Please call SupplementRelief.com at <b>(888) 424-0032</b> if you would like to place an order for NuMedica supplements over the phone.</h3>
		
		<p>It appears you are using a Microsoft Windows computer with Internet Explorer version 8 or older. This is a <b>VERY OLD BROWSER that does not work with modern websites</b>. Even Microsoft no longer support version 8 as of January 12, 2016. If possible, please <b>upgrade your browser to at least version 9</b> or higher (version 11 is current as of January 1, 2016) or choose another modern browser that works on Windows computers such as Chrome or Firefox.</p>
		
		<p>You can download for free from Microsoft a more recent version of Microsoft Internet Explorer <a href="https://support.microsoft.com/en-us/help/17621/internet-explorer-downloads">here</a>.</p>
		
		<br>
	
		<center><img src="https://cdn-manager.net/media/images/supplementrelief-logo-blue-landscape-large.jpg"></center>
	
	</body>
</html>

