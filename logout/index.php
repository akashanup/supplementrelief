<?php 

// echo 'Begin Logout page.<br />';

include_once('../includes/header.php');

getPage('Logout', $connection);

if ($_SESSION['page']['use_authorization']) { include_once('../includes/authorization.php'); }

include_once('../includes/page_header.php');

// Removing user_id changes the menu options displayed on the Logout page removing those options accessible to a Logged In user.
unset($_SESSION['user']['user_id']);

if ($_SESSION['page']['use_menu']) { include_once('../includes/menu.php'); }

if ($_SESSION['page']['use_title_bar']) { include_once('../includes/off_page_canvas_1.php'); }

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

if (!empty($global_page_content_summary)) { echo $global_page_content_summary; }

if ($_SESSION['page']['promotions'] == 1) { include_once('../php/show_project_program_promotions.php'); }

if (!empty($global_page_content)) { echo $global_page_content; }

include_once('../php/logout.php');

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

if ($_SESSION['page']['use_title_bar']) { include_once('../includes/off_page_canvas_2.php'); }

// Begin rest of body content for page.

if ($_SESSION['page']['use_footer']) { include_once('../includes/page_footer.php'); }

if ($_SESSION['page']['use_disclaimer']) { include_once('../includes/disclaimer.php'); }

include_once('../includes/foundation_footer.php'); 

if ($_SESSION['page']['use_system_log']) { include_once('php/system_log_page_access.php'); }

?>



