<?php 

// echo 'Begin Member Dashboard page.<br />';

include_once('../includes/header.php');
	
getPage('Member Dashboard', $connection); 

// show_session();
// die;

if ($_SESSION['page']['use_authorization']) { include_once('../includes/authorization.php'); }
// include_once('../includes/authorization.php');

include('../includes/page_header.php');

if (!isset($_GET['e_id'])) {
	if ($_SESSION['page']['use_menu']) { include_once('../includes/menu.php'); }
	// include('../includes/menu.php'); 
	// Moved to Enrollment section below so that dynamic menu can display latest User Enrollment selection.
}

if ($_SESSION['page']['ecommerce']) {
	include_once('../'.STORE_FOLDER_NAME.'/includes/store_header.php');
}

// Member Dashboard page can be called from:
// 1. Select Program Enrollment page (select-program-enrollment.php)
// 2. Member Dashboard page (list_user_program_enrollments.php)
// 3. Menu system anytime after Login

// When calling page is select-program-enrollment.php or list_user_program_enrollments.php
// https://supplementrelief.com/member-dashboard/?e_id=3905

// Enrollment
if (isset($_GET['e_id'])) {
	$enrollment_id = $_GET['e_id'];
	$_SESSION['enrollment']['enrollment_id'] = $enrollment_id;
	unset($_SESSION['enrollment']['course_id']);

	// Get these data values:
	// 1. enrollments: person_role_id, price_type_code, forum_moderator
	// 2. project_programs: id, name, calendar_id 
	// 3. projects: id, name, calendar_id, FK: host_url from images for project logo to display in the Tools Menu section
	// 4. programs: id, name FK: host_url from images for project logo to display in the Tools Menu section
	// 5. content_assets: course_id - some Enrollments may NOT have a Course so this can be empty
	
	$queryEnrollment = 'SELECT 
	en.id AS enrollment_id, 
	en.person_role_id, 
	en.project_program_id, 
	en.price_type_code, 
	en.effective_timestamp AS enrollment_effective_timestamp, 
	en.primary_enrollment, 
	en.forum_moderator, 
	pp.project_id, 
	pp.program_id, 
	pp.name AS project_program_name, 
	pp.use_topic_scheduling, 
	pp.use_navigation, 
	pp.use_bookmarks,  
	pp.enable_auto_logoff, 
	pp.auto_logoff_time, 
	pp.calendar_id AS project_program_calendar_id, 
	pj.id AS project_id, 
	pj.name AS project_name, 
	pj.calendar_id AS project_calendar_id, 
	pg.id AS program_id, 
	pg.name AS program_name, 
	pji.host_url AS project_logo_strip_url, 
	pgi.host_url AS program_logo_strip_url /*, 
	ppca.id AS ppca_id , 
	ca.id AS course_id */   
	FROM enrollments en 
	LEFT JOIN project_programs pp ON en.project_program_id = pp.id 
	LEFT JOIN projects pj ON pp.project_id = pj.id 
	LEFT JOIN programs pg ON pp.program_id = pg.id 
	LEFT JOIN images pji ON (pj.image_id = pji.content_asset_id  AND pji.size = "LOST")  
	LEFT JOIN images pgi ON (pg.image_id = pgi.content_asset_id  AND pgi.size = "LOST") 
	/* LEFT JOIN project_program_content_assets ppca ON pp.id = ppca.project_program_id 
	LEFT JOIN content_assets ca ON (ppca.content_asset_id = ca.id AND ca.content_asset_type_code = "COURS") */    
	WHERE en.id = '.$enrollment_id.' 
	AND en.active = 1 
	LIMIT 1';
	
	// echo $queryEnrollment.'<br /><hr />';
				
	$result_enrollment = mysqli_query($connection, $queryEnrollment);
	
	if (!$result_enrollment) {
		show_mysqli_error_message($queryEnrollment, $connection);
		die;
	}
				
	$r = mysqli_fetch_assoc($result_enrollment);
	
	// See if there is a Course associated with this Enrollment.				
	$queryProjectProgramCourse = 'SELECT 
	ca.id AS course_id 
	FROM project_program_content_assets ppca
	LEFT JOIN content_assets ca ON (ppca.content_asset_id = ca.id AND ca.content_asset_type_code = "COURS")     
	WHERE ppca.project_program_id = '.$r['project_program_id'].' 
	LIMIT 1';
	
	// echo $queryEnrollment.'<br /><hr />';
		
	$result_course = mysqli_query($connection, $queryProjectProgramCourse);
	
	if (!$result_course) {
		show_mysqli_error_message($queryProjectProgramCourse, $connection);
		die;
	}
	
	$course = mysqli_fetch_assoc($result_course);
	
	// show_array($course);
	
	$course_id = $course['course_id'];
	
	// echo 'Course ID: '.$course_id.'<br>';
	
	mysqli_free_result($result_course);
 					
	// show_array($r);
	// Set Session variables
	// Put this into structured multidimensional arrays when time permits.
	// Downstream code (particularly the Store, is looking for these variables at the root level so will fail if not renamed.
	// $_SESSION['enrollment']['...']
	// $_SESSION['project_program']['...']
	// $_SESSION['project']['...']
	// $_SESSION['program']['...']
	// $_SESSION['education']['...']
	
	// Set the Session variables for this Enrollment.
	if (strlen($r['person_role_id']) > 0) $_SESSION['enrollment']['person_role_id'] = $r['person_role_id'];
	if (strlen($r['enrollment_effective_timestamp']) > 0) $_SESSION['enrollment']['enrollment_effective_timestamp'] = $r['enrollment_effective_timestamp'];
	if (strlen($r['forum_moderator']) > 0) $_SESSION['enrollment']['discussion_forum_moderator'] = $r['forum_moderator'];
	if (strlen($r['price_type_code']) > 0) $_SESSION['enrollment']['user_price_type'] = $r['price_type_code'];
	
	if (strlen($r['project_program_id']) > 0) $_SESSION['enrollment']['project_program_id'] = $r['project_program_id'];
	
	if (strlen($r['use_navigation']) > 0) {
		$_SESSION['enrollment']['use_navigation'] = $r['use_navigation'];
	} else { 
		unset($_SESSION['enrollment']['use_navigation']);
	}

	if (strlen($r['use_topic_scheduling']) > 0) {
		$_SESSION['enrollment']['use_topic_scheduling'] = $r['use_topic_scheduling'];
	} else { 
		unset($_SESSION['enrollment']['use_topic_scheduling']);
	}
	
	// Added by Jay 3/9/17 for data-driven control of Bookmarks feature 
	if (strlen($r['use_bookmarks']) > 0) {
		$_SESSION['enrollment']['use_bookmarks'] = $r['use_bookmarks'];
	} else { 
		unset($_SESSION['enrollment']['use_bookmarks']);
	}

	// Added by Jay 1/23/17 for data-driven control of Auto Logoff for Project Program 
	if (strlen($r['enable_auto_logoff']) > 0) {
		$_SESSION['enrollment']['enable_auto_logoff'] = $r['enable_auto_logoff'];
		$_SESSION['enrollment']['auto_logoff_time'] = $r['auto_logoff_time'];
	} else { 
		unset($_SESSION['enrollment']['enable_auto_logoff']);
		unset($_SESSION['enrollment']['auto_logoff_time']);
	}

	if (strlen($r['project_program_name']) > 0) $_SESSION['enrollment']['project_program_name'] = $r['project_program_name'];
	if (strlen($r['project_program_calendar_id']) > 0) $_SESSION['enrollment']['project_program_calendar_id'] = $r['project_program_calendar_id'];
	
	if (strlen($r['project_id']) > 0) $_SESSION['enrollment']['project_id'] = $r['project_id'];
	if (strlen($r['project_name']) > 0) $_SESSION['enrollment']['project_name'] = $r['project_name'];		
	if (strlen($r['project_calendar_id']) > 0) $_SESSION['enrollment']['project_calendar_id'] = $r['project_calendar_id'];
	if (strlen($r['project_logo_strip_url']) > 0) $_SESSION['enrollment']['project_logo_strip_url'] = $r['project_logo_strip_url'];
	
	if (strlen($r['program_id']) > 0) $_SESSION['enrollment']['program_id'] = $r['program_id'];
	if (strlen($r['program_name']) > 0) $_SESSION['enrollment']['program_name'] = $r['program_name'];
	if (strlen($r['program_logo_strip_url']) > 0) $_SESSION['enrollment']['program_logo_strip_url'] = $r['program_logo_strip_url'];
	
	// if (strlen($r['course_id']) > 0) $_SESSION['enrollment']['course_id'] = $r['course_id'];
	// $_SESSION['enrollment']['course_id'] = $r['course_id']; // Even if empty this needs to be set because it controls the behavior of the Course Outline.
	$_SESSION['enrollment']['course_id'] = $course['course_id']; // Even if empty this needs to be set because it controls the behavior of the Course Outline.
	
	// Can we re-execute the menu to update a potential change in the User selected Program?
	if ($_SESSION['page']['use_menu']) { include_once('../includes/menu.php'); }
	// include('../includes/menu.php');
			
}
	
// Is this still needed?		
$project_id = $_SESSION['enrollment']['project_id']; // Where is this used on Member Dasboard page?
$program_id = $_SESSION['enrollment']['program_id']; // Where is this used on Member Dasboard page?

// Course
if (isset($_GET['course_id'])) {
	// Can only be set in course_outline_query.php which is called from Member Dashboard or Education pages only.
	$course_id = $_GET['course_id'];
	$_SESSION['enrollment']['course_id'] = $course_id; 
	
}	else {
 	$course_id = $_SESSION['enrollment']['course_id'];
 	 // Why is this needed? Can't get to this page without first Logging In and then Selecting a Program Enrollment				
} 

// Topic
// Can only be set in course_outline_query.php which is called from Member Dashboard or Education pages only.
// If coming from the Select Program Enrollment page Topic will not be known yet as only the Course ID is known.
if (isset($_GET['tid'])) {
	$topic_id = $_GET['tid'];
	$_SESSION['enrollment']['topic_id'] = $topic_id;
		
}  else {
	 // Means user is returning to Member Dashboard page from somewhere other than using the Course Outline navigation (likely from a Menu option) which explicitly sets the tid in the URL parameter. Also implies that the tid was set earlier in the SESSION and stored as a SESSION variable for this very purpose so that the user is returned to the correct Topic whenever they leave the Member Dashboard page and then return later.
	 $topic_id = $_SESSION['enrollment']['topic_id'];
}

// Web Page
// Can only be set in course_outline_query.php which is called from Member Dashboard or Education pages only.
// If coming from the Select Program Enrollment page Topic will not be known yet as only the Course ID is known.
if (isset($_GET['wpid'])) {
	$web_page_id = $_GET['wpid'];
	$_SESSION['enrollment']['web_page_id'] = $web_page_id;
		
}  else {

	 // Means user is returning to Member Dashboard page from somewhere other than using the Course Outline navigation (likely from a Menu option) which explicitly sets the wpid in the URL parameter. Also implies that the wpid was set earlier in the SESSION and stored as a SESSION variable for this very purpose so that the user is returned to the correct Web Page whenver they leave the Member Dashboard page and then return later.
	 $web_page_id = $_SESSION['enrollment']['web_page_id'];
	 // $_SESSION['enrollment']['web_page_id'] may have been previously set if the user selected a Topic from the Course Outline which always then goes to the Education page.
	 
}

//show_session();

include('../php/system_log_page_access.php'); 

if ($_SESSION['page']['use_title_bar']) { include_once('../includes/off_page_canvas_1.php'); }
// include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

// show_session();

// See if User has any Orders to display.	
// Query the Orders
$queryOrders = 'SELECT 
	ord.id, 
	ord.user_session_id, 
	ord.order_status, 
	ord.order_project_id,  
	ord.created_timestamp, 
	ord.invoice_num, 
	ord.shipped_by, 
	ord.shipped_date, 
	ord.shipped_carrier, 
	ord.shipped_carrier_waybill, 
	ord.first_name, 
	ord.last_name, 
	ord.city, 
	ord.state,
	ord.zip_code, 
	ord.country, 
	ord.email, 
	ord.shipping_first_name, 
	ord.shipping_last_name, 
	ord.shipping_city, 
	ord.shipping_state, 
	ord.shipping_zip_code, 
	ord.shipping_country,
	ord.shipping_email,  
	ord.shipping_option_type_name, 
	ord.user_id, 
	ord.user_name, 
	ord.card_type, 
	ord.authorization_code,
	ord.product_retail_amount,  
	ord.product_subtotal_amount,
	ord.shipping_total,
	ord.additional_charge_amount,  
	ord.product_cost_amount, 
	ord.shipping_cost_amount,
	ord.discount_amount, 
	ord.customer_service_allowance_amount, 
	ord.promotion_allowance_amount, 
	ord.total_tax, 
	ord.total_amount, 
	us.http_referer AS referring_uri, 
	us.browser_device_type, 
	us.browser_platform, 
	us.browser_name, 
	us.browser_version, 
	us.request_uri  
	FROM orders ord  
	LEFT JOIN user_sessions us ON ord.user_session_id = us.id 
	WHERE ord.user_id = "'.$_SESSION['user']['user_id'].'"
	AND ord.order_status in ("1", "2", "3", "4") 
	ORDER BY ord.created_timestamp DESC ';
						
// echo $queryOrders.'<br>';
// die();

$result_query = mysqli_query($connection, $queryOrders);

$total_orders = mysqli_num_rows($result_query);
// echo 'Total Orders: '.$total_orders.'<br>';

?>

<div class="panel">

	<div class="row">
		<div class="small-12 columns">
			<div id="userWelcomeMessage" class="panel1">
    						
				<h3><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Welcome<?php echo ($_SESSION['user']['first_name'] ? ' '.$_SESSION['user']['first_name'].'!' : ''); ?></h3>
				
				<hr>
				
				<?php
    				        			
                if ($total_orders > 0) { 
                    $orders_message = ' You may review your Order history on the <b>Orders</b> tab below.';
                }
				
				/*	
				$welcome_message .= 
				'<p>Go shopping in the <b>Store</b> for <a href="../numedica/" title="Shop for NuMedica supplements.">NuMedica</a> supplements.'.$orders_message.'</p>
				 <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom radius " title="NuMedica provides professional-grade supplements used by Physicians and Nutritionist around the world."><a href="../numedica/"><img src="https://cdn-manager.net/media/images/numedica-logo-landscape-small.png" alt="NuMedica Advancing Nutrition logo"></a></span>
				<hr>';
				*/
				
				if ($_SESSION['enrollment']['project_program_id'] == 21) {
					// SupplementRelief Member Account
					
					$welcome_message .= '
					<div class="row">		
						<div class="small-12 medium-3 columns">
							<span data-tooltip aria-haspopup="true" class="has-tip tip-bottom radius " title="NuMedica provides professional-grade supplements used by Physicians and Nutritionist around the world."><a href="../numedica/"><img src="https://cdn-manager.net/media/images/numedica-logo-landscape-small.png" alt="NuMedica Advancing Nutrition logo"></a></span>					
						</div>
						
						<div class="small-12 medium-9 columns imageWrapTopMargin">
							<a href="../numedica/" class="button large radius expand" title="shop for NuMedica supplements on SupplementRelief"><i class="fa fa-shopping-cart fa-1x"></i>  Shop for NuMedica</a>
						
						</div>
	
					</div>';		
			
				}
					
				if (!empty($_SESSION['enrollment']['project_program_name']) && !empty($_SESSION['enrollment']['course_id'])) {	
									
					$welcome_message .= '<p>Please check your <a href="#news">News</a> tab below for your <b>'.$_SESSION['enrollment']['project_program_name'].'</b> program. You can select a <b>Topic</b> from the <b>Course Outline</b> to access your program content. You may select a different <a href="#programs">Program</a> if you are currently enrolled into more than one. You may also update your <a href="#account">Account</a> and <a href="#addresses">Address</a> information. You can return to your <b>Member Dashboard</b> at any time by selecting&nbsp;&nbsp;<b><i class="fa fa-user icon-red"></i>&nbsp;'.$_SESSION['user']['full_name'].'</b>&nbsp;from the main menu.</p>';	
													
				} elseif (!empty($_SESSION['enrollment']['project_program_name'])) {	
									
					$welcome_message .= '<p>Please check your <a href="#news">News</a> tab below for your <b>'.$_SESSION['enrollment']['project_program_name'].'</b> program. You may select a different <a href="#programs">Program</a> if you are currently enrolled into more than one. You may also update your <a href="#account">Account</a> and <a href="#addresses">Address</a> information. You can return to your <b>Member Dashboard</b> at any time by selecting&nbsp;&nbsp;<b><i class="fa fa-user icon-red"></i>&nbsp;'.$_SESSION['user']['full_name'].'</b>&nbsp;from the main menu.</p>';		
				
				} else {	
						
					$welcome_message .= '<p>You may update your <b>Account</b> or <b>Addresses</b>.</p>';			
					
				}
				
				if ($orders_message) { $welcome_message .= $orders_message; }			
				
				echo $welcome_message;
				
				?>
			
			</div>
		</div>
		
	</div>
	
</div>

<div class="row">
	
	<?php
	
	if (!empty($course_id)) { 
		// echo 'An Enrollment containing a Course has been selected.<br>';
		
		?>

		<div class="small-12 medium-4 columns">
		
			<!-- Wright Path Campaign promotion -->		
			<?php // include('../php/select_wright_path_campaign.php'); ?>
			
			<!-- Course Outline navigation -->
			<?php 
				
			if (!empty($course_id)) {
				include('../php/course_outline_query.php');			
			}
				 			
			?>
			
			<div id="educationSidebar">
				
				<dl class="accordion" data-accordion>
    				<dd class="accordion-navigation">
    				  	<a href="#courseOutlineLinks">
    				  		<div class="row">
    				  		    <div class="small-9 columns">
    				  		      <div><h4><?php echo $course_title; ?></h4></div>
    				  		   </div>  
    				  		   <div class="small-3 columns">
    				  		      <div class="termIcon"><i class="fa fa-plus right"></i></div>
    				  		   </div>
    				  		</div>
    				    </a> 	
    				    <div id="courseOutlineLinks" class="content active"><?php echo $course_topic_block; ?></div>
                    </dd>
				</dl>	
			</div>
					
			<!-- Program Logo display -->
			<?php
			
			$queryProgramLogo = 'SELECT 
			im.host_url, 
			im.caption, 
			im.alt_text  
			FROM programs pg 
			LEFT JOIN images im ON 
				(pg.image_id = im.content_asset_id AND 
				 im.size = "Program" AND 
				 im.usage_size = "Medium" AND 
				 im.usage_shape = "Block") 
			WHERE pg.id = '.$_SESSION['enrollment']['program_id'].' 
			LIMIT 1';
			
			// echo $queryProgramLogo . '<br /><hr />';
			
			$result_program_logo = mysqli_query($connection, $queryProgramLogo);
			
			if (!$result_program_logo) {
				show_mysqli_error_message($queryProgramLogo, $connection);
				die;
			}
			
			while($row = mysqli_fetch_assoc($result_program_logo)) {		
				$_SESSION['enrollment']['project_logo_image_url'] = $row['host_url'];	
				$_SESSION['enrollment']['project_logo_caption'] = $row['caption'];	
				$_SESSION['enrollment']['project_logo_alt_text'] = $row['alt_text'];				
			}
												
			mysqli_free_result($result_program_logo);
			
			// Project Logo display
			if (!empty($_SESSION['enrollment']['project_logo_image_url'])) {
				// echo '<center><img src="'.$_SESSION['project_logo_image_url'].'" style="width:100%"></center>';
				echo 
				'<center><img class="th" src="'.$_SESSION['application']['root_media_host_url'].$_SESSION['enrollment']['project_logo_image_url'].'" alt="'.$_SESSION['enrollment']['project_logo_alt_text'].'"></center>
				<div class="imageCaption"><i class="fa fa-picture-o"></i>&nbsp;&nbsp;'.$_SESSION['enrollment']['project_logo_caption'].'</div>
				<br />';			
			} else {
				echo 
				'<center><img class="th" src="https://placehold.it/400x400" alt="placeholder image"></center>
				<div class="imageCaption"><i class="fa fa-picture-o"></i>&nbsp;&nbsp;Program image and caption needed!</div>
				<br />';			
			}
			
			?>
		
		</div>
		
		<?php
		
	} // END (!empty($course_id)) { 
	
	if (!empty($course_id)) { 		
		?>	
		<div class="small-12 medium-8 columns">
		<?php			
	} else {		
		?>	
		<div class="small-12 columns">
		<?php
	}		
			
	show_session_message(); 
		
	?>

	<dl id="userTabs" class="tabs" data-options="deep_linking:true; scroll_to_content:false;" data-tab>
		
		<?php			  
		  if (!empty($_SESSION['enrollment']['project_program_id'])) {
			  echo '
			  <dd class="active"><a href="#account">Account</a></dd>			  
			  <dd><a href="#addresses">Address</a></dd>';
			  echo '<dd><a href="#news">News</a></dd>';
			  if ($total_orders > 0) { echo '<dd><a href="#orders">Orders</a></dd>'; }
			  echo '<dd><a href="#programs">Programs</a></dd>';
		  } else {
			  echo '
			  <dd class="active"><a href="#account">Account</a></dd>
			  <dd><a href="#addresses">Address</a></dd>';	  	 			  
		  }
		?>
		
	</dl>
	
	<div class="tabs-content">
		
		<?php			  

		// Show Account
		if (!empty($_SESSION['enrollment']['project_program_id'])) { 
			?>
			<!-- Show Account -->				
		  	<div class="content active" id="account">
			<?php
		} else {
			?>
			<div class="content active" id="account">
			<?php
		}
		?>		
    			<!-- Display the Edit Person Form -->
				<div id="editPersonForm">
    			  	<?php include('../php/edit_person_form.php'); ?>
    			</div>	  																				
		    </div> <!-- END Tab Content section for Account -->
		   
		<!-- Display all of the User's Locations (Addresses) -->
		<div class="content" id="addresses">
	  
	  		<!-- Display the button for invoking the New Address Form -->	  		  		  
	  		<div id="newPersonLocationLink">
	  			<a href="#" class="button small radius" onclick="newPersonLocation();return false">New Address</a>
	  			<p>Shipping and Billing addresses <b>are used for shopping in the Store</b> so you do not have to fill in the information on the <b>Order Checkout form</b> each time you shop.</p>
	  		</div>
	  	
			<div id="newPersonLocationForm" class="hide">
					
				<?php include('../php/new_person_location_form.php'); ?>
						
				<!-- <a class="close-reveal-modal">&#215;</a> -->
				
			</div>
					
			<div id="editPersonLocationForm">
			
			</div>
		
			<div id="listPersonLocations">
			  
				<?php include('../php/list_person_locations.php'); ?>
				
			</div>		
			
	  	</div> <!-- END Tab Content section for Addresses -->
	  		
	  	<?php	  	
		// Show Project Program News  	
		if (!empty($_SESSION['enrollment']['project_program_id'])) {
			
			?>
			<!-- Show Program Offering News -->
			<div class="content" id="news">   
				<?php include('../php/show_project_program_news_posts.php'); ?>  	 
	  		</div>
	  		<?php
			  	  
		}

		// Show User Orders 				  
		if (!empty($_SESSION['enrollment']['project_program_id'])) {
			
			?>
			<!-- Show all of Orders for this User if they have any. -->
			<div class="content" id="orders">
				<?php include('../php/show_customer_orders.php'); ?>
	  		</div>	
	  		<?php
			  	  
		}
		
		// Show User Program Enrollments	  	  
		if (!empty($_SESSION['enrollment']['project_program_id'])) {
			
			?>
			<!-- Show all of the active Program Offerings the User is currently enrolled into. -->
			<div class="content" id="programs">
				<?php include('../php/list_program_enrollments.php'); ?>
	  		</div>	
	  		<?php
			  	  
		}
		?>
			     
	</div> <!-- END Foundation TAB <div class="tabs-content"> -->

	</div> <!-- END <div class="small-12 medium 8 columns"> -->	
</div> <!-- END <div class="row"> -->

<?php 
	
// include('../php/standard_membership_modal.php');

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->
if ($_SESSION['page']['use_title_bar']) { include_once('../includes/off_page_canvas_2.php'); }
// include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

if ($_SESSION['page']['use_footer']) { include_once('../includes/page_footer.php'); }
// include('../includes/page_footer.php');

if ($_SESSION['page']['use_disclaimer']) { include_once('../includes/disclaimer.php'); }
// include('../includes/disclaimer.php');

include('../includes/foundation_footer.php');

if ($_SESSION['page']['ecommerce']) { include_once('../'.STORE_FOLDER_NAME.'/includes/footer.php'); }

if ($_SESSION['page']['use_system_log']) { include_once('php/system_log_page_access.php'); }

?>

<!-- JS and JQuery for Account, Contacts and Addresses tabs. -->
<script type="text/javascript" src="../js/datepicker.js"></script>
<!-- <script type="text/javascript" src="../js/maskfield.js"></script> -->
<script type="text/javascript" src="../js/user_dashboard.js"></script>