
List of minified files-:

-> /js:
1. ajax_discussion_forum.js     -   ajax_discussion_forum.min.js
2. ajax_product_option.js       -   ajax_product_option.min.js 
3. app.js                       -   app.min.js
4. bookmark_page.js             -   bookmark_page.min.js
5. predicitive_search.js        -   predicitive_search.min.js

-> /store/js
1. store.js                     -   store.min.js

-> /stylesheets:
1. supplementrelief.css         -   supplementrelief.min.css

-> /bower_components/modernizr/
1. modernizr.js                 -   modernizr.min.js
