<?php 

// echo 'Begin SOZO News page.<br />';

include_once('../includes/header.php');

$_SESSION['page']['page_id'] = 17;

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

if (!isset($_SESSION['enrollment']['course_id'])) {
	// echo 'Session Course ID is not set';
	$_SESSION['message_type'] = 'alert-box alert radius';				
	$_SESSION['message'] = '<p>Please select a <strong>Program Enrollment</strong> from the listing before proceeding.</p>';		
	header("location: ../select-program-enrollment/");	
	// header("location: ../user_dashboard/index.php");
	exit();	
}

include('../php/show_project_program_news_posts.php'); 

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php'); 

?>

