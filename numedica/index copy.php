<?php 

// echo 'Begin NuMedica page.<br />';

// error_reporting(E_ALL);

include_once('../includes/header.php');

getPage('NuMedica Supplements', $connection);

if ($_SESSION['page']['use_authorization']) { include_once('../includes/authorization.php'); }

include('../includes/page_header.php');

// include('../includes/menu.php');

if ($_SESSION['page']['use_menu']) { include_once('../includes/menu.php'); }

if ($_SESSION['page']['ecommerce']) {
	include_once('../'.STORE_FOLDER_NAME.'/includes/store_header.php');
}
 
// For non e-Commerce pages menu.php ends with </div> which is inside of a "fixed" i.e. "sticky" div which is closed already.
// Below we want to include an e-Commerce header inside of the sticky area so we must manually close the "sticky" div 

	include('../php/e-commerce-header-products.php');
	?>
</div>
<?php

if ($_SESSION['page']['use_title_bar']) { 
	
	include_once('../includes/off_page_canvas_1.php');
	echo '<br>'; 
}	
// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 
	
if ($_SESSION['page']['promotions'] == 1) { include('../php/show_project_promotions.php'); }

// echo '<br><br><br><br><br><br><br>'; // Use so Product Search form will display in IE9.		
include('../php/search_products.php');
include('../php/search_products_form.php');
include('../php/search_products_results_footer.php');

if (!empty($global_page_content_summary)) { echo $global_page_content_summary; }
	
include('../php/show_project_brand_products.php');

if (!empty($global_page_content)) { echo $global_page_content; }
    
// End body content that will appear inside of the Off Canvas <section class="main-section"> -->
if ($_SESSION['page']['use_title_bar']) { include_once('../includes/off_page_canvas_2.php'); }

// Begin rest of body content for page.

if ($_SESSION['page']['use_footer']) { include_once('../includes/page_footer.php'); }

if ($_SESSION['page']['use_disclaimer']) { include_once('../includes/disclaimer.php'); }

include_once('../includes/foundation_footer.php'); 

if ($_SESSION['page']['use_system_log']) { include_once('../php/system_log_page_access.php'); }


	?>
	<!-- This js is only needed on the NuMedica page. -->
	<script>
		$(document).ready(function(){	
		    $("#toggleAdvancedSearch").click(function () {
	        	// $("#advancedSearchForm").toggleClass("hidden unhidden");
				$( "#advancedSearchForm" ).toggle( "slow" );
	    	});    
	  });
	</script>
	<script type="text/javascript" src="../js/predictive_search.js"></script>
	<?php
			
	// This js is only needed on pages that use the Slick Slider.
	if ($_SESSION['page']['promotions'] == 1) {
		// Slick is an open source slider that is used to show Promotions on this page when promotions are enabled.
		?>
		<script type="text/javascript" src="../js/slick.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
		        $(".promotion-slider").show();
		        $('.promotion-slider').slick({
		         autoplay: true,
		         autoplaySpeed: 7000,	
		         dots: true, 
		         slidesToShow: 1,
		         slidesToScroll: 1
		        });
			});
		</script>
		<?php
	}

	if ($_SESSION['page']['ecommerce']) { include_once('../'.STORE_FOLDER_NAME.'/'.STORE_FOOTER); }

?>