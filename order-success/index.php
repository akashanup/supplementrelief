<?php 

// echo 'Begin Order Success page.<br />';

include_once('../includes/header.php');

getPage('Order Success', $connection);

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../php/system_log_page_access.php'); 

include('../includes/menu.php');

include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

include('../php/order_success.php'); 

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

?>

<!-- Google Adwords and Bings Ads tracking code. -->
<!-- <script src="../js/pay_per_click.js"></script> -->

<!-- After successful purchase syncronization with PPC (pay-per-click) marketing. -->

<!-- 06/20/2016 Google Code for Sales Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 879537895;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "MYV8CLT42WcQ592yowM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/879537895/?label=MYV8CLT42WcQ592yowM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- 10/21/15 Bing Code for SupplementRelief Bing Ads PPC Conversion Page -->
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"4074706"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=4074706&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

<?php
	
include('../php/google_ecommerce_analytics_tracking.php');

include('../includes/foundation_footer.php'); 

if(isset($_SESSION['coupon']['coupon_code']))
{
	unset($_SESSION['coupon']['coupon_code']);
}
if(isset($_SESSION['coupon']['coupon_code_message']))
{
	unset($_SESSION['coupon']['coupon_code_message']);
}
?>
<script type="text/javascript">
	sessionStorage.setItem("free_sampleValue", 0);
	sessionStorage.setItem("checkout_comments", '');
</script>