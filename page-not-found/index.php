<?php 
header("HTTP/1.1 404 Not Found");

// echo 'Begin Page Not Found page.<br />';

include_once('../includes/header.php');

getPage('Page Not Found', $connection); 

// $_SESSION['page']['page_id'] = 119;

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

// include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

?>

<!--
<pre>
<?php print_r(headers_list()); ?>
</pre>
-->

<?php // show_array($_SERVER); ?>	
<?php // show_array($_SESSION); ?>
   
<div class="row">		
	<div class="small-12 columns">
    	
        <br>
        	
        <h1><i class="fa fa-exclamation-triangle icon-red"></i>&nbsp;&nbsp;Requested Page Not Found</h1>
    
        <p><b>You are on SupplementRelief.com</b>. We sell <a href="../numedica/" title="Shop for NuMedica supplements online.">NuMedica Supplements</a> and provide online <b>Wellness</b> educational content</b>.</p>
        
        <p>The page you are looking for <?php echo '<b>'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SESSION['USER_REQUEST_URI'].'</b>'; ?> is not available. It may have been removed, replaced or the URL provided may contain a typo.</p>
               			
	</div>
</div>

<div class="row">		
	<div class="small-12 medium-4 columns">
		<center><a href="../numedica/" title="Shop for NuMedica supplements online."><img class="th imageWrapTopMargin" src="https://cdn-manager.net/media/images/numedica-logo-medium-block.jpg"></a></center>				
        <div class="caption"><b>Shop for NuMedica Supplements</b></div>	
	</div>
    <div class="small-12 medium-4 columns">
		<center><a href="../sitemap/" title="View a directory of all of the SupplementRelief website pages."><img class="th imageWrapTopMargin" src="https://cdn-manager.net/media/images/sitemap-medium-block.jpg"></a></center>
        <div class="caption"><b>Sitemap Directory of Pages</b></div>	
	</div>
	<div class="small-12 medium-4 columns">
		<center><a href="../contact/" title="Call or email SupplementRelief if we can help you find anything or answer a question."><img class="th imageWrapTopMargin" src="https://cdn-manager.net/media/images/contact-us-medium-block.jpg"></a></center>				
<div class="caption"><b>We are Here to Help</b></div>	
	</div>
</div>
   
<div class="row">		
	<div class="small-12 columns">
    	<hr>
        <p>Please <b>bookmark your favorite page(s)</b> on our website. Call <b>(888) 424-0032</b> or <a href="../contact/" title="Email Customer Support at SupplementRelief.">email</a> if we can help you find what you are looking for.</p>
        <center><img src="https://cdn-manager.net/media/images/supplementrelief-logo-blue-landscape-large.jpg" width="400px"></center>
        <br>
	</div>
</div>

<br>
    
<?php // echo $global_page_content; // Set in page_header.php ?>					

<?php
		
// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../php/system_log_page_access.php'); 

// include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php'); 

?>