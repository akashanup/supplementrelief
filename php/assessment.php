<?php 

// echo 'Begin assessment.php.<br />';
// SupplementRelief
// Renders an Assessment for an Education Topic

// https://supplementrelief.com/assessment/?id=2422

include_once('../includes/header.php');
include('../includes/assessment_functions.php');

error_reporting(ALL);

// show_array($_GET);
// show_array($_SESSION);

// Process $_GET paramaters
// Only happens first time Assessment page is displayed
if (isset($_GET['id']))
{
	// Only happens first time Assessment page is accessed.
	// Sets up the Assessment Header info
	
	// unset($_SESSION['assessment']);
	
	if (!isset($_SESSION['assessment']['id'])) {
		
		$_SESSION['assessment']['created_timestamp'] = date('Y/m/d H:i:s');	 

		$_SESSION['assessment']['assessment_id'] = safe_sql_data($connection, $_GET['id']);
		
		$_SESSION['assessment']['user_session_id'] = $_SESSION['userStatistics']['session_id'];
				
		// Get the data for the Assessment
		$queryAssessment = 'SELECT 
		am.content_asset_id, 
		am.instructions, 
		am.randomize_items, 
		am.enable_remediation, 
		am.enable_retake, 
		am.max_retake_times, 
		am.discussion_forum, 
		ca.id AS assessment_id, 
		ca.title,
		ca.version,  
		ca.summary_description, 
		ca.description,
		at.name AS assessment_type_name,
		art.name AS assessment_rubric_type_name, 
		im.host_url AS assessment_image_url, 
		im.caption AS assessment_image_caption, 
		im.alt_text AS assessment_image_alt_text, 
		vi.host_url_mp4 AS assessment_video_url_mp4, 
		vi.host_url_webm AS assessment_video_url_webm, 
		vi.caption AS assessment_video_caption, 
		vi.duration_minutes_seconds AS assessment_video_duration_minutes_seconds, 
		imvi.host_url AS assessment_video_poster_url, 
		dfc.discussion_thread_post, 
		dfc.discussion_thread_post_audio, 
		dfc.discussion_thread_post_document, 
		dfc.discussion_thread_post_image, 
		dfc.discussion_thread_post_video,
		dfc.discussion_reply_post,
		dfc.discussion_reply_post_audio,  
		dfc.discussion_reply_post_document, 
		dfc.discussion_reply_post_image, 
		dfc.discussion_reply_post_video 
		FROM assessments am  
		LEFT JOIN content_assets ca ON am.content_asset_id = ca.id 
		LEFT JOIN assessment_types at ON am.assessment_type_code = at.code 
		LEFT JOIN assessment_rubric_types art ON am.assessment_rubric_type_code = art.code  
		LEFT JOIN images im ON  
		(am.image_id = im.content_asset_id AND 
		 im.SIZE = "Assessment" AND 
		 im.usage_size = "Medium" AND 
		 im.usage_shape = "Block" AND 
		 im.format = "JPG")
		LEFT JOIN videos vi ON am.video_id = vi.content_asset_id 
		LEFT JOIN images imvi ON 
			(vi.image_id = imvi.content_asset_id AND 
			 imvi.SIZE = "Video" AND 
			 imvi.usage_size = "Large" AND 
			 imvi.usage_shape = "Landscape" AND 
			 imvi.format = "JPG") 
		LEFT JOIN discussion_forum_configurations dfc ON am.content_asset_id = dfc.assessment_id  		
		WHERE am.content_asset_id = '.$_SESSION['assessment']['assessment_id'].' 
		LIMIT 1'; 
	
		// echo $queryAssessment . '<br /><hr />';
		// die;
	
		$result_assessment = mysqli_query($connection, $queryAssessment);
		
		if (!$result_assessment) {
			show_mysqli_error_message($queryAssessment, $connection);
			die;
		}
		
		$a = mysqli_fetch_assoc($result_assessment);
	
		// show_array($a);
		
		$_SESSION['assessment']['type'] = $a['assessment_type_name'];
		$_SESSION['assessment']['rubric_type'] = $a['assessment_rubric_type_name'];
		$_SESSION['assessment']['title'] = $a['title'];
		$_SESSION['assessment']['version'] = $a['version'];
		$_SESSION['assessment']['summary_description'] = $a['summary_description'];
		$_SESSION['assessment']['description'] = $a['description'];
		
		$_SESSION['assessment']['instructions'] = $a['instructions'];
		$_SESSION['assessment']['randomize_items'] = $a['randomize_items'];
		$_SESSION['assessment']['enable_remediation'] = $a['enable_remediation'];
		$_SESSION['assessment']['enable_retake'] = $a['enable_retake'];
		$_SESSION['assessment']['max_retake_times'] = $a['max_retake_times'];
		
		if ($_SESSION['assessment']['randomize_items']) {
			$_SESSION['assessment']['items_order_by'] = 'RAND()';
		} else { 
			$_SESSION['assessment']['items_order_by'] = 'cau.seq ASC'; 
		}

		if ($a['assessment_image_url']) { 
			$_SESSION['assessment']['image_url'] = $_SESSION['application']['root_media_host_url'].$a['assessment_image_url'];
			$_SESSION['assessment']['image_caption'] = $a['assessment_image_caption'];

		} else {
			$_SESSION['assessment']['image_url'] = 'https://placehold.it/250x250?text=Image+250+x+250';
			$_SESSION['assessment']['image_caption'] = 'Image Caption goes here'; 
		}
		
		$_SESSION['assessment']['image_alt_text'] = $a['assessment_image_alt_text'];
		
		if ($a['assessment_video_url_mp4']) { 
			
			$_SESSION['assessment']['assessment_video_url_mp4'] = $a['assessment_video_url_mp4'];
			$_SESSION['assessment']['assessment_video_url_webm'] = $a['assessment_video_url_webm'];
			$_SESSION['assessment']['assessment_video_caption'] = $a['assessment_video_caption'];
			$_SESSION['assessment']['assessment_video_duration_minutes_seconds'] = $a['assessment_video_duration_minutes_seconds'];
			
			if ($a['assessment_video_poster_url']) { $_SESSION['assessment']['assessment_video_poster_url'] = $a['assessment_video_poster_url']; }
			
		}
		
		if ($a['discussion_forum']) {
			// Set session variables to persist the Discussion Forum Configuration for this Assessment
			$_SESSION['assessment']['discussion_forum']['forum'] = $a['discussion_forum'];	
			$_SESSION['assessment']['discussion_forum']['thread_post'] = $a['discussion_thread_post'];
			$_SESSION['assessment']['discussion_forum']['thread_post_audio'] = $a['discussion_thread_post_audio'];
			$_SESSION['assessment']['discussion_forum']['thread_post_document'] = $a['discussion_thread_post_document'];
			$_SESSION['assessment']['discussion_forum']['thread_post_image'] = $a['discussion_thread_post_image'];
			$_SESSION['assessment']['discussion_forum']['thread_post_video'] = $a['discussion_thread_post_video'];
			$_SESSION['assessment']['discussion_forum']['reply_post'] = $a['discussion_reply_post'];
			$_SESSION['assessment']['discussion_forum']['reply_post_audio'] = $a['discussion_reply_post_audio'];		
			$_SESSION['assessment']['discussion_forum']['reply_post_document'] = $a['discussion_reply_post_document'];	
			$_SESSION['assessment']['discussion_forum']['reply_post_image'] = $a['discussion_reply_post_image'];
			$_SESSION['assessment']['discussion_forum']['reply_post_video'] = $a['discussion_reply_post_video'];		

		} else {
			unset($_SESSION['assessment']['discussion_forum']);
		}
								
		mysqli_free_result($result_assessment);
		
		// Determine the total number of Assessment Items for the Assessment
		
		$content_asset_type_code = '("ASSESSITEM")';

		$queryAssessmentItems = 'SELECT 
			ai.content_asset_id  
			FROM content_asset_usages cau  
			LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id 
			LEFT JOIN assessment_items ai ON ca.id = ai.content_asset_id  
			WHERE cau.content_asset_parent_id = '.$_SESSION['assessment']['assessment_id'].' 
			AND ca.content_asset_type_code IN '.$content_asset_type_code.' 
			AND ai.active = 1';
		
		// echo $queryAssessmentItems . '<br /><hr />';
		// die;
		
		$result_assessment_item = mysqli_query($connection, $queryAssessmentItems);
		
		if (!$result_assessment_item) {
			show_mysqli_error_message($queryAssessmentItems, $connection);
			die;
		}
		
		$_SESSION['assessment']['total_items'] = mysqli_num_rows($result_assessment_item);
		$_SESSION['assessment']['current_item'] = 1;
		$_SESSION['assessment']['items_completed'] = 0;
		$_SESSION['assessment']['item_id_processed_list'] = '""'; 
		
		$_SESSION['assessment']['user_correct_responses'] = 0;
		$_SESSION['assessment']['user_incorrect_responses'] = 0;
				
		mysqli_free_result($result_assessment_item);
		
		// Create the Assessment Session record
					
		$queryInsert = '
		INSERT INTO assessment_sessions (created_timestamp, assessment_id, user_session_id, application_id, project_id, program_id, project_program_id, enrollment_id, person_id) 
		VALUES ( 
		"'.$_SESSION['assessment']['created_timestamp'].'", 
		"'.$_SESSION['assessment']['assessment_id'].'",
		'.no_value_null_check($_SESSION['assessment']['user_session_id']).',	
		'.no_value_null_check($_SESSION['application']['application_id']).',	
		'.no_value_null_check($_SESSION['enrollment']['project_id']).',	
		'.no_value_null_check($_SESSION['enrollment']['program_id']).',	
	    '.no_value_null_check($_SESSION['enrollment']['project_program_id']).',	
		'.no_value_null_check($_SESSION['enrollment']['enrollment_id']).',	
		'.no_value_null_check($_SESSION['user']['user_id']).')';
				
		// echo $queryInsert;	
		// die();
		
		$result_insert = mysqli_query($connection, $queryInsert);
				
		if (!$result_insert) {
			show_mysqli_error_message($queryInsert, $connection);
			die;
		}
		
		$_SESSION['assessment']['assessment_session_id'] = mysqli_insert_id($connection);
		
		// show_array($_SESSION['assessment']); 
		
		?>
		<!-- Display Assessment Header Section -->
		<div id="assessment-header" class="<?php $hide_assessment_header; ?>">
		
			<br />
						
			<div class="row">
					
				<div class="small-12 medium-4 columns">
					<center><img src="<?php echo $_SESSION['assessment']['image_url']; ?>" alt="<?php echo $_SESSION['assessment']['image_alt_text']; ?>" width="250"></center>
					<div class="caption"><?php echo $_SESSION['assessment']['image_caption']; ?></div>
					<br> 					
				</div>
				
				<div class="small-12 medium-8 columns">
					
					<h3><?php echo $_SESSION['assessment']['title']; ?></h3>
					
					<span class="label"><?php echo $_SESSION['assessment']['total_items']; ?>&nbsp;Questions</span>&nbsp;&nbsp;<span class="label"><?php echo $_SESSION['assessment']['type']; ?></span>&nbsp;&nbsp;<span class="label"><?php echo $_SESSION['assessment']['rubric_type']; ?></span>
					
					<br />
					<br />
	
					<?php echo $_SESSION['assessment']['summary_description']; ?>
					
					<?php 
						
					if ($_SESSION['assessment']['instructions']) {
						
						?>
						<div class="row">		
							<div class="small-12 columns">
								
								<div class="row panel callout">		
									<div class="small-12 columns">
										<h4>Instructions</h4>
										<?php echo $_SESSION['assessment']['instructions']; ?>					
									</div>
								</div>
							
							</div>
						</div>
						<?php
						
					}
											
					if ($_SESSION['assessment']['assessment_video_url_mp4']) {
						
						if ($_SESSION['assessment']['assessment_video_poster_url']) { 
							$poster_image_url = $_SESSION['application']['root_media_host_url'].$_SESSION['assessment']['assessment_video_poster_url'];
						} else {
							$poster_image_url = 'https://placehold.it/640x360?text=Image+640+x+360'; 
						}
						?>
						
						<!-- style="width:80%;height:auto;" -->
						<div id="">
							<center>
								<video  style="width:90%;height:auto;" poster="<?php echo $poster_image_url; ?>" controls>
									<source src="<?php echo $_SESSION['application']['root_media_host_url'].$_SESSION['assessment']['assessment_video_url_mp4']; ?>" type="video/mp4">
									<source src="<?php echo $_SESSION['application']['root_media_host_url'].$_SESSION['assessment']['assessment_video_url_webm']; ?>" type="video/webm">
									Your browser does not support HTML5 video.
								</video>
							</center>
						</div>
						
						<?php 
						if ($_SESSION['assessment']['assessment_video_caption']) {
							?>
							<div class="caption"><?php echo $_SESSION['assessment']['assessment_video_caption']; ?></div>
						<?php
						}
						?>
						<br />
				
					<?php	
					}
					?>
													
					<div class="show-for-medium-up">
						<a href="../assessment/" class="button large radius success" id="show-first-question123">Start&nbsp;<?php echo $_SESSION['assessment']['type']; ?></a>
					</div>
					
					<div class="show-for-small-only">
						<a href="../assessment/" class="button large radius success expand" id="">Start&nbsp;<?php echo $_SESSION['assessment']['type']; ?></a>
					</div>
						
				</div>
				
			</div>
			
		</div>
		
		<?php
		
		// $_SESSION['assessment']['show_assessment_header'] = ''; // hide
		// $_SESSION['assessment']['show_assessment_item_form'] = 'hide';

		// show_array($_SESSION['assessment']);
		// die;
		
	}	
	
}

if (!isset($_GET['id']) && ($_SESSION['assessment']['items_completed'] < $_SESSION['assessment']['total_items'])) { 
		
	// Get the next Assessment Item and its Item Options
	
	$content_asset_type_code = '("ASSESSITEM")';
	
	$queryAssessmentItem = 'SELECT 
		ai.content_asset_id AS assessment_item_id, 
		ai.assessment_item_type_code, 
		ai.correct_feedback_text, 
		ai.incorrect_feedback_text, 
		ai.use_image_response, 
		ai.randomize_item_options, 
		ca.title, 
		ca.summary_description, 
		ca.description,
		ait.name AS assessment_item_type_name,
		im.host_url AS assessment_item_image_url, 
		im.caption AS assessment_item_image_caption, 
		im.alt_text AS assessment_item_image_alt_text 
		FROM content_asset_usages cau  
		LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id 
		LEFT JOIN assessment_items ai ON ai.content_asset_id = ca.id 
		LEFT JOIN assessment_item_types ait ON ai.assessment_item_type_code = ait.code 
		LEFT JOIN images im ON  
		(ai.image_id = im.content_asset_id AND 
		 im.SIZE = "Assessment Item" AND 
		 im.usage_size = "Large" AND 
		 im.format = "JPG") 
		WHERE cau.content_asset_parent_id = '.$_SESSION['assessment']['assessment_id'].' 
		AND ca.content_asset_type_code IN '.$content_asset_type_code.' 
		AND ca.id NOT IN ('.$_SESSION['assessment']['item_id_processed_list'].') 
		AND ai.active = 1 
		ORDER BY '.$_SESSION['assessment']['items_order_by'].'  
		LIMIT 1'; 
	
	// echo $queryAssessmentItem . '<br /><hr />';
	// die;
	
	$result_assessment_item = mysqli_query($connection, $queryAssessmentItem);
	
	if (!$result_assessment_item) {
		show_mysqli_error_message($queryAssessmentItem, $connection);
		die;
	}
	
	// $total_assessment_items = mysqli_num_rows($result_assessment_item);
	
	// $assessment_item_count = 0;
	
	$ai = mysqli_fetch_assoc($result_assessment_item); 
		
	// show_array($ai);
	
	$_SESSION['assessment']['item_id_processed_list'] .= ', "'.$ai['assessment_item_id'].'"'; 
	
	$assessment_item_id = $ai['assessment_item_id'];
	// $assessment_item_type = $ai['assessment_item_id'];
	$assessment_item_type_code = $ai['assessment_item_type_code'];
	$use_image_response = $ai['use_image_response'];
	
	// Create $_SESSION['assessment'][assessment_item]
	// correct_feedback
	// incorrect_feedback
	
	$_SESSION['assessment']['assessment_item']['assessment_item_id'] = $ai['assessment_item_id'];
	$_SESSION['assessment']['assessment_item']['assessment_item_type_code'] = $ai['assessment_item_type_code'];
	$_SESSION['assessment']['assessment_item']['correct_feedback'] = $ai['correct_feedback_text'];
	$_SESSION['assessment']['assessment_item']['incorrect_feedback'] = $ai['incorrect_feedback_text'];
	$_SESSION['assessment']['assessment_item']['use_image_response'] = $ai['use_image_response'];
	$_SESSION['assessment']['assessment_item']['randomize_item_options'] = $ai['randomize_item_options'];
	
	if ($_SESSION['assessment']['assessment_item']['randomize_item_options']) {
		$_SESSION['assessment']['assessment_item']['item_options_order_by'] = 'RAND()';
	} else { 
		$_SESSION['assessment']['assessment_item']['item_options_order_by'] = 'aio.seq ASC'; 
	}

	// $assessment_item_count++;
	
	// Query the Assessment Item Options		
	$queryAssessmentItemOptions = 'SELECT 
		aio.id, 
		aio.seq, 
		aio.title, 
		aio.description, 
		aio.image_id,
		aio.video_id,
		aio.is_correct, 
		aio.feedback_text AS option_feedback_text, 
		aio.created_timestamp, 
		aio.modified_timestamp, 
		im.host_url AS assessment_item_option_image_url, 
		im.caption AS assessment_item_image_option_caption, 
		im.alt_text AS assessment_item_option_image_alt_text 
		FROM assessment_item_options aio 
		LEFT JOIN images im ON  
		(aio.image_id = im.content_asset_id AND 
		 im.SIZE = "Assessment Item Option" AND 
		 im.usage_size = "Small" AND 
		 im.format = "JPG") 
		WHERE aio.assessment_item_id = "'.$assessment_item_id.'" 
		ORDER BY '.$_SESSION['assessment']['assessment_item']['item_options_order_by'].'';  
				
	// echo $queryAssessmentItemOptions;
	// die();
	
	// Execute the query
	$result_assessment_item_option = mysqli_query($connection, $queryAssessmentItemOptions);
	
	if (!$result_assessment_item_option) {
		show_mysqli_error_message($queryAssessmentItemOptions, $connection);
		die;
	}
	
	$total_assessment_item_options = mysqli_num_rows($result_assessment_item_option);
				
	// Display Assessment Item
	?>
		
	<!-- <div class="row <?php echo $_SESSION['assessment']['show_assessment_item_form']; ?>" id="assessment-item-form"> -->
	<div id="assessment-item-form" class="<?php echo $display_assessment_item_form; ?>">
			
		<div class="panel">		
			<h4><?php echo $_SESSION['assessment']['type'].': '.$_SESSION['assessment']['title']; ?><small>&nbsp;v<?php echo $_SESSION['assessment']['version']; ?></small></h4>
		</div>
		
		<div class="row">
		
			<div class="small-12 columns">
				
				<?php // if ($_SESSION['assessment']['current_item'] == 1) { echo '<br />'; } ?>
				
				<?php
					
				if ($ai['assessment_item_image_url']) { 
					
					?>
					<div id="assessmentHeaderImage">	
						<br />
						<img src="<?php echo $_SESSION['application']['root_media_host_url'].$ai['assessment_item_image_url']; ?>" alt="<?php echo $a['assessment_item_image_alt_text']; ?>">
					</div>
					<?php
							
				}
					
				?>
				
				<form id="assessmentItemForm" action="../php/assessment_item_response.php" method="post" />
		
					<input type="hidden" id="assessment-item-action" name="action" value="grade-item"/>
					<input type="hidden" name="assessment_session_id" value="<?php echo ($_SESSION['assessment']['assessment_session_id'] ? $_SESSION['assessment']['assessment_session_id'] : ''); ?>"/>				
					<input type="hidden" name="assessment_id" value="<?php echo ($_SESSION['assessment']['assessment_id'] ? $_SESSION['assessment']['assessment_id'] : ''); ?>"/>
					<input type="hidden" name="assessment_item_id" value="<?php echo ($assessment_item_id ? $assessment_item_id : ''); ?>"/>
					<input type="hidden" id="assessmentItemTypeCode" name="assessment_item_type_code" value="<?php echo ($assessment_item_type_code ? $assessment_item_type_code : ''); ?>"/>
			

					<fieldset>
								  
			    		<legend>Question <?php echo $_SESSION['assessment']['current_item'].' of '.$_SESSION['assessment']['total_items']; ?> - <?php echo $ai['assessment_item_type_name']; ?></legend>
			    		
			    		<div id="assessmentItemResponseFeedback">
						</div>

						<div id="assessmentItemValidationFeedback">
						</div>
			    		
			    		<div class="row">		
			    			<div class="small-12 columns">
			    				<h3><?php echo $ai['title']; ?></h3>
			    				<?php echo $ai['summary_description']; ?>					
			    			</div>
			    		</div>
			    		
			    		<?php
				    		
				    	// Display the appropriate directions based upon the Item Type.
				    	
				    	switch ($ai['assessment_item_type_code']) {		
					    	
						    	case 'MCSA':
						    		$item_type_directions = 'Choose one:';
						    		$item_type_validation_feedback = 'One option is required. Please choose!';
						    		break;
						    		
						    	case 'MCMA':					    	
						    		$item_type_directions = 'Choose all that apply:';
						    		$item_type_validation_feedback = 'One or more options are required. Please choose!';
						    		break;
						    		
						    	case 'TF':					    	
						    		$item_type_directions = 'Choose one:';
						    		$item_type_validation_feedback = 'One option is required. Please choose!';
						    		break;
						    		
						    	case 'FIB':					    	
						    		$item_type_directions = 'Type your response:';
						    		$item_type_validation_feedback = 'A text response is required!';
						    		break;	
			    					    		
			    		}
			    		
			    		?>
			    		
			    		<div class="row">
							<div class="small-12 columns">
								<h5><?php echo $item_type_directions; ?></h5>
								<!-- <small class="error"><?php echo $item_type_validation_feedback; ?></small> -->
							</div>
			    		</div>
			    		
			    		<!-- Begin Assessment Item Option display -->
			    		<div class="row">		
							<div class="small-12 columns">
	
					    		<?php
						    	// Determine if the Item Options are displayed with an Image
						    	// If so, prepare a Foundation Block Grid container to display them
						    	
						    	if ($use_image_response) { 
							    	// Need the Foundation Block Grid wrapper
							    	?>
					    			<div class="assessmentResponseImage">
										<ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-5">
									<?php
								}
								
								$display_option_wrapper_begin = '';   	
								$display_image_option = '';	
								$display_option_wrapper_end = '';
	
						    	while($aio = mysqli_fetch_assoc($result_assessment_item_option)) { 
							    	
							    	// $_SESSION['assessment']['assessment_item']['option_feedback_text'] = $aio['option_feedback_text'];
							    	// This is too early to set this feedback. The item response has not been chosen yet so you do not know which feedback you are getting.

							    	// show_array($aio);
							    				    	
							    	if ($use_image_response) { 
								    	
								    	$display_option_wrapper_begin = '
								    	<li>
											<center>	
								    	';
								    	
								    	$display_image_option = '
								    	<img width="150" src="'.$_SESSION['application']['root_media_host_url'].$aio['assessment_item_option_image_url'].'" alt="'. $a['assessment_item_image_alt_text'].'">
										<br>';
										
										$display_option_wrapper_end = '
											</center>
										</li>';
										
									}
									
									echo $display_option_wrapper_begin;
									
									echo $display_image_option;
																		
							    	switch ($ai['assessment_item_type_code']) {		
							    	
								    	case 'MCSA':   	
								    		// echo 'Multiple Choice / Single Answer<br />';
								    		?>
								    		<div class="assessmentItemResponse">
												&nbsp;&nbsp;&nbsp;<input type="radio" class="assessmentResponse" name="mcsa_response" value="<?php echo $aio['id']; ?>" id="<?php echo $aio['id']; ?>">&nbsp;&nbsp;<?php echo $aio['title']; ?><br />
											</div>
											<?php
								    		break;
								    				        
								    	case 'MCMA':
						    	
							    		// echo 'Multiple Choice / Multiple Answer<br />';
							    		?>
							    		
							    		<input type="checkbox" class="assessmentResponse" name="checkbox_response[]" value="<?php echo $aio['id']; ?>" id="<?php echo $aio['id']; ?>">&nbsp;&nbsp;<?php echo $aio['title']; ?><br />
						    		  
										<?php
						    		
										break;
						    		
										case 'TF':
											// echo 'True / False<br />';
						    				?>
						    				&nbsp;&nbsp;&nbsp;<input type="radio" class="assessmentResponse" required name="tf_response" value="<?php echo $aio['id']; ?>" id="<?php echo $aio['id']; ?>">&nbsp;&nbsp;<?php echo $aio['title']; ?><br />
											<?php
										break;
						    		
								    	case 'FIB':
								    		echo 'Fill in the Blank<br />';
								    		break;
						    				
									} // END switch ($ai['assessment_item_type_code']) {	
										
									echo $display_option_wrapper_end;
					    	
								} // END while($aio = mysqli_fetch_assoc($result_assessment_item_option)) { 
									
								if ($use_image_response) { 
									// Need the Foundation Block Grid wrapper close
							    	?>
										</ul>
					    			</div>
									<?php
								}
	
								mysqli_free_result($result_assessment_item_option);
				    	
				    			?>
				    	
							</div>
			    		</div>
			    					    		
			    		<div class="row">			    	
					    	<div class="small-12 columns" id="assessmentItemSubmitButton">
						    	<hr>
				    	    	<div class="show-for-medium-up">
				    	    		<a href="javascript:void(0)" class="assessmentItemSubmit button large radius success">
				    	    			Submit
				    	    		</a>
				    	  		</div>
				    	  		<div class="show-for-small-only">
				    	    	   	<a href="javascript:void(0)" class="assessmentItemSubmit button large radius success expand">
				    	    			Submit
				    	    		</a>				    	    		
				    	  		</div>
			    			</div>
			    		</div>
		    			
					</fieldset>
							
				</form>
			</div>
		</div>
	</div>
	
	<?php
			
	mysqli_free_result($result_assessment_item);
	
} else {
	
	// User has completed all of the Assessment Items
	// Show Assessment Results option is provided after displaying the feedback for the last Assessment Item the user completed
	// User should be taken to Assessment Results page at this point
} // END if ($_SESSION['assessment']['items_completed'] < $_SESSION['assessment']['total_items']) { 
	
// show_array($_SESSION['assessment']);

?>