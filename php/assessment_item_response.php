<?php

include_once('../includes/header.php');
include('../includes/assessment_functions.php');	

// Process $_POST parameters.
// Move this to a separate script that can be called with AJAX for each Assessment Item submit function.
// The action can be "score", "nextItem", "viewResults".
if (isset($_POST['action']) && $_POST['action'] == 'grade-item') {

	// show_array($_POST);
	// show_array($_SESSION['assessment']);
	// die();
	
	$_SESSION['assessment']['show_assessment_header'] = 'hide';
				
	// set POST return values to local variables for processing 
	$assessment_session_id = safe_sql_data($connection, $_POST['assessment_session_id']);
	$assessment_id = safe_sql_data($connection, $_POST['assessment_id']);
	$assessment_item_id = safe_sql_data($connection, $_POST['assessment_item_id']);
	$assessment_item_type_code = safe_sql_data($connection, $_POST['assessment_item_type_code']);
	// $assessment_item_option_id = safe_sql_data($connection, $_POST['assessment_item_option_id']);
	// $text_response = safe_sql_data($connection, $_POST['text_response']);
	
	$_SESSION['assessment']['items_completed']++;
	$_SESSION['assessment']['current_item']++;
	$_SESSION['assessment']['items_to_complete'] = $_SESSION['assessment']['total_items'] - $_SESSION['assessment']['items_completed'];
		
	// Determine the Item Type Response
	
	switch ($assessment_item_type_code) {		
	
    	case 'MCSA':
    	   	// echo $assessment_item_type_code.' processing TBD.<br />';
    		$assessment_item_option_id = safe_sql_data($connection, $_POST['mcsa_response']);
    		$text_response = '';    		
    		createAssessmentSessionResponse($connection, $assessment_session_id, $assessment_id, $assessment_item_id, $assessment_item_option_id, $text_response);
    		gradeAssessmentResponse($connection, $assessment_session_id, $assessment_id, $assessment_item_id, $assessment_item_type_code, $assessment_item_option_id, $text_response);	
     		break;
    		
    	case 'MCMA':
    	   	// echo $assessment_item_type_code.' processing TBD.<br />';
    		// Determine the total number of checkboxes selected
    		$max = count($_POST['checkbox_response']);
			for($i = 0; $i < $max;$i++)
			{
				$assessment_item_option_id = safe_sql_data($connection, $_POST['checkbox_response'][$i]);
				
				if ($assessment_item_option_id) {
					$text_response = '';
					createAssessmentSessionResponse($connection, $assessment_session_id, $assessment_id, $assessment_item_id, $assessment_item_option_id, $text_response);
				}
				
			}
			gradeAssessmentResponse($connection, $assessment_session_id, $assessment_id, $assessment_item_id, $assessment_item_type_code, $assessment_item_option_id, $text_response);	

    		break;
    		
    	case 'TF':
    		// echo $assessment_item_type_code.' processing TBD.<br />';
    		$assessment_item_option_id = safe_sql_data($connection, $_POST['tf_response']);
    		createAssessmentSessionResponse($connection, $assessment_session_id, $assessment_id, $assessment_item_id, $assessment_item_option_id, $text_response);
    		gradeAssessmentResponse($connection, $assessment_session_id, $assessment_id, $assessment_item_id, $assessment_item_type_code, $assessment_item_option_id, $text_response);	    					
    		break;
    		
    	case 'FITB':
    		// echo $assessment_item_type_code.' processing TBD.<br />';
    		$assessment_item_option_id = safe_sql_data($connection, $_POST['fitb_response']);
    		if (isset($_POST['text_response'])) {
				$text_response = safe_sql_data($connection, $_POST['text_response']);
			} else {
				$text_response = '';
			}
    		createAssessmentSessionResponse($connection, $assessment_session_id, $assessment_id, $assessment_item_id, $assessment_item_option_id, $text_response);
    		gradeAssessmentResponse($connection, $assessment_session_id, $assessment_id, $assessment_item_id, $assessment_item_option_id, $text_response);	
    		break;
    		
    } 	
	
}

?>
