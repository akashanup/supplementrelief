<?php 

// echo 'Begin assessment_results.php.<br />';
// SupplementRelief
// Renders an Assessment Result
// Show the User's result and how they compared to others
// Optionally show Content Remediation Outline if activated
// Clear the Assessment $_SESSION
// Suggest what to do next

// https://supplementrelief.com/assessment-results/

// if not session assessment bail out

if (isset($_SESSION['assessment'])) {
		
	include_once('../includes/header.php');
	// include('../includes/assessment_functions.php');
	
	// error_reporting(ALL);
	
	// show_array($_GET);
	// show_array($_SESSION);
	
	$score_percentage = (($_SESSION['assessment']['user_correct_responses'] / $_SESSION['assessment']['total_items']) * 100);
	$_SESSION['assessment']['score_percentage'] = $score_percentage;
	
	$complete_timestamp = date('Y/m/d H:i:s');
	
	// Update the Assessment Session record in the database	with the User response metrics
	$queryUpdateAssessmentSession = '
	UPDATE assessment_sessions SET 
	complete = 1, 
	complete_timestamp = "'.$complete_timestamp.'", 
	total_correct_responses = '.no_value_null_check($_SESSION['assessment']['user_correct_responses']).',
	total_incorrect_responses = '.no_value_null_check($_SESSION['assessment']['user_incorrect_responses']).',  
	score_percentage = '.no_value_null_check($score_percentage).' 
	WHERE user_session_id = "'.$_SESSION['assessment']['user_session_id'].'" 
	AND assessment_id = "'.$_SESSION['assessment']['assessment_id'].'" 
	AND complete IS NULL';	
		
	// echo $queryUpdateAssessmentSession;	
	// die();
		
	$result_update = mysqli_query($connection, $queryUpdateAssessmentSession);
	
	if (!$result_update) {
		show_mysqli_error_message($queryUpdateAssessmentSession, $connection);
		die;
	}
		
	// Determine where the User's score placed within the Assessment Rubrics
	// Check the $_SESSION['assessment']['rubric_type'] : Outcome or Scored
	$queryAssessmentRubric = 'SELECT 
	ar.title AS assessment_rubric_title, 
	ar.description AS assessment_rubric_description, 
	im.host_url AS assessment_rubric_image_url, 
	im.caption AS assessment_rubric_image_caption, 
	im.alt_text AS assessment_rubric_image_alt_text 
	FROM assessment_rubrics ar 
	LEFT JOIN rubrics ru ON ar.rubric_id = ru.id 
	LEFT JOIN images im ON  
		(ar.image_id = im.content_asset_id AND 
		 im.SIZE = "Assessment Rubric" AND 
		 im.usage_size = "Large" AND 
		 im.format = "JPG")
	WHERE ar.assessment_id = '.$_SESSION['assessment']['assessment_id'].' 
	AND ru.range_low <= '.$score_percentage.' 
	AND ru.range_high >= '.$score_percentage.' 
	LIMIT 1'; 
	
	// echo $queryAssessmentRubric . '<br /><hr />';
	// die;
	
	$result_assessment_rubric = mysqli_query($connection, $queryAssessmentRubric);
	
	if (!$result_assessment_rubric) {
		show_mysqli_error_message($queryAssessmentRubric, $connection);
		die;
	}
	
	$ar = mysqli_fetch_assoc($result_assessment_rubric); 
		
	// show_array($ar);
	
	?>
	
	<div class="panel">	
			
		<h4><?php echo $_SESSION['assessment']['type'].': '.$_SESSION['assessment']['title']; ?><small>&nbsp;v<?php echo $_SESSION['assessment']['version']; ?></small></h4>
	
	</div>
	
	<div class="row">		
		<div class="small-12 columns">
			
			<ul class="tabs" data-tab role="tablist">
				
			  	<li class="tab-title active" role="presentation"><a href="#results" role="tab" tabindex="0" aria-selected="true" aria-controls="results">Results</a></li>
			  	
			  	<li class="tab-title" role="presentation"><a href="#products" role="tab" tabindex="0" aria-selected="false" aria-controls="products">Products</a></li>
			  	
			  	<li class="tab-title" role="presentation"><a href="#content" role="tab" tabindex="0" aria-selected="false" aria-controls="content">Content</a></li>
			  	
			  	<?php if ($_SESSION['assessment']['discussion_forum']['forum'] == true) { ?>

			  		<li class="tab-title" role="presentation"><a href="#discuss" role="tab" tabindex="0" aria-selected="false" aria-controls="discuss">Discuss</a></li>
			  		
			  	<?php } ?>
			  	
			</ul>
			
			<div class="tabs-content">
				
				<section role="tabpanel" aria-hidden="false" class="content active" id="results">
			    	<div class="row">		
						<div class="small-12 columns">
									
							<?php
								
							if ($ar['assessment_rubric_image_url']) { 
								
								$display_assessment_rubric_image_url = $_SESSION['application']['root_media_host_url'].$ar['assessment_rubric_image_url']; 
								
							} else {
								
								// $display_assessment_rubric_image_url = 'https://placehold.it/640x360?text=Image+640+x+360';
								
							} 
								
							?>
							<div id="assessmentRubricResultImage">	
							
								<img src="<?php echo $display_assessment_rubric_image_url; ?>" alt="<?php echo $a['assessment_rubric_image_alt_text']; ?>">
								<div class="caption"><?php echo $_SESSION['assessment']['user_correct_responses'].'/'.$_SESSION['assessment']['total_items'].' - Scored '.round($score_percentage).'%';?></div>
								
							</div>
							
							<!-- <br /> -->
										
							<h2>Result: <?php echo $ar['assessment_rubric_title']; ?></h2>
							
							<p><?php echo $ar['assessment_rubric_description'];?></p>
																
						</div>
					</div>
		
			  	</section>
			  
				<section role="tabpanel" aria-hidden="true" class="content" id="products">
				    <h2>Product related content goes here...</h2>
				</section>
				
				<section role="tabpanel" aria-hidden="true" class="content" id="content">
				    <h2>Content related content goes here...</h2>
				</section>
				
				<?php
				if ($_SESSION['assessment']['discussion_forum']['forum'] == true) { 
					?> 

					<section role="tabpanel" aria-hidden="true" class="content" id="discuss">
						
						<?php 
							$_SESSION['forum_type'] = 'assessment';
							include('../php/discussion_forum.php'); 
						?>
						
					</section>
					<?php
				}
				?>

			</div>		
								
		</div>
	</div>
			
	<?php
		
	$queryAssessmentTotalCompletions = 'SELECT 
	ase.id 
	FROM assessment_sessions ase  
	WHERE ase.assessment_id = "'.$_SESSION['assessment']['assessment_id'].'" 
	AND ase.complete = 1'; 
			
	// echo $queryAssessmentTotalCompletions;
	// die();
	
	// Execute the query
	$result_total_assessment_completion = mysqli_query($connection, $queryAssessmentTotalCompletions);
	
	if (!$result_total_assessment_completion) {
		show_mysqli_error_message($queryAssessmentTotalCompletions, $connection);
		die;
	}

	$total_assessments = mysqli_num_rows($result_total_assessment_completion);
		
	mysqli_free_result($result_assessment_total_completion);
		
	$queryAssessmentRubrics = 'SELECT 
	ar.id, 
	ar.seq, 
	ar.title AS assessment_rubric_title, 
	ar.description AS assessment_rubric_description, 
	ru.title AS rubric_title, 
	ru.description AS rubric_description, 
	ar.image_id,
	ar.video_id, 
	ar.created_timestamp, 
	ar.modified_timestamp, 
	ru.range_low, 
	ru.range_high  
	FROM assessment_rubrics ar 
	LEFT JOIN rubrics ru ON ar.rubric_id = ru.id 
	WHERE ar.assessment_id = "'.$_SESSION['assessment']['assessment_id'].'" 
	AND ar.active = 1 
	ORDER BY ar.seq';
			
	// echo $queryAssessmentRubrics;
	// die();
	
	// Execute the query
	$result_assessment_rubric_results = mysqli_query($connection, $queryAssessmentRubrics);
	
	if (!$result_assessment_rubric_results) {
		show_mysqli_error_message($queryAssessmentRubrics, $connection);
		die;
	}
		
	?>
	
	<hr>
	
	<div class="row">			
		<div class="small-12 columns">
			
			<center>					
				<table width="70%">
					
					<caption><?php echo $_SESSION['assessment']['type']; ?> completed <?php echo $total_assessments; ?> times.</caption>
				
					<thead>
						<tr>
						  	<!-- <th><center><i class="fa fa-pencil"></i></center></th>
						  	<th>ID</th>
						  	<th>Sequence</th> -->
						  	<th>Score</th>
						  	<!-- <th>Description</th> -->
						  	<th># Completions</th> 
						  	<th>% Completions</th>  
						  	<!-- <th class="show-for-medium-up">Range Low</th> 		   
						  	<th class="show-for-medium-up">Range High</th>
						  	<th class="show-for-large-up">Created<br />Timestamp</th>
						  	<th class="show-for-large-up">Modified<br />Timestamp</th> -->		
					  	</tr>	
					</thead>
			
					<?php
													
					while($ar = mysqli_fetch_assoc($result_assessment_rubric_results)) {
						
						// show_array($ar);
						
						$rubric_id = $ar['id'];
						$range_low = $ar['range_low'];
						$range_high = $ar['range_high'];
										
						// Determine how many people todate have scored within this Rubric range for this Assessment
						$queryAssessmentRubricTotal = 'SELECT 
						count(*) AS responses  
						FROM assessment_sessions ase 
						WHERE ase.assessment_id = "'.$_SESSION['assessment']['assessment_id'].'" 
						AND ase.complete = 1 
						AND ase.score_percentage >= '.$range_low.'   
						AND ase.score_percentage <= '.$range_high.''; 
				
						// echo $queryAssessmentRubricTotal;
						// die();
						
						// Execute the query
						$result_assessment_rubric_total = mysqli_query($connection, $queryAssessmentRubricTotal);
						
						if (!$result_assessment_rubric_total) {
							show_mysqli_error_message($queryAssessmentRubricTotal, $connection);
							die;
						}
						
						$re = mysqli_fetch_assoc($result_assessment_rubric_total); 
						
						$rubric_score_percentage = (($re['responses'] / $total_assessments) * 100);
	
						?>
						
						<tr>
						
							<!-- <td><?php echo '<center><a href="../edit_assessment_rubric/?a_id='.$assessment_id.'&ar_id='.$ar['id'].'" title="Edit Assessment Rubric" >Edit</a></center>'; ?></td>				
									
							<td><?php echo $ar['id']; ?></td>
							
							<td><?php echo $ar['seq']; ?></td> -->
																							
							<td><?php echo $ar['rubric_title']; ?></td>
							
							<!-- <td><?php echo $ar['rubric_description']; ?></td> -->
							
							<td><?php echo $re['responses']; ?></td>
							
							<td><?php echo round($rubric_score_percentage); ?>%</td>
									
							<!-- <td class="show-for-medium-up"><?php echo $ar['range_low']; ?></td>
																					
							<td class="show-for-medium-up"><?php echo $ar['range_high']; ?></td>
									
							<td><?php echo date('m/d/y h:i A T', strtotime($ar['created_timestamp'])); ?></td>
							
							<td><?php echo no_null_timestamp_check($ar['modified_timestamp']); ?></td> -->
																																
						</tr>
		
					<?php
						
					mysqli_free_result($result_assessment_rubric_total);
		
					}
					
					mysqli_free_result($result_assessment_rubric_results);
					?>
						
				</table>
			</center>
		</div>
	</div>
	
	<?php
	
	if ($_SESSION['assessment']['enable_retake'] OR $_SESSION['assessment']['enable_remediation']) {
		
		?>
		<div class="row">		
			<div class="small-12 columns">
				
				<?php
				if ($_SESSION['assessment']['enable_retake']) { 
					?>
					<a href="../assessment/?id=<?php echo $_SESSION['assessment']['assessment_id']; ?>" class="button large radius success"><i class="fa fa-repeat" aria-hidden="true"></i>&nbsp;&nbsp;Retake&nbsp;<?php echo $_SESSION['assessment']['type']; ?></a>
					<?php
				}

				if ($_SESSION['assessment']['enable_remediation']) {
					
					?>
					<a href="../assessment_remediation/?id=<?php echo $_SESSION['assessment']['assessment_session_id']; ?>" class="button large radius success" title="View educational content related to the items you missed that will help you understand the correct answer.">View Content Remediation Outline</a>					
					<?php
				}
				?>
			</div>
		</div>
		<?php
			
	}
		
	mysqli_free_result($result_assessment_rubric);
		
	unset($_SESSION['assessment']);
	
}

?>