<?php
	include_once('../includes/header.php');
	$postId 		= 	safe_sql_data($connection, $_POST['postId']);
	$type 			= 	safe_sql_data($connection, $_POST['type']);
	$tableName 		= 	safe_sql_data($connection, $_POST['tableName']);

	$json 			=	[];
	$json['upload'] =	0;
	if($postId)
	{
		$file_status=	2;
		$unique_id  = 	$postId . substr($tableName, strpos($tableName,'_'));
		if($type == 'document')
		{
			$fileArray = 	$_FILES['file'];
			$columnUrl =	'file_document_url';
			$columnType=	'file_document_type';
			list($file_name, $file_url, $file_extension) = uploadMedia('', $type, $fileArray, '', '', '', $unique_id);
	
		}
		else if($type == 'image')
		{
			$fileArray = 	$_FILES['file'];
			$columnUrl =	'file_image_url';
			$columnType=	'file_image_type';
			list($file_name, $file_url, $file_extension) = uploadMedia('', $type, $fileArray, '', '', '', $unique_id);
			
		}
		else if($type == 'video')
		{
			$fileArray = 	$_FILES['file'];
			$columnUrl =	'file_video_url';
			$columnType=	'file_video_type';
			list($file_name, $file_url, $file_extension) = uploadMedia('', $type, $fileArray, '', '', '', $unique_id);
			$file_status=	0;
		}
		else if($type == 'audio')
		{
			$fileArray = 	$_FILES['file'];
			$columnUrl =	'file_audio_url';
			$columnType=	'file_audio_type';
			list($file_name, $file_url, $file_extension) = uploadMedia('', $type, $fileArray, '', '', '', $unique_id);
		}
		

		$query = 'UPDATE '.$tableName.' SET file_status = ' . $file_status . ', ' . $columnUrl . ' = "' . $file_url . '", ' . $columnType . ' = "' . $file_extension .'" WHERE id = '.$postId .' AND file_type = "' . $type . '" ';
		if (mysqli_query($connection, $query) == TRUE) 
		{
		    //Video ready to get compressed by cronjob
		    $json['upload']		=	1;
		    $json['file_url']	=	$file_url;
		}
	}
	$json 	=	json_encode($json);
	echo $json;
?>