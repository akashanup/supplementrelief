<?php

// echo 'Begin blog_post_discussion_forum.php<br /><hr />';

/* https://supplementrelief.com/show_blog_post/index.php?bp_id=1229#comment163 */

if (isset($_GET['bp_id'])) { $blog_post_id = $_GET['bp_id']; } 

/* 
// Variables set in show_project_program_recipe.php
	$_SESSION['blog_post']['discussion_forum']['forum'] = $r['discussion_forum'];	
	$_SESSION['blog_post']['discussion_forum']['thread_post'] = $r['discussion_thread_post'];
	$_SESSION['blog_post']['discussion_forum']['thread_post_document'] = $r['discussion_thread_post_document'];	
	$_SESSION['blog_post']['discussion_forum']['thread_post_image'] = $r['discussion_thread_post_image'];
	$_SESSION['blog_post']['discussion_forum']['thread_post_video'] = $r['discussion_thread_post_video'];
	$_SESSION['blog_post']['discussion_forum']['reply_post'] = $r['discussion_reply_post'];
	$_SESSION['blog_post']['discussion_forum']['reply_post_document'] = $r['discussion_reply_post_document'];	
	$_SESSION['blog_post']['discussion_forum']['reply_post_image'] = $r['discussion_reply_post_image'];
	$_SESSION['blog_post']['discussion_forum']['reply_post_video'] = $r['discussion_reply_post_video'];		
*/

// show_array($_SESSION);
// show_array($_SERVER);
 
// Determine if the Product is allowed to use a Discussion Forum
if ($_SESSION['blog_post']['discussion_forum']['forum'] == true) {  
 
    // Determine if the Blog Post allows the User to post a new Comment (different from a Comment Reply)
    if ($_SESSION['blog_post']['discussion_forum']['thread_post'] == true) {
     
        // If true display the form for posting a new Comment   
     
        include('../includes/discussion_forum_header.php'); 
        ?>
         
        <div id="discuss">
            <?php show_session_message(); ?>
        </div>
         
        <div id="discussionThreadForm">
         
            <fieldset>
            <legend>Post Comment</legend>
             
            <form data-abide autocomplete="off" action="../php/discussion_forum_crud.php" method="post" enctype="multipart/form-data">
                 
                <input type="hidden" name="action" value="new_thread"/>
                
                <input type="hidden" name="forum_type" value="blog post"/>
                
                <input type="hidden" name="use_google_recaptcha" value="1"/>
             
                <input type="hidden" name="comment_user_id" value="<?php echo $_SESSION['user']['user_id']; ?>"/>
                
                <input type="text" name="comment_note" value=""/> 
                 
                <!--                             
                <div class="row">
                    <div class="small-11 right">
                        <input type="text" required name="comment_title" style="width: 90%" placeholder="Title..." value="<?php echo $_SESSION['blog_post']['discussion_forum']['comment_title']; ?>" />
                        <small class="error">Title is required</small>
                    </div>
                </div>    
                     
                <div class="row">
                    <div class="small-11 right">
                        <textarea required name="comment_text" style="width: 90%" placeholder="Comment..." rows="3" /><?php echo $_SESSION['blog_post']['discussion_forum']['comment_text']; ?></textarea>
                        <small class="error">Comment is required</small>
                    </div>
                </div>
                -->
                 
                <!--
                <div class="row">
                    <div class="small-12 columns">
                        <input type="text" required name="comment_title" style="width: 100%" placeholder="Title..." value="<?php echo $_SESSION['blog_post']['discussion_forum']['thread_title']; ?>" />
                        <small class="error">Title is required</small>
                    </div>
                </div>
                -->    
                     
                <div class="row">
                    <div class="small-12 columns">
                        <textarea required name="comment_text" style="width: 100%" placeholder="Comment..." rows="3" /><?php echo $_SESSION['temp']['thread_text']; ?></textarea>
                        <small class="error">Comment is required</small>
                    </div>
                </div>
                 
                <?php 
                             
                    // Determine if User is allowed to attached a Document to the post
                    // Server-side script currently validates file type and size.
                    // Validation should be done client-side using Javascript or JQuery.
                    if ($_SESSION['blog_post']['discussion_forum']['thread_post_document']) {   
                        echo '                              
                        <div class="row collapse">
                           <div class="small-2 columns">
                                <div class="right"><b>Document:</b>&nbsp;&nbsp;</div>           
                           </div>
                           <div class="small-10 columns">
                                <input type="file" name="document_file">                  
                           </div>                            
                        </div>';      
                    }                                   
 
                    // Determine if User is allowed to attached an Image to the post
                    // Server-side script currently validates file type and size.
                    // Validation should be done client-side using Javascript or JQuery.                                                
                    if ($_SESSION['blog_post']['discussion_forum']['thread_post_image']) {  
                        echo '                              
                        <div class="row collapse">
                           <div class="small-2 columns">
                                <div class="right"><b>Image:</b>&nbsp;&nbsp;</div>          
                           </div>
                           <div class="small-10 columns">
                                <input type="file" name="image_file">                 
                           </div>                    
                        </div>';      
                    }
                                         
                    // Determine if User is allowed to attached a Video to the post
                    // Server-side script currently validates file type and size.
                    // Validation should be done client-side using Javascript or JQuery.
                    if ($_SESSION['blog_post']['discussion_forum']['thread_post_video']) {  
                        echo '                              
                        <div class="row collapse">
                           <div class="small-2 columns">
                                <div class="right"><b>Video:</b>&nbsp;&nbsp;</div>          
                           </div>
                           <div class="small-10 columns">
                                <input type="file" name="video_file">                 
                           </div>                            
                        </div>';      
                    }                                                                                                                                       
                ?>
                
                <div class="row">		
		    		<div class="small-12 columns">
			    		<!-- Paste this snippet at the end of the <form> where you want the reCAPTCHA widget to appear. -->
	    		    	<div class="g-recaptcha" data-sitekey="<?php echo public_captcha_key; ?>"></div>
	    		    	<br />
	     			</div>
		    	</div>
            
                <div class="row">
                    <div class="small-12 columns">
                        <input type="submit" class="button large radius" value="Post">      
                    </div>
                </div>
                                                 
            </form>
            </fieldset>
     
        </div>
 
        <?php
	        
	    unset($_SESSION['temp']['thread_title']);
	    unset($_SESSION['temp']['thread_text']);
     
    } // if ($_SESSION['blog_post']['discussion_forum']['forum'] == true) {
     
    // Query for all Discussion Threads for this Project Program Course Topic Web Page.
    // Status "S" means show record.
     
    $queryThread = 'SELECT 
        dt.id, 
        dt.created_timestamp, 
        dt.title, 
        dt.text,
        dt.file_document_url, 
        dt.file_document_type,  
        dt.file_image_url, 
        dt.file_image_type, 
        dt.file_video_url, 
        dt.file_video_type,  
        dt.created_by, 
        dt.modified_timestamp, 
        pe.first_name, 
        pe.last_name 
        FROM discussion_threads dt 
        LEFT JOIN persons pe ON dt.created_by = pe.id 
		WHERE dt.blog_post_id = '.$blog_post_id.'   
        AND dt.status in ("S", "R")                     
        ORDER BY dt.modified_timestamp DESC';
                 
    // echo $queryThread . '<br /><hr />';
     
    $result_list_discussion_thread = mysqli_query($connection, $queryThread);
     
    if (!$result_list_discussion_thread) {
        show_mysqli_error_message($queryThread, $connection);
        die;
    }
         
    $comments_block = false;
     
    while($r1 = mysqli_fetch_assoc($result_list_discussion_thread)) {
        // For Each Comment:
        // 1. Display the Comment
        // 2. Display a Reply form if the configuration allows for new Replies to the Comment
        // 3. Display any Comment Replies (oldest first)
     
        // show_array($r1); 
             
        if (!$comments_block) {
            echo '<div class="row">
                    <div class="small 12-columns">        
                        <div class="blogPostCommentContainer">';
            $comments_block = true;
        }
     
        // See if the User logged in has Forum Moderator privileges. If so provide tools to remove unwanted Comments and Replies.
        if ($_SESSION['enrollment']['discussion_forum_moderator']) {
            $comment_moderator = '
            <span class="label">Moderator</span>&nbsp;<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Remove this Comment and all related Replies?">'.               
            '<a href="../php/delete_discussion_thread.php?type=thread&dt_id='.$r1['id'].'" onclick="return confirm('."'Are you sure you want to delete this Comment? Select OK to DELETE.');".'"><i class="fa fa-remove icon-red"></i></a>'.'</span><br />';
            // Delete Discussion Thread should be handled using AJAX to process request in the background.      
        } else {
            $comment_moderator = null;
        }
         
        if (strlen($r1['first_name']) > 1) {
            $display_first_name = $r1['first_name'];
        } else {
            $display_first_name = 'Guest';          
        }       
         
        $commentHeader = '
        <div class="commentHeader">   
            <span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Person who posted this Comment"><i class="fa fa-user icon-red"></i></span>&nbsp;'.$display_first_name.'&nbsp; 
            <span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Post Timestamp"><i class="fa fa-clock-o icon-red"></i></span> '.date('m/d/y h:i A T', strtotime($r1['created_timestamp'])).'&nbsp;
            <span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Report Comment as SPAM"><a href="../php/report_discussion_forum_spam.php?forum_type=blogpost&comment_type=comment&comment_id='.$r1['id'].'" onclick="return confirm('."'Are you sure you want to report this Comment as SPAM? Select OK to REPORT.');".'"><i class="fa fa-ban icon-red"></i></a></span>&nbsp;'
            .$comment_moderator.'                           
        </div>
        <div id="threadPostHeaderTitle">
            <h4>'.$r1['title'].'</h4>
        </div>';
        // Report SPAM should be handled using AJAX to process request in the background.       
         
        // Comment body content display uses different layouts depending upon if a media file is present and what type it is.
         
        if (strlen($r1['file_document_url']) > 0) {      
            // echo 'File Document URL present<br />';
                             
            echo
            // div id used for returning to this record in a URL using a #hashtag   
            '<div id="comment'.$r1['id'].'" class="row collapse">     
                <div class="small-12 medium-8 columns">               
                    <div class="blogPostComment">'
                     
                        .$commentHeader.'   
                                                                                 
                        <div class="commentBodyText"><p>'.$r1['text'].'</p></div>                       
                                                                                 
                    </div>                
                </div>
                                     
                <div class="small-12 medium-4 columns">                                   
                    <div class="commentBodyImage"><a href="'.$r1['file_document_url'].'" target="_blank" title="Preview document"><img src="https://cdn-manager.net/media/images/document.jpg" alt="" width="150"/></a></div> 
                </div>
                                         
            </div>';
         
        } elseif (strlen($r1['file_image_url']) > 0) {       
            // echo 'File Image URL present<br />';
                             
            echo
            // div id used for returning to this record in a URL using a #hashtag   
            '<div id="comment'.$r1['id'].'" class="row collapse">     
                <div class="small-12 medium-8 columns">               
                    <div class="blogPostComment">'
                     
                        .$commentHeader.'   
                                                                                 
                        <div class="commentBodyText"><p>'.$r1['text'].'</p></div>                       
                                                                                 
                    </div>                
                </div>
                                     
                <div class="small-12 medium-4 columns">                                   
                    <div class="commentBodyImage"><img src="'.$r1['file_image_url'].'" alt="" /></div>    
                </div>
                                         
            </div>';
             
        } elseif (strlen($r1['file_video_url']) > 0) {       
            // echo 'File Video URL present<br />';
         
            echo   
            '<div id="comment'.$r1['id'].'" class="row collapse">     
                <div class="small-12 medium-8 columns">               
                    <div class="blogPostComment">'
                                             
                        .$commentHeader.'   
                         
                        <div class="commentBodyText"><p>'.$r1['text'].'</p></div>
                                                                                                     
                    </div>                
                </div>
                                     
                <div class="small-12 medium-4 columns">                                   
                    <div class="commentBodyVideo">
                        <video preload="none" controls poster="https://cdn-manager.net/media/images/user-video-poster.jpeg" style="width:100%;">
                            <source src="'.$r1['file_video_url'].'" type="video/mp4">
                            Your browser does not support HTML5 Video.
                        </video>          
                    </div>    
                </div>
                                         
            </div>';
             
        } else {
         
            // Text only Comment body with no media files.
            echo   
            '<div id="comment'.$r1['id'].'" class="row">      
                <div class="small-12 columns">                
                    <div class="blogPostComment">'    
                                     
                        .$commentHeader.'
                                                                             
                        <div class="commentBodyText"><p>'.$r1['text'].'</p></div>
                                                                 
                    </div>
                </div>
            </div>';
             
        } // May also add support for other media types such as Audio.  
         
        // Determine if the User is allowed to Reply to Comments. If so, display the Comment Reply form
        if ($_SESSION['blog_post']['discussion_forum']['reply_post']) { 
         
            ?>
             
            <div id="discussionReplyForm">
             
                <fieldset>
                <legend>Post Reply</legend>
                 
                <form data-abide autocomplete="off" action="../php/discussion_forum_crud.php" method="post" enctype="multipart/form-data">
                 
                    <input type="hidden" name="action" value="new_reply"/>
                    
                    <input type="hidden" name="forum_type" value="blog post"/>
                                        
                    <input type="hidden" name="comment_reply_id" value="<?php echo $r1['id']; ?>"/>  
                         
                    <input type="hidden" name="comment_reply_user_id" value="<?php echo $_SESSION['user']['user_id']; ?>"/> 
                    
                    <input type="text" name="comment_note" value=""/>    
                         
                    <div class="row">
                        <div class="small-12 columns">
                        
                            <?php 
                                             
                            if ($_SESSION['blog_post']['discussion_forum']['reply_post_document']) {    
                                echo '                              
                                <div class="row collapse">
                                   <div class="small-2 columns">
                                        <div class="right"><b>Document:</b>&nbsp;&nbsp;</div>           
                                   </div>
                                   <div class="small-10 columns">
                                        <input type="file" name="document_file">                  
                                   </div>                            
                                </div>';      
                            }                                   
                                                         
                            if ($_SESSION['blog_post']['discussion_forum']['reply_post_image']) {   
                                echo '                              
                                <div class="row collapse">
                                   <div class="small-2 columns">
                                        <div class="right"><b>Image:</b>&nbsp;&nbsp;</div>          
                                   </div>
                                   <div class="small-10 columns">
                                        <input type="file" name="image_file">                 
                                   </div>                    
                                </div>';      
                            }
                             
                            if ($_SESSION['blog_post']['discussion_forum']['reply_post_video']) {   
                                echo '                              
                                <div class="row collapse">
                                   <div class="small-2 columns">
                                        <div class="right"><b>Video:</b>&nbsp;&nbsp;</div>          
                                   </div>
                                   <div class="small-10 columns">
                                        <input type="file" name="video_file">                 
                                   </div>                            
                                </div>';      
                            }
         
                            ?>       
                         
                        </div>
                    </div>                    
                            
                    <div class="row collapse">
                     
                      <div class="small-12 medium-10 columns">
                        <textarea required name="comment_reply_text" style="width: 100%" placeholder="Reply..."  /><?php echo $_SESSION['temp']['thread_reply_text']; ?></textarea>
                        <small class="error">Reply is required</small>
                      </div>
                       
                       <div class="small-12 medium-2 columns">
                          <!--<a href="../resources/actions/new_discussion_reply.php" class="button small radius" >Reply</a>-->
                          <input type="submit" value="Reply" class="button medium radius expand" >
                       </div>
                        
                   </div>
                                                     
                </form>           
                </fieldset>
             
            </div> <!-- <div id="discussionReplyForm"> -->
                     
            <?php
	        unset($_SESSION['temp']['thread_reply_text']);
             
        } // if ($_SESSION['topic_discussion_reply_post']) {        
                     
        // Query for all Discussion Replies related to the Discussion Thread.
        // Status "S" means show record.
         
        $queryDiscussionReplies = 'SELECT 
            dr.id, 
            dr.created_timestamp, 
            dr.text,
            dr.file_document_url, 
            dr.file_document_type, 
            dr.file_image_url, 
            dr.file_image_type, 
            dr.file_video_url, 
            dr.file_video_type,  
            dr.created_by, 
            pe.first_name, 
            pe.last_name 
            FROM discussion_replies dr
            LEFT JOIN persons pe ON dr.created_by = pe.id 
            WHERE discussion_thread_id = '.$r1['id'].'
            AND dr.status in ("S", "R")         
            ORDER BY dr.created_timestamp ASC';
                                 
        // echo $queryDiscussionReplies . '<br /><hr />';
         
        $result_list_discussion_reply = mysqli_query($connection, $queryDiscussionReplies);
         
        if (!$result_list_discussion_reply) {
         	show_mysqli_error_message($queryDiscussionReplies, $connection);
		 	die;
        }
             
        // echo 'Database Query Discussion Replies for Discussion Thread succeeded.<br /><hr />';
                 
        while($r2 = mysqli_fetch_assoc($result_list_discussion_reply)) {
                     
            if ($_SESSION['enrollment']['discussion_forum_moderator']) {
                $comment_reply_moderator = '&nbsp;&nbsp;<span class="label">Moderator</span>&nbsp;<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Remove this Comment Reply?"><a href="../php/delete_discussion_thread.php?type=reply&dr_id='.$r2['id'].'" onclick="return confirm(\'Are you sure you want to delete this Discussion Reply? Select OK to DELETE.\');"><i class="fa fa-remove icon-red"></i></a></span><br />';       
            } else {
                $comment_reply_moderator = null;
            }
             
            if (strlen($r2['first_name']) > 1) {
                $display_first_name = $r2['first_name'];
            } else {
                $display_first_name = 'Guest';          
            }       
                 
            $commentHeader = '
            <div class="commentHeader">                           
              <span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Person who posted this Comment Reply"><i class="fa fa-user icon-red"></i></span>&nbsp;'.$display_first_name.'&nbsp;
              <span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Date/Time this Comment Reply was posted">&nbsp;<i class="fa fa-clock-o icon-red"></i></span> '.date('m/d/y h:i A T', strtotime($r2['created_timestamp'])).'&nbsp;
              <span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Report Reply as SPAM"><a href="../php/report_discussion_forum_spam.php?forum_type=blogpost&comment_type=reply&comment_id='.$r2['id'].'" onclick="return confirm('."'Are you sure you want to report this Reply as SPAM? Select OK to REPORT.');".'"><i class="fa fa-ban icon-red"></i></a></span>&nbsp;'
              .$comment_reply_moderator.'                             
            </div>';
                     
            echo       
            '<div class="row">        
                <div id="reply'.$r2['id'].'" class="small-12 columns">                
                    <div class="topicCommentReply">';     
                                     
                        // echo $commentHeader;
                         
                        if (strlen($r2['file_document_url']) > 0) {		
							// echo 'File Document URL present<br />';
										
							echo
							// div id used for returning to this record in a URL using a #hashtag	
							'<div id="comment'.$r2['id'].'" class="row collapse">		
								<div class="small-12 medium-8 columns">				
									<div class="topicReply">'
									
										.$commentHeader.'	
																								
										<div class="commentBodyText"><p>'.$r2['text'].'</p></div>						
																								
									</div>				
								</div>
													
								<div class="small-12 medium-4 columns">									
									<div class="topicReplyBodyImage"><a href="'.$r2['file_document_url'].'" target="_blank" title="Preview document"><img src="https://cdn-manager.net/media/images/document.jpg" alt="" width="125"/></a></div>	
								</div>
														
							</div>';
							
						} elseif (strlen($r2['file_image_url']) > 0) {
                         
                            // echo 'File Image URL present<br />';
                                             
                            echo   
                            '<div class="row collapse">   
                             
                                                                                                                   
                                <div class="small-12 medium-8 columns">               
                                    <div class="topicReply">'
                                     
                                        .$commentHeader.'   
                                                                                                 
                                        <div class="commentBodyText"><p>'.$r2['text'].'</p></div>                       
                                                                                                 
                                    </div>                
                                </div>
                                
                                <div class="small-12 medium-4 columns">                                   
                                    <div class="topicReplyBodyImage"><img src="'.$r2['file_image_url'].'" alt="" /></div> 
                                </div>
                                                        
                            </div>';
                            
                        } elseif (strlen($r2['file_video_url']) > 0) {  
	                             
				            // echo 'File Video URL present<br />';
				         
				            echo   
				            '<div id="comment'.$r2['id'].'" class="row collapse">     
				                <div class="small-12 medium-8 columns">               
				                    <div class="topicReply">'
				                                             
				                        .$commentHeader.'   
				                         
				                        <div class="commentBodyText"><p>'.$r2['text'].'</p></div>
				                                                                                                     
				                    </div>                
				                </div>
				                                     
				                <div class="small-12 medium-4 columns">                                   
				                    <div class="commentBodyVideo">
				                        <video preload="none" controls poster="https://cdn-manager.net/media/images/user-video-poster.jpeg" style="width:100%;">
				                            <source src="'.$r2['file_video_url'].'" type="video/mp4">
				                            Your browser does not support HTML5 Video.
				                        </video>          
				                    </div>    
				                </div>
				                                         
				            </div>';
                   
                        } else {
                         
                            echo   
                            '<div class="row">        
                                <div class="small-12 columns">                
                                    <div class="topicReply">'     
                                                     
                                        .$commentHeader.'
                                                                                             
                                        <div class="commentBodyText"><p>'.$r2['text'].'</p></div>
                                                                                 
                                    </div>
                                </div>
                            </div>';
                             
                        }   
                         
                        echo '                      
                    </div>
                </div>
            </div>';              
                                                                                         
        } // while($r2 = mysqli_fetch_assoc($result_list_discussion_reply)) {
     
        mysqli_free_result($result_list_discussion_reply);
         
        if ($comments_block) {
            echo '</div></div></div> <!-- end blogPostCommentContainer -->';
            $comments_block = false;
        }
                         
        ?>
             
        <!-- <hr /> -->
         
        <?php
                             
    } // while($r1 = mysqli_fetch_assoc($result_list_discussion_thread)) {  
     
    mysqli_free_result($result_list_discussion_thread);
     
    include('../includes/discussion_forum_help_modal.php'); 
     
} // END if ($_SESSION['blog_post']['discussion_forum']['forum'] {  
	
// show_array($_SESSION);
   
?>