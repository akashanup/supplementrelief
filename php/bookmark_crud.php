<?php
include_once('../includes/header.php');

if ( ($_SESSION['page']['use_bookmark'] AND strlen($_SESSION['enrollment']['enrollment_id'])) > 0) {
	
	if (isset($_POST['action']) && $_POST['action'] == 'new') {

		$url = safe_sql_data($connection, $_POST['url']);
		list($total_records, $bookmark_id, $bookmark_title, $bookmark_comments) 	= 	checkForBookmark($connection,$url);
	
		$message_type 	= 	'';
		$message 		=	'';
		if ($total_records > 0) 
		{ 
			$message_type 	= 	'alert-box warning radius';		
			$message 		= 	'<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;You already have a <b>Bookmark</b> for this page.</p>';
		} 
		
		else 
		{
		
			$title = safe_sql_data($connection, $_POST['title']);
			$comments = safe_sql_data($connection, $_POST['comments']);		
							
			$created_timestamp = date('Y-m-d H:i:s');
			
			$queryInsertBookmark = '
			INSERT INTO bookmarks (enrollment_id, url, page_id, title, comments, created_by, created_timestamp) 
			VALUES (
			"'.$_SESSION['enrollment']['enrollment_id'].'",
			"'.$url.'",
			"'.$_SESSION['page']['page_id'].'",
			'.no_value_null_check($title).', 
			'.no_value_null_check($comments).', 
			"'.$_SESSION['user']['user_id'].'", 
			"'.$created_timestamp.'")';		
						
			$result_insert_bookmark = mysqli_query($connection, $queryInsertBookmark);
					
			if (!$result_insert_bookmark) {
				show_mysqli_error_message($queryInsertBookmark, $connection);
				die;
			}	
			
			mysqli_free_result($result_insert_bookmark);
							
			$message_type	= 	'alert-box success radius';		
			$message 		= 	'<p>Your <b>Bookmark</b> was successfully created.</p>';
			
		}	
		$html 				 				=	'<div id="bookmarkModalContent" class="'.$message_type.'">'	.
											  		'<h2 id="modalTitle">Bookmark</h2>'						.
										  			$message 												.
												'</div>'													.
												'<a class="close-reveal-modal" aria-label="Close">x</a>';
		echo $html;

	} elseif (isset($_POST['action']) && $_POST['action'] == 'edit') {
	
		
		$bookmark_id = safe_sql_data($connection, $_POST['bookmark_id']);
		$title = safe_sql_data($connection, $_POST['title']);
		$comments = safe_sql_data($connection, $_POST['comments']);		
								
		$modified_timestamp = date('Y-m-d H:i:s');
		
		$queryUpdateBookmark = '
		UPDATE bookmarks SET 
		title = '.no_value_null_check($title).', 
		comments = '.no_value_null_check($comments).',
		modified_by = '.$_SESSION['user']['user_id'].', 
		modified_timestamp = "'.$modified_timestamp.'" 
		WHERE id = "'.$bookmark_id.'"';	
		
		$result_update_bookmark = mysqli_query($connection, $queryUpdateBookmark);
		
		if (!$result_update_bookmark) {
			show_mysqli_error_message($queryUpdateBookmark, $connection);
			die;
		}
		
		mysqli_free_result($result_update_bookmark);
			
		$message_type 	= 	'alert-box success radius';		
		$message 		= 	'<p>Your  <b>Bookmark</b> was successfully updated.</p>';
		$html			=	'<div id="bookmarkModalContent" class="'.$message_type.'">'	.
						  		'<h2 id="modalTitle">Bookmark</h2>'						.
					  			$message 												.
							'</div>'													.
							'<a class="close-reveal-modal" aria-label="Close">x</a>';;
		echo $html;
						
	} elseif (isset($_POST['action']) && $_POST['action'] == 'delete') {
		
		$bookmark_id = safe_sql_data($connection, $_POST['bookmark_id']);

		$queryDeleteBookmark = '
		DELETE FROM bookmarks WHERE id = "'.$bookmark_id.'"';
		
		$result_delete_bookmark = mysqli_query($connection, $queryDeleteBookmark);
		
		if (!$result_delete_bookmark) {
			show_mysqli_error_message($queryDeleteBookmark, $connection);
			die;
		}
		
		mysqli_free_result($result_delete_bookmark);
		
		$message_type 	= 	'alert-box success radius';		
		$message 		= 	'<p>Your <b>Bookmark</b> was successfully deleted.</p>';
		
		$html			=	'<div id="bookmarkModalContent" class="'.$message_type.'">'	.
						  		'<h2 id="modalTitle">Bookmark</h2>'						.
					  			$message 												.
						  	'</div>'													.
						  	'<a class="close-reveal-modal" aria-label="Close">x</a>';
		echo $html;

	}

}