<?php
	include_once('../includes/header.php');
	
	function getCurrentUrl()
	{
		$currentUrl = '';
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$currentUrl 	=	$_SERVER['HTTP_REFERER'];
		}
		else
		{
		   $currentUrl     	=	((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	   	}
	   	return $currentUrl;
   	}

   	function generateEditBookmarkModal($bookmark_id = '', $bookmark_title = '', $bookmark_comments = '', $page_name = '', $url)
   	{
   		$page_name = $page_name ? $page_name : $_SESSION['page']['name'];
   		$html = '';
   		$html .=
   			'<div id="editBookmarkForm">'																.
				'<form data-abide autocomplete="off">'													.
					'<input type="hidden" name="action" value="edit"/>'									.
					'<input type="hidden" name="bookmark_id" value="'.$bookmark_id.'"/>'				.
					'<fieldset>'																		.
						'<legend>Edit Bookmark</legend>'												.
						'<div class="row">'																.
							'<div class="small-12 columns">'											.
								'<label>Page Type'														.
									'<input disabled="disabled" type="text" name="page_type" value="'.$page_name.'"/>'						  								 .
								'</label>'																.
							'</div>'																	.
						'</div>'																		.
						
						'<div class="row">'																.
							'<div class="small-12 columns">'											.
								'<label>Title <small>Optional</small>'									.
									'<textarea autofocus name="title" placeholder="Bookmark title ..." rows="2">'.$bookmark_title.'</textarea>'									   .
								'</label>'																.
							'</div>'																	.
						'</div>'																		.
				
						'<div class="row">'																.
							'<div class="small-12 columns">'											.
								'<label>Description <small>Optional</small>'							.
									'<textarea name="comments" placeholder="Optional comment to describe your bookmark ..." rows="3">'.$bookmark_comments.'</textarea>'					    .
								'</label>'																.
							'</div>'																	.
						'</div>'																		.
															
						'<div class="row">'																.
							'<div class="small-12 columns">'											.
						    	'<label>'	      														.
									'<input class="button small radius" type="button" id="updateBookmarkBtn" value="Update">'.
									'&nbsp;&nbsp;<a href="javascript:void(0)" data-url="'.$url.'" data-id="'.$bookmark_id.'" class="button small radius" id="deleteBookmarkModalBtn"><b>Delete</b></a>'			  	  .
								'</label>'																.
						  	'</div>'																	.
						'</div>'																		.			
					'</fieldset>'																		.
				'</form>'																				.
	    	'</div>'																					.
	    	'<a class="close-reveal-modal" aria-label="Close">x</a>';

    	echo $html;
   	}

   	function generateDeleteBookmarkModal($bookmark_id = '', $url = '')
   	{
   		$html = '';
   		$html .= 
   			'<form data-abide autocomplete="off">'	.
	   			'<input type="hidden" name="action" value="delete"/>'	.
	   			'<input type="hidden" name="bookmark_id" value="'.$bookmark_id.'"/>'.
	   			'<input type="hidden" id="bookmark_url" value="'.$url.'"/>'.
	   			'<div id="deleteBookmarkForm">'			.
					'<div class="row" >'				.
						'<div class="small-12 columns" style="text-align:center" >'		.
							'<p>Are you sure you want to <b>DELETE</b> this Bookmark?</p>'	.
						'</div>'	.
					'</div>'		.
					
					'<div class="row" >'	.
						'<div class="small-6 columns" >'	.
							'<input class="small button radius right" type="button" id="deleteBookmarkYes" value="Yes" />'	.
						'</div>'	.	
						'<div class="small-6 columns" >'																	.
							'<input class="small button radius left" type="button" id="deleteBookmarkNo" value="No" /> '	.
						'</div>'	.
					'</div>'	.
	   			'</div>'	.
   			'</form>'.
   			'<a class="close-reveal-modal">x</a>';

   		echo $html;
   	}

   	$currentUrl 	=	getCurrentUrl();
	$currentUrl     =   strrpos( $currentUrl, "?") ? substr( $currentUrl, 0, strrpos( $currentUrl, "?")) : $currentUrl;

    list($total_records, $bookmark_id, $bookmark_title, $bookmark_comments) = checkForBookmark($connection, $currentUrl);

    $_SESSION['page']['isBookmarked'] = $total_records > 0 ? true : false;
    
?>

<?php
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
	{
		$key = safe_sql_data($connection, $_POST['key']);
		switch ($key) 
		{
			case 'edit':
				$page_name 	=	'';
				$url 		=	$currentUrl;
				if(isset($_POST['id']))
				{
					$bookmark_id = safe_sql_data($connection, $_POST['id']);
				}
				if(isset($_POST['title']))
				{
					$bookmark_title = safe_sql_data($connection, $_POST['title']);
				}
				if(isset($_POST['comments']))
				{
					$bookmark_comments = safe_sql_data($connection, $_POST['comments']);	
				}
				if(isset($_POST['page_name']))
				{
					$page_name = safe_sql_data($connection, $_POST['page_name']);
				}
				if(isset($_POST['url']))
				{
					$url = safe_sql_data($connection, $_POST['url']);
				}
				generateEditBookmarkModal($bookmark_id, $bookmark_title, $bookmark_comments, $page_name, $url);
				break;
			
			case 'delete':
				if(isset($_POST['id']))
				{
					$bookmark_id = safe_sql_data($connection, $_POST['id']);
				}
				if(isset($_POST['url']))
				{
					$url = safe_sql_data($connection, $_POST['url']);
				}
				generateDeleteBookmarkModal($bookmark_id, $url);
				break;
		}
	}
?>

<?php
	if(!(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'))
		{
?>
    <div style="position:fixed;float:right;z-index:999;right:20px;top:95px;">
        <a href="javascript:void(0)" id="bookmarkBtn">
	        
	        <?php
                    
		    if($_SESSION['page']['isBookmarked'])
		    {
		        ?><i class="fa fa-bookmark fa-2x" aria-hidden="true" title="You already have a Bookmark for this page."></i><?php         
		    }
		    else
		    {
		        ?><i class="fa fa-bookmark-o fa-2x" aria-hidden="true" title="Create a Bookmark for this page."></i><?php
		    }
		    
		    ?>                           
    
    	</a>
    </div>
    
    <div id="bookmarkModal" class="reveal-modal small" data-reveal aria-labelledby="bookmarkModalTitle" aria-hidden="true" role="dialog">
    	<h2>Bookmarks&nbsp;<small><?php echo $_SESSION['enrollment']['project_program_name']; ?></small></h2>
    	<hr>
    	<div id="bookmarkModalBody">
	    	<?php
				if($_SESSION['page']['isBookmarked'])
			    {
	        ?>
	        		<a href="javascript:void(0)" id="editBookmarkLnk" title="Edit Bookmark for this page"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Edit Bookmark for this Page</a><br>
			<?php         
			    }
				else
			    {
	        ?>
	        		<a href="" data-reveal-id="newBookmark" title="Bookmark this page"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Bookmark this Page</a><br>
			<?php
			    }
			?>
		</div>
		<a href="javascript:void(0)" id="showBookmarksLnk" title="Show all bookmarks"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Show All Bookmarks</a><br>

     	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>
    
    <div id="showBookmarks" class="reveal-modal large" data-reveal aria-hidden="true" role="dialog">	
    </div>
    
    <div id="newBookmark" class="reveal-modal small" data-reveal aria-labelledby="newBookmarkTitle" aria-hidden="true" role="dialog">
     	
    	<div id="newBookmarkForm">

			<form data-abide autocomplete="off">

				<input type="hidden" name="action" value="new"/>

				<!-- <input type="hidden" name="enrollment_id" value="<?php echo ($_SESSION['enrollment']['enrollment_id'] ? $_SESSION['enrollment']['enrollment_id'] : ''); ?>"/> -->
				<!-- <input type="hidden" name="page_id" value="<?php echo ($_SESSION['page']['page_id'] ? $_SESSION['page']['page_id'] : ''); ?>"/> -->
				<input type="hidden" name="url" value="<?php echo ($currentUrl ? $currentUrl : ''); ?>"/>
	
				<fieldset>
										
					<legend>New Bookmark</legend>
					
					<div class="row">
						<div class="small-12 columns">
							<label>Page Type
								<input disabled="disabled" type="text" name="page_type" value="<?php echo ($_SESSION['page']['name'] ? $_SESSION['page']['name'] : ''); ?>" />
							</label>
							<small class="error">Page Name is optional</small>	    	
						</div>
					</div>
					
					<div class="row">
						<div class="small-12 columns">
							<label>Title <small>Optional</small>
								<textarea autofocus name="title" placeholder="Bookmark title ..." rows="2" /><?php echo ($page_meta_title ? $page_meta_title : ''); ?></textarea>
							</label>
							<small class="error">Title is optional</small>
						</div>
					</div>

				
					<div class="row">
						<div class="small-12 columns">
							<label>Description <small>Optional</small>
								<textarea name="comments" placeholder="Optional comment to describe your bookmark ..." rows="3" /><?php echo ($page_meta_description ? $page_meta_description : ''); ?></textarea>
							</label>
							<small class="error">Description is optional</small>
						</div>
					</div>
														
					<div class="row">
						<div class="small-12 columns">
					    	<label>	      
								<input class="button medium radius" type="button" id="createBookmarkBtn" value="Create Bookmark">
							</label>
					  	</div>
					</div>	
								
				</fieldset>
				
			</form>

    	</div>
    	
    	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>
    
    <div id="editBookmark" class="reveal-modal small" data-reveal aria-hidden="true" role="dialog">     	
    </div>
    			
	<div id ="deleteBookmark" class="reveal-modal small" data-reveal aria-hidden="true" role="dialog">
	</div>		

	<div id="bookmarkResponseModal" class="reveal-modal small" data-reveal aria-hidden="true" role="dialog">
    </div>
<?php
	}
?>