<?php
	include_once('../includes/header.php');
	$option_id 	= mysqli_real_escape_string($connection, stripslashes(htmlspecialchars(trim($_POST['option_id']))));
	$bpu_id 	= $_POST['bpu_id'];

	
	if($option_id != '' && $bpu_id != '')
	{
		$price = get_product_option_price($connection, $option_id, $bpu_id);
		
		$query 		=	
		'SELECT id, brand_product_usage_option_parent_id, variant_brand_product_usage_id 
		FROM brand_product_usage_options 
		where  brand_product_usage_option_parent_id IS NOT NULL 
		AND variant_brand_product_usage_id IS NULL
		AND id =' .$option_id.
		' AND brand_product_usage_id ='.$bpu_id;
		$fetchQuery =	mysqli_query($connection,$query);	
		$rowCount   =   mysqli_num_rows($fetchQuery);
		if($rowCount)
		{
			$result = mysqli_fetch_assoc($fetchQuery);
			$option_id = $result['brand_product_usage_option_parent_id'];
		}

		$resultQueryOptionProduct 	= 	get_query_option_product($connection, $option_id, $bpu_id);
		$resultOptionProduct 		= 	mysqli_fetch_assoc($resultQueryOptionProduct);
		$optionProductCount 		= 	mysqli_num_rows($resultQueryOptionProduct);

		$resultQueryOptionSpecifications 	= 	get_query_option_specifications($connection, $option_id, $bpu_id);
		$optionSpecificationsCount 			= 	mysqli_num_rows($resultQueryOptionSpecifications);

		$json = [];
		if($optionProductCount == 1 )
		{
			$feature_image = '';
			if($resultOptionProduct['feature_image_host_url'] != ''  && $resultOptionProduct['feature_image_alt_text'] != '')
			{
				$feature_image = getProductImage($_SESSION['application']['root_media_host_url'],$resultOptionProduct['feature_image_host_url'],$resultOptionProduct['feature_image_alt_text']);
			}
			$json['feature_image'] 				= 	$feature_image;
			$json['description_html'] 			= 	cleanEncoding($resultOptionProduct['description_html']);
			$json['benefits_html'] 				= 	cleanEncoding($resultOptionProduct['benefits_html']);
			$json['directions_html'] 			= 	cleanEncoding($resultOptionProduct['directions_html']);
			$json['project_brand_product_name'] = 	$resultOptionProduct['name'];
			$json['upc'] 						= 	$resultOptionProduct['upc'];
			$json['sku'] 						= 	$resultOptionProduct['sku'];
			$json['retail_price'] 				= 	$price;
			$json['manufacturer'] 				= 	$resultOptionProduct['manufacturer'];
			$json['group']						=	$bpu_id;
		}
		if($optionSpecificationsCount > 0)
		{
			$ingredients 			=  getProductIngredients($resultQueryOptionSpecifications);
			$json['ingredients'] 	=	$ingredients;
		}
		$json = json_encode($json);
		echo $json;
	}
?>
