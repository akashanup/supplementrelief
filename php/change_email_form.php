<div class="row">
	<div class="small-12 columns">

		<form data-abide action="../php/change_email_process.php" method="post" />		
		  <fieldset>		  
		    <legend>Change Email Address</legend>
		    
		    <div class="row">
		      <div class="small-12 columns">
		      	<p>Please provide your <strong>Username</strong>, <strong>Current Email Address</strong>, <strong>New Email Address</strong> then press the <strong>CHANGE EMAIL ADDRESS</strong> button.</p>
		      </div>
		    </div>
		    		
			<div class="row">
			
			  <div class="small-12 medium-4 columns">
			    <label>Username
			      <input type="text" required name="username" placeholder="MyUserName" value="<?php echo $_SESSION['user']['username']; ?>" />
			    </label>
			    <small class="error">Username is required</small>    
			  </div>
			  
			</div>
			
			<div class="row">
			
				<div class="small-12 medium-6 columns">
				  <label>Current Email Address
				    <input type="email" required name="current_email" placeholder="jane.doe@somedomain.com" value="<?php echo $_SESSION['current_email']; ?>"/>
				  </label>
				  <small class="error">Valid Current Email Address is required</small>
				</div>
				
				<div class="small-12 medium-6 columns">
				  <label>New Email Address
				    <input type="email" required name="new_email" placeholder="jane.doe@somedomain.com" value="<?php echo $_SESSION['new_email']; ?>"/>
				  </label>
				  <small class="error">Valid New Email Address is required</small>
				</div>		
				
			</div>    
						
			<div class="row">
				  
			  <div class="small-12 medium-4 columns">
			    <label>Are you human? Enter Code:	      
			      <img src="../includes/captcha.php" style="margin-bottom: -8px;" /><input type="text" required name="captcha_input" value=""/>
			    </label>
			    <small class="error">Valid Code is required</small>	      	    
			  </div>
		
			</div>		
			
			<div class="row">
			
			  <div class="small-12 columns">
			  
			    <label>	      
			      <input class="button medium radius" id="button" type="submit" value="CHANGE EMAIL ADDRESS">      
			    </label>
			    	    
			  </div>
		
			</div>	
							
		  </fieldset>		
		</form>
	</div>
</div>