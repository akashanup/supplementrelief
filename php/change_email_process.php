<?php

// echo 'Inside of chnage_email_process.php<br><hr>';

include('../includes/header.php');


//$_SESSION['message_type'] = 'alert-box warning radius';	
//$_SESSION['message'] = '<p><b>Change Email Process</b> in development and will be completed soon.</p>';
//header("location: ../../change_email/index.php");

//set POST rvalues to SESSION for return values
$_SESSION['user']['username'] = addslashes(cleanEncoding(strtolower($_POST['username'])));
$_SESSION['current_email'] = addslashes(cleanEncoding(strtolower($_POST['current_email'])));
$_SESSION['new_email'] = addslashes(cleanEncoding(strtolower($_POST['new_email'])));

//validate for required fields

// check captcha entry
if ($_SESSION['captcha'] != $_POST['captcha_input']) {
	$_SESSION['message_type'] = 'alert-box alert radius';		
	$_SESSION['message'] = '<p>The <strong>CODE</strong> you entered is <strong>NOT VALID</strong>. Please try again.</p>';
	header("location: ../change_email/index.php");
	exit();
}

$_SESSION['message_type'] = 'alert-box success radius';		
$_SESSION['message'] = '<p>SOZO v1 Change Email Process in development to be completed during March 2015.</p>';
header("location: ../change_email/index.php");
exit();

/*


// check to see if user exist
$query = 'SELECT * FROM persons WHERE username = "'.$_SESSION['user']['username'].'" AND email = "'.$_SESSION['user']['email'].'"';

$result = mysql_query($query);

$row = mysql_fetch_assoc($result);

if($row) {
	// user exist

	// create temporary password	
	$randpw = rand_passwd();
	$temp_pw = doubleSalt($randpw,$row['username']);
	
	$query1 = 'UPDATE persons SET temp_password = "'.$temp_pw.'" WHERE id = '.$row['id'];
	$result1 = mysql_query($query1); 

	// send verification email
	$to = $row['email'];
	$from = 'support@wrightcheck.com';
	$subject = 'Wright Check Temporary Password Reset';
	$BCC = '';
	
	$message = '
	<br>
	A temporary password has been setup for the next time you LOGIN to the system.
	<br><br>
	<b>Username: '.$row['username'].'<br>
	Temporary Password: '.$randpw.'</b>
	<br><br>
	Please click the LINK below, LOGIN to the system, and establish a new password:
	<br><br>
	<a href="http://'.$_SERVER['SERVER_NAME'].'/spark/login/index.php" ><b>LOGIN USING TEMPORARY PASSWORD</b></a>
	<br><br>
	If you did not request to have your password RESET, please click this link below:
	<br><br>
	<a href="http://'.$_SERVER['SERVER_NAME'].'/spark/report_password_abuse/index.php?temppw=!34rr!&email='.$row['email'].'" ><b>REPORT PASSWORD RESET ABUSE</b></a>
	<br><br>
	If you have any problems, please contact us at support@wrightcheck.com.
	<br><br>
	Wright Check Support Team
	<br><br>
	
	';
	
	send_email_html($from, $to, $subject, $message, $BCC);
		
	// log system event			
	$event = 'PASSWORD';
	$page = 'Reset Password';
	$log_description = 'User REQUEST RESET PASSWORD - Reset Password email sent for Username: '.$_SESSION['user']['username'].' and Email: '.$_SESSION['user']['email'].'.';
	$flag = 'NULL';
	create_spark_log($event, $page, $log_description, $flag);
	
	// RESULT MESSAGE
	$_SESSION['message_type'] = 'alert-box success radius';		
	$_SESSION['message'] = '<p>Hello <strong>'.$row['first_name'].' '.$row['last_name'].'</strong>. We have generated a <strong>TEMPORARY PASSWORD</strong> for you. An email has been sent to the Email Address <strong>'.$_SESSION['user']['email'].'</strong>. Please follow the instructions to <strong>RESET THE PASSWORD</strong> to your account.</p>';
	
	// Clear SESSION vars
	$_SESSION['user']['username'] = '';
	$_SESSION['user']['email'] = '';
	
	header("location: ../../message/index.php");
	exit();
	
}else{
	
	// log system event			
	$event = 'PASSWORD';
	$page = 'Reset Password';
	$log_description = 'User REQUEST RESET PASSWORD attempt FAILED for Username: '.$_SESSION['user']['username'].' and Email: '.$_SESSION['user']['email'].'.';
	$flag = 1;
	create_spark_log($event, $page, $log_description, $flag);
	
	$_SESSION['message_type'] = 'alert-box alert radius';		
	$_SESSION['message'] = '<p>This <strong>Username</strong> and <strong>Email Address</strong> was not found in our system. Please try again.</p>';
	header("location: ../../reset_password/index.php");
	exit();
}