<?php 

// echo 'Begin Change Email Verification page.<br />';

include_once('../includes/header.php');

if (!$_SERVER['REQUEST_METHOD'] == "GET") {
	echo "You literally can't even.";
	exit;
}
	
if (@isset($_GET['si'], $_GET['aa'])) {
	
	// Valid link clicked from email to User to Confirm new email address
		
	$si = sanitize($_GET['si']);
	$aa = sanitize($_GET['aa']);
	
	if ($stmt = $connection->prepare("SELECT temp_password, email, temp_email FROM persons 
		WHERE id = ?
        LIMIT 1")) {
        $stmt->bind_param('s', $si);  // Bind parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($askee, $current_email, $requested_email);
        $stmt->fetch();
		$stmt->close();
	} else {
		
		echo 'Person query failed.<br>';
		die;
			
	}
	
	if ($aa !== "" && $aa == $askee) {
			
		// Valid user found from email link
		if ($stmt = $connection->prepare("UPDATE persons SET 
			previous_email = '" .$current_email ."',
			email = '" .$requested_email ."',  
			temp_password='' " . 
			"WHERE id = ?")) {
			// Bind "$si" to parameter. 
			$stmt->bind_param('i', $si);
			$stmt->execute(); // Execute the prepared query.
			
			// echo 'Your Email has been updated. You may now login <a href="../login/">here</a>.';
						
			$stmt->close();
			// exit;
			
			// Send email to Customer Support informing them of Email change confirmation
			$queryPerson = 'SELECT * 
			FROM persons
			WHERE id = "'.$si.'"';	
										
			// echo $queryPerson;
			// die();
			
			$result_person = mysqli_query($connection, $queryPerson);
			
			if (!$result_person) {
				show_mysqli_error_message($queryPerson, $connection);
				die;
			}
			
			$person = mysqli_fetch_assoc($result_person);
			
			$timestamp = date('Y-m-d H:i:s');
	
			$from = $_SESSION['application']['name'].' <support@'.$_SESSION['application']['domain'];
			$to = 'support@'.$_SESSION['application']['domain'];
			$subject = 'SupplementRelief Member Account Email Change Confirmation';
			
			$body = '<b>Name: </b>'.$person['first_name'].' '.$person['last_name'];
			$body .= '<br><b>Username: </b>'.$person['username'];
			$body .= '<br><b>New Email: </b>'.$person['email'];
			$body .= '<br><b>Previous Email: </b>'.$person['previous_email'];
			$body .= '<br><b>Timestamp: </b>'.date('m/d/y h:i A T', strtotime($timestamp)).'<br>';
	
			send_email_html($from, $to, $subject, $body, "");
			
			mysqli_free_result($result_person);
			
			session_destroy();
			
			$target_uri = '../login/?msg=2&u_id='.$si;
			// echo 'Target URI: '.$target_uri.'<br /><hr />';
			// die;
			header("location: $target_uri"); // Success message displayed to User on Login page.	

			// header('Location: ../login/?msg=1'); // Success message displayed to User on Login page.	
		
		} else {
			echo 'Member Account Email update failed (Error: DB)';
			$stmt->close();
			exit;
		}	
	
	}

}

?>