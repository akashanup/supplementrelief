<?php
	
// echo 'Inside of change_user_email_local.php<br><hr>';
	
include '../includes/header.php';

if (!$_SERVER['REQUEST_METHOD'] == "POST") {
	echo "You literally can't even.";
	exit;
}

// show_array($_POST);
// die;

if (isset($_POST['action']) && ($_POST['action'] == 'change')) {

	$person_id = addslashes(cleanEncoding($_POST['person_id']));
	$user_email = addslashes(cleanEncoding($_POST['user_email']));
						
	$queryUniqueEmail = 'SELECT * 
		FROM persons
		WHERE email = "'.$user_email.'"';	
									
	// echo $queryUniqueEmail;
	// die();
	
	$result_unique = mysqli_query($connection, $queryUniqueEmail);
	
	if (!$result_unique) {
		show_mysqli_error_message($queryUniqueEmail, $connection);
		die;
	}
	
	$total_records = mysqli_num_rows($result_unique);
		
	if ($total_records > 0) { 
		
		echo 'The Email<b> '.$user_email.'</b> is not available. Please choose a different one.<br><br><input class="button medium radius expand" type="button" value="CHANGE" onClick="doChangeEmailLocal();">';
		exit();
		
	} else {
		
		// Email is available
				
		echo 'A confirmation email was sent to <b>'.$user_email.'</b>. You must respond to <b>verify that this is YOUR email</b> before your account will be updated. If you do not receive the confirmation email, make sure <b>the email you entered is correct</b> and that the confirmation email is not in your <b>SPAM</b> or <b>JUNK</b> email folders.<br><br>if you need help <a href="../contact/">email</a> or call us at <b>(888) 424-0032</b>.';
		
		$from = $_SESSION['application']['name'].' <support@'.$_SESSION['application']['domain'];
		// $from = 'support@'.$_SESSION['application']['domain'];
		$to = $user_email;
		$subject = 'Confirm SupplementRelief Member Account Email Change';
		
		$activate = md5(uniqid(mt_rand(), true));
		
		$connection->query("UPDATE persons SET temp_password='$activate', temp_email='$user_email' WHERE id='$person_id'");
					
		$activelink = $_SESSION['application']['root_url'].'/php/change_email_verification.php?si='.$person_id.'&aa='.$activate;
					
		$body = 'Click <a href="'.$activelink.'">here</a> to <b>confirm your SupplementRelief Member Account Email change</b>. Otherwise your account will continue to use the previous Email provided.<br><br>Please reply to this email if you need help or call us at <b>(888) 424-0032</b>.';	

		send_email_html($from, $to, $subject, $body, "");
								
		// Send email to Customer Support informing them of Email change
		$queryPerson = 'SELECT * 
		FROM persons
		WHERE id = "'.$person_id.'"';	
									
		// echo $queryPerson;
		// die();
		
		$result_person = mysqli_query($connection, $queryPerson);
		
		if (!$result_person) {
			show_mysqli_error_message($queryPerson, $connection);
			die;
		}
		
		$person = mysqli_fetch_assoc($result_person);
		
		$timestamp = date('Y-m-d H:i:s');

		$from = $_SESSION['application']['name'].' <support@'.$_SESSION['application']['domain'];
		$to = 'support@'.$_SESSION['application']['domain'];
		$subject = 'SupplementRelief Member Account Email Change';
		
		$body = '<b>Name: </b>'.$person['first_name'].' '.$person['last_name'];
		$body .= '<br><b>Username: </b>'.$person['username'];
		$body .= '<br><b>Current Email: </b>'.$person['email'];
		$body .= '<br><b>Requested Email: </b>'.$user_email;
		$body .= '<br><b>Timestamp: </b>'.date('m/d/y h:i A T', strtotime($timestamp)).'<br>';

		send_email_html($from, $to, $subject, $body, "");
		
		mysqli_free_result($result_person);
		
		exit();
		
	}
	
}

?>