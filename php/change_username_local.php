<?php
	
// echo 'Inside of change_username_local.php<br><hr>';
	
include '../includes/header.php';

if (!$_SERVER['REQUEST_METHOD'] == "POST") {
	echo "You literally can't even.";
	exit;
}

// show_array($_POST);
// die;

if (isset($_POST['action']) && ($_POST['action'] == 'change')) {

	$person_id = addslashes(cleanEncoding($_POST['person_id']));
	$username = addslashes(cleanEncoding($_POST['username']));
						
	$queryUniqueUsername = 'SELECT * 
		FROM persons
		WHERE username = "'.$username.'"';	
									
	// echo $queryUniqueUsername;
	// die();
	
	$result_unique = mysqli_query($connection, $queryUniqueUsername);
	
	if (!$result_unique) {
		show_mysqli_error_message($queryUniqueUsername, $connection);
		die;
	}
	
	$total_records = mysqli_num_rows($result_unique);
		
	if ($total_records > 0) { 
		
		echo 'User ID<b> '.$username.'</b> is not available. Please choose a different one.<br><br><input class="button medium radius expand" type="submit" value="CHANGE">';
		exit();
		
	} else {
		
		// Username is available
						
		$queryUpdateUsername = '
		UPDATE persons SET 
		username = "'.strtolower($username).'" 
		WHERE id = "'.$person_id.'"';	
		
		// echo $queryUpdateUsername;	
		// die();
			
		$result_update = mysqli_query($connection, $queryUpdateUsername);
		
		if (!$result_update) {
			show_mysqli_error_message($queryUpdateUsername, $connection);
			die;
		}
		
		mysqli_free_result($result_update);
		
		echo 'Your User ID has been changed to <b> '.$username.'</b>. You may use it the next time you login.';
		exit();
		
	}
	
}

?>