<?php

// error_reporting(E_ALL);	
include_once('../includes/header.php');
	
// show_session();

if (strlen($_SESSION['user']['full_name']) > 1) {
	// echo 'Session Display Name: '.$_SESSION['user']['full_name'].'<br />';
	// echo strlen($_SESSION['user']['full_name']).'<br />';
	
	if (strlen($name) > 1) {
		// do nothing
	} else {
		$name = $_SESSION['user']['full_name'];			
	}
	
}

if (strlen($_SESSION['user']['email']) > 1) {
	// echo 'Session Email: '.$_SESSION['user']['email'].'<br />';	
	// echo strlen($_SESSION['user']['email']).'<br />';
	
	if (strlen($email) > 1) {
		// do nothing
	} else {
		$email = $_SESSION['user']['email'];	
	}
		
}

if (strlen($_SESSION['user']['user_phone']) > 1) {
	// echo 'Session Phone: '.$_SESSION['user_phone'].'<br />';	
	// echo strlen($_SESSION['user_phone']).'<br />';
	
	if (strlen($phone) > 1) {
		// do nothing
	} else {
		$phone = $_SESSION['user']['user_phone'];	
	}
		
}

if (isset($_POST['action']) && $_POST['action'] == 'send_email') {

	// show_array($_POST);
		
	// set POST return values to local variables for processing
	$name = safe_sql_data($connection, $_POST['name']);
	$email = safe_sql_data($connection, $_POST['email']);
	$phone = safe_sql_data($connection, $_POST['phone']);
	// $message = safe_sql_data($connection, $_POST['message']); // strips out HTML and other code tags which may be desireable in certain situations.
	$message = safe_data($_POST['message']);
	
	$contact_method = safe_sql_data($connection, $_POST['contact_method']);
	
	/*	Previous captcha used before Google Recaptcha
	// check captcha entry
	if ($_SESSION['captcha'] != $_POST['captcha_input']) {
		$_SESSION['message_type'] = 'alert-box alert radius';		
		$_SESSION['message'] = '<p>The <strong>CODE</strong> you entered is <strong>NOT VALID</strong>. Please try again.</p>';
		show_session_message();
    }
	*/
	
		if (googleRecaptcha(private_captcha_key, sanitize($_POST['g-recaptcha-response']), sanitize($_SERVER['REMOTE_ADDR']), sanitize($_SERVER['HTTP_REFERER'].'#sessionMessage'))) {
		// passed Google Recaptcha
		// phone required for Preferred Contact Method of Phone	
		
		if ($contact_method == "P" && empty($phone)) {
			$_SESSION['message_type'] = 'alert-box alert radius';		
			$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;<strong>Phone</strong> is required for your Preferred Contact Method.</p>';
			show_session_message();
			
		} else {
			
			// prepare and send email
			if ($contact_method == "E") { 
				$preferred_contact_method = "email"; 		
			} elseif ($contact_method == "P") {
				$preferred_contact_method = "phone"; 		
			}
			
			$from = $email;	
			$to = $_SESSION['application']['email'];
			// $to = 'jay.todtenbier@rtsadventures.com';
			$subject = $_SESSION['application']['copyright_name'].' Contact Notification';
			$BCC = '';
			
			$email_body_content = '
			From: '.$name.'
			<br /><br />
			Message: <br /><br />'.$message.'
			<br />
			<br />
			Phone: '.$phone.'
			<br />
			Email: '.$email.'
			<br />
			<br />
			Please contact me by '.$preferred_contact_method.'.
			<br /><br />';
				
			send_email_html($from, $to, $subject, $email_body_content, $BCC);
			
			// Create the Contact Message in the database
			$created_timestamp = date('Y/m/d H:i:s');
			// echo 'Current Date and Time is : '.$created_timestamp.'<br /><hr />';
			
			//
			$queryInsertContactMessage = '
			INSERT INTO contact_form_messages (created_timestamp, page_id, user_id, name, email, phone, contact_method, message) 
			VALUES (
			"'.$created_timestamp.'", 
			"'.$_SESSION['page']['page_id'].'", 
			"'.$_SESSION['page']['user_id'].'", 
			"'.$name.'", 
			"'.$email.'", 
			"'.$phone.'", 
			"'.$contact_method.'", 
			"'.$message.'")';
				
			// echo $queryInsertContactMessage.'<br /><hr />';
			
			$result_insert_contact_message = mysqli_query($connection, $queryInsertContactMessage);
			
			if (!$result_insert_contact_message) {
				show_mysqli_error_message($queryInsertContactMessage, $connection);
				die;
			}
								
			// Confirmation Message to User
			// $_SESSION['message_type'] = 'alert-box success radius';				
			$_SESSION['message'] = '
			<h1><i class="fa fa-thumbs-up icon-green" aria-hidden="true"></i>&nbsp;Message Received</h1>
			<p>Thank you for contacting <b>'.$_SESSION['application']['copyright_name'].'</b>. A representative will respond soon. Your message appears below:</p>
			<blockquote>'.$message.'</blockquote>';
								
			// Redirect user to Contact Confirmation page.
			$target_uri = '../contact-confirmation/';
			header("location: ".$target_uri);	
			exit();
				
		}
		
		
	} else {
		
		echo 'googleRecaptcha failed.<br>';
	
	}
			
}
	
?>

<div class="row">
	<div class="small-12 columns">
		
		<form data-abide action="../contact/" method="post" />
		
			<input type="hidden" name="action" value="send_email"/>
									
			<fieldset>			  
		    	<legend>Contact <?php echo $_SESSION['application']['copyright_name']; ?></legend>
		    	
		    	<div class="row">
	    			<div class="small-12 columns">
	    				<p>Please email using the form below. Alternatively you may call <b><?php echo $_SESSION['application']['phone']; ?></b>.</p>
	    			</div>
		    	</div>
		    	
		    	<div class="row">
			    	
		    		<div class="small-12 medium-6 columns">
			    				    			    		    			
		    			<label>Name
		    				<input type="text" autofocus required name="name" placeholder="Jane Doe" value="<?php echo ($name ? $name : ''); ?>" id="ajax-contact-form-name" />
		    			</label>
		    			<small class="error">Name is required</small>
		    			 
						<label>Email
							<input type="email" required name="email" placeholder="jane.doe@domain.com" value="<?php echo ($email ? $email : ''); ?>" />  	
						</label>
						<small class="error">Email is required</small>
				
		    			<fieldset>		  
		    				<legend>Preferred Contact Method</legend>
		    					<input type="radio" required name="contact_method" value="E" <?php if($contact_method == 'E') echo 'checked'; ?> id="ajax-contact_method_email" />
		    					<label for="ajax-contact_method_email">Email</label>
		    					<input  type="radio" required name="contact_method" value="P" <?php if($contact_method == 'P') echo 'checked'; ?> id="ajax-contact_method_phone" />
		    					<label for="ajax-contact_method_phone">Phone</label>
								<small class="error">Preferred Contact Method is required</small>		
		    			</fieldset>
			    					    			
		    			<div id="ajax-contact-method-display">
			    			<?php
								if ($phone) {
									?>  				
									<div class="small-12 columns">		    
										<label>Phone
											<input type="tel" name="phone" placeholder="(555) 555-1212" value="<?php echo ($phone ? $phone : ''); ?>" />			
										</label>
										<small class="error">Phone is required and must be formatted as (555) 555-1212</small>
									</div>
									<?php
								}	
							?>			    						  				    						    				
						</div>
						
		    		</div>
		    			
		    		<div class="small-12 medium-6 columns">
		    		
		    			<div class="row">
		    		
		    				<div class="small-12 columns">		    	
					    		<label>Message
					    			<textarea required name="message" placeholder="Your message..." rows="8" /><?php echo ($message ? $message : ''); ?></textarea> 
					    		</label>
					    		<small class="error">Message is required</small>
					    	</div>
				    		
				    		<!-- Internal Recaptcha that works fine if we do not use Google Recaptcha
				    		<div class="small-12 columns">		    		
				    			<label>Are you human? Enter Code:	      
				    		    	<img src="../includes/captcha.php" style="margin-bottom: -8px;" /><input type="text" required name="captcha_input" value=""/>
				    		  	</label>
				    		  	<small class="error">Valid Code is required</small>	      	    
				    		</div>
				    		-->
				    						    						    					    			
		    			</div>
		    			    		    		
		    		</div>   		    		
		    		
		    	</div>
		    	
		    	<div class="row">		
		    		<div class="small-12 columns">
			    		<!-- Paste this snippet at the end of the <form> where you want the reCAPTCHA widget to appear. -->
	    		    	<!-- <div class="g-recaptcha" data-sitekey="6LeZlBsTAAAAAMsmPQ3JFXZ9jXp-p-zFsTiDCSJ8"></div> -->
	    		    	<div class="g-recaptcha" data-sitekey="<?php echo public_captcha_key; ?>"></div>
	     			</div>
		    	</div>
		    	
		    	<div class="row">		
		    		<div class="small-12 columns">
			    		<br />
						<input class="button large radius expand" type="submit" value="Submit" />      					
		    		</div>
		    	</div>
		    	
		    </fieldset>		
		</form>
		
	</div>
</div>

<div class="row">
			
	<div class="small-12 medium-8 columns">
		<img src="https://cdn-manager.net/media/images/we-are-here-to-help-large-landscape.jpg" alt="we are here to help">
		<div class="caption">Call or email anytime. We respond quickly.</div>
	</div>
	
	<div class="small-12 medium-4 columns show-for-small-up">
		<center><img src="https://cdn-manager.net/media/images/jay.jpg" width="98%" alt="jay, customer service representative"></center>
		<div class="caption">Customer Service: Jay</div>	
	</div>
			
</div>

<br>