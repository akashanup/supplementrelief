<?php

// echo 'Begin content_asset_usages_query.php<br /><hr />';

// Get all Content Asset Usages for this Web Page including Key Thought, Terms, Videos, Challenges, Images, and Text Blocks (with Citations).
// Quiz is currently an attribute of Topic. Multiple Quizes per Topic can be done with a new Content Asset Type of "Quiz" and Content Asset Usages (Child) to the Topic or Web Page depending upon desired context.

$queryContentAssetUsages = 'SELECT 
	ca.content_asset_type_code, 
	ca.version,
	ca.title, 
	ca.summary_description, 
	ca.description, 
	ca.citations, 
	/* ca.text, */ 
	cau.content_asset_child_id, 		
	cau.seq, 
	cau.wellness_category_type_code  
	FROM content_asset_usages cau
	LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id
	WHERE cau.content_asset_parent_id = '.$web_page_id.'
	ORDER BY ca.content_asset_type_code, cau.seq';

// echo $queryContentAssetUsages . '<br /><hr />';

$result_content_asset_usage = mysqli_query($connection, $queryContentAssetUsages);

if (!$result_content_asset_usage) {
	show_mysqli_error_message($queryContentAssetUsages, $connection);
	die;
}

// Initialize content asset types variables to store output for display on Education page.

$key_thought_block = '';
$term_block = '';
$video_block = '';
$video_modal_block = '';
$challenge_block = '';
$image_block = '';
$text_block = '';
$citations_block = '';

// Initialize counter for Web Page Image, Video and Challenge (Container) processing.

$termCounter = 0;
$imageCounter = 1; // Can have 1, 2 or 3 images per Topic. If > 1, images should be same x,y dimensions.
$videoCounter = 1; // Can have any number of videos.
$totalVideos = 0;
// $challengeCounter = 1; // Currently only 1 Challenge per Topic.

// Build content asset type ($r = content_asset_usage)

while($r = mysqli_fetch_assoc($result_content_asset_usage)) {

	// show_array($r);

	switch ($r['content_asset_type_code']) {
		
		case 'KEYTH':

        // $key_thought_block .= '<blockquote>'.$r['description'].'</blockquote><br>';
        $key_thought_block .= '<em>'.$r['description'].'</em>';

        break;
	        
		// End case 'KEYTH':	  

	    case 'TERM':
	    
	    	if ($termCounter == 0) {
	    	// Setup Term Block Header
	    		$term_block .= '
	  			<dl class="accordion" data-accordion>
	  				<dd class="accordion-navigation">	  	
	  			  		<a href="#terms">
		  			  	  	<div class="row">
							   	<div class="small-9 columns">
							     	<div><h4>Terms</h4></div>
							   	</div>  
							   	<div class="small-3 columns">
							     	<div class="accordionIcon"><i class="fa fa-plus right"></i></div>
							   	</div>
							</div>
					    </a>
	  			    <div id="terms" class="content active" >
	  					<dl class="term">';
	    	}
	    		    
	      // $term_block .= '<h5>'.$r['title'].'</h5>'.$r['text'].'<br>';
	      $term_block .= '<div class="termDefinition"><dt>'.$r['title'].'</dt><dd>'.$r['description'].'</dd></div>';
	      
	      $termCounter++;
	              	        
	      break;
	    
	    // End case 'TERM':	          
	        
		case 'VIDEO':
		
			// Optionally limit the number of videos
			$max_number_videos = 10;
			while($totalVideoCount < $max_number_videos) {
				
				// only 1 video allowed until I redo this section with AJAX
					
				$content_asset_child_id = $r['content_asset_child_id'];
	
				$queryVideos = 'SELECT 
					vi.host_url_mp4, 
					vi.host_url_webm, 
					vi.image_id,  
					/* vi.poster_image_url, */
					vi.aspect_ratio, 
					vi.width_pixels, 
					vi.height_pixels, 
					vi.duration_minutes_seconds,  
					vi.embed_code, 
					im.host_url AS video_poster_image_url   
					FROM videos vi
					LEFT JOIN images im ON 
						(vi.image_id = im.content_asset_id 
						 AND im.format = "JPG" 
						 AND im.size = "Video" 
						 AND im.usage_shape = "Landscape") 
					WHERE vi.content_asset_id = '.$content_asset_child_id.' 
					LIMIT 1';
				
				// echo $queryVideos . '<br /><hr />';
				
				$result_video = mysqli_query($connection, $queryVideos);
							
				if (!$result_video) {
					show_mysqli_error_message($queryVideos, $connection);
					die;			
				}
								
				while($v = mysqli_fetch_assoc($result_video)) {
				
					// show_array($v);
					
					$totalVideoCount++;						
					$video_title = 	$r['title'];					
					$host_video_content_asset_id = $content_asset_child_id;
					$host_url_mp4 = $v['host_url_mp4'];	
					$host_url_webm = $v['host_url_webm'];	
					// $host_url_ogg = $v['host_url_ogg'];
					$poster_image_id = $v['image_id'];
					$video_poster_image_url = $v['video_poster_image_url'];
					// $thumbnail_image_url = $v['thumbnail_image_url'];		
					// $poster_image_url = $v['poster_image_url'];	
					$width_pixels = $v['width_pixels'];	
					$height_pixels = $v['height_pixels'];	
					$duration_minutes_seconds = $v['duration_minutes_seconds'];	
					$embed_code = $v['embed_code'];
										
					if (isset($host_url_mp4)) {
						
						// echo 'Inside of launch_topic_video.php<br><hr>';
				
						// $_SESSION['topic_slick_video'] = "Y";
						// $_SESSION['host_video_content_asset_id'] = $host_video_content_asset_id;
						// $_SESSION['host_url_mp4'] = $host_url_mp4;
						// $_SESSION['host_url_webm'] = $host_url_webm;
						// $_SESSION['host_url_ogg'] = $host1_url_ogg; // ogg no longer needed for major browser support
				
						/*
						if (!empty($poster_image_url)) {
							$poster_image_url = $poster_image_url;		
						} else {
							$poster_image_url = 'http://wrightcheck.com/media/images/video-poster-master.jpg';
						}
						*/
						
						if (!empty($video_poster_image_url)) {
							// $poster_image_url = $video_poster_image_url;
							$poster_image_url = $_SESSION['application']['root_media_host_url'].$video_poster_image_url;
							/*
							$video_block .=  
						   '<video controls style="width:100%;height:auto;" poster="'.$poster_image_url.'"> 
					    		<source src="'.$_SESSION['host_url_mp4'].'" type="video/mp4">
					    		<source src="'.$_SESSION['host_url_webm'].'" type="video/webm">
					    	 	Your browser does not support the video tag.
					  		</video>    
					  		<div class="videoCaption">
								<i class="fa fa-play-circle fa-1x"></i>&nbsp;&nbsp;'.$video_title.'&nbsp;&nbsp;<i class="fa fa-clock-o fa-1x"></i>&nbsp;&nbsp;'.$duration_minutes_seconds.'	
							</div>
							<br>';
							*/
							
						} else {
							// $poster_image_url = 'https://cdn-manager.net/media/images/video-poster-master.jpg';
							$poster_image_url = 'https://placehold.it/640x360';
							/*
							$video_block .=  
						   '<video controls style="width:100%;height:auto;"> 
					    		<source src="'.$_SESSION['host_url_mp4'].'" type="video/mp4">
					    		<source src="'.$_SESSION['host_url_webm'].'" type="video/webm">
					    	 	Your browser does not support the video tag.
					  		</video>    
					  		<div class="videoCaption">
								<i class="fa fa-play-circle fa-1x"></i>&nbsp;&nbsp;'.$video_title.'&nbsp;&nbsp;<i class="fa fa-clock-o fa-1x"></i>&nbsp;&nbsp;'.$duration_minutes_seconds.'	
							</div>
							<br>';
							*/
						}
						
						$video_block .=  
					   '<video controls style="width:100%;height:auto;" poster="'.$poster_image_url.'"> 
				    		<source src="'.$_SESSION['application']['root_media_host_url'].$host_url_mp4.'" type="video/mp4">
				    		<source src="'.$_SESSION['application']['root_media_host_url'].$host_url_webm.'" type="video/webm">
				    	 	Your browser does not support the video tag.
				  		</video>    
				  		<div class="videoCaption">
							<i class="fa fa-play-circle fa-1x"></i>&nbsp;&nbsp;'.$video_title.'&nbsp;&nbsp;<i class="fa fa-clock-o fa-1x"></i>&nbsp;&nbsp;'.$duration_minutes_seconds.'	
						</div>
						<br>';
						
						/*
						$video_block .=  
					   '<video controls style="width:100%;height:auto;" poster="'.$_SESSION['application']['root_media_host_url'].$poster_image_url.'"> 
				    		<source src="'.$_SESSION['application']['root_media_host_url'].$_SESSION['host_url_mp4'].'" type="video/mp4">
				    		<source src="'.$_SESSION['application']['root_media_host_url'].$_SESSION['host_url_webm'].'" type="video/webm">
				    	 	Your browser does not support the video tag.
				  		</video>    
				  		<div class="videoCaption">
							<i class="fa fa-play-circle fa-1x"></i>&nbsp;&nbsp;'.$video_title.'&nbsp;&nbsp;<i class="fa fa-clock-o fa-1x"></i>&nbsp;&nbsp;'.$duration_minutes_seconds.'	
						</div>
						<br>';
						*/
											
						// $dataRevealID = 'video'.$videoCounter;
						
						if ($totalVideoCount == 1) {
							// $video_block .= '<div class="slider autoplay">';
							$video_block .= '<div>';
						}
		
						/*
						$video_block .=  
					   '<video controls style="width:100%;height:auto;" poster="'.$poster_image_url.'"> 
				    		<source src="'.$_SESSION['host_url_mp4'].'" type="video/mp4">
				    		<source src="'.$_SESSION['host_url_webm'].'" type="video/webm">
				    	 	Your browser does not support the video tag.
				  		</video>    
				  		<div class="videoCaption">
							<i class="fa fa-play-circle fa-1x"></i>&nbsp;&nbsp;'.$video_title.'&nbsp;&nbsp;<i class="fa fa-clock-o fa-1x"></i>&nbsp;&nbsp;'.$duration_minutes_seconds.'	
						</div>';
						*/
						
						/*	    
						$video_block .=  
							   '<div data-tooltip class="has-tip tip-bottom radius" data-options="disable_for_touch:true" title="Click to play video" data-reveal-id="'.$dataRevealID.'">
							   		<div id="videoPosterFrame">
							   			<a href="#"><img src="'.$poster_image_url.'" style="width:100%;" ></a>
							   		</div>	
							    	<div class="videoCaption">
							    			<i class="fa fa-play-circle fa-1x"></i>&nbsp;&nbsp;'.$video_title.'&nbsp;&nbsp;<i class="fa fa-clock-o fa-1x"></i>&nbsp;&nbsp;'.$duration_minutes_seconds.'	
							    	</div>
							    </div>';			  			  
						*/
						
						/*						 
						$video_modal_block .=   
						 '<div id="'.$dataRevealID.'" class="reveal-modal large" data-reveal>
					  		<h3>'.$video_title.' <small><i class="fa fa-clock-o fa-1x"></i>&nbsp;'.$duration_minutes_seconds.'</small></h3>
					  		<!--<p class="lead">SupplementRelief Video Series<br /><i class="fa fa-clock-o fa-1x"></i>&nbsp;&nbsp;'.$duration_minutes_seconds.'</p>-->
					  		<video controls style="width:100%;height:auto;" poster="'.$poster_image_url.'"> 
					    		<source src="'.$_SESSION['host_url_mp4'].'" type="video/mp4">
					    		<source src="'.$_SESSION['host_url_webm'].'" type="video/webm">
					    	 	Your browser does not support the video tag.
					  		</video>    
					  		<a class="close-reveal-modal">&#215;</a>
					 	 </div>';					 
						*/
							 
						 $video_title = 	'';			
						 $host_video_content_asset_id = '';
						 $host_url_mp4 = '';	
						 $host_url_webm = '';	
						 $host_url_ogg = '';	
						 $poster_image_url = '';
						 $width_pixels = '';
						 $height_pixels = '';	
						 $duration_minutes_seconds = '';	
						 $embed_code = '';
							
					} // End (isset($host1_url_mp4)) {
					
					$videoCounter++;		
																		
				} // End while($v = mysqli_fetch_assoc($result_video)) {
											
				mysqli_free_result($result_video);
							
				break;
				
		    } // END while($totalVideoCount < 2) {

		    
	    // End case 'VIDEO':	        
	        	        
	    case 'CHALL':
	    			
			$content_asset_parent_id = $r['content_asset_child_id'];

			$queryChallenge	 = 'SELECT 
				ca.content_asset_type_code, 
				ca.complexity_level_type_code, 
				ca.version,
				ca.title, 
				ca.summary_description, 
				ca.description, 
				ca.citations, 
				ca.text, 	 
				cau.content_asset_child_id, 		
				cau.seq, 
				cau.wellness_category_type_code 
				FROM content_asset_usages cau
				LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id
				WHERE cau.content_asset_parent_id = '.$content_asset_parent_id.'
				ORDER BY ca.content_asset_type_code, cau.seq';
			
			// echo $queryChallenge . '<br /><hr />';
			
			$result_challenge = mysqli_query($connection, $queryChallenge);
						
			if (!$result_challenge) {
				show_mysqli_error_message($queryChallenge, $connection);
				die;
			}
			
			// echo 'Queried Web Page Challenges successfully.<br /><hr />';	
					
			while($c = mysqli_fetch_assoc($result_challenge)) {	
								
				if (($c['content_asset_type_code'])=="TEXT") {
					// Currently only allowing TEXT content asset types. Others can be accomodated.
					$challenge_block .= '<h3>'.$c['title'].'</h3>'.$c['description'];							
				}						
			}					
			
			mysqli_free_result($result_challenge);
							
		    break;
		    
		// End case 'CHALL':	
		
		case 'IMAGE':
			
			$content_asset_child_id = $r['content_asset_child_id'];

			// Need to address Image Configuration to determine which Configuration to use.
			$queryImage	 = 'SELECT
				seq,  
				host_url, 
				caption, 
				alt_text, 
				format, 
				size, 
				horizontal_pixel_size, 	
				vertical_pixel_size, 
				resolution_dpi, 
				file_size_kb 
				FROM images 
				WHERE content_asset_id = '.$content_asset_child_id.' 
				AND size = "Education"  
				LIMIT 1';
			
			// echo $queryImage . '<br /><hr />';
			
			$result_image = mysqli_query($connection, $queryImage);
						
			if (!$result_image) {
				show_mysqli_error_message($queryImage, $connection);
				die;
			}
			
			// echo 'Queried Topic Images successfully.<br /><hr />';	
					
			if ($imageCounter==1) {
				$totalImageCount = $imageCounter;	
				while($i = mysqli_fetch_assoc($result_image)) {
					// echo 'Inside of Image 1 Query Result Array.<br /><hr />';				
					$image1URL = $_SESSION['application']['root_media_host_url'].$i['host_url'];	 // Update this to host_url_jpg.
					$image1Caption = $i['caption'];	
					$image1AltText = $i['alt_text'];											
				}
						
				// echo $image1Caption . ' ' . $image1URL . '<br /><hr />';
				
			}	elseif ($imageCounter==2) {
					$totalImageCount = $imageCounter;
					while($i = mysqli_fetch_assoc($result_image)) {	
						// echo 'Inside of Image 2 Query Result Array.<br /><hr />';			
						$image2URL = $_SESSION['application']['root_media_host_url'].$i['host_url'];	 // Update this to host_url_jpg.
						$image2Caption = $i['caption'];	
						$image2AltText = $i['alt_text'];													
					}

					// echo $image2Caption . ' ' . $image2URL . '<br /><hr />';					
				
			}	elseif ($imageCounter==3) {
					$totalImageCount = $imageCounter;
					while($i = mysqli_fetch_assoc($result_image)) {
						// echo 'Inside of Image 3 Query Result Array.<br /><hr/ >';			
						$image3URL = $_SESSION['application']['root_media_host_url'].$i['host_url'];	 // Update this to host_url_jpg.
						$image3Caption = $i['caption'];
						$image3AltText = $i['alt_text'];														
					}

					// echo $image3Caption . ' ' . $image3URL . '<br /><hr />';			
			}
			
			mysqli_free_result($result_image);
							
			$imageCounter++;

		    break;
		    
		// End case 'IMAGE':	          	    
		          	    	
		case 'TEXT':
		    $text_block .= '<h2>'.$r['title'].'</h2>'.$r['description'];
		    // $text_block .= '<h2>'.$r['title'].'</h2>'.cleanEncoding($r['description']);
		    if (!empty($r['citations'])) $citations_block .= $r['citations'];	        
		    break;
		    
		// End case 'TEXT':	          
	        	        
	} // End switch ($r['content_asset_type_code']) {
		
} // End while($r = mysqli_fetch_assoc($result_content_asset_usage)) {

if (!empty($term_block)) {
	$term_block .= '</dl></div></dd></dl>';
}

if (!empty($video_block)) {
	$video_block .= '</div>';
}

// echo '<font color="red">Key Thoughts: </font>'.$key_thought_block;
// echo '<font color="red">Terms: </font>'.$term_block;
// echo '<font color="red">Challenges: </font>'.$challenge_block;
// echo '<font color="red">Videos: </font>'.$video_block;
// echo '<font color="red">Texts: </font>'.$text_block;
// echo '<font color="red">Citations: </font>'.$citations_block;

?>
