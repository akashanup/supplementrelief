<?php

// echo 'Begin course_outline_query.php.<br /><hr />';

// Script can be called from:
// 1. User Dashboard page
// 2. Education page
// 3. Tools Menu anytime after Login

// When calling page to User Dashboard is select-program-enrollment/index.php
// https://supplementrelief.com/education/?c_id=1772&t_id=1773&wp_id=1776

/* if (isset($_GET['course_id'])) { $course_id = $_GET['course_id']; }  */
 
// Use the Course ID set from the User Select Program Enrollment to determine which Web Pages to show in the Course Outline.
// $course_id = $_SESSION['enrollment']['course_id'];
 
// determine active Web Page to style link
if (isset($_GET['wp_id'])) {
	$active_web_page = $_GET['wp_id'];	
} else { 
	
 	if (strlen($_SESSION['enrollment']['web_page_id']) > 0) {	
 		$active_web_page = $_SESSION['enrollment']['web_page_id'];		
 	} else { 	
 		$active_web_page = '';  			
 	}
} 	

// Get the Course Title for the chosen Course ID.
$queryCourseTitle  = 'SELECT title FROM content_assets WHERE id = '.$course_id.'  LIMIT 1';

// echo $queryCourseTitle . '<br /><hr />';

$result_content_asset_course_title = mysqli_query($connection, $queryCourseTitle);

if (!$result_content_asset_course_title) {
	show_mysqli_error_message($queryCourseTitle, $connection);
	die;
}

// echo 'Queried Course Title successfully.<br /><hr />';

$course_title = '';

while($c = mysqli_fetch_assoc($result_content_asset_course_title)) {
	$course_title = cleanEncoding($c['title']);
	// echo 'Course Title: '.$course_title.'<br />';
}

mysqli_free_result($result_content_asset_course_title);
	               
// Get the Topics for the Course using the Course ID.

// show_array($_SESSION);

if (strlen($_SESSION['enrollment']['use_topic_scheduling'] > 0) ) {
	
	// echo 'use topic scheduling';
	
	// Limit Topics based upon scheduled timestamps
	$queryCourseTopics = 'SELECT
	ca.content_asset_type_code, 
	ca.complexity_level_type_code, 
	ca.title, 
	ca.text, 
	ca.citations, 
	cau.content_asset_child_id, 
	cau.seq 
	FROM content_asset_usages cau 
	JOIN project_program_content_assets ppca ON 
	(cau.content_asset_child_id = ppca.content_asset_id AND 
	 ppca.project_program_id = '.$_SESSION['enrollment']['project_program_id'].' AND 
	 ppca.scheduled_delivery_timestamp <= DATE_SUB(NOW(), INTERVAL 4 HOUR) AND
	 (ppca.scheduled_delivery_complete_timestamp IS NULL OR ppca.scheduled_delivery_complete_timestamp >= DATE_SUB(NOW(), INTERVAL 4 HOUR ) )) 
	LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id 
	WHERE cau.content_asset_parent_id = '.$course_id.' 
	AND ca.content_asset_type_code = "TOPIC" 
	AND ppca.is_active = 1 
	ORDER BY ca.content_asset_type_code, cau.seq';  

	// DATE_SUB(NOW(), INTERVAL 4 HOUR)) converts GMT to America/Detroit timezone.
	
} else {
	
	// echo 'do not use topic scheduling';
	
	// show all Topics
	
	$queryCourseTopics = 'SELECT
	ca.content_asset_type_code, 
	ca.complexity_level_type_code, 
	ca.title, 
	ca.text, 
	ca.citations, 
	cau.content_asset_child_id, 
	cau.seq 
	FROM content_asset_usages cau
	LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id 
	WHERE cau.content_asset_parent_id = '.$course_id.' 
	AND ca.content_asset_type_code = "TOPIC" 
	ORDER BY ca.content_asset_type_code, cau.seq';
	
}

// echo $queryCourseTopics . '<br /><hr />';
	        
$result_content_asset_usage_topic = mysqli_query($connection, $queryCourseTopics);
	        
if (!$result_content_asset_usage_topic) {
	show_mysqli_error_message($queryCourseTopics, $connection);
	die;
}
     
// echo 'Queried Course Topics successfully.<br /><hr />';

$course_topic_block = '';

$first_topic_id = '';
$topic_counter = 1;
$first_web_page_id = '';
$web_page_counter = 1;

// show_array($_SESSION);

while($t = mysqli_fetch_assoc($result_content_asset_usage_topic)) { 
	
	// show_array($t);

	// Have Topic ID now must query for all Web Page Containers for Topic. Limit 1 for now.
	
	$topic_id = $t['content_asset_child_id'];
		
	if ($topic_counter == 1) {
		$first_topic_id = $t['content_asset_child_id'];
	}
	
	// $topic_counter ++;
		
	// $topic_title = $t['title']; // Not using Topic Title anywhere currently.         	
	
	$queryTopicWebPages	= 'SELECT 
		ca.content_asset_type_code, 
		ca.complexity_level_type_code, 
		ca.title, 
		ca.text, 
		ca.citations,  
		cau.content_asset_child_id, 
		cau.wellness_category_type_code, 
		cau.seq, 
		wct.name AS wellness_category_type_name 
		FROM content_asset_usages cau
		LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id
		LEFT JOIN wellness_category_types wct ON cau.wellness_category_type_code = wct.code 
		WHERE cau.content_asset_parent_id = '.$topic_id.' 
		AND ca.content_asset_type_code = "WEBPG"  
		ORDER BY ca.content_asset_type_code, cau.seq 
		LIMIT 1';
				
	// echo $queryTopicWebPages . '<br /><hr />';
								
	$result_content_asset_usage_web_page = mysqli_query($connection, $queryTopicWebPages);
		        	
	if (!$result_content_asset_usage_web_page) {
		echo $queryTopicWebPages . '<br /><hr />';
		die("Database Web Page for Topic query failed.");
	}
	
	// echo 'Queried Web Page for Topic successfully.<br /><hr />';
	
	while($w = mysqli_fetch_assoc($result_content_asset_usage_web_page)) {
	
	// $web_page_id = $w['content_asset_child_id'];

		if ($topic_counter == 1) {
			$course_topic_block = '<ul class="courseNavigation">'; /* <ul class="side-nav"> for Foundation styling */
			if ($web_page_counter == 1) {
				$first_web_page_id = $w['content_asset_child_id'];
				if ($active_web_page == '') {
					$active_web_page = $first_web_page_id;		
				}
			}
		}

		$web_page_counter ++;
		
		// $pillar = $w['wellness_category_type_name'];	
		   	       	
		if ($w['content_asset_child_id'] == $active_web_page) {
			$course_topic_block .= '<li class="active"><a href="../education/?c_id='.$course_id.'&t_id='.$topic_id.'&wp_id='.$w['content_asset_child_id'].'">'.cleanEncoding($w['title']).'</a></li>'; 	
					} else {
			$course_topic_block .= '<li><a href="../education/?c_id='.$course_id.'&t_id='.$topic_id.'&wp_id='.$w['content_asset_child_id'].'">'.cleanEncoding($w['title']).'</a></li>'; 	
		}
		
	} // End Web Page for Topic loop.
	
	$topic_counter ++;

} // End Topic for Course loop.

if (!empty($course_topic_block)) {
	$course_topic_block .= '</ul>';
}

mysqli_free_result($result_content_asset_usage_topic);
mysqli_free_result($result_content_asset_usage_web_page);

// echo $course_topic_block;
	        
?>