<?php

// This script is called from ../customer-order-lookup/.

?>
								
<div class="row">
	<div class="small-12 columns">

		<form data-abide action="../customer-order-status/" method="post">
			
			<input type="hidden" name="cp" value="<?php echo $_SESSION["token"]; ?>">
		
			<fieldset>
		  
		    <legend>Customer Order Lookup</legend>
		    
		    <div class="row">
		      <div class="small-12 columns">
		      	<p>Please provide your <strong>Order Number</strong> and <strong>Billing Email Address</strong> then press the <b>LOOKUP ORDER</b> button.</p>
		      </div>
		    </div>
		    		
			<div class="row">			
			  <div class="small-12 medium-6 columns">
			  
			    <label>Order Number
			      <input type="text" required name="order_number" placeholder="SR-XXXX" value="<?php echo $order_number; ?>" />
			    </label>
			    <small class="error">Order Number is required</small>    
			  </div>
			  
			  <div class="small-12 medium-6 columns">
			    <label>Billing Email Address
			      <input type="email" required name="billing_email_address" placeholder="someone@domain.com" />
			    </label>
			    <small class="error">Billing Email Address is required</small>	    
			  </div>			  
			</div>
			
			<div class="row">			
			  <!--<div class="small-12 medium-6 medium-push-6 columns">-->
			  <div class="small-12 columns">
			    <label>	      
			      <input class="button large radius show-for-medium-up expand" type="submit" value="LOOKUP ORDER" />
			      <input class="button large radius expand show-for-small-only" type="submit" value="LOOKUP ORDER" />      
			    </label>
			  </div> 
			  
			</div>
											
		  </fieldset>
		  		
		</form>
	</div>
</div>