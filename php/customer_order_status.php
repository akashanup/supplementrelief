<?php 

// echo 'customer_order_status.php<br />';
// die;

// include_once('../includes/header.php');	

// https://supplementrelief.com/php/customer_order_status.php

// show_array($_POST);
// die;

if (isset($_POST['order_number'])) {
	$order_number = sanitize($_POST['order_number']);
} else {
	echo 'Invalid request.';
	die;
}

if (isset($_POST['billing_email_address']))
{
	$billing_email_address = sanitize($_POST['billing_email_address']);
} else {	
	echo 'Invalid request.';
	die;
}

// Query the Order
$queryOrder = 'SELECT 
	ord.id,
	ord.order_status, 
	ord.created_timestamp AS order_timestamp, 
	ord.order_project_id, 
	pj.name AS project_name,  
	ord.invoice_num AS invoice_number, 
	ord.shipped_by, 
	ord.shipped_date, 
	ord.shipped_carrier, 
	ord.shipped_carrier_waybill, 
	ord.notes_displayed, 
	ord.notes_internal, 
	ord.modified_by, 
	ord.modified_timestamp, 	
	ord.transaction_id, 
	ord.authorization_code, 
	ord.response_reason_text, 
	ord.user_name, 
	ord.card_type,
	ord.exp_date, 
	ord.cc_mask,  
	ord.authorization_code, 
	ord.shipping_option_type_name, 
	ord.shipping_description, 
	ord.comments,  
	ord.product_subtotal_amount, 
	ord.discount_amount, 
	ord.shipping_total, 
	ord.total_tax, 
	ord.total_amount, 
	ord.email AS billing_email, 
	ord.phone AS billing_phone, 
	ord.first_name AS billing_first_name, 
	ord.last_name AS billing_last_name, 
	ord.shipping_option_type_name,
	ord.address AS billing_address, 
	ord.city AS billing_city, 
	ord.state AS billing_state, 
	ord.zip_code AS billing_postal_code, 
	ord.country AS billing_country, 
	ord.shipping_description, 
	ord.shipping_email, 
	ord.shipping_phone, 
	ord.shipping_first_name, 
	ord.shipping_last_name, 
	ord.shipping_address, 
	ord.shipping_city, 
	ord.shipping_state, 
	ord.shipping_zip_code AS shipping_postal_code, 
	ord.shipping_country 
	FROM orders ord 
	LEFT JOIN projects pj ON ord.order_project_id = pj.id 	
	WHERE ord.invoice_num = "'.$order_number.'"  
	AND ord.email = "'.$billing_email_address.'"';
			
// echo $queryOrder;
// die();

$result_order = mysqli_query($connection, $queryOrder);

if (!$result_order) {
	show_mysqli_error_message($queryOrder, $connection);
	die;
}

$rowcount = mysqli_num_rows($result_order);

// Order not found
if ($rowcount == 0) {
	$_SESSION['message_type'] = 'alert-box warning radius';		
	$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-2x"></i>&nbsp;&nbsp;An <b>Order</b> was not found using the <b>Order Number</b> and <b>Billing Email Address</b> provided. Please <b>check your email order receipt</b> and verify the information was entered correctly.</p><p>We recently redesigned our website. <b>If you placed an order before August 24, 2015</b>, then you will not be able to look it up using this page but you can contact us by email and provide your order number and name and we will reply with your order status.<p>Please <b><a href="../contact" style="color:white;text-decoration:underline;">contact us</a></b> by email if we can help.</p>';
	header("location: ../customer-order-lookup/");
	exit();	
}

while($r = mysqli_fetch_assoc($result_order)) {

	// show_array($r);
	
	$order_id = $r['id'];
	$order_timestamp = no_null_timestamp_check($r['order_timestamp']);
	$order_status = $r['order_status'];

	$project_name = $r['project_name'];
	$invoice_number = $r['invoice_number'];
	
	$user_name = $r['user_name'];
	$card_type = $r['card_type'];
	$exp_date = $r['exp_date'];
	$cc_mask = $r['cc_mask'];
	
	$transaction_id = $r['transaction_id'];
	$authorization_code = $r['authorization_code'];
	$response_reason_text = $r['response_reason_text'];
	
	$shipping_option_type_name = $r['shipping_option_type_name'];
	$shipping_description = $r['shipping_description'];
	
	$shipped_by = $r['shipped_by'];
	$shipped_date = hdate($r['shipped_date']);
	$shipped_carrier = $r['shipped_carrier'];
	$shipped_carrier_waybill = $r['shipped_carrier_waybill'];

	$comments = $r['comments'];
	$notes_displayed = $r['notes_displayed'];
	$notes_internal = $r['notes_internal'];
	
	$product_subtotal_amount = $r['product_subtotal_amount'];
	$discount_amount = $r['discount_amount'];
	$shipping_amount = $r['shipping_total'];
	$tax_amount = $r['total_tax'];
	$total_amount = $r['total_amount'];
		
	$billing_email = $r['billing_email'];	
	$billing_phone = $r['billing_phone'];	
	$billing_first_name = $r['billing_first_name'];	
	$billing_last_name = $r['billing_last_name'];	
	$billing_address = $r['billing_address'];	
	$billing_city = $r['billing_city'];	
	$billing_state = $r['billing_state'];	
	$billing_postal_code = $r['billing_postal_code'];
	$billing_country = $r['billing_country'];		
		
	$shipping_option_type_name = $r['shipping_option_type_name'];	
	$shipping_description = $r['shipping_description'];
	
	$shipping_email = $r['shipping_email'];	
	$shipping_phone = $r['shipping_phone'];	
	$shipping_first_name = $r['shipping_first_name'];	
	$shipping_last_name = $r['shipping_last_name'];	
	$shipping_address = $r['shipping_address'];	
	$shipping_city = $r['shipping_city'];	
	$shipping_state = $r['shipping_state'];	
	$shipping_postal_code = $r['shipping_postal_code'];
	$shipping_country = $r['shipping_country'];					

	$card_type = $r['card_type'];	
	$authorization_code = $r['authorization_code'];	

	$product_subtotal_amount = $r['product_subtotal_amount'];	
	$discount_amount = $r['discount_amount'];	
	$shipping_total = $r['shipping_total'];	
	$total_tax = $r['total_tax'];	
	$total_amount = $r['total_amount'];	
	$email = $r['email'];	
	
}

switch ($order_status) {
	case '1':
	    $display_order_status = 'New';
	    break;	    
	case '2':
	    $display_order_status = 'In Progress';
	    break;
	case '3':
	    $display_order_status = 'Backordered';
	    break;
	case '4':
	    $display_order_status = 'Complete';
	    break;
	case '5':
	    $display_order_status = 'Archived';
	    break;
	case '6':
	    $display_order_status = 'Cancelled';
	    break;
	case '7':
	    $display_order_status = 'Refunded';
	    break;	    
	default:
       $display_order_status = '';	        
}

switch ($shipped_by) {
	case NULL:
	    $display_shipped_by = '';
	    break;
	case '1':
	    $display_shipped_by = 'NuMedica';
	    break;	    
	case '2':
	    $display_shipped_by = 'SupplementRelief';
	    break;
	case '3':
	    $display_shipped_by = 'Wright Check';
	    break;
	case '4':
	    $display_shipped_by = 'Other';
	    break;
	default:
       $display_shipped_by = 'Not Shipped';	        
}

switch ($shipped_carrier) {
	case NULL:
	    $display_shipped_carrier = '';
	    break;
	case '1':
	    $display_shipped_carrier = 'FedEx';
	    break;	    
	case '2':
	    $display_shipped_carrier = 'UPS';
	    break;
	case '3':
	    $display_shipped_carrier = 'USPS';
	    break;
	case '4':
	    $display_shipped_carrier = 'Other';
	    break;
	default:
       $display_shipped_carrier = '';	        
}

?>

<br />
	
<div class="row">
	<div class="small-12 columns">

		<form id="customer-order-status" data-abide method="post" action="" autocomplete="off" >
		
			<!-- <input type="hidden" id="edit-order-action" name="action" value="edit" /> -->
													
			<fieldset>		  
	    	<legend>Order Status</legend>
	    	
	    	<div class="row">
		      <div class="small-12 columns">
		      	<p>Please review your <b>Order Information</b> below. If you have any questions, please call <b><?php echo $_SESSION['application']['phone']; ?></b> or <a href="../contact/">contact us</a> via email.</p>
		      </div>
		    </div>

	    		    		    	
	    	<div class="row">
	    	
	    		<!--
	    		<div class="small-12 medium-2 columns">
	    			<label>Order ID
	    				<input type="text" name="order_id" value="<?php echo ($order_id ? $order_id : ''); ?>" readonly />
	    			</label>
	    		</div>
	    		-->
	    		
	    		<div class="small-12 medium-3 columns">
	    			<label>Order Number
	    				<input type="text" name="order_number" value="<?php echo ($invoice_number ? $invoice_number : ''); ?>" readonly />
	    			</label>
	    		</div>
	    		
	    		<div class="small-12 medium-3 columns">
	    			<label>Order Date
	    				<input type="text" name="order_timestamp" value="<?php echo ($order_timestamp ? $order_timestamp : ''); ?>" readonly />
	    			</label>
	    		</div>
	    		
	    		<div class="small-12 medium-3 columns">
	    			<label>Member Username
	    				<input type="text" name="user_name" value="<?php echo ($user_name ? $user_name : ''); ?>" readonly />
	    			</label>
	    		</div>
	    		
	    		<div class="small-12 medium-3 columns">
	    			<label>Project
	    				<input type="text" name="project_name" value="<?php echo ($project_name ? $project_name : ''); ?>" readonly />
	    			</label>
	    		</div>
	    		
   			    			    		
	    	</div>
	    	
	    	<fieldset>		  
				<legend>Status</legend>
	    	
	    		<div class="row">
		    	
		    		<!--
		    		<div class="small-12 medium-4 columns">
			    		<label>Order Status 
		    		    <select id="order_status"  disabled name="order_status">
		    		    	<option <?php if (isset($order_status) && $order_status == NULL): ?>selected="selected"<?php endif ?> value="">Select a Status...</option>
		    		    	<option <?php if (isset($order_status) && $order_status == '1'): ?>selected="selected"<?php endif ?> value="1">New</option>
		    		    	<option <?php if (isset($order_status) && $order_status == '2'): ?>selected="selected"<?php endif ?> value="2">In Progress</option>
		    		    	<option <?php if (isset($order_status) && $order_status == '3'): ?>selected="selected"<?php endif ?> value="3">Backordered</option>	    	
							<option <?php if (isset($order_status) && $order_status == '4'): ?>selected="selected"<?php endif ?> value="4">Complete</option>
							<option <?php if (isset($order_status) && $order_status == '5'): ?>selected="selected"<?php endif ?> value="5">Archived</option>
							<option <?php if (isset($order_status) && $order_status == '6'): ?>selected="selected"<?php endif ?> value="6">Cancelled</option>
							<option <?php if (isset($order_status) && $order_status == '7'): ?>selected="selected"<?php endif ?> value="7">Refunded</option>
		    		    </select>
			    		</label>
			    		<small class="error">Order Status is required</small>
			    	</div>
			    	-->
			    	
			    	<div class="small-12 medium-4 columns">
		    			<label>Order Status
		    				<input type="text" name="order_status" value="<?php echo ($display_order_status ? $display_order_status : ''); ?>" readonly />
		    			</label>
		    		</div>
			    	
			    	<div class="small-12 medium-4 columns">
		    			<label>Shipped Method
		    				<input type="text" name="shipping_option_type_name" value="<?php echo ($shipping_option_type_name ? $shipping_option_type_name : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<div class="small-12 medium-4 columns">
		    			<label>Shipped Method Description
		    				<input type="text" name="shipping_description" value="<?php echo ($shipping_description ? $shipping_description : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
	    		</div>
	    			    		
				<div class="row">
			    				    	
			    	<div class="small-12 medium-3 columns">
		    			<label>Shipped By
		    				<input type="text" name="shipped_by" value="<?php echo ($display_shipped_by ? $display_shipped_by : ''); ?>" readonly />
		    			</label>
		    		</div>
			    	
			    	<div class="small-12 medium-3 columns">
				    	<label>Shipped Date
				    	<input type="text" id="datepicker-effective-date"  readonly name="shipped_date" value="<?php echo ($shipped_date ? $shipped_date : ''); ?>" />
				    	</label>
				    	<small class="error">Shipped Date is optional and must be formatted as MM/DD/YYYY</small> 	  
			    	</div>		    	
		    				    	
			    	<div class="small-12 medium-3 columns">
		    			<label>Shipped Carrier
		    				<input type="text" name="shipped_carrier" value="<?php echo ($display_shipped_carrier ? $display_shipped_carrier : ''); ?>" readonly />
		    			</label>
		    		</div>
			    	
			    	<div class="small-12 medium-3 columns">
		    			<label>Carrier Tracking ID
		    				<input type="text" readonly name="shipped_carrier_waybill" value="<?php echo ($shipped_carrier_waybill ? $shipped_carrier_waybill : ''); ?>" />
		    			</label>
		    		</div> 	
			    	
		    	</div>
		    				    					    	
		    	<div class="row">
			    	<div class="small-12 columns">
		    	
				    	<label>Customer Comments
		    				<textarea readonly name="comments" rows="2" /><?php echo ($comments ? $comments : ''); ?></textarea>
		    			</label>
		    			
		    			<label>Customer Service Notes
		    				<textarea readonly name="notes_displayed" rows="6" /><?php echo ($notes_displayed ? $notes_displayed : ''); ?></textarea>
		    			</label>
		    			
		    			<!--
		    			<label>Notes <small>Internal</small>
		    				<textarea name="notes_internal" rows="5" placeholder="Notes are private and are NOT displayed to customer."/><?php echo ($notes_internal ? $notes_internal : ''); ?></textarea>
		    			</label>
		    			-->
		    			
		    		</div>
		    	</div>
		    			    	
	    	</fieldset>
	    	
	    	<fieldset>		  
				<legend>Amounts</legend>
	    	
		    	<div class="row">
			    	    	
		    		<div class="small-12 medium-3 columns">
		    			<label>Subtotal
		    				<input type="text" name="product_subtotal_amount" value="<?php echo '$'.($product_subtotal_amount ? $product_subtotal_amount : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<!--
		    		<div class="small-12 medium-2 columns">
		    			<label>Discount
		    				<input type="text" name="discount_amount" value="<?php echo ($discount_amount ? $discount_amount : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		-->
		    		
		    		<div class="small-12 medium-3 columns">
		    			<label>Tax
		    				<input type="text" name="tax_amount" value="<?php echo '$'.($tax_amount ? $tax_amount : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<div class="small-12 medium-3 columns">
		    			<label>Shipping
		    				<input type="text" name="shipping_amount" value="<?php echo '$'.($shipping_amount ? $shipping_amount : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<div class="small-12 medium-3 columns">
		    			<label>Total
		    				<input type="text" name="total_amount" value="<?php echo '$'.($total_amount ? $total_amount : ''); ?>" readonly />
		    			</label>
		    		</div>	    		
		    		
		    	</div>			    	
	    	</fieldset>
	    	
	    	<fieldset>
		    	<legend>Products</legend>
		    	
		    	<?php

				// Query the Order Items
				$queryOrderItems = 'SELECT 
					oi.id,
					oi.orders_id, 
					oi.brand_product_usage_id,  
					bpu.name AS brand_product_usage_name, 
					oi.order_item_options, 
					oi.quantity, 
					oi.unit_price, 
					oi.amount, 
					oi.created_timestamp 
					FROM order_items oi 
					LEFT JOIN brand_product_usages bpu ON oi.brand_product_usage_id = bpu.id 
					WHERE oi.orders_id = '.$order_id.' 
					ORDER BY bpu.name ASC';
							
				// echo $queryOrderItems;
				// die();
				
				$result_order_item = mysqli_query($connection, $queryOrderItems);
				
				if (!$result_order_item) {
					show_mysqli_error_message($queryOrderItems, $connection);
					die;
				}
				
				?>
				
				<div class="row"> 			
					<div class="small-12 columns">			
				
						<table width="100%">							
						
							<thead class="tableHeader">
								<tr>
									<th>Product</th>				  
									<th>Options</th>
									<th>Quantity</th>
									<th>Unit<br />Price</th> 
									<th>Amount</th>			    							  
							  </tr>	
							</thead>
					
							<?php										
							while($c = mysqli_fetch_assoc($result_order_item)) {
								
								$order_item_options_display = '';
							
								// show_array($i);
								
								$order_item_id = $c['id'];
								
								// Query the Order Item Options
								$queryOrderItemOptions = 'SELECT 
									oio.id,
									oio.order_option_name, 
									pot.name AS order_option_type_name   
									FROM order_item_options oio 
									LEFT JOIN product_option_types pot ON oio.order_option_type_code = pot.code 
									WHERE oio.order_item_id = '.$order_item_id.' 
									ORDER BY oio.id ASC';
											
								// echo $queryOrderItemOptions;
								// die();
								
								$result_order_item_option = mysqli_query($connection, $queryOrderItemOptions);
								
								if (!$result_order_item_option) {
									show_mysqli_error_message($queryOrderItemOptions, $connection);
									die;
								}
				
								while($o = mysqli_fetch_assoc($result_order_item_option)) {
									
									// show_array($o);
				
									$order_item_options_display .= $o['order_option_type_name'].': '.$o['order_option_name'].'<br />';
									
								}
								
								mysqli_free_result($result_order_item_option);	
								
								?>
								<tr>
																										
									<!-- <td><?php echo '<center><a href="../dashboard_order_detail/?order_id='.$c['id'].'" title="View Order Details" >View</a></center>'; ?></td>																
									<td class="show-for-medium-up"><?php echo $c['id']; ?></td>
																	
									<td><?php echo date('m/d/y h:i A T', strtotime($c['created_timestamp'])); ?></td> -->
																									
									<td><?php echo $c['brand_product_usage_name']; ?></td>
									
									<td><?php echo $order_item_options_display; ?></td>
																									
									<td><?php echo $c['quantity']; ?></td>	
									
									<td><?php echo '$'.$c['unit_price']; ?></td>	
									
									<td class="show-for-medium-up"><?php echo '$'.$c['amount']; ?></td>										
																																		
								</tr>
							
								<?php
									
							}
						
							mysqli_free_result($result_order_item);	
								
							?>
						
						</table>
					</div> 			
				</div>
				
	    	</fieldset>
 		    	
	    	<div class="row">
	    	
	    		<div class="small-12 medium-6 columns">
	    		
	    			<fieldset>		  
	    				<legend>Billing</legend>
	    				
	    				<div class="row">
	    				
	    					<div class="small-6 columns">				
	    						<label>Email Address
	    							<input type="email" readonly name="billing_email" required value="<?php echo ($billing_email ? $billing_email : ''); ?>" />
	    						</label>	
	    						<small class="error">Billing Email is required</small>	
	    					</div>
	    					
	    					<div class="small-6 columns">				
	    						<label>Phone
	    							<input type="tel" readonly name="billing_phone" required value="<?php echo ($billing_phone ? $billing_phone : ''); ?>" />
	    						</label>	
	    						<small class="error">Billing Phone is required</small>	
	    					</div>
	    					
	    				</div>
    				
	    				<div class="row">
	    				
	    					<div class="small-6 columns">				
	    						<label>First Name
	    							<input type="text" readonly name="billing_first_name" required value="<?php echo ($billing_first_name ? $billing_first_name : ''); ?>" />
	    						</label>	
	    						<small class="error">Billing First Name is required</small>	
	    					</div>
	    					
	    					<div class="small-6 columns">				
	    						<label>Last Name
	    							<input type="text" readonly name="billing_last_name" required value="<?php echo ($billing_last_name ? $billing_last_name : ''); ?>" />
	    						</label>	
	    						<small class="error">Billing Last Name is required</small>	
	    					</div>
	    					
	    				</div>
	    				
	    				<div class="row">
	    				
	    					<div class="small-12 columns">				   
		    				    				    		
				    			<label>Address
				    				<textarea readonly name="billing_address" required placeholder="Billing Address..." rows="3" /><?php echo ($billing_address ? $billing_address : ''); ?></textarea>
				    				<small class="error">Billing Address is re	quired</small>
				    			</label>
				    			
				    		</div>
				    		
				    	</div>
				    	
				    	<div class="row">
				    		
				    		<div class="small-12 columns">				   
			    				<label>City
			    					<input type="text" readonly name="billing_city" required value="<?php echo ($billing_city ? $billing_city : ''); ?>" />
			    				</label>	
			    				<small class="error">Billing City is required</small>
			    			</div>	    			
				    		
				    	</div>
				    	
				    	<div class="row">
				    	
				    		<div class="small-12 medium-4 columns">				   
				    			<label>State
				    				<input type="text" readonly name="billing_state" required value="<?php echo ($billing_state ? $billing_state : ''); ?>" />
				    			</label>	
				    			<small class="error">Billing State is re	quired</small>	
				    		</div>
				    		
				    		<div class="small-12 medium-4 columns">				   
				    			<label>Postal Code
				    				<input type="text" readonly name="billing_postal_code" required value="<?php echo ($billing_postal_code ? $billing_postal_code : ''); ?>" />
				    			</label>	
				    			<small class="error">Billing Postal Code is re	quired</small>	
				    		</div>
				    		
				    		<div class="small-12 medium-4 columns">				   
				    			<label>Country
				    				<input type="text" readonly name="billing_country" required value="<?php echo ($billing_country ? $billing_country : ''); ?>" />
				    			</label>	
				    			<small class="error">Billing Country is required</small>	
				    		</div>
		    		
				    	</div>	    			
				    			    					    			
		    		</fieldset>    			
	    			
	    		</div>
	    		
	    		<div class="small-12 medium-6 columns">
	    		
	    			<fieldset>		  
	    				<legend>Shipping</legend>
	    				
	    				<div class="row">
	    				
	    					<div class="small-6 columns">				
	    						<label>Email Address
	    							<input type="email" readonly name="shipping_email" required value="<?php echo ($shipping_email ? $shipping_email : ''); ?>" />
	    						</label>	
	    						<small class="error">Shipping Email is required</small>	
	    					</div>
	    					
	    					<div class="small-6 columns">				
	    						<label>Phone
	    							<input type="tel" readonly name="shipping_phone" required value="<?php echo ($shipping_phone ? $shipping_phone : ''); ?>" />
	    						</label>	
	    						<small class="error">Shipping Phone is required</small>	
	    					</div>
	    					
	    				</div>
	    				
	    				<div class="row">
	    				
	    					<div class="small-6 columns">				
	    						<label>First Name
	    							<input type="text" readonly name="shipping_first_name" required value="<?php echo ($shipping_first_name ? $shipping_first_name : ''); ?>" />
	    						</label>	
	    						<small class="error">Shipping First Name is required</small>	
	    					</div>
	    					
	    					<div class="small-6 columns">				
	    						<label>Last Name
	    							<input type="text" readonly name="shipping_last_name" required value="<?php echo ($shipping_last_name ? $shipping_last_name : ''); ?>" />
	    						</label>	
	    						<small class="error">Shipping Last Name is required</small>	
	    					</div>
	    					
	    				</div>

	    				    		
	    				<div class="row">
	    				
	    					<div class="small-12 columns">				   
		    				    				    		
				    			<label>Address
				    				<textarea readonly name="shipping_address" required placeholder="Shipping Address..." rows="3" /><?php echo ($shipping_address ? $shipping_address : ''); ?></textarea>
				    				<small class="error">Shipping Address is required</small>
				    			</label>
				    			
				    		</div>
				    		
				    	</div>
				    	
				    	<div class="row">
				    	
			    			<div class="small-12 columns">				   
			    				<label>City
			    					<input type="text" readonly name="shipping_city" required value="<?php echo ($shipping_city ? $shipping_city : ''); ?>" />
			    				</label>	
			    				<small class="error">Shipping City is required</small>
			    			</div>
			    			
				    	</div>	    	
				    	
				    	<div class="row">
				    					    	
				    		<div class="small-12 medium-4 columns">				   
				    			<label>State
				    				<input type="text" readonly name="shipping_state" required value="<?php echo ($shipping_state ? $shipping_state : ''); ?>" />
				    			</label>	
				    			<small class="error">Shipping State is required</small>	
				    		</div>
				    		
				    		<div class="small-12 medium-4 columns">				   
				    			<label>Postal Code
				    				<input type="text" readonly name="shipping_postal_code" required value="<?php echo ($shipping_postal_code ? $shipping_postal_code : ''); ?>" />
				    			</label>	
				    			<small class="error">Shipping Postal Code is required</small>	
				    		</div>
				    		
				    		<div class="small-12 medium-4 columns">				   
				    			<label>Country
				    				<input type="text" readonly name="shipping_country" required value="<?php echo ($shipping_country ? $shipping_country : ''); ?>" />
				    			</label>	
				    			<small class="error">Shipping Country is required</small>	
				    		</div>
				    		
				    	</div>	    			
		    				
	    			</fieldset>    			
	    			
	    		</div>
	    		
	    	</div>
	    	
	    	<fieldset>		  
				<legend>Credit Card</legend>
	    	
		    	<div class="row">
			    	    	
		    		<div class="small-12 medium-3 columns">
		    			<label>Card Type
		    				<input type="text" name="card_type" value="<?php echo ($card_type ? $card_type : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<div class="small-12 medium-3 columns">
		    			<label>Card Number
		    				<input type="text" name="cc_mask" value="<?php echo ($cc_mask ? $cc_mask : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<!-- 
			    	<div class="small-12 medium-4 columns">
		    			<label>Expiration Date
		    				<input type="text" name="exp_date" value="<?php echo ($exp_date ? $exp_date : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		--> 		    		
		    				    					    	    	
		    		<div class="small-12 medium-3 columns">
		    			<label>Transaction ID
		    				<input type="text" name="transaction_id" value="<?php echo ($transaction_id ? $transaction_id : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<div class="small-12 medium-3 columns">
		    			<label>Authorization Code
		    				<input type="text" name="authorization_code" value="<?php echo ($authorization_code ? $authorization_code : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<!--
		    		<div class="small-12 medium-6 columns">
		    			<label>Response
		    				<input type="text" name="response_reason_text" value="<?php echo ($response_reason_text ? $response_reason_text : ''); ?>" readonly />
		    			</label>
		    		</div>		    		
		    		-->
		    				    		
		    	</div>
		    	
	    	</fieldset>
			
			<!--	    		 				    	
	    	<div class="row">
	    	  <div class="small-12 columns">
	    	    	
	    	    <input class="button medium radius" id="button" type="submit" value="Update">
	    	      	    	    
	    	    <a href="<?php echo '../dashboard_orders/?keyword='.$invoice_number.'#listing'; ?>" class="button medium radius">Cancel</a>    
	    	      	    	      	    	      	    	  				    	    
	    	  </div>
	    	</div>	
	    	-->
	    					    					    	
			</fieldset>
								
		</form>
	
	</div>
</div>		
	
<?php 

mysqli_free_result($result_order);

mysqli_close($connection);

?>