<?php

// echo 'Begin delete_discussion_thread.php<br /><hr />';

include('../includes/header.php');

$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'];

// show_array($_SESSION);
// show_array($_SERVER);

if (isset($_GET['type'])) {
	// echo 'Type: '.$_GET['type'].'<br /><hr />';
		
	if ($_SESSION['enrollment']['discussion_forum_moderator']) {
		// Only a Discussion Forum Moderator is allowed to delete Discussion content.
	
		date_default_timezone_set("America/Detroit");
		$updated_timestamp = date('Y/m/d h:i:s');
		
		$_SESSION['discuss_type'] = $_GET['type'];
		
		// Determine if user is deleting the entire Discussion Thread or a single Reply to the Discussion Thread
		if ($_SESSION['discuss_type'] == "thread") {
			// An Admin user is deleting the Thread and all Replies.
			
			$_SESSION['discussion_thread_id'] = addslashes(cleanEncoding($_GET['dt_id']));	
		
			// Update the Discussion Thread
			// Set Status t0 "D" for logical delete to retain the data.
			$queryUpdateDiscussionThread = '
				UPDATE discussion_threads SET 
				status = "D", 
				modified_by = "'.$_SESSION['user']['user_id'].'", 
				modified_timestamp = "'.$updated_timestamp.'"
				WHERE id = "'.$_SESSION['discussion_thread_id'].'"';
	
			// echo $queryUpdateDiscussionThread.'<br /><hr />';
			$result_update_discussion_thread = mysqli_query($connection, $queryUpdateDiscussionThread);
					
			if (!$result_update_discussion_thread) {
				show_mysqli_error_message($queryUpdateDiscussionThread, $connection);
				die;
			}
			
			mysqli_free_result($result_update_discussion_thread);
		
			// Update the Discussion Replies related to the Discussion Thread
			// Set Status to "D" for logical delete to retain the data.	
			$queryUpdateDiscussionReply = '
				UPDATE discussion_replies SET 
				status = "D", 
				modified_by = "'.$_SESSION['user']['user_id'].'", 
				modified_timestamp = "'.$updated_timestamp.'"
				WHERE discussion_thread_id = "'.$_SESSION['discussion_thread_id'].'"';
				
			// Add WHERE clause to enforce admin user level	
	
			// echo $queryUpdateDiscussionReply.'<br /><hr />';
			$result_update_discussion_reply = mysqli_query($connection, $queryUpdateDiscussionReply);
				
			if (!$result_update_discussion_reply) {
				show_mysqli_error_message($queryUpdateDiscussionReply, $connection);
				die;
			}
			
			mysqli_free_result($result_update_discussion_reply);
		
			mysqli_close($connection);
					
			// Remove SESSION variables.
			unset($_SESSION['discuss_type']);
			unset($_SESSION['discussion_thread_id']);
			
			header("location: ".$_SESSION['target_uri'].'#discuss'); // $_SESSION['target_uri'] is set when the Discussion Forum loads on the page.
			exit();
			
		} // if ($_SESSION['discuss_type'] == "thread") {
	
		// Determine if user is deleting the entire Discussion Thread or a single Reply to the Discussion Thread
		if ($_SESSION['discuss_type'] == "reply") {
			// An Admin user is deleting the Discussion Reply.
			
			$_SESSION['discussion_reply_id'] = addslashes(cleanEncoding($_GET['dr_id']));
				
			// Update the Discussion Reply
			// Set Status to "D" for logical delete to retain the data.
			$queryUpdateDiscussionReply = '
				UPDATE discussion_replies SET 
				status = "D", 
				modified_by = "'.$_SESSION['user']['user_id'].'", 
				modified_timestamp = "'.$updated_timestamp.'"
				WHERE id = "'.$_SESSION['discussion_reply_id'].'"';
	
			// echo $queryUpdateDiscussionReply.'<br /><hr />';
			$result_update_discussion_reply = mysqli_query($connection, $queryUpdateDiscussionReply);
					
			if (!$result_update_discussion_reply) {
				show_mysqli_error_message($queryUpdateDiscussionReply, $connection);
				die;
			}
			
			mysqli_free_result($result_update_discussion_reply);
			
			mysqli_close($connection);
					
			// Remove SESSION variables.
			unset($_SESSION['discuss_type']);
			unset($_SESSION['discussion_reply_id']);
			
			header("location: ".$_SESSION['target_uri'].'#discuss');
			exit();
			
		} // if ($_SESSION['discuss_type'] == "reply") {
	
	} // if ($_SESSION['enrollment']['discussion_forum_moderator']) {
		
} // if (isset($_GET['type'])) {

?>