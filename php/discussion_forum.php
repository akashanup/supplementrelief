<div id="captchaModal" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<form id="captchaFrm"/>
	  	<div class="row">
			<label>Are you human? <b><span id="resetCodeBtn">Reset Code</span></b>	      
			  <img id="imgCaptcha">
			  <input type="text" required id="captcha_input" value=""/>
			</label>
			<span class="error errorSpan" style="display: none">Incorrect Captcha Code</span>	    
		</div>
		<div class="row">  
			<label>	      
			  <input id="btnHuman" class="button" type="button" value="Human">
			  <input id="btnRobot" class="button" type="button" value="Robot">      
			</label>    	    
		</div>	
	</form>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<div id ="deleteBlogModal" class="reveal-modal small" data-reveal aria-hidden="true" role="dialog">
	<form data-abide autocomplete="off">
		<div id="deleteBlog">
			<div class="row" >
				<div id="deleteBlogModalHeader" class="small-12 columns" style="text-align:center" >
				</div>
			</div>	
			<div class="row" >
				<div class="small-6 columns" >
					<input class="small button radius right" type="button" data-url="" id="deleteBlogYesBtn" value="Yes" />
				</div>
				<div class="small-6 columns" >
					<input class="small button radius left" type="button" id="deleteBlogNoBtn" value="No" />
				</div>
			</div>
		</div>
	</form>
	<a class="close-reveal-modal">x</a>
</div>

<?php
	switch ($_SESSION['forum_type']) {
		case 'blog_post':
			$queryId				=	'blog_post_id';
			$queryValueId 			=	$blog_post_id;
			$reply_post 			=	$_SESSION['blog_post']['discussion_forum']['reply_post'];
			$reply_post_image 		=	$_SESSION['blog_post']['discussion_forum']['reply_post_image'];
			$reply_post_audio 		=	$_SESSION['blog_post']['discussion_forum']['reply_post_audio'];
			$reply_post_document 	=	$_SESSION['blog_post']['discussion_forum']['reply_post_document'];
			$reply_post_video 		=	$_SESSION['blog_post']['discussion_forum']['reply_post_video'];
			$forum 					=	$_SESSION['blog_post']['discussion_forum']['forum'];
			$thread_post 			=	$_SESSION['blog_post']['discussion_forum']['thread_post'];
			$thread_post_document   =   $_SESSION['blog_post']['discussion_forum']['thread_post_document'];
			$thread_post_image 		=	$_SESSION['blog_post']['discussion_forum']['thread_post_image'];
			$thread_post_audio 		=	$_SESSION['blog_post']['discussion_forum']['thread_post_audio'];
			$thread_post_video 		=	$_SESSION['blog_post']['discussion_forum']['thread_post_video'];
			$forum_type 			=	'blog post';
			break;
		case 'recipe':
			$queryId				=	'recipe_id';
			$queryValueId 			=	$recipe_id;
			$reply_post 			=	$_SESSION['recipe']['discussion_forum']['reply_post'];
			$reply_post_document 	=	$_SESSION['recipe']['discussion_forum']['reply_post_document'];
			$reply_post_image 		=	$_SESSION['recipe']['discussion_forum']['reply_post_image'];
			$reply_post_audio 		=	$_SESSION['recipe']['discussion_forum']['reply_post_audio'];
			$reply_post_video 		=	$_SESSION['recipe']['discussion_forum']['reply_post_video'];
			$forum 					=	$_SESSION['recipe']['discussion_forum']['forum'];
			$thread_post 			=	$_SESSION['recipe']['discussion_forum']['thread_post'];
			$thread_post_document   =   $_SESSION['recipe']['discussion_forum']['thread_post_document'];
			$thread_post_image 		=	$_SESSION['recipe']['discussion_forum']['thread_post_image'];
			$thread_post_audio 		=	$_SESSION['recipe']['discussion_forum']['thread_post_audio'];
			$thread_post_video 		=	$_SESSION['recipe']['discussion_forum']['thread_post_video'];
			$forum_type 			=	'recipe';
			break;
		case 'resource':
			$queryId				=	'resource_id';
			$queryValueId 			=	$resource_id;
			$reply_post 			=	$_SESSION['resource']['discussion_forum']['reply_post'];
			$reply_post_document 	=	$_SESSION['resource']['discussion_forum']['reply_post_document'];
			$reply_post_image 		=	$_SESSION['resource']['discussion_forum']['reply_post_image'];
			$reply_post_audio 		=	$_SESSION['resource']['discussion_forum']['reply_post_audio'];
			$reply_post_video 		=	$_SESSION['resource']['discussion_forum']['reply_post_video'];
			$forum 					=	$_SESSION['resource']['discussion_forum']['forum'];
			$thread_post 			=	$_SESSION['resource']['discussion_forum']['thread_post'];
			$thread_post_document   =   $_SESSION['resource']['discussion_forum']['thread_post_document'];
			$thread_post_image 		=	$_SESSION['resource']['discussion_forum']['thread_post_image'];
			$thread_post_audio 		=	$_SESSION['resource']['discussion_forum']['thread_post_audio'];
			$thread_post_video 		=	$_SESSION['resource']['discussion_forum']['thread_post_video'];
			$forum_type 			=	'resource';
			break;
		case 'product':
			$queryId				=	'brand_product_usage_id';
			$queryValueId 			=	$bpu_id;
			$reply_post 			=	$_SESSION['brand_product_usage']['discussion_forum']['reply_post'];
			$reply_post_document 	=	$_SESSION['brand_product_usage']['discussion_forum']['reply_post_document'];
			$reply_post_image 		=	$_SESSION['brand_product_usage']['discussion_forum']['reply_post_image'];
			$reply_post_audio 		=	$_SESSION['brand_product_usage']['discussion_forum']['reply_post_audio'];
			$reply_post_video 		=	$_SESSION['brand_product_usage']['discussion_forum']['reply_post_video'];
			$forum 					=	$_SESSION['brand_product_usage']['discussion_forum']['forum'];
			$thread_post 			=	$_SESSION['brand_product_usage']['discussion_forum']['thread_post'];
			$thread_post_document   =   $_SESSION['brand_product_usage']['discussion_forum']['thread_post_document'];
			$thread_post_image 		=	$_SESSION['brand_product_usage']['discussion_forum']['thread_post_image'];
			$thread_post_audio 		=	$_SESSION['brand_product_usage']['discussion_forum']['thread_post_audio'];
			$thread_post_video 		=	$_SESSION['brand_product_usage']['discussion_forum']['thread_post_video'];
			$forum_type 			=	'brand product usage';
			break;
		case 'education':
			$queryId				=	'resource_id';
			$queryValueId 			=	$resource_id;
			$reply_post 			=	$_SESSION['topic']['discussion_forum']['reply_post'];
			$reply_post_document 	=	$_SESSION['topic']['discussion_forum']['reply_post_document'];
			$reply_post_image 		=	$_SESSION['topic']['discussion_forum']['reply_post_image'];
			$reply_post_audio 		=	$_SESSION['topic']['discussion_forum']['reply_post_audio'];
			$reply_post_video 		=	$_SESSION['topic']['discussion_forum']['reply_post_video'];
			$forum 					=	$_SESSION['topic']['discussion_forum']['forum'];
			$thread_post 			=	$_SESSION['topic']['discussion_forum']['thread_post'];
			$thread_post_document   =   $_SESSION['topic']['discussion_forum']['thread_post_document'];
			$thread_post_image 		=	$_SESSION['topic']['discussion_forum']['thread_post_image'];
			$thread_post_audio 		=	$_SESSION['topic']['discussion_forum']['thread_post_audio'];
			$thread_post_video 		=	$_SESSION['topic']['discussion_forum']['thread_post_video'];
			$forum_type 			=	'education';
			break;
		case 'assessment':
			$queryId				=	'assessment_id';
			$queryValueId 			=	$_SESSION['assessment']['assessment_id'];
			$reply_post 			=	$_SESSION['assessment']['discussion_forum']['reply_post'];
			$reply_post_document 	=	$_SESSION['assessment']['discussion_forum']['reply_post_document'];
			$reply_post_image 		=	$_SESSION['assessment']['discussion_forum']['reply_post_image'];
			$reply_post_audio 		=	$_SESSION['assessment']['discussion_forum']['reply_post_audio'];
			$reply_post_video 		=	$_SESSION['assessment']['discussion_forum']['reply_post_video'];
			$forum 					=	$_SESSION['assessment']['discussion_forum']['forum'];
			$thread_post 			=	$_SESSION['assessment']['discussion_forum']['thread_post'];
			$thread_post_document   =   $_SESSION['assessment']['discussion_forum']['thread_post_document'];
			$thread_post_image 		=	$_SESSION['assessment']['discussion_forum']['thread_post_image'];
			$thread_post_audio 		=	$_SESSION['assessment']['discussion_forum']['thread_post_audio'];
			$thread_post_video 		=	$_SESSION['assessment']['discussion_forum']['thread_post_video'];
			$forum_type 			=	'assessment';
			break;
	}

	if ( $forum == true) 
	{  
	    if ($thread_post == true) 
	    {
	     
	        include('../includes/discussion_forum_header.php'); 
			?>
	        <input type="hidden" id="discussion_forum_moderator" value="<?php echo $_SESSION['enrollment']['discussion_forum_moderator']; ?>"> 
	        <input type="hidden" id="discussion_forum_reply_post" value="<?php echo $reply_post ;?>">
	        <input type="hidden" id="discussion_forum_reply_post_document" value="<?php echo $reply_post_document ;?>">
	        <input type="hidden" id="discussion_forum_reply_post_image" value="<?php echo $reply_post_image;?>">
	        <input type="hidden" id="discussion_forum_reply_post_video" value="<?php echo $reply_post_video ;?>">
	        <input type="hidden" id="discussion_forum_reply_post_audio" value="<?php echo $reply_post_audio ;?>">
	        <input type="hidden" id="loggedInUserFirstName" value="<?php echo $_SESSION['user']['first_name'] ;?>">
	        <input type="hidden" id="max_document_size" value="<?php echo MAX_DOCUMENT_SIZE ;?>">
	        <input type="hidden" id="max_image_size" value="<?php echo MAX_IMAGE_SIZE ;?>">
	        <input type="hidden" id="max_audio_size" value="<?php echo MAX_AUDIO_SIZE ;?>">
	        <input type="hidden" id="max_video_size" value="<?php echo MAX_VIDEO_SIZE ;?>">
	        <input type="hidden" id="loggedInUserUserId" value="<?php echo $_SESSION['user']['user_id'] ;?>">
	        
	        <div id="errorResponse">	            
	        </div>

	        <div id="sessionMessage">
		        <?php
		        	if(!(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'))
		        	{		
		        		echo '<div class="'.$_SESSION['message_type'].'">'.$_SESSION['message'].'</div>';
		        		$_SESSION['message'] = '';
		        		$_SESSION['message_type'] = '';
		        	}
	        	?>
	        </div>

	        <div id="discussionThreadForm">
	         
	            <fieldset>
		            <legend>Post Comment</legend>
		             
		            <form id="commentForm" method="POST" action="../php/discussion_forum_crud.php" autocomplete="off" data-id="<?php echo $_SESSION['forum_type']; ?>" enctype="multipart/form-data">
		                <input type="hidden" name="action" value="new_thread"/> 
		                <input type="hidden" name="forum_type" id="forum_type" value="<?php echo $forum_type ;?>"/>
		                <input type="hidden" name="content_assest_id" id="content_assest_id" value="<?php echo $queryValueId?$queryValueId:null ;?>"/>
		                <input type="hidden" name="comment_user_id" id="comment_user_id" value="<?php echo $_SESSION['user']['user_id']; ?>"/>
		                
		                <input type="text" name="comment_note" id="comment_note" value=""/>
		                
		                <input type="hidden" name="use_google_recaptcha" value=""/> 
		                 		                
		                <div class="row">
			                		
		                	<div class="small-12 medium-8 columns">
			                	
			                	<!--
			                	<?php
			                		if($_SESSION['forum_type'] == 'education')
			                		{
	                			?>
	                				<div class="row">
					                    <div class="small-12 columns">
					                        <input type="text" required name="comment_title" id="comment_title" style="width: 100%" placeholder="Title ..." value="<?php echo $_SESSION['temp']['thread_title'] ?>" />
					                        <span class="error errorSpan" style="display: none">Title is required</span>
					                    </div>
					                </div>
	                			<?php
			                		}
			                	?>
			                	-->
			                	
								<div class="row">
				                    <div class="small-12 columns">
				                        <textarea required name="comment_text" id="comment_text" style="width: 100%" placeholder="Comment..." rows="2" /><?php echo $_SESSION['temp']['thread_text']; ?></textarea>
				                        <span class="error errorSpan" style="display: none">Comment is required</span>
				                    </div>
				                </div>
                              
		                        <div class="row collapse">        
		                        	<div class="row">
			                        	<!--                    
			                           	<div class="small-1 columns">
		                           			<span>&nbsp;</span>
			                           	</div>
			                           	-->
			                           	
										<?php 
																						
											if($_SESSION['forum_type'] == 'education') { 
												$icon_spacing_large = 'large-2';	
											} else {
												$icon_spacing_large = 'large-1';
											}
											
											// Determine if User is allowed to attached a audio to the post
						                    if ($thread_post_audio) {  
						                        echo 
						                        	'                              
							                        <div class="small-2 medium-2 '.$icon_spacing_large.' columns">
						                                <div class="right">
						                                	<a href="#fndtn-discuss" data-toggle="tooltip" title="Attach Audio file" class="fileInputAnchor audioAnchor">
																<i class="fa fa-file-audio-o fa-3x" aria-hidden="true"></i>
															</a>
															<input type="file" data-key="3" name="audio_file" class="audio_file fileInput" style="display: none">
					                                	</div>           
						                           	</div>
						                           	';      
						                    }
					                             
						                    // Determine if User is allowed to attached a Document to the post
						                    if ($thread_post_document) {   
						                        echo 
						                        	'                              
							                        <div class="small-2 medium-2 '.$icon_spacing_large.' columns">
						                                <div class="right">
						                                	<a href="#fndtn-discuss" data-toggle="tooltip" title="Attach Document file" class="fileInputAnchor documentAnchor">
						                                		<i class="fa fa-file-text-o fa-3x" aria-hidden="true"></i>
					                                		</a>
					                                		<input type="file" data-key="0" name="document_file" class="document_file fileInput" style="display: none">
					                                	</div>
						                           	</div>
						                           	';      
						                    }                                   
					 
						                    // Determine if User is allowed to attached an Image to the post                                
						                    if ($thread_post_image) {  
						                        echo 
						                        	'                              
							                        <div class="small-2 medium-2 '.$icon_spacing_large.' columns">
						                                <div class="right">
						                                	<a href="#fndtn-discuss" data-toggle="tooltip" title="Attach Image file" class="fileInputAnchor imageAnchor">
						                                		<i class="fa fa-file-image-o fa-3x" aria-hidden="true"></i>
					                                		</a>
					                                		<input type="file" data-key="1" name="image_file" class="image_file fileInput" style="display: none">
					                                	</div>
						                           	</div>
						                           	';      
						                    }

						                    // Determine if User is allowed to attached a Video to the post
						                    if ($thread_post_video) {  
						                        echo 
						                        	'                              
							                        <div class="small-2 medium-2 '.$icon_spacing_large.' columns">
						                                <div class="right">
						                                	<a href="#fndtn-discuss" data-toggle="tooltip" title="Attach Video file" class="fileInputAnchor videoAnchor">
						                                		<i class="fa fa-file-video-o fa-3x" aria-hidden="true"></i>
					                                		</a>
					                                		<input type="file" data-key="2" name="video_file" class="video_file fileInput" style="display: none">
					                                	</div>
						                           	</div>
						                           	';      
						                    }                                                                                                                                       
					                	?>
					                	
					                	<div class="small-1 medium-1 large-1 end"></div>	
										
		                            </div>
		                            
				                	<div class="row uploadedFileName"></div>
				                	
		                        </div>
		                	</div>
		                	
		                	<div class="small-12 medium-4 columns">
			                	
								<!-- 
			                	<div class="row">		
						    		<div class="small-12 columns">
							    		<!-- Paste this snippet at the end of the <form> where you want the reCAPTCHA widget to appear. -->
										<!-- <div class="g-recaptcha" data-sitekey="<?php echo public_captcha_key; ?>"></div>
					    		    	<br />
					     			</div>
						    	</div>
						    	-->
						    	
						    	<!-- <div class="show-for-small-only colwrapmargin"><br></div> -->
						    
				                <div class="row">
				                    <div class="small-12 columns colWrapMargin">
				                        <button type="button" id="postCommentBtn" class="button medium radius">Post</button>
				                        <!-- <input type="submit" id="postCommentBtn1" class="button medium radius" value="Post"> -->
				                        <input type="reset" class="button medium radius resetBtn" value="Reset">
				                    </div>
				                </div>
		                							
		                	</div>
		               	         	
		                </div>
		                                                 
		            </form>
	            </fieldset>
	     
	        </div>
	        
	        <div id="discussResponse"></div>
	        
	        <div id="discussBlogResponse"></div>
	        <?php
		        
		    unset($_SESSION['temp']['thread_title']);
		    unset($_SESSION['temp']['thread_text']);
	     
	    } // if ($forum == true) {
	     
	    // Query for all Discussion Threads for this Project Program Course Topic Web Page.
	    // Status "S" means show record.
	     
	    $queryThread = 'SELECT 
	        dt.id, 
	        dt.created_timestamp, 
	        dt.title, 
	        dt.text,
	        dt.file_document_url, 
	        dt.file_document_type,  
	        dt.file_image_url, 
	        dt.file_image_type, 
	        dt.file_audio_url, 
	        dt.file_audio_type, 
	        dt.file_video_url, 
	        dt.file_video_type,  
	        dt.created_by, 
	        dt.modified_timestamp,
	        dt.file_status,
	        dt.file_type, 
	        pe.first_name, 
	        pe.last_name 
	        FROM discussion_threads dt 
	        LEFT JOIN persons pe ON dt.created_by = pe.id';
	        
	    if($_SESSION['forum_type'] == 'education')
	    {
	    	$queryThreadExtension = ' WHERE dt.project_program_id = "'.$_SESSION['enrollment']['project_program_id'].'" 
	    								AND dt.course_id = "'.$_SESSION['enrollment']['course_id'].'" 
	    								AND dt.topic_id = "'.$_SESSION['enrollment']['topic_id'].'" 
	    								AND dt.web_page_id = "'.$_SESSION['enrollment']['web_page_id'].'"';
	    }
	    else
	    {
	    	$queryThreadExtension = ' WHERE dt.'.$queryId.' = '.$queryValueId;
	    }

	    $queryThread .= $queryThreadExtension.' AND dt.status in ("S", "R") ORDER BY dt.modified_timestamp DESC';
	                 
	    // echo $queryThread . '<br /><hr />';
	     
	    $result_list_discussion_thread = mysqli_query($connection, $queryThread);
	     
	    if (!$result_list_discussion_thread) {
	        show_mysqli_error_message($queryThread, $connection);
	        die;
	    }
	         
	    $comments_block = false;
	     
	    while($r1 = mysqli_fetch_assoc($result_list_discussion_thread)) { 
		    // show_array($r1);
	        // For Each Comment:
	        // 1. Display the Comment
	        // 2. Display a Reply form if the configuration allows for new Replies to the Comment
	        // 3. Display any Comment Replies (oldest first)
	     
	        // show_array($r1); 
	             
	        if (!$comments_block) {
	            echo '<div class="row">
	                    <div class="small 12-columns">        
	                        <div class="blogPostCommentContainer">';
	            $comments_block = true;
	        }
	     
	        // See if the User logged in has Forum Moderator privileges. If so provide tools to remove unwanted Comments and Replies.
	        if ($_SESSION['enrollment']['discussion_forum_moderator']) {
	            $comment_moderator = '
	            <span class="label">Moderator</span>&nbsp;<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Remove this Comment and all related Replies?">'.               
	            '<a data-url="../php/delete_discussion_thread.php?type=thread&dt_id='.$r1['id'].'"  href="#fndtn-discuss" class="moderatorCommentDelete"><i class="fa fa-remove icon-red"></i></a>'.'</span><br />';
	            // Delete Discussion Thread should be handled using AJAX to process request in the background.      
	        } else {
	            $comment_moderator = null;
	        }
	         
	        if (strlen($r1['first_name']) > 1) {
	            $display_first_name = $r1['first_name'];
	        } else {
	            $display_first_name = 'Guest';          
	        }       
	         
	        $commentHeader = '
	        <div class="commentHeader">   
	            <span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Person who posted this Comment"><i class="fa fa-user icon-red"></i></span>&nbsp;'.$display_first_name.'&nbsp; 
	            <span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Post Timestamp"><i class="fa fa-clock-o icon-red"></i></span> '.date('m/d/y h:i A T', strtotime($r1['created_timestamp'])).'&nbsp;
	            <span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Report Comment as SPAM"><a href="#fndtn-discuss" class="headerCommentSpam" data-url="../php/report_discussion_forum_spam.php?forum_type='.$_SESSION["forum_type"].'&comment_type=comment&comment_id='.$r1['id'].'"><i class="fa fa-ban icon-red"></i></a></span>&nbsp;'
	            .$comment_moderator.'                           
	        </div>
	        <div id="threadPostHeaderTitle">
	            <h4>'.$r1['title'].'</h4>
	        </div>';
	        // Report SPAM should be handled using AJAX to process request in the background.       
	         
	        // Comment body content display uses different layouts depending upon if a media file is present and what type it is.
	         
	        if (strlen($r1['file_document_url']) > 0) {      
	            // echo 'File Document URL present<br />';
	                             
	            echo
	            // div id used for returning to this record in a URL using a #hashtag   
	            '<div id="comment'.$r1['id'].'" class="row collapse">     
	                <div class="small-12 medium-8 columns">               
	                    <div class="blogPostComment">'
	                     
	                        .$commentHeader.'   
	                                                                                 
	                        <div class="commentBodyText"><p>'.$r1['text'].'</p></div>                       
	                                                                                 
	                    </div>                
	                </div>
	                                     
	                <div class="small-12 medium-4 columns">                                   
	                    <div class="commentBodyImage">';
	                    	if($r1['file_status'] == 2)
                        	{
                        		echo '<a href="'.$r1['file_document_url'].'" target="_blank" title="Preview document"><img src="https://cdn-manager.net/media/images/document.jpg" alt="" width="150"/></a>';
                        	}
                        	else if($r1['file_status'] == 0 || $r1['file_status'] == 1 )
	                    	{
	                    		echo '<img src="../img/MediaWillBeAvailableSoon.jpg">';
	                    	}
	                    	else
	                    	{
	                    		echo '<img src="../img/uploadfailed.jpg">';
	                    	}
                    	echo 
                    	'</div> 
	                </div>
	                                         
	            </div>';
	         
	        } elseif (strlen($r1['file_image_url']) > 0) {       
	            // echo 'File Image URL present<br />';
	                             
	            echo
	            // div id used for returning to this record in a URL using a #hashtag   
	            '<div id="comment'.$r1['id'].'" class="row collapse">     
	                <div class="small-12 medium-8 columns">               
	                    <div class="blogPostComment">'
	                     
	                        .$commentHeader.'   
	                                                                                 
	                        <div class="commentBodyText"><p>'.$r1['text'].'</p></div>                       
	                                                                                 
	                    </div>                
	                </div>
	                                     
	                <div class="small-12 medium-4 columns">                                   
	                    <div class="commentBodyImage">';
	                    	if($r1['file_status'] == 2)
                        	{
                        		echo '<img src="'.$r1['file_image_url'].'" alt="" />';
                        	}
                        	else if($r1['file_status'] == 0 || $r1['file_status'] == 1 )
	                    	{
	                    		echo '<img src="../img/MediaWillBeAvailableSoon.jpg">';
	                    	}
	                    	else
	                    	{
	                    		echo '<img src="../img/uploadfailed.jpg">';
	                    	}
                    	echo 
                    	'</div>    
	                </div>
	                                         
	            </div>';
	             
	        } elseif (strlen($r1['file_audio_url']) > 0) {       
	            // echo 'File Audio URL present<br />';
	            echo
	            '<div id="comment'.$r1['id'].'" class="row collapse">     
	                <div class="small-12 medium-8 columns">               
	                    <div class="blogPostComment">'
	                     
	                        .$commentHeader.'   
	                                                                                 
	                        <div class="commentBodyText"><p>'.$r1['text'].'</p></div>                       
	                                                                                 
	                    </div>                
	                </div>
	                                     
	                <div class="small-12 medium-4 columns">
	                	<div class="commentBodyAudio">';
		                    if($r1['file_status'] == 2)
                        	{
                        		echo 	'<audio controls>
										  <source src="'.$r1['file_audio_url'].'" type="audio/mpeg">
										  Your browser does not support the audio element.
										</audio>
										';
                        	}
                        	else if($r1['file_status'] == 0 || $r1['file_status'] == 1 )
	                    	{
	                    		echo '<img src="../img/MediaWillBeAvailableSoon.jpg">';
	                    	}
	                    	else
	                    	{
	                    		echo '<img src="../img/uploadfailed.jpg">';
	                    	}
	                    echo 
	                    '</div>
	                </div>
	                                         
	            </div>';
	             
	        } elseif (strlen($r1['file_video_url']) > 0) {       
	            // echo 'File Video URL present<br />';
	         
	            echo   
	            '<div id="comment'.$r1['id'].'" class="row collapse">     
	                <div class="small-12 medium-8 columns">               
	                    <div class="blogPostComment">'
	                                             
	                        .$commentHeader.'   
	                         
	                        <div class="commentBodyText"><p>'.$r1['text'].'</p></div>
	                                                                                                     
	                    </div>                
	                </div>
	                                     
	                <div class="small-12 medium-4 columns">                                   
	                    <div class="commentBodyVideo">';
	                 		if($r1['file_status'] == 2)
	                 		{
	                 			echo
		                        '<video preload="none" controls poster="https://cdn-manager.net/media/images/user-video-poster.jpeg" style="width:100%;">
		                        	<source src="'.$r1['file_video_url'].'" type="video/mp4">
		                            Your browser does not support HTML5 Video.
  		                        </video>';          
	                    	}
	                    	else if($r1['file_status'] == 0 || $r1['file_status'] == 1 )
	                    	{
	                    		echo '<img src="../img/MediaWillBeAvailableSoon.jpg">';
	                    	}
	                    	else
	                    	{
	                    		echo '<img src="../img/uploadfailed.jpg">';
	                    	}
	                    echo 
	                    '</div>    
	                </div>
	                                         
	            </div>';
	             
	        } else {
	         
	            // Text only Comment body with no media files.
	            echo   
	            '<div id="comment'.$r1['id'].'" class="row">      
	                <div class="small-12 columns">                
	                    <div class="blogPostComment">'    
	                                     
	                        .$commentHeader.'
	                                                                             
	                        <div class="commentBodyText"><p>'.$r1['text'].'</p></div>
	                                                                 
	                    </div>
	                </div>
	            </div>';
	             
	        } // May also add support for other media types such as Audio.  
	         
	                     
	        // Query for all Discussion Replies related to the Discussion Thread.
	        // Status "S" means show record.
	         
	        $queryDiscussionReplies = 'SELECT 
	            dr.id, 
	            dr.created_timestamp, 
	            dr.text,
	            dr.file_document_url, 
	            dr.file_document_type, 
	            dr.file_image_url, 
	            dr.file_image_type, 
	            dr.file_video_url, 
	            dr.file_video_type, 
	            dr.file_audio_url, 
	            dr.file_audio_type,  
	            dr.created_by,
	            dr.file_status,
	            dr.file_type, 
	            pe.first_name, 
	            pe.last_name 
	            FROM discussion_replies dr
	            LEFT JOIN persons pe ON dr.created_by = pe.id 
	            WHERE discussion_thread_id = '.$r1['id'].'
	            AND dr.status in ("S", "R")         
	            ORDER BY dr.created_timestamp ASC';
	                                 
	        // echo $queryDiscussionReplies . '<br /><hr />';
	         
	        $result_list_discussion_reply = mysqli_query($connection, $queryDiscussionReplies);
	         
	        if (!$result_list_discussion_reply) {
	         	show_mysqli_error_message($queryDiscussionReplies, $connection);
			 	die;
	        }
	             
	        // echo 'Database Query Discussion Replies for Discussion Thread succeeded.<br /><hr />';
	                 
	        while($r2 = mysqli_fetch_assoc($result_list_discussion_reply)) {
	                     
	            if ($_SESSION['enrollment']['discussion_forum_moderator']) {
	                $comment_reply_moderator = '&nbsp;&nbsp;<span class="label">Moderator</span>&nbsp;<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Remove this Comment Reply?"><a data-url="../php/delete_discussion_thread.php?type=reply&dr_id='.$r2['id'].'" data-id="'.$r2['id'].'" href="#fndtn-discuss" class="moderatorReplyDelete"><i class="fa fa-remove icon-red"></i></a></span><br />';       
	            } else {
	                $comment_reply_moderator = null;
	            }
	             
	            if (strlen($r2['first_name']) > 1) {
	                $display_first_name = $r2['first_name'];
	            } else {
	                $display_first_name = 'Guest';          
	            }       
	                 
	            $commentHeader = '
	            <div class="commentHeader">                           
	              <span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Person who posted this Comment Reply"><i class="fa fa-user icon-red"></i></span>&nbsp;'.$display_first_name.'&nbsp;
	              <span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Date/Time this Comment Reply was posted">&nbsp;<i class="fa fa-clock-o icon-red"></i></span> '.date('m/d/y h:i A T', strtotime($r2['created_timestamp'])).'&nbsp;
	              <span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Report Reply as SPAM"><a href="#fndtn-discuss" class="headerReplySpam" data-url="../php/report_discussion_forum_spam.php?forum_type='.$_SESSION['forum_type'].'&comment_type=reply&comment_id='.$r2['id'].'"><i class="fa fa-ban icon-red"></i></a></span>&nbsp;'
	              .$comment_reply_moderator.'                             
	            </div>';
	                     
	            echo       
	            '<div class="row">        
	                <div id="reply'.$r2['id'].'" class="small-12 columns">
	                	<hr>                
	                    <div class="topicCommentReply">';     
	                                     
	                        // echo $commentHeader;
	                         
	                        if (strlen($r2['file_document_url']) > 0) {		
								// echo 'File Document URL present<br />';
											
								echo
								// div id used for returning to this record in a URL using a #hashtag	
								'<div id="comment'.$r2['id'].'" class="row collapse">		
									<div class="small-12 medium-8 columns">				
										<div class="topicReply">'
										
											.$commentHeader.'	
																									
											<div class="commentBodyText"><p>'.$r2['text'].'</p></div>						
																									
										</div>				
									</div>
														
									<div class="small-12 medium-4 columns">									
										<div class="topicReplyBodyImage">';
											if($r2['file_status'] == 2)
						                    {

						                    	echo
						                        '<a href="'.$r2['file_document_url'].'" target="_blank" title="Preview document"><img src="https://cdn-manager.net/media/images/document.jpg" alt="" width="125"/></a>';
						                    }
					                    	else if($r2['file_status'] == 0 || $r2['file_status'] == 1 )
					                    	{
					                    		echo '<img src="../img/MediaWillBeAvailableSoon.jpg">';
					                    	}
					                    	else
					                    	{
					                    		echo '<img src="../img/uploadfailed.jpg">';
					                    	}
										echo 
										'</div>	
									</div>
															
								</div>';
								
							} elseif (strlen($r2['file_image_url']) > 0) {
	                         
	                            // echo 'File Image URL present<br />';
	                                             
	                            echo   
	                            '<div class="row collapse">   
	                             
	                                                                                                                   
	                                <div class="small-12 medium-8 columns">               
	                                    <div class="topicReply">'
	                                     
	                                        .$commentHeader.'   
	                                                                                                 
	                                        <div class="commentBodyText"><p>'.$r2['text'].'</p></div>                       
	                                                                                                 
	                                    </div>                
	                                </div>
	                                
	                                <div class="small-12 medium-4 columns">                                   
	                                    <div class="topicReplyBodyImage">';
	                                    	if($r2['file_status'] == 2)
						                    {

						                    	echo
						                        '<img src="'.$r2['file_image_url'].'" alt="" />';
						                    }
					                    	else if($r2['file_status'] == 0 || $r2['file_status'] == 1 )
					                    	{
					                    		echo '<img src="../img/MediaWillBeAvailableSoon.jpg">';
					                    	}
					                    	else
					                    	{
					                    		echo '<img src="../img/uploadfailed.jpg">';
					                    	}
										echo
                                    	'</div> 
	                                </div>
	                                                        
	                            </div>';
	                            
	                        } elseif (strlen($r2['file_audio_url']) > 0) {
	                         
	                            // echo 'Audio Image URL present<br />';
	                            echo   
	                            '<div class="row collapse">   
	                             
	                                                                                                                   
	                                <div class="small-12 medium-8 columns">               
	                                    <div class="topicReply">'
	                                     
	                                        .$commentHeader.'   
	                                                                                                 
	                                        <div class="commentBodyText"><p>'.$r2['text'].'</p></div>                       
	                                                                                                 
	                                    </div>                
	                                </div>
	                                
	                                <div class="small-12 medium-4 columns">
	                                	<div class="commentBodyAudio">';                                  
		                                    if($r2['file_status'] == 2)
						                    {

						                    	echo
						                        '<audio controls>
												  <source src="'.$r2['file_audio_url'].'" type="audio/mpeg">
												  Your browser does not support the audio element.
												</audio>
												';
						                    }
					                    	else if($r2['file_status'] == 0 || $r2['file_status'] == 1 )
					                    	{
					                    		echo '<img src="../img/MediaWillBeAvailableSoon.jpg">';
					                    	}
					                    	else
					                    	{
					                    		echo '<img src="../img/uploadfailed.jpg">';
					                    	}
					                    echo
					                    '</div>
	                                </div>
	                                                        
	                            </div>';
	                            
	                        } elseif (strlen($r2['file_video_url']) > 0) {  
		                             
					            // echo 'File Video URL present<br />';
					         
					            echo   
					            '<div id="comment'.$r2['id'].'" class="row collapse">     
					                <div class="small-12 medium-8 columns">               
					                    <div class="topicReply">'
					                                             
					                        .$commentHeader.'   
					                         
					                        <div class="commentBodyText"><p>'.$r2['text'].'</p></div>
					                                                                                                     
					                    </div>                
					                </div>
					                                     
					                <div class="small-12 medium-4 columns">                                   
					                    <div class="commentBodyVideo">';
					                    if($r2['file_status'] == 2)
					                    {

					                    	echo
					                        '<video preload="none" controls poster="https://cdn-manager.net/media/images/user-video-poster.jpeg" style="width:100%;">
					                            <source src="'.$r2['file_video_url'].'" type="video/mp4">
					                            Your browser does not support HTML5 Video.
					                        </video>';
					                    }
				                    	else if($r2['file_status'] == 0 || $r2['file_status'] == 1 )
				                    	{
				                    		echo '<img src="../img/MediaWillBeAvailableSoon.jpg">';
				                    	}
				                    	else
				                    	{
				                    		echo '<img src="../img/uploadfailed.jpg">';
				                    	}
					                    echo
					                    '</div>    
					                </div>
					                                         
					            </div>';
	                   
	                        } else {
	                         
	                            echo   
	                            '<div class="row">        
	                                <div class="small-12 columns">                
	                                    <div class="topicReply">'     
	                                                     
	                                        .$commentHeader.'
	                                                                                             
	                                        <div class="commentBodyText"><p>'.$r2['text'].'</p></div>
	                                                                                 
	                                    </div>
	                                </div>
	                            </div>';
	                             
	                        }   
	                         
	                        echo '                      
	                    </div>
	                </div>
	            </div>';              
	                                                                                         
	        } // while($r2 = mysqli_fetch_assoc($result_list_discussion_reply)) {
	     
	        mysqli_free_result($result_list_discussion_reply);
	        
	        // Determine if the User is allowed to Reply to Comments. If so, display the Comment Reply form
	        if ($reply_post) { 
	         
	            ?>
	             
	            <div class="discussionReplyForm" id="discussionReplyForm<?php echo $r1['id'];?>">
	             
	                <fieldset>
	                <legend>Post Reply</legend>
	                 
	                <form autocomplete="off" class="replyForm" method="POST" action="../php/discussion_forum_crud.php" enctype="multipart/form-data">
	                 
	                    <input type="hidden" name="action" value="new_reply"/>
	                    
	                    <input type="hidden" name="forum_type" value="<?php echo $forum_type ;?>"/>
	                                        
	                    <input type="hidden" name="comment_reply_id" class="comment_reply_id" value="<?php echo $r1['id']; ?>"/>  
	                         
	                    <input type="hidden" name="comment_reply_user_id" class="comment_reply_user_id" value="<?php echo $_SESSION['user']['user_id']; ?>"/> 
	                    
	                    <input type="text" name="comment_note" class="comment_note" value=""/>    
	                         	                            
	                    <div class="row collapse">
	                     
	                      <div class="small-12 medium-8 columns">
	                        <textarea required name="comment_reply_text" class="comment_reply_text" style="width: 100%" placeholder="Reply..."  /><?php echo $_SESSION['temp']['thread_reply_text']; ?></textarea>
	                        <span class="error errorSpan" style="display: none">Reply is required</span>
	                      </div>
	                       
	                       <div class="small-12 medium-4 columns">
	                          <!--<a href="../resources/actions/new_discussion_reply.php" class="button small radius" >Reply</a>-->
	                          <button type="button" class="replyBtn button small radius">Reply</button>
	                          <input type="reset" class="button small radius resetBtn" value="Reset">
	                       </div>
	                        
	                   </div>
	                   
	                   	<div class="row collapse">
	                        <div class="small-12 columns ">
								<?php 
									// Determine if User is allowed to attached a audio to the post
				                    if ($reply_post_audio) {  
				                        echo 
				                        	'                              
					                        <div class="small-2 medium-2 large-1 columns">
				                                <div class="right">
				                                	<a href="#fndtn-discuss" data-toggle="tooltip" title="Attach Audio file" class="fileInputAnchor audioAnchor">
														<i class="fa fa-file-audio-o fa-2x" aria-hidden="true"></i>
													</a>
													<input type="file" data-key="3" name="audio_file" class="audio_file fileInput" style="display: none">
			                                	</div>           
				                           	</div>
				                           	';      
				                    }
			                             
				                    // Determine if User is allowed to attached a Document to the post
				                    if ($reply_post_document) {   
				                        echo 
				                        	'                              
					                        <div class="small-2 medium-2 large-1 columns">
				                                <div class="right">
				                                	<a href="#fndtn-discuss" data-toggle="tooltip" title="Attach Document file" class="fileInputAnchor documentAnchor">
				                                		<i class="fa fa-file-text-o fa-2x" aria-hidden="true"></i>
			                                		</a>
			                                		<input type="file" data-key="0" name="document_file" class="document_file fileInput" style="display: none">
			                                	</div>
				                           	</div>
				                           	';      
				                    }                                   
			 
				                    // Determine if User is allowed to attached an Image to the post                                
				                    if ($reply_post_image) {  
				                        echo 
				                        	'                              
					                        <div class="small-2 medium-2 large-1 columns">
				                                <div class="right">
				                                	<a href="#fndtn-discuss" data-toggle="tooltip" title="Attach Image file" class="fileInputAnchor imageAnchor">
				                                		<i class="fa fa-file-image-o fa-2x" aria-hidden="true"></i>
			                                		</a>
			                                		<input type="file" data-key="1" name="image_file" class="image_file fileInput" style="display: none">
			                                	</div>
				                           	</div>
				                           	';      
				                    }
	
				                    // Determine if User is allowed to attached a Video to the post
				                    if ($reply_post_video) {  
				                        echo 
				                        	'                              
					                        <div class="small-2 medium-2 large-1 columns">
				                                <div class="right">
				                                	<a href="#fndtn-discuss" data-toggle="tooltip" title="Attach Video file" class="fileInputAnchor videoAnchor">
				                                		<i class="fa fa-file-video-o fa-2x" aria-hidden="true"></i>
			                                		</a>
			                                		<input type="file" data-key="2" name="video_file" class="video_file fileInput" style="display: none">
			                                	</div>
				                           	</div>
				                           	';      
				                    }                                                                                                                                       
			                	?>
			                	
			                	<div class="small-1 medium-1 large-1 end"></div>
			                		
	                       	</div>
	                       	
		                	<div class="row uploadedFileName">
		                	</div>
		                	
		                </div>         
	                                                     
	                </form>           
	                </fieldset>
	             
	            </div> <!-- <div id="discussionReplyForm"> -->
	                     
	            <?php
		        unset($_SESSION['temp']['thread_reply_text']);
	             
	        } // if ($_SESSION['topic_discussion_reply_post']) {        

	        
	        
	        
	         
	        if ($comments_block) {
	            echo '</div></div></div> <!-- end blogPostCommentContainer -->';
	            $comments_block = false;
	        }
	                         
	        ?>
	             
	        <!-- <hr /> -->
	         
	        <?php
	                             
	    } // while($r1 = mysqli_fetch_assoc($result_list_discussion_thread)) {  
	     
	    mysqli_free_result($result_list_discussion_thread);
	     
	    include('../includes/discussion_forum_help_modal.php'); 
	     
	} // END if ($forum) {  
		
	// show_array($_SESSION);
   
?>