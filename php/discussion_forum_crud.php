<?php

	// Used to processes new Comments/Replies for Blog Post, Education (Topic), Products, Recipes and Resources Discussion Forum.

	include_once('../includes/header.php');
	// Set the current date and time for the database record audit trail.
	$created_timestamp 		= 	date('Y/m/d H:i:s');
	$modified_timestamp 	= 	$created_timestamp;

	$messageType 			= 	'';
	$message 				=	'';
	$responseMessageType    =	'';
	$responseMessage    	=	'';	
	$last_insert_id 	    =	'';
	$thread_title    		=	'';
	$thread_text 		    =	'';
	$thread_reply_id 	    =	'';
	$thread_reply_text 	    =	'';
	$document_file_url     	=	'';
	$document_file_extension= 	'';
	$image_file_url     	=	'';
	$image_file_extension 	=	'';
	$video_file_url     	=	'';
	$video_file_extension   =	'';
	$audio_file_url 	    =	'';
	$audio_file_extension   =	'';
	$file_status     		=	'';
	$media_type 	    	=	'';
	$post_type 				=	'';
	$forum_subject 			=	'';

	$json 					=	[];
	$json['captcha_status']	=	0;


	function createPost($connection, $ie9)
	{
		global $messageType, $message, $responseMessageType, $responseMessage, $last_insert_id, $thread_title, $thread_text, $thread_reply_id, $thread_reply_text, $document_file_url, $document_file_extension, $image_file_url, $image_file_extension, $video_file_url, $video_file_extension, $audio_file_url, $audio_file_extension, $file_type, $media_type, $json, $created_timestamp, $modified_timestamp, $post_type, $forum_subject;

		if ($_POST['action'] == 'new_thread' OR $_POST['action'] == 'new_reply') 
		{ 
			
			$are_you_a_robot = safe_sql_data($connection, $_POST['comment_note']);
			// Field hidden by css used to reject submissions from robots. In addition to Google Recaptcha because Discussion Forum pages can have many forms and Google Recaptcha is expensive to load each time per form so is used only on New Thread.
			
			if (isset($are_you_a_robot) && strlen($are_you_a_robot) > 0) 
			{
				// echo 'You are a SPAMbot!';
				die();
			}

			$action 				= 	safe_sql_data($connection, $_POST['action']);
			$forum_type 			= 	safe_sql_data($connection, $_POST['forum_type']);
			$use_google_recaptcha 	= 	safe_sql_data($connection, $_POST['use_google_recaptcha']);
			$json['forum_type']	  	= 	$forum_type;

			if ($action == 'new_thread') 
			{ 
				
				$post_type 			= 	'thread';
				$thread_text 		= 	safe_sql_data($connection, $_POST['comment_text']);
				$thread_user_id 	= 	safe_sql_data($connection, $_POST['comment_user_id']);
				
				$_SESSION['temp']['thread_title'] 	= 	$thread_title;
				$_SESSION['temp']['thread_text'] 	= 	$thread_text;
				
				if ($use_google_recaptcha) 
				{
					
					if (googleRecaptcha(private_captcha_key, sanitize($_POST['g-recaptcha-response']), sanitize($_SERVER['REMOTE_ADDR']), sanitize($_SERVER['HTTP_REFERER'].'#sessionMessage'))) 
					{
			
					} 
					else 
					{
					
						// Google Recaptcha is only used for a new Discussion Thread and not for a new Reply 
						// because there is the potential of many replies per Discussion Forum page and there is a cost to load Recaptcha resources for each form.
						// If the function fails it has it's own logic to terminate so this else branch will never execute.	
					}					
			
				}	
				
			} 
			elseif ($action == 'new_reply') 
			{
				
				$post_type 								= 	'reply';			
				$thread_reply_id 						= 	safe_sql_data($connection, $_POST['comment_reply_id']);
				$thread_reply_user_id 					= 	safe_sql_data($connection, $_POST['comment_reply_user_id']);
				$thread_reply_text 						= 	safe_sql_data($connection, $_POST['comment_reply_text']);
				
				$_SESSION['temp']['thread_reply_text'] 	= 	$thread_reply_text;			
			}
			
			switch ($forum_type) 
			{	
			
				case 'blog post':	    	
					$blog_post_id 		= 	$_SESSION['blog_post']['blog_post_id'];
					$forum_subject 		= 	'Blog Post Forum Post Notification';
					$json['forum_type'] = 	'blog_post';
					break;
							        
				case 'brand product usage':	    	
					$brand_product_usage_id 	= 	$_SESSION['brand_product_usage']['brand_product_usage_id'];
					$forum_subject 				= 	'Brand Product Forum Post Notification';
					$json['forum_type'] 		= 	'product';
					break;
					
				case 'education':	    	
					$course_id 			= 	$_SESSION['enrollment']['course_id'];
					$topic_id 			= 	$_SESSION['enrollment']['topic_id'];
					$web_page_id 		= 	$_SESSION['enrollment']['web_page_id'];
					$forum_subject 		= 	'Education Forum Post Notification';
					$json['forum_type'] = 	'education';
					break;
					
				case 'recipe':	    	
					$recipe_id 				= 	$_SESSION['recipe']['recipe_id'];
					$forum_subject 			= 	'Recipe Forum Post Notification';
					$json['forum_type'] 	= 	'recipe';
					break;
					
				case 'resource':	    	
					$resource_id 		= 	$_SESSION['resource']['resource_id'];
					$forum_subject 		= 	'Resource Forum Post Notification';
					$json['forum_type'] = 	'resource';
					break;

				case 'assessment':	    	
					$assessment_id 		= 	safe_sql_data($connection, $_POST['content_assest_id']);//$_SESSION['assessment']['assessment_id'];
					$forum_subject 		= 	'Asessment Forum Post Notification';
					$json['forum_type'] = 	'assessment';
					break;
			}	

			$file_status 		=	NULL;
			$media_type			=	NULL;


			//check for IE9 and do the same as earlier.
			if ($ie9)
			{
				$document_file_name = 	$_FILES['document_file']['name'];
				$image_file_name 	= 	$_FILES['image_file']['name'];	
				$video_file_name 	=	$_FILES['video_file']['name'];
				$audio_file_name 	= 	$_FILES['audio_file']['name'];

				if (isset($document_file_name) && strlen($document_file_name) > 0) {
					$media_type 		= 	'document';
					$documentFileArray 	= 	$_FILES['document_file'];
					list($document_file_name, $document_file_url, $document_file_extension) = uploadMedia($post_type, $media_type, $documentFileArray, $thread_title, $thread_text, $thread_reply_text);
				}
				
				else if (isset($image_file_name) && strlen($image_file_name) > 0) {
					$media_type 	= 	'image';
					$imageFileArray = 	$_FILES['image_file'];
					list($image_file_name, $image_file_url, $image_file_extension) = uploadMedia($post_type, $media_type, $imageFileArray, $thread_title, $thread_text, $thread_reply_text);	

				}
				
				else if (isset($video_file_name) && strlen($video_file_name) > 0) {
					$media_type 	= 	'video';
					$videoFileArray = 	$_FILES['video_file'];
					list($video_file_name, $video_file_url, $video_file_extension) = uploadMedia($post_type, $media_type, $videoFileArray, $thread_title, $thread_text, $thread_reply_text);
				}
				
				else if (isset($audio_file_name) && strlen($audio_file_name) > 0) {
					$media_type 	= 	'audio';
					$audioFileArray = 	$_FILES['audio_file'];
					list($audio_file_name, $audio_file_url, $audio_file_extension) = uploadMedia($post_type, $media_type, $audioFileArray, $thread_title, $thread_text, $thread_reply_text);
				}
				$file_status 		=	0;	

			}
			else
			{
				$document_file_name = 	safe_sql_data($connection, $_POST['document_file']);
				$image_file_name 	= 	safe_sql_data($connection, $_POST['image_file']);	
				$video_file_name 	=	safe_sql_data($connection, $_POST['video_file']);
				$audio_file_name 	= 	safe_sql_data($connection, $_POST['audio_file']);

				if (isset($document_file_name) && strlen($document_file_name) > 0) {
					$media_type = 'document';
					$document_file_url 		=	'uploading';
					$document_file_extension=	'uploading';
					$file_status 			=	-1;
				}
				
				else if (isset($image_file_name) && strlen($image_file_name) > 0) {
					$media_type = 'image';
					$image_file_url 		=	'uploading';
					$image_file_extension	=	'uploading';
					$file_status 			=	-1;	
				}
				
				else if (isset($video_file_name) && strlen($video_file_name) > 0) {
					$media_type = 'video';
					$video_file_url 		=	'uploading';
					$video_file_extension	=	'uploading';
					$file_status 			=	-1;	
				}
				
				else if (isset($audio_file_name) && strlen($audio_file_name) > 0) {
					$media_type = 'audio';
					$audio_file_url 		=	'uploading';
					$audio_file_extension	=	'uploading';
					$file_status 			=	-1;	
				}
			}
			
			if ($action == 'new_thread') { 
				// Create the Discussion Thread
				$queryInsertDiscussionThread = '
				INSERT INTO discussion_threads (title, text, file_document_url, file_document_type, file_image_url, file_image_type, file_video_url, file_video_type, file_audio_url, file_audio_type, status, session_id, project_id, project_program_id, assessment_id, blog_post_id, brand_product_usage_id, course_id, topic_id, web_page_id, recipe_id, resource_id, created_by, created_timestamp, modified_by, modified_timestamp, file_status, file_type) 
				VALUES (
				'.no_value_null_check($thread_title).', 
				"'.$thread_text.'",
				'.no_value_null_check($document_file_url).', 
				'.no_value_null_check($document_file_extension).', 
				'.no_value_null_check($image_file_url).', 
				'.no_value_null_check($image_file_extension).', 
				'.no_value_null_check($video_file_url).', 
				'.no_value_null_check($video_file_extension).', 
				'.no_value_null_check($audio_file_url).', 
				'.no_value_null_check($audio_file_extension).', 
				"S",
				"'.$_SESSION['userStatistics']['session_id'].'", 
				"'.$_SESSION['enrollment']['project_id'].'", 
				"'.$_SESSION['enrollment']['project_program_id'].'", 
				'.no_value_null_check($assessment_id).',
				'.no_value_null_check($blog_post_id).', 
				'.no_value_null_check($brand_product_usage_id).', 
				'.no_value_null_check($course_id).',
				'.no_value_null_check($topic_id).', 
				'.no_value_null_check($web_page_id).', 
				'.no_value_null_check($recipe_id).', 
				'.no_value_null_check($resource_id).',
				'.no_value_null_check($thread_user_id).', 
				"'.$created_timestamp.'",
				'.no_value_null_check($thread_user_id).', 
				"'.$modified_timestamp.'", 
				'.no_value_null_check($file_status).',
				'.no_value_null_check($media_type).')';	

				$result_insert_thread = mysqli_query($connection, $queryInsertDiscussionThread);
				if (!$result_insert_thread) {
					show_mysqli_error_message($queryInsertDiscussionThread, $connection);
					die;
				}
				
				// Get the insert_id of the record just created in the database for the return URL hashtag.
				$last_insert_id 		= 	mysqli_insert_id($connection); 
			    $responseMessageType 	= 	'alert-box success radius';				
				$responseMessage 		= 	'<p><i class="fa fa-check fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Your <b>Comment</b> was posted.</p>';
			} 
			elseif ($_POST['action'] == 'new_reply') 
			{
				// Create the Discussion Thread Reply
				$queryInsertDiscussionReply = '
				INSERT INTO discussion_replies (discussion_thread_id, text, status, file_document_url, file_document_type, file_image_url, file_image_type, file_video_url, file_video_type, file_audio_url, file_audio_type, created_by, created_timestamp, file_status, file_type) 
				VALUES (
				"'.$thread_reply_id.'",	
				"'.$thread_reply_text.'", 
				"S", 
				'.no_value_null_check($document_file_url).', 
				'.no_value_null_check($document_file_extension).', 
				'.no_value_null_check($image_file_url).', 
				'.no_value_null_check($image_file_extension).', 
				'.no_value_null_check($video_file_url).', 
				'.no_value_null_check($video_file_extension).', 
				'.no_value_null_check($audio_file_url).', 
				'.no_value_null_check($audio_file_extension).', 
				"'.$thread_reply_user_id.'",
				"'.$created_timestamp.'",
				'.no_value_null_check($file_status).',
				'.no_value_null_check($media_type).')';
					
				$result_insert_thread_reply = mysqli_query($connection, $queryInsertDiscussionReply);
				
				if (!$result_insert_thread_reply) {
					show_mysqli_error_message($queryInsertDiscussionReply, $connection);
					die;
				}
				
				// Get the insert_id of the record just created in the database for the return URL hashtag.
				$last_insert_id = mysqli_insert_id($connection); 
							
				// Update the Discussion Thread modified_timestamp
				$queryUpdateDiscussionThread = '
					UPDATE discussion_threads SET 
					modified_timestamp = "'.$created_timestamp.'" 
					WHERE id = "'.$thread_reply_id.'"';
				
				$result_update_thread = mysqli_query($connection, $queryUpdateDiscussionThread);
				
				if (!$result_update_thread) {	
					show_mysqli_error_message($queryUpdateDiscussionThread, $connection);
					die;
				}
				
			    $responseMessageType 	= 	'alert-box success radius';				
				$responseMessage 		= 	'<p><i class="fa fa-check fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Your <b>Reply</b> was posted.</p>';	
			}
		}
	}


	if(isset($_SESSION['captcha_verified']) && $_SESSION['captcha_verified'] == true) 
	{
		$json['captcha_status'] = 	1;
		if($ie9 && (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') )
		{
			$json			=	json_encode($json);
			die($json);
		}

		createPost($connection, $ie9);

		unset($_SESSION['temp']);

		$json['message_type']		=	$responseMessageType;
		$json['message']			=	$responseMessage;	
		$json['id']					=	$last_insert_id;
		$json['title']				=	$thread_title == null?'':$thread_title;
		$json['thread_text']		=	$thread_text;
		$json['thread_reply_id'] 	=	$thread_reply_id;
		$json['thread_reply_text']	=	$thread_reply_text == null ? '' : $thread_reply_text;
		$json['created_timestamp'] 	= 	date('m/d/y h:i A T', strtotime($created_timestamp));
		$json['file_document_url'] 	=	$document_file_url;
		$json['file_document_type'] =	$document_file_extension;
		$json['file_image_url']		=	$image_file_url;
		$json['file_image_type']   	=	$image_file_extension;
		$json['file_video_url']		=	$video_file_url;
		$json['file_video_type']	=	$video_file_extension;
		$json['file_audio_url']		=	$audio_file_url;
		$json['file_audio_type']	=	$audio_file_extension;
		$json['file_status']		=	$file_status;
		$json['file_type']			=	$media_type;
		$json['status']				=	'S';

		//Email System Administration copy of post
		$message 	= 	generate_forum_post_email_notification($connection, $post_type, $last_insert_id);
		$from 		= 	$_SESSION['application']['email'];
		$to 		= 	$_SESSION['application']['email'];
		$subject 	= 	$forum_subject;
				   	   
		send_email_html($from, $to, $subject, $message, "");

		mysqli_close($connection);
	}

	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
	{
		$json						=	json_encode($json);
		echo $json;
	}
	else
	{
		//header("location: ".$_SESSION['target_uri']);
		$_SESSION['message_type'] 	= 	$responseMessageType;
		$_SESSION['message']		=	$responseMessage;
		header("location: ".$_SERVER['HTTP_REFERER']."#fndtn-discuss");
		exit();	
	}
?>