<div id ="e-commerce-products-header" class="">
		
	<div class="row">
		    	        
        <!-- Why Choose NuMedica -->
        <div class="small-12 medium-3 columns show-for-medium-up">       	
            <!-- <center><span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Why should you choose NuMedica professional-grade supplements? NuMedica's formulas are used by leading Healthcare Providers and Nutritionists to promote Wellness with their patients and clients. Learn more..."><a href="../why-choose-numedica" ><img id="eCommerceHeaderLogoImage" src="https://cdn-manager.net/media/images/numedica-logo-landscape-medium.png" alt="NuMedica Advancing Nutrition logo"></a></span></center> -->
            <center><a href="../why-choose-numedica" title="Why should you choose NuMedica professional-grade supplements? NuMedica's formulas are used by leading Healthcare Providers and Nutritionists to promote Wellness with their patients and clients. Learn more..."><img id="eCommerceHeaderLogoImage" src="https://cdn-manager.net/media/images/numedica-logo-landscape-medium.png" alt="NuMedica Advancing Nutrition logo"></a></center>
        </div>
        
        <div class="small-12 medium-9 columns">
	        
	        <!-- H1 Title -->
	        <div class="small-12 columns show-for-medium-up">
			    <span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="SupplementRelief has been a trusted NuMedica distributor online since October of 2010. We have first-hand, practical knowledge using NuMedica's formulas to help people Achieve Wellness and Live Better. Call or email anytime if you need some help determining which NuMedica supplements are right for you. We enjoy talking with you and will spend as much time as is needed."><h1 id="productsH1Title"><i class="fa fa-hand-o-right" aria-hidden="true"></i>&nbsp;&nbsp;NuMedica Specialists with Wellness Expertise & Quality Service</h1></span>
	        </div>       	
 
	        <!-- Phone Orders -->
			<div class="small-12 medium-4 columns">
				<!-- <h4 style="color: #5CB64B;"><i class="fa fa-phone fa-lg"></i>&nbsp;&nbsp;<b>Orders (888) 424-0032</b></h4> -->
				<div><center><h5 style="margin-top: 3px; color: red;"><i class="fa fa-phone fa-lg "></i>&nbsp;&nbsp;<b>Orders (888) 424-0032</b></h5></center></div>
			</div>     
			
			<!-- Member Account, Shopping Cart, Order Policy -->			
			<div class="small-12 medium-4 columns">
				
				<div style="margin-top: 3px;">
					<center>
					
						<!-- Register Member Account -->
						<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Register for your FREE Member Account and get Purchase Savings, Promotional Offers, and additional Expert Wellness Education!"><a href="../register-member-account/"><i class="fa fa-money fa-lg icon-blue">&nbsp;&nbsp;</i>$AVE</a></span>&nbsp;&nbsp;
						
						<!-- Shopping Cart -->
						<?php echo integrate_view_cart_button(array('store_path' => '../'.STORE_FOLDER_NAME.'/',
																	'button_html_content' => '<i class="fa fa-shopping-cart fa-lg icon-blue"></i>&nbsp;&nbsp;Cart')); ?>&nbsp;&nbsp;
																						
						<a href="javascript:void(0)" data-reveal-id="shippingHandling" rel="self" title="View Order Processing, Shipping & Handling, Return & Refund Policy"><i class="fa fa-truck fa-lg icon-blue"></i>&nbsp;&nbsp;Shipping</a>	
								
						<!-- <a href="javascript:void(0)" data-reveal-id="showPromotions" rel="self" title="view Promotions"><i class="fa fa-usd fa-lg"></i>&nbsp;Promotions</a> -->
						<!-- <a id="toggleStorePromotions" title="view Store Promotions"><i class="fa fa-usd fa-lg"></i>&nbsp;Promotions</a> -->
						<!-- <a href="../promotions/" title="view Store Promotions"><i class="fa fa-usd fa-lg"></i>&nbsp;Promotions</a> -->
						
					</center>
				</div>
			
			</div>
			
			<!-- Credit Card Image Strip -->
	        <div class="small-12 medium-4 columns show-for-medium-up">
				
				<center><img style="margin-top: 3px;" title="We accept Visa, Mastercard, Discover and American Express credit cards." src="https://cdn-manager.net/media/images/ecommerce-credit-card-strip.png" alt="credit cards accepted include Visa, MasterCard, Discover and American Express" width="200"></center>				
			
			</div>
			
        </div>
								
	</div>
	
</div>