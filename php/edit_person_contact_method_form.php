<?php

// echo 'Begin edit_person_contact_method_form.php.<br />';

include_once('../includes/header.php');

// show_array($_POST);
/*
if (isset($_POST['action']) && $_POST['action'] == 'edit') {
	// set POST return values to local variables	
	$contact_method_id = safe_sql_data($connection, $_POST['contact_method_id']);		
}

$queryPersonContactMethod	= 'SELECT 
	pcm.id AS contact_method_id, 
	pcm.person_id, 
	pcm.contact_method_type_code,  
	cmt.name AS contact_method_type_name,
	pcm.contact_method_value, 
	pcm.effective_date, 
	pcm.end_date  
	FROM person_contact_methods pcm 
	LEFT JOIN contact_method_types cmt ON pcm.contact_method_type_code = cmt.code 
	WHERE pcm.id = "'.$contact_method_id.'"';   
			
// echo $queryPersonContactMethod . '<br /><hr />';

$result_person_contact_method = mysqli_query($connection, $queryPersonContactMethod);

if (!$result_person_contact_method) {
	show_mysqli_error_message($queryPersonContactMethod, $connection);
	die;
}

while($r = mysqli_fetch_assoc($result_person_contact_method)) {

	// show_array($r);
	
	$contact_method_id = $r['contact_method_id'];
	$person_id = $r['person_id'];	
	$contact_method_type_code = $r['contact_method_type_code'];	
	$contact_method_type_name = $r['contact_method_type_name'];
	$contact_method_value = $r['contact_method_value'];
	$effective_date = hdate($r['effective_date']);
	$end_date = hdate($r['end_date']);
		
}

mysqli_free_result($result_person_contact_method);
*/

// Build the Select List for Contact Method Types

$queryContactMethodTypes = 'SELECT   
code, 
name 
FROM contact_method_types  
WHERE effective_date <= CURRENT_DATE 
AND (end_date is NULL or end_date >= CURRENT_DATE)  
ORDER BY name ASC';

// echo $queryContactMethodTypes;
// die();
	
// Execute the query
$result_contact_method_type = mysqli_query($connection, $queryContactMethodTypes);

if (!$result_contact_method_type) {
	show_mysqli_error_message($queryContactMethodTypes, $connection);
	die;
}
							
$contact_method_type_options = '<option value="">Select Contact Method...</option>';

while($contact_method_type_row = mysqli_fetch_array($result_contact_method_type)) {

	$selected = '';
	if(($contact_method_type_code ? $contact_method_type_code : $row1['p']) == $contact_method_type_row['code']) $selected = ' selected';
	$contact_method_type_options .= '<option value="'.$contact_method_type_row['code'].'" '.$selected.'>'.$contact_method_type_row['name'].'</option>';
}
				
mysqli_free_result($result_contact_method_type);

?>
				
<form data-abide id="updatePersonContactMethod" action="../php/person_contact_method_crud.php" method="post">

	<input type="hidden" id="updatePersonContactMethodAction" name="action" value="edit"/>
	<input type="hidden" id="editContactMethodId" name="contact_method_id" value="<?php echo $contact_method_id; ?>"/>
	<!--<input type="hidden" name="person_id" value="<?php echo $person_id; ?>"/>	-->

		<fieldset>
    	<legend>Edit Contact Method</legend>
        		
		<div class="row">
			
			<div class="small-12 medium-4 columns">
				<label>Contact Method <small>required</small>
			  	<select id="editPersonContactMethodSelect" required name="contact_method_type_code">
			        <?php echo $contact_method_type_options; ?>
			  	</select>
			  </label>
			  <small class="error">Contact Type is required</small>
			</div>
			
			<div class="small-12 medium-8 columns">
				<label>Value <small>Required</small>
				  	<input type="text" id="editPersonContactMethodValue" required name="contact_method_value" maxlength="100" value="<?php echo ($contact_method_value ? $contact_method_value : ''); ?>" />  	
			 	</label>
			 	<small class="error">Contact Method Value is required</small>
			</div>  	    	  	    	
			
		</div>
		
		<div class="row">								
			<div class="small-12 columns">
					<div id="editPersonContactMethodDateErrorMessage" style="display:none;"></div>		
			</div>
		</div>

												
		<div class="row">
								
			<div class="small-12 medium-3 columns">
				<label>Effective Date <small>Required</small>
					<input type="text" class="datepicker_date maskDateField" id="editPersonContactMethodEffectiveDate" required name="effective_date" placeholder="MM/DD/YYYY" value="<?php echo ($effective_date ? $effective_date : ''); ?>" />
				</label>
				<small class="error">Effective Date is required and must be formatted as MM/DD/YYYY</small> 	  
			</div>
			
			<div class="small-12 medium-3 columns end">
				<label>End Date
					<input type="text" class="datepicker_date maskDateField" id="editPersonContactMethodEndDate" name="end_date" placeholder="MM/DD/YYYY" value="<?php echo ($end_date ? $end_date : ''); ?>" />
				</label>
				<small class="error">End Date must be formatted as MM/DD/YYYY</small> 	  
			</div>				
			
		</div>	
		
		<div class="row">
			<div class="small-12 columns">
		  	<label>	      
		    	<input id="editPersonContactMethodSubmit" type="button" class="button small radius" value="Update">
		    	
		    	<a href="#" class="button small radius" data-reveal-id="person_contact_method_delete_confirmation_modal"><b>Delete</b></a>
		    	<a href="#" class="button small radius" onclick="editPersonContactMethodCancel();return false"><b>Cancel</b></a>
		    	<!-- <a id="editPersonContactMethodCancel" href="#" class="button small radius"><b>Cancel</b></a>	 -->     
		    </label>
		  </div>
		</div>
					
  	</fieldset>
  	
  	<div id ="person_contact_method_delete_confirmation_modal" class="reveal-modal small" data-reveal>
  		<div class="row" >
  			<div class="small-12 columns" style="text-align:center" >
  				<p>Are you sure you want to DELETE this Contact Method?</p>
  			</div>
  		</div>
  		<div class="row" >
  			<div class="small-6 columns" >
  				<input class="tiny button radius right" type="button" id="deleteContactMethodYes" value="Yes" />
  			</div>	
  			<div class="small-6 columns" >
  				<input class="tiny button radius left" type="button" id="deleteContactMethodNo" value="No" /> 
  			</div>
  		</div>
  		<a class="close-reveal-modal">&#215;</a>
  	</div>
  		
</form>