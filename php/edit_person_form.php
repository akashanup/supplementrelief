<?php

// $_SESSION['user']['user_id'] = $row['id']; // from login_process.php

$queryPerson = 'SELECT 
	pe.id, 
	pe.first_name, 
	pe.last_name, 
	pe.email, 
	pe.phone, 
	pe.birth_date, 
	pe.gender, 
	pe.username  
	FROM persons pe
	WHERE pe.id = "'.$_SESSION['user']['user_id'].'" LIMIT 1'; 
			
// echo $queryPerson . '<br /><hr />';

$result_person = mysqli_query($connection, $queryPerson);

if (!$result_person) {
	show_mysqli_error_message($queryPerson, $connection);
	die;
}

while($row = mysqli_fetch_assoc($result_person)) {

	$person_id = $row['id'];
	$person_first_name = $row['first_name'];
	$person_last_name = $row['last_name'];
	$person_email = $row['email'];
	$person_phone = $row['phone'];
	$person_birth_date = hdate($row['birth_date']);
	$person_gender = $row['gender'];
	$person_username = $row['username'];
	
    // echo 'First Name: '.$person_first_name.'<br />';

}

mysqli_free_result($result_person);

?>

<div id="editPerson">

	<div class="row">			
		<div class="small-12 columns">

			<form data-abide action="../php/person_crud.php" method="post" autocomplete="off">

				<input type="hidden" name="action" value="edit"/>

				<input type="hidden" name="person_id" value="<?php echo ($person_id ? $person_id : ''); ?>"/>
	
				<fieldset>
				  <legend>Update Account</legend>
				      					
					<div class="row">
								
						<div class="small-12 medium-6 columns">	
								
					  		<label>User ID
					    		<input type="text" readonly name="person_username" id="person_username" value="<?php echo ($person_username ? $person_username : ''); ?>" />
					  		</label>
					  		<small class="error">User ID is required</small>
					  								  		
						  	<a href="#" data-reveal-id="changeUserID" onclick="setModalData();">Change User ID</a>&nbsp;|&nbsp;<a href="#" data-reveal-id="changePassword" onclick="focusPassword();">Change Password</a>
							<br><br>
					  							  							  							  							  		
						</div> 
							 		
						<div class="small-12 medium-6 columns"> 		
					  		<label>Email
					    		<input type="text" readonly name="person_email" value="<?php echo ($person_email ? $person_email : ''); ?>" />
					  		</label>
					  		<small class="error">Account Email is required</small>
												  		
					  		<a href="#" data-reveal-id="changeEmail" onclick="focusEmail();">Change Email</a>
							<br><br>
				  									
						</div> 
						 		
					</div>

					<div class="row">
								
						<div class="small-12 medium-6 columns">		
					  		<label>First Name <small>Required</small>
					    		<input type="text" required name="first_name" placeholder="Jane" value="<?php echo ($person_first_name ? $person_first_name : ''); ?>" />
					  		</label>
					  		<small class="error">First Name is required</small>    
						</div> 
						 		
						<div class="small-12 medium-6 columns"> 		
				  			<label>Last Name <small>Required</small>
				    			<input type="text" required name="last_name" placeholder="Doe" value="<?php echo ($person_last_name ? $person_last_name : ''); ?>" />
							</label>
							<small class="error">Last Name is required</small>	    
						</div>
							 		
					</div>
																		
					<div class="row">
						
						<div class="small-12 medium-4 columns">
							<label>Phone <small>optional</small>
								<input type="tel" name="phone" placeholder="(555) 555-1212" value="<?php echo ($person_phone ? $person_phone : ''); ?>" />			
							</label>
							<small class="error">Phone must be formatted as (555) 555-1212 or 555-555-1212</small> 	  
						</div>
					
						<div class="small-12 medium-4 columns">
							<label>Gender <small>optional</small></label> 
						  		<input  type="radio" name="gender" value="F" <?php if($person_gender == 'F') echo 'checked'; ?> id="genderFemale"><label for="genderFemale">Female</label>
						  		<input  type="radio" name="gender" value="M" <?php if($person_gender == 'M') echo 'checked'; ?> id="genderMale"><label for="genderMale">Male</label>
						  		<small class="error">Choose Female or Male</small>
						</div>
												
						<div class="small-12 medium-4 columns">
							<label>Birth Date <small>optional</small>
								<input type="text" class="datepicker_date" name="birth_date" placeholder="MM/DD/YYYY" value="<?php echo ($person_birth_date ? $person_birth_date : ''); ?>" />
							</label>
							<small class="error">Birth Date is optional and must be formatted as MM/DD/YYYY</small> 	  
						</div>
						
					</div>	
														
					<div class="row">
					  <div class="small-12 columns">
					    <label>	      
					      <input class="button medium radius show-for-medium-up expand" type="submit" value="Update Account">
					      <input class="button medium radius expand show-for-small-only" type="submit" value="Update Account">      
					    </label>
					  </div>
					  
					  <div class="small-12 columns">
					  	<div class="right">
						  	<a href="#" title="view our Privacy Policy" data-reveal-id="footer-privacy-policy"><i class="fa fa-lock" aria-hidden="true"></i>&nbsp;Privacy Policy</a>&nbsp;&nbsp;
						</div> 			
					  </div>  
					  
					</div>	
								
				</fieldset>
				
			</form>
		</div>
	</div>	
</div>

<?php $resetcode = md5(uniqid(mt_rand(), true)); ?>

<script type="text/JavaScript" src="../js/sha512.js"></script> 
<script type="text/JavaScript" src="../js/forms.js"></script>

<script type="text/javascript">

	function doChangeUserIDLocal() {	
		
		form_error = test_user_id($("#user_id").val());
		// test_user_id is in forms.js and checks for:
		// 1) min length
		// 2) specific characters
		
		if ( form_error == '' ) {
			
			// alert("About to call script ../php/change_username_local.php");
			// console.log('No errors with Change User ID on Update Account form. Now proceed to ...')	
			
			// If no error then call change_username_local.php with action, person_id, username.
						
			$("#callbackUserID").load('../php/change_username_local.php', {
				'action': $("#actionUserId").val(),
				'person_id': '<?php echo $person_id; ?>',
				'username': $("#user_id").val() },
				function(response) {
			  $("#callbackUserID").html(response);
			});
			document.getElementById("person_username").value = $("#user_id").val();
			return;
			
		} else {
			// alert(form_error);
			$("#callbackUserID").html(form_error + '<br><br><input class="button medium radius expand" type="submit" value="CHANGE">'); 
			$("#user_id").focus();
			return; 
		}
	
	}

	function setModalData(){
		$("#user_id").val('');
		$("#callbackUserID").html('<input class="button medium radius expand" type="submit" value="CHANGE">');
		setTimeout(function(){$("#user_id").focus();},1000);
	}
	function focusEmail(){
		setTimeout(function(){$("#user_email").focus();},1000);
	}
	function focusPassword(){
		setTimeout(function(){$("#pass").focus();},1000);
	}

</script>
	
<div id="changeUserID" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<!-- <form data-abide action="../php/change_username.php" method="post" name="change_username_form" id="change_username_form"> -->
	<form data-abide onsubmit="doChangeUserIDLocal(); return false;">
				
		<input type="hidden" name="cp" value="<?php echo $_SESSION["token"]; ?>">
		<input type="hidden" name="action" value="change" id="actionUserId" />
		<input type="hidden" name="person_id" value="<?php echo ($person_id ? $person_id : ''); ?>"/>
	
		<fieldset>	  
	    	<legend>Change User ID</legend>
	    	    		
			<div class="row">			
				<div class="small-12 columns">
			    	<label>User ID
						<input type="text" required name="username" id="user_id" placeholder="janedoe (or can be same as Email)" value="" />
					</label>
					<small class="error">Username is required</small>    
			  	</div>
			</div>
			 			
			<div class="small-12 columns">	  
			    <label>	      
			    	<div id="callbackUserID">
				    	<input class="button medium radius expand" type="submit" value="CHANGE"" />
				    </div>      
			    </label>    	    
			</div>
						
	  	</fieldset>
	  		
	</form>
	
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	
</div>

<script type="text/javascript">

	function doChangePassLocal() {	
		
		form_error = test_pass($("#pass").val(), $("#confpass").val());
		// test_pass is in forms.js and checks for 1) min length 6, 2) 1 number, 1 lowercase letter, 1 uppercase letter, 3) password = confirm password
		
		if ( form_error == '') {
			
			// console.log('No errors with Reset Password entered values on Update Account form. Now proceed to prat.php.')	
			
			// If no error then call prat.php with user_id, temporary password, hex_sha512 version of user's requested password.	
			
			$("#callbackChangePassword").load('../php/prat_local.php', {
				'si': '<?php echo $person_id; ?>',
				'pr': '<?php echo $resetcode; ?>',
				'p': hex_sha512($("#pass").val())},
				function(response) {
			  $("#callbackChangePassword").html(response);
			});
			return;
			
		} else {
			// alert(form_error);
			$("#callbackChangePassword").html(form_error + '<br><br><input class="button medium radius expand" type="button" value="CHANGE" onClick="doChangePassLocal();">'); 
			$("#pass").focus();
			return; 
		}
	
	}

</script>

<div id="changePassword" class="reveal-modal small" data-reveal>
	
	<form data-abide action="../php/reset_password_process.php" method="post" />	
	  	<fieldset>	  
	    	<legend>Change Password</legend>
	    		    		
			<div class="row">
			
				<div class="small-6 columns">
			    	<label>New Password
						<input type="password" required name="pass" placeholder="********" id="pass" value="" />
					</label>
					<small class="error">New Password is required</small>    
			  	</div>
			  
			  	<div class="small-6 columns">
			    	<label>Confirm Password
						<input type="password" required name="confpass" placeholder="********" id="confpass" value="" />
					</label>
					<small class="error">Confirm Password is required</small>	    
			  	</div>
			  
			</div>
						
			<div class="small-12 columns">	  
			    <label>	      
			    	<div id="callbackChangePassword">
				    	<input class="button medium radius expand" type="button" value="CHANGE" onClick="doChangePassLocal();" />
				    </div>      
			    </label>    	    
			</div>
			  						
	  	</fieldset>		
	</form>
		
	<a class="close-reveal-modal">&#215;</a>
	
</div>

<script type="text/javascript">

	function doChangeEmailLocal() {	
		
		form_error = test_user_email($("#user_email").val());
		// test_user_email is in forms.js
		
		if ( form_error == '' ) {
			
			// console.log('No errors with Change User Email on Update Account form. Now proceed to ...');	
			
			// If no error then call change_useremail_local.php with action, person_id, email.
						
			$("#callbackChangeEmail").load('../php/change_user_email_local.php', {
				'action': $("#actionEmail").val(),
				'person_id': '<?php echo $person_id; ?>',
				'user_email': $("#user_email").val() },
				function(response) {
			  $("#callbackChangeEmail").html(response);
			});
			// document.getElementById("person_username").value = $("#user_id").val();

			return;
			
		} else {
			// alert(form_error);
			$("#callbackChangeEmail").html(form_error + '<br><br><input class="button medium radius expand" type="button" value="CHANGE" onClick="doChangeEmailLocal();">'); 
			$("#user_email").focus();
			return; 
		}
	
	}

</script>

<div id="changeEmail" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<!-- <form data-abide action="../php/change_email.php" method="post" name="change_email_form" id="change_email_form"> -->
	<form data-abide method="post" name="change_email_form" id="change_email_form">
				
		<input type="hidden" name="cp" value="<?php echo $_SESSION["token"]; ?>">
		<!-- <input type="hidden" name="action" value="change"/> -->
		<input type="hidden" name="action" value="change" id="actionEmail" />
		<input type="hidden" name="person_id" value="<?php echo ($person_id ? $person_id : ''); ?>"/>
		<!-- <input type="hidden" name="query_string" value="<?php echo $_SERVER['QUERY_STRING']; ?>"> -->
	
		<fieldset>	  
	    <legend>Change Email</legend>
	    	    		
			<div class="row">			
			  <div class="small-12 columns">
			    <label>Email
			    	<input type="text" name="email" id="user_email" placeholder="someone@domain.com" value="" />
			    </label>
			    <small class="error">Email is required</small>    
			  </div>
			</div>
			  					
			<div class="small-12 columns">	  
			    <label>	      
			    	<div id="callbackChangeEmail">
				    	<input class="button medium radius expand" type="button" value="CHANGE" onClick="doChangeEmailLocal();" />
				    </div>      
			    </label>    	    
			</div>
						
	  	</fieldset>
	  		
	</form>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
