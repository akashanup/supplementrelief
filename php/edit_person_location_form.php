<?php

// echo 'Begin edit_person_location_form.php.<br />';

include_once('../includes/header.php');

if (isset($_POST['action']) && $_POST['action'] == 'edit') {
	$address_id = safe_sql_data($connection, $_POST['address_id']);		
	$location_id = safe_sql_data($connection, $_POST['location_id']);
}

$queryLocation	= 'SELECT 
	lo.id AS location_id, 
	lo.address_type_code,  
	lo.person_id, 
	lo.effective_date AS location_effective_date, 
	lo.end_date AS location_end_date, 
	at.name AS address_type_name, 
	ad.id AS address_id, 
	ad.address, 
	ad.city, 
	ad.state_id, 
	ad.postal_code, 
	ad.country_id  
	FROM locations lo 
	LEFT JOIN address_types at ON lo.address_type_code = at.code 
	LEFT JOIN addresses ad ON lo.address_id = ad.id 
	LEFT JOIN states st ON ad.state_id = st.state_id 
	LEFT JOIN country cou ON ad.country_id = cou.country_id
	WHERE lo.id = "'.$location_id.'"   
	LIMIT 1';
			
// echo $queryLocation . '<br /><hr />';

$result_list_person_location = mysqli_query($connection, $queryLocation);

if (!$result_list_person_location) {
	show_mysqli_error_message($queryLocation, $connection);
	die;
}

// echo 'Database Query List Person Location succeeded.<br /><hr />';	

while($r = mysqli_fetch_assoc($result_list_person_location)) {

	$address_id = $r['address_id'];
	$location_id = $r['location_id'];
	$location_address_type_code = $r['address_type_code'];
	$location_address = $r['address'];
	$location_city = $r['city'];
	$location_state_id = $r['state_id'];
	$location_postal_code = $r['postal_code'];
	$location_country_id = $r['country_id'];
	$location_effective_date = hdate($r['location_effective_date']);	
	$location_end_date = hdate($r['location_end_date']);						

}

// Build the Select List for Address Type

$address_type_query = 'SELECT  
code, 
name AS address_type_name 
FROM address_types
WHERE code in ("BILL", "SHIP")  
ORDER BY address_type_name ASC';

// echo $address_type_query .'<br /><hr />';

$address_type_result = mysql_query($address_type_query);
$address_type_options = '<option value="">Select Address Type...</option>';

while($address_type_row = mysql_fetch_array($address_type_result)) {
	$selected = '';
	if(($location_address_type_code ? $location_address_type_code : $row1['p']) == $address_type_row['code']) $selected = ' selected';
	$address_type_options .= '<option value="'.$address_type_row['code'].'" '.$selected.'>'.$address_type_row['address_type_name'].'</option>';
}

mysql_free_result($address_type_result);

// Build the Select List for States
$queryStates = '
SELECT   
state_id, 
state_abbr, 
state_name, 
country_abbr   
FROM states 
ORDER BY country_abbr DESC, state_name ASC';

// echo $queryStates;
// die();

// Execute the query
$result_state = mysqli_query($connection, $queryStates);

if (!$result_state) {
	show_mysqli_error_message($queryStates, $connection);
	die;
}
							
$state_options = '<option value="">Select State...</option>';

while($state_row = mysqli_fetch_array($result_state)) {

	$selected = '';
	if(($location_state_id ? $location_state_id : $row1['p']) == $state_row['state_id']) $selected = ' selected';
	// $state_options .= '<option value="'.$state_row['state_abbr'].'" '.$selected.'>'.$state_row['state_name'].' ('.$state_row['state_abbr'].') - '.$state_row['country_abbr'].'</option>';
	$state_options .= '<option value="'.$state_row['state_id'].'" '.$selected.'>'.$state_row['state_name'].' ('.$state_row['state_abbr'].') - '.$state_row['country_abbr'].'</option>';

}
				
mysqli_free_result($result_state);

// Build the Select List for Countries
$queryCountries = '
SELECT   
country_id, 
country_abbr, 
country_name 
FROM country  
ORDER BY country_name ASC';

// echo $queryCountries;
// die();

// Execute the query
$result_country = mysqli_query($connection, $queryCountries);

if (!$result_country) {
	show_mysqli_error_message($queryCountries, $connection);
	die;
}
							
$country_options = '
<option value="">Select Country...</option>';

while($country_row = mysqli_fetch_array($result_country)) {

	$selected = '';
	if(($location_country_id ? $location_country_id : $row1['p']) == $country_row['country_id']) $selected = ' selected';
	$country_options .= '<option value="'.$country_row['country_id'].'" '.$selected.'>'.$country_row['country_name'].' ('.$country_row['country_abbr'].')</option>';
}
				
mysqli_free_result($result_country);

?>

<script>
$( ".datepicker_edit_location_date" ).datepicker({
   changeMonth: true,
   changeYear: true
 }); 	
</script>

<div>
	<form data-abide class="update_address" action="../php/user_dashboard_person_location_crud.php" method="post" >
		<input type="hidden" name="action" id="update_address_action" value="edit" />
		<input type="hidden" name="address_id" value="<?php echo ($address_id ? $address_id : ''); ?>"/>
		<input type="hidden" name="location_id" value="<?php echo ($location_id ? $location_id : ''); ?>"/>
	
		<fieldset>
	    	<legend>Edit Address</legend>
	        		
			<div class="row">
				<div class="small-12 medium-6 columns">
				  	<label>Address Type <small>Required</small>
				    	<select name="address_type_code" required>
				          <?php echo $address_type_options; ?>
				    	</select>
				    	<small class="error">Address Type is required</small>
				  	</label>
				</div>
			</div>
								
			<div class="row">
				
		  		<div class="small-12 medium-6 columns">
			  		
	  				<label>Address <small>Required</small>
	  					<textarea required name="address" placeholder="2501 Easy St." rows="5"><?php echo ($location_address ? $location_address : ''); ?></textarea>
	  				</label>
	  				<small class="error">Address is required</small>		  			
		    	</div>
		    	
				<div class="small-12 medium-6 columns">
		    	 	<label>City <small>Required</small>
		    	 	  	<input type="text" required name="city" placeholder="Detroit" value="<?php echo ($location_city ? $location_city : ''); ?>" />  	
		    	  	</label>
		    	  	<small class="error">City is required</small>
		    	</div>
		    	
			</div>  	    	
		    		   		  		
			<div class="row">
					    		   			
				<div class="small-12 medium-4 columns">	
					    						    	
					<label>State <small>Required</small>
					<select required name="state_id">
						<?php echo $state_options; ?>
					</select>
					</label>
					<small class="error">State is required</small>				    	
			    	
			    </div>		    		    		
		    	
		    	<div class="small-12 medium-4 columns">
			    	<label>Postal Code <small>Required</small>
			    		  <input type="text" required name="postal_code" placeholder="48201, 48201-1234, K8N 5W6" value="<?php echo ($location_postal_code ? $location_postal_code : ''); ?>" /> 		  
			    	</label>
			    	<small class="error">Postal Code is required</small>	
			    </div>
			    
			    <div class="small-12 medium-4 columns">
			    
			    	<label>Country <small>Required</small>
			    		<select required name="country_id">
			    	      <?php echo $country_options; ?>
			    		</select>
			    	</label>
			    	<small class="error">Country is required</small>				    	
			    	
			    </div>
		    
			</div>
			
			<!--	
			<div class="row">
									
				<div class="small-12 medium-3 columns">
					<label>Effective Date
						<input type="text" class="datepicker_edit_location_date" required name="location_effective_date" placeholder="MM/DD/YYYY" value="<?php echo ($location_effective_date ? $location_effective_date : ''); ?>" />
					</label>
					<small class="error">Effective Date is required and must be formatted as MM/DD/YYYY</small> 	  
				</div>
				
				<div class="small-12 medium-3 columns end">
					<label>End Date
						<input type="text" class="datepicker_edit_location_date" name="location_end_date" placeholder="MM/DD/YYYY" value="<?php echo ($location_end_date ? $location_end_date : ''); ?>" />
					</label>
					<small class="error">End Date must be formatted as MM/DD/YYYY</small> 	  
				</div>				
				
			</div>
			-->	
			
			<div class="row">
			  <div class="small-12 columns"> 
			      <input class="button small radius" type="submit" value="Update" > 
			      <a href="#" class="button small radius" data-reveal-id="delete_confirmation_modal"><b>Delete</b></a>
			      <a id="update_address_cancel" href="javascript:void(0)" class="button small radius"><b>Cancel</b></a>
			  </div>
			<div>
					
		</fieldset>
	  	
		<div id ="delete_confirmation_modal" class="reveal-modal small" data-reveal>
			<div class="row" >
				<div class="small-12 columns text-center" >
					<p>Are you sure you want to <b>DELETE</b> this Address?</p>
				</div>
			</div>
			<div class="row" >
				<div class="small-6 columns" >
					<input class="tiny button radius right" type="button" id="deleteAddressYes" value="Yes" />
				</div>	
				<div class="small-6 columns" >
					<input class="tiny button radius left" type="button" id="deleteAddressNo" value="No" /> 
				</div>
			</div>
			<a class="close-reveal-modal">&#215;</a>
		</div>

	</form>
	
</div>