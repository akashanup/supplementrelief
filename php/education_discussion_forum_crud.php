<?php

// echo 'Begin education_discussion_forum_crud.php<br /><hr />';
// error_reporting(E_ALL);
// die;
// Called from education_discussion_forum.php
// DRY - Refactor this into reuseable code when time permits.
// Very similar to the script that processes new Comments/Replies for Blog Post, Products, Recipes and Resources Discussion Forum.

include_once('../includes/header.php');
	
// Set the current date and time for the database record audit trail.
$created_timestamp = date('Y/m/d H:i:s');
$modified_timestamp = $created_timestamp;

unset($_SESSION['topic']['discussion_forum']['thread_title']);
unset($_SESSION['topic']['discussion_forum']['thread_text']);
unset($_SESSION['topic']['discussion_forum']['thread_reply_text']);

if ($_POST['action'] == 'new_thread') { 
	
	// echo 'New Education Topic Discussion Forum Thread.<br />';

	// show_array($_POST);		
	// show_array($_FILES);	
	// die();
	
	$post_type = 'thread';
	
	// set POST return values	
	$thread_user_id = safe_sql_data($connection, $_POST['comment_user_id']);
	$thread_title = safe_sql_data($connection, $_POST['comment_title']);
	$thread_text = safe_sql_data($connection, $_POST['comment_text']);
	$bad_dog = safe_sql_data($connection, $_POST['comment_note']);
	// Field hidden by css used to reject submissions from robots. In addition to Google Recaptcha because the page can have many Reply forms and Google Recaptcha is expensive to load multiple times per page for each form.
	
	if (isset($bad_dog) && strlen($bad_dog) > 0) {
		die();
	}
	
	$document_file_name = $_FILES['document_file'] ['name'];
	$image_file_name = $_FILES['image_file'] ['name'];	
	$video_file_name = $_FILES['video_file'] ['name'];
	
	if (isset($document_file_name) && strlen($document_file_name) > 0) {
		$media_type = 'document';
		$documentFileArray = $_FILES['document_file'];
		list($document_file_name, $document_file_url, $document_file_extension) = uploadMedia($post_type, $media_type, $documentFileArray);	
	
		// echo 'Document File Name: '.$document_file_name.'<br>';
		// echo 'Document File URL: '.$document_file_url.'<br>';
		// echo 'Document File Extension: '.$document_file_extension.'<br>';
		// die;
		
	}
	
	if (isset($image_file_name) && strlen($image_file_name) > 0) {
		$media_type = 'image';
		$imageFileArray = $_FILES['image_file'];
		list($image_file_name, $image_file_url, $image_file_extension) = uploadMedia($post_type, $media_type, $imageFileArray);	
	
		// echo 'Image File Name: '.$image_file_name.'<br>';
		// echo 'Image File URL: '.$image_file_url.'<br>';
		// echo 'Image File Extension: '.$image_file_extension.'<br>';
		// die;
		
	}
	
	if (isset($video_file_name) && strlen($video_file_name) > 0) {
		$media_type = 'video';
		$videoFileArray = $_FILES['video_file'];
		list($video_file_name, $video_file_url, $video_file_extension) = uploadMedia($post_type, $media_type, $videoFileArray);	
	
		// echo 'Video File Name: '.$video_file_name.'<br>';
		// echo 'Video File URL: '.$video_file_url.'<br>';
		// echo 'Video File Extension: '.$video_file_extension.'<br>';
		// die;
		
	}
		
	// Create the Discussion Thread
				
	$queryInsertDiscussionThread = '
	INSERT INTO discussion_threads (title, text, file_document_url, file_document_type, file_image_url, file_image_type, file_video_url, file_video_type, status, session_id, project_id, project_program_id, course_id, topic_id, web_page_id, created_by, created_timestamp, modified_timestamp) 
	VALUES (
	"'.$thread_title.'",
	"'.$thread_text.'",
	'.no_value_null_check($document_file_url).', 
	'.no_value_null_check($document_file_extension).', 
	'.no_value_null_check($image_file_url).', 
	'.no_value_null_check($image_file_extension).', 
	'.no_value_null_check($video_file_url).', 
	'.no_value_null_check($video_file_extension).', 
	"S",
	"'.$_SESSION['userStatistics']['session_id'].'", 
	"'.$_SESSION['enrollment']['project_id'].'", 
	"'.$_SESSION['enrollment']['project_program_id'].'", 
	"'.$_SESSION['enrollment']['course_id'].'", 
	"'.$_SESSION['enrollment']['topic_id'].'", 
	"'.$_SESSION['enrollment']['web_page_id'].'",
	"'.$thread_user_id.'",
	"'.$created_timestamp.'",  
	"'.$created_timestamp.'")';	
		
	// echo $queryInsertDiscussionThread.'<br /><hr />';
	
	$result_insert_thread = mysqli_query($connection, $queryInsertDiscussionThread);
	
	if (!$result_insert_thread) {
		show_mysqli_error_message($queryInsertDiscussionThread, $connection);
		die;
	}
	
	// Get the insert_id of the record just created in the database for the return URL hashtag.
	$last_insert_id = mysqli_insert_id($connection); 
	$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#comment'.$last_insert_id;
	    
    $_SESSION['message_type'] = 'alert-box success radius';				
	$_SESSION['message'] = '<p><i class="fa fa-check fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Your <b>Discussion Thread</b> was posted.</p>';

} elseif ($_POST['action'] == 'new_reply') {

	// echo 'New Education Topic Discussion Forum Reply.<br />';
	
	// show_array($_POST);		
	// show_array($_FILES);	
	// die();
	
	$post_type = 'reply';
		
	// set POST return values
	$thread_reply_id = safe_sql_data($connection, $_POST['comment_reply_id']);
	$thread_reply_user_id = safe_sql_data($connection, $_POST['comment_reply_user_id']);
	$thread_reply_text = safe_sql_data($connection, $_POST['comment_reply_text']);
	$bad_dog = safe_sql_data($connection, $_POST['comment_note']);
	
	if (isset($bad_dog) && strlen($bad_dog) > 0) {
		die();
	}
	
	$document_file_name = $_FILES['document_file']['name'];
	$image_file_name = $_FILES['image_file']['name'];	
	$video_file_name = $_FILES['video_file']['name'];
	
	if (isset($document_file_name) && strlen($document_file_name) > 0) {
		$media_type = 'document';
		$documentFileArray = $_FILES['document_file'];
		list($document_file_name, $document_file_url, $document_file_extension) = uploadMedia($post_type, $media_type, $documentFileArray);			
	}
	
	if (isset($image_file_name) && strlen($image_file_name) > 0) {
		$media_type = 'image';
		$imageFileArray = $_FILES['image_file'];
		list($image_file_name, $image_file_url, $image_file_extension) = uploadMedia($post_type, $media_type, $imageFileArray);			
	}
	
	if (isset($video_file_name) && strlen($video_file_name) > 0) {
		$media_type = 'video';
		$videoFileArray = $_FILES['video_file'];
		list($video_file_name, $video_file_url, $video_file_extension) = uploadMedia($post_type, $media_type, $videoFileArray);			
	}
				
	// Create the Discussion Thread Reply
			
	$queryInsertDiscussionReply = '
	INSERT INTO discussion_replies (discussion_thread_id, text, status, file_document_url, file_document_type, file_image_url, file_image_type, file_video_url, file_video_type, created_by, created_timestamp) 
	VALUES (
	"'.$thread_reply_id.'",	
	"'.$thread_reply_text.'", 
	"S", 
	'.no_value_null_check($document_file_url).', 
	'.no_value_null_check($document_file_extension).', 
	'.no_value_null_check($image_file_url).', 
	'.no_value_null_check($image_file_extension).', 
	'.no_value_null_check($video_file_url).', 
	'.no_value_null_check($video_file_extension).', 
	"'.$thread_reply_user_id.'",
	"'.$created_timestamp.'")';
		
	echo $queryInsertDiscussionReply.'<br /><hr />';
	
	$result_insert_thread_reply = mysqli_query($connection, $queryInsertDiscussionReply);
	
	if (!$result_insert_thread_reply) {
		show_mysqli_error_message($queryInsertDiscussionReply, $connection);
		die;
	}
	
	// Get the insert_id of the record just created in the database for the return URL hashtag.
	$last_insert_id = mysqli_insert_id($connection); 
	$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#reply'.$last_insert_id;
				
	// Update the Discussion Thread modified_timestamp
	
	$queryUpdateDiscussionThread = '
		UPDATE discussion_threads SET 
		modified_timestamp = "'.$created_timestamp.'" 
		WHERE id = "'.$thread_reply_id.'"';
	
	// echo $queryUpdateDiscussionThread;
	
	$result_update_thread = mysqli_query($connection, $queryUpdateDiscussionThread);
	
	if (!$result_update_thread) {	
		show_mysqli_error_message($queryUpdateDiscussionThread, $connection);
		die;
	}
		    
    $_SESSION['message_type'] = 'alert-box success radius';				
	$_SESSION['message'] = '<p><i class="fa fa-check fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Your <b>Reply</b> was posted.</p>';

}


$message = generate_forum_post_email_notification($connection, $post_type, $last_insert_id);
	
$from = $_SESSION['application']['email'];
$to = $_SESSION['application']['email'];
$subject = 'Education Forum Post Notification';
		   	   
send_email_html($from, $to, $subject, $message, "");

mysqli_close($connection);
header("location: ".$_SESSION['target_uri']);
exit();	

?>