<?php

// echo 'Begin education_discussion_forum_crud.php<br /><hr />';
// die;
// Called from education_discussion_forum.php
// DRY - Refactor this into reuseable code when time permits.
// Very similar to the script that processes new Comments/Replies for Blog Post, Products, Recipes and Resources Discussion Forum.

include_once('../includes/header.php');
	
// Set the current date and time for the database record audit trail.
$created_timestamp = date('Y/m/d H:i:s');
$modified_timestamp = $created_timestamp;

$_SESSION['topic']['discussion_forum']['comment_title'] = null; // clear in case valued from previous Error Message user has already seen
$_SESSION['topic']['discussion_forum']['comment_text'] = null; // clear in case valued from previous Error Message user has already seen

// show_array($_POST);		
// show_array($_FILES);	
// die();

// create function for media upload


function uploadMedia($connection, $mediaFileArrays) {

	$document_file_name = $mediaFileArrays['document_file']['name'];
	$image_file_name = $mediaFileArrays['image_file']['name'];	
	$video_file_name = $mediaFileArrays['video_file']['name'];
	
	echo 'Document File Name: '.$document_file_name.'<br>';
	echo 'Image File Name: '.$image_file_name.'<br>';
	echo 'Video File Name: '.$video_file_name.'<br>';
	
	show_array($mediaFileArrays);
		
}
	
	// Upload Document file
	if (isset($document_file_name) && strlen($document_file_name) > 0) {
	
		// echo 'File Upload File Type Document: '.$document_file_name.'<br />';	

		$document_file_type = $_FILES['document_file'] ['type'];
		$tmp_document_file_name = $_FILES['document_file'] ['tmp_name'];
		$document_file_error = $_FILES['document_file'] ['error'];
		$document_file_size = $_FILES['document_file'] ['size'];
								
		$document_file_extension = strtolower(substr($document_file_type, strpos($document_file_type, '/') +1));		
		$unique_filename = strtolower(substr($tmp_document_file_name, strpos($tmp_document_file_name, '/tmp/') +5)).'.'.$document_file_extension;	
		echo 'Unique Filename: '.$unique_filename.'<br />';				
		
		// These defines are in /includes/defines.php.
		// define('user_media_domain', 'supplementrelief.com'); // Domain for User uploaded media
		// define('user_media_upload_directory', '/home/supplementrelief/public_html/media/'); // Discussion Forum uploaded documents, images and videos

		$database_document_file_url = 'https://'.user_media_domain.'/media/documents/user/'.$unique_filename;
				
		// Check for a valid document filetype.
		if ($document_file_extension == 'pdf' || $document_file_extension == 'txt') {
		
			// echo 'About to move Valid Document File: '.$database_document_file_url.'<br />';
		
			$directory = user_media_upload_directory.'/documents/user/';
						
			$destination = $directory.$unique_filename;	
			
			// echo $tmp_document_file_name.'<br>';
			// echo $destination;
			// die;		
										
			// If values are set and not empty move_upload_file is invoked to place the uploaded file into the web server directory identified by $directory.
								
			if (move_uploaded_file($tmp_document_file_name, $destination)) {
			
				// echo 'Valid Document File uploaded to: '.$destination.'<br />';
				// die();
															
			} else {
			
				echo '<span class="formErrorMessage">Upload failed. Please contact System Administrator!</span><br><hr>';
				die;	
				
			}
		
		} else {
		
			// Invalid Image File Type so provide user with error message
			$_SESSION['topic']['discussion_forum']['comment_title'] = $comment_title;	 // So user does not loose the Comment previously entered on the form	
			$_SESSION['topic']['discussion_forum']['comment_text'] = $comment_text;	 // So user does not loose the Comment previously entered on the form			
			$_SESSION['message_type'] = 'alert-box warning radius';				
			$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Document File Type</strong> must be <b>pdf or txt</b>. The file type you submitted was <b>'.$document_file_extension.'</b>. Please choose a Document with a supported File Type.</p>';
			mysqli_close($connection);
			$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
			header("location: ".$_SESSION['target_uri']);
			exit();	
			
		} // END if ($document_file_extension == 'pdf' || $document_file_extension == 'txt') {		
		 	
	} // END if (isset($document_file_name) && strlen($document_file_name) > 0) {

	// Upload Image file	
	if (isset($image_file_name) && strlen($image_file_name) > 0) {
	
		// echo 'File Upload File Type Image: '.$image_file_name.'<br />';	

		$tmp_image_file_name = $_FILES['image_file'] ['tmp_name'];
		$image_file_size = $_FILES['image_file'] ['size'];
		$image_file_type = $_FILES['image_file'] ['type'];
		$image_file_error = $_FILES['image_file'] ['error'];	
									
		$image_file_extension = strtolower(substr($image_file_type, strpos($image_file_type, '/') +1));		
		$unique_filename = strtolower(substr($tmp_image_file_name, strpos($tmp_image_file_name, '/tmp/') +5)).'.'.$image_file_extension;	
		// echo 'Unique Filename: '.$unique_filename.'<br />';				
		
		// These defines are in /includes/defines.php.
		// define('user_media_domain', 'supplementrelief.com'); // Domain for User uploaded media
		// define('user_media_upload_directory', '/home/supplementrelief/public_html/media/'); // Discussion Forum uploaded documents, images and videos

		$database_image_file_url = 'https://'.user_media_domain.'/media/images/user/'.$unique_filename;
				
		// Check for a valid image filetype.
		if ($image_file_extension == 'jpg' || $image_file_extension == 'jpeg' || $image_file_extension == 'png' || $image_file_extension == 'gif') {
		
			// echo 'About to move Valid Image File: '.$database_image_file_url.'<br />';
		
			$directory = user_media_upload_directory.'/images/user/';
						
			$destination = $directory.$unique_filename;	
			
			// echo $tmp_image_file_name.'<br>';
			// echo $destination;
			// die;		
										
			// If values are set and not empty move_upload_file is invoked to place the uploaded file into the web server directory identified by $directory.
								
			if (move_uploaded_file($tmp_image_file_name, $destination)) {
			
				// echo 'Valid Image File uploaded to: '.$destination.'<br />About to create Thumbnail.';
				// die();
			
				// proportionally resize images to fit in max-width box parameter
				create_thumb($destination, 300);
				$database_image_file_url_thumbnail = 'https://'.user_media_domain.'/media/images/user/tn_'.$unique_filename;
										
				// echo '<b>Image file successfully uploaded.</b><br><hr>';
				
			} else {
			
				echo '<span class="formErrorMessage">Upload failed. Please contact System Administrator!</span><br><hr>';
				die;	
				
			}
		
		} else {
		
			// Invalid Image File Type so provide user with error message
			$_SESSION['topic']['discussion_forum']['comment_title'] = $comment_title;	 // So user does not loose the Comment previously entered on the form	
			$_SESSION['topic']['discussion_forum']['comment_text'] = $comment_text;	 // So user does not loose the Comment previously entered on the form			
			$_SESSION['message_type'] = 'alert-box warning radius';				
			$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Image File Type</strong> must be <b>jpg, jpeg, png or gif</b>. The file type you submitted was <b>'.$image_file_extension.'</b>. Please choose an Image with a supported File Type.</p>';
			mysqli_close($connection);
			$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
			header("location: ".$_SESSION['target_uri']);
			exit();	
			
		} // if ($file_extension == 'jpg' || $file_extension == 'jpeg' || $file_extension == 'png' || $file_extension == 'gif') {		
		 	
	} // if (isset($image_file_name) && strlen($image_file_name) > 0) {
	
	// Upload Video file
	if (isset($video_file_name) && strlen($video_file_name) > 0) {
															
		// echo 'File Upload File Type Video: '.$video_file_name.'<br />';

		// $video_file_name = $_FILES['image_file'] ['name'];
		$video_file_type = $_FILES['video_file'] ['type'];
		$tmp_video_file_name = $_FILES['video_file'] ['tmp_name'];
		$video_file_error = $_FILES['video_file'] ['error'];
		$video_file_size = $_FILES['video_file'] ['size'];
							
		// $video_file_extension = strtolower(substr($video_file_name, strpos($video_file_name, '.') +1));
		$video_file_extension = strtolower(substr($video_file_type, strpos($video_file_type, '/') +1));	
		
		// echo 'tmp_video_file_name: '.$tmp_video_file_name.'<br />';	
		// echo 'video_file_size: '.$video_file_size.'<br />';	
		// echo 'video_file_type: '.$video_file_type.'<br />';	
		// echo 'video_file_error: '.$video_file_error.'<br />';
		// echo 'Video file extension: '.$video_file_extension.'<br />';			
	
		// Check for a valid HTML5 video filetype.
		if ($video_file_extension != 'mp4' && $video_file_extension != 'webm') {
		
			// echo 'video_file_extension != mp4 or webm: '.$video_file_extension.'<br />';		
			
			$_SESSION['topic']['discussion_forum']['comment_title'] = $comment_title;	 // So user does not loose the Comment previously entered on the form	
			$_SESSION['topic']['discussion_forum']['comment_text'] = $comment_text;	 // So user does not loose the Comment previously entered on the form			
			$_SESSION['message_type'] = 'alert-box warning radius';				
			$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Video File Type</strong> must be <b>mp4</b> or <b>webm</b>. The file type you submitted was <b>'.$video_file_extension.'</b>. Please choose a Video with a supported File Type.</p>';
			mysqli_close($connection);
			$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
			header("location: ".$_SESSION['target_uri']);
			exit();
		}		
	
		// Check the size of the file uploaded.
		// This needs to be done in JS so that the user gets immediate feedback not having to wait for a large file to first upload (or time out) before it gets rejected for being too large.
		// The upload process needs to somehow be done in the background anyway because the user experience is too slow.
		// How does Facebook do it?
		// 5037394 = 5 MB
		// 1000000 = 1 MB
		// 10000000 = 10 MB
		if ($video_file_size > 30000000) {
			// echo 'File sized exceeded 30MB.<br />';
		    // File size is too large so provide user with error message.
		    $_SESSION['topic']['discussion_forum']['comment_title'] = $comment_title;	 // So user does not loose the Comment previously entered on the form	
		    $_SESSION['topic']['discussion_forum']['comment_text'] = $comment_text;	 // So user does not loose the Comment previously entered on the form			
		    $_SESSION['message_type'] = 'alert-box warning radius';				
		    $_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Video File Size</strong> must be less than 30MB. Please choose a smaller-size Video.</p>';
		    mysqli_close($connection);
		    $_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
		    header("location: ".$_SESSION['target_uri']);
		    exit();	
		}
		
		$unique_filename = strtolower(substr($tmp_video_file_name, strpos($tmp_video_file_name, '/tmp/') +5)).'.'.$video_file_extension;
		// echo 'Unique Filename: '.$unique_filename.'<br>';
		// die;			
		
		// define('user_media_domain', 'supplementrelief.com'); // Domain for User uploaded media. Set in /includes/defines.php.
		// define('user_media_upload_directory', '/home/supplementrelief/public_html/media/'); // Discussion Forum uploaded documents, images and videos. Set in /includes/defines.php.
		
		$database_video_file_url = 'https://'.user_media_domain.'/media/videos/user/'.$unique_filename;
		
		$directory = user_media_upload_directory.'/videos/user/';	
					
		$destination = $directory.$unique_filename;
								
		// If values are set and not empty move_upload_file is invoked to place the uploaded file into the web server directory identified by $directory.
		// Find a php function to resize (compress/optimize) large video files.					
		if (move_uploaded_file($tmp_video_file_name, $destination)) {
		
			// echo '<b>Video file successfully uploaded.</b><br><hr>';
			// die;							
			
		} else {
		
			echo '<span class="formErrorMessage">Upload failed. Please contact System Administrator!</span><br><hr>';	
			
		}
			 	
	} // END if (isset($video_file_name) && strlen($video_file_name) > 0) { 
    


if($_POST['action'] == 'new_comment') { 
	
	// echo 'New Education Topic Discussion Forum Comment.<br />';

	$comment_user_id = safe_sql_data($connection, $_POST['comment_user_id']);
	$comment_title = safe_sql_data($connection, $_POST['comment_title']);
	$comment_text = safe_sql_data($connection, $_POST['comment_text']);													
				
					
	// Create the Discussion Thread
	// Need to add session_id to table and populate using $_SESSION['userStatistics']['session_id'].
					
	$queryInsertDiscussionThread = '
	INSERT INTO discussion_threads (title, text, file_document_url, file_document_type, file_image_url, file_image_type, file_video_url, file_video_type, status, project_id, project_program_id, course_id, topic_id, web_page_id, created_by, created_timestamp, modified_timestamp) 
	VALUES (
	"'.$comment_title.'",
	"'.$comment_text.'",
	'.no_value_null_check($database_document_file_url).', 
	'.no_value_null_check($document_file_extension).', 
	'.no_value_null_check($database_image_file_url_thumbnail).', 
	'.no_value_null_check($image_file_extension).', 
	'.no_value_null_check($database_video_file_url).', 
	'.no_value_null_check($video_file_extension).', 
	"S", 
	"'.$_SESSION['enrollment']['project_id'].'", 
	"'.$_SESSION['enrollment']['project_program_id'].'", 
	"'.$_SESSION['enrollment']['course_id'].'", 
	"'.$_SESSION['enrollment']['topic_id'].'", 
	"'.$_SESSION['enrollment']['web_page_id'].'",
	"'.$comment_user_id.'",
	"'.$created_timestamp.'",  
	"'.$created_timestamp.'")';	
		
	// echo $queryInsertDiscussionThread.'<br /><hr />';
	
	$result_insert_comment = mysqli_query($connection, $queryInsertDiscussionThread);
	
	if (!$result_insert_comment) {
		show_mysqli_error_message($queryInsertDiscussionThread, $connection);
		die;
	}
	
	// Get the insert_id of the record just created in the database for the return URL hashtag.
	$last_insert_id = mysqli_insert_id($connection); 
	$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#comment'.$last_insert_id;
	
    $post_type = 'Comment';
		
}

if($_POST['action'] == 'new_reply') {

	// echo 'New Education Topic Discussion Forum Comment Reply.<br />';
	
	// show_array($_POST);		
	// show_array($_FILES);	
	// die();
		
	// set POST return values
	$comment_reply_id = safe_sql_data($connection, $_POST['comment_reply_id']);
	$comment_reply_user_id = safe_sql_data($connection, $_POST['comment_reply_user_id']);
	$comment_reply_text = safe_sql_data($connection, $_POST['comment_reply_text']);
	
	$document_file_name = $_FILES['document_file'] ['name'];
	$image_file_name = $_FILES['image_file'] ['name'];	
	$video_file_name = $_FILES['video_file'] ['name'];
	
	// Upload Document file
	if (isset($document_file_name) && strlen($document_file_name) > 0) {
	
		// echo 'File Upload File Type Document: '.$document_file_name.'<br />';	

		$document_file_type = $_FILES['document_file'] ['type'];
		$tmp_document_file_name = $_FILES['document_file'] ['tmp_name'];
		$document_file_error = $_FILES['document_file'] ['error'];
		$document_file_size = $_FILES['document_file'] ['size'];
								
		$document_file_extension = strtolower(substr($document_file_type, strpos($document_file_type, '/') +1));		
		$unique_filename = strtolower(substr($tmp_document_file_name, strpos($tmp_document_file_name, '/tmp/') +5)).'.'.$document_file_extension;	
		echo 'Unique Filename: '.$unique_filename.'<br />';				
		
		// These defines are in /includes/defines.php.
		// define('user_media_domain', 'supplementrelief.com'); // Domain for User uploaded media
		// define('user_media_upload_directory', '/home/supplementrelief/public_html/media/'); // Discussion Forum uploaded documents, images and videos

		$database_document_file_url = 'https://'.user_media_domain.'/media/documents/user/'.$unique_filename;
				
		// Check for a valid document filetype.
		if ($document_file_extension == 'pdf' || $document_file_extension == 'txt') {
		
			// echo 'About to move Valid Document File: '.$database_document_file_url.'<br />';
		
			$directory = user_media_upload_directory.'/documents/user/';
						
			$destination = $directory.$unique_filename;	
			
			// echo $tmp_document_file_name.'<br>';
			// echo $destination;
			// die;		
										
			// If values are set and not empty move_upload_file is invoked to place the uploaded file into the web server directory identified by $directory.
								
			if (move_uploaded_file($tmp_document_file_name, $destination)) {
			
				// echo 'Valid Document File uploaded to: '.$destination.'<br />';
				// die();
															
			} else {
			
				echo '<span class="formErrorMessage">Upload failed. Please contact System Administrator!</span><br><hr>';
				die;	
				
			}
		
		} else {
		
			// Invalid Image File Type so provide user with error message
			$_SESSION['topic']['discussion_forum']['comment_title'] = $comment_title;	 // So user does not loose the Comment previously entered on the form	
			$_SESSION['topic']['discussion_forum']['comment_text'] = $comment_text;	 // So user does not loose the Comment previously entered on the form			
			$_SESSION['message_type'] = 'alert-box warning radius';				
			$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Document File Type</strong> must be <b>pdf or txt</b>. The file type you submitted was <b>'.$document_file_extension.'</b>. Please choose a Document with a supported File Type.</p>';
			mysqli_close($connection);
			$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
			header("location: ".$_SESSION['target_uri']);
			exit();	
			
		} // END if ($document_file_extension == 'pdf' || $document_file_extension == 'txt') {		
		 	
	} // END if (isset($document_file_name) && strlen($document_file_name) > 0) {

				
	if (isset($image_file_name) && strlen($image_file_name) > 0) {
	
		// echo 'File Upload File Type Image: '.$image_file_name.'<br />';	
			
		$tmp_image_file_name = $_FILES['image_file'] ['tmp_name'];
		$image_file_size = $_FILES['image_file'] ['size'];
		$image_file_type = $_FILES['image_file'] ['type'];
		$image_file_error = $_FILES['image_file'] ['error'];	
									
		$image_file_extension = strtolower(substr($image_file_type, strpos($image_file_type, '/') +1));		
		$unique_filename = strtolower(substr($tmp_image_file_name, strpos($tmp_image_file_name, '/tmp/') +5)).'.'.$image_file_extension;	
		// echo 'Unique Filename: '.$unique_filename.'<br />';
						
		$database_image_file_url = 'https://'.user_media_domain.'/media/images/user/'.$unique_filename;
	
		// Check for a valid image filetype.
		if ($image_file_extension == 'jpg' || $image_file_extension == 'jpeg' || $image_file_extension == 'png' || $image_file_extension == 'gif') {
		
			// echo 'About to move File.<br />';
			
			$directory = user_media_upload_directory.'/images/user/';
						
			$destination = $directory.$unique_filename;	
			
			// echo $tmp_image_file_name.'<br>';
			// echo $destination;
			// die;		
										
			// If values are set and not empty move_upload_file is invoked to place the uploaded file into the web server directory identified by $directory.
								
			if (move_uploaded_file($tmp_image_file_name, $destination)) {
			
				// echo 'Valid Image File uploaded to: '.$destination.'<br />About to create Thumbnail.';
				// die();
			
				// proportionally resize images to fit in max-width box parameter
				create_thumb($destination, 300);
				$database_image_file_url_thumbnail = 'https://'.user_media_domain.'/media/images/user/tn_'.$unique_filename;
										
				// echo '<b>Image file successfully uploaded.</b><br><hr>';
				
			} else {
			
				// echo '<span class="formErrorMessage">Upload failed. Please contact System Administrator!</span><br><hr>';	
				
			}
		
		} else {
		
			// Invalid Image File Type so provide user with error message
			$_SESSION['topic']['discussion_forum']['comment_reply_text'] = $comment_reply_text;	 // So user does not loose the Comment Reply previously entered on the form			
			$_SESSION['message_type'] = 'alert-box warning radius';				
			$_SESSION['message'] = '<p>The <strong>Image File Type</strong> must be jpg, jpeg, png or gif. The file type you submitted was <b>'.$file_extension.'</b>. Please choose an Image with the supported File Type.</p>';
			mysqli_close($connection);
			$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
			header("location: ".$_SESSION['target_uri']);
			exit();	
			
		} // if ($file_extension == 'jpg' || $file_extension == 'jpeg' || $file_extension == 'png' || $file_extension == 'gif') {		
		 	
	} // if (isset($image_file_name) && strlen($image_file_name) > 0) { 
	// else {} extend to handle Document and Video file uploads for Discussion Thread Replies if desired.
	
	// Upload Video file
	if (isset($video_file_name) && strlen($video_file_name) > 0) {
															
		// echo 'File Upload File Type Video: '.$video_file_name.'<br />';

		// $video_file_name = $_FILES['image_file'] ['name'];
		$video_file_type = $_FILES['video_file'] ['type'];
		$tmp_video_file_name = $_FILES['video_file'] ['tmp_name'];
		$video_file_error = $_FILES['video_file'] ['error'];
		$video_file_size = $_FILES['video_file'] ['size'];
							
		// $video_file_extension = strtolower(substr($video_file_name, strpos($video_file_name, '.') +1));
		$video_file_extension = strtolower(substr($video_file_type, strpos($video_file_type, '/') +1));	
		
		// echo 'tmp_video_file_name: '.$tmp_video_file_name.'<br />';	
		// echo 'video_file_size: '.$video_file_size.'<br />';	
		// echo 'video_file_type: '.$video_file_type.'<br />';	
		// echo 'video_file_error: '.$video_file_error.'<br />';
		// echo 'Video file extension: '.$video_file_extension.'<br />';			
	
		// Check for a valid HTML5 video filetype.
		if ($video_file_extension != 'mp4' && $video_file_extension != 'webm') {
		
			// echo 'video_file_extension != mp4 or webm: '.$video_file_extension.'<br />';		
			
			$_SESSION['topic']['discussion_forum']['comment_title'] = $comment_title;	 // So user does not loose the Comment previously entered on the form	
			$_SESSION['topic']['discussion_forum']['comment_text'] = $comment_text;	 // So user does not loose the Comment previously entered on the form			
			$_SESSION['message_type'] = 'alert-box warning radius';				
			$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Video File Type</strong> must be <b>mp4</b> or <b>webm</b>. The file type you submitted was <b>'.$video_file_extension.'</b>. Please choose a Video with a supported File Type.</p>';
			mysqli_close($connection);
			$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
			header("location: ".$_SESSION['target_uri']);
			exit();
		}		
	
		// Check the size of the file uploaded.
		// This needs to be done in JS so that the user gets immediate feedback not having to wait for a large file to first upload (or time out) before it gets rejected for being too large.
		// The upload process needs to somehow be done in the background anyway because the user experience is too slow.
		// How does Facebook do it?
		// 5037394 = 5 MB
		// 1000000 = 1 MB
		// 10000000 = 10 MB
		if ($video_file_size > 30000000) {
			// echo 'File sized exceeded 30MB.<br />';
		    // File size is too large so provide user with error message.
		    $_SESSION['topic']['discussion_forum']['comment_title'] = $comment_title;	 // So user does not loose the Comment previously entered on the form	
		    $_SESSION['topic']['discussion_forum']['comment_text'] = $comment_text;	 // So user does not loose the Comment previously entered on the form			
		    $_SESSION['message_type'] = 'alert-box warning radius';				
		    $_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;The <strong>Video File Size</strong> must be less than 30MB. Please choose a smaller-size Video.</p>';
		    mysqli_close($connection);
		    $_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
		    header("location: ".$_SESSION['target_uri']);
		    exit();	
		}
		
		$unique_filename = strtolower(substr($tmp_video_file_name, strpos($tmp_video_file_name, '/tmp/') +5)).'.'.$video_file_extension;
		// echo 'Unique Filename: '.$unique_filename.'<br>';
		// die;			
		
		// define('user_media_domain', 'supplementrelief.com'); // Domain for User uploaded media. Set in /includes/defines.php.
		// define('user_media_upload_directory', '/home/supplementrelief/public_html/media/'); // Discussion Forum uploaded documents, images and videos. Set in /includes/defines.php.
		
		$database_video_file_url = 'https://'.user_media_domain.'/media/videos/user/'.$unique_filename;
		
		$directory = user_media_upload_directory.'/videos/user/';	
					
		$destination = $directory.$unique_filename;
								
		// If values are set and not empty move_upload_file is invoked to place the uploaded file into the web server directory identified by $directory.
		// Find a php function to resize (compress/optimize) large video files.					
		if (move_uploaded_file($tmp_video_file_name, $destination)) {
		
			// echo '<b>Video file successfully uploaded.</b><br><hr>';
			// die;							
			
		} else {
		
			echo '<span class="formErrorMessage">Upload failed. Please contact System Administrator!</span><br><hr>';	
			
		}
			 	
	} // END if (isset($video_file_name) && strlen($video_file_name) > 0) { 
				
	// Create the Discussion Thread Reply
			
	$queryInsertDiscussionReply = '
	INSERT INTO discussion_replies (discussion_thread_id, text, status, file_document_url, file_document_type, file_image_url, file_image_type, file_video_url, file_video_type, created_by, created_timestamp) 
	VALUES (
	"'.$comment_reply_id.'",	
	"'.$comment_reply_text.'", 
	"S", 
	'.no_value_null_check($database_document_file_url).', 
	'.no_value_null_check($document_file_extension).', 
	'.no_value_null_check($database_image_file_url_thumbnail).', 
	'.no_value_null_check($image_file_extension).', 
	'.no_value_null_check($database_video_file_url).', 
	'.no_value_null_check($video_file_extension).', 
	"'.$comment_reply_user_id.'",
	"'.$created_timestamp.'")';
		
	// echo $queryInsertDiscussionReply.'<br /><hr />';
	
	$result_insert_comment_reply = mysqli_query($connection, $queryInsertDiscussionReply);
	
	if (!$result_insert_comment_reply) {
		show_mysqli_error_message($queryInsertDiscussionReply, $connection);
		die;
	}
	
	// Get the insert_id of the record just created in the database for the return URL hashtag.
	$last_insert_id = mysqli_insert_id($connection); 
	$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#reply'.$last_insert_id;
	
	mysqli_free_result($result_insert_comment_reply);
	
	// Update the Discussion Thread modified_timestamp
	
	$queryDiscussionThread = '
		UPDATE discussion_threads SET 
		modified_timestamp = "'.$created_timestamp.'" 
		WHERE id = "'.$comment_reply_id.'"';
	
	// echo $queryDiscussionThread;
	$result_update_comment = mysqli_query($connection, $queryDiscussionThread);
	
	if (!$result_update_comment) {	
		show_mysqli_error_message($queryDiscussionThread, $connection);
		die;
	}
	
    $post_type = 'Reply';

}

$message = generate_forum_post_email_notification($connection, $post_type, $last_insert_id);
	
$from = $_SESSION['application']['email'];
$to = $_SESSION['application']['email'];
$subject = 'Education Forum Post Notification';
		   	   
send_email_html($from, $to, $subject, $message, "");

mysqli_close($connection);
header("location: ".$_SESSION['target_uri']);
exit();	

?>