<?php

// echo 'Begin education_prefix.php';

// When calling page is User Dashboard (course_outline_query.php).
// http://wrightcheck.com/sozo/education/?c_id=504&t_id=1204&wp_id=1205
	
include_once('../includes/header.php');

// User cannot access this page if we do not know the Course ID. If no Course ID they got here somehow probably from some older code. Redirect them to the Select Program Enrollment page.		
if (!isset($_SESSION['enrollment']['course_id'])) {
	// echo 'Session Course ID is not set. Redirect to Select Program Enrollment.';
	$_SESSION['message_type'] = 'alert-box alert radius';				
	$_SESSION['message'] = '<p>Please select a <strong>Program Enrollment</strong> from the listing before proceeding.</p>';		
	header("location: ../select_enrollment/");	
	exit();	
}

// Course
if (isset($_GET['c_id'])) {
	$course_id = $_GET['c_id'];
	$_SESSION['enrollment']['course_id'] = $_GET['c_id'];
	
 } else {
 	$course_id = $_SESSION['enrollment']['course_id'];	
 	
} 

// Topic
if (isset($_GET['t_id'])) {
	$topic_id = $_GET['t_id'];
	$_SESSION['enrollment']['topic_id'] = $_GET['t_id'];
		
}  else {

	if (strlen($_SESSION['enrollment']['topic_id']) > 0) {
		// Means user is returning to Education page from somewhere other than using the Course Outline navigation which explicitly sets the Topic ID in the URL parameter. Also implies that the Topic ID was set earlier in the SESSION and stored as a SESSION variable for this very purpose so that the user is returned to the correct Topic whenever they leave the Education page and then return later.
		$topic_id = $_SESSION['enrollment']['topic_id'];
	} else {
		// User has navigated to this page without using the Course Outline at some previous point to set the $_SESSION['enrollment']['topic_id'] so send them to the User Dashboard to Select a Topic from the Course Outline before going to the Education page. Don't think this path is possible but is here for insurance.
		// User message
		$_SESSION['message_type'] = 'alert-box warning radius';				
		$_SESSION['message'] = '<p>Please select a <strong>Topic</strong> from the <strong>Course Outline</strong> before accessing the Education page.</p>';		
		header("location: ../user_dashboard/");
		exit();
	}

}

// Web Page
if (isset($_GET['wp_id'])) {
	$web_page_id = $_GET['wp_id'];
	$_SESSION['enrollment']['web_page_id'] = $_GET['wp_id'];
		
}  else {
	 // Means user is returning to Education page from somewhere other than using the Course Outline navigation which explicitly sets the Web Page ID in the URL parameter. Also implies that the Web Page ID was set earlier in the SESSION and stored as a SESSION variable for this very purpose so that the user is returned to the correct Web Page whenever they leave the Education page and then return later.
	 $web_page_id = $_SESSION['enrollment']['web_page_id'];
}
	
$queryWebPageTitle  = "SELECT title FROM content_assets WHERE id = '$web_page_id' LIMIT 1";

// echo $queryWebPageTitle . '<br/><hr />';

$result_web_page_title = mysqli_query($connection, $queryWebPageTitle);

if (!$result_web_page_title) {
	echo $queryWebPageTitle . '<br/><hr />';
	die("Web Page Title query failed.");
}

//echo "Queried Web Page Title successfully.<br/><hr/>';

$web_page_title = '';

while($r = mysqli_fetch_assoc($result_web_page_title)) {
	$web_page_title = cleanEncoding($r['title']);
}

mysqli_free_result($result_web_page_title);

// GLOBAL $web_page_title, $topic_page_title;

?>