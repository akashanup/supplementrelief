<br />
<div id="educationSidebar">
	
	<dl class="accordion" data-accordion>
	  <dd class="accordion-navigation">
	  	<a href="#courseOutlineLinks">
	  		  <div class="row">
	  		   <div class="small-9 columns">
	  		     <div><h4><?php echo $course_title; ?></h4></div>
	  		   </div>  
	  		   <div class="small-3 columns">
	  		     <div class="accordionIcon"><i class="fa fa-plus right"></i></div>
	  		   </div>
	  		 </div>
	  	  </a> 	
	    <div id="courseOutlineLinks" class="content active"><?php echo $course_topic_block; ?></div>
	  </dd>
	</dl>	
		
	<?php
		// Display the Learning & Behavioral Objectives		
		if (!empty($learning_objective) && !empty($behavioral_objective)) {	
			echo ' 
			<div id="educationTopicObjectives">
				<h4>Learning Objective</h4>'
				.cleanEncoding($learning_objective).
				'<hr />			
				<h4>Behavioral Objective</h4>'
				.cleanEncoding($behavioral_objective).'
			</div>';
		}				
	?>
	
	<?php
		// Display the Key Thought	
		if (!empty($key_thought_block)) {			
			echo '
			<div id="educationKeyThought">
				<h4>Key Thought</h4>'
				.cleanEncoding($key_thought_block).'
			</div>';			
		}
	?>	
	
	<?php
		// Display the Terms
		if (!empty($term_block)) {			
			echo '
			<div id="educationTerms">'
				.cleanEncoding($term_block).'
			</div>';			
		}
	?>
				
</div>
<br />