<?php error_log(ALL) ?>

<br />
<dl class="tabs" data-options="deep_linking:true; scroll_to_content:false;" data-tab>

	<?php
		
	// Determine if a Video is present
	if (strlen($video_block) > 0) {	
		$video_present = true;		
		?>
		<dd class="active"><a href="#watch">Watch</a></dd>
		<dd><a href="#read">Read</a></dd>
		<?php
	} else {
		?>
		<dd class="active"><a href="#read">Read</a></dd>
		<?php		
	}
	
	// Determine if a Challenge is present
	if (strlen($challenge_block) > 0) {
		$challenge_present = true;
		?>
		<dd><a href="#challenge">Challenge</a></dd>
		<?php
	}
	
    // Determine if Resource(s) is present
	if (strlen($resource_block) > 0) {
		$resource_present = true;
		?>
		<dd><a href="#topicResource">Resources</a></dd>
		<?php
	}
	
    // Determine if Quiz is present
    // For the Qzzr integration (now retired)
    /*
	if (strlen($quiz) > 0) {
		$quiz_present = true;
		?>
		<dd><a href="#topicQuiz">Quiz</a></dd>
		<?php
	}
	*/
	
	// Determine if Quiz is present
	if (strlen($assessment_block) > 0) {
		$assessment_present = true;
		?>
		<dd><a href="#topicAssessment">Quiz</a></dd>
		<?php
	}
	
	// Determine if Products are present
	// $product_block = 'Have products.';
	
	if (strlen($product_block) > 0) {
		$product_present = true;
		?>
		<dd><a href="#topicProducts">Products</a></dd>
		<?php
	}
 	
	// Determine if the Topic is allowed to use a Discussion Forum
	if ($_SESSION['topic']['discussion_forum']['forum'] == true) {	
		?>
		<dd><a href="#discuss">Discuss</a></dd>
		<?php
	}
	?>
  		
</dl>

<div class="tabs-content">

	<?php
		
	// Video content	
	if ($video_present) {	
		?>
		<div class="content active" id="watch">	
			<div id="educationVideos">
			  	<?php
			  	echo $video_block;
			  	echo $video_modal_block;
			  	?>
			</div>			
		</div>		
		<?php  
	}
 		
	// Read content	
	if ($video_present) { 
		?>
		<div class="content" id="read"> 
	  	<?php
	} else { 
	  	?>	
	  	<div class="content active" id="read"> 
	  	<?php 		
	}
  		  
	?>
      
        <div id="educationImages">
        	    
    	    <div id="educationSingleImage">
    	    <?php
    		    if($totalImageCount==1) {    
    		    	echo '
    	    		<div class="row">
    	    		  <div class="small-12 columns">
    	    		  	<img src="'.$image1URL.'" alt="'.$image1AltText.'" style="width:100%;height:auto"/>
    	    		  </div>	
    	    		</div>';    
    	    		echo '
    	    		<div class="row">
    	    		  <div class="small-12 columns">
    	    		  	<div class="imageCaption"><i class="fa fa-image"></i>&nbsp;&nbsp;'.$image1Caption.'</div>
    	    		  </div>	
    	    		</div>';		    	
    		    }			    
    	    ?>
    		</div>
    		
    		<div id="educationDoubleImage">	
    			<?php
    			if($totalImageCount==2) {		
    				echo '
    				<div class="row collapse">
    				  <div class="small-6 columns">
    				  	<img src="'.$image1URL.'" alt="'.$image1AltText.'" style="width:100%;height:auto"/>
    				  	<div class="imageCaptionMultiple">'.$image1Caption.'</div>	  	
    				  </div>	
    				  <div class="small-6 columns">
    				  	<img src="'.$image2URL.'" alt="'.$image2AltText.'" style="width:100%;height:auto"/>
    				  	<div class="imageCaptionMultiple">'.$image2Caption.'</div>
    				  </div>	
    				</div>';					
    			}		
    			?>	
    		</div>
    			
    		<div id="educationTripleImage">	
    			<?php	
    			if($totalImageCount==3) {			
    				echo '
    				<div class="row collapse">
    				  <div class="small-4 columns">
    				  	<img src="'.$image1URL.'" alt="'.$image1AltText.'" style="width:100%;height:auto"/>
    				  	<div class="imageCaptionMultiple">'.$image1Caption.'</div>	  	
    				  </div>	
    				  <div class="small-4 columns">
    				  	<img src="'.$image2URL.'" alt="'.$image2AltText.'" style="width:100%;height:auto"/>
    				  	<div class="imageCaptionMultiple">'.$image2Caption.'</div>
    				  </div>	
    				  <div class="small-4 columns">
    				  	<img src="'.$image3URL.'" alt="'.$image3AltText.'" style="width:100%;height:auto"/>
    				  	<div class="imageCaptionMultiple">'.$image3Caption.'</div>
    				  </div>	
    				</div>';						
    			}	
    			?>
    		</div>
    				
    		<div id="educationTextBlocks">
    			<?php					
    				GLOBAL $text_block;			
    				echo cleanEncoding($text_block);				
    			?>		
    		</div>
    				
			<?php
		
			GLOBAL $citations_block;
			
			if (!empty($citations_block)) {
				echo '
				<div id="educationCitations">
					<strong>Citations:</strong>
					<br>'.
					cleanEncoding($citations_block).'
				</div>';		
			}
    		
    		?>
    					    
        </div>
    		
    </div> <!-- END Read content -->
    
    <?php
    // Challenge content
	if ($challenge_present) {		
		?>
		<div class="content" id="challenge">  		  		
			<div id="educationChallenge">
				<center><h2><i class="fa fa-certificate fa-1x icon-red"></i>&nbsp;&nbsp;Personal Challenge</h2></center>
				<?php echo cleanEncoding($challenge_block); ?>
			</div>
		</div>
		<?php
	}
	
    // Resource content
	if ($resource_present) {		
		?>
		<div class="content" id="topicResource">  		  		
			<div id="educationChallenge1">
				<!-- <center><h2><i class="fa fa-certificate fa-1x icon-red"></i>&nbsp;&nbsp;Resources</h2></center> -->
				<?php echo cleanEncoding($resource_block); ?>
			</div>
		</div>
		<?php
	}
	
    // Quiz content
    // Retired Qzzr
    /*
	if ($quiz_present) {		
		?>
		<div class="content" id="topicQuiz">  		  		
			<div id="">
				<!-- <center><h2><i class="fa fa-certificate fa-1x icon-red"></i>&nbsp;&nbsp;Resources</h2></center> -->
				<?php echo cleanEncoding($quiz); ?>
			</div>
		</div>
		<?php
	}
	*/
	
	// Assessment content
	if ($assessment_present) {		
		?>
		<div class="content" id="topicAssessment">  		  		
			<div id="">
				<!-- <center><h2><i class="fa fa-certificate fa-1x icon-red"></i>&nbsp;&nbsp;Resources</h2></center> -->
				<?php echo cleanEncoding($assessment_block); ?>
			</div>
		</div>
		<?php
	}
	
	// Products content
	if ($product_present) {		
		?>
		<div class="content" id="topicProducts">  		  		
			<div id="">
				<?php echo cleanEncoding($product_block); ?>
			</div>
		</div>
		<?php
	}

 	// Determine if the Topic is allowed to use a Discussion Forum
	if ($_SESSION['topic']['discussion_forum']['forum'] == true) {
		?>
		<div class="content" id="discuss">
			<div id="educationDiscussionForum">
		  	<?php 
		  		$_SESSION['forum_type'] = 'education';
		  		include('../php/discussion_forum.php'); 
	  		?>
			</div>
		</div>
	<?php
	} 	
  ?>	
   
</div>