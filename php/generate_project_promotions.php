<?php

if (isset($_SESSION['enrollment']['project_id'])) { 	
	$project_id = $_SESSION['enrollment']['project_id']; 	
} else {
	$project_id = public_project_id; // store/includes/defines.php
}
	
if (isset($_SESSION['enrollment']['project_program_id'])) { 	
	$project_program_id = $_SESSION['enrollment']['project_program_id']; 	
} else {
	$project_program_id = public_project_program_id; // store/includes/defines.php
}
	
// show_array($_SESSION);

// Query the Promotions	
$queryPromotion = 'SELECT 
	pr.id, 
	pr.project_id, 
	pr.project_program_id, 
	pr.name, 
	pr.version, 
	pr.description, 
	pr.active, 
	pr.destination_url, 
	pr.link_text, 
	pr.effective_timestamp, 
	pr.end_timestamp, 
	im.host_url, 
	im.caption, 
	im.alt_text 
	FROM promotions pr 
	LEFT JOIN images im ON 
		( pr.image_id = im.content_asset_id AND 
		  im.size = "Banner" AND 
		  im.usage_size = "Small" AND 
		  im.usage_shape = "Block" )
	WHERE pr.project_id = "'.$project_id.'"
	AND pr.project_program_id = "'.$project_program_id.'"';
	
// echo $queryPromotion;
// die();
	
// Execute the query
$result_promotion = mysqli_query($connection, $queryPromotion);

if (!$result_promotion) {
	show_mysqli_error_message($queryPromotion, $connection);
	die;
}

$rowcount = mysqli_num_rows($result_promotion);

// echo '# Promotions: '.$rowcount.'<br>';

if ($rowcount > 0) {
	
	// Setup Slider prefix
	?>
	<!-- Promotion Slider -->
	<br />
	<div class="promotion-slider show-for-medium-up">
	<?php
	
} else {
	
	// show_array($_SESSION);
	
	?>
	
	<br />
	
	<div class="row">		
		<div class="small-12 columns">
			<div class="panel">
				<p>No promotions found for Project Program: <?php echo $project_program_id; ?>.</p>
			</div>							
		</div>
	</div>
	
	<?php	
	
}

while($r = mysqli_fetch_assoc($result_promotion)) {
	
	// show_array($r);

	?>
	<div>
	
	  	<div class="row">
		  			  		
	  		<div class="small-12 medium-3 columns">
		  		<center><img src="<?php echo $r['host_url']; ?>" width="175px"></center>
			</div>
					  							
		  	<div class="small-12 medium-9 columns">
			  	<h3><?php echo $r['name']; ?></h3>
			  	<?php echo $r['description']; ?>
			  	<p><a href="<?php echo $r['destination_url']; ?>" class="button medium radius success"><?php echo  html_entity_decode($r['link_text']); ?></a></p>			
		  	</div>
		  	  			
		</div>
	
	</div>
	
	<?php
	
}

if ($rowcount > 0) {
	
	// Setup Slider postfix
	?>
	</div>
	<?php
	
}

mysqli_free_result($result_promotion);

?>