<?php
	include_once('../includes/header.php');
	if(isset($_POST['searchText']))
	{
		$searchText 	=	safe_sql_data($connection, $_POST['searchText']);	
		$likeName 		=	$searchText;
		$searchTextLen	=	strlen($searchText);
		$fetchQuery 	=	mysqli_query($connection, getProductsQuery($likeName));
		$noOfProducts	=	mysqli_num_rows($fetchQuery);
		
		
		$json 			=	[];
		
		$i 				=	0;
		$elements 		=	0;

		$html 			=	'<div id="productsNameList" style="z-index:9999; position: absolute; background-color:#fafafa; width:100%; border-style: groove;"><ul style="list-style: none; margin-bottom: 0em; font-size: .9em;" id="productsNameUl">';
		$option			=	'<li>No product found!</li>';

		while ( $searchTextLen > $i ) 
		{
			if($noOfProducts)
			{
				$option 	= '';
				//$option 	=	$i ? '<li>No product found! Showing related product(s).</li>' : '';
								
				while($productsName	=	mysqli_fetch_assoc($fetchQuery))
				{
					$elements 		=	$elements + 1;
					$productName 	=	str_replace("NuMedica", "", $productsName['name']);
					$option 		.= 	'<li class="productsNameLi" style="margin-left:-18px; padding-left:10px">'.$productName.'</li>';
				}
				
				break;
			}
			else
			{
				$i++;
				$likeName 		=	substr($searchText, 0, ($searchTextLen-$i));
						
				$fetchQuery 	=	mysqli_query($connection, getProductsQuery($likeName));
				$noOfProducts	=	mysqli_num_rows($fetchQuery);
			}
		}
	}

	$html 				.=	$option.'</ul></div>';
	$json['html']		=	$html;
	$json['elements'] 	=	$elements;
	echo json_encode($json);

	function getProductsQuery($likeName)
	{		
		$query 	=	'SELECT 
					bpu.name 
					FROM project_brand_product_usages pbpu 
					LEFT JOIN brand_product_usages bpu ON pbpu.brand_product_usage_id = bpu.id 
					WHERE pbpu.project_id = "'.$_SESSION['enrollment']['project_id'].'"  
					AND pbpu.active = 1  
					AND pbpu.effective_date <= CURRENT_DATE 
					AND	(pbpu.end_date IS NULL or pbpu.end_date >= CURRENT_DATE)
					AND bpu.name LIKE "%'.$likeName.'%" 
					ORDER BY bpu.name ASC 
					LIMIT 15';		
			
		return $query;
	}
	
?>