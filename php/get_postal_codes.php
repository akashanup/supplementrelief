<?php
include('../includes/header.php');

// show_array($_SERVER);
// die;

// if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["state_code"])) {

	// Build the Select List for Postal Codes
	
	// $state_code = sanitize($_POST["state_code"]);
	$state_code = 'LA';
	
	$queryPostalCodes = '
	SELECT 
	pc.code, 
	pc.county 
	FROM postal_codes pc
	WHERE pc.state_code = "'.$state_code.'"  
	/* AND (pc.code LIKE "711%" OR pc.code LIKE "714%") */
	ORDER BY pc.code ASC'; 
	
	 //echo $queryPostalCodes;
	 //die();
		
	// Execute the query
	$result_postal_code = mysqli_query($connection, $queryPostalCodes);
	
	if (!$result_postal_code) {
		show_mysqli_error_message($queryPostalCodes, $connection);
		die;
	}
								
	$postal_code_options = '<option value="">Select a Postal Code ...</option>';
	
	while($postal_code_row = mysqli_fetch_array($result_postal_code)) { 
		
		// show_array($state_row);
		
		$selected = '';
		if(($postal_code ? $postal_code : $row1['p']) == $postal_code_row['code']) $selected = ' selected';
		$postal_code_options .= '<option value="'.$postal_code_row['code'].'" '.$selected.'>'.$postal_code_row['code'].' - '.$postal_code_row['county'].'</option>';
	}
					
	mysqli_free_result($result_postal_code);
	
	?>
	
	<label>Postal Code
		<select required name="postal_code">
	    <?php echo $postal_code_options; ?>
		</select>
		<span class="form-error">Postal Code is required.</span>	
	</label>
	
	<?php
		
// } 

?>

