<?php
	
// show_array($_SESSION);
	
// echo 'Begin google_ecommerce_analytics_tracking_test.php.<br />';
// die;
	
// Google Analytics e-Commerce Tracking
// https://developers.google.com/analytics/devguides/collection/analyticsjs/ecommerce

// $order_id = '1808';
$order_id = $_SESSION['order']['order_id'];

// Get the Order
$queryOrder = 'SELECT 
	ord.invoice_num AS order_number, 
	ord.product_retail_amount, 
	ord.product_subtotal_amount, 
	ord.shipping_total, 
	ord.total_tax, 
	ord.total_amount   
	FROM orders ord  
 	WHERE ord.id = '.$order_id.' 
 	LIMIT 1 ';
						
// echo $queryOrder . '<br /><hr />';

$result_order = mysqli_query($connection, $queryOrder);

if (!$result_order) {
	show_mysqli_error_message($queryOrder, $connection);
	die;
}

$order = mysqli_fetch_assoc($result_order);

// show_array($order);

// add transaction
/*
ga('ecommerce:addTransaction', {
  'id': '1234',                     // Transaction ID. Required.
  'affiliation': 'Acme Clothing',   // Affiliation or store name.
  'revenue': '11.99',               // Grand Total.
  'shipping': '5',                  // Shipping.
  'tax': '1.29'                     // Tax.
});
*/
	
$affiliation = 'SupplementRelief.com';

// create tracker object
$addTransaction = "
ga('ecommerce:addTransaction', {
'id': '".$order['order_number']."', 
'affiliation': '".$affiliation."',
'revenue': '".$order['product_subtotal_amount']."',
'shipping': '".$order['shipping_total']."', 
'tax': '".$order['total_tax']."' 
});
";

// Get the Order Items
// Get the Order Item Option for each ORder ITem and add it to the product name passed to GA.
$queryOrderItems = 'SELECT 
	oi.id AS order_item_id, 
	oi.brand_product_usage_id, 
	oi.quantity AS order_item_quantity, 
	oi.unit_price AS order_item_unit_price, 
	bpu.name AS brand_product_name, 
	bpu.sku 
	FROM order_items oi 
	JOIN brand_product_usages bpu ON oi.brand_product_usage_id = bpu.id 
	WHERE oi.orders_id = '.$order_id.' 
	ORDER BY oi.created_timestamp ASC';
                
// echo $queryOrderItems . '<br /><hr />';

$result_order_item = mysqli_query($connection, $queryOrderItems);

if (!$result_order_item) {
	show_mysqli_error_message($queryOrderItems, $connection);
	die;
}

// add transaction item
/*
ga('ecommerce:addItem', {
  'id': '$transId',
  'name': '{$item['name']}',
  'sku': '{$item['sku']}',
  'category': '{$item['category']}',
  'price': '{$item['price']}',
  'quantity': '{$item['quantity']}'
});
*/

$category = 'NuMedica Supplements';

while($order_item = mysqli_fetch_assoc($result_order_item)) {	
	
	// show_array($order_item);
	
    $queryOrderItemOptions = 'SELECT 
    	oio.order_option_type_code, 
    	oio.order_option_name, 
        pot.name AS product_option_type_name 
        FROM order_item_options oio
        LEFT JOIN product_option_types pot ON oio.order_option_type_code = pot.code 
        WHERE oio.order_item_id = "'.$order_item['order_item_id'].'" 
        ORDER BY oio.id ASC';
        
    // echo $queryOrderItemOptions . '<br /><hr />';

	$result_order_item_option = mysqli_query($connection, $queryOrderItemOptions);
	
	if (!$result_order_item_option) {
		show_mysqli_error_message($queryOrderItemOptions, $connection);
		die;
	}

	$product_options = '';

	while($order_item_option = mysqli_fetch_assoc($result_order_item_option)) {	
	
		// show_array($order_item_option);
		$product_options .= ' ('.$order_item_option['product_option_type_name'].': '.$order_item_option['order_option_name'].')';
		
	}
	
	$product_name = $order_item['brand_product_name'].$product_options;
		
	$addTransactionItems .= "
	ga('ecommerce:addItem', {
	'id': '".$order['order_number']."', 
	'name': '".$product_name."',
	'sku': '".$order_item['sku']."', 
	'category': '".$category."',
	'price': '".$order_item['order_item_unit_price']."', 
	'quantity': '".$order_item['order_item_quantity']."' 
	});
	";
		
}

mysqli_free_result($result_order);

mysqli_free_result($result_order_item);

mysqli_free_result($result_order_item_option);
                
// echo $addTransaction;
// echo $addTransactionItems;
// die;

?>

<!--
	load the ecommerce plugin for the analytics.js library
	send the data to Google Analytics using the ecommerce:send command:	
-->

<script>
ga('require', 'ecommerce');
<?php
	echo $addTransaction;
	echo $addTransactionItems;
?>
ga('ecommerce:send');
</script>