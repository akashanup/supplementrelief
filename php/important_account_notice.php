<div data-alert class="alert-box secondary radius">
	
	<h3><i class="fa fa-bullhorn icon-red"></i>&nbsp;Announcement for Member Accounts Prior to August 24, 2015</h3>
	
	<p>Our website was updated to offer <b>online Wellness Education programs</b> and accomodate <b>mobile users with SmartPhones and Tablets</b>. <b><span style="color:red">IF THIS IS YOUR FIRST TIME to LOGIN on the UPDATED WEBSITE</span></b>, please click</b>&nbsp;<a href="../reset-password/" style="text-decoration: underline;"><b>here</a> to CHOOSE YOUR NEW PASSWORD or YOU WILL NOT be able to LOGIN</b>. Your Username <b>is still the same Email Address</b> you used to login before.&nbsp;&nbsp;If you need any help</b>, please call <b>(888) 424-0032</b> or <a href="../contact/" style="text-decoration: underline;"><b>email</b></a> and we will change your password for you to the new one you request.</p>
			
	<a href="#" class="close">&times;</a>
</div>
