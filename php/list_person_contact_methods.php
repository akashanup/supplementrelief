<?php
// echo 'Begin list_person_contact_methods.php<br /><hr />';

// Adam: This is a security risk and should be addressed when time permits.
//sec_session_start();
include_once('../includes/core.php');

// $_SESSION['user']['user_id'] set at user login

$queryPersonContactMethods	= 'SELECT 
	pcm.id AS contact_method_id, 
	cmt.name AS contact_method_type_name,
	pcm.contact_method_value, 
	pcm.effective_date, 
	pcm.end_date  
	FROM person_contact_methods pcm
	LEFT JOIN contact_method_types cmt ON pcm.contact_method_type_code = cmt.code 
	WHERE pcm.person_id = "'.$_SESSION['user']['user_id'].'"   
	ORDER BY pcm.effective_date DESC';
			
// echo $queryPersonContactMethods . '<br /><hr />';

$result_list_person_contact_method = mysqli_query($connection, $queryPersonContactMethods);

if (!$result_list_person_contact_method) {
	show_mysqli_error_message($queryPersonContactMethods, $connection);
	die;
}

?>

<div class="row">
	<div class="small-12 columns">
	
		<table width="100%">
		
			<thead>
				<tr>
					<th><center><i class="fa fa-pencil"></i></center></th> 
					<th>Method</th>
					<th>Value</th>				   
					<th>Effective<br />Date</th> 
					<th>End<br />Date</th> 			  		  					  		  
				</tr>
			</thead>
			
			<?php	
			 
			while($r = mysqli_fetch_assoc($result_list_person_contact_method)) {
						
				?>					
				<tr>
				
					<!-- <td><?php echo '<center><a href="../edit_person_contact/id='.$r['id'].'" title="Edit Contact" >Edit</a></center>'; ?></td> -->
					
					<!-- <td><?php echo '<center><a href="#" onclick="editPersonContact();return false">Edit</a>'; ?></td> -->
					
					<td>	
						<form id="editPersonContactMethod" action="../php/person_contact_method_edit_query.php" method="post">
							<input type="hidden" name="action" value="edit"/>
							<input type="hidden" name="contact_method_id" value="<?php echo $r['contact_method_id']; ?>"/>	
							<!-- Adam, can I use a link instead of a button to invoke the Edit form? -->
							<center>
								<input type="submit" class="form_text_link" value="Edit">
							</center>
						</form>						
					</td>
															
					<td><?php echo $r['contact_method_type_name']; ?></td>
					
					<td><?php echo $r['contact_method_value']; ?></td>
																					
					<td><?php echo date('m/d/Y', strtotime($r['effective_date'])); ?></td>
					
					<td><?php echo hdate($r['end_date']); ?></td>
					
				</tr>
		
				<?php 
			} 
		  
			mysqli_free_result($result_list_person_contact_method);
		
			?>
		
		</table>
		
	</div>
</div>

<!-- <script type="text/javascript">console.log("Contacts Listed");</script> -->