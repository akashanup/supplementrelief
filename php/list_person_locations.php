<?php

// echo 'Begin list_person_locations.php<br /><hr />';

// include_once('../resources/includes/core.php');

// $_SESSION['user']['user_id'] set at user login

$queryLocations	= 'SELECT 
	lo.id AS location_id, 
	lo.person_id, 
	lo.effective_date, 
	lo.end_date, 
	at.name AS address_type_name, 
	ad.id AS address_id, 
	ad.address, 
	ad.city, 
	ad.state_id,   
	ad.postal_code, 
	ad.country_id, 
	st.state_name,  
	cou.country_name 
	FROM locations lo 
	LEFT JOIN address_types at ON lo.address_type_code = at.code 
	LEFT JOIN addresses ad ON lo.address_id = ad.id 
	LEFT JOIN states st ON ad.state_id = st.state_id 
	LEFT JOIN country cou ON ad.country_id = cou.country_id
	WHERE lo.person_id = "'.$_SESSION['user']['user_id'].'"   
	ORDER BY lo.effective_date DESC';
			
// echo $queryLocations . '<br /><hr />';

$result_list_person_location = mysqli_query($connection, $queryLocations);

if (!$result_list_person_location) {
	echo $queryLocations . '<br /><hr />';
	die("Database List Person Locations query failed.");
}

?>

<table width="100%">

	<thead>
		<tr>
			<th><center><i class="fa fa-pencil"></i></center></th> 
			<th>Type</th>
			<th>Address</th>
			<!--<th class="show-for-large-up">Contact</th>-->				   
			<!-- <th class="show-for-medium-up">Effective<br />Date</th> -->
			<!-- <th class="show-for-medium-up">End<br />Date</th> 	-->		  		  					  		  
		</tr>
	</thead>
	
	<?php	
	
	while($r = mysqli_fetch_assoc($result_list_person_location)) {
				
		?>		
	
		<tr>
		
			<td>	
				<form id="form_id" method="post" class="edit_address" action="../php/edit_person_location_form.php">
					<input type="hidden" name="action" value="edit"/>
					<input type="hidden" name="address_id" value="<?php echo $r['address_id']; ?>"/>	
					<input type="hidden" name="location_id" value="<?php echo $r['location_id']; ?>"/>
					<center><input class="button tiny radius" type="submit" value="Edit"></center>
					<!-- Instead of a button, could we use a simple text link "Edit"? to trigger the submit function? -->
				</form>						
			</td>
			
			<td><?php echo $r['address_type_name']; ?></td>
							
			<td><?php echo nl2br($r['address']).'<br />'.$r['city'].', '.$r['state_name'].' '.$r['postal_code'].'<br />'.$r['country_name']; ?></td>		
									
			<!-- <td class="show-for-medium-up"><?php echo date('m/d/Y', strtotime($r['effective_date'])); ?></td> -->
			
			<!-- <td class="show-for-medium-up"><?php echo hdate($r['end_date']); ?></td> -->
			
		</tr>

	<?php } 
  
	mysqli_free_result($result_list_person_location);

	?>

</table>

<div class="row">		
	<div class="small-12 columns">
		<div class="right">
			<a href="#" title="view our Privacy Policy" data-reveal-id="footer-privacy-policy"><i class="fa fa-lock" aria-hidden="true"></i>&nbsp;Privacy Policy</a>&nbsp;&nbsp;
		</div> 			
					
	</div>
</div>