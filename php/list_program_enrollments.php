<?php
	
// echo 'Begin list_program_enrollments.php<br>';
 	
$queryEnrollments = 'SELECT 
	en.id AS enrollment_id, 
	en.effective_timestamp AS enrollment_timestamp, 
	en.primary_enrollment, 
	en.active,  
	en.forum_moderator, 
	ouf.name AS organization_unit_facility_name, 
	pp.id AS project_program_id, 
	pp.name AS project_program_name, 
	pp.summary_description, 
	pp.effective_date AS project_program_effective_date, 
	pp.end_date AS project_program_end_date, 
	im.host_url AS program_image_host_url, 
	im.alt_text AS program_image_alt_text, 
	im.caption AS program_image_caption, 
	pj.name AS project_name, 	
	rt.name AS role_type_name 
	FROM person_roles pr 
	LEFT JOIN enrollments en ON pr.id = en.person_role_id  
	LEFT JOIN organization_unit_facilities ouf ON pr.organization_unit_facility_id = ouf.id 
	LEFT JOIN project_programs pp ON en.project_program_id = pp.id 
	LEFT JOIN images im ON (
		pp.image_id = im.content_asset_id AND
		im.size = "Program" AND 
		im.format = "JPG" AND 
		im.usage_size = "Medium" AND 
		im.usage_shape = "Block")  
	LEFT JOIN projects pj ON pp.project_id = pj.id 
	LEFT JOIN role_types rt ON pr.role_type_code = rt.code 
	WHERE pr.person_id = '.$_SESSION['user']['user_id'].' 
	AND en.active = 1  
	AND pp.effective_date <= CURRENT_DATE 
	AND (pp.end_date IS NULL or pp.end_date >= CURRENT_DATE) 			
	ORDER BY en.primary_enrollment DESC, project_program_name ASC';
		
// echo $queryEnrollments . '<br /><hr />';
// die;
	
$result_list_program_enrollment = mysqli_query($connection, $queryEnrollments);

if (!$result_list_program_enrollment) {
	show_mysqli_error_message($queryEnrollments, $connection);
	die;
}

// $rowcount = mysqli_num_rows($result_list_program_enrollment);


while($r = mysqli_fetch_assoc($result_list_program_enrollment)) {	
			
	// show_array($r);
	
	if ($r['program_image_host_url']) { 
		$program_image_host_url = $_SESSION['application']['root_media_host_url'].$r['program_image_host_url']; 
	} else {
		$program_image_host_url = 'https://placehold.it/250x250';
	}
	
	?>
	<div class="row">
		
		<div class="small-12 medium-3 columns">					
			<center><img class="th" src="<?php echo $program_image_host_url; ?>"></center>
		</div>
		
		<div class="small-12 medium-9 columns">
			<a id="memberDashboardProgramListingTitle" href="../member-dashboard/?e_id=<?php echo $r['enrollment_id']; ?>" title="Select Program Enrollment"><?php echo $r['project_program_name']; ?></a>
			<?php echo $r['summary_description']; ?>
		</div>
		
	</div>
	
	<hr>
	
	<?php 
} 

mysqli_free_result($result_list_program_enrollment);

?>