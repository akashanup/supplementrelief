<!--
<div class="row">		
	<div class="small-12 columns">
		<br />
		<?php // include('../php/important_account_notice.php'); ?>					
	</div>
</div>
-->

<?php

// This script is called from ../login/.

// echo 'Begin login_process.php.<br>';
// die;
								
$_SESSION['user']['token'] = md5(uniqid(mt_rand(), true));

// show_session();
				
if (isset($_GET['error'])) {
	// echo '<p class="error">Error Logging In!</p>';
	// Customize error messages based upon error code. 
	
	// echo 'Session Login Status: '.$_SESSION['login_status]'];
	
	if ($_GET['error'] == 1) {	
		
		if ($_SESSION['user']['login_status]'] == 'u') {
			
			$_SESSION['message_type'] = 'alert-box alert radius';					
			$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle"></i>&nbsp;The <strong>User ID</strong> provided was not found.</p><p><p>Please try again if you might have entered it incorrectly. If you need any help please call <b>(888) 424-0032</b> or <a href="../contact/" class="linkUnderlineWhite">email</a>.</p><p>If you <b>do NOT already have an account</b>, and you want to use the <b>User ID</b> provided, you can create a new account right now by clicking <a href="../register-member-account/" class="linkUnderlineWhite">here</a>. </p>';
			
			// <p>If you had a <b>Member Account</b> on our website <b>prior to August 24, 2015</b>, and <b>this is your first time to LOGIN to our updated website</b>, then your <b>Username is your Email Address</b>. You will need to <b>choose a NEW PASSWORD before you can LOGIN</b>. See the link <b>below the LOGIN button on this page</b> that starts with <b>If you forgot your Password</b>. If you need help please call <b>(888) 424-0032</b> or <a href="../contact/" style="color:white; text-decoration: underline;"><b>email</b></a> and will we assist you right away.</p>
		
		}
		
		if ($_SESSION['user']['login_status]'] == 'p') {
			
			$_SESSION['message_type'] = 'alert-box alert radius';					
			$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle"></i>&nbsp;The <strong>Password</strong> provided was not found.</p><p>Please try again if you might have entered it incorrectly. You can also click <a href="../reset-password/" title="Choose your New Password" style="color:white; text-decoration: underline;"><b>here</b></a> to <b>choose a new password</b> if you do not remember it. If you need help please call <b>(888) 424-0032</b> or <a href="../contact/" style="color:white; text-decoration: underline;"><b>email</b></a>.</p>';
		
		}
	
		// Other Errors can be added here.
		
		show_session_message();
		unset($_SESSION['user']['login_status]']);
	}

}

if (isset($_GET['msg'])) {
	
	// Customize message based upon message code.
	
	if ($_GET['msg'] == 1) {
		// User clicked link in email sent to verify registration to complete registration process.	
		
		if (isset($_GET['u_id'])) { 
			
			$user_id = sanitize($_GET['u_id']);

			$queryPerson = 'SELECT 
			pe.first_name, 
			pe.last_name, 
			pe.email, 
			pe.username, 
			pe.created_ip_address 
			FROM persons pe  
			WHERE pe.id = "'.$user_id.'"'; 
								
			// echo $queryPerson . '<br /><hr />';
				
			$result_person = mysqli_query($connection, $queryPerson);
			
			if (!$result_person) {
				show_mysqli_error_message($queryPerson, $connection);
				die;
			}
			
			while($r = mysqli_fetch_assoc($result_person)) { 
    			

    			if ($r['first_name']) { 
        			$display_name = $r['first_name'];
                } else {
                    $display_name = $r['username'];  
                }
					
				$_SESSION['message_type'] = 'alert-box success radius';					
				$_SESSION['message'] = '<p><i class="fa fa-thumbs-up fa-2x"></i>&nbsp;&nbsp;Hi <b>'.$display_name.'</b>. Your <b>Member Account</b> has been <b>activated</b>. You may now <b>Login</b> below. After you Login we recommend you setup your <b>Phone, Shipping and Billing Addresses</b> so that you do not have to re-enter that information each time you make a purchase in the store.</p>';
					
				// Send confirmation email to Customer Support
				
				$from = $_SESSION['application']['name'].' <support@'.$_SESSION['application']['domain'];
				$to = 'support@'.$_SESSION['application']['domain'];
				$subject = 'SupplementRelief Member Account Activated';
				
				$body = '<b>Name: </b>'.$r['first_name'].' '.$r['last_name'];
				$body .= '<br><b>Username: </b>'.$r['username'];
				$body .= '<br><b>Email: </b>'.$r['email'];
				$body .= '<br><b>IP Address: </b>'.$r['created_ip_address'].'<br>';
	
				send_email_html($from, $to, $subject, $body, "");				
					
			}
			
			mysqli_free_result($result_person);		
			
		}

	}
	
	if ($_GET['msg'] == 2) {
		// Member Account registration email verification complete ???
		// session_destroy();

		$_SESSION['message_type'] = 'alert-box success radius';					
		$_SESSION['message'] = '<p><i class="fa fa-check"></i>&nbsp;Email verification complete. Please Login to use your updated Member Account.</p>';
				
	}	
	
	// Other Messages can be added here.
	
	show_session_message();
	unset($_SESSION['message_type']);
	unset($_SESSION['message']);		
}
	
?>

<script type="text/JavaScript" src="../js/sha512.js"></script> 
<script type="text/JavaScript" src="../js/forms.js"></script> 

<div class="row">
	<div class="small-12 columns">

		<form data-abide action="../php/login_process.php" method="post" name="login_form" id="login_form" onsubmit="submitLoginNow(event);">
			
			<!-- 
				1. ../js/forms.js 
				2. ../php/login_process.php
				3. ../includes/core.php function login
				
			-->
			
			<input type="hidden" name="cp" value="<?php echo $_SESSION['user']['token']; ?>">
		
			<fieldset>
			    <legend>Login to Your Account</legend>
			    
			    <div class="row">
			      <div class="small-12 columns">
				      
			      	<p>Please provide your <strong>User ID</strong> and <strong>Password</strong> then press the <b>LOGIN</b> button. If you do not have a Member Account you can <b>get one right now for free</b> <a href="../register-member-account/" title="Register for your FREE Member Account and enjoy Savings, Promotional Offers and additional Wellness Education content.">here</a>.</p>
	
			      </div>
			    </div>
			    		
				<div class="row">
								
				  <div class="small-12 medium-6 columns">
				    <label>User ID
				      <input type="text" required name="username" placeholder="User ID" value="<?php echo $_SESSION['user']['username']; ?>" />
				    </label>
				    <small class="error">User ID is required</small>    
				  </div>
				  
				  <div class="small-12 medium-6 columns">
				    <label>Password
				      <input type="password" required name="password" placeholder="********" id="password" onkeypress="submitLoginKey(event);"/>
				    </label>
				    <small class="error">Password is required</small>	    
				  </div>			  
				</div>
				
				<div class="row">			
					<div class="small-12 columns">
					    <label>	      
					      <input class="button large radius show-for-medium-up expand" type="submit" value="LOGIN" />
					      <input class="button large radius expand show-for-small-only" type="submit" value="LOGIN" />      
					    </label>
				  </div> 
				  
				</div>
				
				<p><a href="../request-user-id/" title="Request your User ID" style="text-decoration:underline;">Forgot User ID?</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="../reset-password/" title="Reset your Password" style="text-decoration:underline;">Forgot Password?</a></p>
				
				<p>If you need help please call <b><?php echo $_SESSION['application']['phone']; ?></b> or <a href="../contact/" title="Email or call <?php echo $_SESSION['application']['name']; ?> Customer Support for help with your Member Account" style="text-decoration:underline;"><b>email</b></a> and we will assist you.</p>
				
				<!-- <p>If you <b>forgot your Password</b>, or want to choose a new one, click <a href="../reset-password/" title="Reset your Password" style="text-decoration:underline;"><b>here</b></a>.&nbsp;&nbsp;If you need help please call <b>(888) 424-0032</b> or <a href="../contact/" title="Email or call SupplementRelief Customer Support for help with your Member Account" style="text-decoration:underline;"><b>email</b></a> and we will assist you.</p> -->
				
				<!--
				<div class="row">			
					<div class="small-12 columns">
				
						<p>If you <b>forgot your Username</b> you can request it <a href="../request_username/" title="Request that your Username be sent to you">here</a>. If you <b>forgot your Password</b>, or want to change it, you can reset it <a href="../reset-password/" title="Reset your Password">here</a>.</p>
				 
				  	</div>
				</div>
				-->
								
		  </fieldset>
		  		
		</form>
	</div>
</div>

<?php // include('../php/standard_membership_modal.php'); ?>