<?php

// echo 'Begin login_process.php.<br />';

include('../includes/header.php');

if (@isset($_POST['username'], $_POST['p'], $_POST["cp"])) {

    if ($_POST['cp'] == $_SESSION['user']['token']) {

        $username = sanitize($_POST['username']);
        $password = sanitize($_POST['p']); // The hashed password.
         
        if (login($username, $password, $connection) == true) {
	        
            // login function is in /includes/core.php
            // Login successful
            
            if (updateUserSessionLogin($connection, $_SESSION['userStatistics']['session_id'], $_SESSION['user']['user_id']) == true) {
                
                // User Session log updated successfully. Allow User to select desired Program Enrollment.
                header('Location: ../select-program-enrollment/'); 
                          
            } else {
                
                // User Session log update failed. System error.
                header('Location: ../login/?error=5');           
            }        
             
        } else {
            
            // Login failed 
            header('Location: ../login/?error=1');
        }
        
    } else {
        
        // The Session Variable does not equal sent token were not sent to this page. 
        echo "Invalid Request: Session token (" . $_SESSION['user']['token'] . ") does not match sent variable (" . sanitize($_POST["cp"]) . ")";
    }

} else {
    
        // The correct POST variables were not sent to this page. 
        echo 'Invalid Request';
}

?>