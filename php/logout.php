<?php
	
// echo 'Begin logout.php<br>';

if (isset($_SESSION['userStatistics']['session_id'])) {
	
	// Update user_sessions with the Logout timestamp
	$logout_timestamp = date('Y/m/d H:i:s');

	$queryUpdateUserSession = '
		UPDATE user_sessions SET 
		logout_timestamp = "'.$logout_timestamp.'" 
		WHERE id = "'.$_SESSION['userStatistics']['session_id'].'"';
	
	// echo $queryUpdateUserSession;
	
	$result_update_user_session = mysqli_query($connection, $queryUpdateUserSession);
	
	if (!$result_update_user_session) {	
		show_mysqli_error_message($queryUpdateUserSession, $connection);
		die;
	}
	
	if (isset($_GET['auto_logoff'])) {
		
		// Set in a new process to be developed by MindFire.
		$event = 'LOGOUT';
		$page = 'Logout';
		$log_description = 'User AUTO LOGGED OUT: '.$_SESSION['user']['username'].'.';
		$flag = 'NULL';
		create_system_log($connection, $event, $page, $log_description, $flag);
	
		$_SESSION['message_type'] = 'alert-box secondary radius';					
		$_SESSION['message'] = '
		<p><i class="fa fa-info fa-2x"></i>&nbsp;&nbsp;You were <b>automatically logged out of '.$_SESSION['enrollment']['project_program_name'].' at '.date('m/d/y h:i A T', strtotime($logout_timestamp)).' </b>after no activity in your browser for an extended period of time.</p>
		<p>You can Login again at any time.</p> 
		<a href="../login/" class="button large radius">Login</a> ';		
		
	} else {
		
		// normal Logout invoked by user
		$event = 'LOGOUT';
		$page = 'Logout';
		$log_description = 'User LOGGED OUT: '.$_SESSION['user']['username'].'.';
		$flag = 'NULL';
		create_system_log($connection, $event, $page, $log_description, $flag);
		
		$_SESSION['message_type'] = 'alert-box success radius';					
		$_SESSION['message'] = '<p><i class="fa fa-thumbs-up fa-2x"></i>&nbsp;&nbsp;You successfully <strong>logged out at '.date('m/d/y h:i A T', strtotime($logout_timestamp)).'</strong>. Please let us know if there is anything we can do to <a href="../contact/" style="color: white; text-decoration: underline;">better serve you</a>.</p>';
				
	}
	
}

show_session_message();	
unset($_SESSION['user']);
session_destroy();

?>