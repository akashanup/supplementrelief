<div id="recap1"></div>

<div id="recap2"></div>

<script type="text/javascript">
  // Support for multiple reCAPTCHAs
  var verifyCallback = function(response) {
    alert(response);
  };
  var widget1;
  var widget2;
  var onloadCallback = function() {
    // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
    // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
    widget1 = grecaptcha.render('recap1', {
      'sitekey' : "<%= ENV['recaptcha_public_key'] %>",
      'theme' : 'light'
    });
    widget2 = grecaptcha.render('recap2', {
      'sitekey' : "<%= ENV['recaptcha_public_key'] %>",
      'theme' : 'light'
    });
  };
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>