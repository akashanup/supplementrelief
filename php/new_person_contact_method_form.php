<?php

// echo 'Begin new_person_contact_method_form.php.<br />';

// show_array($_SESSION);

// Build the Select List for Product Specification Types

$queryContactMethodTypes = '
SELECT   
code, 
name 
FROM contact_method_types  
WHERE effective_date <= CURRENT_DATE 
AND (end_date is NULL or end_date >= CURRENT_DATE)  
ORDER BY name ASC';

// echo $queryContactMethodTypes;
// die();
	
// Execute the query
$result_contact_method_type = mysqli_query($connection, $queryContactMethodTypes);

if (!$result_contact_method_type) {
	show_mysqli_error_message($queryContactMethodTypes, $connection);
	die;
}
							
$contact_method_type_options = '<option value="">Select Contact Method...</option>';

while($contact_method_type_row = mysqli_fetch_array($result_contact_method_type)) {

	$selected = '';
	if(($contact_method_type_code ? $contact_method_type_code : $row1['p']) == $contact_method_type_row['code']) $selected = ' selected';
	$contact_method_type_options .= '<option value="'.$contact_method_type_row['code'].'" '.$selected.'>'.$contact_method_type_row['name'].'</option>';
}
				
mysqli_free_result($result_contact_method_type);

?>
				
<form id="createPersonContactMethod" data-abide action="../php/person_contact_method_crud.php" method="post">

	<input type="hidden" name="action" value="new"/>
	<input type="hidden" name="person_id" value="<?php echo $_SESSION['user']['user_id']; ?>"/>

		<fieldset>
    	<legend>New Contact Method</legend>
        		
		<div class="row">
			
			<div class="small-12 medium-4 columns">
				<label>Contact Method <small>required</small>
			  	<select id="newPersonContactMethod" required name="contact_method_type_code">
			        <?php echo $contact_method_type_options; ?>
			  	</select>
			  </label>
			  <small class="error">Contact Type is required</small>
			</div>
			
			<div class="small-12 medium-8 columns">
				<label>Value <small>Required</small>
				  	<input type="text" id="newPersonContactMethodValue" required name="contact_method_value" maxlength="100" disabled value="<?php echo ($contact_value ? $contact_value : ''); ?>" />  	
			 	</label>
			 	<small class="error">Contact Method Value is required</small>
			</div>  	    	  	    	
			
		</div>
		
		<div class="row">								
			<div class="small-12 columns">
					<div id="newPersonContactMethodDateErrorMessage" style="display:none;"></div>		
			</div>
		</div>
							
		<div class="row">
									
			<div class="small-12 medium-3 columns">
				<label>Effective Date <small>Required</small>
					<input type="text" id="newPersonContactMethodEffectiveDate" class="datepicker_date maskDateField" required name="effective_date" placeholder="MM/DD/YYYY" value="<?php echo date('m/d/Y'); ?>" />
				</label>
				<small class="error">Effective Date is required and must be formatted as MM/DD/YYYY</small> 	  
			</div>
			
			<div class="small-12 medium-3 columns end">
				<label>End Date
					<input type="text" id="newPersonContactMethodEndDate" class="datepicker_date maskDateField" name="end_date" placeholder="MM/DD/YYYY" value="<?php echo ($end_date ? $end_date : ''); ?>" />
				</label>
				<small class="error">End Date must be formatted as MM/DD/YYYY</small> 	  
			</div>
								
		</div>	
		
		<div class="row">
			<div class="small-12 columns">
		  	<label>	      
		    	<input id="newPersonContactMethodSubmit" type="button" class="button small radius" value="Create">
		    	<a href="#" class="button small radius" onclick="newPersonContactMethodCancel();return false"><b>Cancel</b></a>	      
		    </label>
		  </div>
		</div>
					
  	</fieldset>
	
</form>