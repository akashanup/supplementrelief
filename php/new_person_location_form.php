<?php

// echo 'Begin new_person_location_form.php.<br />';

// show_array($_SESSION);

// Build the Select List for Address Type

$address_type_query = 'SELECT  
code, 
name AS address_type_name 
FROM address_types 
WHERE code in ("BILL", "SHIP")  
ORDER BY address_type_name ASC';

// echo $address_type_query .'<br /><hr />';

$address_type_result = mysql_query($address_type_query);
$address_type_options = '<option value="">Select Address Type...</option>';

while($address_type_row = mysql_fetch_array($address_type_result)) {
	$selected = '';
	// if(($_GET['address_type_code'] ? $_GET['address_type_code'] : $row1['p']) == $address_type_row['code']) $selected = ' selected';
	$address_type_options .= '<option value="'.$address_type_row['code'].'" '.$selected.'>'.$address_type_row['address_type_name'].'</option>';
}

mysql_free_result($address_type_result);

// Build the Select List for States
$queryStates = '
SELECT   
state_id, 
state_abbr, 
state_name, 
country_abbr   
FROM states 
ORDER BY country_abbr DESC, state_name ASC';


// echo $queryStates;
// die();

// Execute the query
$result_state = mysqli_query($connection, $queryStates);

if (!$result_state) {
	show_mysqli_error_message($queryStates, $connection);
	die;
}
							
$state_options = '<option value="">Select State...</option>';

while($state_row = mysqli_fetch_array($result_state)) {

	$selected = '';
	// if(($state_abbre ? $state_abbr : $row1['p']) == $state_row['abbr']) $selected = ' selected';
	// $state_options .= '<option value="'.$state_row['state_abbr'].'" '.$selected.'>'.$state_row['state_name'].' ('.$state_row['state_abbr'].') - '.$state_row['country_abbr'].'</option>';
	$state_options .= '<option value="'.$state_row['state_id'].'" '.$selected.'>'.$state_row['state_name'].' ('.$state_row['state_abbr'].') - '.$state_row['country_abbr'].'</option>';

}
				
mysqli_free_result($result_state);

// Build the Select List for Countries
$queryCountries = '
SELECT   
country_id, 
country_abbr, 
country_name 
FROM country  
ORDER BY country_name ASC';

// echo $queryCountries;
// die();

// Execute the query
$result_country = mysqli_query($connection, $queryCountries);

if (!$result_country) {
	show_mysqli_error_message($queryCountries, $connection);
	die;
}
							
$country_options = '
<option value="">Select Country...</option>
<option value="223">United States (US)</option>
<option value="36">Canada (CA)</option>
<option>....................</option>';

while($country_row = mysqli_fetch_array($result_country)) {

	$selected = '';
	// if(($state_abbre ? $state_abbr : $row1['p']) == $state_row['abbr']) $selected = ' selected';
	$country_options .= '<option value="'.$country_row['country_id'].'" '.$selected.'>'.$country_row['country_name'].' ('.$country_row['country_abbr'].')</option>';
}
				
mysqli_free_result($result_country);
?>
				
<form id="createPersonLocation" data-abide action="../php/user_dashboard_person_location_crud.php" method="post">
	
	<input type="hidden" name="action" value="new"/>

		<fieldset>
    	<legend>New Address</legend>
       		
		<div class="row">
		
			<div class="small-12 columns">
			  	<label>Address Type: <small>Required</small>
			    	<select name="address_type_code" required>
			          <?php echo $address_type_options; ?>
			    	</select>
			    	<small class="error">Address Type is required</small>
			  	</label>
			</div>
			
		</div>
		
		<div class="row">
				
			<div class="small-12 medium-6 columns">
				
				<label>Address <small>Required</small>
						<textarea required name="address" placeholder="2501 Easy St." rows="3" /><?php echo $location_address; ?></textarea> 
				</label>
				<small class="error">Address is required</small>
				
			</div>
			
			<div class="small-12 medium-6 columns">
				
				<label>City <small>Required</small>
	    	 	  	<input type="text" required name="city" placeholder="Detroit" value="<?php echo ($location_city ? $account_city : ''); ?>" />  	
	    	  	</label>
	    	  	<small class="error">City is required</small>

			</div>
					
		</div>
					
		<div class="row">
				    		   			
			<div class="small-12 medium-4 columns">	
				    						    	
				<label>State <small>Required</small>
				<select name="state_id">
					<?php echo $state_options; ?>
				</select>
				</label>
				<small class="error">State is required</small>				    	
		    	
		    </div>		    		    		
	    	
	    	<div class="small-12 medium-4 columns">
		    	<label>Postal Code <small>Required</small>
		    		  <input type="text" required name="postal_code" placeholder="48201, 48201-1234, K8N 5W6" value="<?php echo ($location_postal_code ? $location_postal_code : ''); ?>" /> 		  
		    	</label>
		    	<small class="error">Postal Code is required</small>	
		    </div>
		    
		    <div class="small-12 medium-4 columns">
		    
		    	<label>Country <small>Required</small>
		    		<select name="country_id">
		    	      <?php echo $country_options; ?>
		    		</select>
		    	</label>
		    	<small class="error">Country is required</small>				    	
		    	
		    </div>
	    
		</div>
			
		<!-- 				
		<div class="row">
								
			<div class="small-12 medium-3 columns">
				<label>Effective Date <small>Required</small>
					<input type="text" class="datepicker_date" required name="location_effective_date" placeholder="MM/DD/YYYY" value="<?php echo date('m/d/Y'); ?>" />
				</label>
				<small class="error">Effective Date is required and must be formatted as MM/DD/YYYY</small> 	  
			</div>
			
			<div class="small-12 medium-3 columns end">
				<label>End Date
					<input type="text" class="datepicker_date" name="location_end_date" placeholder="MM/DD/YYYY" value="<?php echo ($end_date ? $end_date : ''); ?>" />
				</label>
				<small class="error">End Date must be formatted as MM/DD/YYYY</small> 	  
			</div>				
			
		</div>
		-->	
				
		<div class="row">
			<div class="small-12 columns">
		  	<label>
		  		<!-- <input id="newPersonLocationSubmit" type="button" class="button small radius" value="Create"> -->	      
		    	<input type="submit" class="button small radius" value="Create">
		    	<a href="#" class="button small radius" onclick="newPersonLocationCancel();return false"><b>Cancel</b></a>	      
		    </label>
		  </div>
		</div>
		
					
  	</fieldset>
	
</form>