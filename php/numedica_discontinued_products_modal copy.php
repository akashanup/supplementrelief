<!--googleoff: index-->
<div id="discontinuedProducts" class="reveal-modal" data-reveal>
	<h2>NuMedica Discontinued Products <small>May 14, 2015</small></h2>
	
	<table width="100%">
			
		<thead>
			<tr>
			  <th>Product</th>
			  <th>Recommended Alternative</th>   			
			</tr>	
		</thead>
		
		<tr>
			<td>NuMedica Acetyl Glutathione - 60c</td>
			<td></td>
		</tr>
		<tr>
			<td>NuMedica Allerstop - 90c</td>
			<td></td>
		</tr>
		<tr>
			<td>NuMedica Aprozyme - 90t</td>
			<td></td>
		</tr>
		<tr>
			<td>NuMedica B-12 / Folic Acid Drops - 1 oz</td>
			<td></td>
		</tr>
		<tr>
			<td>NuMedica Calcium D-Glucarate USP - 90c</td>
			<td></td>
		</tr>
		<tr>
			<td>NuMedica ChelaClear - 120c</td>
			<td><a href="../product/?p_id=1&bpu_id=243">NuMedica HM Protect</a></td>
		</tr>
		<tr>
			<td>NuMedica D-Ribose - 60 svgs</td>
			<td><a href="../product/?p_id=1&bpu_id=41">NuMedica D-Ribose with Peak ATP</a></td>
		</tr>
		<tr>
			<td>NuMedica Fiber Factors - 30 svgs</td>
			<td><a href="../product/?p_id=1&bpu_id=271">NuMedica Fiber Factors</a></td>
		</tr>
		<tr>
			<td>NuMedica Garlic EC (Enteric Coated) - 90t</td>
			<td></td>
		</tr>
        <tr>
			<td>NuMedica Ginko Bilboa - 60c</td>
			<td><a href="../numedica-cognitive-balance/">NuMedica Cognitive Balance - 120c</a></td>
		</tr>

		<tr>
			<td>NuMedica Glucosamine Sulfate - 60c</td>
			<td></td>
		</tr>
		<tr>
			<td>NuMedica Heprone - 90t</td>
			<td></td>
		</tr>
		<tr>
			<td>NuMedica Immuno PRP Topical Drops - 1 oz</td>
			<td></td>
		</tr>
		<tr>
			<td>NuMedica ImmunoGraphis - 60c</td>
			<td></td>
		</tr>
		<tr>
			<td>NuMedica Joint Tri-Basic - 120c</td>
			<td><a href="../product/?p_id=1&bpu_id=95">NuMedica Joint Replete</a></td>
		</tr>
		<tr>
			<td>NuMedica L-Glutamine - 90c</td>
			<td><a href="../product/?p_id=1&bpu_id=102">NuMedica L-Glutamine Powder</a></td>
		</tr>
		<tr>
			<td>NuMedica NuVision - New and Improved - 60c</td>
			<td></td>
		</tr>
		<tr>
			<td>NuMedica Omega 300 EC - 90 sfgl</td>
			<td><a href="../product/?p_id=1&bpu_id=143">NuMedica Omega 600/780/950 USP</a></td>
		</tr>
		<tr>
			<td>NuMedica PhosphaLife PPC - 100 sfgl</td>
			<td></td>
		</tr>
		<tr>
			<td>uMedica Pro InflaMed Vanilla - 14 svgs</td>
			<td><a href="../product/?p_id=1&bpu_id=166">NuMedica ProGI Soothe</a></td>
		</tr>
		<tr>
			<td>NuMedica Progensa - 4 oz</td>
			<td></td>
		</tr>
		<tr>
			<td>NuMedica Renex - 90t</td>
			<td></td>
		</tr>
		<tr>
			<td>NuMedica ResveraPlex 500 - 60c</td>
			<td><a href="../product/?p_id=1&bpu_id=266">NuMedica ResveraPlex Plus</a></td>
		</tr>
		<tr>
			<td>NuMedica Salicin-B - 120c</td>
			<td><a href="../product/?p_id=1&bpu_id=178">NuMedica Salicin-B IC (Intensive Care)</a></td>
		</tr>
		<tr>
			<td>NuMedica SedaCalm REM - 60c</td>
			<td></td>
		</tr>
		<tr>
			<td>NuMedica SlimFit Banana - 14 svgs</td>
			<td><a href="../product/?p_id=1&bpu_id=183">NuMedica SlimFit Protein - Chocolate/Vanilla</a></td>
		</tr>
		<tr>
			<td>NuMedica T-100 - 60t</td>
			<td><a href="../product/?p_id=1&bpu_id=188">NuMedica Thyrodex T-150</a></td>
		</tr>
		
	</table>
 
	<a class="close-reveal-modal">&#215;</a>
</div>
<!--googleon: index-->
