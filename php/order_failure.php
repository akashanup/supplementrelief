<?php
	
// echo 'Begin order_failure.php<br>';

// show_array($_GET);

$queryOrder = 'SELECT   
id  
FROM orders  
WHERE hash = "'.$_SESSION[‘order_id_hash’].'"';

// echo $queryOrder;
// die();

$result_order = mysqli_query($connection, $queryOrder);

if (!$result_order) {
	show_mysqli_error_message($queryOrder, $connection);
	die;
}

$rowcount = mysqli_num_rows($result_order);

if ( $rowcount < 0 || !isset($_SESSION['checkout_approved']) ) {
	
	$_SESSION['message_type'] = 'alert-box alert radius';		
	$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-2x"></i>&nbsp;&nbsp;A <b>valid Order</b> was not found. If you had an order in progress it has been cancelled. You may continue shopping shopping by selecting <b>NuMedica</b> from the menu at the top of the page.</p>';
	
	show_session_message();
	
	// header("location: ../customer_order_lookup");
	// exit();
		
} else {
	
	// Check session variables for Order Success and Token and display Order Confirmation to user.

	?>
	<br>
	<div class="row">		
		<div class="small-12 columns">

			<?php echo $_SESSION['transaction_html']; ?>
			
			<a href="../store/php/checkout.php" class="radius button_check_out expand button success">Back to Checkout Page</a>
			
		</div>
	</div>
	<?php

	unset($_SESSION['checkout_approved']);
}

mysqli_free_result($result_order);


// Add Google Adwords and Bing Ads tracking javascript to the ../order_success/ page.

?>