<?php
	
// echo 'Begin order_success.php<br>';

// show_array($_GET);

// show_array($_SESSION);

$queryOrder = 'SELECT   
id  
FROM orders  
WHERE hash = "'.$_SESSION['order_id_hash'].'"';

// echo $queryOrder;
// die();

$result_order = mysqli_query($connection, $queryOrder);

if (!$result_order) {
	show_mysqli_error_message($queryOrder, $connection);
	die;
}

$rowcount = mysqli_num_rows($result_order);

if ( $rowcount < 0 || !isset($_SESSION['checkout_approved'])) {
	
	$_SESSION['message_type'] = 'alert-box alert radius';
	$_SESSION['message'] = '<p><i class="fa fa-exclamation-triangle fa-2x"></i>&nbsp;&nbsp;
	An <b>Order</b> was not found. Please choose an option from the Main Menu.</p>';
	
	show_session_message();
	
	// header("location: ../customer_order_lookup");
	// exit();
		
} else {
	//Save user's address if not saved.
	if(isset($_SESSION['user']['user_id']))
	{	
		$current_user_id = $_SESSION['user']['user_id'];
		checkForAddress($connection, $current_user_id);	
		checkForPhone($connection, $current_user_id);
	}

	// Check session variables for Order Success and Token and display Order Confirmation to user.
	
	?>
	<br>
	<div class="row">
		<div class="small-12 columns">

        <?php echo $_SESSION['transaction_html']; ?>

			<div class="row">
				
				<div class="small-12 medium-6 columns">
					<a href="../numedica/" class="continue_shopping radius button_shopping expand button">Continue Shopping</a>
				</div>
				
				<?php	
				if (isset($_SESSION['user']['user_id'])) {
                    ?>
    				<div class="small-12 medium-6 columns">
					    <a href="../logout/" class="continue_shopping radius button_shopping expand button">Logout</a>
				    </div>
				    <?php
				}
				?>
				
			</div>
				
		</div>
	</div>
	
	<?php
	unset($_SESSION['checkout_approved']);
	unset($_SESSION['checkout']['shipping_type']);
}

mysqli_free_result($result_order);

// Add Google Adwords and Bing Ads tracking javascript to the ../order_success/ page.

?>