<?php

include_once('../includes/header.php');

// show_array($_SERVER);
// $_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'];
$_SESSION['target_uri'] = 'http://wrightcheck.com/supplementrelief/member-dashboard/#fndtn-contacts';
// show_array($_POST);
// die();

// Check to see if the form record submitted is unique (Person, Method, Value)
if (isset($_POST['action']) && ($_POST['action'] == 'new' OR $_POST['action'] == 'edit')) {

	$person_id = addslashes(cleanEncoding($_POST['person_id']));
	$contact_method_type_code = addslashes(cleanEncoding($_POST['contact_method_type_code']));
	$contact_method_value = addslashes(cleanEncoding($_POST['contact_method_value']));	

	if ($_POST['action'] == 'new') {
		
		$queryUniquePersonContactMethod = 'SELECT * 
			FROM person_contact_methods
			WHERE person_id = "'.$person_id.'"
			AND contact_method_type_code = "'.$contact_method_type_code.'" 
			AND contact_method_value = "'.$contact_method_value.'"';	
		
	} elseif ($_POST['action'] == 'edit') {
	
		$contact_method_id = addslashes(cleanEncoding($_POST['contact_method_id']));
	
		$queryUniquePersonContactMethod = 'SELECT * 
			FROM person_contact_methods
			WHERE person_id = "'.$person_id.'"
			AND contact_method_type_code = "'.$contact_method_type_code.'" 
			AND contact_method_value = "'.$contact_method_value.'" 
			AND id != "'.$contact_method_id.'"';		
		
	}
					
	// echo $queryUniquePersonContactMethod;
	// die();
	
	$result_unique = mysqli_query($connection, $queryUniquePersonContactMethod);
	
	if (!$result_unique) {
		show_mysqli_error_message($queryUniquePersonContactMethod, $connection);
		die;
	}
	
	$total_records = mysqli_num_rows($result_unique);
		
	if ($total_records > 0) {
			
		// User error message		
		$_SESSION['message_type'] = 'alert-box alert radius';		
		$_SESSION['message'] = '<p>You already have a Contact record of <b>type: '.$contact_method_type_code.'</b> and <b>value: '.$contact_method_value.'</b>. Please use it or create/edit a different one.</p>';	
		
		// Return to calling form using target uri	
		// header("location: ".$_SESSION['target_uri']);
		echo 'Person Contact Method already exist and must be unique.';
		exit();		
		
	}
	
}
	
if (isset($_POST['action']) && $_POST['action'] == 'new') {

	// show_array($_POST);
	
	// set POST return values to local variables for processing
	// primary key (ID) is generated in the database
	$effective_date = sql_date(addslashes(cleanEncoding($_POST['effective_date'])));
	$end_date = sql_date(addslashes(cleanEncoding($_POST['end_date'])));
						
	// Insert the User record
	
	date_default_timezone_set('America/Detroit'); // set in core.php
	$created_timestamp = date('Y-m-d h:i:s');
				
	$queryInsert = '
	INSERT INTO person_contact_methods (person_id, contact_method_type_code, contact_method_value, effective_date, end_date, created_by, created_timestamp) 
	VALUES (
	"'.$person_id.'", 
	"'.$contact_method_type_code.'", 
	"'.$contact_method_value.'", 	
	"'.$effective_date.'", 
	'.no_value_null_check($end_date).',
	"'.$_SESSION['user']['user_id'].'",
	"'.$created_timestamp.'")';
			
	// echo $queryInsert;	
	// die();
	
	$result_insert = mysqli_query($connection, $queryInsert);
			
	if (!$result_insert) {
		show_mysqli_error_message($queryInsert, $connection);
		echo 'Insert failed.';
		die;
	}
	
	// $last_insert_id = mysqli_insert_id($connection); 		
	
	mysqli_free_result($result_insert);
	
	// User confirmation message		
	// $_SESSION['message_type'] = 'alert-box success radius';		
	// $_SESSION['message'] = '<p>Your Contact Method with value:  <b>'.$contact_method_value.'</b> was successfully created.</p>';
								
}

if (isset($_POST['action']) && $_POST['action'] == 'edit') {

	// show_array($_POST);
	
	// set POST return values to local variables for processing
	$contact_method_id = addslashes(cleanEncoding($_POST['contact_method_id']));	
	$person_id = addslashes(cleanEncoding($_POST['person_id']));
	$contact_method_type_code = addslashes(cleanEncoding($_POST['contact_method_type_code']));
	$contact_method_value = addslashes(cleanEncoding($_POST['contact_method_value']));
	$effective_date = addslashes(cleanEncoding($_POST['effective_date']));
	$end_date = addslashes(cleanEncoding($_POST['end_date']));
			
	// Update the record
	
	date_default_timezone_set('America/Detroit');
	$modified_timestamp = date('Y-m-d h:i:s');
	
	$queryUpdate = '
	UPDATE person_contact_methods SET 
	contact_method_type_code = "'.$contact_method_type_code.'", 
	contact_method_value = "'.$contact_method_value.'", 
	effective_date = "'.sql_date($effective_date).'", 
	end_date = '.no_value_null_check(sql_date($end_date)).', 
	modified_by = '.$_SESSION['user']['user_id'].', 
	modified_timestamp = "'.$modified_timestamp.'" 
	WHERE id = "'.$contact_method_id.'"';	
	
	// echo $queryUpdateApplication;	
	// die();
		
	$result_update = mysqli_query($connection, $queryUpdate);
	
	if (!$result_update) {
		show_mysqli_error_message($queryUpdate, $connection);
		die;
	}
	
	mysqli_free_result($result_update);
		
	// $target_uri = $_SERVER['HTTP_ORIGIN'].$_SERVER['SCRIPT_NAME'].'?keyword='.$last_insert_id.'#listing';
	// echo 'Target URI: '.$target_uri.'<<br /><hr />';	
	
	// User confirmation message		
	// $_SESSION['message_type'] = 'alert-box success radius';		
	// $_SESSION['message'] = '<p>Your Contact Method with value:  <b>'.$contact_method_value.'</b> was successfully updated.</p>';
						
}

if (isset($_POST['action']) && $_POST['action'] == 'delete') {

	// show_array($_POST);
	
	// set POST return values to local variables for processing
	$contact_method_id = addslashes(cleanEncoding($_POST['contact_method_id']));	
	$person_id = addslashes(cleanEncoding($_POST['person_id']));
	$contact_method_type_code = addslashes(cleanEncoding($_POST['contact_method_type_code']));
	$contact_method_value = addslashes(cleanEncoding($_POST['contact_method_value']));
		
	$queryDelete = ' DELETE FROM person_contact_methods WHERE id = "'.$contact_method_id.'" ';

	$result_delete = mysqli_query($connection, $queryDelete);
						
	if (!$result_delete) {
		show_mysqli_error_message($queryDelete, $connection);
		die;
	}
	
	mysqli_free_result($result_delete);	
		
	// User confirmation message
	// $_SESSION['message_type'] = 'alert-box success radius';				
	// $_SESSION['message'] = '<p>Your <strong>Contact Method </strong> has been deleted.</p>';
}

// Return to calling form using target uri	
// header("location: ".$_SESSION['target_uri']);
// exit();	

?>