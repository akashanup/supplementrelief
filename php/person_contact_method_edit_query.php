<?php

// echo 'Begin edit_person_contact_method_form.php.<br />';

include_once('../includes/header.php');

// show_array($_POST);

if (isset($_POST['action']) && $_POST['action'] == 'edit') {
	// set POST return values to local variables	
	$contact_method_id = safe_sql_data($connection, $_POST['contact_method_id']);		
}

$queryPersonContactMethod	= 'SELECT 
	pcm.id AS contact_method_id, 
	pcm.person_id, 
	pcm.contact_method_type_code,  
	cmt.name AS contact_method_type_name,
	pcm.contact_method_value, 
	pcm.effective_date, 
	pcm.end_date  
	FROM person_contact_methods pcm 
	LEFT JOIN contact_method_types cmt ON pcm.contact_method_type_code = cmt.code 
	WHERE pcm.id = "'.$contact_method_id.'"';   
			
// echo $queryPersonContactMethod . '<br /><hr />';

$result_person_contact_method = mysqli_query($connection, $queryPersonContactMethod);

if (!$result_person_contact_method) {
	show_mysqli_error_message($queryPersonContactMethod, $connection);
	die;
}

while($r = mysqli_fetch_assoc($result_person_contact_method)) {

	// show_array($r);
	
	$contact_method_id = $r['contact_method_id'];
	$person_id = $r['person_id'];	
	$contact_method_type_code = $r['contact_method_type_code'];	
	$contact_method_type_name = $r['contact_method_type_name'];
	$contact_method_value = $r['contact_method_value'];
	$effective_date = hdate($r['effective_date']);
	$end_date = hdate($r['end_date']);
		
}

mysqli_free_result($result_person_contact_method);

/*echo '<?xml version="1.0" encoding="utf-8"?>
<ContactMethodId>' . $contact_method_id . '</ContactMethodId>
<ContactMethodTypeCode>' . $contact_method_type_code . '</ContactMethodTypeCode>
<ContactMethodValue>' . $contact_method_value . '</ContactMethodValue>
<EffectiveDate>' . $effective_date . '</EffectiveDate>
<EndDate>' . $end_date . '</EndDate>';*/

echo json_encode(array(	"ContactMethodId"=>$contact_method_id,
												"ContactMethodTypeCode"=>$contact_method_type_code,
												"ContactMethodValue"=>$contact_method_value,
												"ContactMethodEffectiveDate"=>$effective_date,
												"ContactMethodEndDate"=>$end_date
												)); 



