<?php 

// echo 'Begin person_crud.php.<br />';

include_once('../includes/header.php');

// show_array($_SESSION);
// show_array($_SERVER);

// Capture calling URI to return after processing.
$_SESSION['target_uri'] = '../member-dashboard/#fndtn-account';

if (isset($_POST['action']) && $_POST['action'] == 'edit') {
	
	// show_array($_POST);
	// die;

	$person_id = safe_sql_data($connection, $_POST['person_id']);
	$person_first_name = safe_sql_data($connection, $_POST['first_name']);
	$person_last_name = safe_sql_data($connection, $_POST['last_name']);
	$person_phone = safe_sql_data($connection, $_POST['phone']);
	$person_birth_date = safe_sql_data($connection, $_POST['birth_date']);
	$person_gender = safe_sql_data($connection, $_POST['gender']);
				
	if (strlen($person_phone) > 0) { 
		
		$queryAddressCountry = 'SELECT 
			cnty.country_abbr 
			FROM locations loc 
			LEFT JOIN addresses addr ON loc.address_id = addr.id 
			LEFT JOIN country cnty ON addr.country_id = cnty.country_id 
			WHERE loc.person_id = "'.$person_id.'" 
			AND cnty.country_abbr NOT IN ("CA", "US")';
			
		// echo $queryAddressCountry;
		// die;
			
		$resultAddressCountry =	mysqli_query($connection, $queryAddressCountry);
  		$totalAddresses = mysqli_num_rows($resultAddressCountry);
  		
  		if ($totalAddresses > 0) {
	  		$formattedPhone = $person_phone;
	  	} else { 
	  		$formattedPhone = sanitizePhone($person_phone, "US");
	  	}
	  	 				
	} else {
		
		$formattedPhone = '';

	} 	
	
	$queryUpdatePerson = 'UPDATE persons SET 
	first_name = "'.initCapData($person_first_name).'",  
	last_name = "'.initCapData($person_last_name).'", 
	phone = '.no_value_null_check($formattedPhone).',
	birth_date = '.no_value_null_check(sql_date($person_birth_date)).', 	
	gender = '.no_value_null_check($person_gender).' 
	WHERE id = "'.$_SESSION['user']['user_id'].'"';
		
	// echo $queryPerson;
	
	$result_update_person = mysqli_query($connection, $queryUpdatePerson);
	
	if (!$result_update_person) {
		show_mysqli_error_message($queryUpdatePerson, $connection);
		die;
	}
		
	mysqli_free_result($result_update_person);  
		
	// Result Message to User
	$_SESSION['message_type'] = 'alert-box success radius';				
	$_SESSION['message'] = '<p><i class="fa fa-check fa-lg" aria-hidden="true"></i>&nbsp;Your <strong>Account Information</strong> has been updated.</p>';
		
}

// Return to calling form using target uri	
header("location: ".$_SESSION['target_uri']);
exit();	

?>