<?php
include '../includes/header.php';

if (!$_SERVER['REQUEST_METHOD'] == "POST") {
	echo "You literally can't even.";
	exit;
}

// show_array($_POST);

if (@isset($_POST['si'], $_POST['pr'], $_POST["p"])) {
	// Changing password
		
	// show_array($_POST);
	// die;
	
	$user_id = sanitize($_POST['si']); // user id
	$pr = sanitize($_POST['pr']); // temporary password
	
	if ($stmt = $connection->prepare("SELECT id FROM persons 
		WHERE id = ?
        LIMIT 1")) {
        $stmt->bind_param('s', $user_id);  // Bind parameters.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($askee);
        $stmt->fetch();
	
		if ($user_id == $askee) {
			
			// echo 'prat.php - valid user id about to update new password to database.';
			
			$password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
			
			if (strlen($password) != 128) {
				// The hashed pwd should be 128 characters long.
				// If it's not, something really odd has happened
				$error_msg .= 'Invalid password configuration.<br><br><input class="button medium radius" type="button" value="RESET PASSWORD" onClick="doChangePass();">';
			}
	
			// Create a random salt
			$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));

			// Create salted password 
			$password = hash('sha512', $password . $random_salt);

			// Update the password in the database 
			if ($stmt = $connection->prepare("UPDATE persons SET " .
														"password_hash='" . $password . "', " . 
														"salt='" . $random_salt . "', " . 
														"temp_password=''" . 
														" WHERE id = ?")) {
				// Bind "$user_id" to parameter. 
				$stmt->bind_param('i', $user_id);
				$stmt->execute();
							
				// TODO: Change this to the real login page
				echo 'Your password has been reset. You may use it the next time you login.';
				exit;
			} else {
				echo 'Update failed (Error: DB)<br><br><input class="button medium radius" type="button" value="RESET PASSWORD" onClick="doChangePass();">';
				exit;
			}
			$stmt->close();
		}
	}
}

?>