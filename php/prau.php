<?php 

// echo 'Begin Reset Password Entry page.<br />';

include_once('../includes/header.php');

getPage('Reset Password Entry', $connection);

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

// show_session();

if (!$_SERVER['REQUEST_METHOD'] == "GET") {
	echo "You literally can't even.";
	exit;
}
	
if (@isset($_GET['si'], $_GET['uc'])) {
	// Unlocking Account
	$si = sanitize($_GET['si']);
	$uc = sanitize($_GET['uc']);
	
	 if ($stmt = $connection->prepare("SELECT temp_password FROM persons 
		WHERE id = ?
        LIMIT 1")) {
        $stmt->bind_param('s', $si);  // Bind parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($askee);
        $stmt->fetch();
		$stmt->close();
		
		// Adam 8/12/15 3:58pm. In the password reset/account activation/etc. functions, there is a major security vulnerability that I've just found.
		// ANY TIME if ($x == $askee) APPEARS IN OUR CODE IT MUST BE CHANGED TO if ($x !== "" && $x == $askee). Navigating to prau/prat with valid id (i.e. si=someid) and a blank auth 		// string, it will validate and present access for any id that has an empty reset code column in the db.
		// if ($uc == $askee) {
		if ($uc !== "" && $uc == $askee) {		
			$connection->query("DELETE FROM login_attempts WHERE user_id = '$si'");
            $stmt->close();
			
			$connection->query("UPDATE persons SET temp_password='' WHERE id = '$si'");
            $stmt->close();
			
			/*global $subdomain;
			
			if ($role[0] == "f") {
					$subdomain = "fitness.";
				} else if ($role[0] == "t") {
					$subdomain = "training.";
				} else if ($role[0] == "r") {
					$subdomain = "rep.";
				} else if ($role[0] == "l") {
					$subdomain = "licensee.";
				}
			
			echo 'Your account has been unlocked. You may now login <a href="http://' . 
			$subdomain . 'disruptivestrong.com/login.html">here</a>.';*/
			
			echo 'Your account has been unlocked. You may now login <a href="../login/">here</a>.';

			
			exit;
			
		} else {
			echo "Oops! Something went wrong.";
			exit;
		}
	}
} else if (@isset($_GET['si'], $_GET['pr'])) {
	// Resetting password
	$si = sanitize($_GET['si']);
	$pr = sanitize($_GET['pr']);
	
	if ($stmt = $connection->prepare("SELECT temp_password FROM persons 
		WHERE id = ?
        LIMIT 1")) {
        $stmt->bind_param('s', $si);  // user_id
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($askee); // temp_password
        $stmt->fetch();
		$stmt->close();
		
		// Adam 8/12/15 3:58pm. In the password reset/account activation/etc. functions, there is a major security vulnerability that I've just found.
		// ANY TIME if ($x == $askee) APPEARS IN OUR CODE IT MUST BE CHANGED TO if ($x !== "" && $x == $askee). Navigating to prau/prat with valid id (i.e. si=someid) and a blank auth 		// string, it will validate and present access for any id that has an empty reset code column in the db.
		// if ($pr == $askee) {
		if ($pr !== "" && $pr == $askee) {
		
			?>
			
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
			<script type="text/JavaScript" src="../js/sha512.js"></script> 
			<script type="text/JavaScript" src="../js/forms.js"></script>
			<script type="text/javascript">
			
			function doChangePass() {	
			
			
				if ($("#pass").val().length == 0 || $("#confpass").val().length == 0 ) {
					$("#callback").html('Please enter data into both fields.<br><br><input class="button medium radius expand" type="button" value="RESET PASSWORD" onClick="doChangePass();">');
					$("#pass").focus();
					return;
				}

				form_error = test_pass($("#pass").val(), $("#confpass").val());
				
				if ( form_error == '') {		
					
					$("#callback").load('prat.php', {
						'si': '<?php echo $si; ?>',
						'pr': '<?php echo $pr; ?>',
						'p': hex_sha512($("#pass").val())},
						function(response) {
					  $("#callback").html(response);
					});
					return;
					
				} else {
					//alert(form_error);
					$("#callback").html(form_error + '<br><br><input class="button medium radius expand" type="button" value="RESET PASSWORD" onClick="doChangePass();">'); 
					$("#pass").focus();
					return; 
				}
			
			}
			
			</script>
			
			<!-- Want to my own page to create this form that needs all of the standard Foundation and other project resources that this script prau.php does not have. -->
			
			<?php include('../php/reset_password_entry_form.php'); 
				
		} else {
			
			echo "<p>This password reset link has already been used.</p>";
			
		}
	}
} else if (@isset($_GET['si'], $_GET['aa'])) {
	// Activating Account
	$si = sanitize($_GET['si']);
	$aa = sanitize($_GET['aa']);
	
	if ($stmt = $connection->prepare("SELECT temp_password, username FROM persons 
		WHERE id = ?
        LIMIT 1")) {
        $stmt->bind_param('s', $si);  // Bind parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($askee, $un);
        $stmt->fetch();
		$stmt->close();
	}

	// Adam 8/12/15 3:58pm. In the password reset/account activation/etc. functions, there is a major security vulnerability that I've just found.
	// ANY TIME if ($x == $askee) APPEARS IN OUR CODE IT MUST BE CHANGED TO if ($x !== "" && $x == $askee). Navigating to prau/prat with valid id (i.e. si=someid) and a blank auth 		// string, it will validate and present access for any id that has an empty reset code column in the db.
	// 	if ($aa == $askee) {
	if ($aa !== "" && $aa == $askee) {
			
		// Set Account Status to Active
		if ($stmt = $connection->prepare("UPDATE persons SET account_status='" . 
										ACCOUNT_STATUS_ACTIVE .
										"', temp_password='' " . 
										"WHERE id = ?")) {
			// Bind "$si" to parameter. 
			$stmt->bind_param('i', $si);
			$stmt->execute(); // Execute the prepared query.
			
			// echo 'Your account has been activated. You may now login <a href="../login/">here</a>.';
						
			$stmt->close();
			// exit;
			
			$target_uri = '../login/?msg=1&u_id='.$si;
			// echo 'Target URI: '.$target_uri.'<br /><hr />';
			// die;
			header("location: $target_uri"); // Success message displayed to User on Login page.	

			// header('Location: ../login/?msg=1'); // Success message displayed to User on Login page.	
		
		} else {
			echo 'Activation failed (Error: DB)';
			$stmt->close();
			exit;
		}	
	
	}

}

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php'); 


?>
