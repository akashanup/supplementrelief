<?php

// echo 'Begin process_wright_path_campaign_opt_in.php<br /><hr />';

include('../includes/header.php');

// wright_path_campaign_id is the Primary Key for project_program_content_assets
$_SESSION['wright_path_campaign_id'] = $_POST['wright_path_campaign_id'];
// echo 'Campaign ID: '.$_SESSION['wright_path_campaign_id'].'<br /><hr />';

// print_r($_SESSION);

if (!empty($_SESSION['wright_path_campaign_id']) && !empty($_SESSION['enrollment']['enrollment_id'])) {

	$wright_path_campaign_id = $_SESSION['wright_path_campaign_id'];
	$enrollment_id = $_SESSION['enrollment']['enrollment_id'];
	
	// Get the Wright Path Campaign title to be displayed later
	$queryTitle	 = 'SELECT 
				ppca.content_asset_id, 
				ca.title  
				FROM project_program_content_assets ppca  
				LEFT JOIN content_assets ca on ppca.content_asset_id = ca.id 
				WHERE ppca.id = '.$wright_path_campaign_id.' 
				LIMIT 1';
				
	echo $queryTitle . '<br /><hr />';
	
	$result_list_campaign_title = mysqli_query($connection, $queryTitle);
	
	if (!$result_list_campaign_title) {
		echo $queryTitle . '<br /><hr />';
		die("Database List Project Program Content Asset query failed.");
	}
	
	while($r = mysqli_fetch_assoc($result_list_campaign_title)) {
		$campaign_title = $r['title'];
	}
	
	mysqli_free_result($result_list_campaign_title);
	
	// See if the user already has an active opt-in for the Wright Path Campaign chosen.	
	$queryCampaign	 = 'SELECT 
				eoi.id, 
				eoi.enrollment_id, 
				eoi.project_program_content_asset_id, 
				eoi.effective_date, 
				ca.title 
				FROM enrollment_opt_ins eoi  
				LEFT JOIN content_assets ca on eoi.project_program_content_asset_id = ca.id 
				WHERE eoi.project_program_content_asset_id = '.$wright_path_campaign_id.' 
				AND eoi.enrollment_id = '.$enrollment_id.' 
				AND status = "A" 
				LIMIT 1';
				
	// echo $queryCampaign . '<br /><hr />';
	
	$result_list_active_campaign = mysqli_query($connection, $queryCampaign);
	
	if (!$result_list_active_campaign) {
		echo $queryCampaign . '<br /><hr />';
		die("Database List Active Wright Path Campaign Opt-ins query failed.");
	}
	
	$rowcount = mysqli_num_rows($result_list_active_campaign);
	// printf("Result set has %d rows.\n",$rowcount);
	
	if ($rowcount == 0) {
	
		// Create an Enrollment Opt-in for this Wright Path Campaign.
		// date_default_timezone_set('America/Detroit');
		// $effective_date = date;
		$enrollment_status = "A";
	
		$queryInsertOptIn = 'INSERT INTO enrollment_opt_ins (enrollment_id, project_program_content_asset_id, status) VALUES (
		"'.$enrollment_id.'",
		"'.$wright_path_campaign_id.'",
		"'.$enrollment_status.'")';
		
		echo $queryInsertOptIn.'<br /><hr />';   
				
		$result_insert_opt_in_enrollment = mysqli_query($connection, $queryInsertOptIn);
					
		if (!$result_insert_opt_in_enrollment) {
			echo $queryInsertOptIn.'<br /><hr />';  
			echo 'MySql Error No: '.mysqli_error().': '.mysqli_errno().'<br><hr>';
			die("Database Insert into Enrollment Opt-ins failed.");
		}
		
		mysqli_free_result($result_insert_opt_in_enrollment);		
		
		$_SESSION['wright_path_campaign_opt_in_success'] = 'Y';
		$_SESSION['message_type'] = 'alert-box success radius';					
		$_SESSION['message'] = '<p>Thank you for choosing the Wright Path <b>'.$campaign_title.'</b> campaign. We hope you enjoy and learn from your daily emails!</p>';
		
	} else {
	
		// User already has an active opt-in for this Wright Path Campaign. Send a confirmation message.
	
		$_SESSION['wright_path_campaign_opt_in_success'] = 'Y';
		$_SESSION['message_type'] = 'alert-box success radius';		
		$_SESSION['message'] = '<p>Thank you for choosing the Wright Path '.$campaign_title.' campaign. We hope you enjoy and learn from your daily emails!</p>';
				
	}
		
	//echo $_SESSION['message'];
	
	header("location: ../user_dashboard/");	
	
} else {
	// User did not select a Wright Path Campaign so just return to the original page without any processing.
	header("location: ../user_dashboard/");	
}

//echo '<pre>';
//print_r($_SESSION);
//echo '</pre>';

?>