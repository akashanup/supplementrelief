<?php

// echo 'Begin program_registration.php.<br />';

include_once('../includes/header.php');

// show_session();
// die;

if (isset($_GET['pp_id'])) { 
	
	// show_array($_GET);
	$project_program_id = $_GET['pp_id'];	
	
}

if (isset($_GET['s'])) { 
	
	// show_array($_GET);
	$registration_status = $_GET['s'];	
	
}

// Get Project Program
$queryProjectProgram = '
SELECT 
pp.name AS project_program_name, 
pp.summary_description, 
pp.description, 
pp.effective_date, 
pg.name AS program_name,  
vi.host_url_mp4, 
vi.host_url_webm, 
im.host_url AS video_poster_image_url  
FROM project_programs pp 
LEFT JOIN programs pg ON pp.program_id = pg.id 
LEFT JOIN videos vi ON pp.video_id = vi.content_asset_id 
LEFT JOIN images im ON 
	(vi.image_id = im.content_asset_id AND 
	 im.size = "Video" AND 
	 im.usage_size = "Large" AND 
	 im.usage_shape = "Landscape" AND 
	 im.format = "JPG") 
WHERE pp.id = "'.$project_program_id.'"';

// echo $queryProjectProgram;
// die();

$result_project_program = mysql_query($queryProjectProgram);

if (!$result_project_program) {
	show_mysqli_error_message($queryProjectProgram, $connection);
	die;
}
					
while($r = mysql_fetch_array($result_project_program)) { 
	
	// show_array($r);
	
	$project_program_name = $r['project_program_name'];
	$summary_description = $r['summary_description'];
	$description = $r['description'];
	$effective_date = hdate($r['effective_date']);
	$program_name = $r['program_name'];
	$host_url_mp4 = $r['host_url_mp4'];
	$host_url_webm = $r['host_url_webm'];
	$video_poster_image_url = $r['video_poster_image_url'];

}

mysql_free_result($result_project_program);

?>

<br />

<div class="row">		
	<div class="small-12 columns">
		<div class="panel callout">
			<h2><?php echo $program_name.'<small>&nbsp;&nbsp;Begins '.$effective_date.'</small>'; ?></h2>
			<?php echo $summary_description; ?>
		</div>
	</div>
</div>

<!--
<div class="row">		
	<div class="small-12 columns">
		<div class="panel callout">
			<h2>Your Best Weight <small>Weight Loss Program</small></h2>
			
				<p><b>Your Best Weight</b> is offered <b>FREE OF CHARGE</b> to our <a href="#" data-reveal-id="standard-membership">SupplementRelief.com members</a>. This four-week online program, <b>led by Wellness expert Libby Wright</b>, begins <b>Monday, August 31, 2015</b>. Learn <a href="../about/">more</a> about Libby.</p>
				<p>Our unique education, based on real experience with our family and our clients, <b>helps you make informed decisions about supplements</b>, but goes above and beyond by <b>teaching you practical ways to live a healthier life right now</b>. Each week you will have new topical content including <b>articles, emails, videos, webinars, and discussion forums</b>.  You’ll also have access to extra tools like: <b>recipes, health focusing blogs, and, of course, great supplements!</b></p>
				<p>You should expect to commit about <b>one hour per week</b> to include 20 minutes reviewing the weekly topic content online (at a time convenient for you) and another 30 minutes virtually attending the <b>scheduled LIVE webinar</b>. The webinars will be offered at two different times each Tuesday 10am/7pm EDT to provide a scheduling option. More <a href="#program-information">program information</a> is provided below the registration form.</p>	
		</div>
	</div>
</div>
-->

<?php
	
if (!empty($host_url_mp4)) { 
	
	// echo 'We have a video.';
	
	if (!empty($video_poster_image_url)) {
		$video_poster = 'poster="'.$video_poster_image_url.'"';
		// echo $video_poster;
	} else {
		
		$video_poster = '';
	}
	
	?>
	
	<div class="row">
			
		<div class="small-12 columns">
			
			<a href="#" data-reveal-id="watchProgramVideo" class="button large radius success expand"><i class="fa fa-file-video-o"></i>&nbsp;&nbsp;Watch Program Introduction Video</a>
	
			<div id="watchProgramVideo" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
				<div id="featureVideo">
					<video controls style="width:100%;height:auto;" <?php echo $video_poster; ?>> 
					    <source src="<?php echo $host_url_mp4; ?>" type="video/mp4">  
					    <source src="<?php echo $host_url_webm; ?>" type="video/webm">   
				    Your browser does not support the video tag.
				  	</video>
				</div>
				<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			</div>
				
		</div>
	</div>
	
	<?php			
	
}

?>

<!--
<div class="row">
			
	<div class="small-12 columns">
		
		<a href="#" data-reveal-id="watchProgramVideo1" class="button large radius success expand"><i class="fa fa-file-video-o"></i>&nbsp;&nbsp;Watch Program Introduction Video</a>

		<div id="watchProgramVideo1" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			<div id="featureVideo">
				<video controls style="width:100%;height:auto;" poster="https://wrightcheck.com/media/images/nutrition-lifestyle-word-collage-with-finger-pointer-landscape-xlarge.jpg"> 
				    <source src="https://wrightcheck.com/media/videos/your-best-weight-program-introduction.mp4" type="video/mp4">  
				    <source src="https://wrightcheck.com/media/videos/your-best-weight-program-introduction.webm" type="video/webm">   
			    Your browser does not support the video tag.
			  	</video>
			</div>
			<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		</div>
			
	</div>
</div>
-->

<?php
	
show_session_message();
	
if ($registration_status != 1) {
	
	?>
	<div class="row">		
		<div class="small-12 columns">
		
			<script type="text/JavaScript" src="../js/sha512.js"></script> 
			<script type="text/JavaScript" src="../js/forms.js"></script>
						
			<div id="registration_form_anchor"></div>
									
			<form data-abide action="../php/program_registration_process.php" method="post" name="login_form" id="login_form" onsubmit="submitLoginNow(event);">
				<!-- Where is submitLoginNow? /js/forms.js-->
				
				<input type="hidden" name="cp" value="<?php echo $_SESSION["token"]; ?>">
				
				<input type="hidden" name="application_id" value="<?php echo $_SESSION['application']['application_id']; ?>">
				<input type="hidden" name="project_program_id" value="<?php echo $project_program_id; ?>">
				<input type="hidden" name="price_type_code" value="VOLDSC"> <!-- Volume Discount Pricing -->
			
				<fieldset>
			  
			    <legend>Program Registration Form</legend>
			    
			    <div class="row">
			      <div class="small-12 columns">
				      
			      	<p>Please provide your <strong>User ID</strong> and <strong>Password</strong> then press the <b>REGISTER</b> button.</p>
			      	
			      	<div data-alert class="alert-box secondary radius">
						<p>If you do not already have a <b>User ID and Password</b>, create your <b>FREE Member Account</b> <a href="../register-member-account/">here</a>.&nbsp;&nbsp;Return to this page to register for the program when you are done.</p>
						<!--<a href="#" class="close">&times;</a>-->
				  	</div>
			      
			      </div>
			    </div>
			    		
				<div class="row">			
				  <div class="small-12 medium-6 columns">
				  
				    <label>User ID
				      <input type="text" required name="username" placeholder="MyUserID" value="<?php echo $_SESSION['user']['username']; ?>" />
				    </label>
				    <small class="error">User ID is required</small>    
				  </div>
				  
				  <div class="small-12 medium-6 columns">
				    <label>Password
				      <input type="password" required name="password" placeholder="********" id="password" onkeypress="submitLoginKey(event);"/>
				    </label>
				    <small class="error">Password is required</small>	    
				  </div>
				  			  
				</div>
				
				<div class="row">			
				  	<!--<div class="small-12 medium-6 medium-push-6 columns">-->
				  	<div class="small-12 columns">
					  	<p>Upon completion of your registration, we will <b>email your registration confirmation</b> including <b>what to expect</b> with <b>simple instructions</b>.</p>
					    <label>	      
					      	<input class="button large radius expand show-for-medium-up" type="submit" value="REGISTER" />
						  	<input class="button large radius expand show-for-small" type="submit" value="REGISTER" />      
					    </label>
				  	</div>   
				</div>
												
			  </fieldset>
			  		
			</form>
		
		</div>
	</div>
	
	<?php
	
}
	
if (!empty($description)) {
	
	?>
	
	<div class="row">		
		<div class="small-12 columns">
			<?php echo $description; ?>					
		</div>
	</div>
	
	<?php	
}

?>

<!--
<div class="row">		
	<div class="small-12 columns">
		
		<h2 class="panel callout">If you’re ready to lose that weight, let us be your guides!</h2>
													
		<div id="program-information">&nbsp;</div>

		<div class="row">		
			<div class="small-12 medium-4 columns show-for-small-up">
				<img src="https://wrightcheck.com/media/images/women-talking-near-beach-large-landscape.jpg">					
			</div>
			<div class="small-12 medium-4 columns show-for-medium-up">
				<img src="https://wrightcheck.com/media/images/mother-daughter-playing-with-salad-large-landscape.jpg">					
			</div>
			<div class="small-12 medium-4 columns show-for-medium-up">
				<img src="https://wrightcheck.com/media/images/father-son-relaxing-pose-large-landscape.jpg">					
			</div>
		</div>

		<br>
		
		<p>Imagine yourself surrounded by a team of people who want help you succeed.  You wake up every morning rested, energized, and excited to start the day.   Maybe you would be exercising more or pain free?  Maybe you would finally get rid of that extra 10 pounds?  Make it a reality by joining the Your Best Weight Program!</p>
		
		<p>Welcome to the new and improved SupplementRelief.com!  I’m Libby Wright, one of the founders of the company. We started this company to help people find real answers to health problems they are facing.  Believe it or not, 8 years ago I was very sick:  Couldn’t walk across a room, I had a fever almost every day for 2 years, I was miserable and scared.  My husband is a doctor himself, and decided to take matters into his own hands. He started learning how nutrition and supplements, as well as good lifestyle habits, can help heal people.  I started getting better, and so did our clients.</p>
		
		<p>Now we want to share our knowledge with you.  You see, our website is more than just a place to buy high quality supplements. It’s a web community where we learn and grow together!</p> 
		
		<p>You can start changing your health right now by joining our first program.  It’s a <b>four-week weight focused educational series</b> called <b>Your Best Weight</b>.  Register <a href="#registration_form_anchor">now</a>.</p>

		<h3 class="panel callout">To make the most of our program... use the NuMedica supplements.</h3>
				
		<div class="row">		
			<div class="small-12 large-8 columns ">
				<p>We recommend you try one of our <a href="../numedica/?keyword=eliminator">NuMedica Eliminator</a> supplement packages designed specifically for Weight Loss and Maintenance.  These are best selling items that have helped our clients get real, lasting results.  During our time together, we will <b>share additional ways to use these powerful tools on your health journey</b>.  The supplement packages are based on how intensely you plan on going for your weight loss goals.  <a href="../product/?p_id=1&bpu_id=292">Maintain, Don’t Gain</a> is our most popular group of supplements for healthy living without weight loss components.  The <a href="../product/?p_id=1&bpu_id=288">5-Pound Eliminator</a> focuses on busting those carb/sugar cravings.  The <a href="../product/?p_id=1&bpu_id=290">10-Pound Eliminator</a> focuses on suppressing that overeating, as well as good sleep, a key to weight loss. The <a href="../product/?p_id=1&bpu_id=291">15-Pound Eliminator</a>  is our <b>premium NuMedica Weight Loss Package</b>.  By bringing together the best weight loss tools, and nutritional focus, you have a winning combination.  <b>Add a meal replacement shake</b> to reach your goals faster.</p>		
			</div>
			<div class="small-12 large-4 columns ">
				<img src="https://wrightcheck.com/media/images/supplementrelief-numedica-supplement-packs-landscape-xlarge.jpg">					
			</div>

		</div>
											
	</div>
</div>
-->

<?php include('../php/standard_membership_modal.php'); ?>

<br>	