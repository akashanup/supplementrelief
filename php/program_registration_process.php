<?php

// echo 'Begin program_registration_process.php.<br />';

include('../includes/header.php');

// show_session();
// show_array($_POST);
// die;

// if (@isset($_POST['username'], $_POST['p'], $_POST["cp"]) && $_POST["cp"] == $_SESSION["token"]) {
if (@isset($_POST['username'], $_POST['p'], $_POST["cp"]) && $_POST["cp"] == $_SESSION["token"]) {

    $username = sanitize($_POST['username']);
    $password = sanitize($_POST['p']); // The hashed password.
    $validate = true; // calling login function only to validate username and password
    
    $project_program_id = sanitize($_POST['project_program_id']); // from Registration form
	$price_type_code = sanitize($_POST['price_type_code']); // from Registration form


    if (validate_login_credentials($username, $password, $connection) == true) {
        // Username and Password validated
        // Create the Program Registration in the table enrollments.
        
        // show_array($_POST);
        // show_array($_SESSION);
		// die;
		
		// Get Person Role
		$queryPersonRole = '
		SELECT 
		pr.id AS person_role_id, 
		pe.id AS person_id, 
		pe.username, 
		pe.email  
		FROM person_roles pr 
		LEFT JOIN persons pe ON pr.person_id = pe.id 
		WHERE pe.username = "'.$username.'" 
		LIMIT 1';

		// echo $queryPersonRole;
		// die();
		
		$result_person_role = mysql_query($queryPersonRole);
		
		if (!$result_person_role) {
			show_mysqli_error_message($queryPersonRole, $connection);
			die;
		}
							
		while($r = mysql_fetch_array($result_person_role)) { 
			$person_username = $r['username'];
			$person_role_id = $r['person_role_id'];
			$person_id = $r['person_id'];
			$person_email = $r['email'];
		}
		
		mysql_free_result($result_person_role);
		
		// Validate user is not already enrolled into this Project Program
		
		$queryExistingEnrollment = '
		SELECT 
		en.id  
		FROM enrollments en
		WHERE en.person_role_id = "'.$person_role_id.'" 
		AND en.project_program_id = "'.$project_program_id.'" 
		LIMIT 1';

		// echo $queryExistingEnrollment;
		// die();
		
		$result_existing_enrollment = mysql_query($queryExistingEnrollment);
		
		if (!$result_existing_enrollment) {
			show_mysqli_error_message($queryExistingEnrollment, $connection);
			die;
		}
							
		while($r = mysql_fetch_array($result_existing_enrollment)) {
	        // User is already enrolled 
	        $_SESSION['message_type'] = 'alert-box alert radius';		
			$_SESSION['message'] = '<p>This Username is <b>already enrolled into the program</b>. No further action is required.</p>';
			
			$target_uri = '../program_registration/?pp_id='.$project_program_id.'';
			// echo 'Target URI: '.$target_uri.'<br /><hr />';
			// die;
			header("location: $target_uri");
			exit;		  
		}
		
		mysql_free_result($result_existing_enrollment);
	
		// Create the Enrollment
				
		date_default_timezone_set('America/Detroit');
		$created_timestamp = date('Y-m-d H:i:s');
		$effective_timestamp = $created_timestamp;
		$active = 1;
		
		$queryInsertEnrollment = '
		INSERT INTO enrollments (person_role_id, project_program_id, price_type_code, effective_timestamp, active, created_by, created_timestamp) 
		VALUES (
		"'.$person_role_id.'",
		"'.$project_program_id.'",
		"'.$price_type_code.'",
		"'.$effective_timestamp.'",
		"'.$active.'",
		"'.$person_id.'",
		"'.$created_timestamp.'")';		
				
		// echo $queryInsertEnrollment;	
		// die();
		
		$result_insert_enrollment = mysqli_query($connection, $queryInsertEnrollment);
				
		if (!$result_insert_enrollment) {
			show_mysqli_error_message($queryInsertEnrollment, $connection);
			die;
		}
				
		mysqli_free_result($result_insert_enrollment);
		
		// Get Program Registration Confirmation Message
		$queryProjectProgram = '
		SELECT 
		pp.name AS program_name, 
		pp.summary_description, 
		pp.registration_message 
		FROM project_programs pp  
		WHERE pp.id = "'.$project_program_id.'" 
		LIMIT 1';

		// echo $queryProjectProgram;
		// die();
		
		$result_project_program = mysql_query($queryProjectProgram);
		
		if (!$result_project_program) {
			show_mysqli_error_message($queryProjectProgram, $connection);
			die;
		}
							
		while($r = mysql_fetch_array($result_project_program)) {
			// show_array($r);
			// die();
			$program_name = $r['program_name'];
			$program_summary_description = $r['summary_description'];
			$program_registration_message = $r['registration_message'];
		}
		
		mysql_free_result($result_project_program);
		
		// Email the User the Program Registration Confirmation
		$from = $_SESSION['application']['name'].' <support@'.$_SESSION['application']['domain'];
		$to = $person_email;
		$subject = $program_name.' - registration confirmation';

		$message .= $program_registration_message;
		// $message .= '<img src="https://wrightcheck.com/media/images/supplementrelief-logo-email-header.png">';
				
		send_email_html($from, $to, $subject, $message, "");
			
		// User Success confirmation message for Registration form page.
		$_SESSION['message_type'] = 'alert-box success radius';				
		$_SESSION['message'] = '<p><i class="fa fa-thumbs-up fa-lg"></i>&nbsp;&nbsp;<b>Thank You!</b> Your <strong>Program Registration is complete</strong> and an email confirmation was sent to: <b>'.$person_email.'</b>. Please check your <b>SPAM folder</b> if you did not receive the email confirmation.</p>';	
		
		// Return user to Registration page
		$target_uri = '../program_registration/?pp_id='.$project_program_id.'&s=1';
		header("location: $target_uri");
		exit;		  
			           
    } else {
        // User Validation  failed message
        $_SESSION['message_type'] = 'alert-box alert radius';		
		$_SESSION['message'] = '<p><i class="fa fa-exclamation-circle fa-2x"></i>&nbsp;&nbsp;The <b>Username</b> and <b>Password</b> were not found. Please try again.</p>';
		$target_uri = '../program_registration/?pp_id='.$project_program_id.'';
		header("location: $target_uri");
		exit;		  	  
    }
} else {
    // The correct POST variables were not sent to this page. 
    echo 'Invalid Request';
}

?>