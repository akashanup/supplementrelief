<?php

// echo 'Begin project_program.php.<br />';

include_once('../includes/header.php');

// show_session();
// die;

if (isset($_GET['pp_id'])) { 
	
	// show_array($_GET);
	$project_program_id = $_GET['pp_id'];	
	
}

// Get Project Program
$queryProjectProgram = '
SELECT 
pp.name AS project_program_name, 
pp.summary_description, 
pp.description, 
pp.effective_date, 
pg.name AS program_name,  
vi.host_url_mp4, 
vi.host_url_webm, 
im.host_url AS video_poster_image_url  
FROM project_programs pp 
LEFT JOIN programs pg ON pp.program_id = pg.id 
LEFT JOIN videos vi ON pp.video_id = vi.content_asset_id 
LEFT JOIN images im ON 
	(vi.image_id = im.content_asset_id AND 
	 im.size = "Video" AND 
	 im.usage_size = "Large" AND 
	 im.usage_shape = "Landscape" AND 
	 im.format = "JPG") 
WHERE pp.id = "'.$project_program_id.'"';

// echo $queryProjectProgram;
// die();

$result_project_program = mysqli_query($connection, $queryProjectProgram);

if (!$result_project_program) {
	show_mysqli_error_message($queryProjectProgram, $connection);
	die;
}
					
while($r = mysqli_fetch_array($result_project_program)) { 
	
	// show_array($r);
	
	$project_program_name = $r['project_program_name'];
	$summary_description = $r['summary_description'];
	$description = $r['description'];
	$effective_date = hdate($r['effective_date']);
	$program_name = $r['program_name'];
	$host_url_mp4 = $r['host_url_mp4'];
	$host_url_webm = $r['host_url_webm'];
	$video_poster_image_url = $r['video_poster_image_url'];

}

mysqli_free_result($result_project_program);

?>

<br />

<div class="row">		
	<div class="small-12 columns">
		<div class="panel callout">
			<h2><?php echo $project_program_name; ?></h2>
			<?php echo $summary_description; ?>
		</div>
	</div>
</div>

<?php
	
if (!empty($host_url_mp4)) { 
	
	// echo 'We have a video.';
	
	if (!empty($video_poster_image_url)) {
		$video_poster = 'poster="'.$_SESSION['application']['root_media_host_url'].$video_poster_image_url.'"';
		// echo $video_poster;
	} else {
		
		$video_poster = '';
	}
	
	?>
	
	<div class="row">
			
		<div class="small-12 columns">
			
			<div id="featureVideo">
				<video controls style="width:100%; height:auto;" <?php echo $video_poster; ?>> 
				    <source src="<?php echo $_SESSION['application']['root_media_host_url'].$host_url_mp4; ?>" type="video/mp4">  
				    <source src="<?php echo $_SESSION['application']['root_media_host_url'].$host_url_webm; ?>" type="video/webm">   
			    Your browser does not support the video tag.
			  	</video>
			</div>
			
			<br />

			<!--
			<a href="#" data-reveal-id="watchProgramVideo" class="button large radius success expand"><i class="fa fa-file-video-o"></i>&nbsp;&nbsp;Watch Program Video</a>
	
			<div id="watchProgramVideo" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
				<div id="featureVideo">
					<video controls style="width:100%;height:auto;" <?php echo $video_poster; ?>> 
					    <source src="<?php echo $host_url_mp4; ?>" type="video/mp4">  
					    <source src="<?php echo $host_url_webm; ?>" type="video/webm">   
				    Your browser does not support the video tag.
				  	</video>
				</div>
				<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			</div>
			
			-->
				
		</div>
	</div>
	
	<?php			
	
}
		
if (!empty($description)) {
	
	?>
	
	<div class="row">		
		<div class="small-12 columns">
			<?php echo $description; ?>					
		</div>
	</div>
	
	<?php	
}

?>

<?php // include('../php/standard_membership_modal.php'); ?>

<br>	