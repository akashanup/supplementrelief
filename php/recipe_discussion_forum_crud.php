<?php

// echo 'Begin recipe_discussion_forum_crud.php<br /><hr />';

include_once('../includes/header.php');

// show_array($_SESSION);
// show_array($_SERVER);
// show_array($_POST);
// show_array($_FILES);

// die();
	
// Set the current date and time for the database record audit trail.
$created_timestamp = date('Y/m/d H:i:s');
$modified_timestamp = $created_timestamp;

if($_POST['action'] == 'new_comment') {

	// echo 'New Recipe Forum Post<br />';
	// show_array($_POST);
	// show_array($_FILES);

	$_SESSION['comment_text'] = null; // clear in case valued from previous Error Message user has already seen
	
	// set POST return values
	$comment_user_id = safe_sql_data($connection, $_POST['comment_user_id']);
	// $comment_title = safe_sql_data($connection, $_POST['comment_title']);
	$comment_text = safe_sql_data($connection, $_POST['comment_text']);
	$comment_recipe_id = safe_sql_data($connection, $_POST['comment_recipe_id']);
	
    if (!$_POST['g-recaptcha-response']) {
        $_SESSION['recipe']['discussion_forum']['comment_text'] = $comment_text; // So user does not lose the Comment previously entered on the form			
        $_SESSION['message_type'] = 'alert-box warning radius';				
        $_SESSION['message'] = '<p><i class="fa fa-android fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Please let us know that <strong>you are NOT a ROBOT!</strong></p>';
        mysqli_close($connection);
        $_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
        header("location: ".$_SESSION['target_uri']);
    exit();
    
    }	
    
    if ($_POST['private_captcha_key'] != private_captcha_key) {
        $_SESSION['comment_text'] = $comment_text;	 // So user does not loose the Comment previously entered on the form			
        $_SESSION['message_type'] = 'alert-box alert radius';		
    	$_SESSION['message'] = '<p><strong>We cannot process your Forum Post at this time.</strong></p>';
        mysqli_close($connection);
        $_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
        header("location: ".$_SESSION['target_uri']);
        exit();
    }
	
	// phpinfo(); // check the max size of file upload and post settings
	
	/* could not get this to do anything useful
	switch ($_FILES['video_file']['error']) {
	    case UPLOAD_ERR_OK:
	        break;
	    case UPLOAD_ERR_NO_FILE:
	        throw new RuntimeException('No file sent.');
	    case UPLOAD_ERR_INI_SIZE:
	    case UPLOAD_ERR_FORM_SIZE:
	        throw new RuntimeException('Exceeded filesize limit.');
	    default:
	        throw new RuntimeException('Unknown errors.');
	}
	*/
	
	// Files can currently be posted only on Comments, not on Replies.
	// Set to null to be sure only valid User values from the post go into the database.
	// Add document file type later

	/*
	$database_document_file_url = null; 
	$document_file_extension = null;  
	$database_image_file_url = null; 
	$database_image_file_url_thumbnail = null; 
	$image_file_extension = null;	
	$database_video_file_url = null; 
	$video_file_extension = null;
	*/
		
	// Consider making a function for handling the File uploads		
	$document_file_name = $_FILES['document_file'] ['name'];
	$image_file_name = $_FILES['image_file'] ['name'];	
	$video_file_name = $_FILES['video_file'] ['name'];
		
	if (isset($image_file_name) && strlen($image_file_name) > 0) {
		
		// See Education Forum Post CRUD for CDN file upload management settings for Images, Documents and Videos.
	
		// echo 'File Upload File Type Image: '.$image_file_name.'<br />';	
		$image_file_name = $_FILES['image_file'] ['name'];		 
		$tmp_image_file_name = $_FILES['image_file'] ['tmp_name'];
		$image_file_size = $_FILES['image_file'] ['size'];
		$image_file_type = $_FILES['image_file'] ['type'];
		$image_file_error = $_FILES['image_file'] ['error'];	
									
		// $image_file_extension = strtolower(substr($image_file_name, strpos($image_file_name, '.') +1));
		$image_file_extension = strtolower(substr($image_file_type, strpos($image_file_type, '/') +1));		
		$unique_filename = strtolower(substr($tmp_image_file_name, strpos($tmp_image_file_name, '/tmp/') +5)).'.'.$image_file_extension;	
		// echo 'Unique Filename: '.$unique_filename.'<br />';				
		$database_image_file_url = 'http://wrightcheck.com/media/images/user/'.$unique_filename;
				
		// Check for a valid image filetype.
		if ($image_file_extension == 'jpg' || $image_file_extension == 'jpeg' || $image_file_extension == 'png' || $image_file_extension == 'gif') {
		
			// echo 'About to move File.<br />';
		
			$directory = dirname(__FILE__).'/../../media/images/user/';
			// $destination = $directory.$image_file_name;
			$destination = $directory.$unique_filename;
			
			//echo $tmp_name.'<br />';
			//echo $destination.'<br />';
							
			// If values are set and not empty move_upload_file is invoked to place the uploaded file into the web server directory identified by $directory.
								
			if (move_uploaded_file($tmp_image_file_name, $destination)) {
			
				// echo '<b>Image file successfully uploaded to server directory.</b><br><hr>';		
				// user's original file is stored on the server regardless of size
				// proportionally resize image  to fit in specified box ((800x800)
				create_thumb($destination, 300);
				$database_image_file_url_thumbnail = 'http://wrightcheck.com/media/images/user/tn_'.$unique_filename;
										
				// echo '<b>Image file successfully resized.</b><br><hr>';
				
			} else {
			
				echo '<span class="formErrorMessage">Upload failed. Please contact System Administrator!</span><br><hr>';	
				
			}
		
		} else {
		
			// Invalid Image File Type so provide user with error message
			$_SESSION['comment_text'] = $comment_text;	 // So user does not loose the Comment previously entered on the form			
			$_SESSION['message_type'] = 'alert-box warning radius';				
			$_SESSION['message'] = '<p>The <strong>Image File Type</strong> must be jpg, jpeg, png or gif. The file type you submitted was <b>'.$image_file_extension.'</b>. Please choose an Image with the supported File Type.</p>';
			mysqli_close($connection);
			$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
			header("location: ".$_SESSION['target_uri']);
			exit();	
			
		} // if ($file_extension == 'jpg' || $file_extension == 'jpeg' || $file_extension == 'png' || $file_extension == 'gif') {		
		 	
	} // if (isset($image_file_name) && strlen($image_file_name) > 0) {
	
	if (isset($video_file_name) && strlen($video_file_name) > 0) {
															
		// echo 'File Upload File Type Video: '.$video_file_name.'<br />';

		// $video_file_name = $_FILES['image_file'] ['name'];
		$tmp_video_file_name = $_FILES['video_file'] ['tmp_name'];
		$video_file_size = $_FILES['video_file'] ['size'];
		$video_file_type = $_FILES['video_file'] ['type'];
		$video_file_error = $_FILES['video_file'] ['error'];	
		
		// $video_file_extension = strtolower(substr($video_file_name, strpos($video_file_name, '.') +1));
		$video_file_extension = strtolower(substr($video_file_type, strpos($video_file_type, '/') +1));	
		
		// echo $video_file_extension.'<br />';	
		// exit();
		
		// echo 'tmp_video_file_name: '.$tmp_video_file_name.'<br />';	
		// echo 'video_file_size: '.$video_file_size.'<br />';	
		// echo 'video_file_type: '.$video_file_type.'<br />';	
		// echo 'video_file_error: '.$video_file_error.'<br />';		
	
		// Check for a valid HTML5 video filetype.
		if ($video_file_extension != 'mp4' && $video_file_extension != 'webm') {
		
			// echo 'video_file_extension != mp4 or webm: '.$video_file_extension.'<br />';		
			// Invalid HTML5 Video File Type so provide user with error message
			$_SESSION['comment_text'] = $comment_text;	 // So user does not loose the Comment previously entered on the form			
			$_SESSION['message_type'] = 'alert-box warning radius';				
			$_SESSION['message'] = '<p>The <strong>Video File Type</strong> must be <b>mp4</b> or <b>webm</b>. The file type you submitted was <b>'.$video_file_extension.'</b>. Please choose a Video with a supported File Type.</p>';
			mysqli_close($connection);
			$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
			header("location: ".$_SESSION['target_uri']);
			exit();
		}		
	
		// 5037394 = 5 MB
		// 1000000 = 1 MB
		// 10000000 = 10 MB
		if ($video_file_size > 30000000) {
			// echo 'File sized exceeded 30MB.<br />';
		    // File size is too large so provide user with error message
		    $_SESSION['comment_text'] = $comment_text;	 // So user does not loose the Comment previously entered on the form			
		    $_SESSION['message_type'] = 'alert-box warning radius';				
		    $_SESSION['message'] = '<p>The <strong>Video File Size</strong> must be less than 30MB. Please choose a smaller-size Video.</p>';
		    mysqli_close($connection);
		    $_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
		    header("location: ".$_SESSION['target_uri']);
		    exit();	
		}
		
			
		$unique_filename = strtolower(substr($tmp_video_file_name, strpos($tmp_video_file_name, '/tmp/') +5)).'.'.$video_file_extension;	
		// echo 'Unique Filename: '.$unique_filename.'<br />';				
		$database_video_file_url = 'http://wrightcheck.com/media/videos/user/'.$unique_filename;
					
		$directory = dirname(__FILE__).'/../../../media/videos/user/';
		// $destination = $directory.$video_file_name;
		$destination = $directory.$unique_filename;
		//echo 'destination :'.$destination.'<br />';
		
		//echo $tmp_name.'<br />';
		//echo $destination.'<br />';
						
		// If values are set and not empty move_upload_file is invoked to place the uploaded file into the web server directory identified by $directory.
		// Find a php function to resize large video files.					
		if (move_uploaded_file($tmp_video_file_name, $destination)) {
		
			// echo 'moved file<br />';		
			// echo '<b>Video file successfully uploaded.</b><br><hr>';			
			
			$database_video_file_url = 'http://wrightcheck.com/media/videos/user/'.$unique_filename;									
			
		} else {
		
			echo '<span class="formErrorMessage">Upload failed. Please contact System Administrator!</span><br><hr>';	
			
		}
			
		 	
	} // END if (isset($video_file_name) && strlen($video_file_name) > 0) { 
    	
    if (isset($_SESSION['enrollment']['project_program_id'])) { 	
    	$project_program_id = $_SESSION['enrollment']['project_program_id']; 	
    } else {
    	$project_program_id = public_project_program_id; // store/includes/defines.php
    }
					
	// Create the Comment
				
	$queryInsertCommentPost = '
	INSERT INTO discussion_threads (
		title, 
		text, 
		file_document_url, 
		file_document_type, 
		file_image_url, 
		file_image_type, 
		file_video_url, 
		file_video_type, 
		status, 
		project_id, 
		project_program_id, 
		recipe_id, 
		ip_address, 
		created_by, 
		created_timestamp, 
		modified_timestamp)
	VALUES (
	'.no_value_null_check($comment_title).',	
	/* "'.$comment_title.'", */ 
	"'.$comment_text.'",
	'.no_value_null_check($database_document_file_url).',	
	'.no_value_null_check($document_file_extension).',	
	'.no_value_null_check($database_image_file_url_thumbnail).',	
	'.no_value_null_check($image_file_extension).',	
	'.no_value_null_check($database_video_file_url).',	
	'.no_value_null_check($video_file_extension).',	
	"S",
	"'.$_SESSION['enrollment']['project_id'].'",  
	"'.$_SESSION['enrollment']['project_program_id'].'", 
	"'.$comment_recipe_id.'", 
	"'.$_SERVER['REMOTE_ADDR'].'", 	
	"'.$comment_user_id.'", 	
	"'.$created_timestamp.'",	 
	"'.$created_timestamp.'")';
		
	// echo $queryInsertCommentPost.'<br /><hr />';
	// die;
	
	$result_insert_comment = mysqli_query($connection, $queryInsertCommentPost);
	
	if (!$result_insert_comment) {
		show_mysqli_error_message($queryInsertCommentPost, $connection);
		die;
	}
	
	// Get the insert_id of the record just created in the database for the return URL hashtag.
	$last_insert_id = mysqli_insert_id($connection); 
	$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#comment'.$last_insert_id;
	
    $post_type = 'Comment';
	
}

if($_POST['action'] == 'new_reply') {
		
	// echo 'User is creating a Blog Post Comment Reply.<br /><hr />';
	// show_array($_POST);
	// show_array($_FILES);
	
	// set POST return values
	$comment_reply_id = safe_sql_data($connection, $_POST['comment_reply_id']);
	$comment_reply_user_id = safe_sql_data($connection, $_POST['comment_reply_user_id']);
	$comment_reply_text = safe_sql_data($connection, $_POST['comment_reply_text']);
	
	//$database_document_file_url = null; 
	//$document_file_extension = null;  
	$database_image_file_url = null; 
	$database_image_file_url_thumbnail = null; 
	$image_file_extension = null;	
	//$database_video_file_url = null; 
	//$video_file_extension = null;
		
	// Consider making a function for handling the File uploads		
	//$document_file_name = $_FILES['document_file'] ['name'];
	$image_file_name = $_FILES['image_file'] ['name'];	
	//$video_file_name = $_FILES['video_file'] ['name'];
		
	if (isset($image_file_name) && strlen($image_file_name) > 0) {
	
		// echo 'File Upload File Type Image: '.$image_file_name.'<br />';	
		// $image_file_name = $_FILES['image_file'] ['name'];		 
		$tmp_image_file_name = $_FILES['image_file'] ['tmp_name'];
		$image_file_size = $_FILES['image_file'] ['size'];
		$image_file_type = $_FILES['image_file'] ['type'];
		$image_file_error = $_FILES['image_file'] ['error'];	
									
		// $image_file_extension = strtolower(substr($image_file_name, strpos($image_file_name, '.') +1));
		$image_file_extension = strtolower(substr($image_file_type, strpos($image_file_type, '/') +1));		
		$unique_filename = strtolower(substr($tmp_image_file_name, strpos($tmp_image_file_name, '/tmp/') +5)).'.'.$image_file_extension;	
		// echo 'Unique Filename: '.$unique_filename.'<br />';				
		$database_image_file_url = 'http://wrightcheck.com/media/images/user/'.$unique_filename;
		// Get the CDN settings in Education Forum
				
		// Check for a valid image filetype.
		if ($image_file_extension == 'jpg' || $image_file_extension == 'jpeg' || $image_file_extension == 'png' || $image_file_extension == 'gif') {
		
			// echo 'About to move File.<br />';
		
			$directory = dirname(__FILE__).'/../../media/images/user/';
			// $destination = $directory.$image_file_name;
			$destination = $directory.$unique_filename;
			
			//echo $tmp_name.'<br />';
			//echo $destination.'<br />';
							
			// If values are set and not empty move_upload_file is invoked to place the uploaded file into the web server directory identified by $directory.
								
			if (move_uploaded_file($tmp_image_file_name, $destination)) {
			
				// echo '<b>Image file successfully uploaded to server directory.</b><br><hr>';		
				// user's original file is stored on the server regardless of size
				// proportionally resize image  to fit in specified box ((800x800)
				create_thumb($destination, 300);
				$database_image_file_url_thumbnail = 'http://wrightcheck.com/media/images/user/tn_'.$unique_filename;
										
				// echo '<b>Image file successfully resized.</b><br><hr>';
				
			} else {
			
				// echo '<span class="formErrorMessage">Upload failed. Please contact System Administrator!</span><br><hr>';	
				
			}
		
		} else {
		
			// Invalid Image File Type so provide user with error message
			$_SESSION['comment_reply_text'] = $comment_reply_text;	 // So user does not loose the Comment Reply previously entered on the form			
			$_SESSION['message_type'] = 'alert-box warning radius';				
			$_SESSION['message'] = '<p>The <strong>Image File Type</strong> must be jpg, jpeg, png or gif. The file type you submitted was <b>'.$file_extension.'</b>. Please choose an Image with the supported File Type.</p>';
			mysqli_close($connection);
			$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#sessionMessage';							
			header("location: ".$_SESSION['target_uri']);
			exit();	
			
		} // if ($file_extension == 'jpg' || $file_extension == 'jpeg' || $file_extension == 'png' || $file_extension == 'gif') {		
		 	
	} // if (isset($image_file_name) && strlen($image_file_name) > 0) {
	
	
	// Create the Comment Reply
	
	// put NULL into empty fields that are not required on INSERT and UPDATE
	// end_date = '.no_value_null_check($end_date).', 
		
	$queryInsertCommentReply = '
	INSERT INTO discussion_replies (
	discussion_thread_id, 
	created_timestamp, 
	text, 
	status, 
	file_image_url, 
	file_image_type, 
	created_by) 
	VALUES (
	"'.$comment_reply_id.'",
	"'.$created_timestamp.'",
	"'.$comment_reply_text.'", 
	"S", 
	"'.$database_image_file_url_thumbnail.'", 
	"'.$image_file_extension.'", 
	"'.$comment_reply_user_id.'")';
		
	// echo $queryInsertCommentReply.'<br /><hr />';
	
	$result_insert_comment_reply = mysqli_query($connection, $queryInsertCommentReply);
	
	if (!$result_insert_comment_reply) {
		show_mysqli_error_message($queryInsertCommentPost, $connection);
		die;
	}
	
	// Get the insert_id of the record just created in the database for the return URL hashtag.
	$last_insert_id = mysqli_insert_id($connection); 
	$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#reply'.$last_insert_id;
	
	mysqli_free_result($result_insert_comment_reply);
	
	// Update the Discussion Thread modified_timestamp
	
	$queryUpdateBlogPostComment = '
		UPDATE discussion_threads SET 
		modified_timestamp = "'.$created_timestamp.'" 
		WHERE id = "'.$comment_reply_id.'"';
	
	// echo $queryUpdateBlogPostComment;
	$result_update_comment = mysqli_query($connection, $queryUpdateBlogPostComment);
	
	if (!$result_update_comment) {	
		echo $queryUpdateBlogPostComment.'<br /><hr />';	
		die("Database Update Discussion Thread for Blog Post Comment failed.");
	}
	
    $post_type = 'Reply';

}

$message = generate_forum_post_email_notification($connection, $post_type, $last_insert_id);
	
$from = $_SESSION['application']['email'];
$to = $_SESSION['application']['email'];
$subject = 'Recipe Forum Post Notification';
		   	   
send_email_html($from, $to, $subject, $message, "");
		
mysqli_close($connection);
header("location: ".$_SESSION['target_uri']);
exit();	

// echo json_encode($last_insert_id);
// exit(header("Status: 200"));
// header("location: ".$_SESSION['target_uri']);
// exit();	

?>