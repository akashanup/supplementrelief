<?php
	 
// echo 'Begin register_family_care_account.php<br>'; 
// show_array($_SESSION);
// die;	

include_once '../includes/header.php';

$error_msg = "";

if (isset($_POST['action']) && $_POST['action'] == "new") {
	// show_array($_POST);
	// die;				
	
    // Sanitize and validate the data passed in
    $project_program_id = filter_input(INPUT_POST, 'project_program_id', FILTER_SANITIZE_STRING);
    
    $fname = filter_input(INPUT_POST, 'first_name', FILTER_SANITIZE_STRING);

    $lname = filter_input(INPUT_POST, 'last_name', FILTER_SANITIZE_STRING);
    
	$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        // Not a valid email
        $error_msg .= '<p class="error">The Email Address you entered is not valid.</p>';
    }
		    
    // Username validity and password validity have been checked client side.
    // This should should be adequate as nobody gains any advantage from
    // breaking these rules.
     
    $prep_stmt = "SELECT id FROM persons WHERE email = ? LIMIT 1";
    $stmt = $connection->prepare($prep_stmt);
 
   // check for existing email
    if ($stmt) {
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();
 
        if ($stmt->num_rows == 1) {
            // A user with this email address already exists
            $error_msg .= '<p><i class="fa fa-exclamation-circle fa-2x"></i>&nbsp;&nbsp;The <b>Email Address</b> you provided <b>'.$email.'</b> is <b>NOT</b> available. If this <b>is your Email Address</b>, then you already have an account. You can <a href="../login" style="color: white; text-decoration: underline;"><b>login</b></a> if you know your <b>User ID and Password</b>, or you can <a href="../reset-password" style="color: white; text-decoration: underline;"><b>reset your password</b></a> if you don\'t know it or have forgotten it. Otherwise, please choose a different <b>Email Address</b>.</p><p>Please call <b>(888) 424-0032</b> or <a href="../contact" style="color: white; text-decoration: underline;">email</a> if you need help creating or accessing your account.</p>';
            $stmt->close();
        }
        
        $stmt->close();
        
    } else {
        $error_msg .= '<p class="error">Database error (email exists checker)</p>';
        $stmt->close();
    }
 
    // check existing username
    $prep_stmt = "SELECT id FROM persons WHERE username = ? LIMIT 1";
    $stmt = $connection->prepare($prep_stmt);
 
    if ($stmt) {
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();
 
		if ($stmt->num_rows == 1) {
			// A user with this username already exists
			$error_msg .= '<p><i class="fa fa-exclamation-circle fa-2x"></i>&nbsp;&nbsp;The <b>User ID</b> you provided <b>'.$username.'</b> is <b>NOT</b> available. If this <b>is your User ID</b>, then you already have an account. You can <a href="../login" style="color: white; text-decoration: underline;"><b>login</b></a> if you know your <b>User ID and Password</b>, or you can <a href="../reset-password" style="color: white; text-decoration: underline;"><b>reset your password</b></a> if you don\'t know it or have forgotten it. Otherwise, please choose a different <b>User ID</b>.</p><p>Please call <b>(888) 424-0032</b> or <a href="../contact" style="color: white; text-decoration: underline;">email</a> if you need help creating or accessing your account.</p>';
			$stmt->close();
		}
		$stmt->close();
	} else {
			$error_msg .= '<p class="error">Database error (username exists checker)</p>';
			$stmt->close();
	}
	 
    if (empty($error_msg)) {
	    				        
        $application_origin_id = $_SESSION['application']['application_id']; 
        $user_level = 1; // Standard access for a Logged in user.
		$ip_address = $_SERVER['REMOTE_ADDR'];
		
		// $browser = get_user_browser(); // Original browser detection routine retired 2/8/16.
        $browser=getBrowser(); // No param is passed so will use $_SERVER['HTTP_USER_AGENT'].
        // Process returned parameter values.
        $browser_user_agent = safe_data($browser['userAgent']);
        $browser_platform = safe_data($browser['platform']);
        $browser_device_type = safe_data($browser['deviceType']);
        $browser_name = safe_data($browser['name']);
        $browser_version = safe_data($browser['version']);
        		
		$created_by = 1; // System Administrator
		$created_timestamp = date('Y-m-d H:i:s');
		
		// A little formatting to make things look nicer...
		$fname = initCapData($fname);
	    $lname = initCapData($lname);
	       
	    // Create Password = Email
	    // $password = $email; // 'p': hex_sha512($("#pass").val())},
	    
	    // Create a random salt
		// $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));

		// Create salted password 
		// $password_hash = hash('sha512', $password . $random_salt);
		
		// (1 of 4) Create Person
        if ($insert_stmt = $connection->prepare("INSERT INTO persons (application_origin_id, username, user_level, email, first_name, last_name, created_ip_address, created_browser_user_agent, created_browser_platform, created_browser_type, created_browser, created_browser_version, created_by, created_timestamp)
												
			VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('ssssssssssssss', $application_origin_id, $email, $user_level, $email, $fname, $lname, $ip_address, $browser_user_agent, $browser_platform, $browser_device_type, $browser_name, $browser_version, $created_by, $created_timestamp);
            
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                show_mysqli_error_message("register_family_care_account.php: INSERT person database failed", $connection);
				die;
            }
			$stmt->close();
        } else {
	        show_mysqli_error_message("register_family_care_account.php: INSERT person connection", $connection);
	        die;
        }
         		      
        $last_person_insert_id = mysqli_insert_id($connection);
        // echo 'Last Person Insert ID: '.$last_person_insert_id;
                
        // (2 of 4) Create Person Role
        
        $role_type_code = 'AFIL'; // Affiliate
        $effective_timestamp = date('Y-m-d H:i:s');

        if ($insert_stmt = $connection->prepare("INSERT INTO person_roles (person_id, role_type_code, effective_timestamp, created_by, created_timestamp)
												
			VALUES (?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('issis', $last_person_insert_id, $role_type_code, $effective_timestamp, $last_person_insert_id, $created_timestamp);
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                show_mysqli_error_message("register_family_care_account.php: INSERT Person Role failed", $connection);
				die;
            }
			$stmt->close();
        } else {
	        show_mysqli_error_message("register_family_care_account.php: INSERT Person Role connection failed", $connection);
	        die;
        }
        
        $last_person_role_id = mysqli_insert_id($connection); 
           
        // (3 of 3) Create Family Care Enrollment
        
        $project_program_id = 32; // Family Care program
        $price_type_code = "VOLDSC";
        $primary_enrollment = 1; 
        $active = 1; // 1 - active, 0 - inactive
        
        if ($insert_stmt = $connection->prepare("INSERT INTO enrollments (person_role_id, project_program_id, price_type_code, effective_timestamp, primary_enrollment, active, created_by, created_timestamp)
												
			VALUES (?, ?, ?, ?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('iissiiis', $last_person_role_id, $project_program_id, $price_type_code, $effective_timestamp, $primary_enrollment, $active, $last_person_insert_id, $created_timestamp);
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                show_mysqli_error_message("register_family_care_account.php: INSERT Person Role failed", $connection);
				die;
            }
			$stmt->close();
        } else {
	        show_mysqli_error_message("register_member_account.php: INSERT Person Role connection failed", $connection);
	        die;
        }
        
        $last_enrollment_id = mysqli_insert_id($connection);
                
        // Log the Account Creation in the System Log

        $event = 'CREATE ACCOUNT';
		$page = 'Registration';
		$log_description = 'CREATE ACCOUNT for Username: '.$username.' and Email: '.$email.' verification pending.';
		$flag = 'NULL';
		create_system_log($connection, $event, $page, $log_description, $flag);
		
		$_SESSION['message_type'] = 'alert-box success radius';					
		$_SESSION['message'] = '<p><i class="fa fa-check fa-2x"></i>&nbsp;&nbsp;Registration for Email: <b>'.$email.'</b> created successfully. Remember to <b>set the User\'s Password in the CDN Administration system</b>.</p>';
		show_session_message();	
		
	} else {
		
		$_SESSION['message_type'] = 'alert-box alert radius';					
		$_SESSION['message'] = $error_msg;
		show_session_message();	
		
	}

}		
 
?>

<div id="editPerson">

	<div class="row">			
		<div class="small-12 columns">

			<form data-abide action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post">
	
				<fieldset>
				  <legend>Register Family Care Account</legend>
				  
				  	<input type="hidden" name="action" value="new" />
				  	<input type="hidden" name="project_program_id" value="32" />
				      
					<div class="row">
						<div class="small-12 columns">
														
							<p>This form is for Libby to quickly register her <b>Revolutionary Joy</b> subscribers for the <b>Family Care</b> program.</p>
							
							<p>One registration is complete <a href="#" data-reveal-id="setPassword">set the User's Password</a> in the <b>CDN Administration system</b>.</p>
																					
						</div>
					</div>
						
					<div class="row">
						
						<div class="small-12 medium-4 columns"> 		
					  		<label>Email <small>Required</small>
					    		<input required type="text" name="email" placeholder="someone@domain.com" value="<?php echo ($email ? $email : ''); ?>"/>
					  		</label>
					  		<small class="error">Email is required</small>	    
						</div>
		
						<div class="small-12 medium-4 columns">		
					  		<label>First Name <small>Optional</small>
					    			<input autofocus="" type="text" name="first_name" placeholder="Jane" value="<?php echo ($fname ? $fname : ''); ?>" />
					  			</label>
					  		<small class="error">First Name is optional</small>    
						</div> 
						 		
						<div class="small-12 medium-4 columns"> 		
					  		<label>Last Name <small>Optional</small>
					    			<input type="text" name="last_name" placeholder="Doe" value="<?php echo ($lname ? $lname : ''); ?>" />
					  		</label>
					  		<small class="error">Last Name is optional</small>	    
						</div>
						
					</div>																											
					<div class="row">
					  	<div class="small-12 columns">
					    	<label>
					    		<input class="button large radius show-for-medium-up expand success" type="submit" value="REGISTER">      
								<input class="button large radius expand show-for-small success" type="submit" value="REGISTER">      
							</label>
					  	</div>
					  																			
				</fieldset>
				
			</form>
		</div>
	</div>	
</div>

<div id="setPassword" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	
  <h2 id="modalTitle">Set Password Instructions</h2>
  
  <ol>
	  <li>Open another browser window in the CDN Administration system: <b>https://cdn-manager.net/</b></li>
	  <li>Login</li>
	  <li>Select <b>People</b> from the <b>Entities</b> main menu.</li>
	  <li><b>Search</b> using the <b>Email</b> of the person registered.</li>
	  <li>Select <b>Edit</b> for the User's record.</li>
	  <li>Select the <b>Set Password</b> button at the bottom of the <b>Edit Person</b> form.</li>
	  <li>Enter the <b>Email</b> for the <b>Password and Confirm Password</b> fields: recommend you copy and paste the Email so that there is no typo. Be sure you do not copy extra spaces before or after the email begins or ends.</li>
	  <li>Select the <b>Reset Password</b> button.</li>
	  <li><b>The User can now Login using their Email as the User ID and Password</b>. They can change their Password to anything they want from their <b>Member Dashboard</b> page after they Login.</li>
  </ol>
  
  <hr>
  
  <p>If you have a long list of new Users to register for the Family Care program it might be helpful to have the Register Family Care Account form open in one browser window and the Edit Person form open in a separate browser window right next to it.</p>
  
   <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>