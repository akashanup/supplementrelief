<script type="text/JavaScript" src="../js/forms.js"></script>
<script type="text/JavaScript" src="../js/sha512.js"></script> 

<?php 
	
// include_once 'register_account_process.php';

include_once '../includes/header.php';

// show_array($_POST);
// die;

$error_msg = "";

/* Remove the Address portion of the registration process.
if (isset(	$_POST['project_program_id'], 	
			$_POST['first_name'], 
			$_POST['last_name'],
			$_POST['address'], 
			$_POST['city'],
			$_POST['state'],  
			$_POST['postal_code'],
			$_POST['phone'],   
			$_POST['username'], 
			$_POST['email'], 
			$_POST['p'], 
			$_POST['gender'])) {
*/
 
if (isset(	$_POST['project_program_id'], 	
			/* $_POST['first_name'], 
			$_POST['last_name'], */
			/* $_POST['phone'], */   
			$_POST['username'], 
			$_POST['email'], 
			$_POST['p'] /*, 
			$_POST['gender'] */)) {
					
	// ACCOUNT STATUS
	$status = ACCOUNT_STATUS_PRECONFIRM;
	
    // Sanitize and validate the data passed in
    $project_program_id = filter_input(INPUT_POST, 'project_program_id', FILTER_SANITIZE_STRING);
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    
    $fname = filter_input(INPUT_POST, 'first_name', FILTER_SANITIZE_STRING);

    $lname = filter_input(INPUT_POST, 'last_name', FILTER_SANITIZE_STRING);

	// $address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
	// $city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_STRING);
    
	$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        // Not a valid email
        $error_msg .= '<p class="error">The Email Address you entered is not valid.</p>';
    }
	
	$project_program_id = filter_var($project_program_id, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^([1-9]|[1-9][0-9]|[1-9][0-9][0-9])$/")));
	
	// $state = filter_input(INPUT_POST, 'state', FILTER_SANITIZE_STRING);

	// $state = filter_var($state, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^([1-9]|[1-9][0-9]|[1-9][0-9][0-9])$/")));
	
	/*>>>>> /^(AA|AE|AP|AL|AK|AS|AZ|AR|CA|CO|CT|DE|DC|FM|FL|GA|GU|
HI|ID|IL|IN|IA|KS|KY|LA|ME|MH|MD|MA|MI|MN|MS|MO|MT|NE| 
NV|NH|NJ|NM|NY|NC|ND|MP|OH|OK|OR|PW|PA|PR|RI|SC|SD|TN|
TX|UT|VT|VI|VA|WA|WV|WI|WY)$/       <<<<*/

	/* 
	if (!filter_var($state, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^([1-9]|[1-9][0-9]|[1-9][0-9][0-9])$/")))) {
		 // Not a valid US State ID
        $error_msg .= '<p class="error">The state you selected is not a valid state.</p>';
	};
	*/
	
	// Naming standard for later insert.
	// $state_id = $state;
	
	/*
	$zip = filter_input(INPUT_POST, 'postal_code', FILTER_SANITIZE_STRING);
	$zip = filter_var($zip, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^\d{5}(-\d{4})?$/")));
	if (!filter_var($zip, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^\d{5}(-\d{4})?$/")))) {
		 // Not a valid US phone number
        $error_msg .= '<p class="error">The Postal Code you entered is not a valid US Zip Code.</p>';
	};
	*/
	
	if (!empty($phone)) {
		
		$phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
		$phone = filter_var($phone, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^\(?(\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{4})$/")));
		if (!filter_var($phone, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^\(?(\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{4})$/")))) {
			 // Not a valid US phone number
	        $error_msg .= '<p class="error">The Phone Number you entered is not a valid phone number.</p>';
		}	
		
	}
	
	if (!empty($sex)) {

		$sex = filter_input(INPUT_POST, 'gender', FILTER_SANITIZE_STRING);
		
		if ($sex !== "M" && $sex !== "F") {
			// Not a valid age
			$error_msg .= '<p class="error">The gender you entered is not valid.</p>';
			exit;
		}
	}
	
    $password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
    if (strlen($password) != 128) {
        // The hashed pwd should be 128 characters long.
        // If it's not, something really odd has happened
        $error_msg .= '<p class="error">Invalid password configuration.</p>';
    }
    
    // Username validity and password validity have been checked client side.
    // This should should be adequate as nobody gains any advantage from
    // breaking these rules.
     
    $prep_stmt = "SELECT id FROM persons WHERE email = ? LIMIT 1";
    $stmt = $connection->prepare($prep_stmt);
 
   // check for existing email
    if ($stmt) {
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();
 
        if ($stmt->num_rows == 1) {
            // A user with this email address already exists
            $error_msg .= '<p><i class="fa fa-exclamation-circle fa-2x"></i>&nbsp;&nbsp;The <b>Email Address</b> you provided <b>'.$email.'</b> is <b>NOT</b> available. If this <b>is your Email Address</b>, then you already have an account. You can <a href="../login" style="color: white; text-decoration: underline;"><b>login</b></a> if you know your <b>User ID and Password</b>, or you can <a href="../reset-password" style="color: white; text-decoration: underline;"><b>reset your password</b></a> if you don\'t know it or have forgotten it. Otherwise, please choose a different <b>Email Address</b>.</p><p>Please call <b>(888) 424-0032</b> or <a href="../contact" style="color: white; text-decoration: underline;">email</a> if you need help creating or accessing your account.</p>';
            $stmt->close();
        }
        
        $stmt->close();
        
    } else {
        $error_msg .= '<p class="error">Database error (email exists checker)</p>';
        $stmt->close();
    }
 
    // check existing username
    $prep_stmt = "SELECT id FROM persons WHERE username = ? LIMIT 1";
    $stmt = $connection->prepare($prep_stmt);
 
    if ($stmt) {
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $stmt->store_result();
 
		if ($stmt->num_rows == 1) {
				// A user with this username already exists
				$error_msg .= '<p><i class="fa fa-exclamation-circle fa-2x"></i>&nbsp;&nbsp;The <b>User ID</b> you provided <b>'.$username.'</b> is <b>NOT</b> available. If this <b>is your User ID</b>, then you already have an account. You can <a href="../login" style="color: white; text-decoration: underline;"><b>login</b></a> if you know your <b>User ID and Password</b>, or you can <a href="../reset-password" style="color: white; text-decoration: underline;"><b>reset your password</b></a> if you don\'t know it or have forgotten it. Otherwise, please choose a different <b>User ID</b>.</p><p>Please call <b>(888) 424-0032</b> or <a href="../contact" style="color: white; text-decoration: underline;">email</a> if you need help creating or accessing your account.</p>';
				$stmt->close();
		}
		$stmt->close();
	} else {
			$error_msg .= '<p class="error">Database error (username exists checker)</p>';
			$stmt->close();
	}
	 
    if (empty($error_msg)) {
	    		
		// Get the Country ID for the Address records about to be created.
		/* 
		$queryCountry = '
			SELECT 
			co.country_id  
			FROM states st 
			LEFT JOIN country co ON st.country_abbr = co.country_abbr 
			WHERE state_id = "'.$state.'"
			LIMIT 1';
		
		
		// echo $queryCountries;
		// die();
			
		// Execute the query
		$result_country = mysqli_query($connection, $queryCountry);
		
		if (!$result_country) {
			show_mysqli_error_message($queryCountry, $connection);
			die;
		}
		
		while($c = mysqli_fetch_assoc($result_country)) {
			
			// show_array($c);
			
			$country_id = $c['country_id'];
			// $country_id = $s['country_abbr'];			
			
		}

		mysqli_free_result($result_country);
		*/
		
        // Create a random salt
        //$random_salt = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE)); // Did not work
        $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
 
        // Create salted password 
        $password = hash('sha512', $password . $random_salt);
        
        $application_origin_id = $_SESSION['application']['application_id']; // Can later report on which application this person originated from.
        $user_level = 1; // Standard access for a Logged in user.
		$ip_address = $_SERVER['REMOTE_ADDR'];
		
		// $browser = get_user_browser(); // Original browser detection routine retired 2/8/16.
        $browser=getBrowser(); // No param is passed so will use $_SERVER['HTTP_USER_AGENT'].
        // Process returned parameter values.
        $browser_user_agent = safe_data($browser['userAgent']);
        $browser_platform = safe_data($browser['platform']);
        $browser_device_type = safe_data($browser['deviceType']);
        $browser_name = safe_data($browser['name']);
        $browser_version = safe_data($browser['version']);
        		
		$created_by = 1; // System Administrator
		$created_timestamp = date('Y-m-d H:i:s');
		
		// A little formatting to make things look nicer...
		$fname = initCapData($fname);
	    $lname = initCapData($lname);
	    // $address = initCapData($address);	    
	    // $city = initCapData($city);
	    // $phone = sanitizePhone($phone);
		
		// (1 of 7) Create Person
        if ($insert_stmt = $connection->prepare("INSERT INTO persons (application_origin_id, username, email, password_hash, salt, user_level, 
												first_name, last_name, phone, gender, created_ip_address, created_browser_user_agent, created_browser_platform, created_browser_type, created_browser, created_browser_version, account_status, created_by, created_timestamp)
												
												VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('sssssssssssssssssss', $application_origin_id, $username, $email, $password, $random_salt, $user_level, $fname, $lname, $phone, $sex, $ip_address, $browser_user_agent, $browser_platform, $browser_device_type, $browser_name, $browser_version, $status, $created_by, $created_timestamp);
            
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                show_mysqli_error_message("register_member_account.php: INSERT person database failed", $connection);
				die;
            }
			$stmt->close();
        } else {
	        show_mysqli_error_message("register_account_process.php: INSERT person connection", $connection);
	        die;
        }
         		      
        $last_person_insert_id = mysqli_insert_id($connection);
        // echo 'Last Person Insert ID: '.$last_person_insert_id;
        
        // (2 of 7) Create Billing Address
		/* 
        if ($insert_stmt = $connection->prepare("INSERT INTO addresses (address, city, state_id, postal_code, country_id, created_by, created_timestamp)
												
												VALUES (?, ?, ?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('sssssis', $address, $city, $state_id, $zip, $country_id, $last_person_insert_id, $created_timestamp);
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                show_mysqli_error_message("register_member_account.php: INSERT Billing Address failed", $connection);
				die;
            }
			$stmt->close();
        } else {
	        show_mysqli_error_message("register_member_account.php: INSERT Billing Address connection failed", $connection);
	        die;
        }
        
        $last_address_insert_id = mysqli_insert_id($connection);
        
        // (3 of 7) Create Location (Billing)
        
		$effective_date = date('Y-m-d');
		$address_type_code = 'BILL';
        if ($insert_stmt = $connection->prepare("INSERT INTO locations (address_type_code, address_id, person_id, effective_date, created_by, created_timestamp)
												
												VALUES (?, ?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('siisis', $address_type_code, $last_address_insert_id, $last_person_insert_id, $effective_date, $last_person_insert_id, $created_timestamp);
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                show_mysqli_error_message("register_member_account.php: INSERT Billing Location failed", $connection);
				die;
            }
			$stmt->close();
        } else {
	        show_mysqli_error_message("register_account_process.php: INSERT Billing Location connection failed", $connection);
	        die;
        }
        
        // (4 of 7) Create Shipping Address

        if ($insert_stmt = $connection->prepare("INSERT INTO addresses (address, city, state_id, postal_code, country_id, created_by, created_timestamp)
												
												VALUES (?, ?, ?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('sssssis', $address, $city, $state_id, $zip, $country_id, $last_person_insert_id, $created_timestamp);
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                show_mysqli_error_message("register_member_account.php: INSERT Shipping Address failed", $connection);
				die;
            }
			$stmt->close();
        } else {
	        show_mysqli_error_message("register_member_account.php: INSERT Shipping Address connection failed", $connection);
	        die;
        }
        
        $last_address_insert_id = mysqli_insert_id($connection);
        
        // (5 of 7) Create Location (Shipping)

		$address_type_code = 'SHIP';
        if ($insert_stmt = $connection->prepare("INSERT INTO locations (address_type_code, address_id, person_id, effective_date, created_by, created_timestamp)
												
												VALUES (?, ?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('siisis', $address_type_code, $last_address_insert_id, $last_person_insert_id, $effective_date, $last_person_insert_id, $created_timestamp);
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                show_mysqli_error_message("register_member_account.php: INSERT Shipping Location failed", $connection);
				die;
            }
			$stmt->close();
        } else {
	        show_mysqli_error_message("register_member_account.php: INSERT Shipping Location connection failed", $connection);
	        die;
        }
        */
        
        // (6 of 7) Create Person Role
        
        $effective_timestamp = date('Y-m-d H:i:s');
        $role_type_code = 'MEMB'; // Member
        
        if ($insert_stmt = $connection->prepare("INSERT INTO person_roles (person_id, role_type_code, effective_timestamp, created_by, created_timestamp)
												
												VALUES (?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('issis', $last_person_insert_id, $role_type_code, $effective_timestamp, $last_person_insert_id, $created_timestamp);
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                show_mysqli_error_message("register_member_account.php: INSERT Person Role failed", $connection);
				die;
            }
			$stmt->close();
        } else {
	        show_mysqli_error_message("register_member_account.php: INSERT Person Role connection failed", $connection);
	        die;
        }
        
        $last_person_role_id = mysqli_insert_id($connection);    
        
        // (7 of 7) Create Member Enrollment
        
        $price_type_code = "VOLDSC";
        $primary_enrollment = 1; // 1 - TRUE, 0 - FALSE
        $active = 1; // 1 - active, 0 - inactive
        
        if ($insert_stmt = $connection->prepare("INSERT INTO enrollments (person_role_id, project_program_id, price_type_code, effective_timestamp, primary_enrollment, active, created_by, created_timestamp)
												
												VALUES (?, ?, ?, ?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('iissiiis', $last_person_role_id, $project_program_id, $price_type_code, $effective_timestamp, $primary_enrollment, $active, $last_person_insert_id, $created_timestamp);
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                show_mysqli_error_message("register_member_account.php: INSERT Person Role failed", $connection);
				die;
            }
			$stmt->close();
        } else {
	        show_mysqli_error_message("register_member_account.php: INSERT Person Role connection failed", $connection);
	        die;
        }
        
        $last_enrollment_id = mysqli_insert_id($connection);
        
        // (Bonus) Create Enrollment for Your Best Weight Program: Members
        
        $your_best_weight_members_project_program_id = 29;
        
        $price_type_code = "VOLDSC";
        $primary_enrollment = ''; // 1 - TRUE, 0 - FALSE
        $active = 1; // 1 - active, 0 - inactive
        
        if ($insert_stmt = $connection->prepare("INSERT INTO enrollments (person_role_id, project_program_id, price_type_code, effective_timestamp, active, created_by, created_timestamp)
												
												VALUES (?, ?, ?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('iissiis', $last_person_role_id, $your_best_weight_members_project_program_id, $price_type_code, $effective_timestamp, $active, $last_person_insert_id, $created_timestamp);
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                show_mysqli_error_message("register_member_account.php: INSERT Enrollment Your Best Weight: Members failed", $connection);
				die;
            }
			$stmt->close();
        } else {
	        show_mysqli_error_message("register_member_account.php: INSERT Enrollment Your Best Weight: Members connection failed", $connection);
	        die;
        }
        
        $last_enrollment_id = mysqli_insert_id($connection);    
        
        // Log the Account Creation in the System Log

        $event = 'CREATE ACCOUNT';
		$page = 'Registration';
		$log_description = 'CREATE ACCOUNT for Username: '.$username.' and Email: '.$email.' verification pending.';
		$flag = 'NULL';
		create_system_log($connection, $event, $page, $log_description, $flag);
		
		// SEND CONFIRMATION EMAIL
		
		if ($stmt = $connection->prepare("SELECT id FROM persons 
		WHERE email = ?  
        LIMIT 1")) {
			$stmt->bind_param('s', $email);  // Bind parameters
			$stmt->execute();    // Execute the prepared query.
			$stmt->store_result();
	 
			// get variables from result.
			$stmt->bind_result($user_id);
			$stmt->fetch();
			$stmt->close();			
				
			// $from = '"SOZO123 No-Reply" <no-reply@sozo123.com>';
			$from = $_SESSION['application']['name'].' <support@'.$_SESSION['application']['domain'].'>';
			$to = $email;
			$subject = 'Activate Your SupplementRelief Account';
			
			$activate = md5(uniqid(mt_rand(), true));
			
			$connection->query("UPDATE persons SET temp_password='$activate' WHERE id='$user_id'");
						
			$activelink = $_SESSION['application']['root_url'].'/php/prau.php?si='.$user_id.'&aa='.$activate;
						
			$body = 'Click <a href="'.$activelink.'">here</a> to <b>Activate Your SupplementRelief Member Account</b>.<br><br>';			

			send_email_html($from, $to, $subject, $body, "");
									
			// Send email to Customer Support informing them of a new registration
			
			$from = $_SESSION['application']['name'].' <support@'.$_SESSION['application']['domain'].'>';
			$to = 'support@'.$_SESSION['application']['domain'];
			$subject = 'SupplementRelief Member Account Registration';
			
			$body = '<b>Name: </b>'.$fname.' '.$lname;
			$body .= '<br><b>Username: </b>'.$username;
			$body .= '<br><b>Email: </b>'.$email;
			$body .= '<br><b>IP Address: </b>'.$ip_address;
			$body .= '<br><b>Registered: </b>'.date('m/d/y h:i A T', strtotime($created_timestamp)).'<br>';

			send_email_html($from, $to, $subject, $body, "");
			
			/*
			echo 'From: '.$from.'<br>';
			echo 'To: '.$to.'<br>';
			echo 'Subject: '.$subject.'<br>';
			echo 'Body: '.$body.'<br>';
			*/
			
			// Confirmation Message to User
			// $_SESSION['message_type'] = 'alert-box success radius';	
			/*			
			$_SESSION['message'] = '
			<div class="row">		
				<div class="small-12 columns">
						
					<div class="panel">
					  	<h3><i class="fa fa-thumbs-up fa-lg" style="color: #5CB64B;"></i>&nbsp;&nbsp;Thank You for registering for your Member Account!</h3>
					  	<p><b>We just emailed you at <span style="color:red;"><b>'.$email.'</b></span> to make sure we have the right email address</b>. Please leave this browser window open until you see your email and respond. After you <b>click the verification link in that email</b>, you will be ready to login and take advantage of your member benefits!</p>
					</div>
					
					<div class="panel">
							
						<h1 style="color:red;"><i class="fa fa-exclamation-triangle"></i>&nbsp;&nbsp;If You DO NOT SEE YOUR EMAIL</h1>
						
						<ol>
				
							<li>Please check to see <b>if you entered your Email Address correctly</b>. You entered: <span style="color:red;"><b>'.$email.'</b></span>.</li>
					
							<li>If you entered your email address correctly, then the email we sent is probably <b>in your SPAM or JUNK email folder</b>. Please find it and respond to it. Then <b>change your SPAM settings to allow future email from support@supplementrelief.com so they will go into your email inbox</b>. Your email was sent from <b>support@supplementrelief.com</b> and has the subject <b>Activate Your SupplementRelief Account</b>. Remember <b>you still need to respond to the email</b> even if it was sent to your SPAM folder <b>or your account will not be activated</b>.</li>
							
							<li>If the email is <b>still not in your INBOX, SPAM or JUNK email folder</b> please contact us and we will setup your account for you.</li>
							
							<li>Please call <b>(888) 424-0032</b> or <a href="../contact/">email</a> if you need any help.</li>
							
						</ol>
						
					</div>
										
				</div>
			</div>';
			*/
			
			$_SESSION['member_registration_email'] = $email;					
			// Redirect user to Register Member Account Confirmation page.
			$target_uri = '../register-member-account-confirmation/';
			header("location: ".$target_uri);	
			exit();	
	
		} else {
			show_mysqli_error_message("register_account_process.php: EMAIL", $connection);
			die;
		}
  }
}

if (!empty($error_msg)) {
	
	$_SESSION['message_type'] = 'alert-box alert radius';					
	$_SESSION['message'] = $error_msg;
	show_session_message();	
}

// Display the Register Account form.

/*
// Build the Select List for States
$queryStates = '
SELECT 
st.state_id,   
st.state_abbr, 
st.state_name, 
st.country_abbr, 
co.country_name 
FROM states st 
LEFT JOIN country co ON st.country_abbr = co.country_abbr 
ORDER BY state_name ASC';

// echo $queryStates;
// die();

$result_state = mysql_query($queryStates);

if (!$result_state) {
	show_mysqli_error_message($queryStates, $connection);
	die;
}
			
$state_options = '<option value="">Select a State...</option>';

while($state_row = mysql_fetch_array($result_state)) {
	$selected = '';
	if(($state ? $state : $row1['p']) == $state_row['state_id']) $selected = ' selected';
	$state_options .= '<option value="'.$state_row['state_id'].'" '.$selected.'>'.$state_row['state_name'].' - '.$state_row['country_name'].'</option>';
}

mysql_free_result($result_state);
*/
 
?>

<div class="row">		
	<div class="small-12 columns">
    	<br>
		<a href="#" class="button radius large expand" data-reveal-id="standard-membership" title="view Member Benefits description"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Click to See Member Benefits</a>
					
	</div>
</div>

<div class="row">
	
	<div class="small-12 medium-3 columns">
	
		<center><img src="https://cdn-manager.net/media/images/free-membership-small-block.jpg" alt="free membership"></center>
		<div class="caption">Enjoy Savings, Promotional Offers, Expert Wellness Education & More!</div> 
	
	</div>
			
	<div class="small-12 medium-9 columns">

		<div id="featureVideo">
			<video controls style="width:100%;height:auto;" poster="https://cdn-manager.net/media/images/woman-hand-apple-computer-keyboard.jpg">
				<source src="https://cdn-manager.net/media/videos/supplementrelief-membership-promotion.mp4" type="video/mp4">  
			    <source src="https://cdn-manager.net/media/videos/supplementrelief-membership-promotion.webm" type="video/webm">    
		    Your browser does not support the video tag.
		  	</video>
		</div>
				
	</div>
	
</div>

<div id="editPerson">

	<div class="row">			
		<div class="small-12 columns">

			<form data-abide action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" onsubmit="submitRegisterAccountNow(event);" id="register_account_form">
			<!-- submitRegisterAccountNow /..... -->
	
				<fieldset>
				  <legend>Register for Your FREE Member Account</legend>
				  
				  	<input type="hidden" name="project_program_id" value="21" />
				      
					<div class="row">
						<div class="small-12 columns">
							
							<p>Watch the <b>short video</b> for an overview of Member Benefits. There is <b>no cost and no obligation to purchase anything</b>. We <b>DO NOT ask for your credit card information</b>. We <b>DO NOT share</b> your information with <b>ANYONE</b>. You <b>DO NOT</b> have to have a Member Account to shop in our online store or use our website. <!-- Remember to <b>Login to your Member Account</b> when on our website to take advantage of your Member Benefits. --></p>
							<!--
							<p>If you register today we will include at no charge the <strong>Your Best Weight</strong> program that you can view online anytime at your convenience.</p>
</p> -->
	
							<hr>
							
							<?php // include('../php/important_account_notice.php'); ?>
							
							<p>Please provide the information requested below then press the <strong>REGISTER</strong> button. You will immediately be sent an email to <b>verify your registration</b> that you must respond to <b>before you can use your Member Account</b>. If you do not see the email please check your <b>SPAM or JUNK</b> email folder.</p>
																					
						</div>
					</div>
						
					<div class="row">
								
						<div class="small-12 medium-6 columns">		
					  		<label>First Name <small>Optional</small>
					    			<input autofocus="" type="text" name="first_name" placeholder="Jane" value="<?php echo ($fname ? $fname : ''); ?>" />
					  			</label>
					  		<small class="error">First Name is optional</small>    
						</div> 
						 		
						<div class="small-12 medium-6 columns"> 		
					  		<label>Last Name <small>Optional</small>
					    			<input type="text" name="last_name" placeholder="Doe" value="<?php echo ($lname ? $lname : ''); ?>" />
					  		</label>
					  		<small class="error">Last Name is optional</small>	    
						</div>
						
					</div>
					
					<div class="row">
						
						<div class="small-12 medium-6 columns"> 		
					  		<label>Email <small>Required</small>
					    		<input type="text" name="email" placeholder="someone@domain.com" value="<?php echo ($email ? $email : ''); ?>"/>
					  		</label>
					  		<small class="error">Email is required</small>	    
						</div>
							 																			
						<div class="small-12 medium-6 columns">		
				  		<label>User ID <small>Required</small>
				    			<input type="text" name="username" placeholder="janedoe (or can be same as Email)" value="<?php echo ($username ? $username : ''); ?>"/>
				  			</label>
				  		<small class="error">Username is required</small>    
						</div>
						
					</div>
												
					<div class="row"> 

						<div class="small-12 medium-6 columns">
						    <label>Password <small>Required</small>
						      <input type="password" required name="password" placeholder="********" id="password"/>
						    </label>
						    <small class="error">Password is required</small>
						</div>
						
						<div class="small-12 medium-6 columns">
						    <label>Confirm Password <small>Required</small>
						      <input type="password" required name="confirm_password" placeholder="********" id="confirm_password"/>
						    </label>
						    <small class="error">Confirm Password is required</small>
						</div>	    
				    
			  	</div>			  
					
					<!--			
					<div class="row">												

						<div class="small-12 medium-3 columns end">
							<label>Birth Date
								<input type="text" class="datepicker_date" name="birth_date" placeholder="MM/DD/YYYY" value="<?php echo ($person_birth_date ? $person_birth_date : ''); ?>" />
							</label>
							<small class="error">Birth Date is optional and must be formatted as MM/DD/YYYY</small> 	  
						</div>
						
					</div>
					-->		
					
										<!--										
					<div class="row">
						
				  		<div class="medium-6 columns">
			  				<label>Address <small>Required</small>
			  					<textarea required name="address" placeholder="2501 Easy St." rows="6" /><?php echo ($address ? $address : ''); ?></textarea>
			  				</label>
			  				<small class="error">Address is required</small>		  			
				    	</div>
				    		   
					  	<div class="medium-6 columns">
					  		
						    <div class="row">		    
						    	
						    	<div class="small-12 large-6 columns">
						    	 	<label>City <small>Required</small>
						    	 	  	<input type="text" required name="city" placeholder="Detroit" value="<?php echo ($city ? $city : ''); ?>" />  	
						    	  	</label>
						    	  	<small class="error">City is required</small>
						    	</div>  	    	
								
								<div class="small-12 large-6 columns">		    	
							    	<label>State <small>Required</small>
							    		<select name="state">
							    	    	<?php echo $state_options; ?>
							    	    </select>
							    		  
							    	</label>
							    	<small class="error">State is required</small>
							    </div>							    		    		
						    								    
						    </div>
							    
						    <div class="row">
							    
							    <div class="small-6 columns end">
							    	<label>Postal Code <small>Required</small>
							    		<input type="text" required name="postal_code" placeholder="48201, 48201-1234, or M5P 2N7" value="<?php echo ($zip ? $zip : ''); ?>" /> 		  
							    	</label>
							    	<small class="error">Postal Code is required</small>	
							    </div>
							    
						    </div>	
									            		    	  		  	 	    	    	        
						</div>
					</div>
					-->
					
					<!--						
					<div class="row">
						
						<div class="small-12 medium-4 columns"> 		
					  		<label>Email <small>Required</small>
					    		<input type="text" name="email" placeholder="someone@domain.com" value="<?php echo ($email ? $email : ''); ?>"/>
					  		</label>
					  		<small class="error">Email is required</small>	    
						</div>
	
						<div class="small-12 medium-4 columns">
							<label>Phone <small>Optional</small>
								<input type="tel" name="phone" placeholder="(555) 555-1212 or 555-555-1212" value="<?php echo ($phone ? $phone : ''); ?>"/>			
							</label>
							<small class="error">Phone must be formatted as (555) 555-1212</small> 	  
						</div>
											
						<div class="small-12 medium-4 columns">
							<label>Gender <small>Optional</small></label> 
						  		<input  type="radio" name="gender" value="F" <?php if($sex == 'F') echo 'checked'; ?> id="genderFemale"><label for="genderFemale">Female</label>
						  		<input  type="radio" name="gender" value="M" <?php if($sex == 'M') echo 'checked'; ?> id="genderMale"><label for="genderMale">Male</label>
						  		<small class="error">Choose Female or Male</small>
						</div>
						
					</div>
					-->
																	
					<div class="row">
					  <div class="small-12 columns">
					    <label>
					    	<input class="button large radius show-for-medium-up expand success" type="submit" value="REGISTER">      
							<input class="button large radius expand show-for-small success" type="submit" value="REGISTER">      
					    </label>
					  </div>
					  <div class="small-12 columns">
						 <div class="right"><a href="#" title="view our Privacy Policy" data-reveal-id="footer-privacy-policy"><i class="fa fa-lock" aria-hidden="true"></i>&nbsp;Privacy Policy</a>&nbsp;&nbsp;</div> 
					  </div>
					</div>
																		
				</fieldset>
				
			</form>
		</div>
	</div>	
</div>

<br />
<?php include('../php/standard_membership_modal.php'); ?>