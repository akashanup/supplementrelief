<div id="shopping_cart_container">	
<?php
	include_once('../includes/header.php');
	include_once('../store/includes/defines.php');
	include_once('../store/includes/functions.php');
	
	$order_id 		= '';
	$order_item_id 	= '';	

	if (isset($_GET['order_id'])) 
	{ 
		$order_id = safe_sql_data($connection, $_GET['order_id']); 
	}
	if (isset($_GET['order_item_id'])) 
	{ 
		$order_item_id = safe_sql_data($connection, $_GET['order_item_id']); 
	}

	// unset($_SESSION['cart']);

	reorder($connection, $order_id, $order_item_id);

	echo '</div><a class="close-reveal-modal">&#215;</a>';
?>