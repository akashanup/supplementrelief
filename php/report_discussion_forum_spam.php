<?php 

// echo 'Begin report_discussion_forum_spam.php.<br />';
// die;

// error_reporting(E_ALL);

include_once('../includes/header.php');

// show_array($_SESSION);
// die;

// https://supplementrelief.com/php/report_discussion_forum_spam.php?comment_type=comment&comment_id=328
// https://supplementrelief.com/php/report_discussion_forum_spam.php?forum_type=education&comment_type=comment&comment_id=394

// show_array($_GET);
// die;

if (isset($_GET['forum_type'])) { $forum_type = $_GET['forum_type']; } // type = blog, education, product, recipe, resource
if (isset($_GET['comment_type'])) { $content_type = $_GET['comment_type']; } // type = comment or reply
if (isset($_GET['comment_id'])) { $content_id = $_GET['comment_id']; }

// Capture calling URI to return after processing.
$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#discuss';

if ($content_type == 'comment') {

	// Query for the Comment being reported as SPAM
	
	$queryComment = 'SELECT 
		dt.id, 
		dt.created_timestamp, 
		dt.title, 
		dt.text, 
		dt.file_document_url, 
		dt.file_document_type,  
		dt.file_image_url, 
		dt.file_image_type, 
		dt.file_video_url, 
		dt.file_video_type, 
		dt.created_by, 
		dt.modified_timestamp, 
		pe.first_name, 
		pe.last_name,
		cabp.title AS blog_post_title, 
        caed.title AS education_title, 
        bpu.name AS product_title,  
		carc.title AS recipe_title,  
		care.title AS resource_title 
		FROM discussion_threads dt 
		LEFT JOIN persons pe ON dt.created_by = pe.id 
		LEFT JOIN content_assets cabp ON dt.blog_post_id = cabp.id 
        LEFT JOIN content_assets caed ON dt.web_page_id = caed.id 
        LEFT JOIN brand_product_usages bpu ON dt.brand_product_usage_id = bpu.id 
        LEFT JOIN content_assets carc ON dt.recipe_id = carc.id 
        LEFT JOIN content_assets care ON dt.resource_id = care.id 
		WHERE dt.id = '.$content_id.'  		
		LIMIT 1';
				
	// echo $queryComment . '<br /><hr />';
	
	$result_comment = mysqli_query($connection, $queryComment);
	
    if (!$result_comment) {
		show_mysqli_error_message($queryComment, $connection);
		die;
	}
	
	while($r = mysqli_fetch_assoc($result_comment)) {
    	
    	// show_array($r);
    	// die;
	
		$comment_id = $content_id;
		$comment_text = $r['text'];
		$comment_file_document_url = $r['file_document_url'];	
		$comment_file_image_url = $r['file_image_url'];
		$comment_file_video_url = $r['file_video_url'];	
		$comment_created_by = $r['created_by'];
		$comment_created_first_name = $r['first_name'];
		$comment_created_last_name = $r['last_name'];
		$comment_created_timestamp = $r['created_timestamp'];
		
		// echo 'Forum Type is: '.$forum_type.'<br>';
		// die;
		
		if ($forum_type == 'blog_post') { 
            $post_title = $r['blog_post_title'];
        } elseif ($forum_type == 'education') {
            $post_title = $r['education_title'];
        } elseif ($forum_type == 'product') {
            $post_title = $r['product_title'];
        } elseif ($forum_type == 'recipe') {
            $post_title = $r['recipe_title'];
        } elseif ($forum_type == 'resource') {
            $post_title = $r['resource_title'];
        }      
        
        // echo 'Forum Type is: '.$forum_type.'<br>';
        // echo 'Post Title is: '.$post_title.'<br>';
		// die;
 
	} 
	
	// mysqli_free_result($result_comment);
	
} 

if ($content_type == 'reply') {

	// Query for the Reply being reported as SPAM
	
	$queryReply = 'SELECT 
		dr.id, 
		dr.text, 
		dr.created_by, 
		dr.created_timestamp, 	
		pe.first_name, 
		pe.last_name,
        cabp.title AS blog_post_title, 
        caed.title AS education_title, 
        bpu.name AS product_title,  
		carc.title AS recipe_title,  
		care.title AS resource_title 
		FROM discussion_replies dr 
		LEFT JOIN discussion_threads dt ON dr.discussion_thread_id = dt.id 
		LEFT JOIN persons pe ON dr.created_by = pe.id 
		LEFT JOIN content_assets cabp ON dt.blog_post_id = cabp.id 
        LEFT JOIN content_assets caed ON dt.web_page_id = caed.id 
        LEFT JOIN brand_product_usages bpu ON dt.brand_product_usage_id = bpu.id  
        LEFT JOIN content_assets carc ON dt.recipe_id = carc.id 
        LEFT JOIN content_assets care ON dt.resource_id = care.id 
		WHERE dr.id = '.$content_id.'  		
		LIMIT 1';
				
	// echo $queryReply . '<br /><hr />';
	
	$result_reply = mysqli_query($connection, $queryReply);
	
    if (!$result_reply) {
		show_mysqli_error_message($queryReply, $connection);
		die;
	}
		
	while($r2 = mysqli_fetch_assoc($result_reply)) { 
    	
    	// show_array($r2);
    	// die;
	
		$comment_id = $content_id;
		$comment_text = $r2['text'];
		$comment_created_by = $r2['created_by'];
		$comment_created_first_name = $r2['first_name'];
		$comment_created_last_name = $r2['last_name'];
		$comment_created_timestamp = $r2['created_timestamp'];
		
        if ($forum_type == 'blog_post') { 
            $post_title = $r2['blog_post_title'];
        } elseif ($forum_type == 'education') { 
             $post_title = $r2['education_title'];
        } elseif ($forum_type == 'product') {
            $post_title = $r2['product_title'];
        } elseif ($forum_type == 'recipe') {
            $post_title = $r2['recipe_title'];
        } elseif ($forum_type == 'resource') {
            $post_title = $r2['resource_title'];
        }
        
	}
		     	
	// mysqli_free_result($result_reply);
	
}

$created_timestamp = date('Y/m/d H:i:s');
$reported_timestamp = date('m/d/y h:i:s A T'); 
 
if ($forum_type == 'blog_post') { 
    // echo 'forum type is blogpost<br>';
    $forum_name = 'Blog Post';
    if ($comment_type == 'comment') { 
    } elseif ($comment_type == 'reply') {
        $post_title = $r2['blog_post_title'];
    }
} elseif ($forum_type == 'education') {
    $forum_name = 'Education';   
} elseif ($forum_type == 'product') {
    $forum_name = 'Product';
} elseif ($forum_type == 'recipe') {
    $forum_name = 'Recipe';
} elseif ($forum_type == 'resource') { 
    $forum_name = 'Resource';
} 

if (isset($_SESSION['user']['full_name'])) { 
    $display_reporting_name = $_SESSION['user']['full_name'];
} else {
    $display_reporting_name = ' Guest';   
}

if (isset($comment_created_first_name)) { 
    $display_comment_name = $comment_created_first_name.' '.$comment_created_last_name;
} else {
    $display_comment_name = ' Guest';   
}

// $to = $_SESSION['blog_discussion_forum_moderator_email'];
// Add attribute to discussion_forum_configurations for email notification. Also look at enrollments- might already be handled in there.
// $to = 'support@supplementrelief.com';
$to = $_SESSION['application']['email'];
// $from = 'support@supplementrelief.com';
$from = $_SESSION['application']['email'];

$subject = 'Report '.$forum_name.' Forum SPAM';

// $BCC = 'jay.todtenbier@rtsadventures.com';
// $BCC = $_SESSION['application']['email'];

$message = '
<br>'.
$display_reporting_name.' is reporting the following <b>'.$forum_name.' Forum Comment</b> created '.date('m/d/y h:i A T', strtotime($comment_created_timestamp)).' by '.$display_comment_name.' as SPAM.
<br /><br />
<b>Program Name:</b> '.$_SESSION['enrollment']['project_program_name'].'
<br /><br />
<b>Content Title:</b> '.$post_title.'
<br><br>
<b>Comment ID:</b> '.$comment_id.'<br />
<b>Comment Text:</b> '.$comment_text.'
<br><br>
Please <a href="https://supplementrelief.com/login/">login</a> to the Program, go to the Blog Post and review the Comment or Reply (including any file attachments such as Documents, Images or Videos) and remove the content in question from display if appropriate. You may copy the <b>Comment Text</b> and do a <b>Find</b> on the web page to locate the Comment quickly.
<br /><br />
SupplementRelief Administration Services
<br><br>
';

// send_email_html($from, $to, $subject, $message, "");

if (send_email_html($from, $to, $subject, $message, "")) {
        	
} else {
    // echo 'email send failed';
    // die;
}

// Confirmation message to user.
$_SESSION['message_type'] = 'alert-box success radius';				
$_SESSION['message'] = '<p><i class="fa fa-check fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Thank you for bringing this matter to our attention. The <strong>Forum Moderator</strong> has been notified and will review the post and determine what type of action to take.</p>';

// Return to calling form using target uri	
header("location: ".$_SESSION['target_uri']);
exit();

?>


