<!-- Begin request_user_id_form.php -->

<script type="text/JavaScript" src="../js/sha512.js"></script> 
<script type="text/JavaScript" src="../js/forms.js"></script>

<?php 

if (login_check($connection) == true) {
	
	// echo "You cannot reset your password while you are logged in";
	
	$_SESSION['message_type'] = 'alert-box alert radius';					
	$_SESSION['message'] = '<p><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;&nbsp;You cannot <b>Reset your Password</b> while you are logged in.</p>';
	show_session_message();
	exit;
	
}

// show_array($_SESSION);
// show_array($_SERVER);
// show_array($_POST);

if (@isset($_POST['email'], $_POST['cp']) && $_POST['cp'] == $_SESSION['user']['token'] ) {
	
	if ($_SESSION['captcha'] == $_POST['captcha_input']) {
	
		$email = sanitize($_POST['email']);
	
		// Check for user with this email
				
		if ($stmt = $connection->prepare("SELECT id, username  
			FROM persons 
			WHERE email = ? 
			LIMIT 1")) {
	
			$stmt->bind_param('s', $email);  // Bind parameters.
			$stmt->execute();    // Execute the prepared query.
			$stmt->store_result();
			
			if ($stmt->num_rows == 1) {
			
				// email found
				// get variables from result.
				$stmt->bind_result($user_id, $username);
				$stmt->fetch();
				$stmt->close();
								
				$from = $_SESSION['application']['name'].' <support@'.$_SESSION['application']['domain'];
				
				$to = $email;
				$subject = $_SESSION['application']['name'].' User ID Request';
				
				/*
				$resetcode = md5(uniqid(mt_rand(), true));
				
				$connection->query("UPDATE persons SET temp_password='$resetcode' WHERE id='$user_id'");
				$stmt->close();
								
				$resetlink = $_SESSION['application']['root_url'].'/php/prau.php?si='.$user_id.'&pr='.$resetcode;
				*/
				
				// $body = 'Click <a href="'.$resetlink.'">here</a> to reset your <b>Account Password</b>.<br><br>';
				$body = 'A request was just made for the User ID for the Account using Email Address: <b>'.$to.'</b> by choosing the <b>Forgot User ID</b> option on the Login page.<br><br>The <b>User ID</b> is: '.$username.'.<br>';
												
				send_email_html($from, $to, $subject, $body, "");
								
				?>
				<div class="row">		
					<div class="small-12 columns">
						<br>
						<div class="panel">
							<p><i class="fa fa-thumbs-o-up fa-lg icon-green"></i>&nbsp;&nbsp;The <b>User ID</b> was emailed to: <b><?php echo $to; ?></b>. Please check your email then return to the <a href="../login/"><b>Login</b></a> page</b>.</p>
							<hr>
							<h3 style="color:red;"><i class="fa fa-exclamation-circle"></i>&nbsp;&nbsp;If You Do Not See Your Email</h3><p>Please check your <b>email SPAM/JUNK folder</b>. The email was sent from <b><?php echo $_SESSION['application']['email']; ?></b> and is entitled <b><?php echo $_SESSION['application']['name']; ?> User ID Request</b>. You will need to <b>update your Address Book</b> to allow future email from <b><?php echo $_SESSION['application']['email']; ?></b>.</p>
							<p>Please call <b><?php echo $_SESSION['application']['phone']; ?></b> or <a href="../contact/">email</a> if you need any assistance.</p>
						</div>					
					</div>
				</div>				
				
				<?php
				
				exit;
			
			} else {
				// email not found
				$stmt->close();
				$_SESSION['message_type'] = 'alert-box alert radius';					
				$_SESSION['message'] = '
					<p><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;&nbsp;The was no account found using the <strong>Email Address: '.$email.'</strong>. If this is your <b>Email Address</b>, and you do not already have an account, you can create a new account right now by clicking <a href="../register-member-account/" class="linkUnderlineWhite">here</a>.</p>
					<p>If you need any help please call <b>'.$_SESSION['application']['phone'].'</b> or <a href="../contact/" class="linkUnderlineWhite">email</a> and we will help you setup your account.</p>';
				show_session_message();
			}
			
		}
		
	} else {
		// Captcha did not match
		$_SESSION['message_type'] = 'alert-box alert radius';		
		$_SESSION['message'] = '<p><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;&nbsp;The <strong>Reset Code</strong> you entered is <strong>NOT VALID</strong>. Please try again.</p>';
		show_session_message();

	}
		
}

?>

<div class="row">
	<div class="small-12 columns">

		<form data-abide action="<?php echo esc_url($_SERVER['PHP_SELF']);?>" method="post" />
		<!-- <form data-abide action="" method="post" /> -->
			
		  <fieldset>	  
		    <legend>Request User ID</legend>
		    
		    <input type="hidden" name="cp" value="<?php echo $_SESSION['user']['token']; ?>">
		    
		    <div class="row">
		      <div class="small-12 columns">
		      	<p>Please enter the <b>Email Address</b> on your account, the <b>5-digit Reset Code</b> in the dark blue box below, then press the <b>REQUEST USER ID</b> button. The User ID will be <b>emailed to the Email Address on your account</b>. This is for your protection to validate that this is your account. After you receive your email please return to the <a href="../login/">Login</a> page and <b>Login with your User ID</b>.</p>
		      	<p>If you forgot your <b>Email Address</b>, please call <b><?php echo $_SESSION['application']['phone']; ?></b> or <a href="../contact/" style="text-decoration: underline;"><b>email</b></a> and we will assist you.</p>

		      </div>
		    </div>
		    		
			<div class="row">
			
			  <div class="small-12 medium-6 columns">
			    <label>Email Address
			      <input type="text" required name="email" placeholder="someone@domain.com" value="" />
			    </label>
			    <small class="error">Email is required</small>    
			  </div>
			  
			  <div class="small-12 medium-6 columns">
			  
			    <label>Are you human? <b>Reset Code</b>	      
			      <img src="../includes/captcha.php" style="margin-bottom: -8px;" /><input type="text" required name="captcha_input" value=""/>
			    </label>
			    <small class="error">Valid Code is required</small>	    
			    	    
			  </div>
			  
			</div>
			
			<div class="row">
			  
			  <!--<div class="small-12 medium-6 medium-push-6 columns">-->
			  <div class="small-12 columns">	  
			    <label>	      
			      <input class="button large radius show-for-medium-up expand" type="submit" value="REQUEST USER ID">
			      <input class="button large radius expand show-for-small-only" type="submit" value="REQUEST USER ID">      
			    </label>    	    
			  </div>
			  			  
			</div>		
										
		  </fieldset>
		  		
		</form>
	</div>
</div>