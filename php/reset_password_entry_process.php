<?php

// echo 'Begin reset_password_entry_process.php.<br />';

include('../includes/header.php');

if (strlen($_SESSION['user']['user_id']) != 0) {

// validate for required fields
if($_POST['password'] == '' || $_POST['confirm_password'] == '') {
	$_SESSION['message_type'] = 'alert-box alert radius';	
	$_SESSION['message'] = '<p>Both <strong>Password</strong> and <strong>Confirm Password</strong> are required. Please try again.</p>';
	header("location: ../reset-password-entry/");
	exit();
}

// check for password and confirm_password to be same value
if($_POST['password'] != $_POST['confirm_password']) {
	$_SESSION['message_type'] = 'alert-box alert radius';		
	$_SESSION['message'] = '<p><strong>Password</strong> and <strong>Confirm Password</strong> do not match. Please try again.</p>';
	header("location: ../reset-password-entry/");
	exit();
}

// get sign-up data
// Change this to mysqli
$query = 'SELECT * FROM persons WHERE id = "'.$_SESSION['user']['user_id'].'"';
$result = mysql_query($query);
$row = mysql_fetch_assoc($result);

$pw_hash = doubleSalt($_POST['password'],$row['username']);

// UPDATE PASSWORD
$query1 = 'UPDATE persons SET password_hash = "'.$pw_hash.'", temp_password = NULL WHERE id = '.$row['id'];

$result1 = mysql_query($query1);

// Set user session variables for log and user message

$_SESSION['user']['username'] = $row['username'];
$_SESSION['user']['first_name'] = $row['first_name'];
$_SESSION['user']['last_name'] = $row['last_name'];

// log system event			
$event = 'PASSWORD';
$page = 'Reset Password';
$log_description = 'User RESET PASSWORD: '.$_SESSION['user']['username'].'.';
$flag = 'NULL';
create_system_log($connection, $event, $page, $log_description, $flag);

// Set user message
$_SESSION['message_type'] = 'alert-box success radius';	
$_SESSION['message'] = '<p>Thank you <strong>'.$_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name'].'</strong>. Your <strong>NEW PASSWORD</strong> has been saved. Please <strong>Login</strong> using your <strong>NEW PASSWORD.</strong></p>';

// Clear session variables
// use unset
$_SESSION['user']['user_id'] = '';
$_SESSION['user']['username'] = '';
$_SESSION['user']['full_name'] = '';
$_SESSION['user']['email'] = '';
$_SESSION['user']['access_level'] = '';
$_SESSION['user']['login_attemps'] = '';

// Redirect to Login page

header("location: ../login/");
exit();

}

?>