<!-- Begin reset_password_form.php -->

<script type="text/JavaScript" src="../js/sha512.js"></script> 
<script type="text/JavaScript" src="../js/forms.js"></script>

<?php 

if (login_check($connection) == true) {
	
	// echo "You cannot reset your password while you are logged in";
	
	$_SESSION['message_type'] = 'alert-box alert radius';					
	$_SESSION['message'] = '<p><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;&nbsp;You cannot <b>Reset your Password</b> while you are logged in.</p>';
	show_session_message();
	exit;
	
}

// show_array($_SESSION);
// show_array($_SERVER);


if (@isset($_POST['email'], $_POST['cp']) && $_POST['cp'] == $_SESSION['user']['token'] ) {
	
	if ($_SESSION['captcha'] == $_POST['captcha_input']) {
	
		$email = sanitize($_POST['email']);
	
		// Check for user with this email
				
		if ($stmt = $connection->prepare("SELECT id 
			FROM persons 
			WHERE email = ? 
			LIMIT 1")) {
	
			$stmt->bind_param('s', $email);  // Bind parameters.
			$stmt->execute();    // Execute the prepared query.
			$stmt->store_result();
			
			if ($stmt->num_rows == 1) {
			
				// email found
				// get variables from result.
				$stmt->bind_result($user_id);
				$stmt->fetch();
				$stmt->close();
								
				// $from = '"SOZO123 No-Reply" <no-reply@sozo123.com>';
				$from = $_SESSION['application']['name'].' <support@'.$_SESSION['application']['domain'];
				
				$to = $email;
				$subject = $_SESSION['application']['name'].' Password Reset Link';
				
				$resetcode = md5(uniqid(mt_rand(), true));
				
				$connection->query("UPDATE persons SET temp_password='$resetcode' WHERE id='$user_id'");
				$stmt->close();
								
				$resetlink = $_SESSION['application']['root_url'].'/php/prau.php?si='.$user_id.'&pr='.$resetcode;
				
				$body = 	'Click <a href="'.$resetlink.'">here</a> to reset your <b>Account Password</b>.<br><br>';
												
				send_email_html($from, $to, $subject, $body, "");
				
				// echo "A password reset link has been sent to your email.";
								
				?>
				<div class="row">		
					<div class="small-12 columns">
						<br>
						<div class="panel">
							<p><i class="fa fa-thumbs-o-up fa-lg icon-green"></i>&nbsp;&nbsp;A <strong>Password Reset Link</strong> was sent to: <b><?php echo $to; ?></b>. Please follow the simple instruction in the email to complete your <b>Password Reset</b>.
							<hr>
							</p><h3 style="color:red;"><i class="fa fa-exclamation-circle"></i>&nbsp;&nbsp;If You Do Not See Your Email</h3><p>Please check your <b>email SPAM/JUNK folder</b>. The email was sent from <b>support@supplementrelief.com</b> and is entitled <b><?php echo $_SESSION['application']['name']; ?> Password Reset Link</b>. Please <b>update your Address Book</b> to allow future email from <b><?php echo $_SESSION['application']['email']; ?></b>. Remember to <b>respond to the Password Reset Link</b>.</p>
							<p>Please call <b>(888) 424-0032</b> or <a href="../contact/">email</a> if you need any assistance.</p>
						</div>					
					</div>
				</div>				
				
				<?php
				
				exit;
			
			} else {
				// email not found
				$stmt->close();
				$_SESSION['message_type'] = 'alert-box alert radius';					
				$_SESSION['message'] = '
				<p><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;&nbsp;The <strong>Email Address '.$email.'</strong> was not found. If this is your <b>Email Address</b>, and you do not already have an account, you can create a new account right now by clicking <a href="../register-member-account/" class="linkUnderlineWhite">here</a>.</p>
				<p>If you need any help please call <b>'.$_SESSION['application']['phone'].'</b> or <a href="../contact/" class="linkUnderlineWhite">email</a> and we will help you setup your account.</p>';
				show_session_message();
			}
			
		}
		
	} else {
		// Captcha did not match
		$_SESSION['message_type'] = 'alert-box alert radius';		
		$_SESSION['message'] = '<p><i class="fa fa-exclamation-circle fa-lg"></i>&nbsp;&nbsp;The <strong>Reset Code</strong> you entered is <strong>NOT VALID</strong>. Please try again.</p>';
		show_session_message();

	}
		
}

?>

<div class="row">
	<div class="small-12 columns">

		<form data-abide action="<?php echo esc_url($_SERVER['PHP_SELF']);?>" method="post" />
			
		  <fieldset>	  
		    <legend>Reset Password</legend>
		    
		    <input type="hidden" name="cp" value="<?php echo $_SESSION['user']['token']; ?>">
		    
		    <div class="row">
		      <div class="small-12 columns">
		      	<p>Please enter the <b>Email Address</b> on your account, the 5-digit <b>Reset Code</b> in the dark blue box below, then press the <b>RESET PASSWORD</b> button. You will be <b>sent an email to the Email Address provided</b>. This is for your protection to <b>validate that this is really your account</b>. Click the link in the email and you will be taken to the page where you can <b>choose your new Password.</p>
		      	<p>If you forgot your Email Address, please call <b><?php echo $_SESSION['application']['phone']; ?></b> or <a href="../contact/" style="text-decoration: underline;"><b>email</b></a> and we will assist you.</p>

		      </div>
		    </div>
		    		
			<div class="row">
			
			  <div class="small-12 medium-6 columns">
			    <label>Email Address
			      <input type="text" required name="email" placeholder="someone@domain.com" value="" />
			    </label>
			    <small class="error">Email is required</small>    
			  </div>
			  
			  <div class="small-12 medium-6 columns">
			  
			    <label>Are you human? <b>Reset Code</b>	      
			      <img src="../includes/captcha.php" style="margin-bottom: -8px;" /><input type="text" required name="captcha_input" value=""/>
			    </label>
			    <small class="error">Valid Code is required</small>	    
			    	    
			  </div>
			  
			</div>
			
			<div class="row">
			  
			  <!--<div class="small-12 medium-6 medium-push-6 columns">-->
			  <div class="small-12 columns">	  
			    <label>	      
			      <input class="button large radius show-for-medium-up expand" type="submit" value="RESET PASSWORD">
			      <input class="button large radius expand show-for-small-only" type="submit" value="RESET PASSWORD">      
			    </label>    	    
			  </div>
			  			  
			</div>		
										
		  </fieldset>
		  		
		</form>
	</div>
</div>

<!--
<div class="row">
	<div class="small-12 columns">
		<p>If you don't have an account, please <a href="../register-member-account/?a=1" title="Register for an Account">register</a> for one.</p>
	</div>
</div>
-->

<!--
<div class="row">
	<div class="small-12 columns">

		<form data-abide action="../php/reset_password_process.php" method="post" />
		  <fieldset>
		    <legend>Reset Password</legend>
		    
		    <div class="row">
		      <div class="small-12 columns">
		      	<p>Please provide your <strong>Username</strong> and <strong>Email Address</strong> then press the <strong>RESET PASSWORD</strong> button.</p>
		      </div>
		    </div>
		    		
			<div class="row">
			
			  <div class="small-12 medium-4 columns">
			  
			    <label>Username
			      <input type="text" required name="username" placeholder="MyUserName" value="<?php echo $_SESSION['reset_password_username']; ?>" />
			    </label>
			    <small class="error">Username is required</small>    
			  </div>
			  
			  <div class="small-12 medium-4 columns">
			    <label>Email Address
			      <input type="email" required name="email" placeholder="jane.doe@somedomain.com" value="<?php echo $_SESSION['reset_password_email']; ?>"/>
			    </label>
			    <small class="error">Valid Email Address is required</small>	    
			  </div>
			  
			  <div class="small-12 medium-4 columns">
			  
			    <label>Are you human? Enter Code:	      
			      <img src="../includes/captcha.php" style="margin-bottom: -8px;" /><input type="text" required name="captcha_input" value=""/>
			    </label>
			    <small class="error">Valid Code is required</small>	    
			    	    
			  </div>
		
			</div>		
			
			<div class="row">
			
			  <div class="small-12 medium-push-8 columns">
			  
			    <label>	      
			      <input class="button medium radius" type="submit" value="RESET PASSWORD">      
			    </label>
			    	    
			  </div>
		
			</div>	
							
		  </fieldset>	
		</form>
	</div>
</div>
-->