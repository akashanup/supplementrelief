<?php

include_once('../includes/header.php');

$default_limit = 'all';

if (isset($_GET['p_id'])) {
	$project_id = $_GET['p_id'];
} else {
	$project_id = public_project_id; // store/includes/defines.php
}

$_SESSION['enrollment']['project_id'] = $project_id;

if(!isset($_SESSION['enrollment']['user_price_type'])) $user_price_type = USER_PRICE_TYPE; 
else $user_price_type = $_SESSION['enrollment']['user_price_type'];

if (!empty($_GET['savings_item']))
{
	$savings_item = safe_sql_data($connection, $_GET['savings_item']);
	$savings_item_href="&savings_item=".$savings_item;	
}
else
{
	$savings_item_href='';
}

if (!empty($_GET['new_item']))
{
	$new_item = safe_sql_data($connection, $_GET['new_item']);
	$new_item_href="&new_item=".$new_item;	
}
else
{
	$new_item_href='';
}

if (!empty($_GET['featured_item']))
{
	$featured_item = safe_sql_data($connection, $_GET['featured_item']);
	$featured_item_href="&featured_item=".$featured_item;	
}
else
{
	$featured_item_href='';
}

if (!empty($_GET['program_item']))
{
	$program_item = safe_sql_data($connection, $_GET['program_item']);
	$program_item_href="&program_item=".$program_item;	
}
else
{
	$program_item_href='';
}

if (!empty($_GET['pack_item']))
{
	$pack_item = safe_sql_data($connection, $_GET['pack_item']);
	$pack_item_href="&pack_item=".$pack_item;	
}
else
{
	$pack_item_href='';
}

if (!empty($_GET['sample_item']))
{
	$sample_item = safe_sql_data($connection, $_GET['sample_item']);
	$sample_item_href="&sample_item=".$sample_item;	
}
else
{
	$sample_item_href='';
}

if (isset($_GET['education_item']))
{
	$education_item = safe_sql_data($connection, $_GET['education_item']);
	$education_item_href="&education_item=".$education_item;	
}
else
{
	$education_item_href='';
}

if (!empty($_GET['vegetable_capsule']))
{
	$vegetable_capsule = safe_sql_data($connection, $_GET['vegetable_capsule']);
	$vegetable_capsule_href="&vegetable_capsule=".$vegetable_capsule;	
}
else
{
	$vegetable_capsule_href='';
}

if (!empty($_GET['gluten_free']))
{
	$gluten_free = safe_sql_data($connection, $_GET['gluten_free']);
	$gluten_free_href="&gluten_free=".$gluten_free;	
}
else
{
	$gluten_free_href='';
}

if (!empty($_GET['vegetarian']))
{
	$vegetarian = safe_sql_data($connection, $_GET['vegetarian']);
	$vegetarian_href="&vegetarian=".$vegetarian;	
}
else
{
	$vegetarian_href='';
}

if (!empty($_GET['albion_minerals']))
{
	$albion_minerals = safe_sql_data($connection, $_GET['albion_minerals']);
	$albion_minerals_href="&albion_minerals=".$albion_minerals;	
}
else
{
	$albion_minerals_href='';
}
	
if (isset($_GET['keyword']) && !empty($_GET['keyword']))
{
	$keyword = safe_sql_data($connection, $_GET['keyword']);
	$keyword_href="&keyword=".$keyword;	
		
}
else
{
	$keyword_href='';
}
	
if (isset($_GET['orderby']) && !empty($_GET['orderby']))
{
	$orderby= safe_sql_data($connection, $_GET['orderby']);
	$orderby_href="&orderby=".$orderby;
}
else
{
	$orderby = 'name';
	$orderby_href="&orderby=".$orderby;
}
	
if (isset ($_GET['limitby']) && !empty($_GET['limitby']))
{
	if (is_numeric($_GET['limitby']))
	{
		$limit = safe_sql_data($connection, abs(intval($_GET['limitby'])));
	}
	elseif (safe_data($_GET['limitby']) == 'all')
	{
		$limit = 'all';
	}
	else
	{
		$limit = $default_limit;
	}
}
else
{	
	$limit = $default_limit;
}
	
if (isset($_GET['page']) && $_GET['page'] != 0)
{
	$page = safe_sql_data($connection, $_GET['page']);
	$start = ($page-1) * $limit;
}
else
{
	$start=0;
	$page=1;
}

if (isset($_GET['alpha']) && !empty($_GET['alpha']))
{
	$alpha= safe_sql_data($connection, $_GET['alpha']);
	$alpha_href="&alpha=".$alpha;
}
else
{
	$alpha = '';
	$alpha_href="&alpha=".$alpha;
}

if (isset($_GET['gos_id']) && !empty($_GET['gos_id']))
{
	$gos_id= safe_sql_data($connection, $_GET['gos_id']);
	$gos_id_href="&gos_id=".$gos_id;
}
else
{
	$gos_id = '';
	$gos_id_href="&gos_id=".$gos_id;
}

if (isset($_GET['ingr_id']) && !empty($_GET['ingr_id']))
{
	$ingr_id= safe_sql_data($connection, $_GET['ingr_id']);
	$ingr_id_href="&ingr_id=".$ingr_id;
}
else
{
	$ingr_id = '';
	$ingr_id_href="&ingr_id=".$ingr_id;
}


$match  = " ";

if (!empty($keyword))
{
	$kw = html_entity_decode($keyword);
	$kw_array = split(" ",$kw); 
	$kw_texts = "";
	$lk_texts = "";	
	$lk_texts_or = "";	
	for($i  = 0 ; $i < count($kw_array); $i++){
		if(strlen($kw_array[$i]) > 1){
			$kw_texts .= " +".$kw_array[$i];
			if(strlen($kw_array[$i]) >= 2 && strtolower($kw_array[$i]) != 'for'){
				$len = strlen($lk_texts);

				if(strlen($lk_texts) > 0) { 
					if(strlen($kw_array[$i]) <= 2){
						$lk_texts    .= "AND bpu.name_clean LIKE '%".$kw_array[$i]."%' \n";
						$lk_texts_or .= "OR (bpu.name_clean LIKE '%".$kw_array[$i]."%' ) \n";
					} else {
						$lk_texts    .= "AND bpu.name_clean LIKE '".substr($kw_array[$i],0,3)."%' AND bpu.name_clean LIKE '%".substr($kw_array[$i],-3)."' \n";
						$lk_texts_or .= "OR (bpu.name_clean LIKE '".substr($kw_array[$i],0,3)."%' AND bpu.name_clean LIKE '%".substr($kw_array[$i],-3)."' ) \n";
					}
				} else { 
					if(strlen($kw_array[$i]) <= 2) {
						$lk_texts .= "bpu.name_clean LIKE '%".$kw_array[$i]."%' "; 
						$lk_texts .= " OR bpu.name LIKE '%".$keyword."%' \n"; 
						$lk_texts_or = " (" .$lk_texts.") ";
					} else {
						$lk_texts .= "bpu.name_clean LIKE '%".substr($kw_array[$i],0,3)."%' AND bpu.name_clean LIKE '%".substr($kw_array[$i],-3)."%' "; 
						$lk_texts .= " OR bpu.name LIKE '%".$keyword."%' \n"; 
						$lk_texts_or = " (" .$lk_texts.") ";
					}
				}
			}
		}
	}

	$kw_texts = trim($kw_texts);
}

$query = 'SELECT 
	pbpu.id,
	pbpu.project_id,  
	pbpu.brand_product_usage_id, 
	pbpu.backordered, 
	pbpu.backordered_date, 
	pbpu.backordered_description, 
	pbpu.group_options_description, 
	pbpu.new_item, 
	pbpu.featured_item,
	pbpu.savings_item, 
	pbpu.program_item,  
	pbpu.pack_item, 
	pbpu.sample_item, 
	pbpu.education_item,   
	pbpu.page_meta_description, 
	pbpu.effective_date, 
	pbpu.end_date, 
	pbpu.modified_timestamp, 
	bpu.name, 
	bpu.vegetable_capsule, 
	bpu.gluten_free, 
	bpu.vegetarian, 
	bpu.albion_minerals,  
	bpu.image_id,   
	bpu.short_description_html, 
	bpu.sku,
	bpu.alternate_sku, 
	bpu.upc,
	bpu.alternate_upc, 
	bpu.search_terms, 
	bpu.description_html, 
	bpu.benefits_html,  
	pbpup.price, 
	im.host_url AS product_image_host_url, 
	im.host_url_rename AS product_image_host_url_rename, 
	im.alt_text AS product_image_alt_text, 
	urls.url AS seo_url  
	FROM project_brand_product_usages pbpu 
	INNER JOIN brand_product_usages bpu 
		ON (pbpu.brand_product_usage_id = bpu.id AND bpu.active = 1) 
	LEFT JOIN project_brand_product_usage_prices pbpup 
		ON (pbpu.project_id = pbpup.pbpu_project_id AND
				pbpu.brand_product_usage_id = pbpup.brand_product_usage_id AND 
				pbpup.price_type_code = "RETL" AND
				pbpup.price > 0)
	LEFT JOIN images im 
		ON (bpu.image_id = im.content_asset_id AND 
				im.format = "JPG" AND 
				im.size = "Product" AND 
				im.usage_size = "Small" /* AND 
				im.usage_shape = "Portrait" */ ) 
	LEFT JOIN url_usages urlu 
	    ON (pbpu.id = urlu.pbpu_id AND 
            urlu.effective_date <= CURRENT_DATE AND
           (urlu.end_date IS NULL or urlu.end_date >= CURRENT_DATE)) 
	LEFT JOIN urls urls ON urlu.url_id = urls.id 						    			
	WHERE pbpu.project_id = '.$project_id.' 
	AND pbpu.active = 1 ';
	
if (!empty($savings_item))
{
	$query.= "AND (pbpu.savings_item = 1) ";
}

if (!empty($new_item))
{
	$query.= "AND (pbpu.new_item = 1) ";
}

if (!empty($featured_item))
{
	$query.= "AND (pbpu.featured_item = 1) ";
}

if (!empty($program_item))
{
	$query.= "AND (pbpu.program_item = 1) ";
}

if (!empty($pack_item))
{
	$query.= "AND (pbpu.pack_item = 1) ";
}

if (!empty($sample_item))
{
	$query.= "AND (pbpu.sample_item = 1) ";
}

if (!empty($education_item))
{
	$query.= "AND (pbpu.education_item = 1) ";
}

if (!empty($vegetable_capsule))
{
	$query.= "AND (bpu.vegetable_capsule = 1) ";
}

if (!empty($gluten_free))
{
	$query.= "AND (bpu.gluten_free = 1) ";
}

if (!empty($vegetarian))
{
	$query.= "AND (bpu.vegetarian = 1) ";
}

if (!empty($albion_minerals))
{
	$query.= "AND (bpu.albion_minerals = 1) ";
}

if (!empty($alpha)) 
{
	$query.= "AND pbpu.id IN (SELECT project_brand_product_usage_id FROM project_brand_product_category_usages where project_category_usage_id = ".$alpha." AND effective_date <= CURRENT_DATE AND end_date is NULL or end_date >= CURRENT_DATE) ";
	
}

if (!empty($gos_id)) 
{
	$query.= "AND pbpu.id IN (SELECT project_brand_product_usage_id FROM project_brand_product_category_usages where project_category_usage_id = ".$gos_id." AND effective_date <= CURRENT_DATE AND end_date is NULL or end_date >= CURRENT_DATE ) ";					
	
}

if (!empty($ingr_id)) 
{
	$query.= "AND pbpu.id IN (SELECT project_brand_product_usage_id FROM project_brand_product_category_usages where project_category_usage_id = ".$ingr_id." AND effective_date <= CURRENT_DATE AND end_date is NULL or end_date >= CURRENT_DATE ) ";					
	
}
			
$fulltext_expr = "";
if (!empty($keyword))
{

	if(strlen($lk_texts) > 0){
		$lk_texts = "( ".$lk_texts." ) OR ";
	}

	if(strlen($lk_texts_or) > 0){
		$lk_texts_or = "( ".$lk_texts_or." ) OR ";
	}

	$fulltext_expr = 
	      " AND (
	           			$lk_texts
					( match(bpu.name, bpu.name_clean, bpu.short_description_html, bpu.sku, bpu.alternate_sku, bpu.upc, bpu.alternate_upc, 
			               bpu.search_terms, bpu.description_html, bpu.benefits_html)  
						against ('".$kw_texts."' in boolean mode) > 0.1 )
				    OR 
					( match(pbpu.group_options_description, pbpu.page_meta_description)
						against ('".$kw_texts."' in boolean mode) > 0.1 )
			    ) ";

	$fulltext_expr_or = 
	      " AND (
	           			$lk_texts_or
					( match(bpu.name, bpu.name_clean, bpu.short_description_html, bpu.sku, bpu.alternate_sku, bpu.upc, bpu.alternate_upc, 
			               bpu.search_terms, bpu.description_html, bpu.benefits_html)  
						against ('".$kw_texts_or."' in boolean mode) /*> 0.1   */)
				    OR 
					( match(pbpu.group_options_description, pbpu.page_meta_description)
						against ('".$kw_texts_or."' in boolean mode) /*> 0.1  */)
			    ) ";

	$query_or = $query;

    $query .= $fulltext_expr;

    $query_or .= $fulltext_expr_or;
    
}

$query.= "GROUP BY pbpu.brand_product_usage_id";

if ($orderby == 'id')
{
	$query.= " ORDER BY pbpu.id ASC";
}
elseif ($orderby == 'brand_product_name')
{
	$query.= " ORDER BY bpu.name ASC";
}
elseif ($orderby == 'new_item')
{
	$query.= " ORDER BY pbpu.new_item DESC, bpu.name ASC";
}
elseif ($orderby == 'featured_item')
{
	$query.= " ORDER BY pbpu.featured_item DESC, bpu.name ASC";
}
elseif ($orderby == 'program_item')
{
	$query.= " ORDER BY pbpu.program_item DESC, bpu.name ASC";
} 
elseif ($orderby == 'pack_item')
{
	$query.= " ORDER BY pbpu.pack_item DESC, bpu.name ASC";
} 
elseif ($orderby == 'sample_item')
{
	$query.= " ORDER BY pbpu.sample_item DESC, bpu.name ASC";
} 	
elseif ($orderby == 'price_lowest')
{
	$query.= " ORDER BY pbpup.price ASC, bpu.name ASC";
}	
elseif ($orderby == 'price_highest')
{
	$query.= " ORDER BY pbpup.price DESC, bpu.name ASC";
}	
elseif ($orderby == 'last_modified_timestamp')
{
	$query.= " ORDER BY pbpu.modified_timestamp DESC, bpu.name ASC";
}
else 
{
	$query.= " ORDER BY bpu.name ASC";
}				
						
if ($limit == 'all')
{
	$query.= '';
}
else
{
	$query.=' LIMIT ' . $start . ',' . $limit;
}

$query_totalpage = 'SELECT			
pbpu.id, 
bpu.name, 
bpu.short_description_html 
FROM project_brand_product_usages pbpu 
INNER JOIN brand_product_usages bpu 
	ON (pbpu.brand_product_usage_id = bpu.id AND bpu.active = 1) 
WHERE pbpu.project_id = '.$project_id.'    
AND pbpu.active = 1 '; 

if (!empty($savings_item))
{
	$query_totalpage.= "AND (pbpu.savings_item = 1) ";
}

if (!empty($new_item))
{
	$query_totalpage.= "AND (pbpu.new_item = 1) ";
}

if (!empty($featured_item))
{
	$query_totalpage.= "AND (pbpu.featured_item = 1) ";
}

if (!empty($program_item))
{
	$query_totalpage.= "AND (pbpu.program_item = 1) ";
}	

if (!empty($pack_item))
{
	$query_totalpage.= "AND (pbpu.pack_item = 1) ";
}	

if (!empty($sample_item))
{
	$query_totalpage.= "AND (pbpu.sample_item = 1) ";
}

if (!empty($education_item))
{
	$query_totalpage.= "AND (pbpu.education_item = 1) ";
}

if (!empty($vegetable_capsule))
{
	$query_totalpage.= "AND (bpu.vegetable_capsule = 1) ";
}

if (!empty($gluten_free))
{
	$query_totalpage.= "AND (bpu.gluten_free = 1) ";
}

if (!empty($vegetarian))
{
	$query_totalpage.= "AND (bpu.vegetarian = 1) ";
}

if (!empty($albion_minerals))
{
	$query_totalpage.= "AND (bpu.albion_minerals = 1) ";
}

if (!empty($alpha)) 
{
	$query_totalpage.= "AND pbpu.id IN (SELECT project_brand_product_usage_id FROM project_brand_product_category_usages where project_category_usage_id = ".$alpha." AND effective_date <= CURRENT_DATE AND end_date is NULL or end_date >= CURRENT_DATE) ";	
}

if (!empty($gos_id)) 
{
	$query_totalpage.= "AND pbpu.id IN (SELECT project_brand_product_usage_id FROM project_brand_product_category_usages where project_category_usage_id = ".$gos_id." AND effective_date <= CURRENT_DATE AND end_date is NULL or end_date >= CURRENT_DATE) ";
	
}

if (!empty($ingr_id)) 
{
	$query_totalpage.= "AND pbpu.id IN (SELECT project_brand_product_usage_id FROM project_brand_product_category_usages where project_category_usage_id = ".$ingr_id." AND effective_date <= CURRENT_DATE AND end_date is NULL or end_date >= CURRENT_DATE ) ";					
	
}
					
if (!empty($keyword))
{
	
	$query_totalpage_or = $query_totalpage;

    $query_totalpage    .= $fulltext_expr;
    $query_totalpage_or .= $fulltext_expr_or;
}

$result_product_total_page = mysqli_query($connection, $query_totalpage);
$total_records = mysqli_num_rows($result_product_total_page);

if($total_records == 0 AND !empty($keyword)) {
	$result_product_total_page = mysqli_query($connection, $query_totalpage_or);
	if (!$result_product_total_page) {
		show_mysqli_error_message($query_totalpage_or, $connection);
		die;				
	}	
	$total_records = mysqli_num_rows($result_product_total_page);
	$result_product = mysqli_query($connection, $query_or);

	if (!$result_product) {
		show_mysqli_error_message($query_or, $connection);
		die;				
	}	
} else {
	$result_product = mysqli_query($connection, $query);
}


if ($limit > 0)
{
	$last_page = ceil($total_records/$limit);
}
else
{
	$last_page = 0;
}

$last_prev = $last_page-1;			

if (!$result_product) {
	show_mysqli_error_message($query, $connection);
	die;				
}
if (!$result_product_total_page) {
	show_mysqli_error_message($query_totalpage, $connection);
	die;				
}								

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
	include_once('../store/includes/functions.php');
	function generateProductsHTML($connection, $result_product, $total_records, $page, $last_page, $last_prev, $limit, $alpha_href, $keyword_href, $orderby_href, $new_item_href, $featured_item_href, $program_item_href, $pack_item_href, $sample_item_href, $education_item_href, $vegetable_capsule_href, $gluten_free_href, $vegetarian_href, $albion_minerals_href, $gos_id_href, $ingr_id_href)
  	{
			$https_replace = false;
			if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") 
			{
				$https_replace = true;
			}

			$divAdded=0;
			$productHtml =	'';
			while($r = mysqli_fetch_assoc($result_product)) 
			{
					if ($r['seo_url']) 
					{
			    		$product_url = '../'.$r['seo_url'].'/';
					} 
					else 
					{	
			    		$product_url = '../product/?p_id='.$r['project_id'].'&bpu_id='.$r['brand_product_usage_id'];
					}	

					$productHtml .=			
						'<div class="row">'											.
							'<div class="small-12 medium-2 columns lazyDiv">'		.
								'<div class=" show-for-small-only">'				.
									'<center>'										.
										'<h3>'										.
											'<a href="'.$product_url.'" title="Learn more about '.$r['name'].'">'.$r['name'].'</a>'.
										'</h3>'										.
									'</center>'										.
								'</div>';

								if (empty($r['product_image_host_url'])) 
								{
									$display_product_image_url = "https://placehold.it/160x200";
								} 
								else 
								{
									$display_product_image_url = $_SESSION['application']['root_media_host_url'].$r['product_image_host_url'];
								}
					$productHtml  .=
								'<center>'																		.
									'<a href="'.$product_url.'" title="Learn more about '.$r['name'].'">'		.
										'<img class="lazy" src="../thumbnail.png" data-original="'.$display_product_image_url.'" alt="'.$r['product_image_alt_text'].'"/>'									     .
										'<noscript>'	.
										    '<img src="'.$display_product_image_url.'" alt="'.$r['product_image_alt_text'].'"/>'	.
										'</noscript>'			.
									'</a>'																		.
								'</center>'																		.
							'</div>'																			.
							'<div class="small-12 medium-7 columns">'											.
								'<div class="show-for-medium-up">'												.				
									'<h2 class="storeProductTitle">'											.
										'<a href="'.$product_url.'" title="Learn more about '.$r['name'].'">'	.
											$r['name']															.
										'</a>'																	.
									'</h2>'																		.
								'</div>';
								if ($r['savings_item'] || $r['new_item'] || $r['featured_item'] || $r['program_item'] || $r['pack_item'] || $r['sample_item'] || $r['education_item']) 
								{
									
					$productHtml 	.=
								'<div id="storeProductLabels">';
										
									if ($r['savings_item']) 
									{ 
					$productHtml 	.= '<span class="label success radius"><b>$avings</b></span>&nbsp;&nbsp;'; 
									}
										
									if ($r['new_item']) 
									{ 
					$productHtml 	.= '<span class="label info radius"><b>New</b></span>&nbsp;&nbsp;'; 
									}	
									if ($r['featured_item']) 
									{ 
					$productHtml 	.= '<span class="label info radius"><b>Popular</b></span>&nbsp;&nbsp;'; 
									}
									if ($r['program_item']) 
									{ 
					$productHtml 	.= '<span class="label info radius"><b>Program</b></span>&nbsp;&nbsp;'; 
									}
									if ($r['pack_item']) 
									{ 
					$productHtml 	.= '<span class="label info radius"><b>Pack</b></span>&nbsp;&nbsp;'; 
									}
									if ($r['sample_item']) 
									{ 
					$productHtml 	.= '<span class="label info radius"><b>Sample</b></span>&nbsp;&nbsp;'; 
									}
									if ($r['education_item']) 
									{ 
					$productHtml 	.= '<span class="label info radius"><b>Education</b></span>&nbsp;&nbsp;'; 
									}		
					$productHtml 	.= 
								'</div>';
								}
									
								if ($r['vegetable_capsule'] || $r['gluten_free'] || $r['vegetarian'] || $r['albion_minerals']) 
								{
								
				$productHtml 	.= 
								'<div id="storeProductLabels">';
									if ($r['vegetable_capsule']) 
									{ 
				$productHtml 	.= '<span class="label info radius"><b>Vegetable Capsule</b></span>&nbsp;&nbsp;'; 
									}	
									if ($r['gluten_free']) 
									{ 
				$productHtml 	.= '<span class="label info radius"><b>Gluten Free</b></span>&nbsp;&nbsp;'; 
									}
									if ($r['vegetarian']) 
									{ 
				$productHtml 	.= '<span class="label info radius"><b>Vegetarian</b></span>&nbsp;&nbsp;'; 
									}
									if ($r['albion_minerals']) 
									{ 
				$productHtml 	.= '<span class="label info radius"><b>Albion Minerals</b></span>&nbsp;&nbsp;'; 
									}
				$productHtml 	.= 
								'</div>';
									
								}
																															
				$productHtml 	.= 
								'<div id="productShortDescription">'							.
									'<h3>'.strip_tags($r['short_description_html']).'</h3>'	.
								'</div>'													.
								$r['page_meta_description'];
									
								if ($r['group_options_description']) 
								{ 
				$productHtml 	.=  $r['group_options_description']; 
								}
								if ($r['backordered']) 
								{ 
				                        
			                        $backordered_message = '<i class="fa fa-hand-o-right fa-1x"></i>&nbsp;&nbsp;';              
			                        if ($r['backordered_date']) 
			                        { 
			                            $backordered_message .= $r['backordered_description'];
			                            $backordered_message .= ' Estimated available date: '.hdate($r['backordered_date']).'.';
			                        } 
			                        else 
			                        {
			                            $backordered_message .= $r['backordered_description'];                                 
			                        }

			    $productHtml 	.= '<div data-alert class="alert-box warning radius">'	.
			                            $backordered_message							.
			                        '</div>';
			                    }
														
								if ($r['upc']) 
								{
				$productHtml 	.=	
								'<div id="productFeatureUPC">'	.
									'<b>UPC:</b> '.$r['upc'];
									if ($r['alternate_upc']) 
									{ 
				$productHtml 	.= ', '.$r['alternate_upc']; 
									}
									if ($r['sku']) 
									{ 
				$productHtml 	.= ' <b>SKU:</b> '.$r['sku']; 
									}
									if ($r['alternate_sku']) 
									{ 
				$productHtml 	.= ', '.$r['alternate_sku']; 
									}
										
				$productHtml 	.=	'<br><br>'	.
								'</div>';				
								}
				$productHtml 	.=
								'<div class="small-only-text-center">'															.
									'<a href="<?php echo $product_url; ?>" title="Learn more about '.$r['name'].'">'			.
									'<i class="fa fa-hand-o-right fa-lg"></i>&nbsp;&nbsp;more info</a>&nbsp;&nbsp'				.
									integrate_view_cart_button(array('store_path' => '../'.STORE_FOLDER_NAME.'/',
										'button_html_content' => '<i class="fa fa-shopping-cart fa-lg"></i>&nbsp;&nbsp;view cart'
										))						.
									'<br><br>'																					.
								'</div>'																						.
							'</div>'																							.
							
							'<div class="small-12 medium-3 columns">'		.
								integrate_add_to_cart_button($connection, array(
									'p_usage_id' => $r['brand_product_usage_id'],
									'pid' => $r['id'], 
									'project_id' => $r['project_id'],
									'p_name' => $r['name'],
									'form_name' => 'form_large',
									'qty_visible' => TRUE,
									'button_html_content' => '',
									'retail_price' => $r['price'],
									'price_type' => $user_price_type,
									'divAdded'	=>	$divAdded
								));
				$divAdded=1;
				$productHtml 	.=
							'</div>'	.
							'<hr>'		.
						'</div>';
			} 

			$json = [];
			$json['total_products'] = $total_records;
			$json['html'] = $productHtml;
			productsPagination($json, $page, $last_page, $last_prev, $limit, $alpha_href, $keyword_href, $orderby_href, $new_item_href, $featured_item_href, $program_item_href, $pack_item_href, $sample_item_href, $education_item_href, $vegetable_capsule_href, $gluten_free_href, $vegetarian_href, $albion_minerals_href, $gos_id_href, $ingr_id_href, $json);
  	}

  	function generateNoProductsHTML($page, $last_page, $last_prev, $limit, $alpha_href, $keyword_href, $orderby_href, $new_item_href, $featured_item_href, $program_item_href, $pack_item_href, $sample_item_href, $education_item_href, $vegetable_capsule_href, $gluten_free_href, $vegetarian_href, $albion_minerals_href, $gos_id_href, $ingr_id_href)
  	{

  		$html =
  			'<div class="row">'								.
				'<div class="small-12 columns">'			.
					'<div class="panel">';
					if ($_GET['keyword']) 
					{
						$html .=
						'<h2>No product found with Search Keyword(s)'. $_GET['keyword'] .'</h2>';
					} 
					else 
					{
						$html .=
						'<h2>No product found with Search Criterion selected</h2>';
					}
						$html .=						
						'<hr>
						<p>Please call <b>(888) 424-0032</b> or <a href="../contact/">email</a> if you need help finding anything. We do carry the <b>entire NuMedica product line</b> on this site. You can also <b>place your order over the phone</b>.</p>						
						<ol>
							<li>The blue <b>SHOW ALL BUTTON</b>, above to the right of the blue <b>SEARCH BUTTON</b>, resets the Search to display every NuMedica product in alphabetical order with <b>40 products per page</b>. <b>Scroll down to where the product occurs alphabetically</b> and/or <b>advance to the next page</b>.</li>					
							<li><b>Some NuMedica products are no longer manufactured and sold</b>. Click <a href="javascript:void(0)" id="discontinuedProductsAnchor">here</a> to see which products have been discontinued and the recommended alternative.</li>
							<li>View <a href="javascript:void(0)" id="searchHelpAnchor" rel="self" title="view Search Help"></i>Search Help</a>.</li>
						</ol>'	.
					'</div>'	.
				'</div>'		.
			'</div>';
		
		$json = [];
		$json['total_products'] = 0;
		$json['html'] = $html;
		productsPagination($json, $page, $last_page, $last_prev, $limit, $alpha_href, $keyword_href, $orderby_href, $new_item_href, $featured_item_href, $program_item_href, $pack_item_href, $sample_item_href, $education_item_href, $vegetable_capsule_href, $gluten_free_href, $vegetarian_href, $albion_minerals_href, $gos_id_href, $ingr_id_href, $json);
  	}

  	if($total_records == 0)
	{
		$noProductHtml = generateNoProductsHTML($page, $last_page, $last_prev, $limit, $alpha_href, $keyword_href, $orderby_href, $new_item_href, $featured_item_href, $program_item_href, $pack_item_href, $sample_item_href, $education_item_href, $vegetable_capsule_href, $gluten_free_href, $vegetarian_href, $albion_minerals_href, $gos_id_href, $ingr_id_href);
	}
	else
	{
		$productsHtml = generateProductsHTML($connection, $result_product, $total_records, $page, $last_page, $last_prev, $limit, $alpha_href, $keyword_href, $orderby_href, $new_item_href, $featured_item_href, $program_item_href, $pack_item_href, $sample_item_href, $education_item_href, $vegetable_capsule_href, $gluten_free_href, $vegetarian_href, $albion_minerals_href, $gos_id_href, $ingr_id_href);
	}
}