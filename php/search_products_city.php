<?php

// echo 'Begin search_products_city.php<br /><hr />';

// error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
error_reporting(E_ERROR | E_WARNING | E_PARSE);

// show_array($_SERVER);
// show_array($_GET);

// echo '<br>&nbsp;&nbsp;&nbsp;<b>Query Parameters:</b><br>&nbsp;&nbsp;&nbsp;'.$_SERVER['QUERY_STRING'].'<br><br>';

if (isset($_GET['p_id'])) {
	$project_id = $_GET['p_id'];
} else {
	$project_id = public_project_id; // store/includes/defines.php
}

// echo 'Project Id '.$project_id;

$_SESSION['enrollment']['project_id'] = $project_id;

// $_SESSION['user_price_type'] = 'RETL';
// $_SESSION['user_price_type'] = 'VOLDSC';

if($_SESSION['user_price_type'] == '') $USER_PRICE_TYPE = USER_PRICE_TYPE; // store/includes/defines.php
else $USER_PRICE_TYPE = $_SESSION['user_price_type']; // Set in Member Dashboard based upon the Enrollment the User selected.

// Get the User Search parmaters and build query paramters.

// 1. Product Type: New, Featured, Programs, Packs, Samples, Education.

if (!empty($_GET['new_item']))
{
	$new_item = safe_sql_data($connection, $_GET['new_item']);
	$new_item_href="&new_item=".$new_item;	
}
else
{
	$new_item_href='';
}

if (!empty($_GET['featured_item']))
{
	$featured_item = safe_sql_data($connection, $_GET['featured_item']);
	$featured_item_href="&featured_item=".$featured_item;	
}
else
{
	$featured_item_href='';
}

if (!empty($_GET['program_item']))
{
	$program_item = safe_sql_data($connection, $_GET['program_item']);
	$program_item_href="&program_item=".$program_item;	
}
else
{
	$program_item_href='';
}

if (!empty($_GET['pack_item']))
{
	$pack_item = safe_sql_data($connection, $_GET['pack_item']);
	$pack_item_href="&pack_item=".$pack_item;	
}
else
{
	$pack_item_href='';
}

if (!empty($_GET['sample_item']))
{
	$sample_item = safe_sql_data($connection, $_GET['sample_item']);
	$sample_item_href="&sample_item=".$sample_item;	
}
else
{
	$sample_item_href='';
}

if (isset($_GET['education_item']))
{
	$education_item = safe_sql_data($connection, $_GET['education_item']);
	$education_item_href="&education_item=".$education_item;	
}
else
{
	$education_item_href='';
}

// 2. NuMedica Product Characteristic: Vegetable Capsule, Gluten Free, Vegetarian, Albion Minerals

if (!empty($_GET['vegetable_capsule']))
{
	$vegetable_capsule = safe_sql_data($connection, $_GET['vegetable_capsule']);
	$vegetable_capsule_href="&vegetable_capsule=".$vegetable_capsule;	
}
else
{
	$vegetable_capsule_href='';
}

if (!empty($_GET['gluten_free']))
{
	$gluten_free = safe_sql_data($connection, $_GET['gluten_free']);
	$gluten_free_href="&gluten_free=".$gluten_free;	
}
else
{
	$gluten_free_href='';
}

if (!empty($_GET['vegetarian']))
{
	$vegetarian = safe_sql_data($connection, $_GET['vegetarian']);
	$vegetarian_href="&vegetarian=".$vegetarian;	
}
else
{
	$vegetarian_href='';
}

if (!empty($_GET['albion_minerals']))
{
	$albion_minerals = safe_sql_data($connection, $_GET['albion_minerals']);
	$albion_minerals_href="&albion_minerals=".$albion_minerals;	
}
else
{
	$albion_minerals_href='';
}
	
// 3. Keyword Search parameter.

if (isset($_GET['keyword']))
{
	$keyword = safe_sql_data($connection, $_GET['keyword']);
	$keyword_href="&keyword=".$keyword;	
		
}
else
{
	$keyword_href='';
}
			
// 4. Order By parameter.

if (isset($_GET['orderby']) && !empty($_GET['orderby']))
{
	$orderby= safe_sql_data($connection, $_GET['orderby']);
	$orderby_href="&orderby=".$orderby;
}
else
{
	$orderby = 'name';
	$orderby_href="&orderby=".$orderby;
}
			
// 5. Limit By parameter.

if (isset ($_GET['limitby']) && !empty($_GET['limitby']))
{
	if (is_numeric($_GET['limitby']))
	{
		$limit = safe_sql_data($connection, abs(intval($_GET['limitby'])));
	}
	elseif (safe_data($_GET['limitby']) == 'all')
	{
		$limit = 'all';
	}
	else
	{
		$limit = 20;
	}
}
else
{	
	$limit = 20;
	// $limit = 'all';
}
			
// 6. Page View parameter.

if (isset($_GET['page']) && $_GET['page'] != 0)
{
	$page = safe_sql_data($connection, $_GET['page']);
	$start = ($page-1) * $limit;
}
else
{
	$start=0;
	$page=1;
}

// 7. Alphabet Filter parameter.

if (isset($_GET['alpha']) && !empty($_GET['alpha']))
{
	$alpha= safe_sql_data($connection, $_GET['alpha']);
	$alpha_href="&alpha=".$alpha;
}
else
{
	$alpha = '';
	$alpha_href="&alpha=".$alpha;
}

// 8. Group/Organ System Category.

if (isset($_GET['gos_id']) && !empty($_GET['gos_id']))
{
	$gos_id= safe_sql_data($connection, $_GET['gos_id']);
	$gos_id_href="&gos_id=".$gos_id;
}
else
{
	$gos_id = public_geo_gos_id;
	$gos_id_href="&gos_id=".$gos_id;
}

// 9. Ingredient Category.

if (isset($_GET['ingr_id']) && !empty($_GET['ingr_id']))
{
	$ingr_id= safe_sql_data($connection, $_GET['ingr_id']);
	$ingr_id_href="&ingr_id=".$ingr_id;
}
else
{
	$ingr_id = '';
	$ingr_id_href="&ingr_id=".$ingr_id;
}


// Now that we have all of the User parameter selections we build the query using the parameters.	
$match  = " ";

if (!empty($keyword))
{
	$kw = html_entity_decode($keyword); // converts HTML entities to characters, opposite of htmlentities()
	$kw_array = split(" ",$kw); // Split string into array by regular expression

	/*
	if(count($kw_array) >= 1){
		for($i  =  count($kw_array) -1; $i-- ; $i > 0){
			if(strtolower($kw_array[$i]) == 'for' or ){
				unset($kw_array[$i]);
			}
		}		
	}*/
	
	// show_array($kw_array).'<br>';;
	// echo '# search keywords: '.count($kw_array).'<br>';;

	$kw_texts = "";
	$lk_texts = "";	
	$lk_texts_or = "";	
	for($i  = 0 ; $i < count($kw_array); $i++){
		if(strlen($kw_array[$i]) > 1){
			$kw_texts .= " +".$kw_array[$i];
			if(strlen($kw_array[$i]) >= 2 && strtolower($kw_array[$i]) != 'for'){
				$len = strlen($lk_texts);

				if(strlen($lk_texts) > 0) { // not the first value in this string $lk_text 
					if(strlen($kw_array[$i]) <= 2){
						$lk_texts    .= "AND bpu.name_clean LIKE '%".$kw_array[$i]."%' \n";
						$lk_texts_or .= "OR (bpu.name_clean LIKE '%".$kw_array[$i]."%' ) \n";
					} else {
						$lk_texts    .= "AND bpu.name_clean LIKE '".substr($kw_array[$i],0,2)."%' AND bpu.name_clean LIKE '%".substr($kw_array[$i],-2)."' \n";
						$lk_texts_or .= "OR (bpu.name_clean LIKE '".substr($kw_array[$i],0,2)."%' AND bpu.name_clean LIKE '%".substr($kw_array[$i],-2)."' ) \n";
					}
				} else { // the first value in the string $lk_text
					if(strlen($kw_array[$i]) <= 2) {
						// $lk_texts .= "bpu.name_clean LIKE '%".$kw_array[$i]."%' \n";
						$lk_texts .= "bpu.name_clean LIKE '%".$kw_array[$i]."%' "; // Jay Added 12/4/15 so B-Replete, c-750, others would be found.
						$lk_texts .= " OR bpu.name LIKE '%".$keyword."%' \n"; // Jay Added 12/4/15 so B-Replete, c-750, others would be found.
						$lk_texts_or = " (" .$lk_texts.") ";
					} else {
						// $lk_texts .= "bpu.name_clean LIKE '%".substr($kw_array[$i],0,2)."%' AND bpu.name_clean LIKE '%".substr($kw_array[$i],-2)."%' \n";
						$lk_texts .= "bpu.name_clean LIKE '%".substr($kw_array[$i],0,2)."%' AND bpu.name_clean LIKE '%".substr($kw_array[$i],-2)."%' "; // Jay Added 12/4/15 so B-Replete, c-750, others would be found.
						$lk_texts .= " OR bpu.name LIKE '%".$keyword."%' \n"; // // Jay Added 12/4/15 so B-Replete, c-750, others would be found.
                        // $lk_texts .= "bpu.name_clean LIKE '%".substr($kw_array[$i],0,3)."%' AND bpu.name_clean LIKE '%".substr($kw_array[$i],-3)."%' \n";
						$lk_texts_or = " (" .$lk_texts.") ";
					}
				}
			}
		}
	}

	$kw_texts = trim($kw_texts); // removes whitespace and other predefined characters from both sides of a string.
	
	// echo 'kw_texts: '.$kw_texts.'<br>';
	// echo 'lk_texts: '.$lk_texts.'<br>';
	// echo 'lk_texts_or: '.$lk_texts_or.'<br>';

}

// Construct the base product query without user parameters
$query = 'SELECT 
	pbpu.id,
	pbpu.project_id,  
	pbpu.brand_product_usage_id, 
	pbpu.backordered, 
	pbpu.backordered_date, 
	pbpu.backordered_description, 
	pbpu.group_options_description, 
	pbpu.new_item, 
	pbpu.featured_item,
	pbpu.program_item,  
	pbpu.pack_item, 
	pbpu.sample_item, 
	pbpu.education_item,   
	pbpu.page_meta_description, 
	pbpu.effective_date, 
	pbpu.end_date, 
	pbpu.modified_timestamp, 
	bpu.name, 
	bpu.vegetable_capsule, 
	bpu.gluten_free, 
	bpu.vegetarian, 
	bpu.albion_minerals,  
	bpu.image_id,   
	bpu.short_description_html, 
	bpu.sku,
	bpu.alternate_sku, 
	bpu.upc,
	bpu.alternate_upc, 
	bpu.search_terms, 
	bpu.description_html, 
	bpu.benefits_html,  
	pbpup.price, 
	im.host_url AS product_image_host_url, 
	im.alt_text AS product_image_alt_text 
	FROM project_brand_product_usages pbpu 
	INNER JOIN brand_product_usages bpu 
		ON (pbpu.brand_product_usage_id = bpu.id AND bpu.active = 1) 
	LEFT JOIN project_brand_product_usage_prices pbpup 
		ON (pbpu.project_id = pbpup.pbpu_project_id AND
				pbpu.brand_product_usage_id = pbpup.brand_product_usage_id AND 
				pbpup.price_type_code = "RETL" AND
				pbpup.price > 0)
	LEFT JOIN images im 
		ON (bpu.image_id = im.content_asset_id AND 
				im.format = "JPG" AND 
				im.size = "Product" AND 
				im.usage_size = "Small" /* AND 
				im.usage_shape = "Portrait" */ )     			
	WHERE pbpu.project_id = '.$project_id.' 
	AND pbpu.active = 1 ';
	
	// Extend query to allow user to list products by Alphabet letter 5, A-Z as defined in project_brand_product_category_usages.project_category_usage_id
	// User selects a letter from the Alphabet category: project_category_usage_id
	// AND pbpu.id IN (SELECT id FROM project_brand_product_category_usages where project_category_usage_id = ";.$project_category_usage_id.'") 
	
	// AND pbpu.is_primary = 1 	
	// AND pbpu.is_group = 1 '; 
										
// echo $query . '<br/><hr />';

// Now add User Search parmaters to query

// New Item
if (!empty($new_item))
{
	$query.= "AND (pbpu.new_item = 1) ";
}

// Featured Item
if (!empty($featured_item))
{
	$query.= "AND (pbpu.featured_item = 1) ";
}

// Program Item
if (!empty($program_item))
{
	$query.= "AND (pbpu.program_item = 1) ";
}

// Pack Item
if (!empty($pack_item))
{
	$query.= "AND (pbpu.pack_item = 1) ";
}

// Sample Item
if (!empty($sample_item))
{
	$query.= "AND (pbpu.sample_item = 1) ";
}

// Education Item
if (!empty($education_item))
{
	$query.= "AND (pbpu.education_item = 1) ";
}

// Vegetable Capsule
if (!empty($vegetable_capsule))
{
	$query.= "AND (bpu.vegetable_capsule = 1) ";
}

// Gluten Free
if (!empty($gluten_free))
{
	$query.= "AND (bpu.gluten_free = 1) ";
}

// Vegetarian
if (!empty($vegetarian))
{
	$query.= "AND (bpu.vegetarian = 1) ";
}

// Albion Minerals
if (!empty($albion_minerals))
{
	$query.= "AND (bpu.albion_minerals = 1) ";
}

// Alphabet Filter
if (!empty($alpha)) 
{
	$query.= "AND pbpu.id IN (SELECT project_brand_product_usage_id FROM project_brand_product_category_usages where project_category_usage_id = ".$alpha." AND effective_date <= CURRENT_DATE AND end_date is NULL or end_date >= CURRENT_DATE) ";
	
}

// Group/Organ System Category Filter
if (!empty($gos_id)) 
{
	$query.= "AND pbpu.id IN (SELECT project_brand_product_usage_id FROM project_brand_product_category_usages where project_category_usage_id = ".$gos_id." AND effective_date <= CURRENT_DATE AND end_date is NULL or end_date >= CURRENT_DATE ) ";					
	
}

// Ingredient Category Filter
if (!empty($ingr_id)) 
{
	$query.= "AND pbpu.id IN (SELECT project_brand_product_usage_id FROM project_brand_product_category_usages where project_category_usage_id = ".$ingr_id." AND effective_date <= CURRENT_DATE AND end_date is NULL or end_date >= CURRENT_DATE ) ";					
	
}
			
// Keyword
$fulltext_expr = "";
if (!empty($keyword))
{
	
	/*
	$query.= "AND (bpu.name LIKE '%".$keyword."%' OR bpu.short_description_html LIKE '%".
		$keyword."%' OR bpu.sku LIKE '%".$keyword."%' OR bpu.alternate_sku LIKE '%".
		$keyword."%' OR bpu.upc LIKE '%".$keyword."%' OR bpu.alternate_upc LIKE '%".
		$keyword."%' OR bpu.search_terms LIKE '%".$keyword."%' OR bpu.description_html LIKE '%".
		$keyword."%' OR bpu.benefits_html LIKE '%".
		$keyword."%' OR pbpu.group_options_description LIKE '%".
		$keyword."%' OR pbpu.page_meta_description LIKE '%".$keyword."%') ";
	*/

	if(strlen($lk_texts) > 0){
		$lk_texts = "( ".$lk_texts." ) OR ";
	}

	if(strlen($lk_texts_or) > 0){
		$lk_texts_or = "( ".$lk_texts_or." ) OR ";
	}

	$fulltext_expr = 
	      " AND (
	           			$lk_texts
					( match(bpu.name, bpu.name_clean, bpu.short_description_html, bpu.sku, bpu.alternate_sku, bpu.upc, bpu.alternate_upc, 
			               bpu.search_terms, bpu.description_html, bpu.benefits_html)  
						against ('".$kw_texts."' in boolean mode) > 0.1 )
				    OR 
					( match(pbpu.group_options_description, pbpu.page_meta_description)
						against ('".$kw_texts."' in boolean mode) > 0.1 )
			    ) ";

	$fulltext_expr_or = 
	      " AND (
	           			$lk_texts_or
					( match(bpu.name, bpu.name_clean, bpu.short_description_html, bpu.sku, bpu.alternate_sku, bpu.upc, bpu.alternate_upc, 
			               bpu.search_terms, bpu.description_html, bpu.benefits_html)  
						against ('".$kw_texts_or."' in boolean mode) /*> 0.1   */)
				    OR 
					( match(pbpu.group_options_description, pbpu.page_meta_description)
						against ('".$kw_texts_or."' in boolean mode) /*> 0.1  */)
			    ) ";

	$query_or = $query;

    $query .= $fulltext_expr;

    $query_or .= $fulltext_expr_or;
    
    // echo '<br>fulltext_expr: '.$fulltext_expr.'<hr>'; 
    // echo '<br>fulltext_expr_or: '.$fulltext_expr_or.'<hr>';
    
}

// Group By due to multiple price records returned for each product but we only want to display the product one time to the user.
$query.= "GROUP BY pbpu.brand_product_usage_id";

// Order By
if ($orderby == 'id')
{
	$query.= " ORDER BY pbpu.id ASC";
}
elseif ($orderby == 'brand_product_name')
{
	$query.= " ORDER BY bpu.name ASC";
}
elseif ($orderby == 'new_item')
{
	$query.= " ORDER BY pbpu.new_item DESC, bpu.name ASC";
}
elseif ($orderby == 'featured_item')
{
	$query.= " ORDER BY pbpu.featured_item DESC, bpu.name ASC";
}
elseif ($orderby == 'program_item')
{
	$query.= " ORDER BY pbpu.program_item DESC, bpu.name ASC";
} 
elseif ($orderby == 'pack_item')
{
	$query.= " ORDER BY pbpu.pack_item DESC, bpu.name ASC";
} 
elseif ($orderby == 'sample_item')
{
	$query.= " ORDER BY pbpu.sample_item DESC, bpu.name ASC";
} 	
elseif ($orderby == 'price_lowest')
{
	$query.= " ORDER BY pbpup.price ASC, bpu.name ASC";
}	
elseif ($orderby == 'price_highest')
{
	$query.= " ORDER BY pbpup.price DESC, bpu.name ASC";
}	
elseif ($orderby == 'last_modified_timestamp')
{
	$query.= " ORDER BY pbpu.modified_timestamp DESC, bpu.name ASC";
}
/*elseif ($orderby == 'best_match' && !empty($keyword))
{
	$query.= " ORDER BY match1 desc";
}*/
else 
{
	$query.= " ORDER BY bpu.name ASC";
}				
						
// limit
if ($limit == 'all')
{
	$query.= '';
}
else
{
	$query.=' LIMIT ' . $start . ',' . $limit;
}

// echo $query . '<br /><hr />';

/*
if ($_SESSION['user']['user_id'] == 2) {
    // Jay is logged in and is testing something on Production    
    echo '<pre>'.$query.'</pre><br>';
}
*/

// error_log($query);

// Calculate the total number of records of the result set									
$query_totalpage = 'SELECT			
pbpu.id, 
bpu.name, 
bpu.short_description_html 
FROM project_brand_product_usages pbpu 
INNER JOIN brand_product_usages bpu 
	ON (pbpu.brand_product_usage_id = bpu.id AND bpu.active = 1) 
WHERE pbpu.project_id = '.$project_id.'    
AND pbpu.active = 1 '; 

// New Item
if (!empty($new_item))
{
	$query_totalpage.= "AND (pbpu.new_item = 1) ";
}

// Featured Item
if (!empty($featured_item))
{
	$query_totalpage.= "AND (pbpu.featured_item = 1) ";
}

// Program Item
if (!empty($program_item))
{
	$query_totalpage.= "AND (pbpu.program_item = 1) ";
}	

// Pack Item
if (!empty($pack_item))
{
	$query_totalpage.= "AND (pbpu.pack_item = 1) ";
}	

// Sample Item
if (!empty($sample_item))
{
	$query_totalpage.= "AND (pbpu.sample_item = 1) ";
}

// Education Item
if (!empty($education_item))
{
	$query_totalpage.= "AND (pbpu.education_item = 1) ";
}

// Vegetarian
if (!empty($vegetable_capsule))
{
	$query_totalpage.= "AND (bpu.vegetable_capsule = 1) ";
}

// Gluten Free
if (!empty($gluten_free))
{
	$query_totalpage.= "AND (bpu.gluten_free = 1) ";
}

// Vegetarian
if (!empty($vegetarian))
{
	$query_totalpage.= "AND (bpu.vegetarian = 1) ";
}

// Albion Minerals
if (!empty($albion_minerals))
{
	$query_totalpage.= "AND (bpu.albion_minerals = 1) ";
}

// Alphabet Filter
if (!empty($alpha)) 
{
	$query_totalpage.= "AND pbpu.id IN (SELECT project_brand_product_usage_id FROM project_brand_product_category_usages where project_category_usage_id = ".$alpha." AND effective_date <= CURRENT_DATE AND end_date is NULL or end_date >= CURRENT_DATE) ";	
}

// Group/Organ System Category Filter
if (!empty($gos_id)) 
{
	$query_totalpage.= "AND pbpu.id IN (SELECT project_brand_product_usage_id FROM project_brand_product_category_usages where project_category_usage_id = ".$gos_id." AND effective_date <= CURRENT_DATE AND end_date is NULL or end_date >= CURRENT_DATE) ";
	
}

// Ingredient Category Filter
if (!empty($ingr_id)) 
{
	$query_totalpage.= "AND pbpu.id IN (SELECT project_brand_product_usage_id FROM project_brand_product_category_usages where project_category_usage_id = ".$ingr_id." AND effective_date <= CURRENT_DATE AND end_date is NULL or end_date >= CURRENT_DATE ) ";					
	
}
					
// Keyword 			
if (!empty($keyword))
{
	//$query_totalpage.= "AND (bpu.name LIKE '%".$keyword."%' OR bpu.short_description_html LIKE '%".$keyword."%' OR bpu.sku LIKE '%".$keyword."%' OR bpu.alternate_sku LIKE '%".$keyword."%' OR bpu.upc LIKE '%".$keyword."%' OR bpu.alternate_upc LIKE '%".$keyword."%' OR bpu.search_terms LIKE '%".$keyword."%' OR bpu.description_html LIKE '%".$keyword."%' OR bpu.benefits_html LIKE '%".$keyword."%' OR pbpu.group_options_description LIKE '%".$keyword."%' OR pbpu.page_meta_description LIKE '%".$keyword."%') ";

	$query_totalpage_or = $query_totalpage;

    $query_totalpage    .= $fulltext_expr;
    $query_totalpage_or .= $fulltext_expr_or;

    /*" AND (
						match(bpu.short_description_html, bpu.sku, bpu.alternate_sku, bpu.upc, bpu.alternate_upc, 
			               bpu.search_terms, bpu.description_html, bpu.benefits_html)  
						against ('".$keyword."')
				    OR 
						match (pbpu.group_options_description ,pbpu.page_meta_description)
						against ('".$keyword."')
			    ) ";*/

}

// echo $query_totalpage . '<br /><hr />';

$result_product_total_page = mysqli_query($connection, $query_totalpage);
$total_records = mysqli_num_rows($result_product_total_page);

// echo mysqli_error($connection);
// exit;


if($total_records == 0 AND !empty($keyword)) {
	// try an alternate version of the query
	$result_product_total_page = mysqli_query($connection, $query_totalpage_or);
	if (!$result_product_total_page) {
		show_mysqli_error_message($query_totalpage_or, $connection);
		die;				
	}	
	$total_records = mysqli_num_rows($result_product_total_page);
	$result_product = mysqli_query($connection, $query_or);	// should this be query_totalpage_or?

	if (!$result_product) {
		show_mysqli_error_message($query_or, $connection);
		die;				
	}	
	// echo "<pre>"; print_r($query_or); echo "</pre>";
} else {
	// Execute the query, optimize use of server by not executing query if no records will be found
	$result_product = mysqli_query($connection, $query);
}


if ($limit > 0)
{
	$last_page = ceil($total_records/$limit);
}
else
{
	$last_page = 0;
}

$last_prev = $last_page-1;			

//echo "<h1>Aqui</h1>";
//exit;

if (!$result_product) {
	show_mysqli_error_message($query, $connection);
	die;				
}
if (!$result_product_total_page) {
	show_mysqli_error_message($query_totalpage, $connection);
	die;				
}
								