<?php 
// echo 'Begin search_products_form.php'; ?>
				
<form name="search-product-form" id="search-product-form" data-id="">

	<input type="hidden" name="alpha" id="hiddenAlpha" value="" />
	<!-- <input type="hidden" name="gos_id" value="<?php echo ($gos_id ? $gos_id : ''); ?>" /> -->
	<!-- <input type="hidden" name="ingr_id" value="<?php echo ($ingr_id ? $ingr_id : ''); ?>" /> -->
		
	<?php
	
	if (empty($keyword) && empty($savings_item) && empty($new_item) && empty($featured_item) && empty($program_item) && empty($pack_item) && empty($sample_item) && empty($education_item) && empty($vegetable_capsule) && empty($gluten_free) && empty($vegetarian) && empty($albion_minerals) && empty($alpha) && empty($gos_id) && empty($ingr_id) ) {
		?>
		<!-- contains second row of User Search parameters -->				
		<!-- Keyword Search, View and Order By -->
		<div class="row">
			<div class="small-12 medium-6 columns">
								
				<div class="row collapse">
				
				    <div class="small-12 medium-6 columns">
				      <!-- <span class="label"><font size='3'>Search</font></span> -->
				      <input autofocus type="text" placeholder="Search by keyword, upc or sku" class="predictive_search" name="keyword" id="search" data-function="products" value="<?php if (isset($keyword)) { echo $keyword; } else { echo ''; } ?>" autocomplete="off"> 
				    </div>
				    
				    <div class="small-12 medium-6 columns">
				      <!-- <span class="postfix"><input id="button" type="submit" value="GO"> -->
				      <input class="button tiny radius" id="productSearchButton" type="button" value="SEARCH" />
				      <a class="button tiny radius" id="showAllBtn"> <font size="1"><b>SHOW ALL</b></font></a>
				    </div>
				  
				 </div>					
			</div>	<!-- END KeyWord Search GO option -->						
		<?php
				
	} 	else {
		
		// User has entered search criterion so add SHOW ALL option to reset Search	
		?>							
		<!-- display Search by Keyword, Go button and Show All button (uses 6 of 12 columns)(6 + 0 = 6) -->
		<div class="row">
			<div class="small-12 medium-6 columns">
			
				<div class="row collapse">
								
					<div class="small-12 medium-6 columns">
					  <input type="text" placeholder="Search by keyword, upc or sku" class="predictive_search" name="keyword" id="search" data-function="products" value="<?php if (isset($keyword)) { echo $keyword; } else { echo ''; } ?>" autocomplete="off">
					</div>
					
					<!--
					<div class="small-6 columns">
					  <input class="button tiny radius" id="button" type="submit" value="SEARCH" />&nbsp;<a class="button tiny radius" href="<?php echo "?keyword="."&alpha="."&limitby=".$limit.$orderby_href; ?>" ><font size="2">SHOW ALL</font></a> 
					</div>
					-->
					
					<div class="small-12 medium-6 columns">
					  <input class="button tiny radius" id="productSearchButton" type="button" value="SEARCH" />
					  <a class="button tiny radius" id="showAllBtn" data-form='products'><font size="1"><b>SHOW ALL</b></font></a> 
					</div>
					
				</div>		
			</div> <!-- END KeyWord Search GO/Show All option -->
																
		<?php 
		} // END If to determine which Keyword Search option to show - now continue with Order By and View on same row
	
		?>
		
		<!-- display View # Records select list (uses 2 of 12 columns)(6 + 2 = 8) -->
		<div class="small-12 medium-2 columns show-for-medium-up">
		
			<div class="row collapse">
			
				<div class="small-3 medium-5 columns">
				  <span class="postfix">View</span>
				</div>
			
			  <div class="small-9 medium-7 columns">
					<select id="limitby" name='limitby' data-function="products">
					  <option <?php if (isset($limit) && $limit == 10): ?>selected="selected"<?php endif ?> value="10">10</option>
					  <option <?php if (isset($limit) && $limit == 20): ?>selected="selected"<?php endif ?> value="20">20</option>
					  <option <?php if (isset($limit) && $limit == 40): ?>selected="selected"<?php endif ?> value="40">40</option>
					  <option <?php if (isset($limit) && $limit == 60): ?>selected="selected"<?php endif ?> value="60">60</option>
					  <option <?php if (isset($limit) && $limit == 100): ?>selected="selected"<?php endif ?>  value="100">100</option>
					  <option <?php if (isset($limit) && $limit == 'all'): ?>selected="selected"<?php endif ?>  value="all">ALL</option>
					</select>
			  </div>
			  					  
			</div>												
		</div> <!-- END display View # Records select list -->
			
		<!-- display Order By select list (uses 4 of 12 columns)(8 + 4 = 12) -->
		<div class="small-12 medium-4 columns show-for-medium-up">
							
			<div class="row collapse">
			
				<div class="small-5 columns">
				  <span class="postfix">Order By</span>
				</div>
			
			    <div class="small-7 columns">
			      <select id="orderby" name='orderby' data-function="products">
				    <option <?php if (isset($orderby) && $orderby == 'brand_product_name'): ?>selected="selected"<?php endif ?> value="brand_product_name">Name</option>
				    <option <?php if (isset($orderby) && $orderby == 'new_item'): ?>selected="selected"<?php endif ?> value="new_item">New</option>
				    <option <?php if (isset($orderby) && $orderby == 'featured_item'): ?>selected="selected"<?php endif ?> value="featured_item">Featured</option>
				    <option <?php if (isset($orderby) && $orderby == 'program_item'): ?>selected="selected"<?php endif ?> value="program_item">Programs</option>
				    <option <?php if (isset($orderby) && $orderby == 'pack_item'): ?>selected="selected"<?php endif ?> value="pack_item">Packs</option>
				    <option <?php if (isset($orderby) && $orderby == 'sample_item'): ?>selected="selected"<?php endif ?> value="sample_item">Samples</option>
			        <option <?php if (isset($orderby) && $orderby == 'price_lowest'): ?>selected="selected"<?php endif ?> value="price_lowest">Price (Lowest)</option>
			        <option <?php if (isset($orderby) && $orderby == 'price_highest'): ?>selected="selected"<?php endif ?> value="price_highest">Price (Highest)</option>        
					<!-- <option <?php if (isset($orderby) && $orderby == 'best_match'): ?>selected="selected"<?php endif ?> value="best_match">Best Match</option> -->     
					<!-- <option <?php if (isset($orderby) && $orderby == 'last_modified_timestamp'): ?>selected="selected"<?php endif ?> value="last_modified_timestamp">Recently Updated</option> -->                                  
			      </select>
			    </div>						  
	
			</div>
													
		</div> <!-- END display of Order By select list -->
						
	</div> <!-- END row for Keyword Search, View and Order By -->
			
	<!-- display View by Alphabet -->
	<div class="row show-for-medium-up">		
		<div class="small-12 columns text-center">
					
			<dl class="sub-nav">
			  	<dt>View by Alphabet:</dt>
			  	<dd <?php if (isset($alpha) && $alpha == ''): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="">
			  			All
			  		</a>
			  	</dd>
			  	<dd <?php if (isset($alpha) && $alpha == '4'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="4">
			  			0-9
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '5'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="5">
			  			A
		  			</a>
	  			</dd>
			  	<dd <?php if (isset($alpha) && $alpha == '6'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="6">
			  			B
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '7'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="7">
			  			C
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '8'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="8">
			  			D
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '9'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="9">
			  			E
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '10'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="10">
			  			F
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '11'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="11">
			  			G
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '12'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="12">
			  			H
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '13'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="13">
			  			I
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '14'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="14">
			  			J
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '15'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="15">
			  			K
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '16'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="16">
			  			L
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '17'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="17">
			  			M
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '18'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="18">
			  			N
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '19'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="19">
			  			O
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '20'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="20">
			  			P
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '21'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="21">
			  			Q
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '22'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="22">
			  			R
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '23'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="23">
			  			S
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '24'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="24">
			  			T
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '25'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="25">
			  			U
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '26'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="26">
			  			V
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '27'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="27">
			  			W
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '28'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="28">
			  			X
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '29'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="29">
			  			Y
		  			</a>
	  			</dd>
	  			<dd <?php if (isset($alpha) && $alpha == '30'): ?>class="active"<?php endif ?>>
			  		<a class="alphabetSearch" data-page="<?php echo $page; ?>" data-alpha="30">
			  			Z
		  			</a>
	  			</dd>
			</dl>
					
		</div>
	</div>
	
    <!-- display Category select list and Ingredient select list -->
	<div class="row">
		
		<!-- View by Category -->		
		<div class="small-12 medium-6 columns">
			
			<div class="row collapse">
			
				<div class="small-3 columns">
				  <span class="postfix">View By</span>
				</div>
			
			    <div class="small-9 columns">
	
					<?php
					
					// Build the Select List for Project Brand Product Categories (Group/Organ System)
										
					$categories_gos = '
					SELECT  
					pcu.id, 
					pcu.effective_date, 
					pcu.end_date, 
					ca.name AS gos_category_name  
					FROM project_category_usages pcu 		
					JOIN category_usages cu on pcu.category_usage_id = cu.id  
					JOIN categories ca ON ca.code = cu.child_code 
					WHERE pcu.project_id = "'.$project_id.'" 
					AND pcu.effective_date <= CURRENT_DATE  
					AND (pcu.end_date is NULL or pcu.end_date >= CURRENT_DATE)  					
					AND cu.parent_code = "GOS" 
					AND cu.effective_date <= CURRENT_DATE  
					AND (cu.end_date is NULL or cu.end_date >= CURRENT_DATE) 
					ORDER BY pcu.seq ASC, cu.seq ASC';
					
					// echo $categories_gos;
					// die();
					
					$categories_gos_result = mysql_query($categories_gos);
					
					if (!$categories_gos_result) {
						show_mysqli_error_message($categories_gos, $connection);
						die;
					}
												
					$categories_gos_options = '<option value="">Select Category ...</option>';
					
					while($categories_gos_row = mysql_fetch_array($categories_gos_result)) {
						// show_array($categories_gos_row);
						$selected = '';
						if(($gos_id ? $gos_id : $row1['p']) == $categories_gos_row['id']) $selected = ' selected';
						$categories_gos_options .= '<option value="'.$categories_gos_row['id'].'" '.$selected.'>'.$categories_gos_row['gos_category_name'].'</option>';
					}
					
					mysql_free_result($categories_gos_result);
					
					?>
					
			    	<select id="gos_id" name="gos_id">
			          <?php echo $categories_gos_options; ?>
			    	</select>
					
				</div>
			</div>
			
		</div>
		
		<!-- View by Ingredient -->
					
		<div class="small-12 medium-6 columns">
	
			<div class="row collapse">
	
				<div class="small-3 columns">
				  <span class="postfix">View By</span>
				</div>
			
			    <div class="small-9 columns">
		
					<?php
					
					// Build the Select List for Project Brand Product Categories (Ingredient)
					
					$categories_ingredient = '
					SELECT 
					pcu.id, 
					ca.name AS ingredient_name 
					FROM category_usages cu 
					JOIN category_usages cu1 ON cu1.parent_code = cu.child_code 
					JOIN project_category_usages pcu ON pcu.category_usage_id = cu1.id 
					JOIN categories ca ON ca.code = cu1.child_code 
					WHERE cu.parent_code = "INGR" 
					AND pcu.project_id = 1
					AND pcu.effective_date <= CURRENT_DATE 
					AND (pcu.end_date is NULL or pcu.end_date >= CURRENT_DATE)
					AND cu1.effective_date <= CURRENT_DATE 
					AND (cu1.end_date is NULL or cu1.end_date >= CURRENT_DATE)
					ORDER BY cu1.parent_code ASC, ca.name ASC'; 
	  				
					// echo $categories_ingredient;
					// die();
					
					$categories_ingredient_result = mysql_query($categories_ingredient);
					
					if (!$categories_ingredient_result) {
						show_mysqli_error_message($categories_ingredient, $connection);
						die;
					}
												
					$categories_ingredient_options = '<option value="">Select Ingredient ...</option>';
					
					while($categories_ingredient_row = mysql_fetch_array($categories_ingredient_result)) { 
						// show_array($categories_ingredient_row);
						$selected = '';
						if(($ingr_id ? $ingr_id : $row1['p']) == $categories_ingredient_row['id']) $selected = ' selected';
						$categories_ingredient_options .= '<option value="'.$categories_ingredient_row['id'].'" '.$selected.'>'.$categories_ingredient_row['ingredient_name'].'</option>';
					}
					
					mysql_free_result($categories_ingredient_result);
					
					?>
					
			    	<select id="ingr_id" name="ingr_id">
			          <?php echo $categories_ingredient_options; ?>
			    	</select>
					
				</div>
			</div>
		
		</div>
	
	</div>

	<!-- Advanced Search Options -->
	<?php
	// Toggles the display of these Advanced Search options based upon whether they have been previously used.

	if (empty($savings_item) && empty($new_item) && empty($featured_item) && empty($program_item) && empty($pack_item) && empty($sample_item) && empty($education_item) && empty($vegetable_capsule) && empty($gluten_free) && empty($vegetarian) && empty($albion_minerals) && empty($gos_id) && empty($ingr_id)) {
		?>
		<div id="advancedSearchForm" class="row hide"> <!-- class="row hide" turns it off by default until user selects it -->
		<?php
	} else {
		?>
		<div id="advancedSearchForm" class="row">
		<?php
	}

	?>
	
		<!-- <div id="advancedSearchForm" class="row show-for-medium-up"> -->
	
		<fieldset>
			<legend>Advanced Search Options</legend>
			
			<!-- Checkbox options -->
			<div class="small-12 medium-6 columns">
				<input type="checkbox" name="savings_item" value="1" <?php if (isset($savings_item) && $savings_item == "1") { echo 'checked'; } ?>> Savings&nbsp;&nbsp;
				<input type="checkbox" name="new_item" value="1" <?php if (isset($new_item) && $new_item == "1") { echo 'checked'; } ?>> New&nbsp;&nbsp;
				<input type="checkbox" name="featured_item" value="1" <?php if (isset($featured_item) && $featured_item == "1") { echo 'checked'; } ?>> Popluar&nbsp;&nbsp;
				<input type="checkbox" name="program_item" value="1" <?php if (isset($program_item) && $program_item == "1") { echo 'checked'; } ?>> Programs&nbsp;&nbsp;
				<input type="checkbox" name="pack_item" value="1" <?php if (isset($pack_item) && $pack_item == "1") { echo 'checked'; } ?>> Packs&nbsp;&nbsp;		
				<input type="checkbox" name="sample_item" value="1" <?php if (isset($sample_item) &&  $sample_item == "1") { echo 'checked'; } ?>> Samples&nbsp;&nbsp;	    		
				<!-- <input type="checkbox" name="education_item" value="1" <?php if (isset($education_item) &&  $education_item == "1") { echo 'checked'; } ?>> Education -->		    
			</div>
			
			<div class="small-12 medium-6 columns ">
				<input type="checkbox" name="vegetable_capsule" value="1" <?php if (isset($vegetable_capsule) && $vegetable_capsule == "1") { echo 'checked'; } ?>> Vegetable Capsule&nbsp;&nbsp;
				<input type="checkbox" name="gluten_free" value="1" <?php if (isset($gluten_free) && $gluten_free == "1") { echo 'checked'; } ?>> Gluten Free&nbsp;&nbsp;
				<input type="checkbox" name="vegetarian" value="1" <?php if (isset($vegetarian) && $vegetarian == "1") { echo 'checked'; } ?>> Vegetarian&nbsp;&nbsp;
				<input type="checkbox" name="albion_minerals" value="1" <?php if (isset($albion_minerals) && $albion_minerals == "1") { echo 'checked'; } ?>> Albion Minerals&nbsp;&nbsp;	
			</div>

		</fieldset>
		
	</div>
																									
</form>				