
<!-- contains a row for # Search Results & Page Navigation -->						
<div class="row"> <!-- display Page Navigation row -->

	<!-- Advanced Search Switch -->
	<!--
	<div class="small-12 small-only-text-center medium-1 columns">
		
		<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip tip-left radius" title="Advanced Search Options">
			<div class="switch">
				<input id="advancedSearchSwitch" type="checkbox">
				<label for="advancedSearchSwitch"></label>
			</div>
		</span> 	
		
	</div>
	-->
	
	<!-- Advanced Search Switch -->
	<!-- <div class="small-12 small-only-text-center medium-4 columns"> -->
	<div class="small-12 small-only-text-center medium-4 columns">
    	
        <!-- <a href="../why-choose-numedica" title="Why should I choose NuMedica supplements?"><i class="fa fa-info-circle fa-1x"></i>&nbsp;Why NuMedica</a>&nbsp; -->
		<a id="toggleAdvancedSearch" title="view Advanced Search Options"><i class="fa fa-search fa-1x"></i>&nbsp;Advanced Search Options</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="javascript:void(0)" data-reveal-id="searchHelp" rel="self" title="view Search Help"><i class="fa fa-question fa-lx"></i>&nbsp;Help</a>
		
		<!--
		<div class="show-for-small-only">
			<br>
		</div>
		-->
				
	</div>
	
	<!-- # Products Found -->
	<div class="small-12 medium-3 columns colWrapMargin">
		<!-- <div>&nbsp;<i class="fa fa-binoculars"></i>&nbsp;&nbsp;<?php echo $total_records; ?></div> -->
		<div id="productsFound"><center>Products Found:&nbsp;&nbsp;<?php echo $total_records; ?></center></div>			
	</div>
	
	<!-- Page Navigation -->	
	<div class="small-12 medium-5 columns">
		
		<div class="row collapse">
			
			<!--
			<div class="small-3 medium 2 columns">
				<div class="right">Pages:</div>			
			</div>
			-->
			
			<div class="small-12 medium 12 columns">
			
				<div id="pagination-setup" class="pagination-centered">
				<?php
					$url = 
						productsPagination($page, $last_page, $last_prev, $limit, $alpha_href, $keyword_href, $orderby_href, $new_item_href, $featured_item_href, $program_item_href, $pack_item_href, $sample_item_href, $education_item_href, $vegetable_capsule_href, $gluten_free_href, $vegetarian_href, $albion_minerals_href, $gos_id_href, $ingr_id_href, $json);
					echo $url;

				?>
				</div> <!-- END <div id="pagination-centered"> -->
				
			</div>
			
		</div>
		
	</div> <!-- END display Page Navigation columns -->
			
	<hr>

</div> <!-- END contains a row for User Page Navigation -->

<div id="searchHelp" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	
	<div class="panel">
		<h2>Search Help</h2>
		<p>Please call <b><?php echo $_SESSION['application']['phone']; ?></b> or <a href="../contact/" title="Contact SupplementRelief.com Customer Service">email</a> if you have questions or need assistance finding a product. You can also place your order <b>over the phone</b>.</p>	
		<p><b>NuMedica periodically revises their product catalogue</b>. Some products are <b>no longer manufactured and sold</b> and others are <b>improved or replaced</b>. Click <a href="#" data-reveal-id="discontinuedProducts">here</a> to see which products have been discontinued and the recommended alternative.</p>
	</div>
	
	<ol>
		<li>Select <?php echo integrate_view_cart_button(array('store_path' => '../'.STORE_FOLDER_NAME.'/', 'button_html_content' => '<i class="fa fa-shopping-cart icon-blue"></i>&nbsp;Cart')); ?>&nbsp;to see the items and quantities currently in your shopping cart.</li>
		<li>Select <a href="javascript:void(0)" data-reveal-id="shippingHandling" rel="self" title="View Order Processing, Shipping & Handling, Return & Refund Policy"><i class="fa fa-truck  icon-blue"></i>&nbsp;Shipping</a> to see the Shipping & Handling policy.</li>
		<li>Select <a href="<?php echo $_SESSION['file_directory_path_prefix']; ?>register-member-account/" title="Learn about Membership Benefits"><i class="fa fa-user icon-blue"></i>&nbsp;Member Benefits</a> to see the benefits of our FREE membership.</li>
		<li>We accept <b>Visa, MasterCard, Discover and American Express</b> forms of credit card payment.</li>
		<li>Enter <b>Search Keyword(s)</b> to help find a specific product. You may also enter the UPC or SKU if known.</li>
		<li>Select the <b>Search Button</b> after entering your search keywords to execute your search. Search executes automatically when changing the value in any of the other search-related fields.</li>
		<li>Select <b>View</b> to limit the number of products shown per page. The default is 40 products per page.</li>
		<li>Select <b>Order By</b> to sort the products in the results list. The default sort order is by Product Name.</li>
		<li>Select <b>View By Alphabet</b> to see the items grouped by the beginning letter of the Product Name and sorted alphabetically.</li>
		<li>Select <b>View By Category</b> to see the items grouped by Health-related categories.</li>
		<li>Select <b>View By Ingredient</b> to see the items grouped by ingredient used in a product.</li>
		<li><b>Advanced Search Options</b> displays a group of additional options for grouping products.</li>
		<li>Select <b>New, Popular, Programs, Packs and Samples</b> to see the items grouped by these respective categories. You may select more than one at a time.</li>
		<li>Select <b>Vegetable Capsule, Gluten Free, Vegetarian or Albion Minerals</b> to see the items grouped by these respective categories. You may select more than one at a time.</li>
		<li><b>Products Found</b> shows the number of products found that match the current search criterion.</li>
		<li>Select a <b>Page Number</b> to see the items grouped by page. This is related to #7 where you can specify the number of products to show per page.</li>
	</ol>
								
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
  
</div>
<?php
	include_once '../php/numedica_discontinued_products_modal.php';
?>