<?php

// echo 'Begin select_program_enrollment.php<br>';

/* 
User has successfully logged in (login_process.php login()) and SESSION variables have been set:
$_SESSION['user']['login_timestamp'] = date('m/d/y h:i A T');				
$_SESSION['user']['user_id'] = $row['id'];
$_SESSION['user']['username'] = $row['username'];
$_SESSION['user']['full_name'] = $row['first_name'].' '.$row['last_name'];
$_SESSION['user']['email'] = $row['email'];
$_SESSION['user']['phone'] = $row['phone'];
$_SESSION['user']['access_level'] = $row['user_level'];
*/

// show_array($_SESSION);

$member_account = 21; // SupplementRelief Member Account
// Make this a defines variable in /includes/defines.php relevant for each application url

$queryEnrollments = 'SELECT 
	en.id AS enrollment_id, 
	en.effective_timestamp AS enrollment_timestamp, 
	en.active,  
	en.forum_moderator, 
	ouf.name AS organization_unit_facility_name, 
	pp.id AS project_program_id, 
	pp.name AS project_program_name, 
	pp.effective_date AS project_program_effective_date, 
	pp.end_date AS project_program_end_date, 
	rt.name AS role_type_name, 
	pj.name AS project_name 
	FROM person_roles pr 
	LEFT JOIN enrollments en ON pr.id = en.person_role_id  
	LEFT JOIN organization_unit_facilities ouf ON pr.organization_unit_facility_id = ouf.id 
	LEFT JOIN project_programs pp ON en.project_program_id = pp.id 
	LEFT JOIN role_types rt ON pr.role_type_code = rt.code 
	LEFT JOIN projects pj ON pp.project_id = pj.id 
	WHERE pr.person_id = '.$_SESSION['user']['user_id'].' 
    AND en.project_program_id = '.$member_account.' 
	AND en.active = 1  
	AND pp.effective_date <= CURRENT_DATE 
	AND (pp.end_date IS NULL or pp.end_date >= CURRENT_DATE) 			
	ORDER BY en.effective_timestamp ASC';
		
// echo $queryEnrollments . '<br /><hr />';
// die;
	
$result_list_select_program_enrollment = mysqli_query($connection, $queryEnrollments);

if (!$result_list_select_program_enrollment) {
	show_mysqli_error_message($queryEnrollments, $connection);
	die;
}
	
// echo 'Database Query Select Person Program Enrollments succeeded.<br /><hr />';
	
$rowcount = mysqli_num_rows($result_list_select_program_enrollment);

if ($rowcount == 0) {
	
	// echo 'No Enrollments so take user to their Dashboard page ...<br />';
	// die;
	
	header("location: ../member-dashboard/");

	// $_SESSION['message_type'] = 'alert-box info radius';		
	// $_SESSION['message'] = '<p>Hello <strong>'.$_SESSION['user']['full_name'].'</strong>. You are not currently enrolled into any active Wright Check Wellness programs. Please contact your Human Resources department or Wright Check to see what programs you are eligible for and when they begin. You may access the <b>Home</b> and <b>Store</b> menu options.</p>';
	// header("location: ../logout/index.php");
	
	exit();	
}

if ($rowcount == 1) {

	// User has a SupplementRelief Member Account enrollment
	// echo 'Only 1 enrollment so select it automatically and take user to their Dashboard page.<br />';
	
	// Hack for Libby's Revolutionary Joy program
	// Set the default Program to Family Care for when the User lands on the Member Dashboard page.
	
	$family_care_program = 32;
	
	$queryFamilyCareEnrollment = 'SELECT 
	en.id AS enrollment_id 
	FROM person_roles pr 
	LEFT JOIN enrollments en ON pr.id = en.person_role_id  
	LEFT JOIN organization_unit_facilities ouf ON pr.organization_unit_facility_id = ouf.id 
	LEFT JOIN project_programs pp ON en.project_program_id = pp.id 
	LEFT JOIN role_types rt ON pr.role_type_code = rt.code 
	LEFT JOIN projects pj ON pp.project_id = pj.id 
	WHERE pr.person_id = '.$_SESSION['user']['user_id'].' 
    AND en.project_program_id = '.$family_care_program.' 
	AND en.active = 1  
	AND pp.effective_date <= CURRENT_DATE 
	AND (pp.end_date IS NULL or pp.end_date >= CURRENT_DATE) 			
	LIMIT 1';
		
	// echo $queryFamilyCareEnrollment . '<br /><hr />';
	// die;
		
	$result_family_care_enrollment = mysqli_query($connection, $queryFamilyCareEnrollment);
	
	if (!$result_family_care_enrollment) {
		show_mysqli_error_message($queryFamilyCareEnrollment, $connection);
		die;
	}
				
	$rowcount_family_care = mysqli_num_rows($result_family_care_enrollment);
	
	if ($rowcount_family_care == 1) { 
		// User also has a Family Care program enrollment so make that the default for them when they get to the Member Dashboard page.
		$fc = mysqli_fetch_assoc($result_family_care_enrollment);
		$target_uri = '../member-dashboard/?e_id='.$fc['enrollment_id'];
		// echo 'Target URI: '.$target_uri.'<br /><hr />';
		// die;
		header("location: $target_uri");
		mysqli_free_result($result_family_care_enrollment);
		exit();
		
	} else {
		
		$r = mysqli_fetch_assoc($result_list_select_program_enrollment);
		$target_uri = '../member-dashboard/?e_id='.$r['enrollment_id'];
		// echo 'Target URI: '.$target_uri.'<br /><hr />';
		// die;
		header("location: $target_uri");
		mysqli_free_result($result_list_select_program_enrollment);
		exit();	
		
	}
		
}

?>

<?php
$_SESSION['message_type'] = 'alert-box info radius';

// $_SESSION['message'] = '<h3><i class="fa fa-info-circle"></i>&nbsp;&nbsp;Member Announcement <small>January 23, 2016</small></h3>';

// $_SESSION['message'] .= '<p>We added a <b>FREE online Wellness program for our Member Accounts</b> called <b>Your Best Weight: Members</b>. It has 5 online modules of content you can review at your convenience. You may <b>select it now</b> (while you are logging in) or <b>from your Member Dashboard later at any time</b>. You can still <b>shop for NuMedica supplements regardless of which program you select</b>.</p>';

// $_SESSION['message'] .= '<hr>'; 
					
$_SESSION['message'] .= '<p>You are currently enrolled into <b>'.$rowcount.'</b> programs. Please <strong>select one</strong> from the list below before proceeding. You will then be taken to your <b>Member Dashboard</b> page. From there you can <b>switch to another program</b> any time you like while you are logged in.</p>';
show_session_message();

?>
	
<div class="row">	
	<div class="small-12 columns">
		<table width="100%">
				
			<thead>
				<tr>
				  <th><center><i class="fa fa-eye"></i></center></th>
				  <th class="show-for-medium-up">Project</th>   			
				  <th>Program</th>
				  <th>Enrollment<br />Timestamp</th> 
				  <th class="show-for-medium-up">Program<br />Start Date</th> 
				  <th class="show-for-medium-up">Program<br />End Date</th> 			  			 			  			  		   
				  <!-- <th class="show-for-medium-up">Role</th> -->
				  <!-- <th class="show-for-medium-up">Organization<br />Facility<br />Name</th> -->	     
				</tr>	
			</thead>
			
			<tr>
				
				<?php
						
				while($r = mysqli_fetch_assoc($result_list_select_program_enrollment)) {
				
					if (is_null($r['project_program_end_date'])) {
					    $project_program_end_date = '';
					} else {
					    $project_program_end_date = date('m/d/y', strtotime($r['project_program_end_date']));
					}
									
					?>
											
					<td><center><a href="../member-dashboard/?e_id=<?php echo $r['enrollment_id']; ?>" title="Select Program Enrollment" >Select</a></center></td>
			
					<td class="show-for-medium-up"><?php echo $r['project_name']; ?></td>
													
					<td><?php echo $r['project_program_name']; ?></td>
										
					<td><?php echo date('m/d/y h:i A T', strtotime($r['enrollment_timestamp'])); ?></td>
					
					<td class="show-for-medium-up"><?php echo date('m/d/y', strtotime($r['project_program_effective_date'])); ?></td>
					
					<td class="show-for-medium-up"><?php echo $project_program_end_date; ?></td>
									
					<!-- <td class="show-for-medium-up"><?php echo $r['role_type_name']; ?></td> -->
								
					<!-- <td class="show-for-medium-up"><?php echo $r['organization_unit_facility_name']; ?></td> -->
														
				</tr>
			
				<?php } ?>
						 							
		</table>
		
	</div>
</div>

<?php

mysqli_free_result($result_list_select_program_enrollment);

?>