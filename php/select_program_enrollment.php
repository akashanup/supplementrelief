<?php

// echo 'Begin select_program_enrollment.php<br>';

/* 
User has successfully logged in (login_process.php login()) and SESSION variables have been set:
$_SESSION['user']['login_timestamp'] = date('m/d/y h:i A T');				
$_SESSION['user']['user_id'] = $row['id'];
$_SESSION['user']['username'] = $row['username'];
$_SESSION['user']['full_name'] = $row['first_name'].' '.$row['last_name'];
$_SESSION['user']['email'] = $row['email'];
$_SESSION['user']['phone'] = $row['phone'];
$_SESSION['user']['access_level'] = $row['user_level'];
*/

// show_array($_SESSION);

$member_account = 21; // SupplementRelief Member Account
// Make this a defines variable in /includes/defines.php relevant for each application url

$queryEnrollments = 'SELECT 
	en.id AS enrollment_id, 
	en.effective_timestamp AS enrollment_timestamp, 
	en.active,  
	en.forum_moderator, 
	ouf.name AS organization_unit_facility_name, 
	pp.id AS project_program_id, 
	pp.name AS project_program_name, 
	pp.effective_date AS project_program_effective_date, 
	pp.end_date AS project_program_end_date, 
	rt.name AS role_type_name, 
	pj.name AS project_name 
	FROM person_roles pr 
	LEFT JOIN enrollments en ON pr.id = en.person_role_id  
	LEFT JOIN organization_unit_facilities ouf ON pr.organization_unit_facility_id = ouf.id 
	LEFT JOIN project_programs pp ON en.project_program_id = pp.id 
	LEFT JOIN role_types rt ON pr.role_type_code = rt.code 
	LEFT JOIN projects pj ON pp.project_id = pj.id 
	WHERE pr.person_id = '.$_SESSION['user']['user_id'].' 
    /* AND en.project_program_id = '.$member_account.' */ 
    AND en.primary_enrollment = 1 
	AND en.active = 1 
	AND pp.effective_date <= CURRENT_DATE 
	AND (pp.end_date IS NULL or pp.end_date >= CURRENT_DATE) 			
	ORDER BY en.effective_timestamp ASC 
	LIMIT 1';
		
// echo $queryEnrollments . '<br /><hr />';
// die;

$result_list_select_program_enrollment = mysqli_query($connection, $queryEnrollments);

if (!$result_list_select_program_enrollment) {
	show_mysqli_error_message($queryEnrollments, $connection);
	die;
}
	
// echo 'Database Query Select Person Program Enrollments succeeded.<br /><hr />';
	
$rowcount = mysqli_num_rows($result_list_select_program_enrollment);

if ($rowcount == 0) {
	
	// echo 'No Enrollments so take user to their Dashboard page ...<br />';
	// die;
	
	header("location: ../member-dashboard/");

	// $_SESSION['message_type'] = 'alert-box info radius';		
	// $_SESSION['message'] = '<p>Hello <strong>'.$_SESSION['user']['full_name'].'</strong>. You are not currently enrolled into any active programs. Please contact your Human Resources department to see what programs you are eligible for and when they begin. You may access the <b>Home</b> and <b>Store</b> menu options.</p>';
	// header("location: ../logout/index.php");
	
	exit();	
} 

if ($rowcount == 1) {
			
	$r = mysqli_fetch_assoc($result_list_select_program_enrollment);
	$target_uri = '../member-dashboard/?e_id='.$r['enrollment_id'];
	// echo 'Target URI: '.$target_uri.'<br /><hr />';
	// die;
	header("location: $target_uri");
	mysqli_free_result($result_list_select_program_enrollment);
	exit();	
				
}
