<?php

// echo 'Begin select_wright_path_campaign.php<br />';

// Check to see if the User already has an Enrollment Opt-In for any Wright Path Campaign. They are allowed to be enrolled in only one at a time. If so, do NOT show this dialogue or allow them to create another Enrollment Opt-In.

if (!empty($_SESSION['enrollment']['enrollment_id'])) {
	$enrollment_id = $_SESSION['enrollment']['enrollment_id'];	
}

$queryE	 = 'SELECT 
			eoi.id, 
			eoi.enrollment_id, 
			eoi.project_program_content_asset_id, 
			eoi.effective_date 
			FROM enrollment_opt_ins eoi 
			LEFT JOIN project_program_content_assets ppca ON eoi.project_program_content_asset_id = ppca.id 		 
			LEFT JOIN content_assets ca ON ppca.content_asset_id = ca.id 
			WHERE eoi.enrollment_id = '.$enrollment_id.' 
			AND eoi.status = "A"
			AND ca.content_asset_type_code LIKE "WPC%"'; 
			
// status = "A" means active, which means they are curently participating in the campaign and therefore cannot be involved in another
// Note that concurrent Capmpaign participation is limited to an Enrollment (Person Role & Project Program).
// If the User is in another Person Role and/or Project Program, then the restriction does not apply.
			
// echo $queryE . '<br /><hr />';
					
$result_list_existing_opt_in = mysqli_query($connection, $queryE);

if (!$result_list_existing_opt_in) {
	echo $queryE . '<br /><hr />';	
	die("Database List Existing Enrollment Opt-ins query failed.");
}

$rowcount = mysqli_num_rows($result_list_existing_opt_in);

// echo 'Number Opt-ins: '.$rowcount.'<br />';

if ($rowcount == 0) {

	// echo 'No Opt-ins so create one.<br />';
	
	// User has not previously opted-in to a Wright Path Campaign for their PErson Role and this Project Program Offering, or if they have, they have completed the Campaign (status not "A" anymore) so allow them the opportunity to participate in another campaign.

	if (!empty($_SESSION['wright_path_campaign_opt_in_success'])) {
	
		if ($_SESSION['wright_path_campaign_opt_in_success'] == "Y") {
		
			echo '<div data-alert class="alert-box secondary radius">';
			echo '<p>'.$_SESSION['message'].'</p>';
			echo '<a href="#" class="close">&times;</a>';
			echo '</div>';
			// $_SESSION['wright_path_campaign_opt_in_success'] = '';
			$_SESSION['message'] = '';
			// $_SESSION['enrollment_id'] = '';
			// $_SESSION['project_program_id'] = '';	
			
		}
		
	}
	
	if (isset($_SESSION['wright_path_campaign_opt_in_success'])) {
	
		 unset($_SESSION['wright_path_campaign_opt_in_success']);
		 		
	} else {
	
		// echo 'About to see if Enrollment ID and Project Program ID valued.<br><hr>';
		
		if (!empty($_SESSION['enrollment']['enrollment_id']) && !empty($_SESSION['enrollment']['project_program_id'])) {
		
			// echo 'Display the Enrollment form.<br />';
		
			$enrollment_id = $_SESSION['enrollment']['enrollment_id'];
			$project_program_id = $_SESSION['enrollment']['project_program_id'];
			
			$current_timestamp = date('Y-m-d H:i:s');
			// echo 'Current Date and Time in Detroit : '.$current_timestamp_detroit.'<br /><hr />';
		
			$queryCampaigns	 = 'SELECT 
				ppca.id, 
				ppca.project_program_id, 
				ppca.content_asset_id, 
				ca.title, 
				ppca.seq, 
				ppca.opt_in, 
				ppca.effective_date, 
				ppca.scheduled_delivery_timestamp, 
				ppca.actual_delivery_timestamp, 
				ppca.scheduled_delivery_complete_timestamp
				FROM project_program_content_assets ppca 
				LEFT JOIN content_assets ca on ppca.content_asset_id = ca.id 
				WHERE ppca.project_program_id = '.$project_program_id.' 
				AND ppca.opt_in = "Y" 
				AND ppca.scheduled_delivery_timestamp <= "'.$current_timestamp.'" 
				AND (scheduled_delivery_complete_timestamp IS NULL or scheduled_delivery_complete_timestamp >= "'.$current_timestamp.'") 
				AND ca.content_asset_type_code LIKE "WPC%" 
				ORDER BY ppca.seq ASC';
			
			// Need to include scheduled_delivery_timestamp	=> CURRENT_DATE as part of WHERE clause. May also need expose this field in Admin system for configuration.	
			// echo $queryCampaigns . '<br /><hr />';
			
			$result_list_campaigns = mysqli_query($connection, $queryCampaigns);
			
			if (!$result_list_campaigns) {
				echo $queryCampaigns . '<br /><hr />';
				die("Database List Project Program Content Asset Opt-ins query failed.");
			}
			
			// echo 'Database Query Project Program Content Asset Opt-ins succeeded.<br /><hr />';
			
			$counter = 1;
			$campaign_options = '<option value="">Select a Campaign...</option>';
			
			while($r = mysqli_fetch_assoc($result_list_campaigns)) {
			
				if ($counter == 1) {
				
					echo '
					<div data-alert class="alert-box secondary radius">
						<h4>Wright Path Daily Email</h4>
						<center><img style="padding-top: 5px; padding-bottom:10px;" src="http://wrightcheck.com/media/images/wright-path-decision.png" title="View Wright Path Campaigns to select from" alt="" /></center>
						<p>Want to work on one new healthy habit to change your health risks? <b>Take The Wright Path!</b> This <b>daily email reminder</b> will help you make one lasting change at a time over a month. Choose your path and <a href="#" data-reveal-id="wrightPathSignUp">sign up</a> today!</p>
					<a href="#" class="close">&times;</a>
					</div>';
					
					$counter ++;		
				}
				
				$selected = '';
				// if(($_GET['content_asset_id'] ? $_GET['content_asset_id'] : $row1['p']) == $r['content_asset_id']) $selected = ' selected';
				$campaign_options .= '<option value="'.$r['id'].'" '.$selected.'>'.cleanEncoding($r['title']).'</option>';					
			}
			
			mysqli_free_result($result_list_campaigns);
				
		} else {
			echo 'Session Enrollment ID and Project Program ID not valued. User did not select an Enrollmentat Login. Contact your System Administrator.<br/><hr />';
		}
		
	}
	
} else {

	// An existing active Enrollment Opt-in was found. If it was just created, display the confirmation message to the user.
	
	if ($_SESSION['wright_path_campaign_opt_in_success'] == "Y") {
	
		show_session_message();
		unset($_SESSION['wright_path_campaign_opt_in_success']);
		
	}
				
}

mysqli_free_result($result_list_existing_opt_in);	

?>

<div id="wrightPathSignUp" class="reveal-modal small" data-reveal>
	<h3>Wright Path Daily Email</h3>
	<p class="lead">Choose one Campaign below:</p>
	<p>You may participate in any Wright Path Campaign; however you may only participate in one at a time.</p>
	
	<form action="../php/process_wright_path_campaign_opt_in.php" method="post" />
		<input type="hidden" name="action" value="1"/>
		<center><select style="width: 75%; "name="wright_path_campaign_id"><?php echo $campaign_options; ?></select>
		<input class="button tiny radius" id="button" type="submit" value="OPT IN"></center>
	</form>

	<a class="close-reveal-modal">&#215;</a>
	
</div>