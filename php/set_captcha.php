<?php
	include_once('../includes/header.php');
	$json['status'] 	= 	0;
	if(isset($_POST['captcha_input']))
	{
		$captcha_code 	= 	safe_sql_data($connection, $_POST['captcha_input']);
		if($_SESSION['captcha'] == $captcha_code)
		{
			$_SESSION['captcha_verified'] 	= 	true;
			$json['status'] 				= 	1;
		}
	}
	echo json_encode($json);
?>