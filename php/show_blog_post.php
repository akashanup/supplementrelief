<?php

// echo 'Begin show_blog_post.php<br /><hr />';
// Calling Page: Show Blog Post
// https://supplementrelief.com/show_blog_post/?bp_id=1427

if (isset($_GET['bp_id'])) { 
	$blog_post_id = $_GET['bp_id'];
	$_SESSION['blog_post']['blog_post_id'] = $blog_post_id;	
}

include_once('../includes/core.php');

// Get the Blog Post
$queryBlogPost = 'SELECT 
	ca.id, 
	ca.title, 
	ca.summary_description, 
	ca.description, 
	ca.citations, 
	ca.author,
	bp.blog_post_type_code, 
	bp.series AS blog_post_series, 
	bp.summary_text, 
	bp.discussion_forum, 
	bp.post_effective_timestamp,  
	bp.post_end_timestamp, 
	dfc.discussion_thread_post, 
	dfc.discussion_thread_post_document, 
	dfc.discussion_thread_post_image, 
	dfc.discussion_thread_post_video,
	dfc.discussion_thread_post_audio, 
	dfc.discussion_reply_post, 
	dfc.discussion_reply_post_document, 
	dfc.discussion_reply_post_image, 
	dfc.discussion_reply_post_video,
	dfc.discussion_reply_post_audio, 
	bpt.icon_snippet, 
	bpt.name as blog_post_type_name, 
	im.host_url AS blog_post_image_url, 
    im.caption AS blog_post_image_caption, 
    im.alt_text AS blog_post_image_alt_text 
	FROM content_assets ca 
	LEFT JOIN blog_posts bp ON ca.id = bp.content_asset_id 
	LEFT JOIN blog_post_types bpt ON bp.blog_post_type_code = bpt.code 
	LEFT JOIN images im ON 
	(bp.image_id = im.content_asset_id  AND 
	 im.size = "Blog Post" AND 
	 im.usage_size = "Small" AND 
	 /* im.usage_shape = "Block" AND */ 
	 im.format = "JPG") 		
	LEFT JOIN discussion_forum_configurations dfc ON bp.content_asset_id = dfc.blog_post_id 
 	WHERE ca.id = '.$blog_post_id.' 
 	GROUP BY ca.id ';
						
// echo $queryBlogPost . '<br /><hr />';

$result_blog_post = mysqli_query($connection, $queryBlogPost);

if (!$result_blog_post) {
	show_mysqli_error_message($queryBlogPost, $connection);
	die;
}

// Query the Blog Post Categories

$queryBlogPostCategories = 'SELECT 
    catc.name AS child_category_name 
    FROM project_content_asset_category_usages pcacu 
    LEFT JOIN project_category_usages pcu ON pcacu.project_category_usage_id = pcu.id 
    LEFT JOIN category_usages cu ON pcu.category_usage_id = cu.id 
    LEFT JOIN categories catc ON cu.child_code = catc.code 
    WHERE pcacu.content_asset_id = '.$blog_post_id.' 
    AND pcacu.effective_date <= CURRENT_DATE 
	AND (pcacu.end_date is NULL or pcacu.end_date >= CURRENT_DATE)  
    ORDER BY child_category_name ASC';
    
// echo $queryBlogPostCategories . '<br /><hr />';

$result_blog_post_category = mysqli_query($connection, $queryBlogPostCategories);

if (!$result_blog_post_category) {
	show_mysqli_error_message($queryBlogPostCategories, $connection);
	die;
}

$blog_post_category_block = '';

while($bpc = mysqli_fetch_assoc($result_blog_post_category)) {
	
	// if ($_SESSION['user']['user_id'] == 2) { show_array($bpc); }
	
	$blog_post_category_block .= '<span class="label">'.$bpc['child_category_name'].'</span>&nbsp;&nbsp;';
	
}

mysqli_free_result($result_blog_post_category);

$icon_color = 'red';
$blog_post_comment_counter_block = '';
$blog_post_block = '';
$core_text_displayed = false; // Blog Post text field is only displayed once but in different places for different Blog Post Types
$core_video_displayed = false; // A Video Blog Post currently allows for only 1 Video to be displayed

while($r = mysqli_fetch_assoc($result_blog_post)) {

	// show_array($r);
	// if ($_SESSION['user']['user_id'] == 2) { show_array($r); }
	
	if ($r['blog_post_series']) {
		$blog_post_series_block .= '<span class="label success">Blog Post Series</span>&nbsp;&nbsp;';
	}
	
	// $_SESSION['blog_post']['blog_post_id'] = $r['id'];	
	$_SESSION['blog_post']['blog_post_title'] = $r['title'];	

	// Set Discussion Forum and File Type posting behavior options.
	
	if ($r['discussion_forum']) {
		// Set session variables to persist the Discussion Forum Configuration for this Recipe
		$_SESSION['blog_post']['discussion_forum']['forum'] = $r['discussion_forum'];	
		$_SESSION['blog_post']['discussion_forum']['thread_post'] = $r['discussion_thread_post'];
		$_SESSION['blog_post']['discussion_forum']['thread_post_document'] = $r['discussion_thread_post_document'];	
		$_SESSION['blog_post']['discussion_forum']['thread_post_image'] = $r['discussion_thread_post_image'];
		$_SESSION['blog_post']['discussion_forum']['thread_post_video'] = $r['discussion_thread_post_video'];
		$_SESSION['blog_post']['discussion_forum']['thread_post_audio'] = $r['discussion_thread_post_audio'];
		$_SESSION['blog_post']['discussion_forum']['reply_post'] = $r['discussion_reply_post'];
		$_SESSION['blog_post']['discussion_forum']['reply_post_document'] = $r['discussion_reply_post_document'];	
		$_SESSION['blog_post']['discussion_forum']['reply_post_image'] = $r['discussion_reply_post_image'];
		$_SESSION['blog_post']['discussion_forum']['reply_post_video'] = $r['discussion_reply_post_video'];		
		$_SESSION['blog_post']['discussion_forum']['reply_post_audio'] = $r['discussion_reply_post_audio'];		
	} else {
		unset($_SESSION['blog_post']['discussion_forum']);
	}
		
	if ($_SESSION['blog_post']['discussion_forum']) {
		// Determine how many Comments have been posted for this Blog Post and display the count.
	
		// WHERE project_program_id = '.$_SESSION['enrollment']['project_program_id'].' was removed for SupplementRelief code base only.
		$queryCountBlogPostComments = 'SELECT id  
			FROM discussion_threads  
			/* WHERE project_program_id = '.$_SESSION['enrollment']['project_program_id'].' */ 
			WHERE blog_post_id = '.$blog_post_id.'   
			AND status in ("S", "R") '; 			
					
		// echo $queryCountBlogPostComments . '<br /><hr />';
		
		$result_list_count_blog_post_comment = mysqli_query($connection, $queryCountBlogPostComments);
		
		if (!$result_list_count_blog_post_comment) {
			show_mysqli_error_message($queryCountBlogPostComments, $connection);
			die;
		}
		
		// echo 'Database Query Count Comments for Project Program Blog Post succeeded.<br /><hr />';
		
		if (mysqli_num_rows($result_list_count_blog_post_comment) > 0) {			
			$comment_count = mysqli_num_rows($result_list_count_blog_post_comment);
			$blog_post_comment_counter_block = '<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="# Comments"><i class="fa fa-comment icon-'.$icon_color.'"></i></span>&nbsp;&nbsp;'.$comment_count;				
		} else {
			$comment_count = 0;
			$blog_post_comment_counter_block = '';
		}
						
		// echo 'Comment Count: '.$display_comment_count.'<br /><hr />';
		
		// $blog_post_comment_counter_block = '<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="# Comments"><i class="fa fa-comment icon-'.$icon_color.'"></i></span>&nbsp;&nbsp;'.$comment_count;				
				
	}
							
	$blog_post_summary_block .='
	<div class="panel">
				
	    <div class="row">
		 
            <div class="small-12 medium-3 columns">											
                <div id="blogSummaryLogo">
					<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip tip-right" title="'.$r['blog_post_image_caption'].'"><img src="'.$_SESSION['application']['root_media_host_url'].$r['blog_post_image_url'].'" alt="'.$r['blog_post_image_alt_text'].'" class="th" />
					</span>
                </div>
            </div>

			<div class="small-12 medium-9 columns">
				<div class="blogPostTitle">
					<h1><span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="'.$r['blog_post_type_name'].' Blog Post">'.$r['icon_snippet'].'</span>&nbsp;'.$r['title'].'</h1>
					<!--<h3><i class="fa fa-comment icon-'.$icon_color.'"></i>&nbsp;&nbsp;'.$r['title'].'</h3>-->
				</div>
                <div class="row">
				 	<div class="small-12 columns text-left">
				 		<div class="blogPostAuthorTimestamp"><span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Author"><i class="fa fa-user icon-'.$icon_color.'"></i></span>&nbsp;&nbsp;'.$r['author'].'&nbsp;&nbsp;
				 			<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Date Posted"><i class="fa fa-clock-o icon-'.$icon_color.'"></i></span>&nbsp;&nbsp;'.date('m/d/y', strtotime($r['post_effective_timestamp'])).'&nbsp;&nbsp;'.
				 			$blog_post_comment_counter_block.'
				 		</div>
				 	</div>								
                </div>
                <div class="blogPostSummaryDescription">				 
                    <div class="row">
                        <div class="small-12 columns">'
                    	    .cleanEncoding($r['summary_description']).'
                    	    <!--
                    	    <div class="blogPostSeries">'
                            	.$blog_post_series_block.'
							</div>
							-->		
                    	    <div class="blogPostCategories">'
                            	.$blog_post_series_block.$blog_post_category_block.'
							</div>		
                        </div>
		 	 							 
                    </div>

                </div>
            </div>
        </div>
    </div>';
    
    echo $blog_post_summary_block;
    
    $blog_post_block .=' 
	<div id="articleTextBlocks" class="blogPostContainer1">			
		<div class="row">		
			<div class="small-12 columns">';	
	
				// echo 'Blog Post Type Code: '.$r['blog_post_type_code'].'<br /><hr />';
						
				$blog_post_block .= 
				'<div class="blogPostBody">';
				
				if ($r['blog_post_type_code'] == "TEXT" OR $r['blog_post_type_code'] == "VIDEO") {
					if (!$core_text_displayed) {
						$core_text_displayed = true;
						// Display the Blog Post primary text
						$blog_post_block .= 
						'<div id="blogPostPrimaryText">
						
							 <div class="row">
								<div class="small-12 columns">'
									.cleanEncoding($r['description']);
									
									if (!empty($r['citations'])) {
										$blog_post_block .= '
										<div id="blogPostCitations">
											<b>Citations:</b><br>'
											.cleanEncoding($r['citations']).'
										</div>';			
									}
																						
									$blog_post_block .=	'					
								</div>
							</div>		
						</div>';								 									
					
					}
				}
				
                // Get any current Project Brand Products associated with this Blog Post. 
                $queryBlogPostProjectBrandProducts = 'SELECT 
                    bpu.name AS addendum_product_name,
                    pbpu.group_options_description AS addendum_product_group_options_description,  
                    pbpu.page_meta_description AS addendum_product_page_meta_description, 
					pbpu.page_canonical_url AS addendum_product_canonical_url, 
					urls.url AS addendum_product_url, 
                    im.host_url AS addendum_product_image_url, 
					im.alt_text AS addendum_product_image_alt_text 
					FROM content_asset_project_brand_products capbp 
					LEFT JOIN project_brand_product_usages pbpu ON capbp.project_brand_product_usage_id = pbpu.id 
					LEFT JOIN url_usages urlu ON pbpu.id = urlu.pbpu_id 
					LEFT JOIN urls urls ON urlu.url_id = urls.id 
					LEFT JOIN brand_product_usages bpu ON pbpu.brand_product_usage_id = bpu.id 
                    LEFT JOIN images im 
					    ON (bpu.image_id = im.content_asset_id AND 
                            im.size = "Product" AND 
                            im.usage_size = "Small" AND 
                            /* im.usage_shape = "Portrait" AND */ 
                            im.format = "JPG")
					WHERE capbp.content_asset_id = '.$blog_post_id.' 
					AND (capbp.effective_date <= CURRENT_DATE AND
                        (capbp.end_date IS NULL or capbp.end_date >= CURRENT_DATE)) 
                    AND pbpu.active = "1" 
                    ORDER BY bpu.name'; 
                    
                // echo $queryBlogPostProjectBrandProducts . '<br /><hr />';
				
				$result_blog_post_project_brand_product = mysqli_query($connection, $queryBlogPostProjectBrandProducts);
				
				if (!$result_blog_post_project_brand_product) {
					show_mysqli_error_message($queryBlogPostProjectBrandProducts, $connection);
					die;
				}
								
				// echo 'Database Query Project Program Blog Post Content Assets succeeded.<br /><hr />';
				
				$product_addendum_block = '';
				
				while($pbp = mysqli_fetch_assoc($result_blog_post_project_brand_product)) { 
    				
    				if (!$product_addendum_block) {
                		// Create the Resource Product Addendum container.
                		$product_addendum_block .= '
                			<!--googleoff: index-->
                		    <div class="panel callout radius">
                		        <div class="small-12 columns">
                		            <h3><i class="fa fa-product-hunt"></i>  Supplement Referrals</h3>
                		            <p>Learn more about these <b>NuMedica Supplements</b> on our website.</p>
                                </div>';
                	}
                	
                    $addendum_product_image_url = $pbp['addendum_product_image_url'];
                	$addendum_product_image_alt_text = $pbp['addendum_product_image_alt_text'];
                	$addendum_product_url = $pbp['addendum_product_url'];
                	$addendum_product_page_meta_description	= $pbp['addendum_product_page_meta_description'];			
                        
                    $product_addendum_block .= '
                	    <hr>
                	    <div class="row">		
                            <div class="small-12 medium-2 columns text-center">
                	            <img class="th" src="'.$_SESSION['application']['root_media_host_url'].$addendum_product_image_url.'" alt="'.$addendum_product_image_alt_text.'" width="125px">
                            </div>
                            <div class="small-12 medium-10 columns colWrapMargin">
                                <a href="../'.$addendum_product_url.'/"><h4>'.$pbp['addendum_product_name'].'</a></h4>'.
                                $pbp['addendum_product_page_meta_description'].'
                             </div>
                        </div>';		
                }
                
                if ($product_addendum_block) { 
                	    
                	$product_addendum_block .= '
                	    </div> <!-- Close Panel -->
                	    <!--googleon: index-->';
                }
           
                mysqli_free_result($result_blog_post_project_brand_product);
			
				// Get the Blog Post Content Assets (if any)
				$queryBlogPostContentAssets = 'SELECT 
					ca.id, 
					ca.title, 
					ca.summary_description, 
					ca.description, 
					ca.author, 
					ca.content_asset_type_code, 
					cat.name AS category_name, 
					au.host_url_mp3, 
					do.host_url, 
					do.caption AS document_caption,   
					vi.host_url_mp4, 
					vi.host_url_webm, 
					vi.duration_minutes_seconds AS video_duration_minutes_seconds, 
					/* vi.poster_image_url as video_poster_image_url, */ 
					imvi.host_url AS video_poster_image_url, 
					imvi.caption AS video_caption, 
					imvi.alt_text AS video_alt_text,   
					bp.page_canonical_url AS blog_post_canonical_url, 
                    imbp.host_url AS addendum_blog_post_image_url, 
					imbp.alt_text AS addendum_blog_post_image_alt_text,
                    rec.page_canonical_url AS addendum_recipe_canonical_url,     
					imrec.host_url AS addendum_recipe_image_url, 
					imrec.alt_text AS addendum_recipe_image_alt_text,     
					rk.page_canonical_url AS resource_canonical_url,     
					imrk.host_url AS addendum_resource_image_url, 
					imrk.alt_text AS addendum_resource_image_alt_text    
					FROM content_asset_usages cau 
					LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id 
					LEFT JOIN categories cat ON cau.category_code = cat.code 
					LEFT JOIN audios au ON ca.id = au.content_asset_id  
					LEFT JOIN documents do ON ca.id = do.content_asset_id 
					LEFT JOIN videos vi ON ca.id = vi.content_asset_id
					LEFT JOIN images imvi 
					    ON (vi.image_id = imvi.content_asset_id AND 
                            imvi.size = "Video" AND 
                            imvi.usage_size = "Large" AND 
                            imvi.usage_shape = "Landscape" AND  
                            imvi.format = "JPG") 	 	
                    LEFT JOIN blog_posts bp ON ca.id = bp.content_asset_id 
					LEFT JOIN images imbp 
					    ON (bp.image_id = imbp.content_asset_id AND 
                            imbp.size = "Blog Post" AND 
                            imbp.usage_size = "Small" AND 
                            imbp.usage_shape = "Block" AND  
                            imbp.format = "JPG")
                    LEFT JOIN recipes rec ON ca.id = rec.content_asset_id 
					LEFT JOIN images imrec 
					    ON (rec.image_id = imrec.content_asset_id AND 
                            imrec.size = "Recipe" AND 
                            imrec.usage_size = "Small" AND 
                            imrec.usage_shape = "Block" AND  
                            imrec.format = "JPG") 	
					LEFT JOIN resource_kits rk ON ca.id = rk.content_asset_id 
					LEFT JOIN images imrk 
					    ON (rk.image_id = imrk.content_asset_id AND 
                            imrk.size = "Resource" AND 
                            imrk.usage_size = "Small" AND 
                            imrk.usage_shape = "Block" AND  
                            imrk.format = "JPG")
					WHERE cau.content_asset_parent_id = '.$blog_post_id.' 
					/* AND cau.category_code IS NULL */ 
					ORDER BY cau.featured DESC, ca.title ASC';
										
				// echo $queryBlogPostContentAssets . '<br /><hr />';
				
				$result_blog_post_content_asset = mysqli_query($connection, $queryBlogPostContentAssets);
				
				if (!$result_blog_post_content_asset) {
					show_mysqli_error_message($queryBlogPostContentAssets, $connection);
					die;
				}
								
				// echo 'Database Query Project Program Blog Post Content Assets succeeded.<br /><hr />';
					
				while($a = mysqli_fetch_assoc($result_blog_post_content_asset)) { 
    				
    				// show_array($a);
    				
    				$content_asset_id = $a['id'];
    				// echo 'Content Asset Type Code: '.$a['content_asset_type_code'].'<br /><hr />';

    				if (!$a['category_name']) {
        				// This is a Body content asset and not an Addendum content asset.											
					
                        switch ($r['blog_post_type_code']) {
					
						case 'AUDIO':
													
							switch ($a['content_asset_type_code']) {
							
								case 'TEXT':
							
									// Display the Text Block content
									$blog_post_block .= 
									'<div class="blogPostTextBlock">
									
										 <div class="row">
											<div class="small-12 columns">'
												.cleanEncoding($a['description']).'		
											</div>
										</div>		
									</div>';
									
								break;								
																		
								case 'AUDIO':
																					
									$blog_post_block .= 
									'<div class="blogPostAudio">
										 <div class="row">
											<div class="small-12 columns">
												<audio controls> 
													<source src="'.$_SESSION['application']['root_media_host_url'].$a['host_url_mp3'].'" type="audio/mpeg">
													Your browser does not support HTML 5 Audio.
												</audio>   						
											
											</div>
										 </div>		
									</div>';
									
								break;								 									
								
							}
																																	 		
						break;
						
						case 'DOC':
													
							switch ($a['content_asset_type_code']) {
							
								case 'DOC':
																					
									$blog_post_block .= 
									'<div class="blogPostDocument">
										<div class="row">
											<div class="small-12 columns">
												<h2><a href="'.$_SESSION['application']['root_media_host_url'].$a['host_url_pdf'].'" target="_blank" title="View and download PDF document"><i class="fa fa-file-pdf-o"></i></a>&nbsp;'.$a['title'].'</h2>	
											</div>
										 </div>	
									</div>';
									
								break;								 	
							
								case 'TEXT':
							
									// Display the Text Block content
									$blog_post_block .= 
									'<div class="blogPostTextBlock">
									
										 <div class="row">
											<div class="small-12 columns">'
												.cleanEncoding($a['description']).'		
											</div>
										</div>		
									</div>';
									
								break;																
								
							}
																																	 		
						break;						
										
						case 'PHOTO':
													
							switch ($a['content_asset_type_code']) {
							
								case 'IMAGE':
							
									// Get the Image Configurations and determine which one to use.
									// For now we are just getting the first image configuration which is not the right solution.
									
									$queryBlogPostImageConfigurations = 'SELECT 
										im.id as image_configuration_id, 
										im.seq,
										im.host_url, 
										im.caption, 
										im.alt_text, 
										im.format, 
										im.size, 
										im.horizontal_pixel_size, 
										im.vertical_pixel_size 
										FROM images im 
										WHERE im.content_asset_id = '.$content_asset_id.' 
										AND im.format = "JPG" 
										AND im.size = "Blog Post" 
										AND im.usage_size = "Large" 
										ORDER BY im.seq   
										LIMIT 1';
															
									// echo $queryBlogPostImageConfigurations . '<br /><hr />';
									
									$result_blog_post_image = mysqli_query($connection, $queryBlogPostImageConfigurations);
									
									if (!$result_blog_post_image) {
										show_mysqli_error_message($queryBlogPostImageConfigurations, $connection);
										die;
									}
									
									// echo 'Database Query Project Program Blog Post Image Configurations succeeded.<br /><hr />';
										
									while($i = mysqli_fetch_assoc($result_blog_post_image)) {
										
										// show_array($i);								
									
										$blog_post_block .= 
										'<div class="blogPostImage">
											 <div class="row">
												<div class="small-12 columns">
													<img class="th" src="'.$_SESSION['application']['root_media_host_url'].$i['host_url'].'" alt="'.$i['alt_text'].'" />';
													if (!empty($i['caption'])) {
														$blog_post_block .= '
														<div class="blogPostImageCaption">'
															.$i['caption'].'
														</div>';
													}
																									
													$blog_post_block .= ' 
												</div>
											</div>		
										</div>';
										
									}
									
									mysqli_free_result($result_blog_post_image);
									
									if (!$core_text_displayed) {
										$core_text_displayed = true;
										// Display the Blog Post primary text
										$blog_post_block .= '
										<div id="blogPostPrimaryText">
										
											<div class="row">
												<div class="small-12 columns">'
													.cleanEncoding($r['description']);
													
													if (!empty($r['citations'])) {
														$blog_post_block .= '
														<div id="blogPostCitations">
														<b>Citations:</b><br>'
															.cleanEncoding($r['citations']).'
														</div>';			
													}
																										
													$blog_post_block .=	'		
												</div>
											</div>		
										</div>';								 																		
									}
																		
								break;
								
								case 'TEXT':
							
									// Display the Text Block content
									
									if (!$core_text_displayed) {
										$core_text_displayed = true;
										// Display the Blog Post primary text
										$blog_post_block .= 
										'<div id="blogPostPrimaryText">
										
											 <div class="row">
												<div class="small-12 columns">'
													.cleanEncoding($r['description']);
													
													if (!empty($r['citations'])) {
														$blog_post_block .= '
														<div id="blogPostCitations">'
															.cleanEncoding($r['citations']).'
														</div>';			
													}

													$blog_post_block .=	'
																
												</div>
											</div>		
										</div>';								 									
										
									}

									$blog_post_block .= 
									'<div class="blogPostTextBlock">
										 <div class="row">
											<div class="small-12 columns">'
												.cleanEncoding($a['description']).'		
											</div>
										</div>		
									</div>';
									
								break;
														
							}									
									
						break;	
						
						case 'QUOTE': 
						
							// echo 'Blog Post Type: '.$r['blog_post_type_code'].'<br />';
													
							switch ($a['content_asset_type_code']) {
													
								case 'QUOTE':
								
									// echo 'Content Asset Type: '.$a['content_asset_type_code'].'<br />';
																					
									// Display the Quote Block content
										$blog_post_block .= 
										'<div class="blogPostQuote">
											 <div class="row">
												<div class="small-12 columns">
													<blockquote>'.cleanEncoding($a['description']).'</blockquote>
													<span class="blogPostQuoteAuthor">&#126;&nbsp;'.$a['author'].'</span>		
												</div>
											</div>		
										</div>';
										
								break;
																
								case 'TEXT':
								
									// echo 'Content Asset Type: '.$a['content_asset_type_code'].'<br />';
																
									// Display the Text Block content
									$blog_post_block .= 
									'<div class="blogPostTextBlock">
										 <div class="row">
											<div class="small-12 columns">'
												.cleanEncoding($a['description']).'		
											</div>
										</div>		
									</div>';
									
								break;
																	
							}									
									
						break;	
																	
						case 'TEXT':
																			
							switch ($a['content_asset_type_code']) {
							
								case 'TEXT':
							
									// Display the Text Block content
									$blog_post_block .= 
									'<div class="blogPostTextBlock">
									
										 <div class="row">
											<div class="small-12 columns">'
												.cleanEncoding($a['description']).'		
											</div>
										</div>		
									</div>';
									
								break;
								
							}									
									
						break;	
						
						case 'VIDEO':
						
							// show_array($a);
																			
							switch ($a['content_asset_type_code']) {
							
								case 'TEXT':
								
									// show_array($a);
							
									// Display the Text Block content
									$blog_post_block .= 
									'<div class="blogPostTextBlock">
									
										 <div class="row">
											<div class="small-12 columns">'
												.cleanEncoding($a['description']).'		
											</div>
										</div>		
									</div>';
									
								break;								
																		
								case 'VIDEO':
								
									// show_array($a);
																					
									$blog_post_block .= 
									'<div class="blogPostVideo">
										 <div class="row">
											<div class="small-12 columns">
											
												<video controls style="width:100%;height:auto;" poster="'.$_SESSION['application']['root_media_host_url'].$a['video_poster_image_url'].'"> 
													<source src="'.$_SESSION['application']['root_media_host_url'].$a['host_url_mp4'].'" type="video/mp4">
													<source src="'.$_SESSION['application']['root_media_host_url'].$a['host_url_webm'].'" type="video/webm">
													Your browser does not support the video tag.
												</video>
												
												<div class="videoCaption">
													<i class="fa fa-play-circle fa-1x"></i>&nbsp;&nbsp;'.$a['title'].'&nbsp;&nbsp;<i class="fa fa-clock-o fa-1x"></i>&nbsp;&nbsp;'.$a['video_duration_minutes_seconds'].'	
												</div>   						
											
											</div>

										</div>		
									</div>';
									
								break;								 									
								
							}
																																	 		
						break;			
													
					} // end switch ($r['blog_post_type_code']) {
					
                        // echo 'Completed 1 loop through for a Content Asset : '.$a['id'].'<br />';
					
					} elseif ($a['category_name'] == "Blog Post Addendum") {
    					
    					// show_array($a);
    					// echo 'Addendum: '.$a['category_name'].'<br>';
    					
                        if (!$resource_addendum_block) {
            				// Create the Resource Addendum container as a stand alone.
            				$resource_addendum_block .= '
            					<!--googleoff: index-->
            				    <div class="panel callout radius">
            				        <div class="small-12 columns">
            				            <h3><i class="fa fa-medkit"></i>  Related Wellness Resources</h3>
            				            <p>We encourage you to <b>take advantage of these FREE Wellness Resources</b> on our website.</p>
                                    </div>';
            			}
    					
    					$addendum_resource_image_url = '';
    					$addendum_resource_image_alt_text = '';
    					$addendum_resource_canonical_url = '';
    					
                        switch ($a['content_asset_type_code']) {
                            
                            case 'BLOGP':
						    
						        $addendum_resource_image_url = $a['addendum_blog_post_image_url'];
								$addendum_resource_image_alt_text = $a['addendum_blog_post_image_alt_text'];
								$addendum_resource_canonical_url = $a['blog_post_canonical_url'];								
									
                            break;
                            
                            case 'RECIPE':
                            
                                // show_array($a);
						    
						        $addendum_resource_image_url = $a['addendum_recipe_image_url'];
								$addendum_resource_image_alt_text = $a['addendum_recipe_image_alt_text'];
								$addendum_resource_canonical_url = $a['addendum_recipe_canonical_url'];						
									
                            break;

						    case 'RESO':
						    
						        $addendum_resource_image_url = $a['addendum_resource_image_url'];
								$addendum_resource_image_alt_text = $a['addendum_resource_image_alt_text'];
								$addendum_resource_canonical_url = $a['resource_canonical_url'];						
									
                            break;
                            
                        }							
    					    					
    					$resource_addendum_block .= '
    					    <hr>
    					    <div class="row">		
                                <div class="small-12 medium-2 columns text-center">
    					            <img class="th" src="'.$_SESSION['application']['root_media_host_url'].$addendum_resource_image_url.'" alt="'.$addendum_resource_image_alt_text.'" width="125px">
                                </div>
                                <div class="small-12 medium-10 columns colWrapMargin">
                                    <a href="'.$addendum_resource_canonical_url.'"><h4>'.$a['title'].'</a></h4>'.
                                    $a['summary_description'].'
                                </div>
                            </div>';
    					
					}
    					
										
				} // end while($a = mysqli_fetch_assoc($result_blog_post_content_asset)) { 
					
				mysqli_free_result($result_blog_post_content_asset);
				
				// echo 'Completed 1 loop through for Blog Post : '.$r['id'].'<br />';	
				
                if ($resource_addendum_block) { 
                	    
                	$resource_addendum_block .= '
                	    </div> <!-- Close Panel -->
                	    <!--googleon: index-->';
                }
								
				$blog_post_block .= 
							'</div> <!-- end <div class="blogPostBody">-->			
						</div>
					</div>		
				</div>';
				 
} // end while($r = mysqli_fetch_assoc($result_show_project_program_blog_post)) {
    
?>
  
<div class="row">		
    <div class="small-12 columns">
    	
    	<ul class="tabs" data-options="deep_linking:true; scroll_to_content:false;" data-tab>
          <li class="tab-title active"><a href="#content">Blog Post</a></li>
          <?php if ($resource_addendum_block) { 
          	echo '<li class="tab-title"><a href="#resourceAddendum">Wellness Resources</a></li>'; 
          } ?>
          <?php if ($product_addendum_block) { 
          	echo '<li class="tab-title"><a href="#productAddendum">Supplements</a></li>'; 
          } ?>
          <li class="tab-title"><a href="#discuss">Discussion Forum</a></li>
        </ul>
        
        <div class="tabs-content">
        
            <div class="content active" id="content">
                <?
                // Display Article resources if any
                if (!empty($blog_post_block)) {
                	 echo $blog_post_block;
                }
                ?>
            </div>
            
            <?php if ($resource_addendum_block) { 
                    echo '<div class="content" id="resourceAddendum">'.$resource_addendum_block.'</div>'; 
            } ?>
                
            <?php if ($product_addendum_block) { 
                 echo '<div class="content" id="productAddendum">'.$product_addendum_block.'</div>'; 
            } ?>
        
            <div class="content" id="discuss">
                <?php
	                
	                /*
                    if ($_SESSION['blog_post']['blog_post_discussion_forum'] && $_SESSION['blog_post']['blog_post_discussion_forum']) {
                    	include('../php/blog_post_discussion_forum.php');
                    }
                    */
                    

					// Determine if the Blog Post is allowed to use a Discussion Forum
					if ($_SESSION['blog_post']['discussion_forum']['forum'] == true) {	
						
						// show_session();
						
						?>	
						<div id="educationDiscussionForum">
							<!-- Stardardize the ids and css for all Forums -->
							<?php
								$_SESSION['forum_type'] = 'blog_post'; 
								include('../php/discussion_forum.php'); 
							?>
						</div>
						<?php		
					}
                    
                    
                ?>
            </div>
            
        </div>
            				
    </div>
</div>

<?
    
// if ($_SESSION['user']['user_id'] == 2) { show_array($_SESSION); }

// echo 'Discussion Forum: '.$discussion_forum.'<br />';
		
mysqli_free_result($result_blog_post);

?>