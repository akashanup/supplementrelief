<?php

include_once('../includes/core.php');

if ($_SESSION['user']['user_id'] == 2) {
}

if (isset($_GET['blog_id'])) { 
    $blog_id = $_GET['blog_id']; 
} else {
    $blog_id = public_blog_id;
}

$queryBlog = 'SELECT 
	ca.title, 
	ca.summary_description, 
	ca.description, 
	ca.author, 
	im.host_url, 
	im.caption, 
	im.alt_text,
	bl.content_asset_id,  
	bl.discussion_forum, 
	bl.discussion_forum_help 
	FROM blogs bl
	LEFT JOIN content_assets ca on bl.content_asset_id = ca.id 
	LEFT JOIN images im ON bl.image_id = im.content_asset_id 
	AND im.size = "Blog" 	
	WHERE bl.content_asset_id = "'.$blog_id.'" 
	LIMIT 1';  
						
$result_blog = mysqli_query($connection, $queryBlog);

if (!$result_blog) {
	show_mysqli_error_message($queryBlog, $connection);
	die;
}

if (mysqli_num_rows($result_blog) > 0) {
    
	$icon_color = 'aqua';
	
	while($r = mysqli_fetch_assoc($result_blog)) {
    	
    	$_SESSION['blog']['blog_id'] = $blog_id;
    	$_SESSION['blog']['blog_title'] = $r['title'];
    	$_SESSION['blog']['blog_discussion_forum'] = $r['discussion_forum'];
    	                
        if ( (isset($_GET['keyword']) && !empty($_GET['keyword'])) OR isset($_GET['series']) OR isset($_GET['cooking']) OR isset($_GET['detoxification']) OR isset($_GET['disease']) OR isset($_GET['encouragement']) OR isset($_GET['exercise']) OR isset($_GET['family']) OR isset($_GET['financial']) OR isset($_GET['hydration']) OR isset($_GET['motivation']) OR isset($_GET['nutrition']) OR isset($_GET['personal_growth']) OR isset($_GET['sleep']) OR isset($_GET['spiritual']) OR isset($_GET['stress']) OR isset($_GET['supplements']) OR isset($_GET['tradition']) OR isset($_GET['weight_loss']) )  { 
	        
	    } else {
	    if(!(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'))
			{
		?>	
															
		<div class="blog">	
			<div class="row blogContainer">						
				<div class="small-12 columns">		
					<div class="blogSummary">	
															
						<div class="row">
														
							<div class="small-12 medium-4 columns">											
								<div id="blogSummaryLogo">
									<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip tip-right" title="<?php echo $r['caption']; ?>"><img src="<?php echo $_SESSION['application']['root_media_host_url'].$r['host_url']; ?>" alt="<?php echo $r['alt_text']; ?>" class="th" />
									</span>
								</div>
								<div>
    								<center><span class="caption"><?php echo $r['caption']; ?></span></center>
								</div>					
							</div>
												
							<div class="small-12 medium-8 columns">
											
								<div id="blogSummaryTitle">
							  		<h1><i class="fa fa-comment icon-<?php echo $icon_color; ?>"></i>&nbsp;&nbsp;<?php echo $r['title']; ?></h1>
								</div>
							  	
								<div id="blogSummaryDescription">	
								  	<p><?php echo $r['summary_description']; ?></p>
								</div>
																		
							</div>
						
						</div>	
		
					</div>														
				</div>
			</div>
		</div>
		
		<?php
			}
		}															
			
	}
	
}

$icon_color = 'red';
$blog_post_comment_counter_block = null;
$blog_posts_block = '';


function pagination($blog_id, $page, $last_prev, $last_page ,$limit, $keyword_href, $filter_href, $orderby_href, $json = '')
{
	$url = '';
	if ($last_page > 6)
	{
		if ($page == 1)
		{
			$next  = $page + 1;
			$next1 = $page + 2;
			$next2 = $page + 3;
			$url = "<ul class='pagination'>"."<li class='arrow unavailable'><a href=''>&laquo;</a></li>".
			"<li class='current'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$page."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$page."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$next."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$next."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$next1."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$next1."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$next2."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$next2."</a></li>".
			"<li class='unavailable'><a href=''>&hellip;</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$last_prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$last_prev."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$last_page."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$last_page."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$next."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>"."&raquo;"."</a></li></ul>";
		}
		else if ($page == $last_page)
		{
			$first = 1;
			$first1 = $first + 1;
			$first2 = $first + 2;
			$first3 = $first + 3;
			$url= "<ul class='pagination'>".
			"<li class='arrow'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$last_prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>"."&laquo;</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$first."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$first."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$first1."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$first1."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$first2."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$first2."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$first3."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$first3."</a></li>".
			"<li class='unavailable'><a href=''>&hellip;</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$last_prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$last_prev."</a></li>".
			"<li class='current'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$last_page."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$last_page."</a></li>".
			"<li class='arrow unavailable'><a href='' >"."&raquo;"."</a></li></ul>";
		}
		else if ($page == 2)
		{
			$next = $page + 1;
			$prev = $page - 1;
			$next1 = $page + 2;
			$url = "<ul class='pagination'>".
			"<li class='arrow'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>"."&laquo;</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$prev."</a></li>".
			"<li class='current'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$page."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$page."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$next."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$next."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$next1."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$next1."</a></li>".
			"<li><a href=''>&hellip;</a></li>"."<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$last_prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$last_prev."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$last_page."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$last_page."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$next."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>"."&raquo;"."</a></li></ul>";
		}
		elseif ($page > 2 && $page < ($last_prev - 1))
		{
			$next = $page + 1;
			$prev = $page - 1;
			$first = 1;
			$url = "<ul class='pagination'>".
			"<li class='arrow'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>"."&laquo;</a></l>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$first."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$first."</a></li>".
			"<li class='unavailable'><a href=''>&hellip;</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$prev."</a></li>".
			"<li class='current'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$page."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$page."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$next."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$next."</a></li>".
			"<li><a href=''>&hellip;</a></li>"."<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$last_prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$last_prev."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$last_page."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$last_page."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$next."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>"."&raquo;"."</a></li></ul>";

		}
		else if ($page == $last_prev)
		{
			$first = 1;
			$first1 = $first + 1;
			$first2 = $first + 2;
			$first3 = $first + 3;
			$prev = $page - 1;
			$url = "<ul class='pagination'>".
			"<li class='arrow'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>"."&laquo;</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$first."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$first."</a></li>".
			"<li class='unavailable'><a href=''>&hellip;</a></li>"."<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$prev."</a></li>".
			"<li class='current'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$last_prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$last_prev."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$last_page."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$last_page."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$last_page."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>"."&raquo;"."</a></li></ul>";
		}
		else if ($page == ($last_prev - 1))
		{
			$first = 1;
			$first1 = $first + 1;
			$first2 = $first + 2;
			$first3 = $first + 3;
			$prev = $page - 1;
			$next = $page + 1;
			$url= "<ul class='pagination'>".
			"<li class='arrow'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>"."&laquo;</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$first."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$first."</a></li>".
			"<li class='unavailable'><a href=''>&hellip;</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$prev."</a></li>".
			"<li class='current'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$page."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$page."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$next."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$next."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$last_page."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$last_page."</a></li>".
			"<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$next."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>"."&raquo;"."</a></li></ul>";
		}
		
	}
	elseif($limit == 'all')
	{
		$url="<ul class='pagination'>".
			"<li class='arrow unavailable'><a href=''>&laquo;</a></li>".
			"<li class='current'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$page."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$page."</a></li>".
			"<li class='arrow unavailable'><a href=''>&laquo;</a></li>";
	}
	
	else
	{
		$prev = $page - 1;
		$next = $page + 1;
		if ($page == 1)
		{
			$url="<ul class='pagination'>".
			"<li class='arrow unavailable'><a href=''>&laquo;</a></li>";
		}
		else
		{
			$url="<ul class='pagination'>".
			"<li class='arrow'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$prev."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>"."&laquo;</a></li>";
		}
		for ($i=1 ; $i <= $last_page ; $i++)
		{
			if ($i == $page)
			{
				$url.="<li class='current'><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$page."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$page."</a></li>";
			}
			else
			{
				$url.="<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$i."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>".$i."</a></li>";
			}
		}
		
		if ($page == $last_page)
		{
			$url.="<li class='arrow unavailable'><a href='' >"."&raquo;"."</a></li></ul>";
		}
		else
		{
			$url.="<li><a data-form='blogs' class='paginationAnchor' data-url='"."blog_id=".$blog_id."&page=".$next."&limitby=".$limit.$keyword_href.$filter_href.$orderby_href."'>"."&raquo;"."</a></li></ul>";
		}
	}

	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
	{	$json['pagination'] 	=	$url;
		$json 					=	json_encode($json);
		die($json);
	}
	else
	{
		return $url;
	}
}


?>

<?php
	if ($_SESSION['user']['user_id'] == 2 ) {
	}
	
	if (isset($_GET['series'])) { 
		$series = safe_sql_data($connection, $_GET['series']);
		$series_href="&series=".$series;		
		$query_blog_post_series .= ' AND bp.series = 1 ';	
	}
	
	$category_count = 0;
	
	if ( isset($_GET['cooking']) OR isset($_GET['detoxification']) OR isset($_GET['disease']) OR isset($_GET['encouragement']) OR isset($_GET['exercise']) OR isset($_GET['family']) OR isset($_GET['financial']) OR isset($_GET['hydration']) OR isset($_GET['motivation']) OR isset($_GET['nutrition']) OR isset($_GET['personal_growth']) OR isset($_GET['sleep']) OR isset($_GET['spiritual']) OR isset($_GET['stress']) OR isset($_GET['supplements']) OR isset($_GET['tradition']) OR isset($_GET['weight_loss']) )  { 
										
		if (isset($_GET['cooking'])) { 
			$cooking = safe_sql_data($connection, $_GET['cooking']);
			$cooking_href="&cooking=".$cooking;	
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Cooking"';	
		}

		if (isset($_GET['detoxification'])) { 
			$detoxification = safe_sql_data($connection, $_GET['detoxification']);
			$detoxification_href="&detoxification=".$detoxification;	
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Detoxification"';	
		}
		
		if (isset($_GET['disease'])) {
			$disease = safe_sql_data($connection, $_GET['disease']);
			$disease_href="&disease=".$disease;			
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Disease"';				
		}
		
		if (isset($_GET['encouragement'])) {
			$encouragement = safe_sql_data($connection, $_GET['encouragement']);
			$encouragement_href="&encouragement=".$encouragement;			
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Encouragement"';				
		}
		
		if (isset($_GET['exercise'])) {
			$exercise = safe_sql_data($connection, $_GET['exercise']);
			$exercise_href="&exercise=".$exercise;			
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Exercise"';				
		}
		
		if (isset($_GET['family'])) {
			$family = safe_sql_data($connection, $_GET['family']);
			$family_href="&family=".$family;
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Family"';			
		}
		
		if (isset($_GET['financial'])) {
			$financial = safe_sql_data($connection, $_GET['financial']);
			$financial_href="&financial=".$financial;
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Financial"';			
		}

		if (isset($_GET['hydration'])) {
			$hydration = safe_sql_data($connection, $_GET['hydration']);
			$hydration_href="&hydration=".$hydration;			
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Hydration"';			
		}
		
		if (isset($_GET['motivation'])) {
			$motivation = safe_sql_data($connection, $_GET['motivation']);
			$motivation_href="&motivation=".$motivation;			
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Motivation"';			
		}

		if (isset($_GET['nutrition'])) {
			$nutrition = safe_sql_data($connection, $_GET['nutrition']);
			$nutrition_href="&nutrition=".$nutrition;			
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Nutrition"';			
		}
		
		if (isset($_GET['personal_growth'])) {
			$personal_growth = safe_sql_data($connection, $_GET['personal_growth']);
			$personal_growth_href="&personal_growth=".$personal_growth;			
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Personal Growth"';			
		} 
		
		if (isset($_GET['sleep'])) {
			$sleep = safe_sql_data($connection, $_GET['sleep']);
			$sleep_href="&sleep=".$sleep;				
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Sleep"';			
		}
		
		if (isset($_GET['spiritual'])) {
			$spiritual = safe_sql_data($connection, $_GET['spiritual']);
			$spiritual_href="&spiritual=".$spiritual;			
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Spiritual"';			
		}
		
		if (isset($_GET['stress'])) {
			$stress = safe_sql_data($connection, $_GET['stress']);
			$stress_href="&stress=".$stress;			
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Stress"';			
		} 
		
		if (isset($_GET['supplements'])) {
			$supplements = safe_sql_data($connection, $_GET['supplements']);
			$supplements_href="&supplements=".$supplements;			
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Supplements"';			
		}
		
		if (isset($_GET['tradition'])) {
			$tradition = safe_sql_data($connection, $_GET['tradition']);
			$tradition_href="&tradition=".$tradition;				
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Tradition"';			
		} 
		
		if (isset($_GET['weight_loss'])) {
			$weight_loss = safe_sql_data($connection, $_GET['weight_loss']);
			$weight_loss_href="&weight_loss=".$weight_loss;			
			$category_count ++;
			if ($category_count == 1) {$separator .= '';} else {$separator = ', ';}			
			$query_category_list .= $separator.'"Weight Loss"';			
		}
		
		$query_append_category .= ' AND ca.id IN (
		SELECT distinct(pcacu.content_asset_id)  
		FROM project_content_asset_category_usages pcacu 
		INNER JOIN project_category_usages pcu ON pcacu.project_category_usage_id = pcu.id 

		WHERE pcacu.project_category_usage_id IN 

		( SELECT pcu.id 
		FROM project_category_usages pcu 
		WHERE pcu.category_usage_id IN 
		
		( SELECT cu.id  
		FROM category_usages cu  
		INNER JOIN categories catc ON cu.child_code = catc.code 
		AND catc.name IN ('.$query_category_list.') ) ) ) ';
		
	}

	if (isset($_GET['keyword']))
	{
		$keyword = safe_sql_data($connection, $_GET['keyword']);
		$keyword_href="&keyword=".$keyword;	
	}
	else
	{
		$keyword_href='';
	}
	
	$filter_href = $series_href.$cooking_href.$detoxification_href.$disease_href.$encouragement_href.$exercise_href.$family_href.$financial_href.$hydration_href.$motivation_href.$nutrition_href.$personal_growth_href.$sleep_href.$spiritual_href.$stress_href.$supplements_href.$tradition_href.$weight_loss_href; 	

	if (isset($_GET['orderby']) && !empty($_GET['orderby']))
	{
		$orderby= safe_sql_data($connection, $_GET['orderby']);
		$orderby_href="&orderby=".$orderby;
	}
	else
	{
		$orderby = 'newest_post';
		$orderby_href="&orderby=".$orderby;
	}
				
	if (isset ($_GET['limitby']) && !empty($_GET['limitby']))
	{
		if (is_numeric($_GET['limitby']))
		{
			$limit = safe_sql_data($connection, abs(intval($_GET['limitby'])));
		}
		elseif (safe_data($_GET['limitby']) == 'all')
		{
			$limit = 'all';
		}
		else
		{
			$limit = 20;
		}
	}
	else
	{	
		$limit = 20;
	}
				
	if (isset($_GET['page']) && $_GET['page'] != 0)
	{
		$page = safe_sql_data($connection, $_GET['page']);
		$start = ($page-1) * $limit;
	}
	else
	{
		$start=0;
		$page=1;
	}
			
	$current_timestamp = date('Y-m-d H:i:s');
	
	$query = 'SELECT 
		cau.id, 
		cau.content_asset_child_id, 
		cau.seq, 
		ca.title, 
		ca.summary_description,  
		ca.text, 
		ca.author, 
		bp.series AS blog_post_series,  
		bp.discussion_forum, 
		bp.post_effective_timestamp,  
		bp.post_end_timestamp, 
		bp.summary_text, 
		bpt.name as blog_post_type_name, 
		bpt.icon_snippet, 
		im.host_url AS blog_post_image_url, 
		im.caption AS blog_post_image_caption, 
		im.alt_text AS blog_post_image_alt_text, 
		urls.url AS seo_url    
		FROM content_asset_usages cau  
		LEFT JOIN content_assets ca on cau.content_asset_child_id = ca.id 
		LEFT JOIN blog_posts bp ON ca.id = bp.content_asset_id 
		LEFT JOIN blog_post_types bpt ON bp.blog_post_type_code = bpt.code 
		LEFT JOIN images im ON 
			(bp.image_id = im.content_asset_id AND im.size = "Blog Post" AND im.usage_size = "Small" AND im.format = "JPG")
        LEFT JOIN url_usages urlu 
            ON (cau.content_asset_child_id = urlu.blog_post_id AND 
                urlu.effective_date <= CURRENT_DATE AND
               (urlu.end_date IS NULL or urlu.end_date >= CURRENT_DATE))
        LEFT JOIN urls urls ON urlu.url_id = urls.id 	 
			WHERE cau.content_asset_parent_id = "'.$blog_id.'"  
		AND bp.post_effective_timestamp <= "'.$current_timestamp.'" 
		AND (bp.post_end_timestamp IS NULL or bp.post_end_timestamp > "'.$current_timestamp.'") '; 

	if ($keyword)
	{
		$query.= " AND (title LIKE '%".$keyword."%' OR summary_description LIKE '%".$keyword."%' OR author LIKE '%".$keyword."%' OR search_keywords LIKE '%".$keyword."%')";
	} 
	
	if ($query_blog_post_series)
	{
		$query.= $query_blog_post_series;
	} 

	if ($query_append_category)
	{
		$query.= $query_append_category;
	} 
			
	$query.= ' GROUP BY ca.id ';  	

	if ($orderby == 'newest_posts')
	{
		$query.= " ORDER BY bp.post_effective_timestamp DESC";
	}
	elseif ($orderby == 'oldest_posts')
	{
		$query.= " ORDER BY bp.post_effective_timestamp ASC";
	}
	elseif ($orderby == 'title')
	{
		$query.= " ORDER BY ca.title ASC";
	}
	else		{
		$query.= " ORDER BY bp.post_effective_timestamp DESC";
	}			
							
	if ($limit == 'all')
	{
		$query.= '';
	}
	else
	{
		$query.=' LIMIT ' . $start . ',' . $limit;
	}
			
	$query_totalpage = 'SELECT 
		cau.id 
		FROM content_asset_usages cau  
		LEFT JOIN content_assets ca on cau.content_asset_child_id = ca.id 
		LEFT JOIN blog_posts bp ON ca.id = bp.content_asset_id 
		LEFT JOIN blog_post_types bpt ON bp.blog_post_type_code = bpt.code 
		LEFT JOIN images im ON 
			(bp.image_id = im.content_asset_id AND im.size = "Blog Post" AND im.usage_size = "Small" AND im.format = "JPG") 	
		WHERE cau.content_asset_parent_id = "'.$blog_id.'"  
		AND bp.post_effective_timestamp <= "'.$current_timestamp.'" 
		AND (bp.post_end_timestamp IS NULL or bp.post_end_timestamp > "'.$current_timestamp.'")';  	
							
	if ($keyword)
	{
		$query_totalpage.= " AND (title LIKE '%".$keyword."%' OR summary_description LIKE '%".$keyword."%' OR author LIKE '%".$keyword."%' OR search_keywords LIKE '%".$keyword."%')";
	}
	
	if ($query_blog_post_series)
	{
		$query_totalpage.= $query_blog_post_series;
	} 

	if ($query_append_category)
	{
		$query_totalpage.= $query_append_category;
	} 
	
	$query_totalpage.= ' GROUP BY ca.id ';  	

	$result_blog_post = mysqli_query($connection, $query);
	
    if (!$result_blog_post) {
		show_mysqli_error_message($query, $connection);
		die;				
	}

	$result_total_page = mysqli_query($connection, $query_totalpage);
	
    if (!$result_total_page) {
		show_mysqli_error_message($query_totalpage, $connection);
		die;				
	}

	$total_records = mysqli_num_rows($result_total_page);
    
    if ($limit > 0)
	{
		$last_page = ceil($total_records/$limit);
	}
	else
	{
		$last_page = 0;
	}
	
	$last_prev = $last_page-1;																				
?>


<?php
	if(!(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'))
	{
?>
	<div class="row">
		<div class="small-12 columns"> 
			<div class="row">
				<form name="search-blog-posts-form" id="search-blog-posts-form">
	    			
	    			<input type="hidden" name="blog_id" value="<?php echo ($blog_id ? $blog_id : ''); ?>" />
	    			
	    			<fieldset>
		    			<legend>Search Blog Posts</legend>
							
					<?php
					
						if ($keyword == '' && $series == '' && $cooking == '' && $detoxification == '' && $disease == '' && $encouragement == '' && $exercise == '' && $family == '' && $financial == '' && $hydration == '' && $motivation == '' && $nutrition == '' && $personal_growth == '' && $sleep == '' && $spiritual == '' && $stress == '' && $supplements == '' && $tradition == '' && $weight_loss == '') {
							?>			
							<div class="medium-6 columns">
												
								<div class="row collapse">
								
								    <div class="small-10 columns">
								      <input type="text" placeholder="Search by keyword" data-function="blogs" name="keyword" id="search" value="<?php if (isset($keyword)) { echo $keyword; } else { echo ''; } ?>">
								    </div>
								    
								    <div class="small-2 columns">
								      <input class="button tiny radius" id="searchBlogsBtn" type="button" value="GO" />
								    </div>
								  
								 </div>					
							</div>		
							<?php
							
						} else {
						
							?>							
							<div class="medium-6 columns">
							
								<div class="row">
												
									<div class="small-7 columns">
									  <input type="text" placeholder="Search by keyword" data-function="blogs" name="keyword" id="search" value="<?php if (isset($keyword)) { echo $keyword; } else { echo ''; } ?>">
									</div>
									
									<div class="small-5 columns">
									  <input class="button tiny radius" id="searchBlogsBtn" type="button" value="GO" />&nbsp;<a class="button tiny radius" href="<?php echo "?blog_id=".$blog_id."&keyword="."&limitby=".$limit.$orderby_href; ?>" ><font size="2">Show All</font></a>
									 
									</div>
								</div>		
							</div> 												
							<?php 
						}
					?>
					
					<div class="medium-4 columns">
										
						<div class="row collapse">
						
							<div class="small-4 columns">
							  <span class="postfix">Order By</span>
							</div>
						
					    <div class="small-8 columns">
					      <select id="orderby" name='orderby' data-function="blogs">
					        <option <?php if (isset($orderby) && $orderby == 'newest_posts'): ?>selected="selected"<?php endif ?> value="newest_posts">Recent Posts</option>
					        <option <?php if (isset($orderby) && $orderby == 'oldest_posts'): ?>selected="selected"<?php endif ?> value="oldest_posts">Older Posts</option>
					        <option <?php if (isset($orderby) && $orderby == 'title'): ?>selected="selected"<?php endif ?> value="title">Title</option>        
					      </select>
					    </div>						  
						</div>										
					</div> 
					<div class="medium-2 columns">
					
						<div class="row collapse">
						
							<div class="small-5 columns">
							  <span class="postfix">View</span>
							</div>
						
							<div class="small-7 columns">
								<select id="limitby" name='limitby' data-function="blogs">
								  <option <?php if (isset($limit) && $limit == 10): ?>selected="selected"<?php endif ?> value="10">10</option>
								  <option <?php if (isset($limit) && $limit == 20): ?>selected="selected"<?php endif ?> value="20">20</option>
								  <option <?php if (isset($limit) && $limit == 40): ?>selected="selected"<?php endif ?> value="40">40</option>
								  <option <?php if (isset($limit) && $limit == 60): ?>selected="selected"<?php endif ?> value="60">60</option>
								  <option <?php if (isset($limit) && $limit == 100): ?>selected="selected"<?php endif ?>  value="100">100</option>
								  <option <?php if (isset($limit) && $limit == 'all'): ?>selected="selected"<?php endif ?>  value="all">ALL</option>
								</select>
						  	</div>					  
						</div>												
					</div> 
		    						
	     			<div class="small-12 columns">
		     			
		     			<fieldset>
	    					<legend>Filter by Categories</legend>
	     			
			     			<input type="checkbox" name="series" value="1" <?php if ($series ==1) { echo 'checked'; } ?>>&nbsp;
				 			<a title="View a series of posts that contain two or more related posts."><span class="label success">Blog Post Series</span>&nbsp;&nbsp;</a>
		                	<input type="checkbox" name="cooking" value="1" <?php if ($cooking ==1) { echo 'checked'; } ?>> Cooking&nbsp;&nbsp;
		                	<input type="checkbox" name="detoxification" value="1" <?php if ($detoxification ==1) { echo 'checked'; } ?>> Detoxification&nbsp;&nbsp;
							<input type="checkbox" name="disease" value="1" <?php if ($disease ==1) { echo 'checked'; } ?>> Disease&nbsp;&nbsp;
							<input type="checkbox" name="encouragement" value="1" <?php if ($encouragement ==1) { echo 'checked'; } ?>> Encouragement&nbsp;&nbsp;
							<input type="checkbox" name="exercise" value="1" <?php if ($exercise ==1) { echo 'checked'; } ?>> Exercise&nbsp;&nbsp;
							<input type="checkbox" name="family" value="1" <?php if ($family ==1) { echo 'checked'; } ?>> Family/Relationships&nbsp;&nbsp;
							<input type="checkbox" name="financial" value="1" <?php if ($financial ==1) { echo 'checked'; } ?>> Financial&nbsp;&nbsp;
							<input type="checkbox" name="hydration" value="1" <?php if ($hydration ==1) { echo 'checked'; } ?>> Hydration&nbsp;&nbsp;
							<input type="checkbox" name="motivation" value="1" <?php if ($motivation ==1) { echo 'checked'; } ?>> Motivation&nbsp;&nbsp;
							<input type="checkbox" name="nutrition" value="1" <?php if ($nutrition ==1) { echo 'checked'; } ?>> Nutrition&nbsp;&nbsp;
							<input type="checkbox" name="personal_growth" value="1" <?php if ($personal_growth ==1) { echo 'checked'; } ?>> Personal Growth&nbsp;&nbsp;
							<input type="checkbox" name="sleep" value="1" <?php if ($sleep ==1) { echo 'checked'; } ?>> Sleep&nbsp;&nbsp;
							<input type="checkbox" name="spiritual" value="1" <?php if ($spiritual ==1) { echo 'checked'; } ?>> Spiritual&nbsp;&nbsp;
							<input type="checkbox" name="stress" value="1" <?php if ($stress ==1) { echo 'checked'; } ?>> Stress&nbsp;&nbsp;
							<input type="checkbox" name="supplements" value="1" <?php if ($supplements ==1) { echo 'checked'; } ?>> Supplements&nbsp;&nbsp;
							<input type="checkbox" name="tradition" value="1" <?php if ($tradition ==1) { echo 'checked'; } ?>> Tradition&nbsp;&nbsp;
							<input type="checkbox" name="weight_loss" value="1" <?php if ($weight_loss ==1) { echo 'checked'; } ?>> Weight Loss&nbsp;&nbsp;	
						
						</fieldset>
	                	
	               	</div>
					
					</fieldset>			
					
				</form>
			</div> 

			<div class="row">
				<div class="small-12 medium-2 columns">
					<div id="noOfPosts">&nbsp;<i class="fa fa-binoculars"></i>&nbsp;&nbsp;<?php echo $total_records; ?>&nbsp;Posts</div>			
				</div>
				
				<div class="small-12 medium-10 columns">
									
					<div id="pagination-setup">
					<?php
						$url =	pagination($blog_id, $page, $last_prev, $last_page ,$limit, $keyword_href, $filter_href, $orderby_href);
						echo $url;
					?>
					</div>
					
				</div>
			</div> 								
		</div> 	
	</div> 
<?php
	}
?>

<?php

	if ($total_records == 0) {
		$blog_posts_block .=
			'<div class="row">
				<div class="small-12 columns">
					<h1>No Blog Posts found with your Search Criterion '.$keyword. '</h1>
				</div>
			</div>';
		 
	} else {

	   	while($r = mysqli_fetch_assoc($result_blog_post)) { 
	    	
	    	$blog_post_series_block = '';
	    	
	    	if ($r['blog_post_series']) {
				$blog_post_series_block .= '<span class="label success">Blog Post Series</span>&nbsp;&nbsp;';
			}

			$queryBlogPostCategories = 'SELECT 
			    catc.name AS child_category_name 
			    FROM project_content_asset_category_usages pcacu  
			    LEFT JOIN project_category_usages pcu ON pcacu.project_category_usage_id = pcu.id 
			    LEFT JOIN category_usages cu ON pcu.category_usage_id = cu.id 
			    LEFT JOIN categories catc ON cu.child_code = catc.code 
			    WHERE pcacu.content_asset_id = '.$r['content_asset_child_id'].' 
			    AND pcacu.effective_date <= CURRENT_DATE 
				AND (pcacu.end_date is NULL or pcacu.end_date >= CURRENT_DATE)  
			    ORDER BY child_category_name ASC';
			    
			$result_blog_post_category = mysqli_query($connection, $queryBlogPostCategories);
			
			if (!$result_blog_post_category) {
				show_mysqli_error_message($queryBlogPostCategories, $connection);
				die;
			}
			
			$blog_post_category_block = '';
			
			while($bpc = mysqli_fetch_assoc($result_blog_post_category)) {
				
				$blog_post_category_block .= '<span class="label">'.$bpc['child_category_name'].'</span>&nbsp;&nbsp;';
				
			}
			
			mysqli_free_result($result_blog_post_category);
		       		
	        if ($r['seo_url']) {
	            $blog_post_url = '../'.$r['seo_url'].'/';
	    	} else {	
	    		$blog_post_url = '../show_blog_post/?bp_id='.$r['content_asset_child_id'];
	    	}	
	    	        				
			if ($r['discussion_forum'] == true) {
			
				$queryCountBlogPostComments = 'SELECT id  
					FROM discussion_threads  
					WHERE blog_post_id = '.$r['content_asset_child_id'].'   
					AND status in ("S", "R") '; 			
							
				$result_list_count_blog_post_comment = mysqli_query($connection, $queryCountBlogPostComments);
				
				if (!$result_list_count_blog_post_comment) {
					show_mysqli_error_message($queryCountBlogPostComments, $connection);
					die;
				}
							
				if (mysqli_num_rows($result_list_count_blog_post_comment) > 0) {			
					$comment_count = mysqli_num_rows($result_list_count_blog_post_comment);
					$blog_post_comment_counter_block = '<i class="fa fa-comment icon-'.$icon_color.'"></i>&nbsp;&nbsp;'.$comment_count;					
				} else {
					$comment_count = 0;
					$blog_post_comment_counter_block = '';
				}
						
			}
			
			$blog_posts_block .= ' 
			
			<div class="blogPostContainer">
						
				<div class="row collapse">';
				
					if (strlen($r['blog_post_image_url']) > 0) {
						$blog_posts_block .= '
						<div class="small-12 medium-2 columns show-for-medium-up lazyDiv">
							<div class="blogPostSummaryImage">
								<img class="th lazy" src="../thumbnail.png" data-original="'.$_SESSION['application']['root_media_host_url'].$r['blog_post_image_url'].'" title="'.$r['blog_post_image_caption'].'" alt="'.$r['blog_post_image_alt_text'].'" />
								<noscript>
								    <img class="th" src="'.$_SESSION['application']['root_media_host_url'].$r['blog_post_image_url'].'" title="'.$r['blog_post_image_caption'].'" alt="'.$r['blog_post_image_alt_text'].'" />
								</noscript>					
							</div>			
						</div>';
					}
				
					if (strlen($r['blog_post_image_url']) > 0) {
						$blog_posts_block .= '<div class="small-12 medium-10 columns">';						
					} else {
						$blog_posts_block .= '<div class="small-12 columns">';	
					}
					
					$blog_posts_block .= '							
						<div class="blogPostSummary">
						
							 <div class="row">
								<div class="small-12 columns">
									<div class="blogPostTitle">
									        									
										<h3>'.$r['icon_snippet'].'&nbsp;&nbsp<a href="'.$blog_post_url.'" title="View the rest of the Blog Post">'.$r['title'].'</a></h3>																							
									</div>				
								</div>		
							 </div>
							 
							 <div class="row">
							 					 	
							 	<div class="small-12 columns">
							 		<div class="blogPostAuthorTimestamp">
							 			<i class="fa fa-user icon-'.$icon_color.'"></i>&nbsp;&nbsp;'.$r['author'].'&nbsp;&nbsp;
							 			<i class="fa fa-clock-o icon-'.$icon_color.'"></i>&nbsp;&nbsp;'.date('m/d/y', strtotime($r['post_effective_timestamp'])).'&nbsp;&nbsp;'.
							 			$blog_post_comment_counter_block.'&nbsp;&nbsp;
							 			<!-- <a href="../show_blog_post/?bp_id='.$r['content_asset_child_id'].'" title="View the rest of the Blog Post">View</a> -->
							 		</div>
							 	</div>										
							         						 									 
							 </div>
							 
							 <div class="row">
							 	<div class="small-12 columns blogPostSummaryDescription">'
							 		.cleanEncoding($r['summary_description']).'
							 		<div class="blogPostCategories">'
	                                	.$blog_post_series_block.$blog_post_category_block.'
									</div>		
							 	</div>		
							 </div>	 
							 
						</div>
														 		
					</div>';
										
				$blog_posts_block .= '				
				</div>		
			</div>';
			
			$blog_post_comment_counter_block = null;
									 
		}
		
	}


if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
	$json 					= 	[];
	$json['total_records'] 	= 	$total_records;
	$json['html'] 			=	$blog_posts_block;
	pagination($blog_id, $page, $last_prev, $last_page ,$limit, $keyword_href, $filter_href, $orderby_href, $json);
}

?>


<div id="blogsDiv">
	<?php
		if (strlen($blog_posts_block) > 0) {
			echo $blog_posts_block;
			echo '<br />';	
		}
	?>
</div>

<?php
	mysqli_free_result($result_blog);
	mysqli_free_result($result_blog_post);
	mysqli_free_result($result_list_count_blog_post_comment);
?>