<?php

// echo 'Begin show_blogs.php<br />';
// Calling Page: Menu
// Calling Script: menu.php
// http://supplementrelief.com/blog/

include_once('../includes/core.php');

$current_timestamp_detroit = date('Y-m-d H:i:s');
// echo 'Current Date and Time in Detroit : '.$current_timestamp_detroit.'<br /><hr />';

if (isset($_SESSION['enrollment']['project_program_id'])) { 
	
	$pp_id = $_SESSION['enrollment']['project_program_id'];
	
	// Get the Blogs for the Project Program Offering the User is currently viewing and display them on the Blog page.
	
	$queryBlog = 'SELECT 
		ppca.id, 
		ppca.project_program_id, 
		ppca.content_asset_id, 
		ppca.seq, 
		ppca.opt_in, 
		ppca.effective_date, 
		ppca.end_date, 
		ppca.scheduled_delivery_timestamp, 
		ppca.actual_delivery_timestamp, 
		ppca.scheduled_delivery_complete_timestamp, 	
		ca.title, 
		ca.summary_description, 
		ca.description, 
		ca.author, 
		bl.image_id, 
		im.host_url, 
		im.caption, 
		im.alt_text, 
        urls.url AS seo_url   
		FROM project_program_content_assets ppca 
		LEFT JOIN content_assets ca on ppca.content_asset_id = ca.id 
		LEFT JOIN blogs bl ON ca.id = bl.content_asset_id 
		LEFT JOIN images im ON bl.image_id = im.content_asset_id 
        LEFT JOIN url_usages urlu 
            ON (bl.content_asset_id = urlu.blog_id AND 
                urlu.effective_date <= CURRENT_DATE AND
               (urlu.end_date IS NULL or urlu.end_date >= CURRENT_DATE))
        LEFT JOIN urls urls ON urlu.url_id = urls.id 			
		AND im.size = "Blog" 
		WHERE ppca.project_program_id = "'.$pp_id.'" 
		AND ppca.scheduled_delivery_timestamp <= "'.$current_timestamp_detroit.'" 
		AND (scheduled_delivery_complete_timestamp IS NULL or scheduled_delivery_complete_timestamp >= "'.$current_timestamp_detroit.'")	
		AND ca.content_asset_type_code = "BLOG"  
		ORDER BY ppca.seq ASC';
							
	// echo $queryBlog . '<br /><hr />';
	 
} else {

	// Public user, not logged in
	// No Project Program has been selected so show the default Blog
	
	// Make this a Global Variable set in defines.php that is applicable to each installation.
	$default_blog_id = 1299; // Blog of the Nutritional Concierge is the default public Blog for SupplementRelief
	
	$queryBlog = 'SELECT 
		bl.content_asset_id, 
		bl.image_id, 
		ca.title, 
		ca.summary_description,  
		ca.description, 
		ca.author, 
		im.host_url, 
		im.caption, 
		im.alt_text, 
        urls.url AS seo_url   
		FROM blogs bl 
		LEFT JOIN content_assets ca on bl.content_asset_id = ca.id 
		LEFT JOIN images im ON bl.image_id = im.content_asset_id 
        LEFT JOIN url_usages urlu 
            ON (bl.content_asset_id = urlu.blog_id AND 
                urlu.effective_date <= CURRENT_DATE AND
               (urlu.end_date IS NULL or urlu.end_date >= CURRENT_DATE))
        LEFT JOIN urls urls ON urlu.url_id = urls.id 			
		AND im.size = "Blog" 
		WHERE bl.content_asset_id = "'.$default_blog_id.'"'; 
							
	// echo $queryBlog . '<br /><hr />';
	
}

$result_blog = mysqli_query($connection, $queryBlog);

if (!$result_blog) {
	show_mysqli_error_message($queryBlog, $connection);
	die;
}

// echo 'Database Query Show Project Program Blogs succeeded.<br /><hr />';

$total_blogs = mysqli_num_rows($result_blog);

if (mysqli_num_rows($result_blog) > 0) {

	$icon_color = 'aqua';
		
	while($r = mysqli_fetch_assoc($result_blog)) {
	
		$blog_id = $r['content_asset_id'];
		
        if ($r['seo_url']) {
            $blog_url = '../'.$r['seo_url'].'/';
    	} else {	
    		$blog_url = '../show_blog_posts/?blog_id='.$blog_id;
    	}			
		
		/*
		if ($total_blogs = 1) {
			$target_uri = '../show_blog_posts/?blog_id='.$blog_id;
			// echo 'Target URI: '.$target_uri.'<br /><hr />';
			// die;
			header("location: $target_uri");
			exit();		
		}
		*/
				
		// Count the Blog Posts for the Blog and display the number of Blog Posts in the Blog header.
				
		$queryBlogPosts = 'SELECT 
			cau.id  
			FROM content_asset_usages cau  
			LEFT JOIN content_assets ca on cau.content_asset_child_id = ca.id 
			LEFT JOIN blog_posts bp ON ca.id = bp.content_asset_id 
			LEFT JOIN blog_post_types bpt ON bp.blog_post_type_code = bpt.code 
			WHERE cau.content_asset_parent_id = "'.$blog_id.'"  
			AND bp.post_effective_timestamp <= "'.$current_timestamp_detroit.'" 
			AND (bp.post_end_timestamp IS NULL or bp.post_end_timestamp > "'.$current_timestamp_detroit.'")  	
			ORDER BY bp.post_effective_timestamp DESC';
										
		// echo '<br />'.$queryBlogPosts.'<br /><br />';
		// die();
		
		$result_blog_post = mysqli_query($connection, $queryBlogPosts);
		
		if (!$result_blog_post) {
			show_mysqli_error_message($queryBlogPosts, $connection);
			die;
		}
		
		$total_number_blog_posts = mysqli_num_rows($result_blog_post);
		// echo 'Total # Blog Posts: '.$total_number_blog_posts.'<br />';
		
		?>	
															
		<div class="blog">	
			<div class="row blogContainer">						
				<div class="small-12 columns">		
					<div class="blogSummary">	
															
						<div class="row">
																	
							<div class="small-12 medium-4 columns">											
								<div id="blogSummaryLogo">
									<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip tip-right" title="<?php echo 
									$r['caption']; ?>"><a href="<?php echo $blog_url; ?>"><img src="<?php echo $_SESSION['application']['root_media_host_url'].$r['host_url']; ?>" alt="" class="th" /></a>
									</span>
								</div>					
							</div>
						
							<div class="small-12 medium-8 columns">
											
								<div id="blogSummaryTitle">
                                    <h2><i class="fa fa-comment icon-<?php echo $icon_color; ?>"></i>&nbsp;&nbsp<?php echo $r['title']; ?></h2>
							    </div>
							  	
                                <div id="blogSummaryDescription">	
                                    <p><?php echo $r['summary_description']; ?></p>
				                </div>
								
								<div id="">	
									<p><a href="<?php echo $blog_url; ?>">View</a> <b><?php echo $total_number_blog_posts; ?></b> posts.</p>
								</div>
														
							</div>
						
						</div>	
		
					</div>														
				</div>
			</div>
		</div>
		
		<?php															
		
		/*	
		echo '
		<div class="blog">	
			<div class="row blogContainer">						
				<div class="small-12 columns">		
					<div class="blogSummary">	
				
						<div class="row">
									
							<div class="small-12 medium-4 columns">											
								<div id="blogSummaryLogo">
									<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip tip-right" title="'.
									$r['caption'].'"><a href="../show_blog_posts/?ppca_id='.$r['id'].'"><img src="'.$r['host_url'].'" alt="" class="th" /></a>
									</span>
								</div>					
							</div>
														
							<div class="small-12 medium-8 columns">				
								<div id="blogSummaryTitle">
							  	<h2><i class="fa fa-comment icon-'.$icon_color.'"></i>&nbsp;&nbsp;'.$r['title'].'</h2>
							  </div>
							  <div id="blogSummaryDescription">	
							  	<p>'.$r['text'].'</p>
							  	<a href="../show_blog_posts/?ppca_id='.$r['id'].'">View Blog Posts</a>
								</div>						
							</div>
						
						</div>	
											
					</div>				
				</div>			
			</div>																
		</div>';	
		
		*/			
			
	}
	
} else {

	echo '
	<br />
	<div class="row">		
		<div class="small-12 columns">											
			<div class="panel">
				<p>No Blogs currently available for the <b>'.$_SESSION['project_program_name'].'</b> program.</p>
			</div>
		</div>
	</div>';	
}

mysqli_free_result($result_blog);
mysqli_free_result($result_blog_post);

?>