<?php

// echo 'Begin show_calendar.php<br />';

show_array($_SESSION);

$_SESSION['project_global_calendar'] = 1;

if (isset($_SESSION['project_global_calendar']) && $_SESSION['project_global_calendar'] == 1) {
	// echo 'project_global_calendar: '.$_SESSION['project_global_calendar'];		
	$project_global_calendar = ' OR ce.calendar_id = "1';
	// echo $project_global_calendar;
	// die;
}

if (isset($_SESSION['project_calendar_id'])) {
	// echo 'project_calendar_id: '.$_SESSION['project_calendar_id'];		
	$project_calendar_id = $_SESSION['project_calendar_id']; 
}

if (isset($_SESSION['project_program_calendar_id'])) {
	// echo 'project_program_calendar_id: '.$_SESSION['project_program_calendar_id'];		
	$project_program_calendar_id = $_SESSION['project_program_calendar_id']; 
}

// show_array($_GET);
if (isset($_GET['id'])) { 
	$month_id = $_GET['id']; 
} else {
	date_default_timezone_set('America/Detroit');
	$month_id = date('Ym');
}

?>

<div class="row calendar">
  <div class="small-12 columns">
  
  	<div id=""><center><h2>2015 Events Calendar</h2></center></div>
  
  	<div class="pagination-centered">
  	  <ul class="pagination">
  	    <li class="arrow unavailable"><a href="">&laquo;</a></li>
  	    <li <?php if ($month_id == 201506) { echo 'class="current"'; } ?>><a href="../calendar?id=201506">June</a></li>
  	    <li <?php if ($month_id == 201507) { echo 'class="current"'; } ?>><a href="../calendar?id=201507">July</a></li>
  	    <li <?php if ($month_id == 201508) { echo 'class="current"'; } ?>><a href="../calendar?id=201508">August</a></li>
  	    <li <?php if ($month_id == 201509) { echo 'class="current"'; } ?>><a href="../calendar?id=201509">September</a></li>
  	    <!-- <li class="unavailable"><a href="">&hellip;</a></li> -->
  	    <li <?php if ($month_id == 201510) { echo 'class="current"'; } ?>><a href="../calendar?id=201510">October</a></li>
  	    <li <?php if ($month_id == 201511) { echo 'class="current"'; } ?>><a href="../calendar?id=201511">November</a></li>
  	    <li <?php if ($month_id == 201512) { echo 'class="current"'; } ?>><a href="../calendar?id=201512">December</a></li>	    
  	    <li class="arrow"><a href="">&raquo;</a></li>
  	  </ul>
  	</div>
  
    <table class="calendar">
    
      <thead>
        <tr>
          <th width="300">Sun</th>
          <th width="300">Mon</th>
          <th width="300">Tue</th>
          <th width="300">Wed</th>
          <th width="300">Thu</th>
          <th width="300">Fri</th>
          <th width="300">Sat</th>
        </tr>
      </thead>
      
      <?php
      
      if ($month_id == 201506) {
      
      	// Query the Events for the Global, Project and Project Program Calendar(s).
      	
      	$queryCalendarEvents = '
      	SELECT   
      	ev.id AS event_id,
      	ev.event_color,  
      	ev.name AS event_name, 
      	ev.description, 
      	ev.url, 
      	ev.effective_timestamp,
      	ev.end_timestamp, 
      	evt.name AS event_type_name  
      	FROM calendar_events ce 
      	LEFT JOIN calendars ca ON ce.calendar_id = ca.id     	
      	LEFT JOIN events ev ON ce.event_id = ev.id  
      	LEFT JOIN event_types evt ON ev.event_type_code = evt.code  
      	WHERE (ce.calendar_id = "'.$project_program_calendar_id.'" OR ce.calendar_id = "'.$project_calendar_id.'"'.$project_global_calendar.'")       	
      	AND ev.effective_timestamp >= "2015-06-01 00:00:00" AND (ev.end_timestamp is NULL or ev.end_timestamp <= "2015-06-30 23:59:59") 
      	ORDER BY ev.effective_timestamp ASC';
      	
      	// echo $queryCalendarEvents;
      	// die();
      		
      	// Execute the query
      	$result_calendar_event = mysqli_query($connection, $queryCalendarEvents);
      	
      	if (!$result_calendar_event) {
      		show_mysqli_error_message($queryCalendarEvents, $connection);
      		die;
      	}
      	
      	while($r = mysqli_fetch_array($result_calendar_event)) {
      	
      		// show_array($r);
      		
      		$a = date_parse($r['effective_timestamp']);
      		// show_array($a);
      		$effective_year = $a['year'];
      		$effective_month = $a['month'];
      		$effective_day = $a['day'];
      		$effective_hour = str_pad($a['hour'], 2, "0", STR_PAD_LEFT);
      		$effective_minute = str_pad($a['minute'], 2, "0", STR_PAD_LEFT);
      		$effective_second = str_pad($a['second'], 2, "0", STR_PAD_LEFT);
      		     		      	      		
      		// $display_effective_timestamp = $effective_month.'/'.$effective_day.'/'.$effective_year.' '.$effective_hour.':'.$effective_minute.'<br />';
      		$display_effective_timestamp = date('m/d/y h:i A T', strtotime($r['effective_timestamp']));
      		$start_time = substr($display_effective_timestamp, 9, 8);
      		// echo $start_time.'<br />';
      		if (strlen($r['end_timestamp']) > 0) {
      			$display_end_timestamp = date('m/d/y h:i A T', strtotime($r['end_timestamp']));
      		} else {
      			$display_end_timestamp = '';
      		}
      		
      		// echo 'Effective Day: '.$effective_day.'<br />';
      		
      		if ($effective_day == 15) {
      		
      			// .$effective_hour.':'.$effective_minute.'&nbsp;
      			
      			// .calendar .event.yellow {
      			 // background: #f9c846;
      		//	}
      		// style="background-color:lightgrey"
      			// <span class="event yellow ">'
      			
      			     		
      			$day15 .= '
      			<span class="event" style="background-color:#1FB8B5">'
      				.$start_time.'<br />
      			 	<a href="#" data-reveal-id="modal-'.$month_id.'-'.$r['event_id'].'">'
      					.$r['event_name'].'
      				</a>
      			</span>
      			<div id="modal-'.$month_id.'-'.$r['event_id'].'" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
      			  <h2 id="modalTitle">'.$r['event_name'].'</h2>
      			  <p class="lead">Event Type: '.$r['event_type_name'].'</p>
      			  <p>Begin: '.$display_effective_timestamp.'<br />End: '.$display_end_timestamp.'</p>';
      			  
      			  
	      			if (strlen($r['url']) > 0) {
	      				$day15 .= '<p>URL: '.$r['url'].'</p>';
	      			}
	      			
	      			if (strlen($r['description']) > 0) {
	      				$day15 .= $r['description'];
	      			}  
      			  
      			$day15 .= '	  
       			  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      			</div>';
      			
      		}
      		
      		if ($effective_day == 16) {
      		
      			// .$effective_hour.':'.$effective_minute.'&nbsp;
      			     		
      			$day16 .= '
      			<span class="event yellow ">'
      				.$start_time.'<br />
      			 	<a href="#" data-reveal-id="modal-'.$month_id.'-'.$r['event_id'].'">'
      					.$r['event_name'].'
      				</a>
      			</span>
      			<div id="modal-'.$month_id.'-'.$r['event_id'].'" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
      			  <h2 id="modalTitle">'.$r['event_name'].'</h2>
      			  <p class="lead">Event Type: '.$r['event_type_name'].'</p>
      			  <p>Begin: '.$display_effective_timestamp.'<br />End: '.$display_end_timestamp.'</p>';
      			  
      			  
      				if (strlen($r['url']) > 0) {
      					$day16 .= '<p>URL: '.$r['url'].'</p>';
      				}
      				
      				if (strlen($r['description']) > 0) {
      					$day16 .= $r['description'];
      				}  
      			  
      			$day16 .= '	  
      				  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      			</div>';
      			
      		}
      		 	
      	}
      					
      	mysqli_free_result($result_calendar_event);
      	      
      	// Display the June 2015 Calendar.
      	?>
      	
      	<tbody> <!-- June 2015 -->
      	  <tr> <!-- Row 1 -->
      	  
      	    <td class="inactive">
      	      <div>
      	        <span class="day">31</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">1</span>
      	        <span class="event green">Team1 vs Team2</span>
      	        <span class="event yellow">Practice</span>
      	        <span class="event blue">Team1 vs Team2</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">2</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">3</span>
      	        <span class="event yellow">Team1 vs Team2</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">4</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">5</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">6</span>
      	      </div>
      	    </td>
      	    
      	  </tr>
      	  
      	  <tr> <!-- Row 2 -->
      	  
      	    <td>
      	      <div>
      	        <span class="day">7</span>
      	        <span class="event yellow">Practice</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">8</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">9</span>
      	        <span class="event yellow">Team1 vs Team2</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">10</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">11</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">12</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">13</span>
      	      </div>
      	    </td>
      	              
      	  </tr>
      	  
      	  <tr> <!-- Row 3 -->
      	  
      	    <td>
      	      <div>
      	        <span class="day">14</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">15</span>
      	        <?php echo $day15; ?>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">16</span>
      	        <?php echo $day16; ?>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">17</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">18</span>
      	        <span class="event blue">Team1 vs Team2</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">19</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">20</span>
      	      </div>
      	    </td>
      	              
      	  </tr>
      	  
      	  <tr> <!-- Row 4 -->
      	  
      	    <td>
      	      <div>
      	        <span class="day">21</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">22</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">23</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">24</span>
      	        <span class="event green">Team1 vs Team2</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">25</span>
      	        <span class="event green">Practice</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">26</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">27</span>
      	      </div>
      	    </td>     
      	    
      	  </tr>
      	  
      	  <tr> <!-- Row 5 -->
      	  
      	    <td>
      	      <div>
      	        <span class="day">28</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">29</span>
      	      </div>
      	    </td>
      	    <td>
      	      <div>
      	        <span class="day">30</span>
      	      </div>
      	    </td>
      	    <td class="inactive">
      	      <div>
      	        <span class="day">1</span>
      	      </div>
      	    </td>
      	    <td class="inactive">
      	      <div>
      	        <span class="day">2</span>
      	      </div>
      	    </td>
      	    <td class="inactive">
      	      <div>
      	        <span class="day">3</span>
      	      </div>
      	    </td>
      	    <td class="inactive">
      	      <div>
      	        <span class="day">4</span>
      	      </div>
      	    </td>          
      	    
      	  </tr>
      	  
      	</tbody> 
      	
      	<?php  	
      	
      }
      
      ?>
            
      <!-- 
      <tbody>
        <tr>
          <td class="inactive">
            <div>
              <span class="day">29</span>
            </div>
          </td>
          <td class="inactive">
            <div>
              <span class="day">30</span>
            </div>
          </td>
          <td class="inactive">
            <div>
              <span class="day">31</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">1</span>
              <span class="event green">Team1 vs Team2</span>
              <span class="event yellow">Practice</span>
              <span class="event blue">Team1 vs Team2</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">2</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">3</span>
              <span class="event yellow">Team1 vs Team2</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">4</span>
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <div>
              <span class="day">5</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">6</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">7</span>
              <span class="event yellow">Practice</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">8</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">9</span>
              <span class="event yellow">Team1 vs Team2</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">10</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">11</span>
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <div>
              <span class="day">12</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">13</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">14</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">15</span>
              <span class="event yellow">Team1 vs Team2</span>
              <span class="event blue">Practice</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">16</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">17</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">18</span>
              <span class="event blue">Team1 vs Team2</span>
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <div>
              <span class="day">19</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">20</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">21</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">22</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">23</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">24</span>
              <span class="event green">Team1 vs Team2</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">25</span>
              <span class="event green">Practice</span>
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <div>
              <span class="day">26</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">27</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">28</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">29</span>
            </div>
          </td>
          <td>
            <div>
              <span class="day">30</span>
            </div>
          </td>
          <td class="inactive">
            <div>
              <span class="day">1</span>
            </div>
          </td>
          <td class="inactive">
            <div>
              <span class="day">2</span>
            </div>
          </td>
        </tr>
      </tbody>
      
      -->
      
    </table>
    
    <p>*<em>Time Zone: EDT</em></p>
    
  </div>
</div>