<?php

echo 'Begin show_cookbook.php<br /><hr />';

// https://supplementrelief.com/cookbook/?ppca_id=328&ca_id=2255

include_once('../includes/core.php');	

if (isset($_GET['ppca_id'])) {
	$ppca_id = $_GET['ppca_id'];
}
	
if (isset($_GET['ca_id'])) {
	$content_asset_id = $_GET['ca_id'];
	$_SESSION['resource']['resource_id'] = $_GET['ca_id'];
		
}

if (isset($_GET['keyword'])) {
	$_POST['action'] = 'search';
	$_POST['keyword'] = $_GET['keyword'];
}

// show_array($_SERVER);

$query_string = $_SERVER['QUERY_STRING'];

// echo $query_string;

// Get the Resource Title and Description for user display.

$queryResource = 'SELECT 
	ca.title, 
	ca.summary_description, 
	rk.discussion_forum, 
	dfc.discussion_thread_post, 
	dfc.discussion_thread_post_document, 
	dfc.discussion_thread_post_image, 
	dfc.discussion_thread_post_video,
	dfc.discussion_thread_post_audio, 
	dfc.discussion_reply_post, 
	dfc.discussion_reply_post_document, 
	dfc.discussion_reply_post_image, 
	dfc.discussion_reply_post_video,
	dfc.discussion_reply_post_audio, 
	im.host_url,
	im.caption,  
	im.alt_text      	
	FROM content_assets ca 
	LEFT JOIN resource_kits rk ON ca.id = rk.content_asset_id  
	LEFT JOIN discussion_forum_configurations dfc ON rk.content_asset_id = dfc.resource_id 		
	LEFT JOIN images im ON 
		(rk.image_id = im.content_asset_id AND 
		 im.size = "Resource" AND
		 im.usage_size = "Small" AND 
		 im.usage_shape = "Block" AND
		 im.format = "JPG") 	
	WHERE ca.id = '.$content_asset_id.'  
	LIMIT 1';
	
// echo $queryResource;
// die();

$result_resource = mysqli_query($connection, $queryResource);

if (!$result_resource) {
	show_mysqli_error_message($queryResource, $connection);
	die;
}
	
while($r = mysqli_fetch_assoc($result_resource)) {

	// show_array($r);
	
	if ($r['discussion_forum']) {
		// Set session variables to persist the Discussion Forum Configuration for this Recipe
		$_SESSION['resource']['discussion_forum']['forum'] = $r['discussion_forum'];	
		$_SESSION['resource']['discussion_forum']['thread_post'] = $r['discussion_thread_post'];
		$_SESSION['resource']['discussion_forum']['thread_post_document'] = $r['discussion_thread_post_document'];	
		$_SESSION['resource']['discussion_forum']['thread_post_image'] = $r['discussion_thread_post_image'];
		$_SESSION['resource']['discussion_forum']['thread_post_video'] = $r['discussion_thread_post_video'];
		$_SESSION['resource']['discussion_forum']['thread_post_audio'] = $r['discussion_thread_post_audio'];
		$_SESSION['resource']['discussion_forum']['reply_post'] = $r['discussion_reply_post'];
		$_SESSION['resource']['discussion_forum']['reply_post_document'] = $r['discussion_reply_post_document'];	
		$_SESSION['resource']['discussion_forum']['reply_post_image'] = $r['discussion_reply_post_image'];
		$_SESSION['resource']['discussion_forum']['reply_post_video'] = $r['discussion_reply_post_video'];
		$_SESSION['resource']['discussion_forum']['reply_post_audio'] = $r['discussion_reply_post_audio'];		
	} else {
		unset($_SESSION['resource']['discussion_forum']);
	}

	?>
	
	<div class="panel">
	
		<div class="row">
		
			<div class="small-12 medium-3 columns small-only-text-center">
				<img class="th" src="<?php echo $_SESSION['application']['root_media_host_url'].$r['host_url']; ?>" alt="<?php echo $r['alt_text']; ?>" />
				<div class="caption"><?php echo $r['caption']; ?></div>
			</div>
			
			<div class="small-12 medium-9 columns colWrapMargin">
				<h1><?php echo cleanEncoding($r['title']); ?></h1>
				<?php echo cleanEncoding($r['summary_description']); ?>
				
			</div>
					
		</div>
		
	</div>
			
	<?php

}

// $query_append_order_by = ' ORDER BY ca.content_asset_type_code, cau.seq';
// $query_append_order_by = ' ORDER BY ca.content_asset_type_code, ca.title';
// $query_append_order_by = ' ORDER BY category_name ASC, cau.seq ASC';
$query_append_order_by = ' ORDER BY ca.title ASC';


// Recipe Search parameters
if (isset($_POST['action']) && $_POST['action'] == 'search') {

	// show_array($_POST);
	// die();
	
	$query_append_recipe = '';
	
	// Get keyword parameter and checks for empty parameter #1
	if (isset($_POST['keyword']) && !empty($_POST['keyword']))
	{
		$keyword = safe_sql_data($connection, $_POST['keyword']);	
		$query_append_recipe .= " AND (ca.title LIKE '%".$keyword."%' OR ca.description LIKE '%".$keyword."%' OR rec.ingredients LIKE '%".$keyword."%') AND cau.category_code IS NULL ";
	}
		
	if (isset($_POST['number_servings']) && !empty($_POST['number_servings']))
	{
		$number_servings = safe_sql_data($connection, $_POST['number_servings']);	
		
		if ($number_servings < 9) {
			$query_append_recipe .= " AND (rec.number_servings = ".$number_servings.")";		
		} else {
			$query_append_recipe .= " AND (rec.number_servings > '8')";		
		}
		
	}
	
	if (isset($_POST['time_to_prepare']) && !empty($_POST['time_to_prepare']))
		{
		$time_to_prepare = safe_sql_data($connection, $_POST['time_to_prepare']);	
		
		switch ($time_to_prepare) {
			
			case 10:
			$query_append_recipe .= " AND (rec.preparation_time_minutes < ".$time_to_prepare.")";
			break;
			
			case 20:
			$query_append_recipe .= " AND (rec.preparation_time_minutes > '10' AND rec.preparation_time_minutes <= '20')";
			break;
			
			case 30:
			$query_append_recipe .= " AND (rec.preparation_time_minutes > '20' AND rec.preparation_time_minutes <= '30')";
			break;
			
			case 60:
			$query_append_recipe .= " AND (rec.preparation_time_minutes > '30' AND rec.preparation_time_minutes <= '60')";
			break;
			
			case 61:
			$query_append_recipe .= " AND (rec.preparation_time_minutes > '60')";
			break;
						
			default:
			$query_append_recipe .= " AND (rec.preparation_time_minutes < '500')";
			
		}
				
	}
	
	if (isset($_POST['author']) && !empty($_POST['author']))
		{
		$author = safe_sql_data($connection, $_POST['author']);			
		$query_append_recipe .= " AND (ca.author LIKE '%".$author."%')";			
		}
		
	if (isset($_POST['has_video']) && !empty($_POST['has_video']))
		{
		$has_video = safe_sql_data($connection, $_POST['has_video']);			
		$query_append_recipe .= " AND (rec.video_id is NOT NULL)";			
		}
	
	// Checkboxes	
	if (isset($_POST['breakfast']) && !empty($_POST['breakfast']))
		{
		$breakfast = safe_sql_data($connection, $_POST['breakfast']);			
		$query_append_recipe .= " AND (rec.breakfast = '1')";			
		}
		
	if (isset($_POST['lunch']) && !empty($_POST['lunch']))
		{
		$lunch = safe_sql_data($connection, $_POST['lunch']);			
		$query_append_recipe .= " AND (rec.lunch = '1')";			
		}
		
	if (isset($_POST['dinner']) && !empty($_POST['dinner']))
		{
		$dinner = safe_sql_data($connection, $_POST['dinner']);			
		$query_append_recipe .= " AND (rec.dinner = '1')";			
		}
		
	if (isset($_POST['appetizer']) && !empty($_POST['appetizer']))
		{
		$appetizer = safe_sql_data($connection, $_POST['appetizer']);			
		$query_append_recipe .= " AND (rec.appetizer = '1')";			
		}
		
	if (isset($_POST['snack']) && !empty($_POST['snack']))
		{
		$snack = safe_sql_data($connection, $_POST['snack']);			
		$query_append_recipe .= " AND (rec.snack = '1')";			
		}
			
	if (isset($_POST['condiment']) && !empty($_POST['condiment']))
		{
		$condiment = safe_sql_data($connection, $_POST['condiment']);			
		$query_append_recipe .= " AND (rec.condiment = '1')";			
		}
		
	if (isset($_POST['dessert']) && !empty($_POST['dessert']))
		{
		$dessert = safe_sql_data($connection, $_POST['dessert']);			
		$query_append_recipe .= " AND (rec.dessert = '1')";			
		}
		
	if (isset($_POST['drink']) && !empty($_POST['drink']))
		{
		$drink = safe_sql_data($connection, $_POST['drink']);			
		$query_append_recipe .= " AND (rec.drink = '1')";			
		}
		
	if (isset($_POST['main_dish']) && !empty($_POST['main_dish']))
		{
		$main_dish = safe_sql_data($connection, $_POST['main_dish']);			
		$query_append_recipe .= " AND (rec.main_dish = '1')";			
		}
	
	if (isset($_POST['salad']) && !empty($_POST['salad']))
		{
		$salad = safe_sql_data($connection, $_POST['salad']);			
		$query_append_recipe .= " AND (rec.salad = '1')";			
		}
		
	if (isset($_POST['salad_dressing']) && !empty($_POST['salad_dressing']))
		{
		$salad_dressing = safe_sql_data($connection, $_POST['salad_dressing']);			
		$query_append_recipe .= " AND (rec.salad_dressing = '1')";			
		}	
	
	if (isset($_POST['side_dish']) && !empty($_POST['side_dish']))
		{
		$side_dish = safe_sql_data($connection, $_POST['side_dish']);			
		$query_append_recipe .= " AND (rec.side_dish = '1')";			
		}
		
	if (isset($_POST['spread_sauce']) && !empty($_POST['spread_sauce']))
		{
		$spread_sauce = safe_sql_data($connection, $_POST['spread_sauce']);			
		$query_append_recipe .= " AND (rec.spread_sauce = '1')";			
		}
		
	if (isset($_POST['soup']) && !empty($_POST['soup']))
		{
		$soup = safe_sql_data($connection, $_POST['soup']);			
		$query_append_recipe .= " AND (rec.soup = '1')";			
		}
					
	if (isset($_POST['raw']) && !empty($_POST['raw']))
		{
		$raw = safe_sql_data($connection, $_POST['raw']);			
		$query_append_recipe .= " AND (rec.raw = '1')";			
		}
		
	if (isset($_POST['vegan']) && !empty($_POST['vegan']))
		{
		$vegan = safe_sql_data($connection, $_POST['vegan']);			
		$query_append_recipe .= " AND (rec.vegan = '1')";			
		}
		
	if (isset($_POST['vegetarian']) && !empty($_POST['vegetarian']))
		{
		$vegetarian = safe_sql_data($connection, $_POST['vegetarian']);			
		$query_append_recipe .= " AND (rec.vegetarian = '1')";			
		}
		
	if (isset($_POST['dairy_free']) && !empty($_POST['dairy_free']))
		{
		$dairy_free = safe_sql_data($connection, $_POST['dairy_free']);			
		$query_append_recipe .= " AND (rec.dairy_free = '1')";			
		}
			
	if (isset($_POST['gluten_free']) && !empty($_POST['gluten_free']))
		{
		$gluten_free = safe_sql_data($connection, $_POST['gluten_free']);			
		$query_append_recipe .= " AND (rec.gluten_free = '1')";			
		}
		
	if (isset($_POST['nut_free']) && !empty($_POST['nut_free']))
		{
		$nut_free = safe_sql_data($connection, $_POST['nut_free']);			
		$query_append_recipe .= " AND (rec.nut_free = '1')";			
		}
			
	if (isset($_POST['soy_free']) && !empty($_POST['soy_free']))
		{
		$soy_free = safe_sql_data($connection, $_POST['soy_free']);			
		$query_append_recipe .= " AND (rec.soy_free = '1')";			
		}
	
	// Order by	
	if (isset($_POST['order_by']) && !empty($_POST['order_by']))
		{
		$order_by = safe_sql_data($connection, $_POST['order_by']);	
		
		switch ($order_by) {
						
			case 'title':
			$query_append_order_by = ' ORDER BY ca.content_asset_type_code, ca.title';
			break;
			
			case 'author_title':
			$query_append_order_by = ' ORDER BY ca.content_asset_type_code, ca.author, ca.title';
			break;
			
			case 'most_recent_posted':
			$query_append_order_by = ' ORDER BY ca.content_asset_type_code, ca.effective_date DESC'; 
			break;
						
			default:
			$query_append_order_by = ' ORDER BY ca.content_asset_type_code, cau.seq'; 			
		}		
				
	}
			
	// echo $query_append_recipe.'<br />';
	// echo $query_append_recipe_order_by.'<br />';
	
	// die();

}

// Get any current Project Brand Products associated with this Resource. 
$queryResourceProjectBrandProducts = 'SELECT 
    bpu.name AS addendum_product_name,
    pbpu.group_options_description AS addendum_product_group_options_description,  
    pbpu.page_meta_description AS addendum_product_page_meta_description, 
	pbpu.page_canonical_url AS addendum_product_canonical_url, 
	urls.url AS addendum_product_url, 
    im.host_url AS addendum_product_image_url, 
	im.alt_text AS addendum_product_image_alt_text 
	FROM content_asset_project_brand_products capbp 
	LEFT JOIN project_brand_product_usages pbpu ON capbp.project_brand_product_usage_id = pbpu.id 
	LEFT JOIN url_usages urlu ON pbpu.id = urlu.pbpu_id 
	LEFT JOIN urls urls ON urlu.url_id = urls.id 
	LEFT JOIN brand_product_usages bpu ON pbpu.brand_product_usage_id = bpu.id 
    LEFT JOIN images im 
	    ON (bpu.image_id = im.content_asset_id AND 
            im.size = "Product" AND 
            im.usage_size = "Small" AND 
            /* im.usage_shape = "Portrait" AND */ 
            im.format = "JPG")
	WHERE capbp.content_asset_id = '.$content_asset_id.' 
	AND (capbp.effective_date <= CURRENT_DATE AND
        (capbp.end_date IS NULL or capbp.end_date >= CURRENT_DATE)) 
    AND pbpu.active = "1" 
    ORDER BY bpu.name'; 
    
// echo $queryResourceProjectBrandProducts . '<br /><hr />';

$result_resource_project_brand_product = mysqli_query($connection, $queryResourceProjectBrandProducts);

if (!$result_resource_project_brand_product) {
	show_mysqli_error_message($queryResourceProjectBrandProducts, $connection);
	die;
}
				
// echo 'Database Query Project Program Blog Post Content Assets succeeded.<br /><hr />';

$product_addendum_block = '';

while($pbp = mysqli_fetch_assoc($result_resource_project_brand_product)) { 
	
    if (!$product_addendum_block) {
		// Create the Resource Product Addendum container.
		$product_addendum_block .= '
		    <!--googleoff: index-->
		    <div class="panel callout radius">
		        <div class="small-12 columns">
		            <h3><i class="fa fa-product-hunt"></i>  Supplement Referrals</h3>
		            <p>Learn more about these <b>NuMedica Supplements</b> on our website.</p>
                </div>';
	}
	
    $addendum_product_image_url = $pbp['addendum_product_image_url'];
	$addendum_product_image_alt_text = $pbp['addendum_product_image_alt_text'];
	$addendum_product_url = $pbp['addendum_product_url'];
	$addendum_product_page_meta_description	= $pbp['addendum_product_page_meta_description'];			
        
    $product_addendum_block .= '
	    <hr>
	    <div class="row">		
            <div class="small-12 medium-2 columns text-center">
	            <img class="th" src="'.$_SESSION['application']['root_media_host_url'].$addendum_product_image_url.'" alt="'.$addendum_product_image_alt_text.'" width="125px">
            </div>
            <div class="small-12 medium-10 columns colWrapMargin">
                <a href="../'.$addendum_product_url.'/"><h4>'.$pbp['addendum_product_name'].'</a></h4>'.
                $pbp['addendum_product_page_meta_description'].'
             </div>
        </div>';		
}

if ($product_addendum_block) { 
	    
	$product_addendum_block .= '
	    </div> <!-- Close Panel -->
	    <!--googleon: index-->';
}

mysqli_free_result($result_resource_project_brand_product);
	 
// Get all Content Asset Usages for this Resource i.e. of type Code, Document, Recipe, Video, etc including Addendum Content Assets.

$queryResourceAssets = 'SELECT 
ca.content_asset_type_code, 
ca.version,
ca.title, 
ca.summary_description, 
ca.description, 
ca.author, 
ca.text, 
ca.citations,  
ca.effective_date, 
cau.content_asset_child_id, 		
cau.seq, 
cau.category_code, 
cat.name AS category_name,  
co.embed_code, 
do.image_id AS document_image_id, 
do.host_url AS document_host_url, 
do.caption AS document_caption, 
do.alt_text AS document_alt_text,  
rec.image_id AS recipe_image_id, 
rec.video_id AS recipe_video_id, 
rec.ingredients AS recipe_ingredients, 
rec.directions AS recipe_directions, 
rec.number_servings, 
rec.preparation_time_minutes, 
rec.breakfast, 
rec.lunch, 
rec.dinner, 
rec.appetizer, 
rec.snack, 
rec.condiment, 
rec.dessert, 
rec.drink,  
rec.main_dish, 
rec.salad, 
rec.salad_dressing, 
rec.side_dish, 
rec.spread_sauce, 
rec.soup, 
rec.raw, 
rec.vegan, 
rec.vegetarian, 
rec.dairy_free, 
rec.gluten_free, 
rec.nut_free, 
rec.soy_free,
rec.page_canonical_url AS addendum_recipe_canonical_url,     
imdo.host_url AS document_image_host_url,  
imre.host_url AS recipe_image_host_url, 
imre.alt_text AS addendum_recipe_image_alt_text,     
vi.host_url_mp4, 
vi.host_url_webm, 
imvi1.host_url AS thumbnail_image_url, 
imvi2.host_url AS poster_image_url, 
vi.duration_minutes_seconds, 
urls.url AS seo_url,
rk.page_canonical_url AS resource_canonical_url,     
imrk.host_url AS addendum_resource_image_url, 
imrk.alt_text AS addendum_resource_image_alt_text, 
bl.page_canonical_url AS blog_canonical_url, 
imbl.host_url AS addendum_blog_image_url, 
imbl.alt_text AS addendum_blog_image_alt_text,  
bp.page_canonical_url AS blog_post_canonical_url, 
imbp.host_url AS addendum_blog_post_image_url, 
imbp.alt_text AS addendum_blog_post_image_alt_text 
FROM content_asset_usages cau
LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id 
LEFT JOIN categories cat ON cau.category_code = cat.code 
LEFT JOIN blogs bl ON ca.id = bl.content_asset_id 
LEFT JOIN images imbl 
    ON (bl.image_id = imbl.content_asset_id AND 
        imbl.size = "Blog" AND 
        imbl.usage_size = "Small" AND 
        imbl.usage_shape = "Block" AND  
        imbl.format = "JPG")		 
LEFT JOIN blog_posts bp ON ca.id = bp.content_asset_id 
LEFT JOIN images imbp 
    ON (bp.image_id = imbp.content_asset_id AND 
        imbp.size = "Blog Post" AND 
        imbp.usage_size = "Small" AND 
        imbp.usage_shape = "Block" AND  
        imbp.format = "JPG")		
LEFT JOIN codes co ON cau.content_asset_child_id = co.content_asset_id 
LEFT JOIN documents do ON cau.content_asset_child_id = do.content_asset_id 
LEFT JOIN images imdo ON 
	(do.image_id = imdo.content_asset_id AND 
	 imdo.size = "Resource" AND 
	 imdo.usage_size = "Small" AND
	 imdo.usage_shape = "Block" AND
	 imdo.format = "JPG")
LEFT JOIN recipes rec ON cau.content_asset_child_id = rec.content_asset_id 
LEFT JOIN images imre ON 
	(rec.image_id = imre.content_asset_id AND 
	 imre.size = "Recipe" AND 
	 imre.usage_size = "Small" AND
	 imre.usage_shape = "Block" AND
	 imre.format = "JPG")
LEFT JOIN videos vi ON cau.content_asset_child_id = vi.content_asset_id 
LEFT JOIN images imvi1 ON 
	(vi.image_id = imvi1.content_asset_id AND 
	 imvi1.size = "Resource" AND
	 imvi1.usage_size = "Small" AND 
	 imvi1.usage_shape = "Block" AND
	 imvi1.format = "JPG") 	
LEFT JOIN images imvi2 ON 
	(vi.image_id = imvi2.content_asset_id AND 
	 imvi2.size = "Video" AND
	 imvi2.usage_size = "Large" AND 
	 imvi2.usage_shape = "Landscape" AND 
	 imvi2.format = "JPG") 
LEFT JOIN url_usages urlu 
    ON (cau.content_asset_child_id = urlu.recipe_id AND 
        urlu.effective_date <= CURRENT_DATE AND
       (urlu.end_date IS NULL or urlu.end_date >= CURRENT_DATE))
LEFT JOIN urls urls ON urlu.url_id = urls.id 
LEFT JOIN resource_kits rk ON ca.id = rk.content_asset_id 
LEFT JOIN images imrk 
    ON (rk.image_id = imrk.content_asset_id AND 
        imrk.size = "Resource" AND 
        imrk.usage_size = "Small" AND 
        imrk.usage_shape = "Block" AND  
        imrk.format = "JPG")	
WHERE cau.content_asset_parent_id = '.$content_asset_id.
$query_append_recipe.
$query_append_order_by;

// echo '<br />'.$queryResourceAssets.'<br /><br />';
// die();

$result_resource_asset = mysqli_query($connection, $queryResourceAssets);

if (!$result_resource_asset) {
	show_mysqli_error_message($queryResourceAssets, $connection);
	die;
}

$total_records = mysqli_num_rows($result_resource_asset);

if ($total_records < 1) {

	$recipe_results_list .= '			  	 						
		<div class="row">
			<div class="small-12 medium-3 columns">
				<img class="th" src="https://cdn-manager.net/media/images/not-found.jpg" alt="no recipes found using the search criterion" />
			</div>			
			<div class="small-12 medium-9 columns">
				<div data-alert class="alert-box warning">
				  <p>No Recipes were found matching your search parameters. Please <b>refine your search</b> and try again.</p>			
				  <!-- <a href="#" class="close">&times;</a> -->
				</div>
			</div>
		</div>';
	
}
 
$code_block = '';
$document_block = '';
$recipe_block = '';
$video_block = '';
$video_modal_block .= '';
$addendum_block .= '';

$codeCount = 0;
$documentCount = 0;
$recipeCount = 0;
$videoCount = 0;

while($c = mysqli_fetch_assoc($result_resource_asset)) {

    // if ($_SESSION['user']['user_id'] == 2) { echo 'Jay testing ...<br>'; show_array($c); }

	// show_array($c);
	// echo '<hr />Asset Type: '.$c['content_asset_type_code'].' | Seq: '.$c['seq'].' | Title: '.cleanEncoding($c['title']).'<br /><hr />';
	
    if (!$c['category_name']) {
        // This is a Body content asset and not an Addendum content asset.				
 	
        switch ($c['content_asset_type_code']) {
	
    		// $content_asset_child_id = $c['content_asset_child_id'];
    		
    		case 'ARTCL':
    		
    			// echo 'Switch CASE ARTCL: '.$c['content_asset_type_code'].'<br />';
    				    	    	
    			$article_block .= '
    			<div class="row">
    				<div class="small-12 columns">
    					<div id="articleTextBlocks">'.
    						$c['description'].'
    					</div>';
    					
    					if (strlen($c['citations']) > 0) {
    						$article_block .= '
    						<div id="articleCitations" class="panel">
    							<p>Citations:</p>'.
    							$c['citations'].'
    						</div>';
    					}
    					
    					$article_block .= '
    				</div>
    			</div>';
    	    
    		case 'CODE':
    		
    			// echo 'Switch CASE CODE: '.$c['content_asset_type_code'].'<br />';
    				    	    	
    		  	$code_block .= $c['embed_code'];	    	
    		    // echo 'code_block Embed Code: '.$code_block;
    		    break;
    	    
    		case 'DOC':
    
    			// echo 'Switch CASE DOC: '.$c['content_asset_type_code'].'<br />';
    			
    			if ($documentCount == 0) {
    				// Setup the Document block header
    				$document_block .= ' 
    				 <!-- Document Block Grid -->
    				 <div class="row">
    				 	<div class="large-12 columns">
    				 		<h3><i class="fa fa-book"></i> Documents</h3>
    						<ul class="small-block-grid-1 small-only-text-center medium-block-grid-3 large-block-grid-4">';  	
    			}
    			
    			$documentCount++;
    						
    			$document_block .= ' 
    			<li>
    				<a href="'.$_SESSION['application']['root_media_host_url'].$c['document_host_url'].'" target="_blank" title="Select to view and/or download Document"><img src="'.$_SESSION['application']['root_media_host_url'].$c['document_image_host_url'].'" class="th" data-tooltip data-options="disable_for_touch:true" class="has-tip tip-bottom" title="'.$c['title'].'"></a>
    				<!-- <div class="imageCaption"><i class="fa fa-image"></i>&nbsp;&nbsp;'.$c['document_caption'].'</div> -->
    				<!-- <div><center></i>'.$c['document_caption'].'</center></div> -->
    			</li>';				
    					
    	        break;
    	        
    			// End case 'DOC':
    	    
    		 case 'RECIPE':
    		  
    		  	// echo 'Switch CASE RECIPE: '.$c['content_asset_type_code'].'<br />';
    		  	// echo 'Recipe URL: '.$c['seo_url'].'<br />';
    		  		  
    			// Add the Recipe to the Search Results List for column 2
    			
                if ($c['seo_url']) {
                    $recipe_url = '../'.$c['seo_url'].'/';
            	} else {	
            		$recipe_url = '../recipe/?re_id='.$c['content_asset_child_id'];
            	}			
    			
    			if ($c['recipe_image_host_url']) {
    				$image_source_url = $_SESSION['application']['root_media_host_url'].$c['recipe_image_host_url'];				
    			} else {
    				$image_source_url = 'https://placehold.it/150x150';
    			}
    					
    			// Display the Recipe content
    			
    			// <h3>'.$c['title'].' <small>'.$c['author'].'&nbsp;&nbsp;<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip" title="Posted '.no_null_date_check($c['effective_date']).'"><i class="fa fa-clock-o"></i></small></h3>'.
    
    			$recipe_results_list .= '
    			<div id="recipeRow">
    						  	 						
    				<div class="row collapse">
    				
    					<div class="small-12 medium-3 columns small-only-text-center">
    						<a href="'.$recipe_url.'" title="view Recipe for '.$c['title'].'"><img class="th" src="'.$image_source_url.'" alt="" /></a>
    					</div>
    					
    					<div class="small-12 medium-9 columns colWrapMargin">
    						
    						<a href="'.$recipe_url.'" title="view Recipe"><h3 style="color: #61AFD1;">'.$c['title'].'</h3></a>'.
    						cleanEncoding($c['summary_description']).'Chef: '.$c['author'].'
    						&nbsp;<a href="'.$recipe_url.'" title="view Recipe"><i class="fa fa-hand-o-right"></i>&nbsp;view recipe</a>
    	
    					</div>
    					
    				</div>
    				
    				<hr>
    			
    			</div>';
    							  	    
    			break;
    	  	    
    		case 'VIDEO':
    
    			// echo 'Switch CASE VIDEO: '.$c['content_asset_type_code'].'<br />';		
    	        
    	        if ($videoCount == 0) {
    	        	// Setup the video block header
    	        	$video_block .= '
    	        	<!-- Video Block Grid -->
    	        	<div class="row">
    	        		<div class="small-12 columns">
    	        			<h3><i class="fa fa-file-video-o"></i> Videos</h3>';
    	        		if(!$ie9){
    	        			$video_block .= '<ul class="small-block-grid-1 small-only-text-center medium-block-grid-3 large-block-grid-4">';  	
    	        		}
    	        }
    	        
    	        $videoCount++;
    	                
    
    	        if($ie9) {
    
    	        $video_block .= ' 
    	        <div class="row">
    	        	<div class="medium-3 hide-for-small-down columns">&nbsp;</div>
    		        <div id="video'.$videoCount.'" class="small-12 medium-6 columns">
    			    	<h3>'.$c['title'].'<small>&nbsp;&nbsp;<i class="fa fa-clock-o"></i>&nbsp;'.$c['duration_minutes_seconds'].'</small></h3>
    		            <!-- <p class="lead">Wright Check Wellness Video Series</p> -->
    		            <video controls preload="none" style="width:100%;height:auto;" poster="'.$_SESSION['application']['root_media_host_url'].$c['poster_image_url'].'" > 
    		              <source src="'.$_SESSION['application']['root_media_host_url'].$c['host_url_mp4'].'" type="video/mp4">
    		              <source src="'.$_SESSION['application']['root_media_host_url'].$c['host_url_webm'].'" type="video/webm">   
    		              Your browser does not support the video tag.
    		            </video>
    		        </div>
    		        <div class="medium-3 hide-for-small-down columns">&nbsp;</div>
    	        </div>
    	        ';
    
    	        } else {
    
    	        $video_block .= '
    	        <li>
    	        	<a href="#" data-reveal-id="video'.$videoCount.'" title="Select to view Video"><img src="'.$_SESSION['application']['root_media_host_url'].$c['thumbnail_image_url'].'" class="th" data-tooltip data-options="disable_for_touch:true" class="has-tip tip-bottom" title="'.$c['title'].' - Time: '.$c['duration_minutes_seconds'].'"></a>
    	        </li>';
    	        
    	        $video_modal_block .= ' 
    	        <div id="video'.$videoCount.'" class="reveal-modal medium" data-reveal>
    		    	<h2>'.$c['title'].'<small>&nbsp;&nbsp;<i class="fa fa-clock-o"></i>&nbsp;'.$c['duration_minutes_seconds'].'</small></h2>
    	            <!-- <p class="lead">Wright Check Wellness Video Series</p> -->
    	            <video controls preload="none" style="width:100%;height:auto;" poster="'.$_SESSION['application']['root_media_host_url'].$c['poster_image_url'].'" > 
    	              <source src="'.$_SESSION['application']['root_media_host_url'].$c['host_url_mp4'].'" type="video/mp4">
    	              <source src="'.$_SESSION['application']['root_media_host_url'].$c['host_url_webm'].'" type="video/webm">   
    	              Your browser does not support the video tag.
    	            </video>    
    	            <a class="close-reveal-modal">&#215;</a>
    	        </div>';
    
    	        }
    
    	        break;
    	        
    	    // End case 'VIDEO':	      	    	  	  
     		    
    	} // End switch ($c['content_asset_type_code']) {
        	
    } else {
                
        if ($c['category_name'] == "Resource Addendum") {
    					
			// show_array($c);
			// echo 'Addendum: '.$a['category_name'].'<br>';

			if (!$resource_addendum_block) {
				// Create the Resource Addendum container as a stand alone.
				$resource_addendum_block .= '
				    <!--googleoff: index-->
				    <div class="panel callout radius">
				        <div class="small-12 columns">
				            <h3><i class="fa fa-medkit"></i>  Related Wellness Resources</h3>
				            <p>We encourage you to <b>take advantage of these FREE Wellness Resources</b> on our website.</p>
                        </div>';
			}
						
			$addendum_image_url = '';
			$addendum_image_alt_text = '';
			$addendum_canonical_url = '';
			
            switch ($c['content_asset_type_code']) {
                
                case 'BLOG':
                			    
			        $addendum_image_url = $c['addendum_blog_image_url'];
					$addendum_image_alt_text = $c['addendum_blog_image_alt_text'];
					$addendum_canonical_url = $c['blog_canonical_url'];								
						
                break;

                case 'BLOGP':
			    
			        $addendum_image_url = $c['addendum_blog_post_image_url'];
					$addendum_image_alt_text = $c['addendum_blog_post_image_alt_text'];
					$addendum_canonical_url = $c['blog_post_canonical_url'];								
						
                break;
                
                case 'RECIPE':
                
                    // show_array($c);
			    
			        $addendum_image_url = $c['recipe_image_host_url'];
					$addendum_image_alt_text = $c['addendum_recipe_image_alt_text'];
					$addendum_canonical_url = $c['addendum_recipe_canonical_url'];						
						
                break;
                
			    case 'RESO':
			    
			        $addendum_image_url = $c['addendum_resource_image_url'];
					$addendum_image_alt_text = $c['addendum_resource_image_alt_text'];
					$addendum_canonical_url = $c['resource_canonical_url'];						
						
                break;
                
            }							
			    					
			$resource_addendum_block .= '
				<hr>
			    <div class="row">		
                    <div class="small-12 medium-2 columns text-center">
			            <img class="th" src="'.$_SESSION['application']['root_media_host_url'].$addendum_image_url.'" alt="'.$addendum_image_alt_text.'" width="125px">
                    </div>
                    <div class="small-12 medium-10 columns colWrapMargin">
                        <a href="'.$addendum_canonical_url.'"><h4>'.$c['title'].'</a></h4>'.
                        $c['summary_description'].'
                    </div>
                </div>';
			
		} // END if ($c['category_name'] == "Resource Addendum") {
    		      
    } // END if (!$c['category_name']) {
		    		
} // End while($c = mysqli_fetch_assoc($result_content_asset_usage)) {
    
if ($resource_addendum_block) { 
	    
	$resource_addendum_block .= '
	    </div> <!-- Close Panel -->
	    <!--googleon: index-->';
}
				    		            
mysqli_free_result($result_resource);

mysqli_free_result($result_resource_asset);
 
if (!empty($document_block)) {
	// Complete the document block wrappers
	$document_block .= '</ul></div></div>';	
}

if (!empty($video_block)) {
	// Complete the video block wrappers
	$video_block .= '</ul></div></div>';	
}

// Display content to User.

?>

<div class="row">		
	<div class="small-12 columns">
    	
    	<ul class="tabs" data-options="deep_linking:true; scroll_to_content:false;" data-tab>
        	<li class="tab-title active"><a href="#content">Content</a></li>
			<?php if ($resource_addendum_block) { ?><li class="tab-title"><a href="#resourceAddendum">Wellness Resources</a></li><?php } ?>
			<?php if ($product_addendum_block) { ?><li class="tab-title"><a href="#productAddendum">Supplements</a></li><?php } ?>
			<?php if ($_SESSION['resource']['discussion_forum']['forum']) { ?><li class="tab-title"><a href="#discuss">Discussion Forum</a></li><?php } ?>  
        </ul>
        
        <div class="tabs-content">
            
            <div class="content active" id="content">
                <?
                // Display Article resources if any
                if (!empty($article_block)) {
                	 echo $article_block;
                }
                
                // Display Document resources if any
                if (!empty($document_block) && empty($video_block)) {
                	 echo $document_block;
                }
                
                // Display Recipe resources if any
                if (!empty($recipe_results_list)) {
                
                	// Create the Search form
                
                	// Build the Chef select list
                	$chef_query = '
                	SELECT 
                	distinct(author)   
                	FROM content_assets 
                	WHERE content_asset_type_code = "RECIPE" 
                	AND effective_date <= CURRENT_DATE 
                	AND (end_date is NULL or end_date >= CURRENT_DATE)  
                	ORDER BY author ASC';
                	
                	// echo $chef_query;
                	// die();
                	
                	$chef_result = mysql_query($chef_query);
                	
                	if (!$chef_result) {
                		show_mysqli_error_message($chef_query, $connection);
                		die;
                	}
                				
                	$chef_options = '<option value="">Chef...</option>';
                	
                	while($chef_row = mysql_fetch_array($chef_result)) {
                		$selected = '';
                		if(($author ? $author : $row1['p']) == $chef_row['author']) $selected = ' selected';
                		$chef_options .= '<option value="'.$chef_row['author'].'" '.$selected.'>'.$chef_row['author'].'</option>';
                	}
                	
                	mysql_free_result($chef_result);
                	
                	// echo $recipe_results_list
                
                	?>
                	
                	<div class="row">
                			
                		<div class="small-12 medium-3 columns">
                    		
                    		<?php
                    		 	$form_action = "../resource/";
                    			//$form_action = "../resource/?ppca_id=".$ppca_id."&ca_id=".$content_asset_id.$query_string;
                    		?>
                				
                	    	<form id="recipe_search_form" data-abide action="<?php echo $form_action; ?>" method="GET" />
                	    		<input type="hidden" name="ppca_id" value="<?php echo $ppca_id ?>"/>
                	    		<input type="hidden" name="ca_id" value="<?php echo $content_asset_id.$query_string ?>"/>
                	    		<input type="hidden" id="recipe-search-action" name="action" value="search"/>
                	    		<!-- <input type="hidden" name="ca_id" value="<?php echo $content_asset_id; ?>"/> -->
                	    				
                	    			    	
                	    		<div class="row">
                	    		  <div class="small-8 columns">
                	    		    	
                	    		    <input class="button medium radius expand" id="search" type="submit" tabindex="1 "value="Search">
                	    		    	    	      					    	   				    	      	    	      	    	      	    	  				    	    
                	    		  </div>
                	    		  <div class="small-4 columns">		    	
                	    		    <div><i class="fa fa-binoculars"></i>&nbsp;&nbsp;<?php echo $total_records; ?><br /><br /></div>    	      	    	  				    	    
                	    		  </div>
                	    		</div>
                	    																	  	
                	    	  <input type="text" tabindex="1" name="keyword" maxlength="255" placeholder="Search by title, description, ingredient..." value="<?php echo ($keyword ? $keyword : ''); ?>" />
                	    	  
                	    	  <select id="order_by_recipe" tabindex="2" name='order_by'>
                	    	  	<option <?php if (isset($order_by) && $order_by == NULL): ?>selected="selected"<?php endif ?> value="">Order by...</option>
                	    	  	<option <?php if (isset($order_by) && $order_by == 'title'): ?>selected="selected"<?php endif ?> value="title">Title</option>
                	    	  	<option <?php if (isset($order_by) && $order_by == 'author_title'): ?>selected="selected"<?php endif ?> value="author_title">Author | Title</option>
                	    	  	<option <?php if (isset($order_by) && $order_by == 'most_recent_posted'): ?>selected="selected"<?php endif ?> value="most_recent_posted">Most Recent Posted</option>
                	    	  </select>	  
                	    	  
                	    	  <select id="number_servings" tabindex="2" name='number_servings'>
                	    	  	<option <?php if (isset($number_servings) && $number_servings == NULL): ?>selected="selected"<?php endif ?> value="">Serving Size...</option>
                	    	  	<option <?php if (isset($number_servings) && $number_servings == '1'): ?>selected="selected"<?php endif ?> value="1">1</option>
                	    	  	<option <?php if (isset($number_servings) && $number_servings == '2'): ?>selected="selected"<?php endif ?> value="2">2</option>
                	    	  	<option <?php if (isset($number_servings) && $number_servings == '3'): ?>selected="selected"<?php endif ?> value="3">3</option>
                	    	  	<option <?php if (isset($number_servings) && $number_servings == '4'): ?>selected="selected"<?php endif ?> value="4">4</option>
                	    	  	<option <?php if (isset($number_servings) && $number_servings == '5'): ?>selected="selected"<?php endif ?> value="5">5</option>
                	    	  	<option <?php if (isset($number_servings) && $number_servings == '6'): ?>selected="selected"<?php endif ?> value="6">6</option>	
                	    	  	<option <?php if (isset($number_servings) && $number_servings == '7'): ?>selected="selected"<?php endif ?> value="7">7</option>	
                	    	  	<option <?php if (isset($number_servings) && $number_servings == '8'): ?>selected="selected"<?php endif ?> value="8">8</option>						  
                	    	  	<option <?php if (isset($number_servings) && $number_servings > '8'): ?>selected="selected"<?php endif ?> value="9">More than 8</option>					    		
                	    	  </select>
                	    	  				    					    	
                	    	  <select id="time_to_prepare" tabindex="3" name='time_to_prepare'>
                	    	  	<option <?php if (isset($time_to_prepare) && $time_to_prepare == NULL): ?>selected="selected"<?php endif ?> value="">Time to Prepare...</option>
                	    	  	<option <?php if (isset($time_to_prepare) && $time_to_prepare < '11'): ?>selected="selected"<?php endif ?> value="10">10 minutes or less</option>
                	    	  	<option <?php if (isset($time_to_prepare) && $time_to_prepare > '10' && $time_to_prepare <= '20'): ?>selected="selected"<?php endif ?> value="20">11 to 20 minutes</option>	  	
                	    	  	<option <?php if (isset($time_to_prepare) && $time_to_prepare > '20' && $time_to_prepare <= '30'): ?>selected="selected"<?php endif ?> value="30">21 to 30 minutes</option>
                	    	  	<option <?php if (isset($time_to_prepare) && $time_to_prepare > '30' && $time_to_prepare <= '60'): ?>selected="selected"<?php endif ?> value="60">31 to 60 minutes</option>
                	    		<option <?php if (isset($time_to_prepare) && $time_to_prepare > '60'): ?>selected="selected"<?php endif ?> value="61">more than 1 hour</option>					    		
                	    	  </select>
                	    	  			  
                	    	  <select id="limit_by_chef" tabindex="4" name="author">
                	    	    <?php echo $chef_options; ?>
                	    	  </select>
                	    	  
                	    	  <!-- <input type="checkbox" name="has_video" tabindex="5" value="1" <?php if ($has_video ==1) { echo 'checked'; } ?>> Has Video<br> -->	
                	    	  				    				  			    					    						    						    		    	
                	    	  <fieldset>     	
                	    	  <legend>Meal</legend>  	
                	    	  
                	    	  	<div class="row">    	
                	    	  	
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="breakfast" tabindex="6" value="1" <?php if ($breakfast ==1) { echo 'checked'; } ?>> Breakfast
                	    	  		</div>
                	    	  	
                	    	  	  <div class="small-12 columns">			
                	    	  			<input type="checkbox" name="lunch" value="1" <?php if ($lunch ==1) { echo 'checked'; } ?>> Lunch
                	    	  		</div>
                	    	  		
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="dinner" value="1" <?php if ($dinner ==1) { echo 'checked'; } ?>> Dinner
                	    	  		</div>
                	    	  		
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="appetizer" value="1" <?php if ($appetizer ==1) { echo 'checked'; } ?>> Appetizer
                	    	  		</div>
                	    	  		
                	    	  		<div class="small-12 columns end">			
                	    	  			<input type="checkbox" name="snack" value="1" <?php if ($snack ==1) { echo 'checked'; } ?>> Snack
                	    	  		</div>
                	    	  				    		
                	    	  	</div>
                	    	  
                	    	  </fieldset>
                	    	  
                	    	  <fieldset> 
                	    	  <legend>Type</legend>  	
                	    	  
                	    	  	<div class="row"> 
                	    	  	
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="condiment" value="1" <?php if ($condiment ==1) { echo 'checked'; } ?>> Condiment
                	    	  		</div>	   	
                	    	  	
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="dessert" value="1" <?php if ($dessert ==1) { echo 'checked'; } ?>> Dessert
                	    	  		</div>
                	    	  		
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="drink" value="1" <?php if ($drink ==1) { echo 'checked'; } ?>> Drink
                	    	  		</div>
                	    	  	
                	    	  	  <div class="small-12 columns">			
                	    	  			<input type="checkbox" name="main_dish" value="1" <?php if ($main_dish ==1) { echo 'checked'; } ?>> Main Dish
                	    	  		</div>
                	    	  			    	  			    	  		
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="salad" value="1" <?php if ($salad ==1) { echo 'checked'; } ?>> Salad
                	    	  		</div>
                	    	  		
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="salad_dressing" value="1" <?php if ($salad_dressing ==1) { echo 'checked'; } ?>> Salad Dressing
                	    	  		</div>
                	    	  			    	  			   			
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="side_dish" value="1" <?php if ($side_dish ==1) { echo 'checked'; } ?>> Side Dish
                	    	  		</div>
                	    	  		
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="spread_sauce" value="1" <?php if ($spread_sauce ==1) { echo 'checked'; } ?>> Spread & Sauce
                	    	  		</div>  
                	    	  		
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="soup" value="1" <?php if ($soup ==1) { echo 'checked'; } ?>> Soup
                	    	  		</div>    			
                	    	  				    		
                	    	  	</div>
                	    	  
                	    	  </fieldset>										
                	    	  
                	    	  <fieldset>  	
                	    	  <legend>Diet</legend>  	
                	    	  
                	    	  	<div class="row">
                	    	  	    		
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="raw" value="1" <?php if ($raw ==1) { echo 'checked'; } ?>> Raw
                	    	  		</div>
                	    	  		    			
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="vegan" value="1" <?php if ($vegan ==1) { echo 'checked'; } ?>> Vegan
                	    	  		</div>
                	    	  		
                	    	  		<div class="small-12 columns end">			
                	    	  			<input type="checkbox" name="vegetarian" value="1" <?php if ($vegetarian ==1) { echo 'checked'; } ?>> Vegetarian
                	    	  		</div>
                	    	  		
                	    	  	</div>
                	    	  
                	    	  </fieldset>					
                	    	  	    	
                	    	  <fieldset>  	
                	    	  <legend>Food Sensitivity</legend>  	
                	    	  
                	    	  	<div class="row">    	
                	    	  	
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="dairyfree" value="1" <?php if ($dairyfree ==1) { echo 'checked'; } ?>> Dairy Free
                	    	  		</div>
                	    	  	
                	    	  	  <div class="small-12 columns">			
                	    	  			<input type="checkbox" name="gluten_free" value="1" <?php if ($gluten_free ==1) { echo 'checked'; } ?>> Gluten Free
                	    	  		</div>
                	    	  		
                	    	  		<div class="small-12 columns">			
                	    	  			<input type="checkbox" name="nut_free" value="1" <?php if ($nut_free ==1) { echo 'checked'; } ?>> Nut Free
                	    	  		</div>
                	    	  		
                	    	  		<div class="small-12 columns end">			
                	    	  			<input type="checkbox" name="soy_free" value="1" <?php if ($soy_free ==1) { echo 'checked'; } ?>> Soy Free
                	    	  		</div>
                	    	  				    		
                	    	  	</div>
                	    	  
                	    	  </fieldset>		
                	
                	    	</form>		    	
                				    								
                			</div>
                		
                		<div class="small-12 medium-9 columns">
                			<?php 
                				echo $recipe_results_list; 
                				// lorem_ipsum(2);			
                			?>							
                		</div>
                	
                	</div>
                	
                	<br />
                			
                	<?php 
                	
                }
                  
                // Display Video resources if any
                if (!empty($video_block)) {
                	if($ie9){
                		echo $video_block;
                        // Display Document resources if any
                        if (!empty($document_block)) {
                        	 echo $document_block;
                        }
                	} else {
                		echo $video_block;
                		echo $video_modal_block;
                        // Display Document resources if any
                        if (!empty($document_block)) {
                        	 echo $document_block;
                        }
                	}
                }
         
                ?>
                
            </div>
            
            <?php if ($resource_addendum_block) { 
                // echo '<div class="content" id="resourceAddendum">content here ...</div>'; 
                echo '<div class="content" id="resourceAddendum">'.$resource_addendum_block.'</div>'; 
            } ?>
            
            <?php if ($product_addendum_block) { 
                 echo '<div class="content" id="productAddendum">'.$product_addendum_block.'</div>'; 
            } ?>

            <div class="content" id="discuss">
                <?
                if ($_SESSION['resource']['discussion_forum']['forum']) {
                    include('../php/resource_discussion_forum.php');
                }
                ?>
            </div>
            
        </div>
        				
	</div>
</div>