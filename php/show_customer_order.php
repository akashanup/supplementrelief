<?php 

// echo 'Begin show_customer_order.php.<br />';
// die;
// Report for tables:
// 1. orders
// 2. order_items
// 3. order_item_options

include_once('../includes/header.php');	

// show_array($_SESSION);

// $query_string = $_SERVER['QUERY_STRING'];
// echo $query_string;

// https://supplementrelief.com/show-customer-order/?order_id=1185

if (isset($_GET['order_id']))
{
	$order_id = safe_sql_data($connection, $_GET['order_id']);
}

// show_array($_GET);

// Determine if User edited a Category record
if (isset($_POST['action']) && $_POST['action'] == 'edit') {
	
	// show_array($_POST);
	// die;
				
	// set POST return values to local variables for processing
	$order_number = sanitize($_POST['order_number']);
	$order_status = sanitize($_POST['order_status']);
	$user_name = sanitize($_POST['user_name']);
	
	$shipped_by = sanitize($_POST['shipped_by']);
	$shipped_date = sanitize($_POST['shipped_date']);	
	$shipped_carrier = sanitize($_POST['shipped_carrier']);	
	$shipped_carrier_waybill = sanitize($_POST['shipped_carrier_waybill']);
	
	$product_retail_amount = sanitize($_POST['product_retail_amount']);
	$product_cost_amount = sanitize($_POST['product_cost_amount']);
	
	$shipping_cost_amount = sanitize($_POST['shipping_cost_amount']);
	$customer_service_allowance_amount = sanitize($_POST['customer_service_allowance_amount']);
	$promotion_allowance_amount = sanitize($_POST['promotion_allowance_amount']);
	$additional_charge_amount = sanitize($_POST['additional_charge_amount']);
	$revised_total_amount = sanitize($_POST['revised_total_amount']);
	
	$product_subtotal_amount = sanitize($_POST['product_subtotal_amount']);
	$discount_amount = sanitize($_POST['discount_amount']);
	$tax_amount = sanitize($_POST['tax_amount']);
	$shipping_amount = sanitize($_POST['shipping_amount']);
	$total_amount = sanitize($_POST['total_amount']);
	
	$notes_displayed = addslashes(cleanEncoding($_POST['notes_displayed']));
	$notes_internal = addslashes(cleanEncoding($_POST['notes_internal']));
	
	$billing_email = sanitize($_POST['billing_email']);
	$billing_phone = sanitize($_POST['billing_phone']);
	$billing_first_name = sanitize($_POST['billing_first_name']);
	$billing_last_name = sanitize($_POST['billing_last_name']);
	$billing_address = sanitize($_POST['billing_address']);	
	$billing_city = sanitize($_POST['billing_city']);
	$billing_state = sanitize($_POST['billing_state']);	
	$billing_postal_code = sanitize($_POST['billing_postal_code']);
	$billing_country = sanitize($_POST['billing_country']);
	
	$shipping_email = sanitize($_POST['shipping_email']);
	$shipping_phone = sanitize($_POST['shipping_phone']);
	$shipping_first_name = sanitize($_POST['shipping_first_name']);
	$shipping_last_name = sanitize($_POST['shipping_last_name']);
	$shipping_address = sanitize($_POST['shipping_address']);	
	$shipping_city = sanitize($_POST['shipping_city']);
	$shipping_state = sanitize($_POST['shipping_state']);	
	$shipping_postal_code = sanitize($_POST['shipping_postal_code']);
	$shipping_country = sanitize($_POST['shipping_country']);
					
	// Update the User record
	
	$modified_timestamp = date('Y-m-d H:i:s');
	
	$queryUpdateOrder = '
	UPDATE orders SET 
	order_status = "'.$order_status.'", 
	user_name = '.no_value_null_check($user_name).',
	shipped_by = "'.$shipped_by.'", 
	shipped_date = '.no_value_null_check(sql_date($shipped_date)).', 
	shipped_carrier = '.no_value_null_check($shipped_carrier).', 
	shipped_carrier_waybill = '.no_value_null_check($shipped_carrier_waybill).',
	product_retail_amount = '.no_value_null_check($product_retail_amount).',
	product_cost_amount = '.no_value_null_check($product_cost_amount).',
	shipping_cost_amount = '.no_value_null_check($shipping_cost_amount).',
	customer_service_allowance_amount = '.no_value_null_check($customer_service_allowance_amount).',
	promotion_allowance_amount = '.no_value_null_check($promotion_allowance_amount).',
	additional_charge_amount = '.no_value_null_check($additional_charge_amount).',
	revised_total_amount = '.no_value_null_check($revised_total_amount).',	
	product_subtotal_amount = '.no_value_null_check($product_subtotal_amount).',
	discount_amount = '.no_value_null_check($discount_amount).',
	total_tax = '.no_value_null_check($tax_amount).',
	shipping_total = '.no_value_null_check($shipping_amount).',
	total_amount = '.no_value_null_check($total_amount).',
	notes_displayed = '.no_value_null_check($notes_displayed).',
	notes_internal = '.no_value_null_check($notes_internal).',	
	email = '.no_value_null_check($billing_email).',		
	phone = '.no_value_null_check($billing_phone).',
	first_name = '.no_value_null_check($billing_first_name).',
	last_name = '.no_value_null_check($billing_last_name).',
	address = '.no_value_null_check($billing_address).',
	city = '.no_value_null_check($billing_city).',
	state = '.no_value_null_check($billing_state).',	
	zip_code = '.no_value_null_check($billing_postal_code).',
	country = '.no_value_null_check($billing_country).',
	shipping_email = '.no_value_null_check($shipping_email).',	
	shipping_phone = '.no_value_null_check($shipping_phone).',
	shipping_first_name = '.no_value_null_check($shipping_first_name).',
	shipping_last_name = '.no_value_null_check($shipping_last_name).',
	shipping_address = '.no_value_null_check($shipping_address).',
	shipping_city = '.no_value_null_check($shipping_city).',
	shipping_state = '.no_value_null_check($shipping_state).',	
	shipping_zip_code = '.no_value_null_check($shipping_postal_code).',
	shipping_country = '.no_value_null_check($shipping_country).',
	modified_by = '.$_SESSION['user']['user_id'].', 
	modified_timestamp = "'.$modified_timestamp.'" 
	WHERE id = "'.$order_id.'"';	
	
	// echo $queryUpdateOrder;	
	// die();
		
	$result_update_order = mysqli_query($connection, $queryUpdateOrder);
	
	if (!$result_update_order) {
		show_mysqli_error_message($queryUpdateOrder, $connection);
		die;
	}
	
	mysqli_free_result($result_update_order);
		
	// $target_uri = '../dashboard_orders/?'.$_SESSION['query_string'].'#listing';
	$target_uri = '../dashboard_orders/?keyword='.$order_number.'#listing';
	
	// echo 'Target URI: '.$target_uri.'<br /><hr />';	
			
	// Result Message to User
	$_SESSION['message_type'] = 'alert-box success radius';		
	$_SESSION['message'] = '<p>Your Order <b>'.$order_number.'</b> was successfully updated.</p>';
			
	// Return User to Listing page
	header("location: ".$target_uri);	
	exit();

} elseif (isset($_POST['action']) && $_POST['action'] == 'delete') {

	// echo 'Post ACTION is DELETE';
	// set POST return values to local variables for processing
	$order_number = sanitize($_POST['order_number']);
		
	$queryDeleteOrderItemOptions = '
	DELETE FROM order_item_options WHERE order_id = "'.$order_id.'"';
			
	// echo $queryDeleteOrderItemOptions.'<br /><hr />';
	
	$result_delete_order_item_option = mysqli_query($connection, $queryDeleteOrderItemOptions);
	
	if (!$result_delete_order_item_option) {
		show_mysqli_error_message($queryDeleteOrderItemOptions, $connection);
		die;
	}
	
	mysqli_free_result($result_delete_order_item_option);
	
	$queryDeleteOrderItems = '
	DELETE FROM order_items WHERE orders_id = "'.$order_id.'"';
			
	// echo $queryDeleteOrderItems.'<br /><hr />';
	
	$result_delete_order_item = mysqli_query($connection, $queryDeleteOrderItems);
	
	if (!$result_delete_order_item) {
		show_mysqli_error_message($queryDeleteOrderItems, $connection);
		die;
	}
	
	mysqli_free_result($result_delete_order_item);
	
	$queryDeleteOrder = '
	DELETE FROM orders WHERE id = "'.$order_id.'"';
			
	// echo $queryDeleteOrder.'<br /><hr />';
	
	$result_delete_order = mysqli_query($connection, $queryDeleteOrder);
	
	if (!$result_delete_order) {
		show_mysqli_error_message($queryDeleteOrder, $connection);
		die;
	}
	
	mysqli_free_result($result_delete_order);
	
	$target_uri = '../dashboard_orders/?'.$_SESSION['query_string'].'#listing';
			
	// Result Message to User
	$_SESSION['message_type'] = 'alert-box success radius';		
	$_SESSION['message'] = '<p>Your Order <b>'.$order_id.'</b> was successfully deleted.</p>';
			
	// Return User to Listing page
	header("location: ".$target_uri);	
	exit();

}

// Query the Order
$queryOrder = 'SELECT 
	ord.id,
	ord.order_status, 
	ord.created_timestamp AS order_timestamp, 
	ord.user_id AS order_user_id, 
	ord.order_project_id, 
	pj.name AS project_name,  
	ord.invoice_num AS invoice_number, 
	ord.shipped_by, 
	ord.shipped_date, 
	ord.shipped_carrier, 
	ord.shipped_carrier_waybill, 
	ord.notes_displayed, 
	ord.notes_internal, 
	ord.modified_by, 
	ord.modified_timestamp, 	
	ord.transaction_id, 
	ord.authorization_code, 
	ord.response_code, 
	ord.response_reason_code, 
	ord.response_subcode, 
	ord.response_reason_text, 
	ord.user_name, 
	ord.card_type,
	ord.exp_date, 
	ord.cc_mask,  
	ord.authorization_code, 
	ord.shipping_option_type_name, 
	ord.shipping_description, 
	ord.comments, 
	ord.free_sample,  
	ord.product_subtotal_amount, 
	ord.product_retail_amount, 
	ord.product_cost_amount, 
	ord.shipping_cost_amount, 
	ord.discount_amount, 
	ord.shipping_total, 
	ord.total_tax, 
	ord.total_amount,
	ord.additional_charge_amount, 
	ord.revised_total_amount, 
	ord.customer_service_allowance_amount, 
	ord.promotion_allowance_amount,  
	ord.email AS billing_email, 
	ord.phone AS billing_phone, 
	ord.first_name AS billing_first_name, 
	ord.last_name AS billing_last_name, 
	ord.shipping_option_type_name,
	ord.address AS billing_address, 
	ord.city AS billing_city, 
	ord.state AS billing_state, 
	ord.zip_code AS billing_postal_code, 
	ord.country AS billing_country, 
	ord.shipping_description, 
	ord.shipping_email, 
	ord.shipping_phone, 
	ord.shipping_first_name, 
	ord.shipping_last_name, 
	ord.shipping_address, 
	ord.shipping_city, 
	ord.shipping_state, 
	ord.shipping_zip_code AS shipping_postal_code, 
	ord.shipping_country 
	FROM orders ord 
	LEFT JOIN projects pj ON ord.order_project_id = pj.id 	
	WHERE ord.id = '.$order_id.' ';
			
// echo $query;
// die();

$result_order = mysqli_query($connection, $queryOrder);

if (!$result_order) {
	show_mysqli_error_message($queryOrder, $connection);
	die;
}
	
// Query the Order Items
$queryOrderItems = 'SELECT 
	oi.id,
	oi.orders_id, 
	oi.brand_product_usage_id,  
	bpu.name AS brand_product_usage_name, 
	oi.order_item_options, 
	oi.quantity, 
	oi.unit_price, 
	oi.amount, 
	urls.url AS project_brand_product_url,  
	oi.created_timestamp 
	FROM order_items oi 
	LEFT JOIN brand_product_usages bpu ON oi.brand_product_usage_id = bpu.id 
	LEFT JOIN project_brand_product_usages pbpu ON oi.brand_product_usage_id = pbpu.brand_product_usage_id 
	LEFT JOIN url_usages urlu ON pbpu.id = urlu.pbpu_id 
	LEFT JOIN urls urls ON urlu.url_id = urls.id 
	WHERE oi.orders_id = '.$order_id.' 
	GROUP BY oi.brand_product_usage_id 
	ORDER BY bpu.name ASC';
			
// echo $queryOrderItems;
// die();

$result_order_item = mysqli_query($connection, $queryOrderItems);

if (!$result_order_item) {
	show_mysqli_error_message($queryOrderItems, $connection);
	die;
}

while($r = mysqli_fetch_assoc($result_order)) {

	// show_array($r);
	
	$order_id = $r['id'];
	$order_timestamp = no_null_timestamp_check($r['order_timestamp']);
	$order_status = $r['order_status'];
    $order_user_id = $r['order_user_id'];

	$project_name = $r['project_name'];
	$invoice_number = $r['invoice_number'];
	
	$user_name = $r['user_name'];
	$card_type = $r['card_type'];
	$exp_date = $r['exp_date'];
	$cc_mask = $r['cc_mask'];
	
	$transaction_id = $r['transaction_id'];
	$authorization_code = $r['authorization_code'];
		
	$response_code = $r['response_code'];
	$response_reason_code = $r['response_reason_code'];
	$response_subcode = $r['response_subcode'];
	$response_reason_text = $r['response_reason_text'];
	
	$shipping_option_type_name = $r['shipping_option_type_name'];
	$shipping_description = $r['shipping_description'];
	
	$shipped_by = $r['shipped_by'];
	$shipped_date = hdate($r['shipped_date']);
	$shipped_carrier = $r['shipped_carrier'];
	$shipped_carrier_waybill = $r['shipped_carrier_waybill'];

	$comments = $r['comments'];
    $free_sample = $r['free_sample'];

	$notes_displayed = $r['notes_displayed'];
	$notes_internal = $r['notes_internal'];
	
	$product_subtotal_amount = $r['product_subtotal_amount'];
	
	$product_cost_amount = $r['product_cost_amount'];
	$shipping_cost_amount = $r['shipping_cost_amount'];

	$discount_amount = $r['discount_amount'];
	$shipping_amount = $r['shipping_total'];
	$tax_amount = $r['total_tax'];
	$total_amount = $r['total_amount'];
	
	$customer_service_allowance_amount = $r['customer_service_allowance_amount'];
	$promotion_allowance_amount = $r['promotion_allowance_amount'];
	
	$additional_charge_amount = $r['additional_charge_amount'];
	$revised_total_amount = $r['revised_total_amount'];
		
	$billing_email = $r['billing_email'];	
	$billing_phone = $r['billing_phone'];	
	$billing_first_name = $r['billing_first_name'];	
	$billing_last_name = $r['billing_last_name'];	
	$billing_address = $r['billing_address'];	
	$billing_city = $r['billing_city'];	
	$billing_state = $r['billing_state'];	
	$billing_postal_code = $r['billing_postal_code'];
	$billing_country = $r['billing_country'];		
		
	$shipping_option_type_name = $r['shipping_option_type_name'];	
	$shipping_description = $r['shipping_description'];
	
	$shipping_email = $r['shipping_email'];	
	$shipping_phone = $r['shipping_phone'];	
	$shipping_first_name = $r['shipping_first_name'];	
	$shipping_last_name = $r['shipping_last_name'];	
	$shipping_address = $r['shipping_address'];	
	$shipping_city = $r['shipping_city'];	
	$shipping_state = $r['shipping_state'];	
	$shipping_postal_code = $r['shipping_postal_code'];
	$shipping_country = $r['shipping_country'];					

	$card_type = $r['card_type'];	
	$authorization_code = $r['authorization_code'];	

	$product_subtotal_amount = $r['product_subtotal_amount'];
	$product_retail_amount = $r['product_retail_amount'];	
	
	$member_discount_amount	= ($product_retail_amount - $product_subtotal_amount);
	
	$discount_amount = $r['discount_amount'];	
	$shipping_total = $r['shipping_total'];	
	$total_tax = $r['total_tax'];	
	$total_amount = $r['total_amount'];	
	$email = $r['email'];
	
	if ($r['shipping_state'] == 'CA') {
		$prop65_message = '<br><br><span id="prop65">CA Proposition 65 Eligible. Apply product labels.</span>';
	} else {
		$prop65_message = '';
	}
	
	// $user_orders = getCustomerNumberOrders($connection, $c['order_status'], $c['user_id'], $c['created_timestamp'], $c['last_name'], $c['zip_code']);
	$user_orders = getCustomerNumberOrders($connection, $order_status, $order_user_id, $r['order_timestamp'], $billing_last_name, $billing_postal_code);			
	
}

$shipping_label = 
$shipping_first_name.' '.$shipping_last_name.'<br>'.
$shipping_address.'<br>'.
$shipping_city.', '.$shipping_state.' '.$shipping_postal_code.$prop65_message;

// echo $shipping_label;

// Get the total cost of the products on the order
// $project_discount_amount = 5.00;
// Get discount amount from projects.numedica_discount_amount
// $product_cost_amount = calculate_order_product_cost($project_discount_amount, $order_id, $connection);

// $total_order_product_cost = $total_order_product_cost * (1 - ($project_discount_amount / 100));

// $product_cost_amount = $product_cost_amount * (1 - $project_discount_amount/100);

?>
<div id="cart_modal" class="reveal-modal" data-reveal></div>
<div class="row">
	<div class="small-12 columns">
				
		<form id="edit-order-form" data-abide method="post" action="" autocomplete="off" >
		
			<input type="hidden" id="edit-order-action" name="action" value="edit" />
													
			<fieldset>		  
	    	<legend>Order Details</legend>
	    	
	    	<!--
	    	<div class="row">
	    	  <div class="small-12 columns">
	    	    	
	    	    <input class="button medium radius" type="submit" value="Update">
	    	    
	    	    <a href="#" class="button medium radius" data-reveal-id="delete_confirmation_modal"><b>Delete</b></a>
	    	      	    	    
	    	    <a href="<?php echo '../dashboard_orders/?keyword='.$invoice_number.'#listing'; ?>" class="button medium radius">Cancel</a>    
	    	      	    	      	    	      	    	  				    	    
	    	  </div>
	    	</div>
	    	-->	
	    		    		    	
	    	<div class="row">
	    	
	    	    <!-- 
	    		<div class="small-12 medium-2 columns">
	    			<label>Order ID
	    				<input type="text" name="order_id" value="<?php echo ($order_id ? $order_id : ''); ?>" readonly />
	    			</label>
	    		</div>
	    		--> 
	    		
	    		<div class="small-12 medium-3 columns">
	    			<label>Order Number
	    				<input type="text" name="order_number" value="<?php echo ($invoice_number ? $invoice_number : ''); ?>" readonly />
	    			</label>
	    		</div>
	    		
	    		<div class="small-12 medium-3 columns">
	    			<label>Order Date
	    				<input type="text" name="order_timestamp" value="<?php echo ($order_timestamp ? $order_timestamp : ''); ?>" readonly />
	    			</label>
	    		</div>
	    		
	    		<div class="small-12 medium-3 columns">
	    			<label>Project
	    				<input type="text" name="project_name" value="<?php echo ($project_name ? $project_name : ''); ?>" readonly />
	    			</label>
	    		</div>
	    		
	    		<div class="small-12 medium-3 columns">
	    			<label>Username
	    				<input type="text" name="user_name" value="<?php echo ($user_name ? $user_name : ''); ?>" readonly />
	    			</label>
	    		</div>
   			    			    		
	    	</div>
	    	
	    	<fieldset>		  
				<legend>Products</legend>

	    	<div class="row"> 			
                <div class="small-12 columns">			
            
            		<table width="100%">							
            		
            			<thead class="tableHeader">
            				<tr>
            					<th>Product</th>
            					<th><center><i class="fa fa-shopping-cart"></i></center></th>				  
            					<th>Options</th>
            					<th>Quantity</th>
            					<th>Unit<br />Price</th> 
            					<th>Amount</th>			    							  
            			  </tr>	
            			</thead>
            	
            			<?php										
            			while($c = mysqli_fetch_assoc($result_order_item)) {
            				
            				$order_item_options_display = '';
            			
            				// show_array($i);
            				
            				$order_item_id = $c['id'];
            				
            				// Query the Order Item Options
            				$queryOrderItemOptions = 'SELECT 
            					oio.id,
            					oio.order_option_name, 
            					pot.name AS order_option_type_name   
            					FROM order_item_options oio 
            					LEFT JOIN product_option_types pot ON oio.order_option_type_code = pot.code 
            					WHERE oio.order_item_id = '.$order_item_id.' 
            					ORDER BY oio.id ASC';
            							
            				// echo $queryOrderItemOptions;
            				// die();
            				
            				$result_order_item_option = mysqli_query($connection, $queryOrderItemOptions);
            				
            				if (!$result_order_item_option) {
            					show_mysqli_error_message($queryOrderItemOptions, $connection);
            					die;
            				}
            
            				while($o = mysqli_fetch_assoc($result_order_item_option)) {
            					
            					// show_array($o);
            
            					$order_item_options_display .= $o['order_option_type_name'].': '.$o['order_option_name'].'<br />';
            					
            				}
            				
            				mysqli_free_result($result_order_item_option);	
            				
            				?>
            				<tr>
            																						
            					<!-- <td><?php echo '<center><a href="../dashboard_order_detail/?order_id='.$c['id'].'" title="View Order Details" >View</a></center>'; ?></td>																
            					<td class="show-for-medium-up"><?php echo $c['id']; ?></td>
            													
            					<td><?php echo date('m/d/y h:i A T', strtotime($c['created_timestamp'])); ?></td> -->
            																					
            					<td><a href="../<?php echo $c['project_brand_product_url']; ?>/"><?php echo $c['brand_product_usage_name']; ?></a></td>
            					
            					<td><?php echo '<center><a href="../php/reorder.php?order_id='.$order_id.'&order_item_id='.$c['id'].'" title="Prepare a new order using this item and quantity." data-reveal-id="cart_modal" data-reveal-ajax="true">Reorder</a></center>'; ?></td>
            					
            					<td><?php echo $order_item_options_display; ?></td>
            																					
            					<td><?php echo $c['quantity']; ?></td>	
            					
            					<td><?php echo '$'.$c['unit_price']; ?></td>	
            					
            					<td class="show-for-medium-up"><?php echo '$'.$c['amount']; ?></td>										
            																														
            				</tr>
                    															
            				<?php
            					
            			}
            		
            			mysqli_free_result($result_order_item);	
            				
            			?>
            		
            		</table>
            	</div> 			
            </div>
            
            </fieldset>		    	    
	    	
	    	<fieldset>		  
				<legend>Status</legend>
	    	
	    		<div class="row">
		    		
		    		<!--
			    	<div class="small-12 medium-2 columns">				    	
				    	<?php echo $shipping_label; ?>
				    	<br><br>				    			
		    		</div>
		    		-->
		    						    		
			    	<div class="small-12 medium-3 columns">
				    	<label>Total Orders	
				    	<input type="text" name="total_orders" value="<?php echo ($user_orders ? $user_orders : ''); ?>" readonly />			    	
				    	</label>				    			
		    		</div>

		    		<div class="small-12 medium-3 columns">
			    		<label>Order Status 
		    		    <select id="order_status"  name='order_status' disabled>
		    		    	<option <?php if (isset($order_status) && $order_status == NULL): ?>selected="selected"<?php endif ?> value="">Select a Status...</option>
		    		    	<option <?php if (isset($order_status) && $order_status == '1'): ?>selected="selected"<?php endif ?> value="1">New</option>
		    		    	<option <?php if (isset($order_status) && $order_status == '2'): ?>selected="selected"<?php endif ?> value="2">In Progress</option>
		    		    	<option <?php if (isset($order_status) && $order_status == '3'): ?>selected="selected"<?php endif ?> value="3">Backordered</option>	    	
							<option <?php if (isset($order_status) && $order_status == '4'): ?>selected="selected"<?php endif ?> value="4">Complete</option>
							<option <?php if (isset($order_status) && $order_status == '5'): ?>selected="selected"<?php endif ?> value="5">Archived</option>
							<option <?php if (isset($order_status) && $order_status == '6'): ?>selected="selected"<?php endif ?> value="6">Cancelled</option>
							<option <?php if (isset($order_status) && $order_status == '7'): ?>selected="selected"<?php endif ?> value="7">Refunded</option>
							<option <?php if (isset($order_status) && $order_status == '9'): ?>selected="selected"<?php endif ?> value="9">Failed</option>
		    		    </select>
			    		</label>
			    		<small class="error">Order Status is required</small>
			    	</div>
	    						    	
			    	<div class="small-12 medium-3 columns">
		    			<label>Shipped Method
		    				<input type="text" name="shipping_option_type_name" value="<?php echo ($shipping_option_type_name ? $shipping_option_type_name : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<div class="small-12 medium-3 columns">
		    			<label>Shipped Method Description
		    				<input type="text" name="shipping_description" value="<?php echo ($shipping_description ? $shipping_description : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
	    		</div>
	    		
	    		<div class="row">
		    		
		    		<!--
	    			<div class="small-12 medium-2 columns">
		    			<label>Shipping & Handling Cost
		    				<input type="text" name="shipping_cost_amount" value="<?php echo ($shipping_cost_amount ? $shipping_cost_amount : ''); ?>" />
		    			</label>
		    		</div>
		    		-->
		    						    	
			    	<div class="small-12 medium-3 columns">
			    		<label>Shipped By 
		    		    <select id="shipped_by" name='shipped_by' disabled>
		    		    	<option <?php if (isset($shipped_by) && $shipped_by == NULL): ?>selected="selected"<?php endif ?> value="">Select a Shipper...</option>
		    		    	<option <?php if (isset($shipped_by) && $shipped_by == '1'): ?>selected="selected"<?php endif ?> value="1">NuMedica</option>
		    		    	<option <?php if (isset($shipped_by) && $shipped_by == '2'): ?>selected="selected"<?php endif ?> value="2">SupplementRelief.com</option>
		    		    	<option <?php if (isset($shipped_by) && $shipped_by == '3'): ?>selected="selected"<?php endif ?> value="3">Wright Check</option>	    	
							<option <?php if (isset($shipped_by) && $shipped_by == '4'): ?>selected="selected"<?php endif ?> value="4">Other</option>
		    		    </select>
			    		</label>
			    		<small class="error">Shipped By is optional</small>
			    	</div>
			    	
			    	<div class="small-12 medium-3 columns">
				    	<label>Shipped Date
				    	<input type="text" id="datepicker-effective-date"  name="shipped_date" placeholder="MM/DD/YYYY" value="<?php echo ($shipped_date ? $shipped_date : ''); ?>" readonly />
				    	</label>
				    	<small class="error">Shipped Date is optional and must be formatted as MM/DD/YYYY</small> 	  
			    	</div>		    	
			    	
			    	<div class="small-12 medium-3 columns">
			    		<label>Shipped Carrier
		    		    <select id="shipped_carrier" name='shipped_carrier' disabled>
		    		    	<option <?php if (isset($shipped_carrier) && $shipped_carrier == NULL): ?>selected="selected"<?php endif ?> value="">Select a Carrier...</option>
		    		    	<option <?php if (isset($shipped_carrier) && $shipped_carrier == '1'): ?>selected="selected"<?php endif ?> value="1">FedEx</option>
		    		    	<option <?php if (isset($shipped_carrier) && $shipped_carrier == '2'): ?>selected="selected"<?php endif ?> value="2">UPS</option>
		    		    	<option <?php if (isset($shipped_carrier) && $shipped_carrier == '3'): ?>selected="selected"<?php endif ?> value="3">USPS</option>	    	
							<option <?php if (isset($shipped_carrier) && $shipped_carrier == '4'): ?>selected="selected"<?php endif ?> value="4">Other</option>
		    		    </select>
			    		</label>
			    		<small class="error">Shipped Carrier is optional</small>
			    	</div>
			    	
			    	<div class="small-12 medium-3 columns">
		    			<label>Carrier Tracking ID
		    				<input type="text" name="shipped_carrier_waybill" placeholder="9400111899562281798277" value="<?php echo ($shipped_carrier_waybill ? $shipped_carrier_waybill : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
	    		</div>
		    		
                <div class="row">
                    
			    	<div class="small-12 medium-6 columns">
		    	
				    	<label>Customer Comments
		    				<textarea name="comments" readonly rows="3" /><?php echo ($comments ? $comments : ''); ?></textarea>
		    			</label>
		    							    			    				    			
		    		</div>
		    		
                    <div class="small-12 medium-6 columns">
		    	
				    	<label>Free Sample
		    				<textarea name="free_sample" readonly rows="3" /><?php echo ($free_sample ? $free_sample : ''); ?></textarea>
		    			</label>
		    							    			    				    			
		    		</div>

	    	    </div>
		    		
                <!-- <fieldset>
                    		  
                    <legend>Confirmation</legend> -->

		    		<div class="row">
			    		
			    		<div class="small-12 columns">

				    		<label>Notes
                                <textarea name="notes_displayed" readonly rows="3" placeholder="Notes are public and are displayed to customer." /><?php echo ($notes_displayed ? $notes_displayed : ''); ?>  </textarea>
				    		</label>
				    		
                        </div>
                        
                        <!--
                        <div class="small-12 medium-6 columns">
		
				    		<label>Notes <small>Internal</small>
			    				<textarea name="notes_internal" rows="3" placeholder="Notes are private and are NOT displayed to customer."/><?php echo ($notes_internal ? $notes_internal : ''); ?></textarea>
			    			</label>

				    	</div>
				    	-->
				    	
				    </div>
				    
				    <!--
				    <div class="row">
				    	
				    	<div class="small-12 columns">

				    		<a href="../php/generate_order_shipping_confirmation_email.php?order_id=<?php echo $order_id; ?>" class="button large radius expand">Email Shipping Confirmation</a>
				    		
				    	</div>
				    	
				    </div>
				    -->
				    
				    <?php // show_session_message(); ?>

		    	<!-- </fieldset> -->
	    						    	
		    	<fieldset>		  
					<legend>Financial</legend>
		    	
			    	<div class="row">
				    	
				    	<div class="small-12 medium-2 columns">
			    			<label>Retail Price
			    				<input readonly type="text" name="product_retail_amount" value="<?php echo ($product_retail_amount ? $product_retail_amount : ''); ?>" />
			    			</label>
			    		</div>
			    		
			    		<div class="small-12 medium-2 columns">
			    			<label>Member Discount
			    				<input readonly type="text" name="member_discount_amount" value="<?php echo ($member_discount_amount ? $member_discount_amount : '0.00'); ?>" />
			    			</label>
			    		</div>

			    		<div class="small-12 medium-2 columns">
			    			<label>Product Subtotal
			    				<input readonly type="text" name="product_subtotal_amount" value="<?php echo ($product_subtotal_amount ? $product_subtotal_amount : ''); ?>" />
			    			</label>
			    		</div>
			    		
			    		<div class="small-12 medium-2 columns">
			    			<label>Promotion Discount
			    				<input readonly type="text" name="discount_amount" value="<?php echo ($discount_amount ? $discount_amount : ''); ?>" />
			    			</label>
			    		</div>
			    			
			    		<div class="small-12 medium-2 columns">
			    			<label>Sales Tax
			    				<input readonly type="text" name="tax_amount" value="<?php echo ($tax_amount ? $tax_amount : ''); ?>" />
			    			</label>
			    		</div>
			    		
			    		<div class="small-12 medium-2 columns">
			    			<label>Shipping Charge
			    				<input readonly type="text" name="shipping_amount" value="<?php echo ($shipping_amount ? $shipping_amount : ''); ?>"  />
			    			</label>
			    		</div>
			    		
			    	</div>
			    	
			    	<div class="row">
			    							    								    	
				    	<div class="small-12 medium-4 columns">
			    			<label>Total Amount
			    				<input readonly type="text" name="total_amount" value="<?php echo ($total_amount ? $total_amount : ''); ?>" />
			    			</label>
			    		</div>	    		

				    	
				    	<div class="small-12 medium-4 columns">
			    			<label>Additional Charge Amount
			    				<input readonly type="text" name="additional_charge_amount" value="<?php echo ($additional_charge_amount ? $additional_charge_amount : ''); ?>" />
			    			</label>
			    		</div>
				    	    	
			    		<div class="small-12 medium-4 columns end">
			    			<label>Revised Total Amount
			    				<input readonly type="text" name="revised_total_amount" value="<?php echo ($revised_total_amount ? $revised_total_amount : ''); ?>" readonly />
			    			</label>
			    		</div>
			    		
			    		<!--					    	    	
			    		<div class="small-12 medium-2 columns">
			    			<label>Products (Cost)
			    				<input type="text" name="product_cost_amount" value="<?php echo ($product_cost_amount ? $product_cost_amount : ''); ?>" />
			    			</label>
			    		</div>
			    		
                        <div class="small-12 medium-2 columns">
			    			<label>Promotion Allowance (Samples)
			    				<input type="text" name="promotion_allowance_amount" value="<?php echo ($promotion_allowance_amount ? $promotion_allowance_amount : ''); ?>" />
			    			</label>
			    		</div>
			    		
			    		<div class="small-12 medium-2 columns end">
			    			<label>Customer Service Allowance
			    				<input type="text" name="customer_service_allowance_amount" value="<?php echo ($customer_service_allowance_amount ? $customer_service_allowance_amount : ''); ?>" />
			    			</label>
			    		</div>
			    		-->	
			    						    		
			    	</div>			    	
		    	</fieldset>			    						    				    					    	
		    			    	
	    	</fieldset>
	    		    				    	
	    	<div class="row">
	    	
	    		<div class="small-12 medium-6 columns">
	    		
	    			<fieldset>		  
	    				<legend>Billing</legend>
	    				
	    				<div class="row">
	    				
	    					<div class="small-12 columns">				
	    						<label>Email Address
	    							<input readonly type="email" name="billing_email" required value="<?php echo ($billing_email ? $billing_email : ''); ?>" />
	    						</label>	
	    						<small class="error">Billing Email is required</small>	
	    					</div>
	    					
	    					<div class="small-12 columns">				
	    						<label>Phone
	    							<input readonly type="tel" name="billing_phone" required value="<?php echo ($billing_phone ? $billing_phone : ''); ?>" />
	    						</label>	
	    						<small class="error">Billing Phone is required</small>	
	    					</div>
	    					
	    				</div>
    				
	    				<div class="row">
	    				
	    					<div class="small-6 columns">				
	    						<label>First Name
	    							<input readonly type="text" name="billing_first_name" required value="<?php echo ($billing_first_name ? $billing_first_name : ''); ?>" />
	    						</label>	
	    						<small class="error">Billing First Name is required</small>	
	    					</div>
	    					
	    					<div class="small-6 columns">				
	    						<label>Last Name
	    							<input readonly type="text" name="billing_last_name" required value="<?php echo ($billing_last_name ? $billing_last_name : ''); ?>" />
	    						</label>	
	    						<small class="error">Billing Last Name is required</small>	
	    					</div>
	    					
	    				</div>
	    				
	    				<div class="row">
	    				
	    					<div class="small-12 columns">				   
		    				    				    		
				    			<label>Address
				    				<textarea readonly name="billing_address" required placeholder="Billing Address..." rows="3" /><?php echo ($billing_address ? $billing_address : ''); ?></textarea>
				    				<small class="error">Billing Address is required</small>
				    			</label>
				    			
				    		</div>
				    		
				    	</div>
				    	
				    	<div class="row">
				    		
				    		<div class="small-12 columns">				   
			    				<label>City
			    					<input readonly type="text" name="billing_city" required value="<?php echo ($billing_city ? $billing_city : ''); ?>" />
			    				</label>	
			    				<small class="error">Billing City is required</small>
			    			</div>	    			
				    		
				    	</div>
				    	
				    	<div class="row">
				    	
				    		<div class="small-12 medium-4 columns">				   
				    			<label>State
				    				<input readonly type="text" name="billing_state" required value="<?php echo ($billing_state ? $billing_state : ''); ?>" />
				    			</label>	
				    			<small class="error">Billing State is required</small>	
				    		</div>
				    		
				    		<div class="small-12 medium-4 columns">				   
				    			<label>Postal Code
				    				<input readonly type="text" name="billing_postal_code" required value="<?php echo ($billing_postal_code ? $billing_postal_code : ''); ?>" />
				    			</label>	
				    			<small class="error">Billing Postal Code is re	quired</small>	
				    		</div>
				    		
				    		<div class="small-12 medium-4 columns">				   
				    			<label>Country
				    				<input readonly type="text" name="billing_country" required value="<?php echo ($billing_country ? $billing_country : ''); ?>" />
				    			</label>	
				    			<small class="error">Billing Country is required</small>	
				    		</div>
		    		
				    	</div>	    			
				    			    					    			
		    		</fieldset>    			
	    			
	    		</div>
	    		
	    		<div class="small-12 medium-6 columns">
	    		
	    			<fieldset>		  
	    				<legend>Shipping</legend>
	    				
	    				<div class="row">
	    				
	    					<div class="small-12 columns">				
	    						<label>Email Address
	    							<input readonly type="email" name="shipping_email" required value="<?php echo ($shipping_email ? $shipping_email : ''); ?>" />
	    						</label>	
	    						<small class="error">Shipping Email is required</small>	
	    					</div>
	    					
	    					<div class="small-12 columns">				
	    						<label>Phone
	    							<input readonly type="tel" name="shipping_phone" required value="<?php echo ($shipping_phone ? $shipping_phone : ''); ?>" />
	    						</label>	
	    						<small class="error">Shipping Phone is required</small>	
	    					</div>
	    					
	    				</div>
	    				
	    				<div class="row">
	    				
	    					<div class="small-6 columns">				
	    						<label>First Name
	    							<input readonly type="text" name="shipping_first_name" required value="<?php echo ($shipping_first_name ? $shipping_first_name : ''); ?>" />
	    						</label>	
	    						<small class="error">Shipping First Name is required</small>	
	    					</div>
	    					
	    					<div class="small-6 columns">				
	    						<label>Last Name
	    							<input readonly type="text" name="shipping_last_name" required value="<?php echo ($shipping_last_name ? $shipping_last_name : ''); ?>" />
	    						</label>	
	    						<small class="error">Shipping Last Name is required</small>	
	    					</div>
	    					
	    				</div>

	    				    		
	    				<div class="row">
	    				
	    					<div class="small-12 columns">				   
		    				    				    		
				    			<label>Address
				    				<textarea readonly name="shipping_address" required placeholder="Shipping Address..." rows="3" /><?php echo ($shipping_address ? $shipping_address : ''); ?></textarea>
				    				<small class="error">Shipping Address is required</small>
				    			</label>
				    			
				    		</div>
				    		
				    	</div>
				    	
				    	<div class="row">
				    	
			    			<div class="small-12 columns">				   
			    				<label>City
			    					<input readonly type="text" name="shipping_city" required value="<?php echo ($shipping_city ? $shipping_city : ''); ?>" />
			    				</label>	
			    				<small class="error">Shipping City is required</small>
			    			</div>
			    			
				    	</div>	    	
				    	
				    	<div class="row">
				    					    	
				    		<div class="small-12 medium-4 columns">				   
				    			<label>State
				    				<input readonly type="text" name="shipping_state" required value="<?php echo ($shipping_state ? $shipping_state : ''); ?>" />
				    			</label>	
				    			<small class="error">Shipping State is required</small>	
				    		</div>
				    		
				    		<div class="small-12 medium-4 columns">				   
				    			<label>Postal Code 
				    				<input readonly type="text" name="shipping_postal_code" required value="<?php echo ($shipping_postal_code ? $shipping_postal_code : ''); ?>" />
				    			</label>	
				    			<small class="error">Shipping Postal Code is required</small>	
				    		</div>
				    		
				    		<div class="small-12 medium-4 columns">				   
				    			<label>Country 
				    				<input readonly type="text" name="shipping_country" required value="<?php echo ($shipping_country ? $shipping_country : ''); ?>" />
				    			</label>	
				    			<small class="error">Shipping Country is required</small>	
				    		</div>
				    		
				    	</div>	    			
		    				
	    			</fieldset>    			
	    			
	    		</div>
	    		
	    	</div>
	    	
	    	<fieldset>		  
				<legend>Credit Card</legend>
	    	
		    	<div class="row">
			    	    	
		    		<div class="small-12 medium-2 columns">
		    			<label>Card Type
		    				<input type="text" name="card_type" value="<?php echo ($card_type ? $card_type : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<div class="small-12 medium-2 columns">
		    			<label>Card Number
		    				<input type="text" name="cc_mask" value="<?php echo ($cc_mask ? $cc_mask : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<div class="small-12 medium-2 columns">
		    			<label>Expiration Date
		    				<input type="text" name="exp_date" value="<?php echo ($exp_date ? $exp_date : ''); ?>" readonly />
		    			</label>
		    		</div>		    		
		    				    							    	    	
		    		<div class="small-12 medium-2 columns">
		    			<label>Transaction ID
		    				<input type="text" name="transaction_id" value="<?php echo ($transaction_id ? $transaction_id : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<div class="small-12 medium-2 columns end">
		    			<label>Authorization Code
		    				<input type="text" name="authorization_code" value="<?php echo ($authorization_code ? $authorization_code : ''); ?>" readonly />
		    			</label>
		    		</div>
		    				    		
		    	</div>
		    	
		    	<!--
		    	<div class="row">
			    	    	
		    		<div class="small-12 medium-2 columns">
		    			<label>Response Code
		    				<input type="text" name="response_code" value="<?php echo ($response_code ? $response_code : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<div class="small-12 medium-2 columns">
		    			<label>Response Reason Code
		    				<input type="text" name="response_reason_code" value="<?php echo ($response_reason_code ? $response_reason_code : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<div class="small-12 medium-2 columns">
		    			<label>Response Subcode
		    				<input type="text" name="response_subcode" value="<?php echo ($response_subcode ? $response_subcode : ''); ?>" readonly />
		    			</label>
		    		</div>
		    		
		    		<div class="small-12 medium-6 columns">
		    			<label>Response Reason Text
		    				<input type="text" name="response_reason_text" value="<?php echo ($response_reason_text ? $response_reason_text : ''); ?>" readonly />
		    			</label>
		    		</div>		    			    		
		    				    		
		    	</div>
		    	-->
		    	
	    	</fieldset>
				    		 				    				    					    					    	
			</fieldset>
			
			<div id ="delete_confirmation_modal" class="reveal-modal small" data-reveal>
				
				<div class="row" >
					<div class="small-12 columns" style="text-align:center" >
						<p>Are you sure you want to <b>DELETE</b> this Order?</p>
					</div>
				</div>
				<div class="row" >
					<div class="small-6 columns" >
						<input class="small button radius right" type="button" id="yes" value="Yes" />
					</div>	
					<div class="small-6 columns" >
						<input class="small button radius left" type="button" id="no" value="No" /> 
					</div>
				</div>
				<a class="close-reveal-modal">&#215;</a>
			</div>		
					
		</form>
	
	</div>
</div>