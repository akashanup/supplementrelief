<?php 

// echo 'Begin show_customer_orders.php.<br />';

// Query is executed earlier on /member-dashboard/index.php
											
if ($total_orders== 0) {
	?>
	<div class="row">
		<div class="small-12 columns">
			<h1>No Orders found.</h1>
		</div>
	</div>
	<?php
	 
} else {

	// Display records found
	
	?>
    <div id="cart_modal" class="reveal-modal" data-reveal></div>
	<div class="row"> 	<!-- display Orders row -->				
		<div class="small-12 columns"> <!-- display Search results columns -->			

			<table width="100%">							
			
				<thead class="tableHeader">
					<tr>
					  <th><center><i class="fa fa-search"></i></center></th>
					  <th><center><i class="fa fa-shopping-cart"></i></center></th>
					  <th><center>Order<br />Number</center></th>
					  <!-- <th>Session<br />ID</th>	-->		  
					  <th>Date<br />Placed</th>	  
					  <th>Status</th>
                      <th>Total<br />Amount</th>
					  <!-- <th class="show-for-medium-up">Card<br />Type</th> -->				
					  <!-- <th>Shipped<br />To</th> -->
					  <!-- <th class="show-for-medium-up">Username</th> -->
                      <!-- <th class="show-for-large-up">Device<br />Type</th>
					  <th class="show-for-large-up">Browser<br />Platform</th>
					  <th class="show-for-large-up">Browser<br />Name</th>
					  <th class="show-for-large-up">Browser<br />Version</th> -->			 				  
					  <th class="show-for-medium-up">Shipping<br />Method</th> 
					  <!-- <th class="show-for-medium-up">Shipped<br />By</th> -->
					  <th class="show-for-medium-up">Shipped<br />Date</th>
					  <th class="show-for-medium-up">Shipped<br />Carrier</th>
					  <th class="show-for-medium-up">Tracking<br />ID</th>  	   
					  <!-- <th>Product<br />Cost</th> -->			    							  
				  </tr>	
				</thead>
		
				<?php
					
				$total_orders = 0;
				
				$revenue_order = array("1", "2", "3", "4");
				// New, In-Progress, Backordered, Complete
														
				while($c = mysqli_fetch_assoc($result_query)) {
											
					// show_array($c);
																						
					switch ($c['shipped_by']) {		
					    case '1':	    	
					        $shipped_by_display = 'NuMedica';
					        break;		        
					    case '2':
							$shipped_by_display = 'SupplementRelief.com';
							break;
					    case '3':
							$shipped_by_display = 'Wright Check';
							break;
						case '4':
							$shipped_by_display = 'Other';
							break;
						default:
							$shipped_by_display = '';																			
					}
					
					switch ($c['order_status']) {		
					    case '1':	    	
					        $order_status_display = 'New';
					        break;		        
					    case '2':
							$order_status_display = 'In Progress';
							break;
					    case '3':
							$order_status_display = 'Backordered';
							break;
						case '4':
							$order_status_display = 'Complete';
							break;	
						case '5':
							$order_status_display = 'Archived';
							break;
						case '6':
							$order_status_display = 'Cancelled';
							break;			
						case '7':
							$order_status_display = 'Refunded';
							break;
						case '9':
							$order_status_display = 'Failed';
							break;																											
					}
					
					switch ($c['shipped_carrier']) {		
					    case '1':	    	
					        $shipped_carrier_display = 'FedEx';
					        break;		        
					    case '2':
							$shipped_carrier_display = 'UPS';
							break;
					    case '3':
							$shipped_carrier_display = 'USPS';
							break;
						case '4':
							$shipped_carrier_display = 'Other';
							break;
						default:
							$shipped_carrier_display = '';																	
					}				
																											
					if ($c['shipping_state'] == 'CA') {
						// echo 'State is CA';
						// die;
						$display_proposition_65 = 'prop65';
					} else {
						$display_proposition_65 = '';
					}						
																					
					?>							
					<tr>
    					
    					<?php
        				
        				/*	
        				if ($_SESSION['user']['user_id'] ==2) {
            				?><td><?php echo '<center><a href="../show-customer-order/?order_id='.$c['id'].'" title="View Order Details" >'.$c['invoice_num'].'</a></center>'; ?></td><?php		
        				} else {
        				    ?><td><center><?php echo $c['invoice_num']; ?></center></td><?php
                        }
                        */
                        ?>
																							
						<td><?php echo '<center><a href="../show-customer-order/?order_id='.$c['id'].'" title="View Order Details" >View</a></center>'; ?></td>
						<td><?php echo '<center><a href="../php/reorder.php?order_id='.$c['id'].'" title="Prepare a new order using the items and quantities from this order." data-reveal-id="cart_modal" data-reveal-ajax="true">Reorder</a></center>'; ?></td>
						<td><?php echo '<center>'.$c['invoice_num'].'</center>'; ?></td>																																							
						<!-- <td><?php echo '<center><a href="../session_page_views/?order_id='.$c['id'].'&session_id='.$c['user_session_id'].'" title="View Session Page Views" >'.$c['user_session_id'].'</a></center>'; ?></td>	-->
																										
						<td><?php echo date('m/d/y h:i A T', strtotime($c['created_timestamp'])); ?></td>
																												
						<td><?php echo $order_status_display ?></td>
						
                        <td><?php echo '$'.$c['total_amount']; ?></td>
								
						<!-- <td><?php echo $c['shipping_first_name'].' '.$c['shipping_last_name']; ?></td> -->
															
						<!-- <td class="show-for-medium-up"><?php echo $c['user_name']; ?></td> -->
						
                        <!-- <td class="show-for-large"><?php echo $c['browser_device_type']; ?></td>
						
						<td class="show-for-large"><?php echo $c['browser_platform']; ?></td>
						
                        <td class="show-for-large"><?php echo $c['browser_name']; ?></td>
                        
                        <td class="show-for-large"><?php echo $c['browser_version']; ?></td> -->
																												
						<td class="show-for-medium-up"><?php echo $c['shipping_option_type_name']; ?></td>
																	
						<td class="show-for-medium-up"><?php echo hdate($c['shipped_date']); ?></td>
						
						<td class="show-for-medium-up"><?php echo $shipped_carrier_display ?></td>
						
						<td class="show-for-medium-up"><?php echo $c['shipped_carrier_waybill']; ?></td>
																										
						<!-- <td><?php echo '$'.$c['product_cost_amount']; ?></td> -->					
																																										
					</tr>
					<?php
				}
		
				mysqli_free_result($result_query);	
																
				?>
		
			</table>
			
		</div> 			
	</div>
    <?php
	    
	
} // END if ($total_records == 0) {
?>