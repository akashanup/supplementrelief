<?php

	include_once('../includes/header.php');

	function showAllBookmarks($connection)
	{
		if (isset($_SESSION['enrollment']['project_program_id'])) {
			$project_program_id = $_SESSION['enrollment']['project_program_id'];
		}

		$current_timestamp = date('Y-m-d H:i:s');

		$queryEnrollmentBookmarks	= 'SELECT 
			bm.id, 
			bm.title, 
			bm.comments, 
			bm.url, 
			bm.created_timestamp, 
			bm.page_id,
			pg.name AS page_name  
			FROM bookmarks bm  
			LEFT JOIN pages pg on bm.page_id = pg.id  
			WHERE bm.enrollment_id = "'.$_SESSION['enrollment']['enrollment_id'].'"
			 ORDER BY bm.created_timestamp DESC';
					
		$result_list_enrollment_bookmarks = mysqli_query($connection, $queryEnrollmentBookmarks);

		if (!$result_list_enrollment_bookmarks) {
			show_mysqli_error_message($queryEnrollmentBookmarks, $connection);
			die;
		}


   		$html = 
   			'<h2>Show Bookmarks&nbsp;<small>'.$_SESSION['enrollment']['project_program_name'].'</small></h2>'.
    		'<hr>';
		if (mysqli_num_rows($result_list_enrollment_bookmarks) > 0) {
			
			$html .= 
			'<div class="row">'	.
				'<div class="small-12 columns">'.
					'<table width="100%">'.					
						'<thead class="tableHeader">'.
							'<tr>'.
							  '<th><center><i class="fa fa-eye"></i></center></th>'.
							  '<th><center><i class="fa fa-pencil"></i></center></th>'.
							  '<th>Created Timestamp</th>'.
							  '<th>Page Type</th>'.
							  '<th>Title<br />Description</th>'.
							'</tr>'.	
						'</thead>';

						while($r = mysqli_fetch_assoc($result_list_enrollment_bookmarks)) 
						{
												
							$bookmark = '';
							
							if ($r['title']) { $bookmark = '<strong>'.$r['title'].'</strong>'; }
							
							if ($r['comments'] AND $r['title']) {
								$bookmark .= '<br>'.$r['comments']; 
							} elseif ($r['comments']) {
								$bookmark = $r['comments'];
							}
							
							if ($bookmark) { $bookmark .= '<br>'; }
							
							$html .=
							'<tr>'.
								'<td><center><a href="'.$r['url'].'" title="View Page">View</a></center></td>'.
								'<td><center><a href="#" title="Edit Bookmark" data-id="'.$r['id'].'" class="editBookmarkBtn">Edit</a></center></td>'.
								'<td>'.date('m/d/y h:i A T', strtotime($r['created_timestamp'])).'</td>'.
								'<td>'.$r['page_name'].'</td>'.
								'<td>'.$bookmark.'</td>'.
							'</tr>';				   	  
						}
					$html .=
					'</table>'.
				'</div>'.
			'</div>';
		} 
		else {

			$html .= '
			<br />
			<div class="row">		
				<div class="small-12 columns">											
					<div class="panel">
						<p>No Bookmarks currently available for the <b>'.$_SESSION['project_program_name'].'</b> program.</p>
						
					</div>
				</div>
			</div>';	
		}

		$html .=
			'<a class="close-reveal-modal" aria-label="Close">x</a>';
		echo $html;
		mysqli_free_result($result_list_enrollment_bookmarks);
	}

	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
	{

		if(isset($_POST['key']) && $_POST['key'] == 'show')
		{
			showAllBookmarks($connection);
		}

		else
		{

			$bookmark_id = safe_sql_data($connection, $_POST['id']);

			$query	= 'SELECT
				bm.id,
				bm.title, 
				bm.comments,
				bm.url, 
				pg.name AS page_name  
				FROM bookmarks bm  
				LEFT JOIN pages pg on bm.page_id = pg.id  
				WHERE bm.enrollment_id = "'.$_SESSION['enrollment']['enrollment_id'].'" 
				 AND bm.id ="'.$bookmark_id.'" 
				 ORDER BY bm.created_timestamp DESC';
			
			$fetchQuery = mysqli_query($connection, $query);

			if (!$fetchQuery) {
				show_mysqli_error_message($query, $connection);
				die;
			}

			if (mysqli_num_rows($fetchQuery) > 0) 
			{
				$r 					= 	mysqli_fetch_assoc($fetchQuery);
				$bookmark_id 		=	$r['id'];
				$bookmark_title 	= 	$r['title'];
				$bookmark_comments 	=	$r['comments'];
				$page_name 			=	$r['page_name'];
				$url				=	$r['url'];

				generateEditBookmarkModal($bookmark_id, $bookmark_title, $bookmark_comments, $page_name, $url);
			}

		}
	}
?>