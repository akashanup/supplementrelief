<?php

// echo 'Begin show_project_brand_product.php<br />';
// die();

// if ( $_SESSION['user']['user_id'] == 2 ) { show_array($_SESSION); }

// show_array($_SERVER);
// show_array($_SESSION);
// show_array($_GET);

if (isset($_GET['p_id'])) {
	$project_id = $_GET['p_id'];
} else {
	$project_id = public_store_project_id; // store/includes/defines.php
}

// This $_SESSION variable is set at user login, else if a public user, and not set yet, set it here (default to the Project ID set in defines.php for the project)
$_SESSION['enrollment']['project_id'] = $project_id;

if(!$_SESSION['enrollment']['user_price_type']) $user_price_type = USER_PRICE_TYPE; // public User (not Logged In) store/includes/defines.php
else $user_price_type = $_SESSION['enrollment']['user_price_type']; // set at User Login

if (isset($_GET['bpu_id'])) { 
	$bpu_id = $_GET['bpu_id']; 
	$_SESSION['brand_product_usage']['brand_product_usage_id'] = $bpu_id;	
}

// Set the return URL to the Store page because the Browser Back button doesn't work in Foundation 5 once the user clicks a tab.
// Applied Foundation 5 javascript fix 7/10/15 - back button working now
$store_return_url = $_SERVER['HTTP_REFERER'].'?'.$_SERVER['QUERY_STRING'];
// echo $store_return_url;

if(isset($_GET['option_id']))
{
	$queryOptionProduct =
	'SELECT
	bpu.name,
	bpu.sku,
	bpu.upc,
	bpu.description_html,
	bpu.benefits_html,
	bpu.directions_html,
	bpu.retail_price,
	prd.manufacturer,
	im.host_url AS feature_image_host_url,
	im.caption AS feature_image_caption,
	im.alt_text AS feature_image_alt_text

	FROM brand_product_usage_options bpuo

	LEFT JOIN brand_product_usages bpu ON bpuo.variant_brand_product_usage_id = bpu.id
	LEFT JOIN products prd ON bpu.product_id = prd.id
	LEFT JOIN images im
	ON (bpu.image_id = im.content_asset_id AND
	im.format = "JPG" AND
	im.size = "Product" AND
	im.usage_size = "Medium")

	WHERE bpuo.id = '.$_GET['option_id'].
	' AND bpuo.brand_product_usage_id = '.$bpu_id.
	' AND variant_brand_product_usage_id IS NOT NULL
	LIMIT 1';

	$resultQueryOptionProduct = mysqli_query($connection, $queryOptionProduct);
	$resultOptionProduct = mysqli_fetch_assoc($resultQueryOptionProduct);
	$optionProductCount = mysqli_num_rows($resultQueryOptionProduct);
}

$queryProduct = 'SELECT 
pbpu.id, 
pbpu.brand_product_usage_id, 
pbpu.backordered, 
pbpu.backordered_date, 
pbpu.backordered_description, 
pbpu.new_item, 
pbpu.featured_item, 
pbpu.program_item, 
pbpu.pack_item, 
pbpu.sample_item, 
pbpu.education_item, 
pbpu.show_related_items, 
pbpu.savings_item, 
pbpu.page_meta_title, 
pbpu.group_options_description,  
pbpu.page_meta_description, 
pbpu.page_call_to_action,    
pbpu.effective_date, 
pbpu.end_date, 
pbpu.modified_timestamp,
bpu.brand_id, 
bpu.product_id,  
bpu.name AS project_brand_product_name, 
bpu.vegetable_capsule, 
bpu.gluten_free, 
bpu.vegetarian, 
bpu.albion_minerals,  
bpu.product_group_status,
bpu.sku, 
bpu.alternate_sku, 
bpu.upc, 
bpu.alternate_upc,  
bpu.retail_price,  		
bpu.short_description_html,
bpu.description_html, 
bpu.benefits_html, 
bpu.directions_html,
bpu.discussion_forum, 
dfc.discussion_thread_post, 
dfc.discussion_thread_post_document, 
dfc.discussion_thread_post_image, 
dfc.discussion_thread_post_video, 
dfc.discussion_reply_post, 
dfc.discussion_reply_post_document, 
dfc.discussion_reply_post_image, 
dfc.discussion_reply_post_video, 
pr.manufacturer, 
im.host_url AS feature_image_host_url, 
im.caption AS feature_image_caption, 
im.alt_text AS feature_image_alt_text,
vi.host_url_mp4, 
vi.host_url_webm, 
vi.duration_minutes_seconds, 
ca.title as video_title, 
im2.host_url as video_poster_url, 
pbpup.price 
FROM project_brand_product_usages pbpu 
LEFT JOIN brand_product_usages bpu ON pbpu.brand_product_usage_id = bpu.id 
LEFT JOIN discussion_forum_configurations dfc ON bpu.id = dfc.brand_product_id  		 
LEFT JOIN products pr ON bpu.product_id = pr.id 
LEFT JOIN images im 
	ON (bpu.image_id = im.content_asset_id AND 
			im.format = "JPG" AND 
			im.size = "Product" AND 
			im.usage_size = "Medium")     		
LEFT JOIN project_brand_product_usage_prices pbpup 
	ON (pbpu.project_id = pbpup.pbpu_project_id AND
			pbpu.brand_product_usage_id = pbpup.brand_product_usage_id AND 
			pbpup.price_type_code = "RETL" AND
			pbpup.price > 0)
LEFT JOIN videos vi ON (bpu.video_id = vi.content_asset_id ) 
LEFT JOIN content_assets ca ON (vi.content_asset_id = ca.id)  
LEFT JOIN images im2 
	ON (vi.image_id = im2.content_asset_id AND
		im2.format = "JPG" AND 
		im2.size = "Video")      	    		
WHERE pbpu.project_id = '.$project_id.' 
AND pbpu.brand_product_usage_id = '.$bpu_id.'  
AND pbpu.active = 1  
LIMIT 1';
			
// echo $queryProduct;

$result_product = mysqli_query($connection, $queryProduct);

if (!$result_product) {
	show_mysqli_error_message($queryProduct, $connection);
	die;
}

$product = mysqli_fetch_assoc($result_product);

// show_array($product);

$_SESSION['brand_product_usage']['brand_product_usage_id'] = $bpu_id;

if ($product['discussion_forum']) {
	// Set session variables to persist the Discussion Forum Configuration for this Product
	$_SESSION['brand_product_usage']['discussion_forum']['forum'] = $product['discussion_forum'];	
	$_SESSION['brand_product_usage']['discussion_forum']['thread_post'] = $product['discussion_thread_post'];
	$_SESSION['brand_product_usage']['discussion_forum']['thread_post_document'] = $product['discussion_thread_post_document'];	
	$_SESSION['brand_product_usage']['discussion_forum']['thread_post_image'] = $product['discussion_thread_post_image'];
	$_SESSION['brand_product_usage']['discussion_forum']['thread_post_video'] = $product['discussion_thread_post_video'];
	$_SESSION['brand_product_usage']['discussion_forum']['reply_post'] = $product['discussion_reply_post'];
	$_SESSION['brand_product_usage']['discussion_forum']['reply_post_document'] = $product['discussion_reply_post_document'];	
	$_SESSION['brand_product_usage']['discussion_forum']['reply_post_image'] = $product['discussion_reply_post_image'];
	$_SESSION['brand_product_usage']['discussion_forum']['reply_post_video'] = $product['discussion_reply_post_video'];		
} else {
	unset($_SESSION['brand_product_usage']['discussion_forum']);
}

if ($product['feature_image_host_url']) { 
    $feature_image = $_SESSION['application']['root_media_host_url'].$product['feature_image_host_url'];
} else {
  $feature_image = 'https://placehold.it/210x350'; 
}

// Query the Brand Product Variation Images
$queryVariationImages = 'SELECT 
bpuo.variant_brand_product_usage_id, 
bpu.name, 
bpu.upc,
im.seq,
im.host_url as feature_image_host_url, 
im.caption, 
im.alt_text 
FROM brand_product_usage_options bpuo  
LEFT JOIN brand_product_usages bpu ON bpuo.variant_brand_product_usage_id = bpu.id 
LEFT JOIN images im 
	ON (bpu.image_id = im.content_asset_id AND 
			im.format = "JPG" AND 
			im.size = "Product" AND 
			im.usage_size = "Large")     		
WHERE bpuo.brand_product_usage_id = '.$bpu_id.' 
AND bpuo.variant_brand_product_usage_id is NOT NULL 
AND (bpuo.effective_date <= CURDATE() AND (bpuo.end_date is NULL or bpuo.end_date >= CURDATE() )) 
GROUP BY feature_image_host_url 
ORDER BY bpu.name ASC'; 
// ORDER BY bpuo.product_option_type_code ASC, bpuo.option_sort ASC, bpuo.value_sort ASC, im.SEQ ASC';

// echo $queryVariationImages;

$result_product_image_variation = mysqli_query($connection, $queryVariationImages);

if (!$result_product_image_variation) {
	show_mysqli_error_message($queryVariationImages, $connection);
	die;
}

// Display any other images in the Ligthbox
while($i = mysqli_fetch_assoc($result_product_image_variation)) {

    /*
    if ($_SESSION['user']['user_id'] == 2) {
        echo $queryVariationImages;
       	show_array($i);
    }
    */
    
    /*
    $image_modal .= '
	<div id="productImageModal">
		<img src="'.$feature_image.'" alt="" />
		<div id="productImageModalCaption">'.$i['caption'].'</div>
	</div>';
	*/

	$image_modal .= '
	<div id="productImageModal">
		<img src="'.$_SESSION['application']['root_media_host_url'].$i['feature_image_host_url'].'" alt="" />
		<div id="productImageModalCaption">'.$i['caption'].'</div>
	</div>';

		
}

mysqli_free_result($result_product_image_variation);	
	
// Query the Brand Product Specifications

if(isset($_GET['option_id']))
{
	$queryOptionSpecifications =
	'SELECT 
	bps.id, 
	bps.specification_type_code,  
	bps.name, 
	bps.value 
	FROM brand_product_specifications bps 
	LEFT JOIN brand_product_usages bpu
	ON bps.brand_product_usage_id = bpu.id
	LEFT JOIN brand_product_usage_options bpuo
	ON bpuo.variant_brand_product_usage_id = bpu.id
	WHERE bpuo.id = '.$_GET['option_id'].
	' AND bpuo.brand_product_usage_id = '.$bpu_id.
	' AND bps.effective_date <= CURRENT_DATE 
	AND (bps.end_date is NULL or bps.end_date >= CURRENT_DATE) 
	ORDER BY bps.specification_type_code ASC, bps.seq ASC';


	$resultQueryOptionSpecifications = mysqli_query($connection, $queryOptionSpecifications);
	$optionSpecificationsCount = mysqli_num_rows($resultQueryOptionSpecifications);
}

$querySpecifications = 'SELECT 
bps.id, 
bps.specification_type_code,  
bps.name, 
bps.value 
FROM brand_product_specifications bps 
WHERE bps.brand_product_usage_id = '.$bpu_id.' 
AND bps.effective_date <= CURRENT_DATE 
AND (bps.end_date is NULL or bps.end_date >= CURRENT_DATE) 
ORDER BY bps.specification_type_code ASC, bps.seq ASC';
			
// echo $querySpecifications;

$result_product_specification = mysqli_query($connection, $querySpecifications);

// Set up output blocks for page display.

$ingredients = false;

// Loop through each record returned and process the specification.

if($optionSpecificationsCount > 0)
{
	$specificationArray = $resultQueryOptionSpecifications;
}
else
{
	if (!$result_product_specification) 
	{
		show_mysqli_error_message($querySpecifications, $connection);
		die;
	}
	$specificationArray = $result_product_specification;
}
while($specification = mysqli_fetch_assoc($specificationArray)) {
	
	if (!$ingredients) {
		$ingredients = true;
		$ingredients_header = '';
		$ingredients_table =
		'<table style="width:100%;">
			<thead>
				<tr>
				  <th align="left">Ingredient</th> 
				  <th align="left">Amount</th> 
				</tr>
			</thead>';		
		$ingredients_footer = '';
	}

	switch ($specification['specification_type_code']) {
	
		/*
		case 'IMAGE':
			
			// Image. Can be more than one record of this type ordered by seq. Type of image for processing is determined in the Name attribute.			
		    // Looking for bps.specification_type_code = "IMAGE" AND bps.name = "Medium-JPG"  
		    
		    if ($specification['name'] == "Medium-JPG") { $product_image_url_jpg = $specification['value']; }
		                  	        
		    break;
		*/
	
		case 'INGRH':
			
			// Ingredient Header. Only one record of this type.        
		    $ingredients_header .= '<div id="ingredientsHeader">'.$specification['value'].'</div>'; 
		    // $ingredients_header .= '<span class="label">'.$specification['value'].'</span>';  
 
		    // echo cleanEncoding($ingredients_header);
		          	                	        
		    break;
		
		case 'INGR':

	        // Ingredient. Can be more than one record of this type ordered by seq.
	        $ingredients_table .= '
	        <tr>
	        	<td>'
	        		.$specification['name'].'
	        	</td>
	        	<td>'
	        		.$specification['value'].'
	        	</td>
	        </tr>';        

	        break;
	        
	    case 'INGRF':
	    	
	    	// Ingredient Footer. Only one record of this type.        
	        // $ingredients_footer .= $specification['value'];
	        $ingredients_footer .= '<div id="ingredientsFooter">'.$specification['value'].'</div>';  
	            	                	        
	        break;	    
	        
	} // End switch ($specification['specification_type_code']) {
			
}

$ingredients_table .= '</table>';

mysqli_free_result($result_product_specification);

// Get the Project Brand Product Content Assets (if any)
$queryProductContentAssets = 'SELECT 
	ca.id, 
	ca.title, 
	ca.summary_description, 
	ca.author, 
	ca.content_asset_type_code, 
	bp.page_canonical_url AS blog_post_canonical_url, 
    imbp.host_url AS addendum_blog_post_image_url, 
	imbp.alt_text AS addendum_blog_post_image_alt_text,
    rec.page_canonical_url AS addendum_recipe_canonical_url,     
	imrec.host_url AS addendum_recipe_image_url, 
	imrec.alt_text AS addendum_recipe_image_alt_text,     
	rk.page_canonical_url AS resource_canonical_url,     
	imrk.host_url AS addendum_resource_image_url, 
	imrk.alt_text AS addendum_resource_image_alt_text    
	FROM content_asset_project_brand_products capbp  
	LEFT JOIN content_assets ca ON capbp.content_asset_id = ca.id 
	LEFT JOIN audios au ON ca.id = au.content_asset_id  
	LEFT JOIN documents do ON ca.id = do.content_asset_id 
	LEFT JOIN videos vi ON ca.id = vi.content_asset_id 
    LEFT JOIN blog_posts bp ON ca.id = bp.content_asset_id 
	LEFT JOIN images imbp 
	    ON (bp.image_id = imbp.content_asset_id AND 
            imbp.size = "Blog Post" AND 
            imbp.usage_size = "Small" AND 
            /* imbp.usage_shape = "Block" AND */
            imbp.format = "JPG")
    LEFT JOIN recipes rec ON ca.id = rec.content_asset_id 
	LEFT JOIN images imrec 
	    ON (rec.image_id = imrec.content_asset_id AND 
            imrec.size = "Recipe" AND 
            imrec.usage_size = "Small" AND 
            imrec.usage_shape = "Block" AND  
            imrec.format = "JPG") 	
	LEFT JOIN resource_kits rk ON ca.id = rk.content_asset_id 
	LEFT JOIN images imrk 
	    ON (rk.image_id = imrk.content_asset_id AND 
            imrk.size = "Resource" AND 
            imrk.usage_size = "Small" AND 
            imrk.usage_shape = "Block" AND  
            imrk.format = "JPG") 	
	WHERE capbp.project_brand_product_usage_id = '.$product['id'].' 
	ORDER BY ca.title ASC';
						
// echo $queryBlogPostContentAssets . '<br /><hr />';

$result_product_content_asset = mysqli_query($connection, $queryProductContentAssets);

if (!$result_product_content_asset) {
	show_mysqli_error_message($queryProductContentAssets, $connection);
	die;
}
					
while($a = mysqli_fetch_assoc($result_product_content_asset)) { 
			
	// show_array($a);
	
	if (!$resource_addendum_block) {
		// Create the Resource Addendum container as a stand alone.
		$resource_addendum_block .= '
		<!--googleoff: index-->
		    <div class="panel callout radius">
		        <div class="small-12 columns">
		            <h3><i class="fa fa-medkit"></i>  Related Wellness Resources</h3>
		            <p>We encourage you to <b>take advantage of these FREE Wellness Resources</b> on our website.</p>
	            </div>';
	}
	
	$addendum_resource_image_url = '';
	$addendum_resource_image_alt_text = '';
	$addendum_resource_canonical_url = '';
					
	switch ($a['content_asset_type_code']) {
	    
	    case 'BLOGP':
	    
	        $addendum_resource_image_url = $a['addendum_blog_post_image_url'];
			$addendum_resource_image_alt_text = $a['addendum_blog_post_image_alt_text'];
			$addendum_resource_canonical_url = $a['blog_post_canonical_url'];								
				
	    break;
	    
	    case 'RECIPE':
	    
	        // show_array($a);
	    
	        $addendum_resource_image_url = $a['addendum_recipe_image_url'];
			$addendum_resource_image_alt_text = $a['addendum_recipe_image_alt_text'];
			$addendum_resource_canonical_url = $a['addendum_recipe_canonical_url'];						
				
	    break;
	
	    case 'RESO':
	    
	        $addendum_resource_image_url = $a['addendum_resource_image_url'];
			$addendum_resource_image_alt_text = $a['addendum_resource_image_alt_text'];
			$addendum_resource_canonical_url = $a['resource_canonical_url'];						
				
	    break;
	    
	}							
					    					
	$resource_addendum_block .= '
	    <hr>
	    <div class="row">		
	        <div class="small-12 medium-2 columns text-center">
	            <img class="th" src="'.$_SESSION['application']['root_media_host_url'].$addendum_resource_image_url.'" alt="'.$addendum_resource_image_alt_text.'" width="125px">
	        </div>
	        <div class="small-12 medium-10 columns colWrapMargin">
	            <a href="'.$addendum_resource_canonical_url.'"><h4>'.$a['title'].'</a></h4>'.
	            $a['summary_description'].'
	        </div>
	    </div>';
	
}	

if ($resource_addendum_block) {     
	$resource_addendum_block .= '
	    </div> <!-- Close Panel -->
	    <!--googleon: index-->';
}

mysqli_free_result($result_product_content_asset); 

if ($product['show_related_items']) { 
		
	$queryRelatedItems = 'SELECT 
	bpu.id AS bpu_id, 
	bpu.name AS brand_product_name, 
	bpu.retail_price,  		
	bpu.short_description_html AS brand_product_tagline,  
	pbpu.page_meta_description, 
	im.host_url AS product_image_host_url, 
	im.alt_text AS product_image_alt_text, 
	urls.url AS seo_url 
	FROM project_brand_product_related_products pbprp 
	JOIN project_brand_product_usages pbpu ON pbprp.project_brand_product_child_id = pbpu.id 
	JOIN brand_product_usages bpu ON pbpu.brand_product_usage_id = bpu.id 
	LEFT JOIN images im 
		ON (bpu.image_id = im.content_asset_id AND 
			im.format = "JPG" AND 
			im.size = "Product" AND 
			im.usage_size = "Small") 
	LEFT JOIN url_usages urlu 
	    ON (pbpu.id = urlu.pbpu_id AND 
            urlu.effective_date <= CURRENT_DATE AND
           (urlu.end_date IS NULL or urlu.end_date >= CURRENT_DATE)) 
	LEFT JOIN urls urls ON urlu.url_id = urls.id 						    					   		
	WHERE pbprp.project_brand_product_parent_id = '.$product['id'].' 
	AND pbprp.effective_date <= CURRENT_DATE  
	AND (pbprp.end_date is NULL or pbprp.end_date >= CURRENT_DATE) 
	ORDER BY brand_product_name ASC';
						
	// echo $queryRelatedItems . '<br /><hr />';
	
	$result_related_item = mysqli_query($connection, $queryRelatedItems);
	
	if (!$result_related_item) {
		show_mysqli_error_message($queryRelatedItems, $connection);
		die;
	}
	
	$count_related_items = mysqli_num_rows($result_related_item);

}

// Display the Product on the page

// include('../php/e-commerce-header.php');
			
?>
<input type="hidden" id="bpu_id" value="<?php echo $bpu_id;?>">
<div class="row">

	<div class="small-12 medium-3 columns">
		
		<div class="show-for-medium-up">
			
			<div id="productFeatureImage">
					
				<?php
					if( isset($_GET['option_id']) && $optionProductCount == 1 && $resultOptionProduct['feature_image_host_url'])
					{
						$feature_image_host_url = $resultOptionProduct['feature_image_host_url'];
						$feature_image_alt_text = $resultOptionProduct['feature_image_alt_text'];
					}
					else
					{
						$feature_image_host_url = $product['feature_image_host_url'];
						$feature_image_alt_text = $product['feature_image_alt_text'];
					}
			    ?>

				<a href="javascript:void(0)" data-reveal-id="productImages" rel="self" title="View Additional Product Images" >
					<img src="<?php echo $_SESSION['application']['root_media_host_url'].$feature_image_host_url; ?>" alt="<?php echo $feature_image_alt_text; ?>" />
				</a>																
			</div>

			<div id="productAddtoCart">
				
				<?php								 
				echo integrate_add_to_cart_button($connection, array(
					'p_usage_id' => $product['brand_product_usage_id'],
					'pid' => $product['id'],
					'project_id' => $project_id,
					'p_name' => $product['project_brand_product_name'],
					'form_name' => 'form_large',
					'qty_visible' => TRUE,
					'button_html_content' => '',
					'retail_price' => $product['retail_price'],
					'price_type' => $user_price_type
				));
				
				/*
				$t = array(
					'p_usage_id' => $product['brand_product_usage_id'],
					'pid' => $product['id'],
					'project_id' => $_GET['project_id'],
					'p_name' => $product['project_brand_product_name'],
					'form_name' => 'form_large',
					'qty_visible' => TRUE,
					'button_html_content' => '',
					'retail_price' => $product['retail_price'],			
					'price_type' => $user_price_type 
				);
									
				echo '<pre>';
				print_r($t);
				echo '</pre>';
				*/
						
				?>
				
			</div>
						
		</div>
		
		<div class="show-for-small-only">
						
			<div id="productFeatureImage">
					
				<?php

					if( isset($_GET['option_id']) && $optionProductCount == 1 && $resultOptionProduct['feature_image_host_url'])
					{
						$feature_image_host_url = $resultOptionProduct['feature_image_host_url'];
						$feature_image_alt_text = $resultOptionProduct['feature_image_alt_text'];
					}
					else
					{
						$feature_image_host_url = $product['feature_image_host_url'];
						$feature_image_alt_text = $product['feature_image_alt_text'];
					}
				?>
				<a href="javascript:void(0)" data-reveal-id="productImages" rel="self" title="View Additional Product Images" >
					<img src="<?php echo $_SESSION['application']['root_media_host_url'].$feature_image_host_url; ?> " alt="<?php echo $feature_image_alt_text; ?>" />
				</a>		

																			
			</div>
			
			<br />
			
			<div id="productAddtoCart">
						
				<?php								 
				echo integrate_add_to_cart_button($connection, array(
					'p_usage_id' => $product['brand_product_usage_id'],
					'pid' => $product['id'],
					'project_id' => $project_id,
					'p_name' => $product['project_brand_product_name'],
					'form_name' => 'form_large',
					'qty_visible' => TRUE,
					'button_html_content' => '',
					'retail_price' => $product['retail_price'],
					'price_type' => $user_price_type
				));
				?>
								
			</div>
		
		</div>
							
		<div id="productIcons" class="text-centered">
			
			<!--<a href="#" title="View Product Video"><i class="fa fa-file-video-o fa-3x"></i></a>&nbsp;&nbsp;-->
			
			<?php
								
				echo integrate_view_cart_button(array(
					'store_path' => '../'.STORE_FOLDER_NAME.'/',
					'button_html_content' => '<i class="fa fa-shopping-cart fa-3x icon-blue"></i>'
				));
				
			?>
			
			&nbsp;&nbsp;<a href="javascript:void(0)" data-reveal-id="shippingHandling" rel="self" title="View Shipping and Handling Policy" <span class="fa fa-truck fa-3x icon-blue"></span></a>
			
			&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" data-reveal-id="productImages" rel="self" title="View Additional Product Images" <span class="fa fa-picture-o fa-3x icon-blue"></span></a>
			
			<?php
            /*
			if (strlen($product['upc']) > 0) {
				echo '<div id="productFeatureUPC">UPC: <b>'.$product['upc'].'</b></div>';
			}
			*/
			?>
						
		</div>
		
		<hr>
		
		<div>
			<a class="button radius expand" href="../numedica/" title="View the complete catalog listing of NuMedica supplements."><i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;VIEW ALL NuMedica</a>
			
			<form action="../numedica/" data-id="product">
				
				<input type="text" placeholder="Search by keyword, upc or sku" class="predictive_search" name="keyword" id="search" value="<?php if (isset($keyword)) { echo $keyword; } else { echo ''; } ?>" autocomplete="off">
				
				<input class="button medium radius expand" type="submit" value="SEARCH NuMedica" />	
	
			</form> 
			
		</div>
		
		<div class="show-for-medium-up">
		
			<!--
			<ul class="accordion" data-accordion>
			  	<li class="accordion-navigation">
			    	<a href="#numedicaCategories">
				    	
				    	<div class="row">
				  		   <div class="small-9 columns">
				  		     <div><h4>Categories</h4></div>
				  		   </div>  
				  		   <div class="small-3 columns">
				  		     <div class="accordionIcon"><i class="fa fa-plus right"></i></div>
				  		   </div>
				  		</div>
					</a>
					
				    <div id="numedicaCategories" class="content ">
				    -->
					    
					   <?php			
				
						// $category_project_id = 1;	
						$parent_code = 'GOS';
							
						$queryProjectCategoryGroupOrganSystems = 'SELECT 
							pcu.id AS gos_id, 
							cat.name AS category_name 
							FROM project_category_usages pcu 
							LEFT JOIN category_usages cu ON pcu.category_usage_id = cu.id 
							LEFT JOIN categories cat ON cu.child_code = cat.code  
							WHERE pcu.project_id = "'.$_SESSION['enrollment']['project_id'].'" 
							AND cu.parent_code = "'.$parent_code.'" 
							AND pcu.effective_date <= CURRENT_DATE  
							AND (pcu.end_date is NULL or pcu.end_date >= CURRENT_DATE)	
							ORDER BY category_name ASC';
												
						// echo $queryProjectCategoryGroupOrganSystems . '<br /><hr />';
						
						$result_group_organ_system = mysqli_query($connection, $queryProjectCategoryGroupOrganSystems);
						
						if (!$result_group_organ_system) {
							show_mysqli_error_message($queryProjectCategoryGroupOrganSystems, $connection);
							die;
						}
											
						while($a = mysqli_fetch_assoc($result_group_organ_system)) { 
							
							echo '&nbsp;&nbsp;<a href="../numedica/?gos_id='.$a['gos_id'].'" title="View NuMedica Supplements for '.$a['category_name'].'.">'.$a['category_name'].'</a><br>';
							
						}
						
						mysqli_free_result($result_group_organ_system);
						
						?>
					
					<!--	
			    	</div>
			    		
				</li>
			</ul>
			-->

			<br>
			
		</div>
						
	</div>
	
	<div class="small-12 medium-9 columns">
				
		<div class="panel">
		
			<div id="productMetaTitle">
				<?php if (isset($product['project_brand_product_name'])) { echo '<h1>'.$product['project_brand_product_name'].'</h1>';  } ?>			
			</div>
			
			<?php
				
			if ($product['savings_item'] || $product['new_item'] || $product['featured_item'] || $product['program_item'] || $product['pack_item'] || $product['sample_item'] || $product['education_item']) {
			
				echo '<div id="storeProductLabels">';
				
				if ($product['savings_item']) { echo '<span class="label success radius"><b>$avings</b></span>&nbsp;&nbsp;'; }
				
				if ($product['new_item']) { echo '<span class="label info radius"><b>New</b></span>&nbsp;&nbsp;'; }	
											
				if ($product['featured_item']) { echo '<span class="label info radius"><b>Popular</b></span>&nbsp;&nbsp;'; }
				
				if ($product['program_item']) { echo '<span class="label info radius"><b>Program</b></span>&nbsp;&nbsp;'; }
				
				if ($product['pack_item']) { echo '<span class="label info radius"><b>Pack</b></span>&nbsp;&nbsp;'; }
				
				if ($product['sample_item']) { echo '<span class="label info radius"><b>Sample</b></span>&nbsp;&nbsp;'; }
				
				if ($product['education_item']) { echo '<span class="label info radius"><b>Education</b></span>&nbsp;&nbsp;'; }
				
				echo '</div>';
				
			}				
								
			if ($product['vegetable_capsule'] || $product['gluten_free'] || $product['vegetarian'] || $product['albion_minerals']) {
			
				echo '<div id="storeProductLabels">';
				
					if ($product['vegetable_capsule']) { echo '<span class="label info radius"><b>Vegetable Capsule</b></span>&nbsp;&nbsp;'; }	
												
					if ($product['gluten_free']) { echo '<span class="label info radius"><b>Gluten Free</b></span>&nbsp;&nbsp;'; }
					
					if ($product['vegetarian']) { echo '<span class="label info radius"><b>Vegetarian</b></span>&nbsp;&nbsp;'; }
					
					if ($product['albion_minerals']) { echo '<span class="label info radius"><b>Albion Minerals</b></span>&nbsp;&nbsp;'; }
										
				echo '</div>';
				
			}
			
			if ($product['group_options_description']) { echo $product['group_options_description']; }
			
            if ($product['backordered']) { 
                
                $backordered_message = '<i class="fa fa-hand-o-right fa-1x"></i>&nbsp;&nbsp;';
                    
                if ($product['backordered_date']) { 
                    $backordered_message .= $product['backordered_description'];
                    $backordered_message .= ' Estimated available date: '.hdate($product['backordered_date']).'.';
                } else {
                    $backordered_message .= $product['backordered_description'];                                                     
                }
                
                ?>
                <div data-alert class="alert-box warning radius">
                    <?php echo $backordered_message; ?>
                </div>
                <?           
                    
            }
            
            echo '<div id="productShortDescription"><h2>'.strip_tags($product['short_description_html']).'</h2></div>';
	
			?>
	
			<div id="productMetaDescription">
			 <?php echo '<div id="productShortDescription">'.cleanEncoding($product['page_meta_description']).'</div>'; ?>
			</div>
			
			<div id="productCallToAction1" >
			<!-- <i class="fa fa-star"></i> -->
				<div style="color: white;">
					<?php echo cleanEncoding($product['page_call_to_action']); ?>
				</div>
			</div>
			
			<?php
			if ($product['upc']) {
    			?>
    			<div id="productFeatureUPC">
    				<?php 
    					echo '<b>UPC:</b> '.$product['upc'];
    					if ($product['alternate_upc']) { echo ', '.$product['alternate_upc']; };
    					if ($product['sku']) { echo ' <b>SKU:</b> '.$product['sku']; };
    					if ($product['alternate_sku']) { echo ', '.$product['alternate_sku']; };
    				?>
    				<br><br>
    			</div> 								
    			<?php				
            }
              
            if ($count_related_items > 0) { 
	            ?>
	            <a href="#relatedProducts"><i class="fa fa-search" aria-hidden="true"></i> view related products</a>
	            <?
		     }

            ?>
            												
		</div>
							
		<div id="product-tabs">
			<!-- Tab header -->
			<ul class="tabs" data-options="deep_linking:true; scroll_to_content:false;" data-tab>
    			<li class="tab-title active"><a href="#description">Description</a></li>
				<?php
					if (strlen($product['host_url_mp4']) > 0)	{
						echo '<li class="tab-title"><a href="#video">Video</a></li>';
					}
				?>
				<li class="tab-title"><a href="#benefits">Benefits</a></li>					
				<li class="tab-title"><a href="#directions">Directions</a></li>
				<?php
					if ($ingredients){
						echo '<li class="tab-title"><a href="#ingredients">Ingredients</a></li>';
					}
				?>
				<?php
					if ($resource_addendum_block){
						echo '<li class="tab-title"><a href="#resources">Resources</a></li>';
					}
					
					if ($_SESSION['brand_product_usage']['discussion_forum']['forum'] == true) {  					
						?>
						<li class="tab-title"><a href="#discuss">Discuss</a></li>
						<?php
					}
				?>
			</ul>
			
			<!-- Tab content -->
			<div class="tabs-content">
    			
                <div class="content active" id="description">
                    <!-- Placed first because this is the ACTIVE tab when the page loads and has the most optimized content for SEO -->
                    
					<div id="product-description">
						<?php // echo $clearing_lightbox; 

							if( isset($_GET['option_id']) && $optionProductCount == 1 && $resultOptionProduct['description_html'])
							{
								$description_html = $resultOptionProduct['description_html'];
							}
							else
							{
								
								$description_html = $product['description_html'];
							}
							
							echo cleanEncoding($description_html); 
						?>
					</div>
									
				</div>
					
				<?php
				if (strlen($product['host_url_mp4']) > 0) {
					
					?>			
					<div class="content" id="video">
					
						<div id="product-video">
																					
							<div id="featureVideo">
								<video controls style="width:100%;height:auto;" poster="<?php echo $_SESSION['application']['root_media_host_url'].$product['video_poster_url']; ?>"> 
								    <source src="<?php echo $_SESSION['application']['root_media_host_url'].$product['host_url_mp4']; ?>" type="video/mp4">  
								    <source src="<?php echo $_SESSION['application']['root_media_host_url'].$product['host_url_webm']; ?>" type="video/webm">   
									Your browser does not support the video tag.
							  	</video>
							</div>
							<div class="videoCaption">
								<div class="row">
									<div class="small-12 columns">
										<i class="fa fa-play-circle fa-1x"></i>&nbsp;&nbsp;<?php echo $product['video_title']; ?>&nbsp;&nbsp;
										<i class="fa fa-clock-o fa-1x"></i>&nbsp;&nbsp;<?php echo $product['duration_minutes_seconds']; ?>
									</div>
									<!--
									<div class="small-1 columns">
										<img src="https://cdn-manager.net.com/media/images/supplementrelief-logo-circular-yellow.png" title="SupplementRelief - Your Wellness Concierge" width="30" height="30"/>
									</div>
									-->
								</div>	
							</div>
												
						</div>
					
					</div>
					<?php
				}
				?>
										
				<div class="content" id="benefits">									
					<div id="product-benefits" class="panel">	
						<?php 
							if( isset($_GET['option_id']) && $optionProductCount == 1 && $resultOptionProduct['benefits_html'])
							{
								$benefits_html = $resultOptionProduct['benefits_html'];
							}
							else
							{
								
								$benefits_html = $product['benefits_html'];
							}
							
							echo cleanEncoding($benefits_html); 
						?>
					</div>
				</div>
				
				<div class="content" id="directions">
						
					<div id="product-directions">
						<?php 
							if( isset($_GET['option_id']) && $optionProductCount == 1 && $resultOptionProduct['directions_html'])
							{
								$directions_html = $resultOptionProduct['directions_html'];
							}
							else
							{
								
								$directions_html = $product['directions_html'];
							}

							echo cleanEncoding($directions_html); 
						?>
					</div>
						
				</div>
				
				<?php
					if ($ingredients){
						?>
						<div class="content" id="ingredients">
														
							<?php echo '<b>'.cleanEncoding($ingredients_header).'</b>'; ?>
						
							<div id="product-ingredients" >
								<?php 
									echo cleanEncoding($ingredients_table); 
								?>
							</div>
								
							<?php echo cleanEncoding($ingredients_footer); ?>
		
						</div>
						<?php
					}
				?>
				
				<?php

					if ($resource_addendum_block){
						?>
						<div class="content" id="resources">
							<?php echo $resource_addendum_block; ?>
						</div>
						<?
					}
					
					if ($_SESSION['brand_product_usage']['discussion_forum']['forum'] == true) {  					
						?>
						<div class="content" id="discuss">
													
							<div id="product-discussion-forum" >
								<?php 
									$_SESSION['forum_type'] = 'product';
									include('../php/discussion_forum.php'); 
								?>					
							</div>
						</div>
						<?php
					}
					
				?>
											
			</div> <!-- END <div class="tabs-content" -->
				
		</div> <!-- END <div id="product-tabs"> -->				
	
	</div>
	
</div>

<?php
			
if ($product['show_related_items']) { 
		
	if ($count_related_items > 0) { 
		
		// Create Orbit header
		$orbit_slider = '
		<div id="relatedProducts" class="row">		
			<div class="small-12 columns">
				<div class="panel radius">
					<h4><i class="fa fa-hand-o-right" aria-hidden="true"></i>&nbsp;&nbsp;Related NuMedica Supplements</h4>
				</div>
				<ul class="example-orbit-content" data-orbit>';
		
		while($ri = mysqli_fetch_assoc($result_related_item)) { 
		
			// show_array($ri); 
			
			$orbit_slider .= '
			<li data-orbit-slide="'.$ri['bpu_id'].'">
				<div class="small-12 medium-3 columns">
					<center><img src="'.$_SESSION['application']['root_media_host_url'].$ri['product_image_host_url'].'" alt="'.$ri['product_image_alt_text'].'"></center>
					<div class="caption">Retail: $'.$ri['retail_price'].'</div>					
				</div>
				<div class="small-12 medium-9 columns">
					<h3><a href="../'.$ri['seo_url'].'/" title="Learn more about '.$ri['brand_product_name'].'."">'.$ri['brand_product_name'].'</a></h3>
					<h4>'.strip_tags($ri['brand_product_tagline']).'</h4>'
					.$ri['page_meta_description'].'
					<a href="../'.$ri['seo_url'].'/" title="Learn more about '.$ri['brand_product_name'].'"><i class="fa fa-search" aria-hidden="true"></i> view product</a>		
				</div>																					
			</li>';
		
		}
		
		$orbit_slider .= '</ul></div></div><br>';
		
		echo $orbit_slider;
		
	}
							
}
	
?>

<div id="productImages" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <!-- <h2 id="modalTitle" class="panel">View Additional Product Images</h2> -->
  <!--<p class="lead">View Additional Images</p>-->
  
  <?php echo $image_modal; ?>
    
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<?php
	if( isset($_GET['option_id']) && $optionProductCount == 1 ) 
	{
		if($resultOptionProduct['name'])
		{
			$project_brand_product_name = $resultOptionProduct['name'];
		}
		else
		{
			$project_brand_product_name = $product['project_brand_product_name'];
		}
		if($resultOptionProduct['upc'])
		{
			$upc = $resultOptionProduct['upc'];
		}
		else
		{
			$upc = $product['upc'];
		}
		if($resultOptionProduct['manufacturer'])
		{
			$manufacturer = $resultOptionProduct['manufacturer'];
		}
		else
		{
			$manufacturer = $product['manufacturer'];
		}
		if($resultOptionProduct['sku'])
		{
			$sku = $resultOptionProduct['sku'];
		}
		else
		{
			$sku = $product['sku'];
		}
		if($resultOptionProduct['retail_price'])
		{
			$retail_price = $resultOptionProduct['retail_price'];
		}
		else
		{
			$retail_price = $product['retail_price'];
		}
	}
	else
	{
		$project_brand_product_name = $product['project_brand_product_name'];
		$upc = $product['upc'];
		$sku = $product['sku'];
		$retail_price = $product['retail_price'];
	}
?>

<!-- Product Microdata for SEO -->
<div id="product-microdata" itemscope itemtype="http://schema.org/Product" class="hide">
	<span id="microdata_name" itemprop="name"><?php echo $project_brand_product_name; ?></span>
	<meta id="microdata_gtin14" itemprop="gtin14" content="<?php echo $upc; ?>" />
	<meta id="microdata_manufacturer" itemprop="manufacturer" content="<?php echo $manufacturer ; ?>" />
	<meta id="microdata_mpn" itemprop="mpn" content="<?php echo $sku; ?>" />
	<meta id="microdata_sku" itemprop="sku" content="<?php echo $sku; ?>" />
	
	<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		<meta itemprop="priceCurrency" content="USD" />$
		<span id="microdata_offer_price" itemprop="price"><?php echo $retail_price; ?></span>
		<meta itemprop="itemCondition" itemtype="http://schema.org/OfferItemCondition" content="http://schema.org/NewCondition"/>New
		<meta itemprop="availability" content="http://schema.org/InStock"/>Available online
	</div>
</div>

<?php
	mysqli_free_result($result_product);
	// include_once '../php/standard_membership_modal.php';	
?>