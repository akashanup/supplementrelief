<?php
	
// echo 'Begin show_project_brand_products.php<br /><hr />';

// Begin Search Results display
// Script preceded by /php/search_products.php

// Create a log of the User's search terms for analysis.
// Make this a function. DRY
if (!empty($keyword)) {
	
	// date_default_timezone_set('America/Detroit');
	$created_timestamp = date('Y-m-d H:i:s');
				
	$queryInsert = '
	INSERT INTO search_term_logs (application_id, project_id, page_id, search_term, query_string, total_results_found, created_by, ip_address, created_timestamp) 
	VALUES ( 
	'.no_value_null_check($_SESSION['application']['application_id']).',
	'.no_value_null_check($_SESSION['enrollment']['project_id']).',
	'.no_value_null_check($_SESSION['page']['page_id']).',
	"'.$keyword.'",
 	'.no_value_null_check($_SERVER['QUERY_STRING']).',		
	"'.$total_records.'",
 	'.no_value_null_check($_SESSION['user']['user_id']).',	
	"'.$_SERVER['REMOTE_ADDR'].'",	
	"'.$created_timestamp.'")';
			
	// echo $queryInsert;	
	// die();
	
	$result_insert = mysqli_query($connection, $queryInsert);
			
	if (!$result_insert) {
		show_mysqli_error_message($queryInsert, $connection);
		die;
	}
	
	// mysqli_free_result($result_insert); // not needed on SQL insert or update or delete
	
}

// If no records found display user message.
if ($total_records == 0) {

	?>
	<div class="row">
		<div class="small-12 columns">
			<div class="panel">
				<?php
				
				if ($keyword) {
					?>
					<h2>No product found with Search Keyword(s) <?php echo '"'.$keyword.'"'; ?></h2>
		
					<?php						
				} else {
					?>
					<h2>No product found with Search Criterion selected</h2>			
					<?php			
				}
					
				?>
					
				<hr>
				<p>Please call <b>(888) 424-0032</b> or <a href="../contact/">email</a> if you need help finding anything. We do carry the <b>entire NuMedica product line</b> on this site. You can also <b>place your order over the phone</b>.</p>						
				<ol>
					<li>The blue <b>SHOW ALL BUTTON</b>, above to the right of the blue <b>SEARCH BUTTON</b>, resets the Search to display every NuMedica product in alphabetical order with <b>40 products per page</b>. <b>Scroll down to where the product occurs alphabetically</b> and/or <b>advance to the next page</b>.</li>					
					<li><b>Some NuMedica products are no longer manufactured and sold</b>. Click <a href="#" data-reveal-id="discontinuedProducts">here</a> to see which products have been discontinued and the recommended alternative.</li>
					<li>View <a href="javascript:void(0)" data-reveal-id="searchHelp" rel="self" title="view Search Help"></i>Search Help</a>.</li>
				</ol>		
					
			</div>
		</div>
	</div>

	<?php
	 
} else {

	// display products found
	$https_replace = false;
	if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
		$https_replace = true;
	}
	
	// echo '<hr>';		
	?>
	<!--
	<div class="row">		
		<div class="small-12 columns">
			<hr>					
		</div>
	</div>
	-->
	<?php		
	$divAdded=0;
	while($r = mysqli_fetch_assoc($result_product)) {
	
		// show_array($r);
		
		if ($r['seo_url']) {
    		$product_url = '../'.$r['seo_url'].'/';
		} else {	
    		$product_url = '../product/?p_id='.$r['project_id'].'&bpu_id='.$r['brand_product_usage_id'];
		}			
		?>
					
		<div class="row">
		
			<!-- <hr /> -->		
							
			<div class="small-12 medium-2 columns lazyDiv">
				
				<div class=" show-for-small-only">
					<center><h3><a href="<?php echo $product_url; ?>" title="Learn more about <?php echo $r['name']; ?>"><?php echo $r['name']; ?></a></h3></center>
				</div>
				
				<?php							
				if (empty($r['product_image_host_url'])) {
					$display_product_image_url = "https://placehold.it/160x200";
				} else {
					$display_product_image_url = $_SESSION['application']['root_media_host_url'].$r['product_image_host_url'];
				}
				echo '<center><a href="'.$product_url.'" title="Learn more about '.$r['name'].'"><img class="lazy" data-original="'.$display_product_image_url.'" alt="'.$r['product_image_alt_text'].'"/></a></center>';  
				?>
				
			</div>
					
			<div class="small-12 medium-7 columns">
				
				<div class="show-for-medium-up">	
					<h2 class="storeProductTitle"><a href="<?php echo $product_url; ?>" title="Learn more about <?php echo $r['name']; ?>"><?php echo $r['name']; ?></a>
					<?php /*Score: < ? php echo round($r['match1'],3) .'/'.round($r['match2'],3) ; */ ?>
					</h2>
				</div>
					
				<?php
				
					if ($r['savings_item'] || $r['new_item'] || $r['featured_item'] || $r['program_item'] || $r['pack_item'] || $r['sample_item'] || $r['education_item']) {
					
						echo '<div id="storeProductLabels">';
						
						if ($r['savings_item']) { echo '<span class="label success radius"><b>$avings</b></span>&nbsp;&nbsp;'; }
						
						if ($r['new_item']) { echo '<span class="label info radius"><b>New</b></span>&nbsp;&nbsp;'; }	
													
						if ($r['featured_item']) { echo '<span class="label info radius"><b>Popular</b></span>&nbsp;&nbsp;'; }
						
						if ($r['program_item']) { echo '<span class="label info radius"><b>Program</b></span>&nbsp;&nbsp;'; }
						
						if ($r['pack_item']) { echo '<span class="label info radius"><b>Pack</b></span>&nbsp;&nbsp;'; }
						
						if ($r['sample_item']) { echo '<span class="label info radius"><b>Sample</b></span>&nbsp;&nbsp;'; }
						
						if ($r['education_item']) { echo '<span class="label info radius"><b>Education</b></span>&nbsp;&nbsp;'; }
						
						echo '</div>';
						
					}
					
					if ($r['vegetable_capsule'] || $r['gluten_free'] || $r['vegetarian'] || $r['albion_minerals']) {
					
						echo '<div id="storeProductLabels">';
						
						if ($r['vegetable_capsule']) { echo '<span class="label info radius"><b>Vegetable Capsule</b></span>&nbsp;&nbsp;'; }	
													
						if ($r['gluten_free']) { echo '<span class="label info radius"><b>Gluten Free</b></span>&nbsp;&nbsp;'; }
						
						if ($r['vegetarian']) { echo '<span class="label info radius"><b>Vegetarian</b></span>&nbsp;&nbsp;'; }
						
						if ($r['albion_minerals']) { echo '<span class="label info radius"><b>Albion Minerals</b></span>&nbsp;&nbsp;'; }
												
						echo '</div>';
						
					}
																											
					echo '<div id="productShortDescription"><h3>'.strip_tags($r['short_description_html']).'</h3></div>';
					
					echo $r['page_meta_description'];
					
					if ($r['group_options_description']) { echo $r['group_options_description']; }
					
                    if ($r['backordered']) { 
                        
                        $backordered_message = '<i class="fa fa-hand-o-right fa-1x"></i>&nbsp;&nbsp;';
                        
                        if ($r['backordered_date']) { 
                            $backordered_message .= $r['backordered_description'];
                            $backordered_message .= ' Estimated available date: '.hdate($r['backordered_date']).'.';
                        } else {
                            $backordered_message .= $r['backordered_description'];                                                     
                        }
                        
                        ?>
                        <div data-alert class="alert-box warning radius">
                            <?php echo $backordered_message; ?>
                        </div>
                        <?           
                        
                    }
										
					if ($r['upc']) {
						?>
						<div id="productFeatureUPC">
							<?php 
								echo '<b>UPC:</b> '.$r['upc'];
								if ($r['alternate_upc']) { echo ', '.$r['alternate_upc']; };
								if ($r['sku']) { echo ' <b>SKU:</b> '.$r['sku']; };
								if ($r['alternate_sku']) { echo ', '.$r['alternate_sku']; };
							?>
							<br><br>
						</div> 								
						<?php				
					}
					
					/*
					if ($r['sku']) {
						?>
						<div id="productFeatureUPC">
							<?php 
								echo '<b>SKU:</b> '.$r['sku']; 
								if ($r['alternate_sku']) { echo ', '.$r['alternate_sku']; };
							?>
							<br><br>
						</div>
						<?php				
					}						
					*/
					
				?>
				
				<div class="small-only-text-center">
					<a href="<?php echo $product_url; ?>" title="Learn more about <?php echo $r['name']; ?>"><i class="fa fa-hand-o-right fa-lg"></i>&nbsp;&nbsp;more info</a>&nbsp;&nbsp;
					<?php echo integrate_view_cart_button(array(
						'store_path' => '../'.STORE_FOLDER_NAME.'/',
						'button_html_content' => '<i class="fa fa-shopping-cart fa-lg"></i>&nbsp;&nbsp;view cart'
						));
					?>
			
					<br><br>
				</div>
																					
				<!-- Price: $<?php echo '<b>'.$r['price'].'</b>'; ?> -->
																
			</div>
			
			<div class="small-12 medium-3 columns">
				<?php
				
					// 
					echo integrate_add_to_cart_button($connection, array(
						'p_usage_id' => $r['brand_product_usage_id'],
						'pid' => $r['id'], // project_brand_product_usages.id
						'project_id' => $r['project_id'],
						'p_name' => $r['name'], // bpu.name
						'form_name' => 'form_large',
						'qty_visible' => TRUE,
						'button_html_content' => '',
						'retail_price' => $r['price'],
						'price_type' => $user_price_type,
						'divAdded'	=>	$divAdded
					));
					$divAdded=1;
				?>
			</div>
			
			<hr>
			
		</div> <!-- ENDS row for Product data - 1 row created for each product -->
			
	<?php 
	} 
						
	mysqli_free_result($result_product);
			
}

// include_once '../php/standard_membership_modal.php';
include_once '../php/numedica_discontinued_products_modal.php';

?>

<!--
<script type="text/javascript" src="../js/slick.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
      $(".promotion-slider").show();
	  $('.promotion-slider').slick({
		 autoplay: true,
		 autoplaySpeed: 7000,	
		 dots: true, 
		 slidesToShow: 1,
		 slidesToScroll: 1
	  });
	  
	});
</script>
-->