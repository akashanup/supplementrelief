<?php
	
// echo 'Begin show_project_brand_products.php<br /><hr />';

// Begin Search Results display

// If no records found display user message.
if ($total_records == 0) {

	?>
	<div class="row">
		<div class="small-12 columns">
			<div class="panel">
				<?php
				
				if ($keyword) {
					?>
					<h2>No product found with Search Keyword(s) <?php echo '"'.$keyword.'"'; ?></h2>
		
					<?php						
				} else {
					?>
					<h2>No product found with Search Criterion selected</h2>			
					<?php			
				}
					
				?>
					
				<hr>
				<p>Please call <b>(888) 424-0032</b> or <a href="../contact/">email</a> if you need help finding anything. We do carry the <b>entire NuMedica product line</b> on this site. You can also <b>place your order over the phone</b>.</p>						
				<ol>
					<li>The blue <b>SHOW ALL BUTTON</b>, above to the right of the blue <b>SEARCH BUTTON</b>, resets the Search to display every NuMedica product in alphabetical order with <b>20 products per page</b>. <b>Scroll down to where the product occurs alphabetically</b> and/or <b>advance to the next page</b>.</li>					
					<li><b>On August 1, 2015, NuMedica revised their product catalogue</b>. Some products are no longer manufactured and sold. Click <a href="#" data-reveal-id="discontinuedProducts">here</a> to see which products have been discontinued and the recommended alternative.</li>
					<li>View <a href="javascript:void(0)" data-reveal-id="searchHelp" rel="self" title="view Search Help"></i>Search Help</a>.</li>
				</ol>		
					
			</div>
		</div>
	</div>

	<?php
	 
} else {

	// display records found
	$https_replace = false;
	if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
		$https_replace = true;
	}
						
	while($r = mysqli_fetch_assoc($result_product)) {
	
		// show_array($r);
					
		?>
		
        <div class="row">
		
			<!-- <hr /> -->		
							
			<div class="small-12 medium-3 columns">
				
				<div class=" show-for-small-only">
					<center><h3><a href="../product/?p_id=<?php echo $r['project_id'].'&bpu_id='.$r['brand_product_usage_id']; ?>" title="Learn more about <?php echo $r['name']; ?>"><?php echo $r['name']; ?></a></h3></center>
				</div>
				
				<?php							
				if (empty($r['product_image_host_url'])) {
					$display_product_image_url = "http://placehold.it/160x200";
				} else {
					$display_product_image_url = $r['product_image_host_url'];
				}
				if ($https_replace) {
					$display_product_image_url = str_replace('http://', 'https://', $display_product_image_url);
				}				
				// echo '<center><a href="../product/?p_id='.$r['project_id'].'&bpu_id='.$r['brand_product_usage_id'].'" title="Learn more about '.$r['name'].'"><img src="'.$display_product_image_url.'" /></a></center>';
				echo '<center><a href="../product/?p_id='.$r['project_id'].'&bpu_id='.$r['brand_product_usage_id'].'" title="Learn more about '.$r['name'].'"><img src="'.$_SESSION['application']['root_media_host_url'].$display_product_image_url.'" alt="'.$r['product_image_alt_text'].'" /></a></center>';  
				?>
				
			</div>
					
			<div class="small-12 medium-9 columns">
				
				<div class="show-for-medium-up">	
					<h2 class="storeProductTitle"><a href="../product/?p_id=<?php echo $r['project_id'].'&bpu_id='.$r['brand_product_usage_id']; ?>" title="Learn more about <?php echo $r['name']; ?>"><?php echo $r['name']; ?></a>
					<?php /*Score: < ? php echo round($r['match1'],3) .'/'.round($r['match2'],3) ; */ ?>
					</h2>
				</div>
					
				<?php
				
					if ($r['new_item'] || $r['featured_item'] || $r['program_item'] || $r['pack_item'] || $r['sample_item'] || $r['education_item']) {
					
						echo '<div id="storeProductLabels">';
						
						if ($r['new_item']) { echo '<span class="label info radius"><b>New</b></span>&nbsp;&nbsp;'; }	
													
						if ($r['featured_item']) { echo '<span class="label info radius"><b>Popular</b></span>&nbsp;&nbsp;'; }
						
						if ($r['program_item']) { echo '<span class="label info radius"><b>Program</b></span>&nbsp;&nbsp;'; }
						
						if ($r['pack_item']) { echo '<span class="label info radius"><b>Pack</b></span>&nbsp;&nbsp;'; }
						
						if ($r['sample_item']) { echo '<span class="label info radius"><b>Sample</b></span>&nbsp;&nbsp;'; }
						
						if ($r['education_item']) { echo '<span class="label info radius"><b>Education</b></span>&nbsp;&nbsp;'; }
						
						echo '</div>';
						
					}
					
					if ($r['vegetable_capsule'] || $r['gluten_free'] || $r['vegetarian'] || $r['albion_minerals']) {
					
						echo '<div id="storeProductLabels">';
						
						if ($r['vegetable_capsule']) { echo '<span class="label info radius"><b>Vegetable Capsule</b></span>&nbsp;&nbsp;'; }	
													
						if ($r['gluten_free']) { echo '<span class="label info radius"><b>Gluten Free</b></span>&nbsp;&nbsp;'; }
						
						if ($r['vegetarian']) { echo '<span class="label info radius"><b>Vegetarian</b></span>&nbsp;&nbsp;'; }
						
						if ($r['albion_minerals']) { echo '<span class="label info radius"><b>Albion Minerals</b></span>&nbsp;&nbsp;'; }
												
						echo '</div>';
						
					}
					
					if ($r['group_options_description']) { echo $r['group_options_description']; }
					
					/*					
                    if ($r['backordered']) { 
                        
                        $backordered_message = '<i class="fa fa-hand-o-right fa-1x"></i>&nbsp;&nbsp;';
                        
                        if ($r['backordered_date']) { 
                            $backordered_message .= $r['backordered_description'];
                            $backordered_message .= ' Estimated available date: '.hdate($r['backordered_date']).'.';
                        } else {
                            $backordered_message .= $r['backordered_description'];                                                     
                        }
                        
                        ?>
                        <div data-alert class="alert-box warning radius">
                            <?php echo $backordered_message; ?>
                        </div>
                        <?           
                        
                    }
					*/
																						
                    echo '<div id="productShortDescription"><h3>'.strip_tags($r['short_description_html']).'</h3></div>';

					echo $r['page_meta_description'];
					
					/*										
					if ($r['upc']) {
						?>
						<div id="productFeatureUPC">
							<?php 
								echo '<b>UPC:</b> '.$r['upc'];
								if ($r['alternate_upc']) { echo ', '.$r['alternate_upc']; };
								if ($r['sku']) { echo ' <b>SKU:</b> '.$r['sku']; };
								if ($r['alternate_sku']) { echo ', '.$r['alternate_sku']; };
							?>
							<br><br>
						</div> 								
						<?php	
					}
					*/
					
				?>
				
				<div class="small-only-text-center">
					<a href="../product/?p_id=<?php echo $r['project_id'].'&bpu_id='.$r['brand_product_usage_id']; ?>" title="Learn more about <?php echo $r['name']; ?>"><i class="fa fa-hand-o-right fa-lg"></i>&nbsp;&nbsp;more info</a>&nbsp;&nbsp;
			
					<br><br>
				</div>
																					
				<!-- Price: $<?php echo '<b>'.$r['price'].'</b>'; ?> -->
																
			</div>
			
			<hr>
						
		</div> <!-- ENDS row for Product data - 1 row created for each product -->
							
	<?php 
	} 
						
	mysqli_free_result($result_product);
			
}

?>