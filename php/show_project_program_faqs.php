<?php

// echo 'Begin show_project_program_faq.php<br /><hr />';

if (isset($_SESSION['enrollment']['project_program_id'])) { $pp_id = $_SESSION['enrollment']['project_program_id']; }

$current_timestamp = date('Y-m-d H:i:s');

$queryProgramFAQ = 'SELECT 
	ppca.id, 
	ppca.project_program_id, 
	ppca.content_asset_id, 
	ppca.seq, 
	ppca.scheduled_delivery_timestamp, 
	ppca.scheduled_delivery_complete_timestamp, 	
	ca.title as faq_title, 
	ca.text as faq_description  
	FROM project_program_content_assets ppca 
	LEFT JOIN content_assets ca on ppca.content_asset_id = ca.id 
	WHERE ppca.project_program_id = '.$pp_id.' 
	AND (ppca.scheduled_delivery_timestamp IS NULL OR ppca.scheduled_delivery_timestamp <= "'.$current_timestamp.'") 
	AND (ppca.scheduled_delivery_complete_timestamp IS NULL or ppca.scheduled_delivery_complete_timestamp >= "'.$current_timestamp.'") 
	AND ca.content_asset_type_code = "FAQ" 
	ORDER BY ppca.seq ASC';
			
// echo $queryProgramFAQ;
// die();

$result_show_project_program_faq = mysqli_query($connection, $queryProgramFAQ);

if (!$result_show_project_program_faq) {
	show_mysqli_error_message($queryProgramFAQ, $connection);
	die;
}

$active_tab = true;
$active_content = true;
$faq_items_display_block = '';

if (mysqli_num_rows($result_show_project_program_faq) > 0) {

	?>

	<br />
	<div class="row">
		<div class="small-12 columns">
			<!-- Tab header -->
			<ul class="tabs vertical" data-options="deep_linking:true; scroll_to_content:false;" data-tab>
			
				<?php
							
				while($f = mysqli_fetch_assoc($result_show_project_program_faq)) { 
			
					$faq_content_asset_id = $f['content_asset_id'];
					$faq_title = $f['faq_title'];
					// show_array($f);
					
					if ($active_tab) {
						$active_tab = false;
						echo '<li class="tab-title active"><a href="#FAQ-'.$faq_content_asset_id.'">'.$faq_title.'</a></li>';
					} else {
						echo '<li class="tab-title"><a href="#FAQ-'.$faq_content_asset_id.'">'.$faq_title.'</a></li>';
					}
					
					// echo '<br />';
														
					// Get the FAQ Items for each FAQ						
					$queryFAQItems = 'SELECT 
						cau.content_asset_parent_id, 
						cau.seq, 
						ca.title AS faq_item_title,  
						faqi.content_asset_id, 
						faqi.question, 
						faqi.answer 
						FROM content_asset_usages cau 
						LEFT JOIN content_assets ca on cau.content_asset_child_id = ca.id 
						LEFT JOIN faq_items faqi ON ca.id = faqi.content_asset_id 
						WHERE cau.content_asset_parent_id = '.$faq_content_asset_id.' 
						AND cau.effective_date <= CURRENT_DATE
						AND (cau.end_date IS NULL or cau.end_date >= CURRENT_DATE)  
						ORDER BY cau.seq ASC';	
						
					// echo $queryProgramFAQItems;
					// die();
						
					$result_show_faq_item = mysqli_query($connection, $queryFAQItems);
						
					if (!$result_show_faq_item) {
						show_mysqli_error_message($queryFAQItems, $connection);
						die;
					}
					
					$first_faq_item = true;
					
					$faq_item_counter = 0;		
						
					while($i = mysqli_fetch_assoc($result_show_faq_item)) {
					
						$faq_item_id = $i['content_asset_id'];
						
						$faq_item_counter = $faq_item_counter + 1;
						
						// show_array($i);
						// build $faq_items_display
						
						if ($active_content) {
							$active = ' active';
							$active_content = false;
							
						} else {
							$active = null;
						}
						
						if ($first_faq_item) {
							$first_faq_item = false;
							$faq_items_display_block .= '
							<div class="content'.$active.'" id="FAQ-'.$faq_content_asset_id.'">
								<ul class="accordion primary" data-accordion data-options="multi_expand:true;toggleable: true;">';								
						}
											
						$faq_items_display_block .= '
						<li class="accordion-navigation">
							<a href="#panel-'.$faq_item_id.'">'.$faq_item_counter.'. '.$i['faq_item_title'].'</a>
							<div id="panel-'.$faq_item_id.'" class="content">'.
						    	$i['answer'].'
							</div>
						</li>';
									
					}
					
					$faq_items_display_block .= '</ul></div>'; // END class="content '.$active.'" id="#FAQ-'.$faq_content_asset_id.'">';	
					$first_faq_item = true;	// reset to true for next FAQ if there is one					
					mysqli_free_result($result_show_faq_item);							
			
				} // END while($f = mysqli_fetch_assoc($result_show_project_program_faq)) { 
				
				?>
			
			</ul>

			<!-- Tab content -->
			<div class="tabs-content vertical">			
				<?php echo $faq_items_display_block; ?>		
			</div>
			
		</div>		
	</div>
	
	<?php
	
	// echo $faq_items_display_block;
				
} else {

	// No FAQs found for this Project Program Offering

	?>
	<br />
	<div class="row">		
		<div class="small-12 columns">											
			<div class="panel">
				
				<?php
				if ($_SESSION['enrollment']['project_program_name']) {
					?>
					<p>No <b>FAQs</b> currently available for the <b><?php echo $_SESSION['enrollment']['project_program_name']; ?></b>.</p>
					<?php
					
				} else {
					?>
					<p>No <b>FAQs</b> currently available.</p>
					<?php
				}
				?>

			</div>
		</div>
	</div>
	<?php	
}

mysqli_free_result($result_show_project_program_faq);

?>