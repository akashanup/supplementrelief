<?php

// echo 'Begin show_project_program_news_posts.php<br /><hr />';
// die;

if (isset($_SESSION['enrollment']['project_program_id'])) {
	$project_program_id = $_SESSION['enrollment']['project_program_id'];
}

// date_default_timezone_set('America/Detroit');
$current_timestamp = date('Y-m-d H:i:s');
// echo 'Current Date and Time in Detroit : '.$current_timestamp_detroit.'<br /><hr />';

$queryNewsPosts	= 'SELECT 
	ppca.id, 
	ppca.project_program_id, 
	ppca.content_asset_id, 
	ppca.seq, 
	ppca.effective_date, 
	ppca.scheduled_delivery_timestamp, 
	ppca.scheduled_delivery_complete_timestamp, 	
	ca.title, 
	ca.summary_description, 
	ca.description    
	FROM project_program_content_assets ppca 
	LEFT JOIN content_assets ca on ppca.content_asset_id = ca.id 
	WHERE ppca.project_program_id = '.$project_program_id.' 
	AND ppca.scheduled_delivery_timestamp <= "'.$current_timestamp.'" 
	AND (scheduled_delivery_complete_timestamp IS NULL or scheduled_delivery_complete_timestamp >= "'.$current_timestamp.'") 
	AND ca.content_asset_type_code = "NEWSP" 
	ORDER BY ppca.scheduled_delivery_timestamp DESC';
			
// echo $queryNewsPosts . '<br /><hr />';

$result_list_project_program_news = mysqli_query($connection, $queryNewsPosts);

if (!$result_list_project_program_news) {
	show_mysqli_error_message($queryNewsPosts, $connection);
	die;
}

$news_block = '';

if (mysqli_num_rows($result_list_project_program_news) > 0) {

	while($r = mysqli_fetch_assoc($result_list_project_program_news)) {
		
		// show_array($r);
		
		// <h3>'.cleanEncoding($r['title']).' <small> posted: '.date('m/d/y h:i A T', strtotime($r['scheduled_delivery_timestamp'])).'</small></h3>'
		
		$news_block .= 
		  '<div class="row">
			 <div class="large-12 columns">
			   <div class="panel">
			   		<h3>'.cleanEncoding($r['title']).' <small> posted: '.date('m/d/y', strtotime($r['scheduled_delivery_timestamp'])).'</small></h3>'.cleanEncoding($r['summary_description']).'
			   </div>
			 </div>
		   </div>';	  
	}
	
} else {

	echo '
	<br />
	<div class="row">		
		<div class="small-12 columns">											
			<div class="panel">
				<p>No News currently available for the <b>'.$_SESSION['project_program_name'].'</b> program.</p>
				
			</div>
		</div>
	</div>';	
	
}

if (!empty($news_block)) {
	echo '<br />'.$news_block;
}

mysqli_free_result($result_list_project_program_news);

?>