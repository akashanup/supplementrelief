<?php

// echo 'Begin show_project_program_recipe.php<br /><hr />';

// Calling Page: Resource
// Calling Script: show_project_proram_resource.php
// https://supplementrelief.com/recipe/?re_id=1349

include_once('../includes/core.php');	

// recipe
if (isset($_GET['re_id'])) {
	$recipe_id = $_GET['re_id'];
	$_SESSION['recipe']['recipe_id'] = $recipe_id;
}

// show_array($_GET);
// die();

$query_string = $_SERVER['QUERY_STRING'];

// Get the Recipe Title and Description for user display.
$queryRecipe = 'SELECT 
	ca.title AS recipe_title, 
	ca.author AS recipe_author, 
	ca.summary_description, 
	ca.description, 
	ca.text, 
	rec.image_id AS recipe_image_id, 
	rec.video_id AS recipe_video_id, 
	rec.ingredients, 
	rec.directions, 
	rec.number_servings, 
	rec.preparation_time_minutes, 
	rec.cooking_time_minutes, 
	rec.ready_time_minutes, 
	rec.breakfast, 
	rec.lunch, 
	rec.dinner, 
	rec.appetizer, 
	rec.snack, 
	rec.condiment, 
	rec.dessert, 
	rec.drink, 
	rec.main_dish, 
	rec.salad, 
	rec.salad_dressing, 
	rec.side_dish, 
	rec.spread_sauce, 
	rec.soup, 
	rec.raw, 
	rec.vegan, 
	rec.vegetarian, 
	rec.dairy_free, 
	rec.gluten_free, 
	rec.nut_free, 
	rec.soy_free, 
	rec.discussion_forum,  
	im.host_url AS clearing_featured_image_thumbnail_url, 
	vi.host_url_mp4, 
	vi.host_url_webm, 
	vi.duration_minutes_seconds, 
	vi.image_id,  
	ca1.title as video_title, 
	im1.host_url as recipe_image_url, 
	im1.caption as recipe_image_caption, 
	im1.alt_text as recipe_image_alt_text,         	
	im2.host_url as video_poster_image_url, 
	dfc.discussion_thread_post, 
	dfc.discussion_thread_post_document, 
	dfc.discussion_thread_post_image, 
	dfc.discussion_thread_post_video, 
	dfc.discussion_reply_post, 
	dfc.discussion_reply_post_document, 
	dfc.discussion_reply_post_image, 
	dfc.discussion_reply_post_video         	
	FROM content_assets ca 
	LEFT JOIN recipes rec ON ca.id = rec.content_asset_id  
	LEFT JOIN images im ON 
		(rec.image_id = im.content_asset_id AND 
		 im.size = "Recipe" AND 
		 im.usage_size = "Small" AND
		 im.usage_shape = "Block" AND
		 im.format = "PNG")
	LEFT JOIN videos vi on rec.video_id = vi.content_asset_id 
	LEFT JOIN content_assets ca1 ON vi.content_asset_id = ca1.id 
	LEFT JOIN images im1 ON 
		(rec.image_id = im1.content_asset_id AND 
		 im1.size = "Recipe" AND 
		 im1.usage_size = "Large" AND 
		 im1.usage_shape = "Landscape" AND 
		 im1.format = "JPG") 
	LEFT JOIN images im2 ON 
		(vi.image_id = im2.content_asset_id AND 
		 im2.size = "Video" AND 
		 im2.usage_size = "Large" AND 
		 im2.usage_shape = "Landscape" AND 
		 im2.format = "JPG") 
	LEFT JOIN discussion_forum_configurations dfc ON rec.content_asset_id = dfc.recipe_id 			 
	WHERE ca.id = '.$recipe_id.'  
	LIMIT 1';
	
// echo $queryRecipe;
// die();

$result_recipe = mysqli_query($connection, $queryRecipe);

if (!$result_recipe) {
	show_mysqli_error_message($queryRecipe, $connection);
	die;
}
	
while($r = mysqli_fetch_assoc($result_recipe)) {

	// show_array($r);
	
	$recipe_image_id = $r['recipe_image_id'];
	$recipe_image_url = $r['recipe_image_url'];	
	$recipe_image_caption = $r['recipe_image_caption'];	
	$recipe_image_alt_text = $r['recipe_image_alt_text'];		
	
	$recipe_video_id = $r['recipe_video_id'];
	$host_url_mp4 = $r['host_url_mp4'];
	$host_url_webm = $r['host_url_webm'];
	$duration_minutes_seconds = $r['duration_minutes_seconds'];
	$video_title = $r['video_title'];
	$video_poster_image_url = $r['video_poster_image_url'];	
		
	$recipe_title = $r['recipe_title'];	
	$recipe_author = $r['recipe_author'];	

	$summary_description = $r['summary_description'];	 
	$description = $r['description'];	
	$clearing_featured_image_thumbnail_url = $r['clearing_featured_image_thumbnail_url'];	 	
	
	$preparation_time_minutes = $r['preparation_time_minutes'];	 
	$cooking_time_minutes = $r['cooking_time_minutes']; 
	$ready_time_minutes = $r['ready_time_minutes']; 
	
	$ingredients = $r['ingredients']; 
	$directions = $r['directions']; 
	$number_servings = $r['number_servings']; 
		
	$breakfast = $r['breakfast'];
	$lunch = $r['lunch'];
	$dinner = $r['dinner'];
	$appetizer = $r['appetizer'];
	$snack = $r['snack'];
	$condiment = $r['condiment'];
	$dessert = $r['dessert'];
	$drink = $r['drink'];
	$main_dish = $r['main_dish'];
	$salad = $r['salad'];
	$salad_dressing = $r['salad_dressing'];	
	$side_dish = $r['side_dish'];
	$spread_sauce = $r['spread_sauce'];
	$soup = $r['soup'];
	$raw = $r['raw'];
	$vegan = $r['vegan'];
	$vegetarian = $r['vegetarian'];
	$dairy_free = $r['dairy_free'];
	$gluten_free = $r['gluten_free'];
	$nut_free = $r['nut_free'];
	$soy_free = $r['soy_free'];
	
	if ($r['discussion_forum']) {
		// Set session variables to persist the Discussion Forum Configuration for this Recipe
		$_SESSION['recipe']['discussion_forum']['forum'] = $r['discussion_forum'];	
		$_SESSION['recipe']['discussion_forum']['thread_post'] = $r['discussion_thread_post'];
		$_SESSION['recipe']['discussion_forum']['thread_post_document'] = $r['discussion_thread_post_document'];	
		$_SESSION['recipe']['discussion_forum']['thread_post_image'] = $r['discussion_thread_post_image'];
		$_SESSION['recipe']['discussion_forum']['thread_post_video'] = $r['discussion_thread_post_video'];
		$_SESSION['recipe']['discussion_forum']['reply_post'] = $r['discussion_reply_post'];
		$_SESSION['recipe']['discussion_forum']['reply_post_document'] = $r['discussion_reply_post_document'];	
		$_SESSION['recipe']['discussion_forum']['reply_post_image'] = $r['discussion_reply_post_image'];
		$_SESSION['recipe']['discussion_forum']['reply_post_video'] = $r['discussion_reply_post_video'];		
	} else {
		unset($_SESSION['recipe']['discussion_forum']);
	}
	
	$classifications = '';
	
	// echo '<div id="storeProductLabels">';
	
	if ($c['breakfast']) { $classifications .= '<span class="label info radius"><b>Breakfast</b></span>&nbsp;&nbsp;'; }	
	if ($r['lunch']) { $classifications .= '<span class="label info radius"><b>Lunch</b></span>&nbsp;&nbsp;'; }
	if ($r['dinner']) { $classifications .= '<span class="label info radius"><b>Dinner</b></span>&nbsp;&nbsp;'; }
	if ($r['appetizer']) { $classifications .= '<span class="label info radius"><b>Appetizer</b></span>&nbsp;&nbsp;'; }
	if ($r['snack']) { $classifications .= '<span class="label info radius"><b>Snack</b></span>&nbsp;&nbsp;'; }
	
	if ($r['condiment']) { $classifications .= '<span class="label info radius"><b>Condiment</b></span>&nbsp;&nbsp;'; }	
	if ($r['dessert']) { $classifications .= '<span class="label info radius"><b>Dessert</b></span>&nbsp;&nbsp;'; }
	if ($r['condiment']) { $classifications .= '<span class="label info radius"><b>Condiment</b></span>&nbsp;&nbsp;'; }	
	if ($r['drink']) { $classifications .= '<span class="label info radius"><b>Drink</b></span>&nbsp;&nbsp;'; }
	if ($r['main_dish']) { $classifications .= '<span class="label info radius"><b>Main Dish</b></span>&nbsp;&nbsp;'; }	
	if ($r['salad']) { $classifications .= '<span class="label info radius"><b>Salad</b></span>&nbsp;&nbsp;'; }
	if ($r['salad_dressing']) { $classifications .= '<span class="label info radius"><b>Salad Dressing</b></span>&nbsp;&nbsp;'; }	
	if ($r['side_dish']) { $classifications .= '<span class="label info radius"><b>Side Dish</b></span>&nbsp;&nbsp;'; }
	if ($r['spread_sauce']) { $classifications .= '<span class="label info radius"><b>Spread & Sauce</b></span>&nbsp;&nbsp;'; }	
	if ($r['soup']) { $classifications .= '<span class="label info radius"><b>Soup</b></span>&nbsp;&nbsp;'; }
	
	if ($r['raw']) { $classifications .= '<span class="label info radius"><b>Raw</b></span>&nbsp;&nbsp;'; }
	if ($r['vega']) { $classifications .= '<span class="label info radius"><b>Vegan</b></span>&nbsp;&nbsp;'; }	
	if ($r['vegetarian']) { $classifications .= '<span class="label info radius"><b>Vegetarian</b></span>&nbsp;&nbsp;'; }
	
	if ($r['dairy_free']) { $classifications .= '<span class="label info radius"><b>Dairy Free</b></span>&nbsp;&nbsp;'; }
	if ($r['gluten_free']) { $classifications .= '<span class="label info radius"><b>Gluten Free</b></span>&nbsp;&nbsp;'; }	
	if ($r['nut_free']) { $classifications .= '<span class="label info radius"><b>Nut Free</b></span>&nbsp;&nbsp;'; }
	if ($r['soy_free']) { $classifications .= '<span class="label info radius"><b>Soy Free</b></span>&nbsp;&nbsp;'; }
	
}

mysqli_free_result($result_recipe);
		
if (empty($recipe_image_url)) {
	$display_recipe_featured_image_url = 'https://placehold.it/720x540&text=Need+Featured+Image';
} else {
	$display_recipe_featured_image_url = $recipe_image_url;
}	
		
?>

<br />
	
<div class="row">
	
	<div class="small-12 medium-5 columns">
	
		<?php 
			echo '<h1>'.cleanEncoding($recipe_title).'</h1>';
			echo '<p class="lead">Chef: '.cleanEncoding($recipe_author).'</p>';
			echo '<div id="storeProductLabels">'.$classifications.'</div>';		
			echo cleanEncoding($summary_description);
			echo cleanEncoding($description);
			echo '<p class="lead">Serves: '.cleanEncoding($number_servings).'</p>';
		?>
		
		<fieldset>
		<legend><i class="fa fa-clock-o"></i> Time (Minutes)</legend>		
		
			<div id="recipe-preparation-time">
				<h5>Preparation:&nbsp;<?php echo cleanEncoding($preparation_time_minutes); ?></h5>	
			</div>
			
			<div id="recipe-cooking-time">
				<h5>Cooking:&nbsp;<?php echo cleanEncoding($cooking_time_minutes); ?></h5>
			</div>
			
			<div id="recipe-ready-time">
				<h5>Ready:&nbsp;<?php echo cleanEncoding($ready_time_minutes); ?></h5>
			</div>
			
		</fieldset>
		
		<?php
		
		if ($host_url_mp4) {
					
			if (!empty($video_poster_image_url)) {
				$poster_image_url = $_SESSION['application']['root_media_host_url'].$video_poster_image_url;	
			} else {
				// $poster_image_url = 'http://wrightcheck.com/media/images/video-poster-master.jpg';
				$poster_image_url = 'https://placehold.it/640x360';						
			}
			
			// http://wrightcheck.com/supplementrelief/recipe/?ppca_id=276&ca_id=1280&re_id=1307
					
			?>
						
			<div class="row">
				
				<div class="small-12 columns">
				
					<a href="#" data-reveal-id="video-overview-1"><img src="<?php echo $poster_image_url; ?>" class="th"></a>
					
					<div class="videoCaption">
						<i class="fa fa-play-circle fa-1x"></i>&nbsp;&nbsp;<?php echo $video_title; ?>&nbsp;&nbsp;<i class="fa fa-clock-o fa-1x"></i>&nbsp;&nbsp;<?php echo $duration_minutes_seconds; ?>
					</div>
											
					<div id="video-overview-1" class="reveal-modal medium" data-reveal>
					  <h2><?php echo $video_title; ?></h2>
					  <p class="lead">Wellness Cooking Video Series<small>&nbsp;&nbsp;<?php echo $duration_minutes_seconds; ?></small></p>
					  <video controls style="width:100%;height:auto;" poster="<?php echo $poster_image_url; ?>"> 
					    <source src="<?php echo $_SESSION['application']['root_media_host_url'].$host_url_mp4; ?>" type="video/mp4">
					    <source src="<?php echo $_SESSION['application']['root_media_host_url'].$host_url_webm; ?>" type="video/webm">   
					    Your browser does not support the video tag.
					  </video>    
					  <a class="close-reveal-modal">&#215;</a>
					</div>
				
				</div>
				
			</div>
			
			<?php		
		}	
		?>
		
	</div>
	
	<div class="small-12 medium-7 columns">
	
		<img src="<?php echo $_SESSION['application']['root_media_host_url'].$display_recipe_featured_image_url; ?>" title="<?php echo $recipe_image_caption; ?>" alt="<?php echo $recipe_image_alt_text; ?>" class="th">
		<div class="show-for-small-only"><br /></div>
		
	</div>
	
</div>
	
<div class="row">
	<div class="small-12 columns">				
		<div class="row collapse panel">
				
			<div class="small-12 medium-6 columns">	
		
				<div id="recipe-ingredients">
					<h3>Ingredients</h3>
					<hr>
					<?php echo cleanEncoding($ingredients); ?>
				</div>
		
			</div>	
					
			<div class="small-12 medium-6 columns">
			
				<div id="recipe-directions">
					<h3>Directions</h3>
					<hr>
					<?php echo cleanEncoding($directions); ?>
				</div>
		
			</div>
			
		</div>		
	</div>	
</div>

<div class="row">
    
    <div class="small-12 columns">

    <fieldset>
		<legend>Recipe Classifications</legend>

		<div class="small-12 columns">	
			<fieldset>
			<legend>Meal</legend>		
				<input type="checkbox" disabled <?php if ($breakfast ==1) { echo 'checked'; } ?> > Breakfast&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($lunch ==1) { echo 'checked'; } ?>> Lunch&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($dinner ==1) { echo 'checked'; } ?>> Dinner&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($appetizer ==1) { echo 'checked'; } ?>> Appetizer&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($snack ==1) { echo 'checked'; } ?>> Snack
			</fieldset>
		</div>

		<div class="small-12 columns">	
			<fieldset>
			<legend>Type</legend>		
				<input type="checkbox" disabled <?php if ($condiment ==1) { echo 'checked'; } ?>> Condiment&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($dessert ==1) { echo 'checked'; } ?>> Dessert&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($drink ==1) { echo 'checked'; } ?>> Drink&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($main_dish ==1) { echo 'checked'; } ?>> Main Dish&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($salad ==1) { echo 'checked'; } ?>> Salad&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($salad_dressing ==1) { echo 'checked'; } ?>> Salad Dressing&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($side_dish ==1) { echo 'checked'; } ?>> Side Dish&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($spread_sauce ==1) { echo 'checked'; } ?>> Spread & Sauce&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($soup ==1) { echo 'checked'; } ?>> Soup
			</fieldset>
		</div>
		
		<div class="small-12 columns">	
			<fieldset>
			<legend>Diet</legend>		
				<input type="checkbox" disabled <?php if ($raw ==1) { echo 'checked'; } ?> > Raw&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($vegan ==1) { echo 'checked'; } ?>> Vegan&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($vegetarian ==1) { echo 'checked'; } ?>> Vegetarian
			</fieldset>
		</div>
		
		<div class="small-12 columns">	
			<fieldset>
			<legend>Food Sensitivity</legend>		
				<input type="checkbox" disabled <?php if ($dairy_free ==1) { echo 'checked'; } ?> > Dairy Free&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($gluten_free ==1) { echo 'checked'; } ?>> Gluten Free&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($nut_free ==1) { echo 'checked'; } ?>> Nut Free&nbsp;&nbsp;
				<input type="checkbox" disabled <?php if ($soy_free ==1) { echo 'checked'; } ?>> Soy Free
			</fieldset>
		</div>
		
    </fieldset>
    
    </div>
			
</div>

<?php // show_array($_SESSION); ?>

<div class="row">
	
	<div class="small-12 columns">
	
		<?php				
		// Determine if the Recipe is allowed to use a Discussion Forum
		if ($_SESSION['recipe']['discussion_forum']['forum'] == true) {	
			?>	
			<div id="educationDiscussionForum">
				<!-- Stardardize the ids and css for all Forums -->
				<?php include('../php/recipe_discussion_forum.php'); ?>
			</div>
			<?php		
		}
		?>
		
	</div>
	
</div>