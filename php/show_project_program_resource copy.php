<?php

include_once('../includes/core.php');	

if (isset($_GET['ppca_id'])) {
	$ppca_id = $_GET['ppca_id'];
}
	
if (isset($_GET['ca_id'])) {
	$content_asset_id = $_GET['ca_id'];
	$_SESSION['resource']['resource_id'] = $_GET['ca_id'];
		
}

$query_string = $_SERVER['QUERY_STRING'];

$queryResource = 'SELECT 
	ca.title, 
	ca.summary_description, 
	rk.discussion_forum, 
	dfc.discussion_thread_post, 
	dfc.discussion_thread_post_document, 
	dfc.discussion_thread_post_image, 
	dfc.discussion_thread_post_video, 
	dfc.discussion_reply_post, 
	dfc.discussion_reply_post_document, 
	dfc.discussion_reply_post_image, 
	dfc.discussion_reply_post_video, 
	im.host_url,
	im.caption,  
	im.alt_text      	
	FROM content_assets ca 
	LEFT JOIN resource_kits rk ON ca.id = rk.content_asset_id  
	LEFT JOIN discussion_forum_configurations dfc ON rk.content_asset_id = dfc.resource_id 		
	LEFT JOIN images im ON 
		(rk.image_id = im.content_asset_id AND 
		 im.size = "Resource" AND
		 im.usage_size = "Small" AND 
		 im.usage_shape = "Block" AND
		 im.format = "JPG") 	
	WHERE ca.id = '.$content_asset_id.'  
	LIMIT 1';
	
$result_resource = mysqli_query($connection, $queryResource);

if (!$result_resource) {
	show_mysqli_error_message($queryResource, $connection);
	die;
}
	
while($r = mysqli_fetch_assoc($result_resource)) {

	if ($r['discussion_forum']) {
		$_SESSION['resource']['discussion_forum']['forum'] = $r['discussion_forum'];	
		$_SESSION['resource']['discussion_forum']['thread_post'] = $r['discussion_thread_post'];
		$_SESSION['resource']['discussion_forum']['thread_post_document'] = $r['discussion_thread_post_document'];	
		$_SESSION['resource']['discussion_forum']['thread_post_image'] = $r['discussion_thread_post_image'];
		$_SESSION['resource']['discussion_forum']['thread_post_video'] = $r['discussion_thread_post_video'];
		$_SESSION['resource']['discussion_forum']['reply_post'] = $r['discussion_reply_post'];
		$_SESSION['resource']['discussion_forum']['reply_post_document'] = $r['discussion_reply_post_document'];	
		$_SESSION['resource']['discussion_forum']['reply_post_image'] = $r['discussion_reply_post_image'];
		$_SESSION['resource']['discussion_forum']['reply_post_video'] = $r['discussion_reply_post_video'];		
	} else {
		unset($_SESSION['resource']['discussion_forum']);
	}
	$heading_host_url = $r['host_url'];
	$heading_alt_text = $r['alt_text'];
	$heading_caption = $r['caption'];
	$heading_title = $r['title'];
	$heading_summary_description = $r['summary_description'];
}

$query_append_order_by = ' ORDER BY ca.title ASC';
$query_append_recipe = '';

if (isset($_GET['keyword']) && !empty($_GET['keyword']))
{
	$keyword = safe_sql_data($connection, $_GET['keyword']);	
	$query_append_recipe .= " AND (ca.title LIKE '%".$keyword."%' OR ca.description LIKE '%".$keyword."%' OR rec.ingredients LIKE '%".$keyword."%') ";
}
	
if (isset($_GET['number_servings']) && !empty($_GET['number_servings']))
{
	$number_servings = safe_sql_data($connection, $_GET['number_servings']);	
	
	if ($number_servings < 9) {
		$query_append_recipe .= " AND (rec.number_servings = ".$number_servings.")";		
	} else {
		$query_append_recipe .= " AND (rec.number_servings > '8')";		
	}
	
}

if (isset($_GET['time_to_prepare']) && !empty($_GET['time_to_prepare']))
	{
	$time_to_prepare = safe_sql_data($connection, $_GET['time_to_prepare']);	
	
	switch ($time_to_prepare) {
		
		case 10:
		$query_append_recipe .= " AND (rec.preparation_time_minutes <= ".$time_to_prepare.")";
		break;
		
		case 20:
		$query_append_recipe .= " AND (rec.preparation_time_minutes > '10' AND rec.preparation_time_minutes <= '20')";
		break;
		
		case 30:
		$query_append_recipe .= " AND (rec.preparation_time_minutes > '20' AND rec.preparation_time_minutes <= '30')";
		break;
		
		case 60:
		$query_append_recipe .= " AND (rec.preparation_time_minutes > '30' AND rec.preparation_time_minutes <= '60')";
		break;
		
		case 61:
		$query_append_recipe .= " AND (rec.preparation_time_minutes > '60')";
		break;
					
		default:
		$query_append_recipe .= " AND (rec.preparation_time_minutes < '500')";
		
	}
			
}

if (isset($_GET['author']) && !empty($_GET['author']))
	{
	$author = safe_sql_data($connection, $_GET['author']);			
	$query_append_recipe .= " AND (ca.author LIKE '%".$author."%')";			
	}
	
if (isset($_GET['has_video']) && !empty($_GET['has_video']))
	{
	$has_video = safe_sql_data($connection, $_GET['has_video']);			
	$query_append_recipe .= " AND (rec.video_id is NOT NULL)";			
	}

if (isset($_GET['breakfast']) && !empty($_GET['breakfast']))
	{
	$breakfast = safe_sql_data($connection, $_GET['breakfast']);			
	$query_append_recipe .= " AND (rec.breakfast = '1')";			
	}
	
if (isset($_GET['lunch']) && !empty($_GET['lunch']))
	{
	$lunch = safe_sql_data($connection, $_GET['lunch']);			
	$query_append_recipe .= " AND (rec.lunch = '1')";			
	}
	
if (isset($_GET['dinner']) && !empty($_GET['dinner']))
	{
	$dinner = safe_sql_data($connection, $_GET['dinner']);			
	$query_append_recipe .= " AND (rec.dinner = '1')";			
	}
	
if (isset($_GET['appetizer']) && !empty($_GET['appetizer']))
	{
	$appetizer = safe_sql_data($connection, $_GET['appetizer']);			
	$query_append_recipe .= " AND (rec.appetizer = '1')";			
	}
	
if (isset($_GET['snack']) && !empty($_GET['snack']))
	{
	$snack = safe_sql_data($connection, $_GET['snack']);			
	$query_append_recipe .= " AND (rec.snack = '1')";			
	}
		
if (isset($_GET['condiment']) && !empty($_GET['condiment']))
	{
	$condiment = safe_sql_data($connection, $_GET['condiment']);			
	$query_append_recipe .= " AND (rec.condiment = '1')";			
	}
	
if (isset($_GET['dessert']) && !empty($_GET['dessert']))
	{
	$dessert = safe_sql_data($connection, $_GET['dessert']);			
	$query_append_recipe .= " AND (rec.dessert = '1')";			
	}
	
if (isset($_GET['drink']) && !empty($_GET['drink']))
	{
	$drink = safe_sql_data($connection, $_GET['drink']);			
	$query_append_recipe .= " AND (rec.drink = '1')";			
	}
	
if (isset($_GET['main_dish']) && !empty($_GET['main_dish']))
	{
	$main_dish = safe_sql_data($connection, $_GET['main_dish']);			
	$query_append_recipe .= " AND (rec.main_dish = '1')";			
	}

if (isset($_GET['salad']) && !empty($_GET['salad']))
	{
	$salad = safe_sql_data($connection, $_GET['salad']);			
	$query_append_recipe .= " AND (rec.salad = '1')";			
	}
	
if (isset($_GET['salad_dressing']) && !empty($_GET['salad_dressing']))
	{
	$salad_dressing = safe_sql_data($connection, $_GET['salad_dressing']);			
	$query_append_recipe .= " AND (rec.salad_dressing = '1')";			
	}	

if (isset($_GET['side_dish']) && !empty($_GET['side_dish']))
	{
	$side_dish = safe_sql_data($connection, $_GET['side_dish']);			
	$query_append_recipe .= " AND (rec.side_dish = '1')";			
	}
	
if (isset($_GET['spread_sauce']) && !empty($_GET['spread_sauce']))
	{
	$spread_sauce = safe_sql_data($connection, $_GET['spread_sauce']);			
	$query_append_recipe .= " AND (rec.spread_sauce = '1')";			
	}
	
if (isset($_GET['soup']) && !empty($_GET['soup']))
	{
	$soup = safe_sql_data($connection, $_GET['soup']);			
	$query_append_recipe .= " AND (rec.soup = '1')";			
	}
				
if (isset($_GET['raw']) && !empty($_GET['raw']))
	{
	$raw = safe_sql_data($connection, $_GET['raw']);			
	$query_append_recipe .= " AND (rec.raw = '1')";			
	}
	
if (isset($_GET['vegan']) && !empty($_GET['vegan']))
	{
	$vegan = safe_sql_data($connection, $_GET['vegan']);			
	$query_append_recipe .= " AND (rec.vegan = '1')";			
	}
	
if (isset($_GET['vegetarian']) && !empty($_GET['vegetarian']))
	{
	$vegetarian = safe_sql_data($connection, $_GET['vegetarian']);			
	$query_append_recipe .= " AND (rec.vegetarian = '1')";			
	}
	
if (isset($_GET['dairy_free']) && !empty($_GET['dairy_free']))
	{
	$dairy_free = safe_sql_data($connection, $_GET['dairy_free']);			
	$query_append_recipe .= " AND (rec.dairy_free = '1')";			
	}
		
if (isset($_GET['gluten_free']) && !empty($_GET['gluten_free']))
	{
	$gluten_free = safe_sql_data($connection, $_GET['gluten_free']);			
	$query_append_recipe .= " AND (rec.gluten_free = '1')";			
	}
	
if (isset($_GET['nut_free']) && !empty($_GET['nut_free']))
	{
	$nut_free = safe_sql_data($connection, $_GET['nut_free']);			
	$query_append_recipe .= " AND (rec.nut_free = '1')";			
	}
		
if (isset($_GET['soy_free']) && !empty($_GET['soy_free']))
	{
	$soy_free = safe_sql_data($connection, $_GET['soy_free']);			
	$query_append_recipe .= " AND (rec.soy_free = '1')";			
	}

if (isset($_GET['order_by']) && !empty($_GET['order_by']))
	{
	$order_by = safe_sql_data($connection, $_GET['order_by']);	
	
	switch ($order_by) {
					
		case 'title':
		$query_append_order_by = ' ORDER BY ca.content_asset_type_code, ca.title';
		break;
		
		case 'author_title':
		$query_append_order_by = ' ORDER BY ca.content_asset_type_code, ca.author, ca.title';
		break;
		
		case 'most_recent_posted':
		$query_append_order_by = ' ORDER BY ca.content_asset_type_code, ca.effective_date DESC'; 
		break;
					
		default:
		$query_append_order_by = ' ORDER BY ca.content_asset_type_code, cau.seq'; 			
	}		
			
}

$queryResourceAssets = 'SELECT 
ca.content_asset_type_code, 
ca.version,
ca.title, 
ca.summary_description, 
ca.description, 
ca.author, 
ca.citations,  
ca.effective_date, 
cau.content_asset_child_id, 		
cau.seq, 
cau.category_code, 
cat.name AS category_name,  
co.embed_code, 
do.image_id AS document_image_id, 
do.host_url AS document_host_url, 
do.caption AS document_caption, 
do.alt_text AS document_alt_text,  
rec.image_id AS recipe_image_id, 
rec.video_id AS recipe_video_id, 
rec.ingredients AS recipe_ingredients, 
rec.directions AS recipe_directions, 
rec.number_servings, 
rec.preparation_time_minutes, 
rec.breakfast, 
rec.lunch, 
rec.dinner, 
rec.appetizer, 
rec.snack, 
rec.condiment, 
rec.dessert, 
rec.drink,  
rec.main_dish, 
rec.salad, 
rec.salad_dressing, 
rec.side_dish, 
rec.spread_sauce, 
rec.soup, 
rec.raw, 
rec.vegan, 
rec.vegetarian, 
rec.dairy_free, 
rec.gluten_free, 
rec.nut_free, 
rec.soy_free,
rec.page_canonical_url AS addendum_recipe_canonical_url,     
imdo.host_url AS document_image_host_url,  
imre.host_url AS recipe_image_host_url, 
imre.alt_text AS addendum_recipe_image_alt_text,     
vi.host_url_mp4, 
vi.host_url_webm, 
imvi1.host_url AS thumbnail_image_url, 
imvi2.host_url AS poster_image_url, 
vi.duration_minutes_seconds, 
urls.url AS seo_url,
rk.page_canonical_url AS resource_canonical_url,     
imrk.host_url AS addendum_resource_image_url, 
imrk.alt_text AS addendum_resource_image_alt_text, 
bl.page_canonical_url AS blog_canonical_url, 
imbl.host_url AS addendum_blog_image_url, 
imbl.alt_text AS addendum_blog_image_alt_text,  
bp.page_canonical_url AS blog_post_canonical_url, 
imbp.host_url AS addendum_blog_post_image_url, 
imbp.alt_text AS addendum_blog_post_image_alt_text 
FROM content_asset_usages cau
LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id 
LEFT JOIN categories cat ON cau.category_code = cat.code 
LEFT JOIN blogs bl ON ca.id = bl.content_asset_id 
LEFT JOIN images imbl 
    ON (bl.image_id = imbl.content_asset_id AND 
        imbl.size = "Blog" AND 
        imbl.usage_size = "Small" AND 
        imbl.usage_shape = "Block" AND  
        imbl.format = "JPG")		 
LEFT JOIN blog_posts bp ON ca.id = bp.content_asset_id 
LEFT JOIN images imbp 
    ON (bp.image_id = imbp.content_asset_id AND 
        imbp.size = "Blog Post" AND 
        imbp.usage_size = "Small" AND 
        imbp.usage_shape = "Block" AND  
        imbp.format = "JPG")		
LEFT JOIN codes co ON cau.content_asset_child_id = co.content_asset_id 
LEFT JOIN documents do ON cau.content_asset_child_id = do.content_asset_id 
LEFT JOIN images imdo ON 
	(do.image_id = imdo.content_asset_id AND 
	 imdo.size = "Resource" AND 
	 imdo.usage_size = "Small" AND
	 imdo.usage_shape = "Block" AND
	 imdo.format = "JPG")
LEFT JOIN recipes rec ON cau.content_asset_child_id = rec.content_asset_id 
LEFT JOIN images imre ON 
	(rec.image_id = imre.content_asset_id AND 
	 imre.size = "Recipe" AND 
	 imre.usage_size = "Small" AND
	 imre.usage_shape = "Block" AND
	 imre.format = "JPG")
LEFT JOIN videos vi ON cau.content_asset_child_id = vi.content_asset_id 
LEFT JOIN images imvi1 ON 
	(vi.image_id = imvi1.content_asset_id AND 
	 imvi1.size = "Resource" AND
	 imvi1.usage_size = "Small" AND 
	 imvi1.usage_shape = "Block" AND
	 imvi1.format = "JPG") 	
LEFT JOIN images imvi2 ON 
	(vi.image_id = imvi2.content_asset_id AND 
	 imvi2.size = "Video" AND
	 imvi2.usage_size = "Large" AND 
	 imvi2.usage_shape = "Landscape" AND 
	 imvi2.format = "JPG") 
LEFT JOIN url_usages urlu 
    ON (cau.content_asset_child_id = urlu.recipe_id AND 
        urlu.effective_date <= CURRENT_DATE AND
       (urlu.end_date IS NULL or urlu.end_date >= CURRENT_DATE))
LEFT JOIN urls urls ON urlu.url_id = urls.id 
LEFT JOIN resource_kits rk ON ca.id = rk.content_asset_id 
LEFT JOIN images imrk 
    ON (rk.image_id = imrk.content_asset_id AND 
        imrk.size = "Resource" AND 
        imrk.usage_size = "Small" AND 
        imrk.usage_shape = "Block" AND  
        imrk.format = "JPG")	
WHERE cau.content_asset_parent_id = '.$content_asset_id.' 
AND cau.category_code IS NULL '.
$query_append_recipe.
$query_append_order_by;

$result_resource_asset = mysqli_query($connection, $queryResourceAssets);

if (!$result_resource_asset) {
	show_mysqli_error_message($queryResourceAssets, $connection);
	die;
}

$total_records = mysqli_num_rows($result_resource_asset);

if ($total_records < 1) {

	$recipe_results_list .= '			  	 						
		<div class="row">
			<div class="small-12 medium-3 columns">
				<img class="th" src="https://cdn-manager.net/media/images/not-found.jpg" alt="no recipes found using the search criterion" />
			</div>			
			<div class="small-12 medium-9 columns">
				<div data-alert class="alert-box warning">
				  <p><i class="fa fa-search fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;<b>No Recipes were found</b> matching your search parameters. Please <b>refine your search</b> and try again.</p>
				</div>
			</div>
		</div>';
	
}
 
$code_block = '';
$document_block = '';
$recipe_block = '';
$video_block = '';
$video_modal_block .= '';
$addendum_block .= '';

$codeCount = 0;
$documentCount = 0;
$recipeCount = 0;
$videoCount = 0;

while($c = mysqli_fetch_assoc($result_resource_asset)) {

		
    switch ($c['content_asset_type_code']) {

		case 'ARTCL':
			$article_block .= '
			<div class="row">
				<div class="small-12 columns">
					<div id="articleTextBlocks">'.
						$c['description'].'
					</div>';
					
					if (strlen($c['citations']) > 0) {
						$article_block .= '
						<div id="articleCitations" class="panel">
							<p>Citations:</p>'.
							$c['citations'].'
						</div>';
					}
					
					$article_block .= '
				</div>
			</div>';
	    
		case 'CODE':
		
		  	$code_block .= $c['embed_code'];	    	
		    break;
	    
		case 'DOC':
			if ($documentCount == 0) {
				$document_block .= ' 
				 
				 <div class="row">
				 	<div class="large-12 columns">
				 		<h3><i class="fa fa-book"></i> Documents</h3>
						<ul class="small-block-grid-1 small-only-text-center medium-block-grid-3 large-block-grid-4">';  	
			}
			
			$documentCount++;
						
			$document_block .= ' 
			<li>
				<a href="'.$_SESSION['application']['root_media_host_url'].$c['document_host_url'].'" target="_blank" title="Select to view and/or download Document"><img src="'.$_SESSION['application']['root_media_host_url'].$c['document_image_host_url'].'" class="th" data-tooltip data-options="disable_for_touch:true" class="has-tip tip-bottom" title="'.$c['title'].'"></a>
				
				
			</li>';				
					
	        break;
	        
		case 'RECIPE':
		  
            if ($c['seo_url']) {
                $recipe_url = '../'.$c['seo_url'].'/';
        	} else {	
        		$recipe_url = '../recipe/?re_id='.$c['content_asset_child_id'];
        	}			
			
			if ($c['recipe_image_host_url']) {
				$image_source_url = $_SESSION['application']['root_media_host_url'].$c['recipe_image_host_url'];				
			} else {
				$image_source_url = 'https://placehold.it/150x150';
			}
					
			
			$classifications = '';
			
			
			if ($c['breakfast']) { $classifications .= '<span class="label info radius"><b>Breakfast</b></span>&nbsp;&nbsp;'; }	
			if ($c['lunch']) { $classifications .= '<span class="label info radius"><b>Lunch</b></span>&nbsp;&nbsp;'; }
			if ($c['dinner']) { $classifications .= '<span class="label info radius"><b>Dinner</b></span>&nbsp;&nbsp;'; }
			if ($c['appetizer']) { $classifications .= '<span class="label info radius"><b>Appetizer</b></span>&nbsp;&nbsp;'; }
			if ($c['snack']) { $classifications .= '<span class="label info radius"><b>Snack</b></span>&nbsp;&nbsp;'; }
			
			if ($c['condiment']) { $classifications .= '<span class="label info radius"><b>Condiment</b></span>&nbsp;&nbsp;'; }	
			if ($c['dessert']) { $classifications .= '<span class="label info radius"><b>Dessert</b></span>&nbsp;&nbsp;'; }
			if ($c['condiment']) { $classifications .= '<span class="label info radius"><b>Condiment</b></span>&nbsp;&nbsp;'; }	
			if ($c['drink']) { $classifications .= '<span class="label info radius"><b>Drink</b></span>&nbsp;&nbsp;'; }
			if ($c['main_dish']) { $classifications .= '<span class="label info radius"><b>Main Dish</b></span>&nbsp;&nbsp;'; }	
			if ($c['salad']) { $classifications .= '<span class="label info radius"><b>Salad</b></span>&nbsp;&nbsp;'; }
			if ($c['salad_dressing']) { $classifications .= '<span class="label info radius"><b>Salad Dressing</b></span>&nbsp;&nbsp;'; }	
			if ($c['side_dish']) { $classifications .= '<span class="label info radius"><b>Side Dish</b></span>&nbsp;&nbsp;'; }
			if ($c['spread_sauce']) { $classifications .= '<span class="label info radius"><b>Spread & Sauce</b></span>&nbsp;&nbsp;'; }	
			if ($c['soup']) { $classifications .= '<span class="label info radius"><b>Soup</b></span>&nbsp;&nbsp;'; }
			
			if ($c['raw']) { $classifications .= '<span class="label info radius"><b>Raw</b></span>&nbsp;&nbsp;'; }
			if ($c['vega']) { $classifications .= '<span class="label info radius"><b>Vegan</b></span>&nbsp;&nbsp;'; }	
			if ($c['vegetarian']) { $classifications .= '<span class="label info radius"><b>Vegetarian</b></span>&nbsp;&nbsp;'; }
			
			if ($c['dairy_free']) { $classifications .= '<span class="label info radius"><b>Dairy Free</b></span>&nbsp;&nbsp;'; }
			if ($c['gluten_free']) { $classifications .= '<span class="label info radius"><b>Gluten Free</b></span>&nbsp;&nbsp;'; }	
			if ($c['nut_free']) { $classifications .= '<span class="label info radius"><b>Nut Free</b></span>&nbsp;&nbsp;'; }
			if ($c['soy_free']) { $classifications .= '<span class="label info radius"><b>Soy Free</b></span>&nbsp;&nbsp;'; }

			$recipe_results_list .= '
			<div id="recipeRow">
						  	 						
				<div class="row collapse">
				
					<div class="small-12 medium-3 columns small-only-text-center lazyDiv">
						<a href="'.$recipe_url.'" title="view Recipe for '.$c['title'].'"><img class="th lazy" data-original="'.$image_source_url.'" alt="" /></a>
					</div>
					    					
					<div class="small-12 medium-9 columns colWrapMargin">
						
						<a href="'.$recipe_url.'" title="view Recipe"><h3 style="color: #61AFD1;">'.$c['title'].'</h3></a>
						<div id="storeProductLabels">'.$classifications.'</div>';
						
						$recipe_results_list .= 
						$c['summary_description'].
						'Chef: '.$c['author'].'
						&nbsp;
						
	
					</div>
					
				</div>
				
				<hr>
			
			</div>';
							  	    
			break;
	  	    
		case 'VIDEO':

	        if ($videoCount == 0) {
	        	$video_block .= '
	        	
	        	<div class="row">
	        		<div class="small-12 columns">
	        			<h3><i class="fa fa-file-video-o"></i> Videos</h3>';
	        		if(!$ie9){
	        			$video_block .= '<ul class="small-block-grid-1 small-only-text-center medium-block-grid-3 large-block-grid-4">';  	
	        		}
	        }
	        
	        $videoCount++;
	                

	        if($ie9) {

	        $video_block .= ' 
	        <div class="row">
	        	<div class="medium-3 hide-for-small-down columns">&nbsp;</div>
		        <div id="video'.$videoCount.'" class="small-12 medium-6 columns">
			    	<h3>'.$c['title'].'<small>&nbsp;&nbsp;<i class="fa fa-clock-o"></i>&nbsp;'.$c['duration_minutes_seconds'].'</small></h3>
		            
		            <video controls preload="none" style="width:100%;height:auto;" poster="'.$_SESSION['application']['root_media_host_url'].$c['poster_image_url'].'" > 
		              <source src="'.$_SESSION['application']['root_media_host_url'].$c['host_url_mp4'].'" type="video/mp4">
		              <source src="'.$_SESSION['application']['root_media_host_url'].$c['host_url_webm'].'" type="video/webm">   
		              Your browser does not support the video tag.
		            </video>
		        </div>
		        <div class="medium-3 hide-for-small-down columns">&nbsp;</div>
	        </div>
	        ';

	        } else {

	        $video_block .= '
	        <li>
	        	<a href="#" data-reveal-id="video'.$videoCount.'" title="Select to view Video"><img src="'.$_SESSION['application']['root_media_host_url'].$c['thumbnail_image_url'].'" class="th" data-tooltip data-options="disable_for_touch:true" class="has-tip tip-bottom" title="'.$c['title'].' - Time: '.$c['duration_minutes_seconds'].'"></a>
	        </li>';
	        
	        $video_modal_block .= ' 
	        <div id="video'.$videoCount.'" class="reveal-modal medium" data-reveal>
		    	<h2>'.$c['title'].'<small>&nbsp;&nbsp;<i class="fa fa-clock-o"></i>&nbsp;'.$c['duration_minutes_seconds'].'</small></h2>
	            
	            <video controls preload="none" style="width:100%;height:auto;" poster="'.$_SESSION['application']['root_media_host_url'].$c['poster_image_url'].'" > 
	              <source src="'.$_SESSION['application']['root_media_host_url'].$c['host_url_mp4'].'" type="video/mp4">
	              <source src="'.$_SESSION['application']['root_media_host_url'].$c['host_url_webm'].'" type="video/webm">   
	              Your browser does not support the video tag.
	            </video>    
	            <a class="close-reveal-modal">&#215;</a>
	        </div>';

	        }

	        break;	    
	} 
}

$queryAddendumResourceAssets = 'SELECT 
ca.content_asset_type_code, 
ca.version,
ca.title, 
ca.summary_description, 
ca.description, 
ca.author, 
ca.text, 
ca.citations,  
ca.effective_date, 
cau.content_asset_child_id, 		
cau.seq, 
cau.category_code, 
cat.name AS category_name,  
co.embed_code, 
do.image_id AS document_image_id, 
do.host_url AS document_host_url, 
do.caption AS document_caption, 
do.alt_text AS document_alt_text,  
rec.image_id AS recipe_image_id, 
rec.video_id AS recipe_video_id, 
rec.ingredients AS recipe_ingredients, 
rec.directions AS recipe_directions, 
rec.number_servings, 
rec.preparation_time_minutes, 
rec.breakfast, 
rec.lunch, 
rec.dinner, 
rec.appetizer, 
rec.snack, 
rec.condiment, 
rec.dessert, 
rec.drink,  
rec.main_dish, 
rec.salad, 
rec.salad_dressing, 
rec.side_dish, 
rec.spread_sauce, 
rec.soup, 
rec.raw, 
rec.vegan, 
rec.vegetarian, 
rec.dairy_free, 
rec.gluten_free, 
rec.nut_free, 
rec.soy_free,
rec.page_canonical_url AS addendum_recipe_canonical_url,     
imdo.host_url AS document_image_host_url,  
imre.host_url AS recipe_image_host_url, 
imre.alt_text AS addendum_recipe_image_alt_text,     
vi.host_url_mp4, 
vi.host_url_webm, 
imvi1.host_url AS thumbnail_image_url, 
imvi2.host_url AS poster_image_url, 
vi.duration_minutes_seconds, 
urls.url AS seo_url,
rk.page_canonical_url AS resource_canonical_url,     
imrk.host_url AS addendum_resource_image_url, 
imrk.alt_text AS addendum_resource_image_alt_text, 
bl.page_canonical_url AS blog_canonical_url, 
imbl.host_url AS addendum_blog_image_url, 
imbl.alt_text AS addendum_blog_image_alt_text,  
bp.page_canonical_url AS blog_post_canonical_url, 
imbp.host_url AS addendum_blog_post_image_url, 
imbp.alt_text AS addendum_blog_post_image_alt_text 
FROM content_asset_usages cau
LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id 
LEFT JOIN categories cat ON cau.category_code = cat.code 
LEFT JOIN blogs bl ON ca.id = bl.content_asset_id 
LEFT JOIN images imbl 
    ON (bl.image_id = imbl.content_asset_id AND 
        imbl.size = "Blog" AND 
        imbl.usage_size = "Small" AND 
        imbl.usage_shape = "Block" AND  
        imbl.format = "JPG")		 
LEFT JOIN blog_posts bp ON ca.id = bp.content_asset_id 
LEFT JOIN images imbp 
    ON (bp.image_id = imbp.content_asset_id AND 
        imbp.size = "Blog Post" AND 
        imbp.usage_size = "Small" AND 
        imbp.usage_shape = "Block" AND  
        imbp.format = "JPG")		
LEFT JOIN codes co ON cau.content_asset_child_id = co.content_asset_id 
LEFT JOIN documents do ON cau.content_asset_child_id = do.content_asset_id 
LEFT JOIN images imdo ON 
	(do.image_id = imdo.content_asset_id AND 
	 imdo.size = "Resource" AND 
	 imdo.usage_size = "Small" AND
	 imdo.usage_shape = "Block" AND
	 imdo.format = "JPG")
LEFT JOIN recipes rec ON cau.content_asset_child_id = rec.content_asset_id 
LEFT JOIN images imre ON 
	(rec.image_id = imre.content_asset_id AND 
	 imre.size = "Recipe" AND 
	 imre.usage_size = "Small" AND
	 imre.usage_shape = "Block" AND
	 imre.format = "JPG")
LEFT JOIN videos vi ON cau.content_asset_child_id = vi.content_asset_id 
LEFT JOIN images imvi1 ON 
	(vi.image_id = imvi1.content_asset_id AND 
	 imvi1.size = "Resource" AND
	 imvi1.usage_size = "Small" AND 
	 imvi1.usage_shape = "Block" AND
	 imvi1.format = "JPG") 	
LEFT JOIN images imvi2 ON 
	(vi.image_id = imvi2.content_asset_id AND 
	 imvi2.size = "Video" AND
	 imvi2.usage_size = "Large" AND 
	 imvi2.usage_shape = "Landscape" AND 
	 imvi2.format = "JPG") 
LEFT JOIN url_usages urlu 
    ON (cau.content_asset_child_id = urlu.recipe_id AND 
        urlu.effective_date <= CURRENT_DATE AND
       (urlu.end_date IS NULL or urlu.end_date >= CURRENT_DATE))
LEFT JOIN urls urls ON urlu.url_id = urls.id 
LEFT JOIN resource_kits rk ON ca.id = rk.content_asset_id 
LEFT JOIN images imrk 
    ON (rk.image_id = imrk.content_asset_id AND 
        imrk.size = "Resource" AND 
        imrk.usage_size = "Small" AND 
        imrk.usage_shape = "Block" AND  
        imrk.format = "JPG")	
WHERE cau.content_asset_parent_id = '.$content_asset_id.' 
AND cau.category_code IS NOT NULL 
ORDER BY ca.title'; 

$result_addendum_resource_asset = mysqli_query($connection, $queryAddendumResourceAssets);

if (!$result_addendum_resource_asset) {
	show_mysqli_error_message($queryResourceAssets, $connection);
	die;
}

$total_addendum_records = mysqli_num_rows($result_addendum_resource_asset); 
         
while($r = mysqli_fetch_assoc($result_addendum_resource_asset)) {
  
    if ($r['category_name'] == "Resource Addendum") {
					

		if (!$resource_addendum_block) {
			$resource_addendum_block .= '
			    
			    <div class="panel callout radius">
			        <div class="small-12 columns">
			            <h3><i class="fa fa-medkit"></i>  Related Wellness Resources</h3>
			            <p>We encourage you to <b>take advantage of these FREE Wellness Resources</b> on our website.</p>
                    </div>';
		}
					
		$addendum_image_url = '';
		$addendum_image_alt_text = '';
		$addendum_canonical_url = '';
		
        switch ($r['content_asset_type_code']) {
            
            case 'BLOG':
            			    
		        $addendum_image_url = $r['addendum_blog_image_url'];
				$addendum_image_alt_text = $r['addendum_blog_image_alt_text'];
				$addendum_canonical_url = $r['blog_canonical_url'];								
					
            break;

            case 'BLOGP':
		    
		        $addendum_image_url = $r['addendum_blog_post_image_url'];
				$addendum_image_alt_text = $r['addendum_blog_post_image_alt_text'];
				$addendum_canonical_url = $r['blog_post_canonical_url'];								
					
            break;
            
            case 'RECIPE':
            			    
		        $addendum_image_url = $r['recipe_image_host_url'];
				$addendum_image_alt_text = $r['addendum_recipe_image_alt_text'];
				$addendum_canonical_url = $r['addendum_recipe_canonical_url'];						
					
            break;
            
		    case 'RESO':
		    
		        $addendum_image_url = $r['addendum_resource_image_url'];
				$addendum_image_alt_text = $r['addendum_resource_image_alt_text'];
				$addendum_canonical_url = $r['resource_canonical_url'];						
					
            break;
            
        }							
		    					
		$resource_addendum_block .= '
			<hr>
		    <div class="row">		
                <div class="small-12 medium-2 columns text-center">
		            <img class="th" src="'.$_SESSION['application']['root_media_host_url'].$addendum_image_url.'" alt="'.$addendum_image_alt_text.'" width="125px">
                </div>
                <div class="small-12 medium-10 columns colWrapMargin">
                    <a href="'.$addendum_canonical_url.'"><h4>'.$r['title'].'</a></h4>'.
                    $r['summary_description'].'
                </div>
            </div>';
		
	}
}	

$queryResourceProjectBrandProducts = 'SELECT 
    bpu.name AS addendum_product_name,
    pbpu.group_options_description AS addendum_product_group_options_description,  
    pbpu.page_meta_description AS addendum_product_page_meta_description, 
	pbpu.page_canonical_url AS addendum_product_canonical_url, 
	urls.url AS addendum_product_url, 
    im.host_url AS addendum_product_image_url, 
	im.alt_text AS addendum_product_image_alt_text 
	FROM content_asset_project_brand_products capbp 
	LEFT JOIN project_brand_product_usages pbpu ON capbp.project_brand_product_usage_id = pbpu.id 
	LEFT JOIN url_usages urlu ON pbpu.id = urlu.pbpu_id 
	LEFT JOIN urls urls ON urlu.url_id = urls.id 
	LEFT JOIN brand_product_usages bpu ON pbpu.brand_product_usage_id = bpu.id 
    LEFT JOIN images im 
	    ON (bpu.image_id = im.content_asset_id AND 
            im.size = "Product" AND 
            im.usage_size = "Small" AND 
            im.format = "JPG")
	WHERE capbp.content_asset_id = '.$content_asset_id.' 
	AND (capbp.effective_date <= CURRENT_DATE AND
        (capbp.end_date IS NULL or capbp.end_date >= CURRENT_DATE)) 
    AND pbpu.active = "1" 
    ORDER BY bpu.name'; 
    

$result_resource_project_brand_product = mysqli_query($connection, $queryResourceProjectBrandProducts);

if (!$result_resource_project_brand_product) {
	show_mysqli_error_message($queryResourceProjectBrandProducts, $connection);
	die;
}
				
$product_addendum_block = '';

while($pbp = mysqli_fetch_assoc($result_resource_project_brand_product)) { 
	
    if (!$product_addendum_block) {
		$product_addendum_block .= '
		    
		    <div class="panel callout radius">
		        <div class="small-12 columns">
		            <h3><i class="fa fa-product-hunt"></i>  Supplement Referrals</h3>
		            <p>Learn more about these <b>NuMedica Supplements</b> on our website.</p>
                </div>';
	}
	
    $addendum_product_image_url = $pbp['addendum_product_image_url'];
	$addendum_product_image_alt_text = $pbp['addendum_product_image_alt_text'];
	$addendum_product_url = $pbp['addendum_product_url'];
	$addendum_product_page_meta_description	= $pbp['addendum_product_page_meta_description'];			
        
    $product_addendum_block .= '
	    <hr>
	    <div class="row">		
            <div class="small-12 medium-2 columns text-center">
	            <img class="th" src="'.$_SESSION['application']['root_media_host_url'].$addendum_product_image_url.'" alt="'.$addendum_product_image_alt_text.'" width="125px">
            </div>
            <div class="small-12 medium-10 columns colWrapMargin">
                <a href="../'.$addendum_product_url.'/"><h4>'.$pbp['addendum_product_name'].'</a></h4>'.
                $pbp['addendum_product_page_meta_description'].'
             </div>
        </div>';		
}

if ($product_addendum_block) { 
	    
	$product_addendum_block .= '
	    </div> ';
}
    
if ($resource_addendum_block) { 
	    
	$resource_addendum_block .= '
	    </div> ';
}
				    		            
mysqli_free_result($result_resource);
mysqli_free_result($result_resource_asset);
mysqli_free_result($result_addendum_resource_asset);
mysqli_free_result($result_resource_project_brand_product);

if (!empty($document_block)) {
	$document_block .= '</ul></div></div>';	
}

if (!empty($video_block)) {
	$video_block .= '</ul></div></div>';	
}

?>

<?php
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
	{
		$json 							=	[];
		$json['recipe_results_list']	=	$recipe_results_list;		
		$json['video_block']			=	$video_block;
		$json['resource_addendum_block']=	$resource_addendum_block;
		$json['product_addendum_block']	=	$product_addendum_block;
		$json['total_records']			=	$total_records;
		$json 							=	json_encode($json);
		die($json);
	}
?>


<div class="panel">
			
	<div class="row">
	
		<div class="small-12 medium-3 columns small-only-text-center">
			<img class="th" src="<?php echo $_SESSION['application']['root_media_host_url'].$heading_host_url; ?>" alt="<?php echo $heading_alt_text; ?>" />
			<div class="caption"><?php echo $heading_caption; ?></div>
		</div>
		
		<div class="small-12 medium-9 columns colWrapMargin">
			<h1><?php echo cleanEncoding($heading_title); ?></h1>
			<?php echo cleanEncoding($heading_summary_description); ?>
			
		</div>
				
	</div>
	
</div>

<div class="row">		
	<div class="small-12 columns">
    	
    	<ul class="tabs" data-options="deep_linking:true; scroll_to_content:false;" data-tab>
        	<li class="tab-title active"><a href="#content">Content</a></li>
			<?php if ($resource_addendum_block) { ?><li class="tab-title"><a href="#resourceAddendum">Wellness Resources</a></li><?php } ?>
			<?php if ($product_addendum_block) { ?><li class="tab-title"><a href="#productAddendum">Supplements</a></li><?php } ?>
			<?php if ($_SESSION['resource']['discussion_forum']['forum']) { ?><li class="tab-title"><a href="#discuss">Discussion Forum</a></li><?php } ?>  
        </ul>
        
        <div class="tabs-content">
            
            <div class="content active" id="content">
                <?
                // Display Article resources if any
                if (!empty($article_block)) {
                	 echo $article_block;
                }
                
                // Display Document resources if any
                if (!empty($document_block) && empty($video_block)) {
                	 echo $document_block;
                }
                
                // Display Recipe resources if any
                if (!empty($recipe_results_list)) {
                
                	// Create the Search form
                
                	// Build the Chef select list
                	$chef_query = '
                	SELECT 
                	distinct(author)   
                	FROM content_assets 
                	WHERE content_asset_type_code = "RECIPE" 
                	AND effective_date <= CURRENT_DATE 
                	AND (end_date is NULL or end_date >= CURRENT_DATE)  
                	ORDER BY author ASC';
                	
                	// echo $chef_query;
                	// die();
                	
                	$chef_result = mysql_query($chef_query);
                	
                	if (!$chef_result) {
                		show_mysqli_error_message($chef_query, $connection);
                		die;
                	}
                				
                	$chef_options = '<option value="">Chef...</option>';
                	
                	while($chef_row = mysql_fetch_array($chef_result)) {
                		$selected = '';
                		if(($author ? $author : $row1['p']) == $chef_row['author']) $selected = ' selected';
                		$chef_options .= '<option value="'.$chef_row['author'].'" '.$selected.'>'.$chef_row['author'].'</option>';
                	}
                	
                	mysql_free_result($chef_result);
                	
                	// echo $recipe_results_list
                
                	?>
                	
                	<div class="row">
                			
                		<div class="small-12 medium-3 columns">
                    		
                    		<?php
                    		 	// $form_action = "../resource/";
                    		 	$form_action = "../healthy-living-whole-foods-cookbook/";
                    			if (isset($_SESSION['resource']['url'])) {
	                    			$form_action = '../'.$_SESSION['resource']['url'].'/'; 
                    			} else {
	                    			$form_action = '../resource/';
                    			}
                    		?>
                				
                	    	<form id="recipe_search_form" data-abide action="<?php echo $form_action; ?>" method="GET" />
                	    	    <input type="hidden" id="recipe-search-action" name="action" value="search"/>
                	    		<input type="hidden" name="ppca_id" value="<?php echo $ppca_id; ?>"/>
                	    		<input type="hidden" name="ca_id" value="<?php echo $content_asset_id; ?>"/>
                	    			    	
                	    		<div class="row">
                	    			<div class="small-8 columns">
                	    		    	<input class="button medium radius expand" id="searchRecipeBtn" type="button" value="Search"> 	      	    	  				    	    
                	    		  	</div>
								  	<div class="small-4 columns">		    	
                	    		    	<div id="noOfRecipe"><i class="fa fa-search"></i>&nbsp;&nbsp;<?php echo $total_records; ?><br /><br /></div>    	      	    	  				    	    
                	    		  	</div>                	    		  
                	    		</div>
                	    																	  	
								<input type="text" name="keyword" id="keyword" maxlength="255" placeholder="Search by title, ingredient..." value="<?php echo ($keyword ? $keyword : ''); ?>" />
                	    	                  	    	  
								<select id="number_servings" name='number_servings'>
                	    	  		<option <?php if (isset($number_servings) && $number_servings == NULL): ?>selected="selected"<?php endif ?> value="">Serving Size...</option>
							  		<option <?php if (isset($number_servings) && $number_servings == '1'): ?>selected="selected"<?php endif ?> value="1">1</option>
							  		<option <?php if (isset($number_servings) && $number_servings == '2'): ?>selected="selected"<?php endif ?> value="2">2</option>
							  		<option <?php if (isset($number_servings) && $number_servings == '3'): ?>selected="selected"<?php endif ?> value="3">3</option>
							  		<option <?php if (isset($number_servings) && $number_servings == '4'): ?>selected="selected"<?php endif ?> value="4">4</option>
							  		<option <?php if (isset($number_servings) && $number_servings == '5'): ?>selected="selected"<?php endif ?> value="5">5</option>
							  		<option <?php if (isset($number_servings) && $number_servings == '6'): ?>selected="selected"<?php endif ?> value="6">6</option>	
							  		<option <?php if (isset($number_servings) && $number_servings == '7'): ?>selected="selected"<?php endif ?> value="7">7</option>	
							  		<option <?php if (isset($number_servings) && $number_servings == '8'): ?>selected="selected"<?php endif ?> value="8">8</option>						  
							  		<option <?php if (isset($number_servings) && $number_servings > '8'): ?>selected="selected"<?php endif ?> value="9">More than 8</option>
                	    	  	</select>
                	    	  				    					    	
							  	<select id="time_to_prepare" name='time_to_prepare'>
                	    	  		<option <?php if (isset($time_to_prepare) && $time_to_prepare == NULL): ?>selected="selected"<?php endif ?> value="">Time to Prepare...</option>
							  		<option <?php if (isset($time_to_prepare) && $time_to_prepare < '11'): ?>selected="selected"<?php endif ?> value="10">10 minutes or less</option>
							  		<option <?php if (isset($time_to_prepare) && $time_to_prepare > '10' && $time_to_prepare <= '20'): ?>selected="selected"<?php endif ?> value="20">11 to 20 minutes</option>	  	
							  		<option <?php if (isset($time_to_prepare) && $time_to_prepare > '20' && $time_to_prepare <= '30'): ?>selected="selected"<?php endif ?> value="30">21 to 30 minutes</option>
							  		<option <?php if (isset($time_to_prepare) && $time_to_prepare > '30' && $time_to_prepare <= '60'): ?>selected="selected"<?php endif ?> value="60">31 to 60 minutes</option>
							  		<option <?php if (isset($time_to_prepare) && $time_to_prepare > '60'): ?>selected="selected"<?php endif ?> value="61">more than 1 hour</option>
                	    	  	</select>
                	    	  			  
							  	<select id="limit_by_chef" name="author">
                	    	  		<?php echo $chef_options; ?>
                	    	  	</select>
                	    	  
							  	<select id="order_by_recipe" name='order_by'>
                	    	  		<option <?php if (isset($order_by) && $order_by == NULL): ?>selected="selected"<?php endif ?> value="">Order by...</option>
							  		<option <?php if (isset($order_by) && $order_by == 'title'): ?>selected="selected"<?php endif ?> value="title">Title</option>
							  		<option <?php if (isset($order_by) && $order_by == 'author_title'): ?>selected="selected"<?php endif ?> value="author_title">Author | Title</option>
							  		<option <?php if (isset($order_by) && $order_by == 'most_recent_posted'): ?>selected="selected"<?php endif ?> value="most_recent_posted">Most Recent Posted</option>
                	    	  	</select>	  
             	    	  
							  	<!-- <input type="checkbox" name="has_video" tabindex="5" value="1" <?php if ($has_video ==1) { echo 'checked'; } ?>> Has Video<br> -->	
                	    	  	
                	    	  	<div class="show-for-medium-up">
                	    	  				    				  			    					    						    						    		    	
							  	<fieldset>     	
	                	    		<legend>Meal</legend>  	
	                	    	  
	                	    	  	<div class="row">    	
	                	    	  	
	                	    	  		<div class="small-12 columns">			
	                	    	  			<input type="checkbox" name="breakfast" tabindex="6" value="1" <?php if ($breakfast ==1) { echo 'checked'; } ?>> Breakfast
	                	    	  		</div>
	                	    	  	
		                	    	  	<div class="small-12 columns">			
		                	    	  		<input type="checkbox" name="lunch" value="1" <?php if ($lunch ==1) { echo 'checked'; } ?>> Lunch
		                	    	  	</div>
		                	    	  		
		                	    	  	<div class="small-12 columns">			
		                	    	  		<input type="checkbox" name="dinner" value="1" <?php if ($dinner ==1) { echo 'checked'; } ?>> Dinner
		                	    	  	</div>
		                	    	  		
		                	    	  	<div class="small-12 columns">			
		                	    	  		<input type="checkbox" name="appetizer" value="1" <?php if ($appetizer ==1) { echo 'checked'; } ?>> Appetizer
		                	    	  	</div>
		                	    	  		
		                	    	  	<div class="small-12 columns end">			
		                	    	  		<input type="checkbox" name="snack" value="1" <?php if ($snack ==1) { echo 'checked'; } ?>> Snack
		                	    	  	</div>
		                	    	  				    		
		                	    	</div>
	                	    	  
	                	    	</fieldset>
                	    	  
								<fieldset> 
									<legend>Type</legend>  	
                	    	  
									<div class="row"> 
                	    	  	
                	    	  			<div class="small-12 columns">			
                	    	  				<input type="checkbox" name="condiment" value="1" <?php if ($condiment ==1) { echo 'checked'; } ?>> Condiment
                	    	  			</div>	   	
                	    	  	
							  			<div class="small-12 columns">			
                	    	  				<input type="checkbox" name="dessert" value="1" <?php if ($dessert ==1) { echo 'checked'; } ?>> Dessert
                	    	  			</div>
                	    	  		
							  			<div class="small-12 columns">			
                	    	  				<input type="checkbox" name="drink" value="1" <?php if ($drink ==1) { echo 'checked'; } ?>> Drink
                	    	  			</div>
                	    	  	
							  			<div class="small-12 columns">			
                	    	  				<input type="checkbox" name="main_dish" value="1" <?php if ($main_dish ==1) { echo 'checked'; } ?>> Main Dish
                	    	  			</div>
                	    	  			    	  			    	  		
							  			<div class="small-12 columns">			
                	    	  				<input type="checkbox" name="salad" value="1" <?php if ($salad ==1) { echo 'checked'; } ?>> Salad
                	    	  			</div>
                	    	  		
							  			<div class="small-12 columns">			
                	    	  				<input type="checkbox" name="salad_dressing" value="1" <?php if ($salad_dressing ==1) { echo 'checked'; } ?>> Salad Dressing
                	    	  			</div>
                	    	  			    	  			   			
							  			<div class="small-12 columns">			
                	    	  				<input type="checkbox" name="side_dish" value="1" <?php if ($side_dish ==1) { echo 'checked'; } ?>> Side Dish
                	    	  			</div>
                	    	  		
							  			<div class="small-12 columns">			
                	    	  				<input type="checkbox" name="spread_sauce" value="1" <?php if ($spread_sauce ==1) { echo 'checked'; } ?>> Spread & Sauce
                	    	  			</div>  
                	    	  		
							  			<div class="small-12 columns">			
                	    	  				<input type="checkbox" name="soup" value="1" <?php if ($soup ==1) { echo 'checked'; } ?>> Soup
                	    	  			</div>    			
                	    	  				    		
                	    	  		</div>
                	    	  
                	    	  	</fieldset>										
                	    	  
							  	<fieldset>  	
								  	<legend>Diet</legend>  	
                	    	  
								  	<div class="row">
                	    	  	    		
                	    	  			<div class="small-12 columns">			
                	    	  				<input type="checkbox" name="raw" value="1" <?php if ($raw ==1) { echo 'checked'; } ?>> Raw
                	    	  			</div>
                	    	  		    			
	                	    	  		<div class="small-12 columns">			
	                	    	  			<input type="checkbox" name="vegan" value="1" <?php if ($vegan ==1) { echo 'checked'; } ?>> Vegan
	                	    	  		</div>
	                	    	  		
	                	    	  		<div class="small-12 columns end">			
	                	    	  			<input type="checkbox" name="vegetarian" value="1" <?php if ($vegetarian ==1) { echo 'checked'; } ?>> Vegetarian
	                	    	  		</div>
                	    	  		
                	    	  		</div>
                	    	  
                	    	  	</fieldset>					
                	    	  	    	
							  	<fieldset>  	
								  	<legend>Food Sensitivity</legend>  	
                	    	  
								  	<div class="row">    	
                	    	  	
                	    	  			<div class="small-12 columns">			
                	    	  				<input type="checkbox" name="dairyfree" value="1" <?php if ($dairyfree ==1) { echo 'checked'; } ?>> Dairy Free
                	    	  			</div>
                	    	  	
							  			<div class="small-12 columns">			
                	    	  				<input type="checkbox" name="gluten_free" value="1" <?php if ($gluten_free ==1) { echo 'checked'; } ?>> Gluten Free
                	    	  			</div>
                	    	  		
							  			<div class="small-12 columns">			
                	    	  				<input type="checkbox" name="nut_free" value="1" <?php if ($nut_free ==1) { echo 'checked'; } ?>> Nut Free
                	    	  			</div>
                	    	  		
							  			<div class="small-12 columns end">			
                	    	  				<input type="checkbox" name="soy_free" value="1" <?php if ($soy_free ==1) { echo 'checked'; } ?>> Soy Free
                	    	  			</div>
                	    	  				    		
                	    	  		</div>
                	    	  
                	    	  	</fieldset>
                	    	  	
                	    	  	</div>		
                	
                	    	</form>		    	
                				    								
                		</div>
                		
                		<div class="small-12 medium-9 columns" id="recipeResultList">
                			<?php 
                				echo $recipe_results_list; 
                 			?>							
                		</div>
                	
                	</div>
                	
                	<br />
                			
                	<?php 
                	
                }
                  
                // Display Video resources if any
                if (!empty($video_block)) {
                	if($ie9){
                		echo $video_block;
                        // Display Document resources if any
                        if (!empty($document_block)) {
                        	 echo $document_block;
                        }
                	} else {
                		echo $video_block;
                		echo $video_modal_block;
                        // Display Document resources if any
                        if (!empty($document_block)) {
                        	 echo $document_block;
                        }
                	}
                }
         
                ?>
                
            </div>
            
            <?php if ($resource_addendum_block) { 
                // echo '<div class="content" id="resourceAddendum">content here ...</div>'; 
                echo '<div class="content" id="resourceAddendum">'.$resource_addendum_block.'</div>'; 
            } ?>
            
            <?php if ($product_addendum_block) { 
                 echo '<div class="content" id="productAddendum">'.$product_addendum_block.'</div>'; 
            } ?>

            <div class="content" id="discuss">
                <?php 
	                if ($_SESSION['resource']['discussion_forum']['forum']) {
	                	$_SESSION['forum_type'] = 'resource';
	                    include('../php/discussion_forum.php');
	                }
                ?>
            </div>
            
        </div>
        				
	</div>
</div>