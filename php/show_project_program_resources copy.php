<?php

// echo 'Begin show_project_program_resources.php<br /><hr />';
// https://supplementrelief.com/resources/

if (isset($_SESSION['enrollment']['project_program_id'])) { 	
	$project_program_id = $_SESSION['enrollment']['project_program_id']; 	
} else {
	$project_program_id = public_project_program_id; // /includes/defines.php
}

$_SESSION['enrollment']['project_program_id'] = $project_program_id;

$current_timestamp_detroit = date('Y-m-d H:i:s');
// echo 'Current Date and Time in Detroit : '.$current_timestamp_detroit.'<br /><hr />';

$queryProgramResources = 'SELECT 
	ppca.id, 
	ppca.project_program_id, 
	ppca.content_asset_id, 
	ppca.seq, 
	ppca.opt_in, 
	ppca.effective_date,
	ppca.scheduled_delivery_timestamp, 
	ppca.actual_delivery_timestamp, 
	ppca.scheduled_delivery_complete_timestamp, 	
	ca.title, 
	ca.summary_description, 
	ca.content_asset_type_code, 
	im.host_url, 
    urls.url AS seo_url   
	FROM project_program_content_assets ppca 
	LEFT JOIN content_assets ca on ppca.content_asset_id = ca.id 
	LEFT JOIN resource_kits rk on ppca.content_asset_id = rk.content_asset_id 
	LEFT JOIN images im ON 
		(rk.image_id = im.content_asset_id AND 
		 im.size = "Resource" AND
		 im.usage_size = "Small" AND 
		 im.usage_shape = "Block" /* AND
		 im.format = "JPG" */) 
    LEFT JOIN url_usages urlu 
        ON (ppca.content_asset_id = urlu.resource_id AND 
            urlu.effective_date <= CURRENT_DATE AND
           (urlu.end_date IS NULL or urlu.end_date >= CURRENT_DATE))
    LEFT JOIN urls urls ON urlu.url_id = urls.id 			
	WHERE ppca.project_program_id = '.$project_program_id.' 
	AND ppca.scheduled_delivery_timestamp <= "'.$current_timestamp_detroit.'" 
	AND (scheduled_delivery_complete_timestamp IS NULL or scheduled_delivery_complete_timestamp >= "'.$current_timestamp_detroit.'") 
	AND ca.content_asset_type_code = "RESO" 
	ORDER BY ppca.scheduled_delivery_timestamp DESC';
			
// echo $queryProgramResources;
// die();

$result_program_resource = mysqli_query($connection, $queryProgramResources);

if (!$result_program_resource) {
	show_mysqli_error_message($queryProgramResources, $connection);
	die;
}

if (mysqli_num_rows($result_program_resource) > 0) {
	
	?> 	
	<br />
	<!-- 
	<div class="row">						
		<div class="small-12 columns">		
			<div class="panel">
				<p>We encourage you to use the <b>Resources</b> below. Select the Title and you are taken to the page where you can view the detailed information.</p>
			</div>
		</div>	
	</div>
	-->		
	
	<?php

	while($r = mysqli_fetch_assoc($result_program_resource)) {
	
		// show_array($r);
		
        if ($r['seo_url']) {
            $resource_url = '../'.$r['seo_url'].'/';
    	} else {	
    		$resource_url = '../resource/?ppca_id='.$r['id'].'&ca_id='.$r['content_asset_id'];
    	}			
		
		?>
		<div class="row panel">	
			<div class="small-12 medium-3 columns lazyDiv">
				<center><img data-original="<?php echo $_SESSION['application']['root_media_host_url'].$r['host_url']; ?>" class="th lazy" data-tooltip data-options="disable_for_touch:true" class="has-tip tip-bottom" title="<?php echo $r['title']; ?>"></center>
			</div>
			<div class="small-12 medium-9 columns colWrapMargin">
				<a href="<?php echo $resource_url; ?>" title="view Resource"><h3 style="color: #61AFD1;"><?php echo $r['title']; ?><small>&nbsp;&nbsp;<?php echo date('m/d/y', strtotime($r['scheduled_delivery_timestamp'])); ?></small></h3></a>
				<?php echo $r['summary_description']; ?>
				<a href="<?php echo $resource_url; ?>" title="view Resource"><i class="fa fa-hand-o-right"></i>&nbsp;more info</a>
			</div>	
		</div>
		<?php
		
	}	
	
} else {

	echo '
	<br />
	<div class="row">		
		<div class="small-12 columns">											
			<div class="panel">
				<p>No Resources currently available for the <b>'.$_SESSION['project_program_name'].'</b> program.</p>
			</div>
		</div>
	</div>';	
}

mysqli_free_result($result_program_resource);

?>