<?php

// echo 'Begin show_project_promotions.php<br /><hr />';
// die;


// if ($_SESSION['user']['user_id'] == 2 ) { show_array($_SESSION); }

include_once('../includes/header.php');
	
// Set the current date and time.
$created_timestamp = date('Y/m/d H:i:s');

if (!$_SESSION['enrollment']['project_id']) { $_SESSION['enrollment']['project_id'] = public_project_id; } 
	
$queryProjectPromotions	= 'SELECT 
	pr.name, 
	pr.description, 
	pr.destination_url, 
	pr.link_text, 
	im.host_url, 
	im.caption, 
	im.alt_text 
	FROM promotions pr  
	LEFT JOIN images im ON 
	    (pr.image_id = im.content_asset_id AND 
	     im.size = "Promotion" AND 
	     im.usage_size = "Small" AND 
	     im.usage_shape = "Block" AND 
	     im.format = "JPG") 
	WHERE pr.project_id = "'.$_SESSION['enrollment']['project_id'].'"  
	AND pr.active = "1" 
	AND pr.effective_timestamp <= "'.$created_timestamp.'" 
	AND (pr.end_timestamp IS NULL OR pr.end_timestamp > "'.$created_timestamp.'")  
	ORDER BY pr.seq ASC';
			
// echo $queryProjectPromotions . '<br /><hr />';

$result_promotion = mysqli_query($connection, $queryProjectPromotions);

if (!$result_promotion) {
	show_mysqli_error_message($queryProjectPromotions, $connection);
	die;
}

$promotion = '
<!-- Promotion Slider -->
<div id="storePromotions">	
	<div class="promotion-slider">';
		
	while($r = mysqli_fetch_assoc($result_promotion)) {
	    
	    // show_array($r);
	    
	    $promotion .= '
	    <div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><a href="'.$r['destination_url'].'"><img src="'.$_SESSION['application']['root_media_host_url'].$r['host_url'].'" alt="'.$r['alt_text'].'" width="175px" ></a></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>'.$r['name'].'</h3>'
				  	.$r['description'].'
				  	<p><a href="'.$r['destination_url'].'" class="button medium radius success">'.$r['link_text'].'</a></p>			
			  	</div>
			  			
			</div>		
		</div>';
	    
	}

$promotion .= '</div></div>';

mysqli_free_result($result_promotion);

echo $promotion;

?>