<!--googleoff: index-->
<div id="standard-membership" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	
	<?php
		
	$project_program_id = 21;
		
	// Query the Project Program	
	$queryProjectProgram = '
		SELECT 
		pp.id, 
		pp.name, 
		pp.summary_description, 
		pp.description 
		FROM project_programs pp 
		WHERE pp.id = "'.$project_program_id.'"';
	
	// echo $queryProjectProgram;
	// die();
		
	// Execute the query
	$result_project_program = mysqli_query($connection, $queryProjectProgram);
	
	if (!$result_project_program) {
		show_mysqli_error_message($queryProjectProgram, $connection);
		die;
	}
	
	while($r = mysqli_fetch_assoc($result_project_program)) {
	
		// show_array($r);
		
		$project_program_name =  $r['name'];
		$project_program_summary_description = $r['summary_description']; //$r['short_description']; short_description does't exists in adobe query
		$project_program_description =  $r['description'];
		
		echo '
			<br />
			<div class="panel">
				<h2>'.$project_program_name.'</h2>';
		
				if (strlen($project_program_summary_description) > 0) {
			
					echo $project_program_summary_description;
			
				}
				
		echo '</div>';
		
		if (strlen($project_program_description) > 0) {
			
			echo $project_program_description;
			
		}
		
	}
	
	mysqli_free_result($result_project_program);
	
	?> 
   
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
  
</div>
<!--googleon: index-->

