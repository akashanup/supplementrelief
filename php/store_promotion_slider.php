<!-- Promotion Slider -->
<div id="storePromotions" class="show-for-medium-up">
		
	<div class="promotion-slider">
		
		<div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><a href="../register-member-account/"><img src="https://wrightcheck.com/media/images/free-membership-small-block.jpg" alt="free membership button" width="175px" ></a></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Become a Member: Savings & Wellness Benefits</h3>
				  	<p><b>$avings, Expert Wellness Content, Promotional Offers</b> and more...</p>
				  	<p><a href="../register-member-account/" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Learn More</b></a></p>			
			  	</div>
			  			
			</div>		
		</div>
				
        <div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<br />
			  		<center><img src="https://wrightcheck.com/media/images/numedica-logo-landscape-small.png" width="175px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Why Choose NuMedica?</h3>
				  	<p>Purity. Potency. Quality. With <b>professional-grade supplements</b> you get all three.</p>
				  	<p><a href="../why-choose-numedica/" class="button medium radius success"><b><i class="fa fa-question-circle"></i>&nbsp;&nbsp;Why NuMedica</b></a></p>					
			  	</div>		
			</div>		
		</div>
		
        <div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/numedica-power-reds-pink-lemonade-small.jpg" width="100px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>NuMedica Power Reds Pink Lemonade</h3>
				  	<p>Great-tasting <b>probiotic, antioxidant, superfood</b> powder-drink mix provides nutrition to <b>boost immunity, detox the body, and ward off disease</b>.</p>
				  	<p><a href="../numedica-power-reds-pink-lemonade/" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Check It Out!</b></a></p>			
			  	</div>
			  			
			</div>		
		</div>
		
        <div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/man-in-suit-with-head-exploding-into-smoke-small-block.jpg" width="175px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Three Secret Weapons to Combat Stress</h3>
				  	<p>Blog series shows you how to give your body <b>the nutrition it needs to cope with the daily stresses of life</b>.</p>
				  	<p><a href="../three-secret-weapons-to-combat-stress-part-1/" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;View Blog Series</b></a></p>			
			  	</div>
			  			
			</div>		
		</div>

		
        <div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/disappointed-young-woman-standing-on-medical-scale-small-block.jpg" width="175px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Why Do Diets Fail?</h3>
				  	<p>This 4-part blog series follows a Brandy's weight-loss journey and illustrates <b>common challenges many of us face</b>.</p>
				  	<p><a href="../brandy-weight-loss-journey-part-1/" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;View Blog Series</b></a></p>			
			  	</div>
			  			
			</div>		
		</div>
	
        <div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/gross-pile-of-fast-fried-food-small-block.jpg" width="175px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Evolution of Dinner</h3>
				  	<p>This 5-part blog series shows how a stay-at-home mom transitioned her family <b>from processed foods into natural, whole foods nutrition</b> that <b>radically improved her family's health</b>.</p>
				  	<p><a href="../evolution-of-dinner-part-1/" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;View Blog Series</b></a></p>			
			  	</div>
			  			
			</div>		
		</div>

        <div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/young-girl-with-food-choice-small-block.jpg" width="175px"></center>
				</div>	  							
				<div class="small-12 medium-9 columns">
			  		<h3>Webinar: Four Letter Word for F.O.O.D.</h3>
			  		<p>Facilitator <a href="../about/">Libby Wright</a> discusses the <b>unhealthy food consumed in the S.A.D (Sad American Diet)</b> and recommends alternative food choices, recipes and <b>NuMedica supplements</b>.</p>
			  		<p><a href="../resource/?ppca_id=339&ca_id=2273" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Watch Webinar</b></a></p>					
		  		</div>	
		  	</div>
	  	</div>
				
		<div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/slim-n-fit-numedica-supplement-pack-block-small.jpg" width="175px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>NuMedica Slim-N-Fit Pack</h3>
				  	<p>Our <b>best selling combo supplement pack at a savings!</b> Burn fat, eliminate toxins, and nourish your body.</p>
				  	<p><a href="../product/?p_id=1&bpu_id=305#fndtn-description" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;View Supplement Pack</b></a></p>		
			  	</div>
			  			
			</div>		
		</div>
		
		<div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/black-man-grocery-shopping-with-list-small-block.jpg" width="175px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Healthy Grocery Shopping List</h3>
				  	<p>Stock your pantry and refrigerator with these <b>fresh and healthy nutritious foods</b>.</p>
				  	<p><a href="../resource/?ppca_id=330&ca_id=2255" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;View List</b></a></p>			
			  	</div>
			  			
			</div>		
		</div>
		
        <div>
			<div class="row">		  		
			  	<div class="small-12 medium-3 columns">
				  	<center><img src="https://wrightcheck.com/media/images/recipe-cookbook-small-block.jpg" width="175px"></center>
				</div>  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Healthy Living Whole Foods Cookbook</h3>
				  	<p>This customizable cookbook features <b>amazing RAW foods recipes</b> and shares <b>tips in healthy cooking</b>.</p>
				  	<p><a href="../resource/?ppca_id=308&ca_id=1280" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;View Cookbook</b></a></p>						
			  	</div>
			</div>	
		</div>
		
        <div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/attractive-overweight-hispanic-woman-small-block.jpg" width="175px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Supplements for Resetting Your Metabolism</h3>
				  	<p>Sometimes, it's more about <b>getting your body to respond appropriately to the fuel you give it</b>. This blog series explores <b>NuMedica's weight-loss programs</b>.</p>
				  	<p><a href="../show_blog_post/?bp_id=1430" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Read Blog Post Series</b></a></p>						
			  	</div>		
			</div>		
		</div>				
	
        <div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/head-with-illuniated-brain-small-block.jpg" width="175px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Fatigue, Brain Fog, Memory, Sleep, ADHD & Behavioral Issues?</h3>
				  	<p>What if <b>supplements</b> could <b>reduce memory and sleep problems</b> and <b>improve behavioral issues</b>?</p>
				  	<p><a href="../resource/?ppca_id=337&ca_id=2262" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Read Article</b></a></p>			
			  	</div>
			  			
			</div>		
		</div>
		
        <div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/numedica-core-nutrition-supplement-pack-small-portrait.jpg" width="120px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Core Nutrition Pack</h3>
				  	<p>Our <b>most powerful and versatile supplement pack at a savings!</b> Three quality NuMedica supplements for everyone to <b>nourish your body, boost energy and reduce the effects of stress</b>.</p>
				  	<p><a href="../numedica-core-nutrition-supplement-pack/" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;View Supplement Pack</b></a></p>		
			  	</div>
			  			
			</div>		
		</div>
		
        <div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/young-black-man-sleeping-on-couch-resource-small-block.jpg" width="175px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Webinar: Sleep & Weight Loss</h3>
				  	<p>Facilitator <a href="../about/">Libby Wright</a> interviews Dr. Jamie Wright <b>about the role of sleep in weight loss</b> and <b>recommends NuMedica supplements</b>.</p>
				  	<p><a href="../resource/?ppca_id=315&ca_id=2240" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Watch Webinar</b></a></p>						
			  	</div>		
			</div>		
		</div>
		
        <div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/boy-protected-by-immune-system-small-block.jpg" width="175px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Why You Should Care About Gut Health</h3>
				  	<p>The <b>GUT</b> is where approximately <b>80% of your immune system lives</b>. Here's <b>why you should care!</b></p>
				  	<p><a href="../show_blog_post/?bp_id=1391" class="button medium radius success"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;<b>Read Blog Post</b></a></p>						
			  	</div>		
			</div>		
		</div>				

		<div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/middle-aged-man-sitting-at-desk-small-block.jpg" width="175px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Webinar: Eight and One Half Steps to Relieve Stress</h3>
				  	<p>Facilitator <a href="../about/">Libby Wright</a> discusses how we perceive situations and provides <b>practical tips for managing stress</b> and <b>recommends NuMedica supplements</b>.</p>
				  	<p><a href="../resource/?ppca_id=320&ca_id=2247" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Watch Webinar</b></a></p>
			  	</div>		
			</div>		
		</div>
						
		<div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/chocolate-almond-delightful-drink-block-small.jpg" width="175px"></center>
				</div>		  							
			  	<div class="small-12 medium-9 columns">
				  	<h3>Healthy Chocolate Recipes?</h3>
				  	<p>You bet! <b>Try these out with family and friends</b>. And they are fun to make!</p>
				  	<p><a href="../resource/?ppca_id=276&ca_id=1280&keyword=chocolate" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;View Recipes</b></a></p>						
			  	</div>		
			</div>		
		</div>
					  	  
		<div>
		  	<div class="row">		  		
		  		<div class="small-12 medium-3 columns">
			  		<center><img src="https://wrightcheck.com/media/images/middle-aged-woman-drinking-water-after-workout-small-block.jpg" width="175px"></center>
				</div>	  							
				<div class="small-12 medium-9 columns">
			  		<h3>Webinar: Hydration & Weight Loss</h3>
			  		<p>Facilitator <a href="../about/">Libby Wright</a> discusses the <b>role of hydration in weight loss</b> and <b>recommends NuMedica supplements</b>.</p>
			  		<p><a href="../resource/?ppca_id=316&ca_id=2234" class="button medium radius success"><b><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Watch Webinar</b></a></p>					
		  		</div>	
		  	</div>
	  	</div>
	  	
	</div>
		  
</div>