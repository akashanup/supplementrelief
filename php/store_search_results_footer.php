
<!-- contains a row for # Search Results & Page Navigation -->						
<div class="row"> <!-- display Page Navigation row -->

	<!-- # Products Found -->
	<div class="small-12 medium-3 columns colWrapMargin">
		<!-- <div>&nbsp;<i class="fa fa-binoculars"></i>&nbsp;&nbsp;<?php echo $total_records; ?></div> -->
		<div id="productsFound"><center>Products Found:&nbsp;&nbsp;<?php echo $total_records; ?></center></div>			
	</div>
		
	<!-- Page Navigation -->	
	<div class="small-12 medium-6 columns">
		
		<div class="row collapse">
			
			<!--
			<div class="small-3 medium 2 columns">
				<div class="right">Pages:</div>			
			</div>
			-->
			
			<div class="small-12 medium 12 columns">
			
				<div id="pagination-setup" class="pagination-centered">
				<?php
				//Check for total no. of pages which are greater than 6 #6
				if ($last_page > 6)
				{
					//Execute if the page number is one  #7
					if ($page == 1)
					{
						$next  = $page + 1;
						$next1 = $page + 2;
						$next2 = $page + 3;
						$url = "<ul class='pagination'>"."<li class='arrow unavailable'><a href=''>&laquo;</a></li>".
						"<li class='current'><a href='"."?page=".$page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$page."</a></li>".
						"<li><a href='"."?page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next."</a></li>".
						"<li><a href='"."?page=".$next1."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next1."</a></li>".
						"<li><a href='"."?page=".$next2."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next2."</a></li>".
						"<li class='unavailable'><a href=''>&hellip;</a></li>".
						"<li><a href='"."?page=".$last_prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_prev."</a></li>".
						"<li><a href='"."?page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_page."</a></li>".
						"<li><a href='"."?page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&raquo;"."</a></li></ul>";
						echo $url;	
					}
					//Execute if the page number is equal to last page number #8
					else if ($page == $last_page)
					{
						$first = 1;
						$first1 = $first + 1;
						$first2 = $first + 2;
						$first3 = $first + 3;
						$url= "<ul class='pagination'>".
						"<li class='arrow'><a href='"."?page=".$last_prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&laquo;</a></li>".
						"<li><a href='"."?page=".$first."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first."</a></li>".
						"<li><a href='"."?page=".$first1."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first1."</a></li>".
						"<li><a href='"."?page=".$first2."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first2."</a></li>".
						"<li><a href='"."?page=".$first3."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first3."</a></li>".
						"<li class='unavailable'><a href=''>&hellip;</a></li>".
						"<li><a href='"."?page=".$last_prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_prev."</a></li>".
						"<li class='current'><a href='"."?page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_page."</a></li>".
						"<li class='arrow unavailable'><a href='' >"."&raquo;"."</a></li></ul>";
						echo $url;
					}
					//Execute if the page number is equal to 2 #9
					else if ($page == 2)
					{
						$next = $page + 1;
						$prev = $page - 1;
						$next1 = $page + 2;
						$url = "<ul class='pagination'>".
						"<li class='arrow'><a href='"."?page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&laquo;</a></li>".
						"<li><a href='"."?page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$prev."</a></li>".
						"<li class='current'><a href='"."?page=".$page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$page."</a></li>".
						"<li><a href='"."?page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next."</a></li>".
						"<li><a href='"."?page=".$next1."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next1."</a></li>".
						"<li><a href=''>&hellip;</a></li>"."<li><a href='"."?page=".$last_prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_prev."</a></li>".
						"<li><a href='"."?page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_page."</a></li>".
						"<li><a href='"."?page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&raquo;"."</a></li></ul>";
						echo $url;
					}
					//Execute if the page number is greater than 2 and less than page number  of previous of  previous  of last page #10
					//means lastpage number-2
					elseif ($page > 2 && $page < ($last_prev - 1))
					{
						$next = $page + 1;
						$prev = $page - 1;
						$first = 1;
						echo "<ul class='pagination'>".
						"<li class='arrow'><a href='"."?page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&laquo;</a></l>".
						"<li><a href='"."?page=".$first."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first."</a></li>".
						"<li class='unavailable'><a href=''>&hellip;</a></li>".
						"<li><a href='"."?page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$prev."</a></li>".
						"<li class='current'><a href='"."?page=".$page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$page."</a></li>".
						"<li><a href='"."?page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next."</a></li>".
						"<li><a href=''>&hellip;</a></li>"."<li><a href='"."?page=".$last_prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_prev."</a></li>".
						"<li><a href='"."?page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_page."</a></li>".
						"<li><a href='"."?page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&raquo;"."</a></li></ul>";
						echo $url;
					}
					//Execute if the page number is equal to page number of previous page of last page #11
					//means last page number-1
					else if ($page == $last_prev)
					{
						$first = 1;
						$first1 = $first + 1;
						$first2 = $first + 2;
						$first3 = $first + 3;
						$prev = $page - 1;
						$url = "<ul class='pagination'>".
						"<li class='arrow'><a href='"."?page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&laquo;</a></li>".
						"<li><a href='"."?page=".$first."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first."</a></li>".
						"<li class='unavailable'><a href=''>&hellip;</a></li>"."<li><a href='"."?page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$prev."</a></li>".
						"<li class='current'><a href='"."?page=".$last_prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_prev."</a></li>".
						"<li><a href='"."?page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_page."</a></li>".
						"<li><a href='"."?page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&raquo;"."</a></li></ul>";
						echo $url;
					}
					//Execute if the page number is equal to than page number  of previous of  previous  of last page #12
					else if ($page == ($last_prev - 1))
					{
						$first = 1;
						$first1 = $first + 1;
						$first2 = $first + 2;
						$first3 = $first + 3;
						$prev = $page - 1;
						$next = $page + 1;
						$url= "<ul class='pagination'>".
						"<li class='arrow'><a href='"."?page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&laquo;</a></li>".
						"<li><a href='"."?page=".$first."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$first."</a></li>".
						"<li class='unavailable'><a href=''>&hellip;</a></li>".
						"<li><a href='"."?page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$prev."</a></li>".
						"<li class='current'><a href='"."?page=".$page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$page."</a></li>".
						"<li><a href='"."?page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$next."</a></li>".
						"<li><a href='"."?page=".$last_page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$last_page."</a></li>".
						"<li><a href='"."?page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&raquo;"."</a></li></ul>";
						echo $url;
					}
					
				} // if ($last_page > 6)
				elseif($limit == 'all')
				{
					$url="<ul class='pagination'>".
						"<li class='arrow unavailable'><a href=''>&laquo;</a></li>".
						"<li class='current'><a href='"."?page=".$page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$page."</a></li>".
						"<li class='arrow unavailable'><a href=''>&laquo;</a></li>";
						echo $url;
				}
				
				// if total no. of pages are less than 6  #13
				else
						
				{
					$prev = $page - 1;
					$next = $page + 1;
					//if page number will be one ,then previous link will be disabled  #14
					if ($page == 1)
					{
						$url="<ul class='pagination'>".
						"<li class='arrow unavailable'><a href=''>&laquo;</a></li>";
					}
					else
					{
						$url="<ul class='pagination'>".
						"<li class='arrow'><a href='"."?page=".$prev."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&laquo;</a></li>";
					}
					// it will display all page numbers up to last page number 
					for ($i=1 ; $i <= $last_page ; $i++)
					{
						// if the current page number will be matched with the counter value, then it will enable active status to that page number. #15
						if ($i == $page)
						{
							$url.="<li class='current'><a href='"."?page=".$page."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$page."</a></li>";
						}
						else
						{
							$url.="<li><a href='"."?page=".$i."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>".$i."</a></li>";
						}
					}
						//if the current page number will be equal to the last page number ,then next link will be disabled. #16
					if ($page == $last_page)
					{
						$url.="<li class='arrow unavailable'><a href='' >"."&raquo;"."</a></li></ul>";
					}
					else
					{
						$url.="<li><a href='"."?page=".$next."&limitby=".$limit.$alpha_href.$keyword_href.$orderby_href.$new_item_href.$featured_item_href.$program_item_href.$pack_item_href.$sample_item_href.$education_item_href.$vegetable_capsule_href.$gluten_free_href.$vegetarian_href.$albion_minerals_href.$gos_id_href.$ingr_id_href."'>"."&raquo;"."</a></li></ul>";
					}
					echo $url;
				}
				?>
				</div> <!-- END <div id="pagination-centered"> -->
				
			</div>
			
		</div>
		
	</div> <!-- END display Page Navigation columns -->
	
	<!-- Advanced Search Switch -->
	<!--
	<div class="small-12 small-only-text-center medium-1 columns">
		
		<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip tip-left radius" title="Advanced Search Options">
			<div class="switch">
				<input id="advancedSearchSwitch" type="checkbox">
				<label for="advancedSearchSwitch"></label>
			</div>
		</span> 	
		
	</div>
	-->
	
	<!-- Advanced Search Switch -->
	<div class="small-12 small-only-text-center medium-3 columns">
        <a href="../why-choose-numedica" title="Why should I choose NuMedica supplements?"><i class="fa fa-question fa-lg"></i>&nbsp;<em>Why choose NuMedica?</em></a>
		<a id="toggleAdvancedSearch" title="view Advanced Search Options"><i class="fa fa-binoculars"></i>&nbsp;Advanced Search</a>&nbsp;
		<a href="javascript:void(0)" data-reveal-id="searchHelp" rel="self" title="view Search Help"><i class="fa fa-question fa-lg"></i>&nbsp;Help</a>
		
		<div class="show-for-small-only">
			<br>
		</div>
				
	</div>
		
	<hr>

</div> <!-- END contains a row for User Page Navigation -->


<div id="shippingHandling" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	
	<div class="panel">
		<h3>Order Policy</h3>
		<p>Please call <b>(888) 424-0032</b> or <a href="../contact/" title="Contact SupplementRelief.com Customer Service">email</a> if you have questions or need assistance.</p>
	</div>
	
	<?
  
	// Query the Project Shipping & Handling Policy	
	$queryProject = 'SELECT 
		pj.payment_policy, 
		pj.email_shipping_policy, 
		pj.email_refund_policy 
		FROM projects pj
		WHERE pj.id = "'.$project_id.'"';
		
	// echo $queryProject;
	// die();
		
	// Execute the query
	$result_project = mysqli_query($connection, $queryProject);
	
	if (!$result_project) {
		show_mysqli_error_message($queryProject, $connection);
		die;
	}
	
	while($r = mysqli_fetch_assoc($result_project)) {
	
		// show_array($r);
		
		echo '<h3>Payments</h3>'.$r['payment_policy'].'<hr>';
				
		echo '<h3>Privacy</h3>'.$_SESSION['application']['privacy_policy'].'<hr>';
		
		echo '<h3>Shipping & Handling</h3>'.$r['email_shipping_policy'].'<hr>';
		
		echo '<h3>Returns & Refunds</h3>'.$r['email_refund_policy'];
		
	}
	
	mysqli_free_result($result_project);
	
	?>
	
	<img src="https:/wrightcheck.com/media/images/package-delivery-large-landscape.jpg" alt="orders are delivered to your door">				
	
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
  
</div>

<div id="searchHelp" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	
	<div class="panel">
		<h3>Search Help</h3>
		<p>Please call <b>(888) 424-0032</b> or <a href="../contact/" title="Contact SupplementRelief.com Customer Service">email</a> if you have questions or need assistance finding a product. You can also place your order over the phone.</p>	
		<p><b>On August 1, 2015, NuMedica revised their product catalogue</b>. Some products are no longer manufactured and sold. Click <a href="#" data-reveal-id="discontinuedProducts">here</a> to see which products have been discontinued and the recommended alternative.</p>
	</div>
	
	<center><img src="https:/wrightcheck.com/media/images/numedica-supplements-search-help-large-landscape.jpg" alt="help for using the NuMedica supplements search form"></center>
	<br>
						
	<ol>
		<li>Call <b>(888) 424-0032</b> to place your order over the phone.</li>
		<li>Select <b>View Cart</b> to see the items and quantities currently in your shopping cart.</li>
		<li>Select <b>Shipping</b> to see the Shipping & Handling policy.</li>
		<li>Select <b>Membership</b> to see the benefits of our FREE membership.</li>
		<li>We accept <b>Visa, MasterCard, Discover and American Express</b> forms of credit card payment.</li>
		<li>Enter <b>Search Keyword(s)</b> to help find a specific product. You may also enter the UPC or SKU if known.</li>
		<li>Select the <b>Search Button</b> after entering your search keywords to execute your search. Search executes automatically when changing the value in any of the other search-related fields.</li>
		<li>Select <b>View</b> to limit the number of products shown per page. The default is 20 products per page.</li>
		<li>Select <b>Order By</b> to sort the products in the results list. The default sort order is by Product Name.</li>
		<li>Select <b>View By Alphabet</b> to see the items group by the beginning letter of the Product Name.</li>
		<li>Select <b>View By Category</b> to see the items grouped by Health-related categories.</li>
		<li>Select <b>View By Ingredient</b> to see the items grouped by ingredient used in a product.</li>
		<li><b>Advanced Search Options</b> displays a group of additional options for grouping products.</li>
		<li>Select <b>New, Popular, Programs, Packs and Samples</b> to see the items grouped by these respective categories. You may select more than one at a time.</li>
		<li>Select <b>Vegetable Capsule, Gluten Free, Vegetarian or Albion Minerals</b> to see the items grouped by these respective categories. You may select more than one at a time.</li>
		<li><b>Products Found</b> shows the number of products found that match the current search criterion.</li>
		<li>Select a <b>Page Number</b> to see the items grouped by page. This is related to #8 where you can specify the number of products to show per page.</li>
		<li>Select <b>Advanced Search</b> to see additional search options. These are described in #13, #14 and #15.</li>
		<li>Select <b>Help</b> to see the Help for how to use the search options.</li>
	</ol>
								
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
  
</div>
