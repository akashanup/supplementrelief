<?php

// echo 'Begin topic_assessments.php<br /><hr />';

$content_asset_type_code = '("ASSESSMENT")';

$queryTopicAssessments = 'SELECT 
	ca.id AS assessment_id, 
	ca.title, 
	ca.summary_description, 
	ca.description,
	at.name AS assessment_type_name,
	art.name AS assessment_rubric_type_name, 
	im.host_url AS assessment_image_url, 
	im.caption AS assessment_image_caption, 
	im.alt_text AS assessment_image_alt_text 
	FROM content_asset_usages cau 
	LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id 
	LEFT JOIN assessments am ON ca.id = am.content_asset_id 
	LEFT JOIN assessment_types at ON am.assessment_type_code = at.code 
	LEFT JOIN assessment_rubric_types art ON am.assessment_rubric_type_code = art.code  
	LEFT JOIN images im ON  
	(am.image_id = im.content_asset_id AND 
	 im.SIZE = "Assessment" AND 
	 im.usage_size = "Medium" AND 
	 im.usage_shape = "Block" AND 
	 im.format = "JPG") 
	WHERE cau.content_asset_parent_id = '.$topic_id.' 
	AND ca.content_asset_type_code IN '.$content_asset_type_code.' 
	AND am.active = 1   
	ORDER BY cau.featured DESC, cau.seq ASC, ca.title ASC'; 

// echo $queryTopicAssessments . '<br /><hr />';
// die;

$result_topic_assessment = mysqli_query($connection, $queryTopicAssessments);

if (!$result_topic_assessment) {
	show_mysqli_error_message($queryTopicAssessments, $connection);
	die;
}

// Initialize content asset types variables to store output for display on Education page.

$assessment_block = '';
			
if (mysqli_num_rows($result_topic_assessment) > 0) { 
    
    $assessment_block .= '<br />';

	while($r = mysqli_fetch_assoc($result_topic_assessment)) {
	
		// show_array($r);
		
		if ($r['assessment_image_url']) { 
			$image_url = $_SESSION['application']['root_media_host_url'].$r['assessment_image_url'];
		} else {
			$image_url = 'https:/placehold.it/250x250?text=no+image+selected'; 
		}
				
		$assessment_block .= '
		<div class="row">		
			<div class="small-12 medium-4 columns text-centered">
				<center><img src="'.$image_url.'" alt="'.$assessment_image_alt_text.'" width="200"></center> 					
			</div>
			
			<div class="small-12 medium-8 columns">
				<h3><a href="../assessment/?id='.$r['assessment_id'].'" title="'.$r['title'].'">'.$r['title'].'</a></h3>'.
				$r['summary_description'].'
				<span class="label">'.$r['assessment_type_name'].'</span>&nbsp;&nbsp;<span class="label">'.$r['assessment_rubric_type_name'].'</span>	
			</div>

		</div>';
		
	}
}

?>