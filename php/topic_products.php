<?php

// echo 'Begin topic_products.php<br /><hr />';
// die;

if (isset($_SESSION['enrollment']['project_program_id'])) { 	
	$project_program_id = $_SESSION['enrollment']['project_program_id']; 	
} else {
	$project_program_id = public_project_program_id; // store/includes/defines.php
}

$_SESSION['enrollment']['project_program_id'] = $project_program_id;

$current_timestamp = date('Y-m-d H:i:s');


// Get any current Project Brand Products associated with this Topic. 
$queryTopicProjectBrandProducts = 'SELECT 
    bpu.name AS addendum_product_name,
    pbpu.group_options_description AS addendum_product_group_options_description,  
    pbpu.page_meta_description AS addendum_product_page_meta_description, 
	pbpu.page_canonical_url AS addendum_product_canonical_url, 
	urls.url AS addendum_product_url, 
    im.host_url AS addendum_product_image_url, 
	im.alt_text AS addendum_product_image_alt_text 
	FROM content_asset_project_brand_products capbp 
	LEFT JOIN project_brand_product_usages pbpu ON capbp.project_brand_product_usage_id = pbpu.id 
	LEFT JOIN url_usages urlu ON pbpu.id = urlu.pbpu_id 
	LEFT JOIN urls urls ON urlu.url_id = urls.id 
	LEFT JOIN brand_product_usages bpu ON pbpu.brand_product_usage_id = bpu.id 
    LEFT JOIN images im 
	    ON (bpu.image_id = im.content_asset_id AND 
            im.size = "Product" AND 
            im.usage_size = "Small" AND 
            /* im.usage_shape = "Portrait" AND */ 
            im.format = "JPG")
	WHERE capbp.content_asset_id = '.$topic_id.' 
	AND (capbp.effective_date <= CURRENT_DATE AND
        (capbp.end_date IS NULL or capbp.end_date >= CURRENT_DATE)) 
    AND pbpu.active = "1" 
    ORDER BY bpu.name'; 
    
// echo $queryTopicProjectBrandProducts . '<br /><hr />';

$result_topic_project_brand_product = mysqli_query($connection, $queryTopicProjectBrandProducts);

if (!$result_topic_project_brand_product) {
	show_mysqli_error_message($queryTopicProjectBrandProducts, $connection);
	die;
}
				
$product_block = '';

while($pbp = mysqli_fetch_assoc($result_topic_project_brand_product)) { 
	
	if (!$product_block) {
		// Create the Product Addendum container.
		$product_block .= '
			<!--googleoff: index-->
		    <div class="panel callout radius">
		        <div class="small-12 columns">
		            <h3><i class="fa fa-product-hunt"></i>  Supplement Referrals</h3>
		            <p>Learn more about these <b>NuMedica Supplements</b> on our website.</p>
                </div>';
	}
	
    $addendum_product_image_url = $pbp['addendum_product_image_url'];
	$addendum_product_image_alt_text = $pbp['addendum_product_image_alt_text'];
	$addendum_product_url = $pbp['addendum_product_url'];
	$addendum_product_page_meta_description	= $pbp['addendum_product_page_meta_description'];			
        
    $product_block .= '
	    <hr>
	    <div class="row">		
            <div class="small-12 medium-2 columns text-center">
	            <img class="th" src="'.$_SESSION['application']['root_media_host_url'].$addendum_product_image_url.'" alt="'.$addendum_product_image_alt_text.'" width="125px">
            </div>
            <div class="small-12 medium-10 columns colWrapMargin">
                <a href="../'.$addendum_product_url.'/"><h4>'.$pbp['addendum_product_name'].'</a></h4>'.
                $pbp['addendum_product_page_meta_description'].'
             </div>
        </div>';		
}

if ($product_block) { 
	    
	$product_block .= '
	    </div> <!-- Close Panel -->
	    <!--googleon: index-->';
}

mysqli_free_result($result_topic_project_brand_product);

?>