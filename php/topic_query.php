<?php

// echo 'Begin topic_query.php<br /><hr />';

// https://supplementrelief.com/education/?c_id=1772&t_id=1777&wp_id=1778

if (isset($_SESSION['enrollment']['topic_id'])) {
	$topic_id = $_SESSION['enrollment']['topic_id'];	
 } else {
 	$topic_id = $first_topic_id; 
 	// Don't think this is needed anymore since Login takes user to User Dashboard where they must select a Topic from the Course Outline.
 	// Set in Course Outline query as a default to display something in the Education page right after Login before they have had a chance to select a Topic from the navigation.	 
}

// echo 'Topic ID: '.$topic_id.'<br />';

// Get the Topic title.
$queryTopicTitle = "SELECT title from content_assets WHERE id = '$topic_id' LIMIT 1";

// echo $queryTopicTitle . '<br /><hr />';

$result_topic_title = mysqli_query($connection, $queryTopicTitle);

if (!$result_topic_title) {
	show_mysqli_error_message($queryTopicTitle, $connection);
	die;
}

while($r = mysqli_fetch_assoc($result_topic_title)) {
	$topic_page_title = cleanEncoding($r['title']);
	// Where is this displayed to the user or used in any way? We use Web Page Title.
}

mysqli_free_result($result_topic_title);

// Get the Topic Learning/Behavioral Objectives and Discussion Forum settings.

$queryTopic	= 'SELECT 
	tp.learning_objective, 
	tp.behavioral_objective, 
	tp.quiz, 
	tp.discussion_forum, 
	dfc.discussion_thread_post, 
	dfc.discussion_thread_post_document, 
	dfc.discussion_thread_post_image, 
	dfc.discussion_thread_post_video,
	dfc.discussion_thread_post_audio, 
	dfc.discussion_reply_post, 
	dfc.discussion_reply_post_document, 
	dfc.discussion_reply_post_image, 
	dfc.discussion_reply_post_video,
	dfc.discussion_reply_post_audio 
	FROM topics tp  
	LEFT JOIN discussion_forum_configurations dfc ON tp.content_asset_id = dfc.topic_id 		
	WHERE tp.content_asset_id = '.$topic_id.' LIMIT 1'; 
									
// echo $queryTopic . '<br /><hr />';

$result_topic = mysqli_query($connection, $queryTopic);

if (!$result_topic) {
	show_mysqli_error_message($queryTopic, $connection);
	die;
}

// echo 'Queried Topic successfully.<br /><hr />';

while($topic = mysqli_fetch_assoc($result_topic)) {

	// show_array($topic);
	
	$_SESSION['topic']['topic_id'] = $topic_id;
			
	$learning_objective = $topic['learning_objective'];
     // echo 'Learning Objective: ' . $learning_objective . '<br />';
	
	$behavioral_objective = $topic['behavioral_objective'];
	// echo 'Behavioral Objective: ' . $behavioral_objective .'<br /><hr />';
	
    $quiz = $topic['quiz'];
	// echo 'Quiz: ' . $quiz .'<br /><hr />';
	// Quiz has matured into Assessment but may still be used for external Quiz solutions.
	
	if ($topic['discussion_forum']) {
		// Set session variables to persist the Discussion Forum Configuration for this Recipe
		$_SESSION['topic']['discussion_forum']['forum'] = $topic['discussion_forum'];	
		$_SESSION['topic']['discussion_forum']['thread_post'] = $topic['discussion_thread_post'];
		$_SESSION['topic']['discussion_forum']['thread_post_document'] = $topic['discussion_thread_post_document'];	
		$_SESSION['topic']['discussion_forum']['thread_post_image'] = $topic['discussion_thread_post_image'];
		$_SESSION['topic']['discussion_forum']['thread_post_video'] = $topic['discussion_thread_post_video'];
		$_SESSION['topic']['discussion_forum']['thread_post_audio'] = $topic['discussion_thread_post_audio'];
		$_SESSION['topic']['discussion_forum']['reply_post'] = $topic['discussion_reply_post'];
		$_SESSION['topic']['discussion_forum']['reply_post_document'] = $topic['discussion_reply_post_document'];	
		$_SESSION['topic']['discussion_forum']['reply_post_image'] = $topic['discussion_reply_post_image'];
		$_SESSION['topic']['discussion_forum']['reply_post_video'] = $topic['discussion_reply_post_video'];		
		$_SESSION['topic']['discussion_forum']['reply_post_audio'] = $topic['discussion_reply_post_audio'];		
	} else {
		unset($_SESSION['topic']['discussion_forum']);
	}
	  	
}

mysqli_free_result($result_topic);

?>