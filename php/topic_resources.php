<?php

// echo 'Begin topic_resources.php<br /><hr />';
// die;

if (isset($_SESSION['enrollment']['project_program_id'])) { 	
	$project_program_id = $_SESSION['enrollment']['project_program_id']; 	
} else {
	$project_program_id = public_project_program_id; // store/includes/defines.php
}

$_SESSION['enrollment']['project_program_id'] = $project_program_id;

$current_timestamp = date('Y-m-d H:i:s');

$content_asset_type_code = '("BLOGP", "RESO")';

$queryTopicResources = 'SELECT 
	ca.id AS content_asset_id, 
	ca.content_asset_type_code, 
	ca.title, 
	ca.summary_description, 
	ca.description, 
	ca.citations, 
	cau.content_asset_child_id, 
	cau.featured, 		
	cau.seq, 
	imbp.host_url AS blog_post_image_url, 
	imbp.caption AS blog_post_image_caption, 
	imbp.alt_text AS blog_post_image_alt_text, 
    imrk.host_url AS resource_image_url, 
	imrk.caption AS resource_image_caption, 
	imrk.alt_text AS resource_image_alt_text, 
	urlsbp.url AS blog_post_url, 
	urlsrk.url AS resource_url  

	/* ppca.id AS project_program_content_asset_id  */
	FROM content_asset_usages cau 
	LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id 
	LEFT JOIN blog_posts bp ON ca.id = bp.content_asset_id 
		LEFT JOIN images imbp ON 
		(bp.image_id = imbp.content_asset_id AND 
		 imbp.size IN  ("Blog Post") AND
		 imbp.usage_size = "Small" AND 
		 imbp.usage_shape = "Block" AND
		 imbp.format = "JPG") 
    LEFT JOIN resource_kits rk on ca.id = rk.content_asset_id 
	LEFT JOIN images imrk ON 
		(rk.image_id = imrk.content_asset_id AND 
		 imrk.size IN  ("Blog Post", "Resource") AND
		 imrk.usage_size = "Small" AND 
		 imrk.usage_shape = "Block" /* AND
		 imrk.format = "JPG" */) 
    /* LEFT JOIN project_program_content_assets ppca ON 
        (ca.id = ppca.content_asset_id AND 
         ppca.project_program_id = '.$project_program_id.' AND 
         ppca.scheduled_delivery_timestamp <= "'.$current_timestamp.'" AND 
        (scheduled_delivery_complete_timestamp IS NULL or scheduled_delivery_complete_timestamp >= "'.$current_timestamp.'"))  */ 
        
    LEFT JOIN url_usages urlubp ON ca.id = urlubp.blog_post_id 
	LEFT JOIN urls urlsbp ON urlubp.url_id = urlsbp.id 
	
	LEFT JOIN url_usages urlurk ON ca.id = urlurk.resource_id 
	LEFT JOIN urls urlsrk ON urlurk.url_id = urlsrk.id 
     
	WHERE cau.content_asset_parent_id = '.$topic_id.' 
	AND ca.content_asset_type_code IN '.$content_asset_type_code.' 
	AND cau.category_code = "RESOAD" 
	AND (cau.effective_date <= CURRENT_DATE AND (cau.end_date is NULL or cau.end_date >= CURRENT_DATE))   
	ORDER BY cau.featured DESC, cau.seq ASC, ca.title ASC'; 

// echo $queryTopicResources . '<br /><hr />';
// die;

$result_topic_resource = mysqli_query($connection, $queryTopicResources);

if (!$result_topic_resource) {
	show_mysqli_error_message($queryTopicResources, $connection);
	die;
}

// Initialize content asset types variables to store output for display on Education page.

$resource_block = '';
			
if (mysqli_num_rows($result_topic_resource) > 0) { 
    
    // $resource_block .= '<br />';

	while($r = mysqli_fetch_assoc($result_topic_resource)) {
	
	    // show_array($_SESSION);
		// show_array($r);
		
		if ($r['featured']) { 
			$featured_label = '<span class="label info radius"><b>Featured</b></span>'; 
		} else {
			$featured_label = '';
		}
		
		if ($r['content_asset_type_code'] == "BLOGP") {
			$image_host_url = $r['blog_post_image_url'];
			$image_caption = $r['blog_post_image_caption'];
			$image_alt_text = $r['blog_post_image_alt_text'];
			$target_url = $r['blog_post_url'];
		} elseif ($r['content_asset_type_code'] == "RESO") { 
			$image_host_url = $r['resource_image_url'];
			$image_caption = $r['resource_image_caption'];
			$image_alt_text = $r['resource_image_alt_text'];
			$target_url = $r['resource_url'];
		}
		
		$resource_block .= '
		<div class="row">
			<!--
			<div class="small-12 columns">
				<a href="../'.$target_url.'/" title="View Resource: '.$r['title'].'"><h3>'.$r['title'].' '.$featured_label.'</h3></a>
			</div>
			-->
			<div class="small-12 medium-3 columns">
				<center><img src="'.$_SESSION['application']['root_media_host_url'].$image_host_url.'" alt="'.$image_alt_text.'"></center>
				<!-- <center>'.$featured_label.'</center> -->			
			</div>
            <div class="small-12 medium-9 columns">
            	<h3><a href="../'.$target_url.'/" title="View Resource: '.$r['title'].'.">'.$r['title'].' '.$featured_label.'</a></h3>'.
				$r['summary_description'].'
				<!-- <a href="../'.$target_url.'/" title="View Resource: '.$r['title'].'."><i class="fa fa-hand-o-right"></i>&nbsp;view</a>	-->						
			</div>
		</div>
		<hr>';
		
		/*
		$resource_block .= '
		<div class="row">
			<div class="small-12 columns">
				<h3>'.$r['title'].'</h3>
			</div>
			<div class="small-12 medium-3 columns">
				<center><img src="'.$_SESSION['application']['root_media_host_url'].$image_host_url.'"></center>
				<center><a href="../resource/?ppca_id='.$r['project_program_content_asset_id'].'&ca_id='.$r['content_asset_child_id'].'" title="View Resource: '.$r['title'].'."><i class="fa fa-hand-o-right"></i>&nbsp;view</a></center>					
			</div>
            <div class="small-12 medium-9 columns">'.
				$r['summary_description'].'					
			</div>
		</div>
		<hr>';
		*/
	
	}	
	
}

mysqli_free_result($result_topic_resource);

?>