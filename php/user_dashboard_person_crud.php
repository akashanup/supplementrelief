<?php 

// echo 'Begin user_dashboard_person_crud.php.<br />';

include_once('../includes/header.php');

/*
echo '<pre>';
print_r($_SESSION);
echo '</pre>';

echo '<pre>';
print_r($_SERVER);
echo '</pre>';
*/

// Capture calling URI to return after processing.
$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#account';

if (isset($_POST['action']) && $_POST['action'] == 'up_pe') {

	// set POST rvalues to local variables for return values
	$person_id = safe_sql_data($connection, $_POST['person_id']);
	// $person_first_name = addslashes(cleanEncoding($_POST['first_name']));
	$person_first_name = safe_sql_data($connection, $_POST['first_name']);
	$person_last_name = safe_sql_data($connection, $_POST['last_name']);
	$person_phone = safe_sql_data($connection, $_POST['phone']);
	$person_birth_date = safe_sql_data($connection, $_POST['birth_date']);
	$person_gender = safe_sql_data($connection, $_POST['gender']);
	
	// echo 'First Name: '.$person_first_name.'<br />';
	// echo 'Last Name: '.$person_last_name.'<br />';
	// echo 'Gender: '.$person_gender.'<br /><hr />';
		
	$mysql_birth_date = date('Y-m-d H:i:s', strtotime($person_birth_date));
	// $mysql_birth_date = date('Y-m-d', $person_birth_date); // did not work
		
	$queryPerson = 'UPDATE persons SET 
	first_name = "'.$person_first_name.'", 
	last_name = "'.$person_last_name.'", 
	phone = "'.$person_phone.'", 
	birth_date = "'.$mysql_birth_date.'", 
	gender = "'.$person_gender.'" 
	WHERE id = "'.$_SESSION['user']['user_id'].'"';
	
	//echo $queryPerson;
	$result_person = mysqli_query($connection, $queryPerson);
	
	// RESULT MESSAGE
	$_SESSION['message_type'] = 'alert-box success radius';				
	$_SESSION['message'] = '<p>Your <strong>Account Information</strong> has been updated.</p>';
		
}

// Return to calling form using target uri	
header("location: ".$_SESSION['target_uri']);
exit();	

?>