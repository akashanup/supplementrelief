<?php 

// echo 'Begin user_dashboard_person_location_crud.php.<br />';

include_once('../includes/header.php');

// show_array($_SESSION);
// show_array($_SERVER);
// show_array($_POST);
// die();

// Capture calling URI to return after processing.

// $_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#addresses';
$_SESSION['target_uri'] = $_SERVER['HTTP_REFERER'].'#addresses';

if (isset($_POST['action']) && $_POST['action'] == 'new') {

	// show_array($_POST);
	// die();
	
	// set POST return values to local variables		
	// $address_type_code = safe_sql_data($connection, $_POST['address_type_code']);
	// $person_contact_method_email_id = addslashes(cleanEncoding($_POST['person_contact_method_email_id']));		
	// $person_contact_method_phone_id = addslashes(cleanEncoding($_POST['person_contact_method_phone_id']));				
	$address_type_code = addslashes(cleanEncoding($_POST['address_type_code']));	
	$address = cleanEncoding($_POST['address']);	
	$city = addslashes(cleanEncoding($_POST['city']));	
	// $state_code = addslashes(cleanEncoding($_POST['state_code']));
	$state_id = addslashes(cleanEncoding($_POST['state_id']));		
	$postal_code = addslashes(cleanEncoding($_POST['postal_code']));	
	// $country_code = addslashes(cleanEncoding($_POST['country_code']));
	$country_id = addslashes(cleanEncoding($_POST['country_id']));		
	// $location_effective_date = addslashes(cleanEncoding($_POST['location_effective_date']));	
	// $location_end_date = addslashes(cleanEncoding($_POST['location_end_date']));
	// Simplified User Form to remove Effective Dates.
	$location_effective_date = date('m/d/Y');
				
	// Check for a valid date format (MM/DD/YYYY) and then convert it to SQL format (YYYY-MM-DD).	

	if (!valid_date($location_effective_date)) {
		// Message user invalid date format.
		$_SESSION['message_type'] = 'alert-box radius';				
		$_SESSION['message'] = '<p>The <strong>Effective Date </strong> must be formatted as MM/DD/YYYY.</p>';
		header("location: ".$_SESSION['target_uri']);	
		exit();			
	}
	
	if (isset($location_end_date) && !empty($location_end_date)) {
		if (!valid_date($location_end_date)) {
			// Message user invalid date format.
			$_SESSION['message_type'] = 'alert-box radius';				
			$_SESSION['message'] = '<p>The <strong>End Date </strong> must be formatted as MM/DD/YYYY.</p>';
			header("location: ".$_SESSION['target_uri']);	
			exit();			
		}
		
		if ($location_end_date <= $location_effective_date) {
			// Message user End Date must be > Effective Date.
			$_SESSION['message_type'] = 'alert-box radius';				
			$_SESSION['message'] = '<p>The <strong>End Date </strong> must be later than the <b>Effective Date</b>.</p>';
			header("location: ".$_SESSION['target_uri']);	
			exit();				
		}
					
	}
	
	$lowercaseAddr = $address;
	$ucTitleStringAddr = ucwords($lowercaseAddr);
	// echo "New Address - $ucTitleStringAddr";
		
	$lowercaseCity = $city;
	$ucTitleStringCity = ucwords($lowercaseCity);
	// echo "New City - $ucTitleStringCity";
	
	// Set the Timestamp for the insert record audit trail.
	date_default_timezone_set("America/Detroit");
	$created_timestamp = date('Y/m/d h:i:s');
	// echo 'Current Date and Time is : '.$created_timestamp.'<br /><hr />';	
	
	// Create a new Address for the Person ($_SESSION['user']['user_id'])		
	$queryInsertAddress = 'INSERT INTO addresses (address, city, state_id, postal_code, country_id, created_by, created_timestamp) VALUES (
	"'.$ucTitleStringAddr.'",
	"'.$ucTitleStringCity.'",
	"'.$state_id.'",
	"'.$postal_code.'", 
	"'.$country_id.'", 
	"'.$_SESSION['user']['user_id'].'",
	"'.$created_timestamp.'")';

	$resultInsertAddress = mysqli_query($connection, $queryInsertAddress);
	
	// echo $queryInsertAddress.'<br /><hr />';   
					
	if (!$resultInsertAddress) {
		show_mysqli_error_message($queryInsertAddress, $connection);
		die;
	}	
	
	// Create a new Location for the Person and Address ($_SESSION['user']['user_id'], $address_id)
	$address_id = mysqli_insert_id($connection);
		
	$queryInsertLocation = '
	INSERT INTO locations (address_type_code, address_id, person_id, /* person_contact_method_email_id, person_contact_method_phone_id, */ effective_date, end_date, created_by, created_timestamp) 
	VALUES (
	"'.$address_type_code.'",
	"'.$address_id.'",
	"'.$_SESSION['user']['user_id'].'",
	/* '.no_value_null_check($person_contact_method_email_id).',
	'.no_value_null_check($person_contact_method_phone_id).', */ 	
	'.no_value_null_check(sql_date($location_effective_date)).', 
	'.no_value_null_check(sql_date($location_end_date)).', 
	"'.$_SESSION['user']['user_id'].'",		
	"'.$created_timestamp.'")';
	
	$resultInsertLocation = mysqli_query($connection, $queryInsertLocation);
	
	// echo $queryInsertLocation.'<br /><hr />';   
					
	if (!$resultInsertAddress) {
		show_mysqli_error_message($queryInsertLocation, $connection);
		die;
	}
	
	mysqli_free_result($resultInsertLocation);
	
	$location_id = mysqli_insert_id($connection);
	
	/*
	// Create the Email Contact for the Location	
	$queryInsertContactEmail = '
	INSERT INTO contacts (person_id, contact_type_code, contact_value, location_id, name, effective_date, end_date, created_by, created_timestamp) 
	VALUES (
	"'.$_SESSION['user']['user_id'].'", 
	"EMAIL", 
	'.no_value_null_check($location_contact_email).', 
	"'.$location_id.'",	
	"Location Email Contact", 
	'.no_value_null_check(sql_date($location_effective_date)).', 
	'.no_value_null_check(sql_date($location_effective_date)).', 
	"'.$_SESSION['user']['user_id'].'",		
	"'.$created_timestamp.'")';
	
	// echo $queryInsertContactEmail.'<br /><hr />';   
			
	$resultInsertContactEmail = mysqli_query($connection, $queryInsertContactEmail);
						
	if (!$resultInsertContactEmail) {
		show_mysqli_error_message($queryInsertContactEmail, $connection);
		die;
	}
	
	mysqli_free_result($resultInsertContactEmail);	
	
	// Create the Phone Contact for the Location
	$queryInsertContactPhone = '
	INSERT INTO contacts (person_id, contact_type_code, contact_value, location_id, name, effective_date, end_date, created_by, created_timestamp) 
	VALUES (
	"'.$_SESSION['user']['user_id'].'", 
	"PHONE", 
	'.no_value_null_check($location_contact_phone).', 
	"'.$location_id.'",	
	"Location Phone Contact", 
	'.no_value_null_check(sql_date($location_effective_date)).', 
	'.no_value_null_check(sql_date($location_effective_date)).', 
	"'.$_SESSION['user']['user_id'].'",		
	"'.$created_timestamp.'")';
	
	// echo $queryInsertContact1.'<br /><hr />';   
			
	$resultInsertContactPhone = mysqli_query($connection, $queryInsertContactPhone);
						
	if (!$resultInsertContactPhone) {
		show_mysqli_error_message($queryInsertContactPhone, $connection);
		die;
	}
	
	mysqli_free_result($resultInsertContactPhone);		
	
	*/
				
	// RESULT MESSAGE
	$_SESSION['message_type'] = 'alert-box success radius';				
	$_SESSION['message'] = '<p>Your <strong>Address</strong> has been created.</p>';
	// $_SESSION['message'] = '<p>Your <strong>Address</strong> has been created with Address ID: '.$address_id.' and your <strong>Location</strong> has been created with Location ID: '.$location_id.'</p>';
	
} elseif (isset($_POST['action']) && $_POST['action'] == 'edit') {

	// User is updating an existing Address Location
	
	// set POST return values to local variables
			
	$address_id = safe_sql_data($connection, $_POST['address_id']); // unique to update
	$location_id = safe_sql_data($connection, $_POST['location_id']); // unique to update
	// $person_contact_method_email_id = addslashes(cleanEncoding($_POST['person_contact_method_email_id']));		
	// $person_contact_method_phone_id = addslashes(cleanEncoding($_POST['person_contact_method_phone_id']));				
	$address_type_code = safe_sql_data($connection, $_POST['address_type_code']);
	$address = safe_sql_data($connection, $_POST['address']);
	$city = safe_sql_data($connection, $_POST['city']);
	// $state = safe_sql_data($connection, $_POST['state']);
	$state_id = addslashes(cleanEncoding($_POST['state_id']));		
	$country_id = addslashes(cleanEncoding($_POST['country_id']));		
	$postal_code = safe_sql_data($connection, $_POST['postal_code']);
	// $location_effective_date = safe_sql_data($connection, $_POST['location_effective_date']);
	// $location_end_date = safe_sql_data($connection, $_POST['location_end_date']);
	
	// echo 'Post Effective Date: '.$_POST['location_effective_date'].'<br />';
	// echo '$location_effective_date: '.$location_effective_date.'<br />';
	// echo 'sql_date($location_effective_date): '.sql_date($location_effective_date).'<br />';	
			
	// Check for a valid date format (MM/DD/YYYY) and then convert it to SQL format (YYYY-MM-DD).	
	/*
	if (!valid_date($location_effective_date)) {
		// Message user invalid date format.
		$_SESSION['message_type'] = 'alert-box radius';				
		$_SESSION['message'] = '<p>The <strong>Effective Date </strong> must be formatted as MM/DD/YYYY.</p>';
		header("location: ".$_SESSION['target_uri']);	
		exit();			
	}
	
	if (isset($location_end_date) && !empty($location_end_date)) {
		if (!valid_date($location_end_date)) {
			// Message user invalid date format.
			$_SESSION['message_type'] = 'alert-box radius';				
			$_SESSION['message'] = '<p>The <strong>End Date </strong> must be formatted as MM/DD/YYYY.</p>';
			header("location: ".$_SESSION['target_uri']);	
			exit();			
		}
		
		if ($location_end_date <= $location_effective_date) {
			// Message user End Date must be > Effective Date.
			$_SESSION['message_type'] = 'alert-box radius';				
			$_SESSION['message'] = '<p>The <strong>End Date </strong> must be later than the <b>Effective Date</b>.</p>';
			header("location: ".$_SESSION['target_uri']);	
			exit();				
		}			
	}
	*/
	
	$lowercaseAddr = $address;
	$ucTitleStringAddr = ucwords($lowercaseAddr);
	// echo "New Address - $ucTitleStringAddr";
		
	$lowercaseCity = $city;
	$ucTitleStringCity = ucwords($lowercaseCity);
	// echo "New City - $ucTitleStringCity";

	
	// Set the Timestamp for the update record audit trail.
	$modified_timestamp = date('Y/m/d H:i:s');
		
	// Update the Address
	$queryUpdateAddress = 'UPDATE addresses SET 
	address = "'.$ucTitleStringAddr.'", 
	city = "'.$ucTitleStringCity.'", 
	state_id = "'.$state_id.'", 
	postal_code = "'.$postal_code.'", 
	country_id = "'.$country_id.'", 
	modified_by = "'.$_SESSION['user']['user_id'].'", 
	modified_timestamp = "'.$modified_timestamp.'" 
	WHERE id = "'.$address_id.'"';
	
	// echo $queryUpdateAddress.'<br /><hr />'; 
		
	$resultUpdateAddress = mysqli_query($connection, $queryUpdateAddress);
						
	if (!$resultUpdateAddress) {
		show_mysqli_error_message($queryUpdateAddress, $connection);
		die;
	}
		
	mysqli_free_result($resultUpdateAddress);  
		
	// Update the Location
	$queryUpdateLocation = 'UPDATE locations SET 
	address_type_code = "'.$address_type_code.'", 
	/* person_contact_method_email_id = '.no_value_null_check($person_contact_method_email_id).', 
	person_contact_method_phone_id = '.no_value_null_check($person_contact_method_phone_id).', */ 
	/* effective_date = '.no_value_null_check(sql_date($location_effective_date)).', 
	end_date = '.no_value_null_check(sql_date($location_end_date)).', */
	modified_by = "'.$_SESSION['user']['user_id'].'", 
	modified_timestamp = "'.$modified_timestamp.'" 
	WHERE id = "'.$location_id.'"';
	
	// echo $queryUpdateLocation.'<br /><hr />';
	
	$resultUpdateLocation = mysqli_query($connection, $queryUpdateLocation);
						
	if (!$resultUpdateLocation) {
		show_mysqli_error_message($queryUpdateLocation, $connection);
		die;
	}
		
	mysqli_free_result($resultUpdateLocation);  
	
	// RESULT MESSAGE
	$_SESSION['message_type'] = 'alert-box success radius';				
	$_SESSION['message'] = '<p>Your <strong>Address </strong> has been updated.</p>';
	
} elseif(isset($_POST['action']) && $_POST['action'] == 'delete') {

	$address_id = safe_sql_data($connection, $_POST['address_id']); 
	$location_id = safe_sql_data($connection, $_POST['location_id']); 
	
	$queryDeleteLocation = ' DELETE FROM locations WHERE id = "'.$location_id.'" ';
	$resultDeleteLocation = mysqli_query($connection, $queryDeleteLocation);
						
	if (!$resultDeleteLocation) {
		show_mysqli_error_message($queryDeleteLocation, $connection);
		die;
	}
	
	mysqli_free_result($resultDeleteLocation);
	
	$queryDeleteAddress = ' DELETE FROM addresses WHERE id = "'.$address_id.'" ';
	$resultDeleteAddress = mysqli_query($connection, $queryDeleteAddress);
						
	if (!$resultDeleteAddress) {
		show_mysqli_error_message($queryDeleteAddress, $connection);
		die;
	}
	mysqli_free_result($resultDeleteAddress);
	
	// RESULT MESSAGE
	$_SESSION['message_type'] = 'alert-box success radius';				
	$_SESSION['message'] = '<p>Your <strong>Address </strong> has been deleted.</p>';
}

// Return to calling form using target uri	
header("location: ".$_SESSION['target_uri']);
exit();	

?>