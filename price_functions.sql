CREATE DEFINER=`root`@`localhost` FUNCTION `getPrice`(`p_usageid` INT, `p_qty` INT, `p_bpuo_id` INT, `p_project` INT, `p_price_type` VARCHAR(30)) RETURNS decimal(11,2)
begin
   declare p_price decimal(11,2);
   declare p_prev_id int;
  
  
  searching : LOOP
     set p_price = (select getPrice2(p_usageid,p_qty,p_bpuo_id,p_project,p_price_type) as p);
     
     if p_price is null then
        set p_prev_id = p_bpuo_id;
        -- getting the new parent
		set p_bpuo_id = (select opt.brand_product_usage_option_parent_id 
							FROM brand_product_usage_options opt
							WHERE opt.id = p_prev_id);
						
        if p_bpuo_id is null then 
			leave searching;
        end if;
     else -- price found
		leave searching;
     end if;
  
  END LOOP searching;
 
  return p_price;

end

CREATE DEFINER=`root`@`localhost` FUNCTION `getPrice2`(`p_usageid` INT, `p_qty` INT, `p_bpuo_id` INT, `p_project` INT, `p_price_type` VARCHAR(30)) RETURNS decimal(11,2)
begin
   declare p_price decimal(11,2);
  
  set p_price = (
   SELECT price -- price_type_code, price,quantity_low_range ,quantity_high_range 
		FROM  project_brand_product_usage_prices
		WHERE 
        bpuo_id = p_bpuo_id
        AND  brand_product_usage_id = p_usageid
		AND pbpu_project_id = p_project
		AND price_type_code = p_price_type
        
		AND
		((quantity_low_range <= p_qty AND (quantity_high_range >= p_qty OR quantity_high_range IS NULL))) 
        
		limit 1
    ) ;

   -- getting the default price
   if p_price is null then
	  set p_price = (
	   SELECT price 
			FROM  project_brand_product_usage_prices
			WHERE 
			bpuo_id = p_bpuo_id
			AND  brand_product_usage_id = p_usageid
			AND pbpu_project_id = p_project
			AND price_type_code = p_price_type
			
			AND
			  (quantity_low_range IS NULL AND quantity_high_range IS NULL) 
			 
		limit 1
		) ;
	
   end if;
   
  
   return p_price;
end