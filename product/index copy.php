<?php
     
// echo 'Begin Product page.<br />';
// die;

include_once('../includes/header.php');

getPage('Product', $connection);

if ($_SESSION['page']['use_authorization']) { include_once('../includes/authorization.php'); }

include_once('../includes/page_header.php');

if ($_SESSION['page']['use_menu']) { include_once('../includes/menu.php'); }

	if ($_SESSION['page']['ecommerce']) {
		require_once('../'.STORE_FOLDER_NAME.'/includes/store_header.php');
		include_once('../php/e-commerce-header-product.php');
	}

	?>
	
</div> <!-- closes the "fixed" div containing the Nav and Search -->

<div class="productTitle show-for-small-only">
	<?php 
	if (isset($page_meta_title)) { 
		echo '<center><b>'.$page_meta_title.'</b></center>';  
	} 
	?>
</div>

<?php
	
if ($_SESSION['page']['use_title_bar']) { 
	include_once('../includes/off_page_canvas_1.php');
	echo '<br>'; 
}	
// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

include('../php/show_project_brand_product.php'); 

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->
if ($_SESSION['page']['use_title_bar']) { include_once('../includes/off_page_canvas_2.php'); }

// Begin rest of body content for page.

if ($_SESSION['page']['use_footer']) { include_once('../includes/page_footer.php'); }

if ($_SESSION['page']['use_disclaimer']) { include_once('../includes/disclaimer.php'); }

// show_session();

include_once('../includes/foundation_footer.php');

if ($_SESSION['page']['use_system_log']) { include('../php/system_log_page_access.php'); }

if ($_SESSION['page']['ecommerce']) { require_once('../'.STORE_FOLDER_NAME.'/'.STORE_FOOTER); }

?>
<script type="text/javascript" src="../js/ajax_product_option.js"></script>
<script type="text/javascript" src="../js/predictive_search.js"></script>