<?php

include_once('../includes/header.php');


include_once('../'.STORE_FOLDER_NAME.'/includes/functions.php'); // Can these resources be moved into the Project /includes/foundation_footer.php? 

$parent_id = isset($_POST['parent_id'])?$_POST['parent_id']:0;
$price_type = isset($_POST['price_type'])?$_POST['price_type']:'';
$parent_price = isset($_POST['parent_price'])?$_POST['parent_price']:null;
$quantity = isset($_POST['quantity'])?$_POST['quantity']:1;
$level = isset($_POST['level'])?$_POST['level']:0;
$selections = isset($_POST['selections'])?$_POST['selections']:'0|0|0|0|0|0|0|0|0|0|0|0|0|0';

//if($level == -1 ) exit;

echo generate_item_option_html($connection, array(
			'pid' => $_POST['pid'],
			'p_usage_id' =>  $_POST['p_usage_id'] ,
			'selections' => $selections,
			'price_type' => $price_type,
			'quantity' => $quantity,
			'parent_price' => $parent_price
		)
,$level == 0 ? null : $parent_id 
,$level, false);	


