<?php 

// echo 'Begin Promotions page.<br />';

include_once('../includes/header.php');

getPage('Promotions', $connection);

// $_SESSION['page']['page_id'] = 71; // SOZO

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

echo '<br>';
	
include('../php/store_promotion_slider.php'); 

include('../php/system_log_page_access.php'); 

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php'); 

?>

<script type="text/javascript" src="../js/slick.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
          $(".promotion-slider").show();
	  $('.promotion-slider').slick({
		 autoplay: true,
		 autoplaySpeed: 7000,	
		 dots: true, 
		 slidesToShow: 1,
		 slidesToScroll: 1
	  });
	  
	});
</script>

