<?php 

// echo 'Begin Register Member Account Confirmation page.<br />';

include_once('../includes/header.php');

getPage('Register Member Account Confirmation', $connection);

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

/*
if (strlen($_SESSION['message']) > 0) { 
	echo $_SESSION['message'];
	$_SESSION['message_type'] = null;
	$_SESSION['message'] = null;
}		   
*/

?>

<br>

<div class="row">		
	<div class="small-12 columns">
			
		<div class="panel">
	  	<h3><i class="fa fa-thumbs-up fa-lg" style="color: #5CB64B;"></i>&nbsp;&nbsp;Thank You for registering for your Member Account!</h3>
	  	<p><b>We just emailed you at <span style="color:red;"><b><?php echo $_SESSION['member_registration_email']; ?></b></span> to make sure we have the right email address</b>. Please leave this browser window open until you see your email and respond. After you <b>click the verification link in that email</b>, you will be ready to login and take advantage of your member benefits!</p>
		</div>
		
		<div class="panel">
				
			<h1 style="color:red;"><i class="fa fa-exclamation-triangle"></i>&nbsp;&nbsp;If You DO NOT SEE YOUR EMAIL... Check Your JUNK EMAIL FOLDER</h1>
			
			<ol>
	
				<li>Please make sure <b>you entered your Email Address correctly</b>. You entered: <span style="color:red;"><b><?php echo $_SESSION['member_registration_email']; ?></b></span>.</li>
		
				<li>If it is correct, the email we sent is probably <b>in your JUNK or SPAM email folder</b>. Please find it and respond to it. It has the subject <b>Activate Your SupplementRelief Account</b>.</li>
				
				<li>Please <b>update your SPAM settings to allow email from SupplementRelief.com to go into your email inbox</b>. This is usually accomplished by <b>adding support@supplementrelief.com as a Contact in your Contact Address Book</b>.</li>
				
				<li>Remember <b>you still need to respond to the email or your account WILL NOT BE ACTIVATED</b>.</li>
				
				<li>If for any reason you still need some help,</b> please call <b>(888) 424-0032</b> or <a href="../contact/">email</a> and we will help you complete your account registration!</li>
				
			</ol>
			
		</div>
							
	</div>
</div>

<?php unset($_SESSION['member_registration_email']); ?>

<!-- Google Code for Whiteshark Register Member Account Confirmation Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 879537895;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "WD08CJuS5mcQ592yowM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/879537895/?label=WD08CJuS5mcQ592yowM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<?php

// include('../php/contact_confirmation.php');

include('../php/system_log_page_access.php'); 

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php'); 

?>