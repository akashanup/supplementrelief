<?php 

// echo 'Begin Show Customer Order page.<br />';

include_once('../includes/header.php');

getPage('Show Customer Order', $connection);

// show_array($_SESSION);

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

if ($_SESSION['page']['ecommerce']) {
	include_once('../'.STORE_FOLDER_NAME.'/includes/store_header.php');
}

include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 
	
// if (isset($_SESSION['enrollment']['project_program_id'])) { $pp_id = $_SESSION['enrollment']['project_program_id']; }

include('../php/show_customer_order.php'); 

include('../php/system_log_page_access.php'); 

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php'); 

if ($_SESSION['page']['ecommerce']) { include_once('../'.STORE_FOLDER_NAME.'/includes/footer.php'); }

?>

