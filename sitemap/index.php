<?php 

// echo 'Sitemap page.<br />';

include_once('../includes/header.php');

getPage('Sitemap', $connection);

// show_array($_SESSION);

// include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

?>

<br>
<div class="row">		
	<div class="small-12 columns">
		<dl class="sub-nav">
            <dt><b>Locator:</b></dt>
            <dd class="active"><a href="#">All</a></dd>
            <dd><a href="#mainPages">Main</a></dd>
            <dd><a href="#numedicaProducts">Products</a></dd>
            <dd><a href="#blogPosts">Blog Posts</a></dd>
            <dd><a href="#wellnessResources">Resources</a></dd>
            <dd><a href="#healthyRecipes">Recipes</a></dd>
        </dl>			
	</div>
</div>

<div id="locator"></div>

<div id="mainPages"></div>

<?

$application_id = public_application_id;

// Query the Core Pages for the Application
$queryCorePages = 'SELECT 
    pg.name AS page_name, 
    pg.relative_url, 
	pg.meta_title, 
	pg.meta_description 
	FROM pages pg
	WHERE pg.application_id = '.$application_id.' 
	AND pg.core = 1 
	ORDER BY pg.name ASC';

// echo $queryCorePages;
// die();

$result_page = mysqli_query($connection, $queryCorePages);

if (!$result_page) {
	show_mysqli_error_message($queryCorePages, $connection);
	die;
}

$total_pages = mysqli_num_rows($result_page);
// echo 'Total # Products: '.$total_products.'<br />';

?>

<div class="row panel">		
	<div class="small-12 columns">
        <h2 class="sitemapHeaderH2">Main Pages <small><?php echo $total_pages; ?></small></h2>
	</div>
</div>

<br>

<?php

while($r = mysqli_fetch_assoc($result_page)) { 
    
    // show_array($r);
    
    if ($_SESSION['user']['user_id'] == 2 ) {
    
        $meta_description_length = strlen($r['meta_description']);
        if ($meta_description_length > 155) {
            $metaLengthTooLong = 'metaLengthTooLong';          
        } else {
            $metaLengthTooLong = '';
        }
        
    }
      
    $relative_url = $r['relative_url'];
        
    ?>
    <div class="row">		
    	<div class="small-12 columns">
    		<h3 class="sitemapHeaderH3"><a href="<?php echo '../'.$relative_url; ?>" title="View Page: <?php echo $r['page_name']; ?>"><?php echo $r['meta_title']; ?></a></h3>
    		<p id="<?php echo $metaLengthTooLong;?>"><?php echo strip_tags($r['meta_description']); ?></p>					
    	</div>
    </div>
    <?php  
       
}

mysqli_free_result($result_page);

?>

<?php
    
$project_id = public_project_id;
    
// Query the Project Brand Product
$query = 'SELECT 
    bpu.id AS brand_product_usage_id, 
	pbpu.page_meta_title, 
	pbpu.page_meta_description, 
	bpu.name AS brand_product_name, 
	urls.url AS seo_url 
	FROM project_brand_product_usages pbpu 
	LEFT JOIN brand_product_usages bpu ON pbpu.brand_product_usage_id = bpu.id 
	LEFT JOIN url_usages urlu 
        ON (pbpu.id = urlu.pbpu_id AND 
            urlu.effective_date <= CURRENT_DATE AND
           (urlu.end_date IS NULL or urlu.end_date >= CURRENT_DATE))
    LEFT JOIN urls urls ON urlu.url_id = urls.id 			 
	WHERE pbpu.project_id = '.$project_id.' 
	AND pbpu.active = 1 
	ORDER BY bpu.name ASC';

// echo $query;
// die();

$result_products = mysqli_query($connection, $query);

if (!$result_products) {
	show_mysqli_error_message($query, $connection);
	die;
}

$total_products = mysqli_num_rows($result_products);
// echo 'Total # Products: '.$total_products.'<br />';

?>

<div id="numedicaProducts">&nbsp;</div>

<div class="row panel radius">		
	<div class="small-12 columns">
        <h2 class="sitemapHeaderH2">Products <small><?php echo $total_products; ?>&nbsp;&nbsp;<a href="#top">[&nbsp;<i class="fa fa-arrow-up"></i>&nbsp;return to top&nbsp;]</a></small></h2>		
	</div>
</div>

<br>

<?php

while($r = mysqli_fetch_assoc($result_products)) { 
    
    // show_array($r);
    
        if ($r['seo_url']) {
    		$product_url = '../'.$r['seo_url'].'/';
		} else {	
    		$product_url = '../product/?p_id='.$r['project_id'].'&bpu_id='.$r['brand_product_usage_id'];
		}			

    if ($_SESSION['user']['user_id'] == 2 ) {
    
        $meta_description_length = strlen($r['page_meta_description']);
        if ($meta_description_length > 155) {
            $metaLengthTooLong = 'metaLengthTooLong';          
        } else {
            $metaLengthTooLong = '';
        }
        
    }
   
    $bpu_id = $r['brand_product_usage_id'];
        
    ?>
    <div class="row">		
    	<div class="small-12 columns">
    		<h3 class="sitemapHeaderH3"><a href="<?php echo $product_url; ?>" title="View Product: <?php echo $r['brand_product_name']; ?>"><?php echo $r['page_meta_title']; ?></a></h3>
    		<p id="<?php echo $metaLengthTooLong;?>"><?php echo strip_tags($r['page_meta_description']); ?></p>					
    	</div>
    </div>
    <?php  
       
}

mysqli_free_result($result_products);

?>

<div id="blogPosts"></div>

<?php

	date_default_timezone_set('America/Detroit');
	$current_timestamp = date('Y-m-d H:i:s');
	$blog_id = public_blog_id; // 1299
	
	$queryBlogPosts = 'SELECT 
	    ca.id AS blog_post_id, 
		bp.page_meta_title, 
		bp.page_meta_description, 
		ca.title AS blog_post_title, 
        urls.url AS seo_url 
		FROM content_asset_usages cau  
		LEFT JOIN content_assets ca on cau.content_asset_child_id = ca.id 
		LEFT JOIN blog_posts bp ON ca.id = bp.content_asset_id 
        LEFT JOIN url_usages urlu 
            ON (ca.id = urlu.blog_post_id AND 
                urlu.effective_date <= CURRENT_DATE AND
               (urlu.end_date IS NULL or urlu.end_date >= CURRENT_DATE))
        LEFT JOIN urls urls ON urlu.url_id = urls.id 			 
		WHERE cau.content_asset_parent_id = "'.$blog_id.'"  
		AND bp.post_effective_timestamp <= "'.$current_timestamp.'" 
		AND (bp.post_end_timestamp IS NULL or bp.post_end_timestamp > "'.$current_timestamp.'")  	
		ORDER BY bp.post_effective_timestamp DESC';
									
	// echo '<br />'.$queryBlogPosts.'<br /><br />';
	// die();
	
	$result_blog_post = mysqli_query($connection, $queryBlogPosts);
	
	if (!$result_blog_post) {
		show_mysqli_error_message($queryBlogPosts, $connection);
		die;
	}
	
	$total_blog_posts = mysqli_num_rows($result_blog_post);
	// echo 'Total # Blog Posts: '.$total_number_blog_posts.'<br />';

?>

<div class="row panel radius">		
	<div class="small-12 columns">
        <h2 class="sitemapHeaderH2">Blog Posts <small><?php echo $total_blog_posts; ?>&nbsp;&nbsp;<a href="#top">[&nbsp;<i class="fa fa-arrow-up"></i>&nbsp;return to top&nbsp;]</a></small></h2>		
	</div>
</div>

<br>

<?php

while($r = mysqli_fetch_assoc($result_blog_post)) { 
               
    // show_array($r);
    
    $blog_post_id = $r['blog_post_id'];

    if ($r['seo_url']) {
		$blog_post_url = '../'.$r['seo_url'].'/';
	} else {	
		$blog_post_url = '../show_blog_post/?bp_id='.$blog_post_id;
	}			
    
    if ($_SESSION['user']['user_id'] == 2 ) {
    
        $meta_description_length = strlen($r['page_meta_description']);
        if ($meta_description_length > 155) {
            $metaLengthTooLong = 'metaLengthTooLong';          
        } else {
            $metaLengthTooLong = '';
        }
        
    }
  
    ?>
    <div class="row">		
    	<div class="small-12 columns">
    		<h3 class="sitemapHeaderH3"><a href="<?php echo $blog_post_url; ?>" title="View Blog Post: <?php echo $r['blog_post_title']; ?>"><?php echo $r['page_meta_title']; ?></a></h3>
    		<p id="<?php echo $metaLengthTooLong;?>"><?php echo strip_tags($r['page_meta_description']); ?></p>					
    	</div>
    </div>
    <?php  
       
}

mysqli_free_result($result_blog_post);

?>

<div id="healthyRecipes">&nbsp;</div>

<?php
 
$ppca_id = 308; // SupplementRelief/Whole Foods Cookbook   
$cookbook_id = public_cookbook_id;

$queryRecipes = 'SELECT 
rec.page_meta_title, 
rec.page_meta_description, 
ca.id AS recipe_id,  
ca.title AS recipe_name, 
urls.url AS seo_url 
FROM content_asset_usages cau 
LEFT JOIN content_assets ca ON cau.content_asset_child_id = ca.id 
LEFT JOIN recipes rec ON cau.content_asset_child_id = rec.content_asset_id 
LEFT JOIN url_usages urlu 
ON (ca.id = urlu.recipe_id AND 
    urlu.effective_date <= CURRENT_DATE AND
   (urlu.end_date IS NULL or urlu.end_date >= CURRENT_DATE))
LEFT JOIN urls urls ON urlu.url_id = urls.id 			
WHERE cau.content_asset_parent_id = '.$cookbook_id.' 
AND cau.effective_date <= CURRENT_DATE  
AND (cau.end_date is NULL or cau.end_date >= CURRENT_DATE)  	
ORDER BY ca.title';

// echo '<br />'.$queryRecipes.'<br /><br />';
// die();

$result_recipe = mysqli_query($connection, $queryRecipes);

if (!$result_recipe) {
	show_mysqli_error_message($queryRecipes, $connection);
	die;
}

$total_recipes = mysqli_num_rows($result_recipe);

?>

<div class="row panel radius">		
	<div class="small-12 columns">
        <h2 class="sitemapHeaderH2">Recipes <small><?php echo $total_recipes; ?>&nbsp;&nbsp;<a href="#top">[&nbsp;<i class="fa fa-arrow-up"></i>&nbsp;return to top&nbsp;]</a></small></h2>		

	</div>
</div>

<br>

<?php

while($r = mysqli_fetch_assoc($result_recipe)) { 
               
    // show_array($r);
    
    $recipe_id = $r['recipe_id'];
    
    if ($r['seo_url']) {
		$recipe_url = '../'.$r['seo_url'].'/';
	} else {	
		$recipe_url = '../recipe/?re_id='.$recipe_id;
    }			
    
    if ($_SESSION['user']['user_id'] == 2 ) {
    
        $meta_description_length = strlen($r['page_meta_description']);
        if ($meta_description_length > 155) {
            $metaLengthTooLong = 'metaLengthTooLong';          
        } else {
            $metaLengthTooLong = '';
        }
        
    }
  
    ?>
    <div class="row">		
    	<div class="small-12 columns">
    		<h3 class="sitemapHeaderH3"><a href="<?php echo $recipe_url; ?>" title="View Recipe: <?php echo $r['recipe_name']; ?>"><?php echo $r['page_meta_title']; ?></a></h3>
    		<p id="<?php echo $metaLengthTooLong;?>"><?php echo strip_tags($r['page_meta_description']); ?></p>					
    	</div>
    </div>
    <?php  
       
}

mysqli_free_result($result_recipe);

?>

<div id="wellnessResources">&nbsp;</div>

<?php
    
$project_program_id = public_project_program_id;

$queryResources = 'SELECT 
    ppca.id AS ppca_id, 
	rk.page_meta_title, 
	rk.page_meta_description, 
	ca.id AS resource_id,  
	ca.title AS resource_name,
	urls.url AS seo_url   
	FROM project_program_content_assets ppca 
	LEFT JOIN content_assets ca on ppca.content_asset_id = ca.id 
	LEFT JOIN resource_kits rk on ppca.content_asset_id = rk.content_asset_id 
	LEFT JOIN url_usages urlu 
    ON (ca.id = urlu.resource_id AND 
        urlu.effective_date <= CURRENT_DATE AND
       (urlu.end_date IS NULL or urlu.end_date >= CURRENT_DATE))
    LEFT JOIN urls urls ON urlu.url_id = urls.id 			
	WHERE ppca.project_program_id = '.$project_program_id.' 
	AND ppca.scheduled_delivery_timestamp <= "'.$current_timestamp.'" 
	AND (scheduled_delivery_complete_timestamp IS NULL or scheduled_delivery_complete_timestamp >= "'.$current_timestamp.'") 
	AND ca.content_asset_type_code = "RESO" 
	ORDER BY ppca.scheduled_delivery_timestamp DESC';
			
// echo $queryRecipes;
// die();

$result_resource = mysqli_query($connection, $queryResources);

if (!$result_resource) {
	show_mysqli_error_message($queryResources, $connection);
	die;
}

$total_resources = mysqli_num_rows($result_resource);
// echo 'Total # Resources: '.$result_resource.'<br />';

?>

<div class="row panel radius">		
	<div class="small-12 columns">
        <h2 class="sitemapHeaderH2">Resources <small><?php echo $total_resources; ?>&nbsp;&nbsp;<a href="#top">[&nbsp;<i class="fa fa-arrow-up"></i>&nbsp;return to top&nbsp;]</a></small></h2>		
	</div>
</div>

<br>

<?php

while($r = mysqli_fetch_assoc($result_resource)) { 
               
    // show_array($r);
    
    if ($r['seo_url']) {
        $resource_url = '../'.$r['seo_url'].'/';
	} else {	
		$resource_url = '../resource/?ppca_id='.$ppca_id.'&ca_id='.$resource_id;
	}			
 
    if ($_SESSION['user']['user_id'] == 2 ) {
    
        $meta_description_length = strlen($r['page_meta_description']);
        if ($meta_description_length > 155) {
            $metaLengthTooLong = 'metaLengthTooLong';          
        } else {
            $metaLengthTooLong = '';
        }
        
    }
  
    $ppca_id = $r['ppca_id'];
    $resource_id = $r['resource_id'];

    ?>
    <div class="row">		
    	<div class="small-12 columns">
    		<h3 class="sitemapHeaderH3"><a href="<?php echo $resource_url; ?>" title="View Resource: <?php echo $r['resource_name']; ?>"><?php echo $r['page_meta_title']; ?></a></h3>
    		<p id="<?php echo $metaLengthTooLong;?>"><?php echo strip_tags($r['page_meta_description']); ?></p>					
    	</div>
    </div>
    <?php  
       
}

mysqli_free_result($result_resource);

include('../php/system_log_page_access.php'); 

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php'); 

?>