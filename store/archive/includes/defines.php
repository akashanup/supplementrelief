<?php
	
// echo 'Begin /store/includes/defines.php<br>';

// show_array($_SERVER);

/* 
Contains paths to external libraries, css sheets, plug-ins needed for e-Commerce.
By default the Store module needs the following css and js files.
If you want to define any additional paths ,then type it by adding a new line
EX. define("path name in short form","complete path name")
*/

//include_once('sub_defines.php');


if(isset($_SESSION['BOWER_PATH']) && !empty($_SESSION['BOWER_PATH']))
{
    define("BOWER_PATH", $_SESSION['BOWER_PATH']."includes/foundation/"); // Path To Foundation Theme
}
else
{
     define("BOWER_PATH", "../bower_components/"); // Path To Foundation Theme
}
define("STORE_HEADER", "includes/header.php"); // Store Header
define("STORE_FOOTER", "includes/footer.php"); // Store Footer

define("STORE_JS_DEFINE", "includes/js_defines.php");
define("STORE_FUNCTIONS", "functions.php");
//define("STORE_NEW_FUNCTION", "includes/new_functions.php");

define("STORE_FOUNDATION_CSS", BOWER_PATH."fundation/foundation.css"); // directory mispelled fundation
define("STORE_MODERNIZR_JS", BOWER_PATH."modernizr/modernizr.js");
define("STORE_IE8_JS", "../store/js/ie8.js");
define("STORE_IE8REM_JS", "../store/js/ie8rem.js");
define("STORE_JQUERY_STICKY_JS", "../store/js/jquery_sticky.js");
define("STORE_READMORE_JS", "../store/js/readmore.min.js");

define("STORE_STORE_CSS", "../store/css/store.min.css");
// define("STORE_APP_CSS", "../stylesheets/app.min.css");
define("STORE_APP_CSS", "../stylesheets/app.css");
define("STORE_STORE_JS", "../store/js/store.js?_2015-09-26");
define("STORE_MOBILE_JS", "../store/js/store_mobile.js");
define("STORE_FOOTER_JS", "../store/js/store_footer.js");
define("STORE_CHECKOUT_JS", "js/checkout.js");

//define("STORE_FOUNDATION_JS", BOWER_PATH."foundation/js/foundation.js");
define("STORE_FOUNDATION_MIN_JS", BOWER_PATH."foundation/js/foundation.min.js");
define("STORE_SCROLLUP_JS", "../store/js/jquery.scrollUp.min.js");
define("STORE_BLOCKUI_JS", "../store/js/jquery.blockUI.js");

define("STORE_FOUNDATION_JQUERY_JS","../bower_components/foundation/js/vendor/jquery.js");
//define("STORE_FOUNDATION_JQUERY_JS","../bower_components/jquery/dist/jquery.min.js");
//define("STORE_FOUNDATION_JQUERY_JS","../store/js/jquery.js");


define("FONT_AWESOME_CSS", "//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css");

define("core", "../includes/core.php"); // CORE site file




	switch ($_SERVER['HTTP_HOST']) {
		// Jay
    	case 'supplementrelief:8888':
    		define("base_url", "http://supplementrelief:8888/"); // Base URL
			define("STORE_LOG_ENABLED", true);
			define("SQL_ERROR_ACTION", 'log');
			define("STORE_LOG_FILE", "checkout.log");
			break;

		// Akash
    	case 'local.supplementreliefdev.com':
    		define("base_url", "http://local.supplementreliefdev.com/"); // Base URL
			define("STORE_LOG_ENABLED", true);
			define("SQL_ERROR_ACTION", 'log');
			define("STORE_LOG_FILE", "checkout.log");
			break;

    	// Don
    	case 'supplementalrelief:8888':
    		define("base_url", "http://supplementalrelief:8888/"); // Base URL
			define("STORE_LOG_ENABLED", true);
			define("SQL_ERROR_ACTION", 'log');
			define("STORE_LOG_FILE", "checkout.log");
			break;
    	// Ivan
    	case 'ivancp.dw04.pe':
    		define("base_url", "http://ivancp.dw04.pe/"); // Base URL
			define("STORE_LOG_ENABLED", true);
			define("SQL_ERROR_ACTION", 'log');
			define("STORE_LOG_FILE", "checkout.log");
		    break;

    	// Development
    	case 'supplementrelief.com':
    		// define("base_url", "https://supplementrelief.com/development/"); // Base URL
    		define("base_url", "https://supplementrelief.com/development/"); // Base URL
			define("STORE_LOG_ENABLED", true);
			define("SQL_ERROR_ACTION", 'log');
			define("STORE_LOG_FILE", "checkout.log");
		    break;
    	// Production
    	default:
    		define("base_url", "https://supplementrelief.com/"); // Base URL
			define("STORE_LOG_ENABLED", true);
			define("SQL_ERROR_ACTION", 'log');
			define("STORE_LOG_FILE", "checkout.log");
		    break;
    }

define("root_directory", ""); // Root Directory

// Authorize.net configuration
// require_once './includes/anet_php_sdk/AuthorizeNet.php'; // DO NOT CHANGE

	// Test 
	
	define("AUTHORIZENET_API_LOGIN_ID", "57AkU4ua"); // Jay's RtS Adventures, LLC Sandbox created 11/1/2016
	define("AUTHORIZENET_TRANSACTION_KEY", "5Fmagr368R3MuF43"); // Jay's RtS Adventures, LLC Sandbox
	define("AUTHORIZENET_SANDBOX", true); // make false in PRODUCTION

/* 
	// Production
	define("AUTHORIZENET_API_LOGIN_ID", "2cw8X9B8DWA"); // RtS Adventures, LLC Production
	define("AUTHORIZENET_TRANSACTION_KEY", "53s26RbPRFkH5p3H"); // RtS Adventures, LLC Production
	define("AUTHORIZENET_SANDBOX", false); // make false in PRODUCTION, true for DEVELOPMENT/TESTING
*/
	

include_once('../includes/defines.php');
// if this is referring to /includes/defines.php than this file is already being included for every page on the website regardless of whether it is Store or not so p[lease remove this. 
//DEFINE STORE FOLDER NAME
//define('STORE_FOLDER_NAME', 'store1');
/*
IMPORTANT!!! There is also one store_folder reference in a CSS file that must be updated manually:
store_folder_name/includes/foundation/store.css ON LINE 315:
background-image: url("/sozo/store1/store_img/top.png");
*/

//Define "view order" url or description url prefix
// define('ORDER_URL', 'http://'.$_SERVER['HTTP_HOST'].'/'.root_directory.'sozo/'.STORE_FOLDER_NAME.'/'); // keep as is
// define('ORDER_URL', 'http://'.$_SERVER['HTTP_HOST'].'/'.root_directory.'supplementrelief/'.STORE_FOLDER_NAME.'/'); // keep as is
define('ORDER_URL', 'https://'.$_SERVER['HTTP_HOST'].'/'.root_directory.'development/'.STORE_FOLDER_NAME.'/'); // keep as is

define('page_title', isset($_SESSION['page_meta_title'])?$_SESSION['page_meta_title']:'' ); // e-Commerce Prototype
// Don't think this is needed if the Store and Product pages are using the standard headers and footers.
define('store_path', '../'.STORE_FOLDER_NAME.'/');

define('public_project_id', '1'); // This is already defined at the higher level defines.php
define('authnet_vendor_name', 'SupplementRelief.com');
define('USER_PRICE_TYPE', 'RETL');

define('CHECK_OUT_PROMOTION_CODE', 'yes'); // yes or no
define('CHECK_OUT_FREE_SAMPLES', 'yes'); // yes or no
define('CHECK_OUT_TERMS_CHECK_BOX', 'no'); // yes or no

define('STORE_CHECKOUT_MODAL', 'yes'); // yes or no
define('STORE_CHECKOUT_MODAL_DELAY', '3000'); // milliseconds

/*if(isset($_SESSION['foundation_path'])){
	unset($_SESSION['foundation_path']); // keep as is
	// What for ???
}*/

?>