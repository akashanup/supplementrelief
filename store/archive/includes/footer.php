
<!--<div id="script_content">-->
	<!--<script src="<?php //echo STORE_IE8REM_JS; ?>"></script>-->
	<!-- Do we still need IE8 js? Only supporting IE9 and up -->
	<!--<script src="<?php //echo STORE_FOUNDATION_MIN_JS; ?>"></script>-->
	<!-- Is this needed? foundation.min.js is already pulled in from /includes/foundation_footer.php. Is this some special js that the Store needs? -->
	<!--script src="<?php //echo $_SESSION['file_directory_path_prefix'].'js/app.js'; ?>"></script-->
	<!--<script src="<?php //echo STORE_FOUNDATION_JQUERY_JS; ?>"></script>-->
	<!-- <script src="<?php //echo STORE_MODERNIZR_JS; ?>"></script> -->
	<!--[if IE 9]>
	  <script src="<?php //echo STORE_IE8_JS; ?>"></script>
	<![endif]-->
	<!-- Latest Jquery  -->
	<!-- <script src="<?php //echo STORE_FOUNDATION_JQUERY_JS; ?>"></script> -->
	<!-- Sticky plugin -->
	<script src="<?php echo STORE_JQUERY_STICKY_JS; ?>"></script>
	<!-- Readmore plugin -->
	<script src="<?php echo STORE_READMORE_JS; ?>"></script>
	<!-- Custom java script file for store section (All screen devices)  -->
	<script src="<?php echo STORE_STORE_JS; ?>"></script>
	<!-- Custom java script file for store (small screen ) -->
	<script src="<?php echo STORE_MOBILE_JS; ?>"></script>
	<!-- Scroll to top plugin -->
	<script src="<?php echo STORE_SCROLLUP_JS; ?>"></script>
	<!-- Block UI plugin for creating modal -->
	<script src="<?php echo STORE_BLOCKUI_JS; ?>"></script>
	<!--[if lt IE 9]><meta http-equiv="refresh" content="0; url=../legacy_browser/"><![endif]--> 
<!--</div>-->
