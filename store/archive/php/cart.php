<?php
define("STORE_FOLDER_NAME","store");
include_once('../includes/store_page_header.php');
include_once('../'.core);

if(isset($_POST['command']) && isset($_POST['pid']) && $_POST['command']=='delete' && $_POST['pid'] >= 0)
{
	remove_product($_POST['pid']);
}
else if(isset($_POST['command']) && isset($_POST['pid']) && $_POST['command']=='update' && $_POST['pid'] >= 0)
{
	$pid=$_POST['pid'];
	$q=intval($_POST['product_'.$pid]);
	update_quantity($_POST['pid'],$q);
}

if (isset($_POST['mob_command']) && isset($_POST['mob_pid']) && $_POST['mob_command']=='delete' && $_POST['mob_pid'] >= 0)
{
	remove_product($_POST['mob_pid']);
}
else if(isset($_POST['mob_command']) && isset($_POST['mob_pid'])  && $_POST['mob_command']=='update' && $_POST['mob_pid'] >= 0)
{
	$pid=$_POST['mob_pid'];
	$q=intval($_POST['mob_product_'.$pid]);
	update_quantity($_POST['mob_pid'],$q);
}
if(isset($_POST['medium_command']) && isset($_POST['medium_pid']) && $_POST['medium_command']=='delete' && $_POST['medium_pid'] >= 0)
{
	remove_product($_POST['medium_pid']);
}
else if(isset($_POST['medium_command']) && isset($_POST['medium_pid']) && $_POST['medium_command']=='update' && $_POST['medium_pid'] >= 0)
{
	$pid=$_POST['medium_pid'];
	$q=intval($_POST['medium_product_'.$pid]);
	update_quantity($_POST['medium_pid'],$q);
}	
?>

<div id="shopping_cart_container" >	
	<?php
		if(isset($_POST['read_only']) && !empty($_POST['read_only']))
		{
			$read_only = safe_data($_POST['read_only']);
		}
		else
		{
			$read_only = '';
		}
		
		if(isset($_POST['p_usage_id']) && !empty($_POST['p_usage_id']))
		{
			$p_usage_id = safe_sql_data($connection, $_POST['p_usage_id']);
		}
		else
		{
			$p_usage_id = '';
		}
		
		if(isset($_POST['q']) && !empty($_POST['q']))
		{
			$quantity = safe_sql_data($connection, $_POST['q']);
		}
		else
		{
			$quantity = '';
		}
		
		$post_select = 'prd_'.$p_usage_id;
		if(isset($_POST[$post_select]) && !empty($_POST[$post_select]))
		{
			$select_arr = $_POST[$post_select];
		}
		else
		{
			$select_arr = null;
		}

		$p_last_selected = isset($_POST['p_last_selected'])?$_POST['p_last_selected']:null;
		if(isset($_POST['price_type']) && !empty($_POST['price_type']))
		{
			$_SESSION['price_type'] = safe_sql_data($connection, $_POST['price_type']);
		}
		add_to_cart($connection, array(
					'p_usage_id' => $p_usage_id,
					'quantity' => $quantity,
					'select_options' => $select_arr,
					'read_only' => $read_only,
					'p_last_selected' => $p_last_selected
		));
		unset($_SESSION['price_type']);
		echo '</div><a class="close-reveal-modal">&#215;</a>';
?>
