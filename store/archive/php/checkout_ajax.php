<?php
	
// What event triggers this script to execute? Place Order button on Checkout Form?

// error_reporting( E_ALL );
// ini_set('display_errors', 1);
define("STORE_FOLDER_NAME","store");
include_once('../includes/store_page_header.php');
include_once('../'.core);
// Include this file for integrating authorize.net(new version)
require_once('../includes/sdk-php-master/autoload.php'); 

$logfile = dirname(__FILE__).STORE_LOG_FILE;

if(STORE_LOG_ENABLED){
	file_put_contents( $logfile , "\n\nCHECKOUT LOG STARTED - ".date('Y-m-d H:i:s'),FILE_APPEND);
	file_put_contents( $logfile , "\n_POST[]:\n",FILE_APPEND);
	file_put_contents( $logfile , print_r($_POST,true), FILE_APPEND);

	$check_flags = ( "Flags: ".
		isset($_POST['x_first_name']) . " | ". 
		isset($_POST['x_last_name']) . " | ". 
		isset($_POST['x_address']) . " | ". 
		isset($_POST['x_city']) . " | ". 
		isset($_POST['x_country']) . " | ". 
		isset($_POST['x_state']) . " | ". 
		isset($_POST['x_zip']) . " | ". 
		isset($_POST['x_phone']) . " | ". 
		isset($_POST['x_shipping_type']) . " | ". 
		isset($_POST['x_shipping_first_name']) . " | ". 
		isset($_POST['x_shipping_last_name']) . " | ". 
		isset($_POST['x_shipping_address']) . " | ". 
		isset($_POST['x_shipping_city']) . " | ". 
		isset($_POST['x_shipping_country']) . " | ". 
		isset($_POST['x_shipping_state']) . " | ". 
		isset($_POST['x_shipping_zip']) . " | ". 
		isset($_POST['x_shipping_email']) . " | ". 
		isset($_POST['x_shipping_phone']) . " | ". 
		isset($_POST['x_card_num']) . " | ". 
		isset($_POST['exp_month']) . " | ". 
		isset($_POST['exp_year']) . " | ". 
		isset($_POST['x_card_code']) . " | ". 
		isset($_POST['x_email']) 
	);

	file_put_contents( $logfile , "\nCHECK FLAGS (every 1 is post value ok, if blank then one is missing):\n",FILE_APPEND);
	file_put_contents( $logfile , $check_flags."\n", FILE_APPEND);
}

// Checking for POST data #1
if (isset($_POST['x_first_name']) &&
	isset($_POST['x_last_name']) &&
	isset($_POST['x_address']) &&
	isset($_POST['x_city']) &&
	isset($_POST['x_country']) &&
	isset($_POST['x_state']) &&
	isset($_POST['x_zip']) &&
	isset($_POST['x_phone']) &&
	isset($_POST['x_shipping_type']) &&
	isset($_POST['x_shipping_first_name']) &&
	isset($_POST['x_shipping_last_name']) &&
	isset($_POST['x_shipping_address']) &&
	isset($_POST['x_shipping_city']) &&
	isset($_POST['x_shipping_country']) &&
	isset($_POST['x_shipping_state']) &&
	isset($_POST['x_shipping_zip']) &&
	isset($_POST['x_shipping_email']) &&
	isset($_POST['x_shipping_phone']) &&
	isset($_POST['x_card_num']) &&
	isset($_POST['exp_month']) &&
	isset($_POST['exp_year']) &&
	isset($_POST['x_card_code']) &&
	isset($_POST['x_email']))
{
    //$amount = safe_data( $_POST['amount']);
    $coupon_code = safe_data($_POST['x_coupon_code']);
	$first_name = safe_data($_POST['x_first_name']);
	$last_name = safe_data($_POST['x_last_name']);
	$address = safe_data($_POST['x_address']);
	$city = safe_data($_POST['x_city']);
	$country = safe_data($_POST['x_country']);
	$state = safe_data($_POST['x_state']);
	$zip = safe_data($_POST['x_zip']);
	$phone = safe_data($_POST['x_phone']);
	$shipping_type = safe_data($_POST['x_shipping_type']);
	$shipping_first_name = safe_data($_POST['x_shipping_first_name']);
	$shipping_last_name = safe_data($_POST['x_shipping_last_name']);
	$shipping_address = safe_data($_POST['x_shipping_address']);
	$shipping_city = safe_data($_POST['x_shipping_city']);
	$shipping_country = safe_data($_POST['x_shipping_country']);
	$shipping_state = safe_data($_POST['x_shipping_state']);
	$shipping_zip = safe_data($_POST['x_shipping_zip']);
	$shipping_email = safe_data($_POST['x_shipping_email']);
	$shipping_phone = safe_data($_POST['x_shipping_phone']);
	$card_num = safe_data($_POST['x_card_num']);
	$exp_month = safe_data($_POST['exp_month']);
	$exp_year = safe_data($_POST['exp_year']);
	$ccv = safe_data($_POST['x_card_code']);
    //$invoice_num = safe_data($_POST['x_invoice_num']);
	$email = safe_data($_POST['x_email']);
	
}
else
{
    //echo 'CLEAR';
    //$amount = "";
    $coupon_code = '';
	$first_name = '';
	$last_name = '';
	$address = '';
	$city = '';
	$country = '';
	$state = '';
	$zip = '';
	$phone = '';
	$shipping_type = '';
	$shipping_first_name = '';
	$shipping_last_name = '';
	$shipping_address = '';
	$shipping_city = '';
	$shipping_country = '';
	$shipping_state = '';
	$shipping_zip = '';
	$shipping_email = '';
	$shipping_phone = '';
	$card_num = '';
	$exp_month = '';
	$exp_year = '';
	$ccv = '';
    //$invoice_num = '';
	$email = '';
	//$tax = '0.00';
	if(STORE_LOG_ENABLED){
		file_put_contents( $logfile , "WARNING: All Post data have cleared before send to payment transaction, check the flags.\n", FILE_APPEND);
	}

}

// show_array($_SESSION);

// Checking for non empty state_tax #2
if (isset($_SESSION['checkout']['state_tax']) && !empty($_SESSION['checkout']['state_tax']))
{
	$tax = safe_data($_SESSION['checkout']['state_tax']);
}
else
{
	$tax = 0.00;
}

$cc_mask = 'XXXX'.substr($card_num, -4);

$exp_date = $exp_month.'/'.$exp_year;

$get_invoice_prefix_sql = 'SELECT
								invoice_num_start
								FROM
								projects pr
								WHERE
								pr.id = '.$_SESSION['enrollment']['project_id'].'';
	$result_get_invoice_prefix_sql = mysqli_query($connection, $get_invoice_prefix_sql);
	$row_invoice_prefix = mysqli_fetch_assoc($result_get_invoice_prefix_sql);
	$inv_start_num =  $row_invoice_prefix['invoice_num_start'];
	
//get current order id
$last_insert_id = get_last_insert_id($_SESSION['enrollment']['project_id'], $connection);
//$invoice_num_start = get_invoice_num_start($_SESSION['enrollment']['project_id'], $connection);
$invoice_prefix = get_invoice_prefix($_SESSION['enrollment']['project_id'], $connection);
if($last_insert_id == 0)
{
	$last_insert_id = get_first_insert_id($connection);
	$invoice_num = $invoice_prefix.($inv_start_num + $last_insert_id);
	$hash_order_id = substr(double_salt($last_insert_id, $first_name.$last_name), 0, 10);
}
else
{
	$hash_order_id = substr(double_salt($last_insert_id, $first_name.$last_name), 0, 10);
	$invoice_num = $invoice_prefix.($inv_start_num + $last_insert_id);
}

$_SESSION['hash_order_id'] = $hash_order_id;

//$invoice_num = get_invoice_num($_SESSION['enrollment']['project_id'], $connection);
//invoice block ends here
$project_name = get_project_name($_SESSION['enrollment']['project_id'], $connection);
$description = ORDER_URL.'php/order_email_receipt.php?order_id='.$hash_order_id;
//$description = nl2br($description);
$transaction = new AuthorizeNetAIM;
$transaction->setSandbox(AUTHORIZENET_SANDBOX);
//Calculating grand_total #3
if (!empty($_SESSION['cart']) && isset($_SESSION['cart']))
{
		$total_tax = total_tax($tax, $connection);
		$sub_total = get_order_total($connection);
		if($_SESSION['enrollment']['user_price_type'] == '') $_SESSION['enrollment']['user_price_type'] = USER_PRICE_TYPE;
		if($_SESSION['checkout']['shipping_type'] == '') $shipping_total = '0.00';
		else $shipping_total = get_shipping_cost($_SESSION['checkout']['shipping_type'], $connection);
		$_SESSION['coupon_code'] = strtolower($coupon_code);
		$coupon_code_discount = get_coupon_code_discount($connection);
		$grand_total = ($sub_total - $coupon_code_discount) + $total_tax + $shipping_total;

}
else
{
	$grand_total = '0.00';
}

// QUERY SHIPPING DATA
$sq = 'SELECT pso.*, sot.*
FROM project_shipping_options pso
LEFT JOIN shipping_option_types sot ON pso.shipping_option_type_code = sot.code
WHERE pso.id = '.safe_data($_POST['x_shipping_type']); //$shipping_type;

$_SESSION['checkout']['shipping_type'] = safe_data($_POST['x_shipping_type']);

//error_log(str_replace("\n", " ", $sq));

$sr = mysqli_query($connection, $sq);
$srow = mysqli_fetch_assoc($sr);
$shipping_option_type_code = $srow['shipping_option_type_code'];

$shipping_amount = $_SESSION['checkout']['shipping_total'];
//$shipping_amount = $srow['amount'];

$shipping_option_type_name = $srow['name'];
$shipping_option_type_description = $srow['description'];

$freight = 'Freight<|>'.$shipping_option_type_name.' ('.$shipping_option_type_description.')<|>'.$shipping_amount;

// $cc_mask = cc_mask($card_num);

// Data cleansing/formatting of contact information before insert into database.

$first_name = initCapData($first_name);
$last_name = initCapData($last_name);
$address = initCapData($address);
$city = initCapData($city);
$shipping_first_name = initCapData($shipping_first_name);
$shipping_last_name = initCapData($shipping_last_name);
$shipping_address = initCapData($shipping_address);
$shipping_city = initCapData($shipping_city);
$phone = sanitizePhone($phone);
$shipping_phone = sanitizePhone($shipping_phone);

// Sending parameters to authorize.net

if(STORE_LOG_ENABLED){
	$TX =  array(
        'amount' => $grand_total, 
        'card_num' => $card_num, 
        'exp_date' => $exp_date,
        'first_name' => $first_name,
        'last_name' => $last_name,
        'address' => $address,
        'city' => $city,
        'state' => $state,
        'country' => $country,
        'phone' => $phone,
        'zip' => $zip,
        'card_code' => $ccv,
        'invoice_num' => $invoice_num,
		'email' => $email,
		'freight' => $freight,
		'ship_to_first_name' => $shipping_first_name,
		'ship_to_last_name' => $shipping_last_name,
		'ship_to_address' => $shipping_address,
		'ship_to_city' => $shipping_city,
		'ship_to_state' => $shipping_state,
		'ship_to_zip' => $shipping_zip,
		'ship_to_country' => $shipping_country,
		'tax' => $total_tax,
		'customer_ip' => $_SERVER['REMOTE_ADDR'],
		'description' => $description,
		'shipping_email' => $shipping_email,
		'shipping_phone' => $shipping_phone
        );

	file_put_contents( $logfile , "\nSENT TRANSACTION FIELDS\n",FILE_APPEND);
	file_put_contents( $logfile , print_r($TX,true), FILE_APPEND);
}


 $transaction->setFields(
        array(
        'amount' => $grand_total, 
        'card_num' => $card_num, 
        'exp_date' => $exp_date,
        'first_name' => $first_name,
        'last_name' => $last_name,
        'address' => $address,
        'city' => $city,
        'state' => $state,
        'country' => $country,
        'phone' => $phone,
        'zip' => $zip,
        'card_code' => $ccv,
        'invoice_num' => $invoice_num,
		'email' => $email,
		'freight' => $freight,
		'ship_to_first_name' => $shipping_first_name,
		'ship_to_last_name' => $shipping_last_name,
		'ship_to_address' => $shipping_address,
		'ship_to_city' => $shipping_city,
		'ship_to_state' => $shipping_state,
		'ship_to_zip' => $shipping_zip,
		'ship_to_country' => $shipping_country,
		'tax' => $total_tax,
		'customer_ip' => $_SERVER['REMOTE_ADDR'],
		'description' => $description
        )
    );

// If you add additional custom fields, add it this array below
      
$transaction->setCustomFields(array(
		'shipping_email' => $shipping_email,
		'shipping_phone' => $shipping_phone
));


 //Checking for non-empty message which is optional #4
 if (isset($_POST['x_message']) && !empty($_POST['x_message']))
	{
		$comment = safe_data($_POST['x_message']);
		$transaction->setCustomFields(
					   array(
					   'comment' => $comment
					   )
				   );
	}
	
if (isset($_POST['x_coupon_code']) && !empty($_POST['x_coupon_code']))
	{
		$coupon_code = safe_data($_POST['x_coupon_code']);
		$coupon_code_discount = safe_data($coupon_code_discount);
		$transaction->setCustomFields(
					   array(
					   'coupon_code' => $coupon_code,
					   'coupon_code_discount' => $coupon_code_discount
					   )
				   );
	}

// Added by Jay 2/18/16 for Free Sample.	
if (isset($_POST['free_sample']) && !empty($_POST['free_sample']))
	{
		$free_sample = safe_data($_POST['free_sample']);
		$transaction->setCustomFields(
					   array(
					   'free_sample' => $free_sample
					   )
				   );
	}			
 $response = $transaction->authorizeAndCapture();
 //unset($_SESSION['cart']);
 /*
 $transaction_html = '<div data-alert class="alert-box warning radius" style="text-align:center">'.
														'Please do not refresh or reload the page otherwise you will lose all payment information'.
												  '</div>';
*/
	/*
	$transaction_html = '
 	<div data-alert class="alert-box warning radius">
 		<p>Please do not <b>refresh or reload this page</b> otherwise you will lose this payment confirmation.</p>
 	</div>';
 	*/												  

//echo "<pre>";
//print_r($response);

$success_json = array('url' => base_url.'order_failure/index.php');

$_SESSION['checkout_approved'] = $response->approved;

if(STORE_LOG_ENABLED){
	file_put_contents( $logfile , "\nTRANSACTION RESPONSE\n",FILE_APPEND);
	//ob_start();
	//var_dump($response);
	//$result = ob_get_clean();
	file_put_contents( $logfile , print_r($response,true), FILE_APPEND);
}

// Updating data to orders table
	
		// date_default_timezone_set('America/Detroit');
		$created_timestamp = date('Y-m-d H:i:s');
		$order_status = 1; // 1 = New
		// Make this a DEFINE variable and allow for a 8 = test value.
		$update_sql_orders = '';
 		if ($response->approved) //getting data from response
 		{			
			$update_sql_orders = 'UPDATE
	                             orders
								 SET
								 `created_timestamp` = "'.$created_timestamp.'", 
								 `order_status` = "'.$order_status.'", 
								 `order_project_id` = "'.$_SESSION['enrollment']['project_id'].'",
								 `product_subtotal_amount` = "'.$sub_total.'",
								 `total_amount` = "'.$response->amount.'",
								 `total_tax` = "'.$response->tax.'",
								 `first_name` = "'.$response->first_name.'",
								 `last_name` = "'.$response->last_name.'",
								 `address` = "'.$response->address.'",
								 `city` = "'.$response->city.'",
								 `country` = "'.$response->country.'",
								 `state` = "'.$response->state.'",
								 `zip_code` = "'.$response->zip_code.'",
								 `email` = "'.$response->email_address.'",
								 `phone` = "'.$phone.'",
								 `shipping_first_name` = "'.$response->ship_to_first_name.'",
								 `shipping_last_name` = "'.$response->ship_to_last_name.'",
								 `shipping_address` = "'.$response->ship_to_address.'",
								 `shipping_city` = "'.$response->ship_to_city.'",
								 `shipping_country` = "'.$response->ship_to_country.'",
								 `shipping_state` = "'.$response->ship_to_state.'",
								 `shipping_zip_code` = "'.$response->ship_to_zip_code.'",
								 `shipping_email` = "'.$shipping_email.'",
								 `shipping_phone` = "'.$shipping_phone.'",
								 `shipping_description` = "'.$shipping_option_type_description.'",
								 `shipping_option_type_code` = "'.$shipping_option_type_code.'",
								 `shipping_option_type_name` = "'.$shipping_option_type_name.'",
								 `shipping_total` = "'.$shipping_amount.'",
								 `exp_date` = "'.$exp_date.'",
								 `transaction_id` = "'.$response->transaction_id.'",
								 `invoice_num` = "'.$response->invoice_number.'",
								 `purchase_order_number` = "'.$response->invoice_number.'",
								 `authorization_code` = "'.$response->authorization_code.'",
								 `avs_response` = "'.$response->avs_response.'",
								 `card_code_response` = "'.$response->card_code_response.'",
								 `cavv_response` = "'.$response->cavv_response.'",
								 `card_type` = "'.$response->card_type.'",
								 `cc_mask` = "'.$cc_mask.'",
								 `status` = "'.$response->status.'",
								 `response_code` = "'.$response->response_code.'",
								 `response_subcode` = "'.$response->response_subcode.'",
								 `response_reason_code` = "'.$response->response_reason_code.'",
								 `response_reason_text` = "'.$response->response_reason_text.'",
								 `hash` = "'.$hash_order_id.'"';
		}else{ //if fails then getting data from post vars
			
			$order_status = 9; // 9 = Failed


		    $coupon_code = safe_data($_POST['x_coupon_code']);
			$first_name = safe_data($_POST['x_first_name']);
			$last_name = safe_data($_POST['x_last_name']);
			$address = safe_data($_POST['x_address']);
			$city = safe_data($_POST['x_city']);
			$country = safe_data($_POST['x_country']);
			$state = safe_data($_POST['x_state']);
			$zip = safe_data($_POST['x_zip']);
			$phone = safe_data($_POST['x_phone']);
			$shipping_type = safe_data($_POST['x_shipping_type']);
			$shipping_first_name = safe_data($_POST['x_shipping_first_name']);
			$shipping_last_name = safe_data($_POST['x_shipping_last_name']);
			$shipping_address = safe_data($_POST['x_shipping_address']);
			$shipping_city = safe_data($_POST['x_shipping_city']);
			$shipping_country = safe_data($_POST['x_shipping_country']);
			$shipping_state = safe_data($_POST['x_shipping_state']);
			$shipping_zip = safe_data($_POST['x_shipping_zip']);
			$shipping_email = safe_data($_POST['x_shipping_email']);
			$shipping_phone = safe_data($_POST['x_shipping_phone']);
			$card_num = safe_data($_POST['x_card_num']);
			$exp_month = safe_data($_POST['exp_month']);
			$exp_year = safe_data($_POST['exp_year']);
			$ccv = safe_data($_POST['x_card_code']);
		    //$invoice_num = safe_data($_POST['x_invoice_num']);
			$email = safe_data($_POST['x_email']);

			$update_sql_orders = 
			'UPDATE orders
			   SET
				 `created_timestamp` = "'.$created_timestamp.'", 
				 `order_status` = "'.$order_status.'", 
				 `order_project_id` = "'.$_SESSION['enrollment']['project_id'].'",
				 `product_subtotal_amount` = "'.$sub_total.'",
				 `total_amount` = "'.$grand_total.'",
				 `total_tax` = "'.$total_tax.'",
				 `first_name` = "'.$first_name.'",
				 `last_name` = "'.$last_name.'",
				 `address` = "'.$address.'",
				 `city` = "'.$city.'",
				 `country` = "'.$country.'",
				 `state` = "'.$state.'",
				 `zip_code` = "'.$zip.'",
				 `email` = "'.$email.'",
				 `phone` = "'.$phone.'",
				 `shipping_first_name` = "'.$shipping_first_name.'",
				 `shipping_last_name` = "'.$shipping_last_name.'",
				 `shipping_address` = "'.$shipping_address.'",
				 `shipping_city` = "'.$shipping_city.'",
				 `shipping_country` = "'.$shipping_country.'",
				 `shipping_state` = "'.$shipping_state.'",
				 `shipping_zip_code` = "'.$shipping_zip.'",
				 `shipping_email` = "'.$shipping_email.'",
				 `shipping_phone` = "'.$shipping_phone.'",
				 `shipping_description` = "'.$shipping_option_type_description.'",
				 `shipping_option_type_code` = "'.$shipping_option_type_code.'",
				 `shipping_option_type_name` = "'.$shipping_option_type_name.'",
				 `shipping_total` = "'.$shipping_amount.'",
				 `exp_date` = "'.$exp_date.'",
				 `invoice_num` = "'.$invoice_num.'",
 				 `avs_response` = "'.$response->avs_response.'",
				 `card_code_response` = "'.$response->card_code_response.'",
				 `cavv_response` = "'.$response->cavv_response.'",
				 `card_type` = "'.$response->card_type.'",
				 /*`transaction_id` = "'.$response->transaction_id.'", 
				 `authorization_code` = "'.$response->authorization_code.'", */
				 `cc_mask` = "'.$cc_mask.'",
				 `status` = "'.$response->status.'",
				 `response_code` = "'.$response->response_code.'",
				 `response_subcode` = "'.$response->response_subcode.'",
				 `response_reason_code` = "'.$response->response_reason_code.'",
				 `response_reason_text` = "'.$response->response_reason_text.'",
				 `hash` = "'.$hash_order_id.'"';
		}

		// All updates below apply whether Order was success or fail.
				
		// Added by Jay 12/5/15 to tie Order to User Session Metrics for Marketing
		if (isset($_SESSION['userStatistics']['session_id']) && !empty($_SESSION['userStatistics']['session_id']))
		{
			$update_sql_orders.= ' ,`user_session_id` = "'.$_SESSION['userStatistics']['session_id'].'"';
		}
	
		// Added by Jay 11/26/15 because user_name can be changed by the User but user_id stays the same.
		if (isset($_SESSION['user']['user_id']) && !empty($_SESSION['user']['user_id']))
		{
			$update_sql_orders.= ' ,`user_id` = "'.$_SESSION['user']['user_id'].'"';
		}
		
		if (isset($_SESSION['user']['username']) && !empty($_SESSION['user']['username']))
		{
			$update_sql_orders.= ' ,`user_name` = "'.$_SESSION['user']['username'].'"';
		}
		
		// if you want to add custom fields, you need to append them to this query using example line below
		//$update_sql_orders.= ', `ngs_number` = "'.$ngs_no.'", `date_of_birth` = "'.$dob.'"';
											
		if (isset($_POST['x_message']) && !empty($_POST['x_message']))
		{
			$update_sql_orders.= ', `comments` = "'.$_POST['x_message'].'"';
		}
		
		if (isset($_POST['x_coupon_code']) && !empty($_POST['x_coupon_code']))
		{
			$update_sql_orders.= ', `project_shopping_code_id` = "'.$_SESSION['checkout']['coupon']['coupon_code_id'].'"';
			$update_sql_orders.= ', `discount_amount` = "'.$coupon_code_discount.'"';
		}
		
		// Added by Jay 2/18/16 for Free Sample.
        if (isset($_POST['free_sample']) && !empty($_POST['free_sample']))
		{
			$update_sql_orders.= ', `free_sample` = "'.$free_sample.'"';
		}
		
		if (isset($shipping_type) && !empty($shipping_type))
		{
			$update_sql_orders.= ', `project_shipping_option_id` = "'.$shipping_type.'"';
			$update_sql_orders.= ', `shipping_total` = "'.$shipping_amount.'"';
		}
		
		// New attributes added by Jay 8/1/15 for providing filters to view order data by.
		
		if (isset($_SESSION['application']['application_id']) && !empty($_SESSION['application']['application_id']))
		{
			$update_sql_orders.= ' ,`application_id` = "'.$_SESSION['application']['application_id'].'"';
		}
		
		if (isset($_SESSION['enrollment']['project_program_id']) && !empty($_SESSION['enrollment']['project_program_id']))
		{
			$update_sql_orders.= ' ,`project_program_id` = "'.$_SESSION['enrollment']['project_program_id'].'"';
		}
		
		if (isset($_SESSION['enrollment']['course_id']) && !empty($_SESSION['enrollment']['course_id']))
		{
			$update_sql_orders.= ' ,`course_id` = "'.$_SESSION['enrollment']['course_id'].'"';
		}
		
		if (isset($_SESSION['enrollment']['topic_id']) && !empty($_SESSION['enrollment']['topic_id']))
		{
			$update_sql_orders.= ' ,`topic_id` = "'.$_SESSION['enrollment']['topic_id'].'"';
		}

		if (isset($_SESSION['enrollment']['web_page_id']) && !empty($_SESSION['enrollment']['web_page_id']))
		{
			$update_sql_orders.= ' ,`web_page_id` = "'.$_SESSION['enrollment']['web_page_id'].'"';
		}

		if (isset($_SESSION['blog_id']) && !empty($_SESSION['blog_id']))
		{
			$update_sql_orders.= ' ,`blog_id` = "'.$_SESSION['blog_id'].'"';
		}

		if (isset($_SESSION['blog_post']['blog_post_id']) && !empty($_SESSION['blog_post']['blog_post_id']))
		{
			$update_sql_orders.= ' ,`blog_post_id` = "'.$_SESSION['blog_post']['blog_post_id'].'"';
		}

		// SSL prevents $_SERVER from showing $_SERVER['HTTP_REFERER'] for connections from http to https that
		// get redirected through htaccess file. 
		// Need to find another to get referring URL to tie marketing to sales conversion.
		// Research solution when have time.
		if (isset($_SESSION['referring_url']) && !empty($_SESSION['referring_url']))
		{
			$update_sql_orders.= ' ,`referring_url` = "'.$_SESSION['referring_url'].'"';
		}
			
		$update_sql_orders .= ' WHERE id = '.$last_insert_id;
			//echo $update_sql_orders;
			
		$result_update_orders = mysqli_query($connection, $update_sql_orders);
		
		if (!$result_update_orders)
		{
			log_sql_error('Error on update orders',$update_sql_orders,$connection);
			 $_SESSION['transaction_html'] = '<h1>Internal Error Occurred, please contact Customer Service at support@supplementrelief.com</h1>';
			die(json_encode($success_json));
		}
		
	// Inserting data to order_items table #7.
	if (!empty($_SESSION['cart']) && isset($_SESSION['cart']))
	{
		/*
		$get_order_id_sql = 'SELECT id
						 FROM orders
						 where transaction_id = '.$response->transaction_id.'';
		$result=mysqli_query($connection,$get_order_id_sql);
		$row = mysqli_fetch_assoc($result);
		$order_id = $row['id'];
		*/
		$order_id = $last_insert_id;
		// Added by Jay 8/12/16 for Google Analytics Ecommerce Tracking
		$_SESSION['order']['order_id'] = $order_id;
		$max=count($_SESSION['cart']);
			for($i=0;$i<$max;$i++)
			{
				$pid=$_SESSION['cart'][$i]['productid'];
				$q=$_SESSION['cart'][$i]['qty'];
				$price_type = $_SESSION['cart'][$i]['price_type'];
				$post_select1 = 'prd_'.$pid;
				$last_selected = $_SESSION['cart'][$i]['last_selected'];
				
				if (isset($_SESSION['cart'][$i][$post_select1]) && !empty($_SESSION['cart'][$i][$post_select1]))
				 {
					
					$post_select_session = $_SESSION['cart'][$i][$post_select1];
					$p_price=get_price($pid,$q,$price_type,$post_select_session,$connection,$last_selected);
					$p_option_name =  addslashes(serialize($post_select_session));
					
				 }
				 else
				 {
					$p_price=get_price($pid,$q,$price_type,null,$connection,$last_selected);
					$p_option_name = NULL;
				 }
				 $total = number_format((float)$p_price*$q, 2, '.', '');
				
				 $insert_sql_order_items = 'INSERT INTO order_items (
																		`orders_id`,`brand_product_usage_id`,
																		`order_item_options`, `quantity` ,`unit_price`,
																		`amount`
																	 ) VALUES('.
																	 " '$order_id',
																	'$pid', '$p_option_name', '$q','$p_price', '$total'".')';
			
				
				$result_insert_sql_order_items = mysqli_query($connection, $insert_sql_order_items);
				if (!$result_insert_sql_order_items)
				{
					log_sql_error('Error on insert order item',$insert_sql_order_item,$connection);
					 $_SESSION['transaction_html'] = '<h1>Internal Error Occurred, please contact to Customer Service</h1>';
					die(json_encode($success_json));
				}
				if (isset($_SESSION['cart'][$i][$post_select1]) && !empty($_SESSION['cart'][$i][$post_select1]))
				{
					$post_select_session = $_SESSION['cart'][$i][$post_select1];
					foreach($post_select_session AS $key => $value)
					{
						$get_item_no = 'SELECT
										id
										FROM
										order_items
										WHERE
										orders_id = '.$order_id.'
										ORDER BY id DESC
										LIMIT 1 ';
						$result_get_item_no = mysqli_query($connection, $get_item_no);
						$row_item_no = mysqli_fetch_assoc($result_get_item_no);
						$item_seq_no = $row_item_no['id'];
						$option_name_sql = 'SELECT option_value
											FROM `brand_product_usage_options`
											WHERE id = '.$value[0].' 
											AND (effective_date <= CURDATE() AND (end_date is NULL or end_date >= CURDATE() )) ';
											
						$result_option_name_sql = mysqli_query($connection, $option_name_sql);
						$row_option_name = mysqli_fetch_assoc($result_option_name_sql);
						$item_option_name = $row_option_name['option_value'];
						
						$insert_order_item_options_sql = 'INSERT INTO order_item_options (
																			`order_id`,
																			`order_item_id`,
																			`brand_product_usage_id`,
																			 `item_option_id`,
																			 `order_option_type_code`,
																			 `order_option_name`
																		 ) VALUES('.
																		 " '$order_id','$item_seq_no',
																		'$pid', '$value[0]',$key,'$item_option_name'".')';
						$result_insert_order_item_options_sql = mysqli_query($connection, $insert_order_item_options_sql);
						if (! $result_insert_order_item_options_sql)
						{
							//echo $insert_order_item_options_sql;
							//echo mysqli_error($connection);
							log_sql_error('Database insert option item query failed',$insert_order_item_options_sql,$connection);
							 $_SESSION['transaction_html'] = '<h1>Internal Error Occurred, please contact to Customer Service</h1>';
							die(json_encode($success_json));
						}
					}
				}
				
			}
			
	}
	
	// Update the Order with Total Product Cost
	
	// Get the total retail and cost amounts of the products on the order
	// $product_cost_amount = calculate_order_total($last_insert_id, $connection);
	list($product_retail_amount, $product_cost_amount) = calculate_order_total($last_insert_id, $_SESSION['enrollment']['project_id'], $connection);
	
	$update_sql_order = '
	UPDATE orders SET 
	product_retail_amount = "'.$product_retail_amount.'", 
	product_cost_amount = "'.$product_cost_amount.'" 
	WHERE id = '.$last_insert_id;
	// echo $update_sql_order;
			
	$result_update_order = mysqli_query($connection, $update_sql_order);
		
	if (!$result_update_order)
	{
		//echo $update_sql_order;
		//die("Database Orders product_cost_amount update failed.");
		log_sql_error('Database Orders product_cost_amount update failed',$update_sql_order,$connection);
		 $_SESSION['transaction_html'] = '<h1>Internal Error Occurred, please contact to Customer Service</h1>';
		die(json_encode($success_json));

	}

$from = get_from_email($_SESSION['enrollment']['project_id'], $connection);	

	// Send email to customer.
if ($response->approved){
	$email = generate_order_html($last_insert_id , $connection);
	
	
	// $subject = $project_name.' '.'Order Receipt - Invoice #'.' '.$invoice_num;
	$subject = $project_name.' '.'Order Receipt: #'.' '.$invoice_num;
	
	if($shipping_email != $response->email_address) send_email($from, $response->email_address, $subject, $email, $shipping_email);
	else send_email($from, $response->email_address, $subject, $email, '');
	
	// Send abbreviated version of email to Customer Service.
	
	$customer_service_email = generate_customer_service_order_html($last_insert_id , $connection);
	$customer_service_to = CUSTOMER_SERVICE_EMAIL;
	// $subject = $project_name.' '.'Order Receipt - Invoice #'.' '.$invoice_num;
	
	send_email($from, $customer_service_to, $subject, $customer_service_email, '');				     
	
	/*							 
	$transaction_html.= '<ul class="pricing-table">'.
                                '<li class="title">Payment Details (Success)</li>'.
                                '<li class="title">Invoice Number:'.' '.$response->invoice_number .'</li>'.
                                '<li class="price">Amount: '.'$'.$response->amount.' '.'is recieved by '.authnet_vendor_name.'</li>'.
                                '<li class="price">Your transaction completed successfully.</li>'.
                        '</ul>';
    */
    
    // Display alternate email in Order Confirmation message if one is provided.
    if (!empty($shipping_email) && ($shipping_email != $response->email_address)) {
	   $alternate_email = ' with a copy to <b>'.$shipping_email.'</b>';	    
    } else {
	   $alternate_email = ''; 	    
    }
                                
    $transaction_html.= '
    <h3><i class="fa fa-thumbs-up" style="color: #5CB64B;"></i>&nbsp;Thank You!</h3>
    <p>Your order completed successfully. Your <b>Order Number</b> is <b>'.$response->invoice_number.'</b>. The amount charged to your credit card was <b>$'.$response->amount.'</b>. An order confirmation email was sent to <b>'.$response->email_address.'</b>'.$alternate_email.'. If you do not receive your email soon, please check that the <b>Email Address used on your order is correct</b> and that the email sent is not <b>in your SPAM folder</b>.<p>When your order ships we will <b>email you the delivery tracking information</b>.</p></p><p>Please call <b>(888) 424-0032</b> or <a href="../contact">email</a> if we can be of further assistance.</p>';
					
    $_SESSION['transaction_html'] = $transaction_html;
    $_SESSION['invoice_number'] = $response->invoice_number;
    $_SESSION['amount'] = $response->amount;
    $_SESSION['email_address'] = $response->email_address;
    $_SESSION['alternate_email'] = $alternate_email;
    
    unset($_SESSION['cart']);
    
    $success_json['url'] = base_url.'order_success/index.php';

    //remove CART if exists
    unset($_SESSION['LAST_ACTIVITY']);
    unset($_SESSION['_INFO']);
    
    // REDIRECT to supplementalrelief/order_success/index.php
        
 }
 else
 {
	// if transaction is declined #8
	/*
    $transaction_html.= '<ul class="pricing-table1">'.
                            '<li class="title">Payment Details(Failure)</li>'.
                            '<li class="price">Amount: '.'$0.00'.' '.'is received by '.authnet_vendor_name.'</li>'.
                            '<li class="price">Your transaction is declined due to following reason.</li>'.
                            '<li class="price">'.$response->response_reason_text.'</li>'.
                        '</ul>';
   
   */
   
   // Trap the error code and provide the user with an error message.

   // http://developer.authorize.net/tools/responsereasoncode/   
   // echo 'Authorize.net Response Code: '.$response->response_code.'<br>';
   	   
   $transaction_html.= '
   <div data-alert class="alert-box warning">
   		<p><i class="fa fa-exclamation-triangle fa-2x"></i>&nbsp;&nbsp;<b>The credit card information provided was declined</b>. The most likely reason is because the <b>Card Number, Expiration Date, CCV or Billing Address was NOT VALID</b>. Please select the <b>Back to Checkout Page</b> button to <b>review the information provided</b> and then <b>try again if any of this information was entered incorrectly</b>. Otherwise, please <b>check with your card issuer to resolve the issue</b> or <b>try again with a different card</b>. You may also call us at <b>(888) 424-0032</b> if you need further assistance.</p> 
   </div>';
	      
   /*
   if ($response->response_code == 2) {
	   // Expiration Date or CCV invalid
	   
   		$transaction_html.= '
   		<div data-alert class="alert-box warning">
  	 		<p><i class="fa fa-exclamation-triangle"></i>&nbsp;&nbsp;<b>The credit card information provided was declined</b>. The most likely reason is because the <b>Card Number</b>, <b>Expiration Date or CCV</b> was not valid. Please select the <Back to Checkout Page</b> button to review the information provided and then try again if any of this information was entered incorrectly. Otherwise, please <b>check with your card issuer to resolve the issue</b> or <b>try again with a different card</b>.</p> 
  	 	</div>';
	   
   } elseif ($response->response_code == 3) {
	  // Card Number invalid 
	  
	    $transaction_html.= '
  		<div data-alert class="alert-box warning">
  	 		<p><i class="fa fa-exclamation-triangle"></i>&nbsp;&nbsp;<b>The credit card information prov was declined</b>. The most likely reason is because the <b>Card Number</b> provided on the order was not valid. Please try again if this information was entered incorrectly. Otherwise, please <b>check with your card issuer to resolve the issue</b> or <b>try again with a different card</b>.</p> 
  	 	</div>';

	   
   } else {
	   
	$transaction_html.= '
	<div data-alert class="alert-box warning">
	    <p><i class="fa fa-exclamation-triangle"></i>&nbsp;&nbsp;The credit card entered was <b>declined</b> for the following reason: '.$response->response_reason_text.' Your card has not been charged. Please check with your card issuer to resolve the issue or try again with a different card.</p>
	</div>'; 
	   
   }
   */

	//sending a email with transactions failed
    $subject = $project_name.' '.'Order Receipt No #'.$invoice_num.' FAILED';

 	$customer_service_email = generate_customer_service_order_html($last_insert_id , $connection, true /*failed*/, $response->response_code, $response->response_subcode, $response->response_reason_code);
 	 	
	$customer_service_to = CUSTOMER_SERVICE_EMAIL_FAIL;
	
	send_email($from, $customer_service_to, $subject, $customer_service_email, '');				     

	//saving transaction CC for 5 mins

	$_SESSION['_INFO'] = array(	
		'card_num' => safe_data($_POST['x_card_num']),
		'exp_month' => safe_data($_POST['exp_month']),
		'exp_year' => safe_data($_POST['exp_year']),
		'ccv' => safe_data($_POST['x_card_code']),
	);

	$_SESSION['LAST_ACTIVITY'] = time();

 $_SESSION['transaction_html'] = $transaction_html;
 
 // REDIRECT to supplementalrelief/order_failure/index.php
 
 }
/* 
 $transaction_html.= '<center><img src="https://wrightcheck.com/media/images/supplementrelief-logo-blue-landscape-large.jpg" /></center><br />'. 
 					 '<a href="./checkout.php" class="radius button_shopping expand button">Back to Checkout Page</a>'.
					 '<a href="../numedica/" class="radius button_shopping expand button success">Continue Shopping</a>';
*/
//echo $transaction_html;
//echo '<a class="close-reveal-modal">&#215;</a>';
echo json_encode($success_json);

?>
