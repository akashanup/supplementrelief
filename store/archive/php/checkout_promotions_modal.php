<div id="checkoutPromotion" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">

	<h2 id="modalTitle">Closeout Special</h2>
  
	<p class="lead">Include with your order today!</p>
  
	<hr>
  
	<div class="row">
				
		<div class="small-12 medium-6 columns">
		
			<?php 
				
			echo integrate_add_to_cart_button($connection, array(
				'p_usage_id' => 4,
				'pid' => 264,
				'project_id' => 1,
				'p_name' => 'NuMedica Absolute Protein Gorilla',
				'form_name' => 'form_large',
				'qty_visible' => TRUE,
				'button_html_content' => '',
				'retail_price' => 59.95,
				'price_type' => 'RETL'
			));
			
			?>

		</div>
	
		<div class="small-12 medium-6 columns">
			<center><img src="https://cdn-manager.net/media/images/numedica-Absolute-Protein-Vanilla-39svg-medium.jpg"></center>
		</div>

	</div>
	
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	
</div>

<script type="text/javascript">
	
	$(document).ready(function(){
		var width = $(window).width(), height = $(window).height();
		console.log(width + " "+ height);
		if ((width > 480) && (height > 640)) 
		{
			window.setTimeout(function() {
			$('#checkoutPromotion').foundation('reveal', 'open');}, <?php echo STORE_CHECKOUT_MODAL_DELAY; ?>);	
		}
		
	});
	
</script>
