<?php
define("STORE_FOLDER_NAME","store");
include_once('../includes/store_page_header.php');
include_once('../'.core);
//Check for empty post data #1

if(isset($_POST['shipping_type']) && !empty($_POST['shipping_type'])) {
    $shipping_type = safe_data($_POST['shipping_type']);
    $_SESSION['checkout']['shipping_type'] = $shipping_type;
    //Get shipping type amount
	?>

	<!-- Replacing the checkout block content with ajax -->
	<div id="check_out_block" >
		
		<div id="header_checkout_div" style="text-align: center">
			<h6><strong>ORDER SUMMARY</strong></h6>
		</div>
		
		<div id="order_total_checkout" style="padding-top: 20px; padding-left: 20px; padding-right: 20px;" >
			<?php
			//echo '<pre>';
			//print_r($_SESSION);
		
			if (!empty($_SESSION['cart'])) {
				$project_id = safe_data($_SESSION['enrollment']['project_id']);
                //$sales_tax = get_sales_tax($project_id, $connection);
                $total_tax = total_tax($_SESSION['checkout']['state_tax'], $connection);
				$sub_total = get_order_total($connection);
				if($_SESSION['enrollment']['user_price_type'] == '') $_SESSION['enrollment']['user_price_type'] = USER_PRICE_TYPE;
				if($_SESSION['checkout']['shipping_type'] == '') $shipping_total = '0.00';
				else $shipping_total = get_shipping_cost($_SESSION['checkout']['shipping_type'], $connection);
				$coupon_code_discount = get_coupon_code_discount($connection);
				$grand_total = ($sub_total - $coupon_code_discount) + $total_tax + $shipping_total;
				$grand_total = pFormat($grand_total);
				
				echo 
				'<div class="panel">';
					// echo '<table width="100%" style="background-color:#f2f2f2" >
					echo 
					'<table width="100%" style="background-color:#f2f2f2" >
						<tbody>
							<tr>
								<td style="text-align:right">Product Subtotal:</td>
								<td style="text-align:right">$'.pFormat($sub_total).'</td>
							</tr>';
							
						if(CHECK_OUT_PROMOTION_CODE == 'yes') {
							echo 
							'<tr>
								<td style="text-align:right">Promotion Discount:</td>
								<td style="text-align:right">$'.pFormat($coupon_code_discount).'</td>
							</tr>';
						}
						
						echo '<tr>
								<td style="text-align:right">Sales Tax: </td>
								<td style="text-align:right">$'.pFormat($total_tax).'</td>
							</tr>
							<tr>
								<td style="text-align:right">Shipping:</td>
								<td style="text-align:right">$'.pFormat($shipping_total).'</td>
							</tr>
							<tr>
								<td style="text-align:right"><font size="4" color="RED">Order Total: </font></td>
								<td style="text-align:right"><font size="4" color="RED">$'.$grand_total.'</font></td>
						   </tr>
					   </tbody>
					</table>';
				echo 
				'</div>';
				//echo '<div class="radius expand button right buttton_grand_total">'.'<b>Order Total: </b>&nbsp; $'.$grand_total.'</div>';
				
			} else {
				echo 
				'<div class="panel">';
					echo 
					'<table width="100%" style="background-color:#f2f2f2" >
						<tbody>
							<tr>
								<!-- <td style="text-align:right">Product Subtotal:</td> -->
								<td style="text-align:right">Product Subtotal:</td>
								<td style="text-align:right">$0.00</td>
							</tr>';
							
							if(CHECK_OUT_PROMOTION_CODE == 'yes') {
								echo 
								'<tr>
									<td style="text-align:right">Promotion Discount: </td>
									<td style="text-align:right">$0.00</td>
								</tr>';
							}
						
							echo 
							'<tr>
								<td style="text-align:right">Sales Tax: </td>
								<td style="text-align:right">$0.00</td>
							</tr>
							<tr>
								<td style="text-align:right">Shipping: </td>
								<td style="text-align:right">$0.00</td>
							</tr>
							<tr>
								<td style="text-align:right"><font size="4" color="RED">Order Total: </font></td>
								<td style="text-align:right"><font size="4" color="RED">$0.00</font></td>
							</tr>
						</tbody>
					</table>';
				echo 
				'</div>';
				//echo '<div class="radius expand button right buttton_grand_total">'.'<b>Order Total: </b>&nbsp; $'.'0.00'.'</div>';
			}	
			?>
		</div>
		
		<?php
		if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])) {
			?>
			<div style="padding-left: 20px; padding-right: 20px;" id="checkout_submit_div">
				<input type="button" value="Place Order" id="checkout_submit" class="submit button medium radius expand" >
			</div>
			<?php
		} else {
	  		?>
	  		<div style="padding-left: 20px; padding-right: 20px;" >
				<input type="button" value="Place Order"  class="disabled submit button medium radius expand" >
	  		</div>
	  		<?php 
		}
		?>
    </div>
    <?php
        
} else {
	$shipping_type = '';
    echo 'invalid link';
}

?>
