<?php
define("STORE_FOLDER_NAME","store");
include_once('../includes/store_page_header.php');
include_once('../'.core);
//Checking for non empty order id #1
if(isset($_GET['order_id']) && !empty($_GET['order_id']))
{
    //Getting hash from url
    $order_hash = safe_data($_GET['order_id']);
    $get_order_id_sql = 'SELECT
                            id, order_project_id
                            FROM
                            orders
                            WHERE
                            hash = "'.$order_hash.'"';
    $result_get_order_id_sql = mysqli_query($connection, $get_order_id_sql);
    $total_rows = mysqli_num_rows($result_get_order_id_sql);
    if($total_rows == 1)
    {
        //Generate order html
        $row_order = mysqli_fetch_assoc($result_get_order_id_sql);
        $_SESSION['enrollment']['project_id'] = $row_order['order_project_id'];
        $html = generate_order_html($row_order['id'],$connection);
        echo $html;
    }
    else
    {
        echo "This is not a valid link";
    }
    
}
//Showing error message #1
else
{
    $order_id = '';
    echo "This is not a valid link";
}

?>
