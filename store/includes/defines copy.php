	<?php
	if(!defined ("VERSION"))
	{
		define("VERSION", '1');
	}
	define("BOWER_PATH", "../bower_components/"); // Path To Foundation Resources at the Project level
	define("STORE_HEADER", "includes/header.php"); // Store Header
	define("STORE_FOOTER", "includes/footer.php"); // Store Footer
	define("STORE_JS_DEFINE", "includes/js_defines.php");
	define("STORE_FUNCTIONS", "functions.php");
	define("STORE_MODERNIZR_JS", BOWER_PATH."modernizr/modernizr.js");
	define("STORE_JQUERY_STICKY_JS", "../store/js/jquery_sticky.js");
	define("STORE_READMORE_JS", "../store/js/readmore.min.js");
	define("STORE_STORE_CSS", "../store/css/store.min.css?_version=".VERSION);
	define("STORE_APP_CSS", "../stylesheets/app.min.css?_version=".VERSION);
	define("STORE_STORE_JS", "../store/js/store.js?_version=".VERSION);
	define("STORE_MOBILE_JS", "../store/js/store_mobile.js?_version=".VERSION);
	//define("STORE_FOOTER_JS", "../store/js/store_footer.js");
	define("STORE_CHECKOUT_JS", "js/checkout.js?_version=".VERSION);
	define("STORE_FOUNDATION_MIN_JS", BOWER_PATH."foundation/js/foundation.min.js");
	define("STORE_SCROLLUP_JS", "../js/jquery.scrollUp.min.js");
	define("STORE_BLOCKUI_JS", "../store/js/jquery.blockUI.js");
	define("STORE_FOUNDATION_JQUERY_JS","../bower_components/foundation/js/vendor/jquery.js");
	define("CORE", "../../includes/core.php"); // CORE site file
	
	switch ($_SERVER['HTTP_HOST']) {
		// Jay
    	case 'supplementrelief:8888':
    		define("base_url", "http://supplementrelief:8888/"); // Base URL
			define("STORE_LOG_ENABLED", true);
			define("SQL_ERROR_ACTION", 'log');
			define("STORE_LOG_FILE", "checkout.log");
			break;

		// Akash
    	case 'local.supplementreliefdev.com':
    		define("base_url", "http://local.supplementreliefdev.com/"); // Base URL
			define("STORE_LOG_ENABLED", true);
			define("SQL_ERROR_ACTION", 'log');
			define("STORE_LOG_FILE", "checkout.log");
			break;

    	// Don
    	case 'supplementalrelief:8888':
    		define("base_url", "http://supplementalrelief:8888/"); // Base URL
			define("STORE_LOG_ENABLED", true);
			define("SQL_ERROR_ACTION", 'log');
			define("STORE_LOG_FILE", "checkout.log");
			break;
			
    	// Ivan
    	case 'ivancp.dw04.pe':
    		define("base_url", "http://ivancp.dw04.pe/"); // Base URL
			define("STORE_LOG_ENABLED", true);
			define("SQL_ERROR_ACTION", 'log');
			define("STORE_LOG_FILE", "checkout.log");
		    break;

    	// Development
    	case 'develop.supplementrelief.com':
    		define("base_url", "https://supplementrelief.com/development/"); // Base URL
			define("STORE_LOG_ENABLED", true);
			define("SQL_ERROR_ACTION", 'log');
			define("STORE_LOG_FILE", "checkout.log");
		    break;
    	// Production
    	default:
    		define("base_url", "https://supplementrelief.com/"); // Base URL
			define("STORE_LOG_ENABLED", true);
			define("SQL_ERROR_ACTION", 'log');
			define("STORE_LOG_FILE", "checkout.log");
		    break;
    }

	define("ROOT_DIRECTORY", ""); // Root Directory
	
	// Development & Testing
	/*
	define("AUTHORIZENET_API_LOGIN_ID", "57AkU4ua"); // Jay's RtS Adventures, LLC Sandbox created 11/1/2016
	define("AUTHORIZENET_TRANSACTION_KEY", "5Fmagr368R3MuF43"); // Jay's RtS Adventures, LLC Sandbox
	define("AUTHORIZENET_SANDBOX", true); // make false in PRODUCTION
	*/
	
	
	// Production
	define("AUTHORIZENET_API_LOGIN_ID", "2cw8X9B8DWA"); // RtS Adventures, LLC Production
	define("AUTHORIZENET_TRANSACTION_KEY", "53s26RbPRFkH5p3H"); // RtS Adventures, LLC Production
	define("AUTHORIZENET_SANDBOX", false); // make false in PRODUCTION, true for DEVELOPMENT/TESTING
	
	
	include_once('../includes/defines.php');
	
	define('ORDER_URL', 'https://'.$_SERVER['HTTP_HOST'].'/'.ROOT_DIRECTORY.STORE_FOLDER_NAME.'/'); // keep as is
//	define('USER_PRICE_TYPE', 'RETL');
	define('CHECK_OUT_PROMOTION_CODE', 'no'); // yes or no
	define('CHECK_OUT_FREE_SAMPLES', 'yes'); // yes or no
	define('CHECK_OUT_TERMS_CHECK_BOX', 'no'); // yes or no
	define('STORE_CHECKOUT_MODAL', 'no'); // yes or no
	define('STORE_CHECKOUT_MODAL_DELAY', '3000'); // milliseconds

?>
