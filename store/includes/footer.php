<!-- Sticky plugin -->
<script src="<?php echo STORE_JQUERY_STICKY_JS; ?>"></script>
<!-- Readmore plugin -->
<script src="<?php echo STORE_READMORE_JS; ?>"></script>
<!-- Custom java script file for store section (All screen devices)  -->
<script src="<?php echo STORE_STORE_JS; ?>"></script>
<!-- Custom java script file for store (small screen ) -->
<script src="<?php echo STORE_MOBILE_JS; ?>"></script>
<!-- Scroll to top plugin -->
<script src="<?php echo STORE_SCROLLUP_JS; ?>"></script>
<!-- Block UI plugin for creating modal -->
<script src="<?php echo STORE_BLOCKUI_JS; ?>"></script>
<!--[if lt IE 9]><meta http-equiv="refresh" content="0; url=../legacy_browser/"><![endif]--> 
<!--</div>-->
