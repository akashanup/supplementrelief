<?php
/**
	* Method Name : get_state_name
	* Desc : Used to get state's fullname
	* @access public
	* @param String $abbr country abbreviation
	* @param Object $connection Mysqli connection object
	* @return string
**/

function get_state_name($abbr, $connection)
{
    $get_state_name_sql = 'select
                           state_name
                           FROM
                           states
                           WHERE
                           state_abbr = "'.$abbr.'"';
    $result_get_state_name_sql = mysqli_query($connection, $get_state_name_sql);
	$row_state_name = mysqli_fetch_assoc($result_get_state_name_sql);
    return $row_state_name['state_name'];
}

/**
	* Method Name : get_country_name
	* Desc : Used to get country's fullname
	* @access public
	* @param String $abbr country abbreviation
	* @param Object $connection Mysqli connection object
	* @return string
**/
function get_country_name($abbr, $connection)
{
    $get_country_name_sql = 'select
                           country_name
                           FROM
                           country
                           WHERE
                           country_abbr = "'.$abbr.'"';
    $result_get_country_name_sql = mysqli_query($connection, $get_country_name_sql);
	$row_country_name = mysqli_fetch_assoc($result_get_country_name_sql);
    return $row_country_name['country_name'];
}

/**
	* Method Name : get_from_email
	* Desc : Used to get send's email-id
	* @access public
	* @param integer $project_id project id
	* @param Object $connection Mysqli connection object
	* @return string
**/
function get_from_email($project_id, $connection)
{
	$get_from_email_sql = 'SELECT
							order_receipt_from_email
							FROM
							projects pr
							WHERE
							pr.id = '.$project_id.'';
	$result_get_from_email_sql = mysqli_query($connection, $get_from_email_sql);
	$row_from = mysqli_fetch_assoc($result_get_from_email_sql);
	return $row_from['order_receipt_from_email'];
}

/**
	* Method Name : send_email
	* Desc : Used to send email
	* @access public
	* @param String $from email id of sender
	* @param String $to email id of receiver
	* @param String $subject subject of email
	* @param string $message body of email
	* @return void
**/
function send_email($from, $to, $subject, $message, $cc = '')
{
	
	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

	// Additional headers
	//$headers .= "To: $to \r\n";
	$headers .= "From: $from \r\n";
	if(strlen($cc) > 0) $headers .= "CC: $cc \r\n";
	//echo $headers;
	//if mail is sent successfully #1
	if (mail($to,$subject,$message,$headers) )
	{
	   // echo "Email sent successfully to ".$to.". Please close this window/tab.";
	}
	//Else show error message #1
	else
	{
	   echo "Email couldn't be sent to ".$to.". Please close this window/tab and try again.";
	   die;
	}
}

/**
	* Method Name : get_project_name
	* Desc : Used to get project name from projects table
	* @access public
	* @param integer $project_id project id
	* @param Object $connection Mysqli connection object
	* @return string
**/
function get_project_name($project_id, $connection)
{
	$get_project_name_sql = 'SELECT
							name
							FROM
							projects pr
							WHERE
							pr.id = '.$project_id.'';
	$result_get_project_name_sql = mysqli_query($connection, $get_project_name_sql);
	$row_project_name = mysqli_fetch_assoc($result_get_project_name_sql);
	return $row_project_name['name'];
	
}

/**
	* Method Name : get_invoice_prefix
	* Desc : Used to get invoice prefix from projects table
	* @access public
	* @param integer $project_id project id
	* @param Object $connection Mysqli connection object
	* @return string
**/
function get_invoice_prefix($project_id, $connection)
{
	$get_invoice_prefix_sql = 'SELECT
								invoice_prefix
								FROM
								projects pr
								WHERE
								pr.id = '.$project_id.'';
	$result_get_invoice_prefix_sql = mysqli_query($connection, $get_invoice_prefix_sql) or die($get_invoice_prefix_sql);
	$row_invoice_prefix = mysqli_fetch_assoc($result_get_invoice_prefix_sql);
	return $row_invoice_prefix['invoice_prefix'];
}

/**
	* Method Name : get_last_insert_id
	* Desc : Used to get last inserted row id
	* @access public
	* @param integer $project_id project id
	* @param Object $connection Mysqli connection object
	* @return integer
**/
function get_last_insert_id($project_id , $connection)
{
	$get_prev_order_sql =   'SELECT
							id
							FROM
							orders
							ORDER BY id DESC
							LIMIT 1 ';
	$result_prev_order_sql = mysqli_query($connection, $get_prev_order_sql);
	if(mysqli_num_rows($result_prev_order_sql) > 0)
	{
		$row_prev = mysqli_fetch_assoc($result_prev_order_sql);
		$check_empty_sql = 'SELECT *
							FROM orders
							where id = '.$row_prev['id'].'';
		
		$result_check_empty_sql = mysqli_query($connection, $check_empty_sql);
		$row_empty = mysqli_fetch_assoc($result_check_empty_sql);
		//Check for empty fields #2
		if (is_null ($row_empty['total_amount']) &&
		   is_null($row_empty['first_ Name']) &&
		   is_null($row_empty['last_Name']) &&
		   is_null($row_empty['address']) &&
		   is_null($row_empty['city']) &&
		   is_null($row_empty['country']) &&
		   is_null($row_empty['state']) &&
		   is_null($row_empty['zip_code']) &&
		   is_null($row_empty['email']) &&
		   is_null($row_empty['exp_date']) &&
		   is_null($row_empty['transaction_id']) &&
		   is_null($row_empty['invoice_num']) &&
		   is_null($row_empty['purchase_order_number']) &&
		   is_null($row_empty['authorization_code']) &&
		   is_null($row_empty['avs_response']) &&
		   is_null($row_empty['card_code_response']) &&
		   is_null($row_empty['cavv_response']) &&
		   is_null($row_empty['card_type']) &&
		   is_null($row_empty['status']) &&
		   is_null($row_empty['response_code']) &&
		   is_null($row_empty['response_reason_text']) 
		   )
		{
			
		}
		//Else insert blank row #2
		else
		{
			$insert_sql = 'INSERT INTO orders VALUES()';
			$result_insert_sql = mysqli_query($connection, $insert_sql);
		}
		$get_last_id_sql = 'SELECT
							id
							FROM
							orders
							ORDER BY id DESC
							LIMIT 1 ';
		$result_get_last_id_sql  = mysqli_query($connection, $get_last_id_sql);
		if (!$result_get_last_id_sql)
		{
			//echo $get_last_id_sql;
			//die("Database get last id failed.");
		}
		$row_last = mysqli_fetch_assoc($result_get_last_id_sql);
		return $row_last['id'];
	}
	else
	{
		return 0;
	}
	

}
function get_first_insert_id($connection)
{
	
	$insert_sql = 'INSERT INTO orders VALUES()';
	$result_insert_sql = mysqli_query($connection, $insert_sql);
	$get_first_id_sql = 'SELECT
							id
							FROM
							orders
							ORDER BY id DESC
							LIMIT 1 ';
	$result_get_first_id_sql  = mysqli_query($connection, $get_first_id_sql);
	if(!$result_get_first_id_sql)
	{
		die("first id failed");
	}
	$row_first = mysqli_fetch_assoc($result_get_first_id_sql);
		return $row_first['id'];
}

/**
	* Method Name : get_invoice_num_start
	* Desc : Used to get initialized invoice number from projects table
	* @access public
	* @param integer $project_id project id
	* @param Object $connection Mysqli connection object
	* @return integer
**/
function get_invoice_num_start($project_id , $connection)
{
	$invoice_sql = 'SELECT
					invoice_num_start
					FROM
					projects pr
					WHERE
					pr.id = '.$project_id.'';
	$result_invoice = mysqli_query($connection, $invoice_sql);
	$row_invoice = mysqli_fetch_assoc($result_invoice);
	return $row_invoice['invoice_num_start'];
}

/**
	* Method Name : get_sales_tax
	* Desc : Used to get sales tax from projects table
	* @access public
	* @param integer $project_id project id
	* @param Object $connection Mysqli connection object
	* @return float
**/
function get_sales_tax($project_id, $state_abbr, $connection)
{
	'SELECT * FROM project_states_tax_rates WHERE tax_project_id = '.$project_id.' AND tax_state_abbr = "'.$state_abbr.'"';
	$get_sales_tax_query = 'SELECT * FROM project_states_tax_rates WHERE tax_project_id = '.$project_id.' AND tax_state_abbr = "'.$state_abbr.'"';
	$result = mysqli_query($connection,$get_sales_tax_query);
	$row = mysqli_fetch_assoc($result);
	return $row['tax_rate'];
		
}

/**
	* Method Name : total_tax
	* Desc : Used to get total tax
	* @access public
	* @param float $sales_tax sales tax
	* @param Object $connection Mysqli connection object
	* @return float
**/
function total_tax($sales_tax, $connection)
{
	$total_tax = 0.00;
	//checking empty session #3
	if (!empty($_SESSION['cart']))
	{
		$max=count($_SESSION['cart']);
		//looping through the session #3
		for ($i=0;$i<$max;$i++)
		{
			$pid=$_SESSION['cart'][$i]['productid'];
			$q=$_SESSION['cart'][$i]['qty'];
			$price_type = $_SESSION['cart'][$i]['price_type'];
			$post_select1 = 'prd_'.$pid;
			$last_selected = $_SESSION['cart'][$i]['last_selected'];
			//echo $pid.'<br>';
			//checking if select options are present on session #4
			if (isset($_SESSION['cart'][$i][$post_select1]) && !empty($_SESSION['cart'][$i][$post_select1]))
			 {
				
				$post_select_session = $_SESSION['cart'][$i][$post_select1];
				$p_price=get_price($pid,$q,$price_type,$post_select_session,$connection,$last_selected);
			 }
			 //Else put null instead of select option array #4
			 else
			 {
				$p_price=get_price($pid,$q,$price_type,null,$connection,$last_selected);
			 }
			 $total = number_format((float)$p_price*$q, 2, '.', '');
			$taxable = is_taxable($pid,$connection);
			//checking for taxable products #5
			if ($taxable)
			{
				$total_tax += $total * $sales_tax;
				$total_tax = number_format((float)$total_tax,2, '.', '');
			}
			else
			{
				$total_tax += 0.00;
			}
		}
			
	}
	return $total_tax;
}

/**
	* Method Name : is_taxable
	* Desc : Used to check the taxable items from the table
	* @access public
	* @param integer $p_usage_id project_brand_product_usage_id
	* @param Object $connection Mysqli connection object
	* @return boolean
**/
function is_taxable($p_usage_id, $connection)
{
	$taxable_query = 'SELECT
					taxable,
					taxable_or
					FROM brand_product_usages bpu
					LEFT JOIN project_brand_product_usages pbpu on  bpu.id = pbpu.brand_product_usage_id
					WHERE
					pbpu.brand_product_usage_id = ' . $p_usage_id . '
					AND
					pbpu.project_id = ' .$_SESSION['enrollment']['project_id'] . '  
					AND pbpu.active = 1';
	$result=mysqli_query($connection,$taxable_query);
	$row = mysqli_fetch_assoc($result);
	//if product will be taxable in both brand_product_usage table and project_brand_product_usage table #6
	if ($row['taxable_or'] == '1')
	{
		return true ;
	}
	else
	{
		return false;
	}
}

/**
	* Method Name : get_option_name
	* Desc : Used to get product option name from the table
	* @access public
	* @param Integer $p_usage_id project_brand_product_usage_id
	* @param Array $post_select_session select options
	* @param Object $connection Mysqli connection object
	* @return string
**/
function get_option_name($p_usage_id, $post_select_session, $connection)
{
	$name = '';
	foreach($post_select_session AS $key => $value)
	{
		$q_options = 'SELECT bpuo.*, pot.name AS pOption  
		    FROM brand_product_usage_options bpuo
            LEFT JOIN product_option_types pot ON bpuo.product_option_type_code = pot.code
            WHERE bpuo.brand_product_usage_id = '.$p_usage_id.'
            AND bpuo.id = '.$value[0].'
            AND bpuo.product_option_type_code = '. $key .' 
            AND (bpuo.effective_date <= CURDATE() AND (bpuo.end_date is NULL or bpuo.end_date >= CURDATE() )) '; 
            
		$result=mysqli_query($connection,$q_options);
		
		$row=mysqli_fetch_assoc($result);
		$name.= $row['pOption'].':'.'&nbsp;'.$row['option_value'].'<br/>';
			
	}
	return $name;
		
}

/**
	* Method Name : get_product_name
	* Desc : Used to get product name from the table
	* @access public
	* @param Integer $p_usage_id project_brand_product_usage_id
	* @param Object $connection Mysqli connection object
	* @return string
**/
function get_product_name($p_usage_id, $connection)
{
	$query = 'SELECT 
		bpu.name AS project_brand_product_name
		FROM project_brand_product_usages pbpu 
		LEFT JOIN brand_product_usages bpu on pbpu.brand_product_usage_id = bpu.id 
		WHERE
		pbpu.brand_product_usage_id = ' . $p_usage_id . '
		AND
		pbpu.project_id = ' .$_SESSION['enrollment']['project_id'] . '      
		AND pbpu.active = 1';
	$result=mysqli_query($connection,$query);
	$row=mysqli_fetch_assoc($result);
	return  $row['project_brand_product_name'];
}
	
/**
	* Method Name : get_product_image
	* Desc : Used to get product image from the table
	* @access public
	* @param Integer $p_usage_id project_brand_product_usage_id
	* @param Object $connection Mysqli connection object
	* @return string
**/
function get_product_image($p_usage_id, $connection)
{
	$query = 'SELECT 
		im.host_url AS project_brand_product_image_small_url
		FROM project_brand_product_usages pbpu 
		LEFT JOIN brand_product_usages bpu on pbpu.brand_product_usage_id = bpu.id		
		LEFT JOIN images im ON (bpu.image_id = im.content_asset_id 
			/* AND im.format = "JPG" */
			AND im.size = "Product" 
			AND im.usage_size = "Small" 
			/*AND im.usage_shape = "Portrait"*/)
		WHERE pbpu.brand_product_usage_id = ' . $p_usage_id . ' 
			AND pbpu.project_id = ' .$_SESSION['enrollment']['project_id'] . ' 
			AND pbpu.active = 1
		ORDER BY im.format DESC ,im.usage_shape DESC
		LIMIT 1';
	$result=mysqli_query($connection,$query);
	$row=mysqli_fetch_assoc($result);
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
        $row['project_brand_product_image_small_url'] = str_replace('http://', 'https://', $row['project_brand_product_image_small_url']);
    }   	
	return $row['project_brand_product_image_small_url'];
}

/**
	* Method Name : get_price
	* Desc : Used to get product price from the table
	* @access public
	* @param Integer $p_usage_id project_brand_product_usage_id
	* @param Integer $quantity quantity of item
	* @param String $price_type price type code
	* @param Array $post_select_session select options array
	* @param Object $connection Mysqli connection object
	* @return float
**/
function get_price($p_usage_id, $quantity,$price_type, $post_select_session , $connection, $last_sel = null)
{
	//print_r($post_select_session);

	$sql = 'select getPrice('.$p_usage_id.','.$quantity.','.$last_sel.','.$_SESSION['enrollment']['project_id'].',"'.$price_type.'") as price';
	
	//error_log($sql);

	$result = mysqli_query($connection, $sql);
	if(!$result)
	{
		$callers=debug_backtrace();
		echo 'From: '.$callers[1]['function'].'<br>';

		echo mysqli_error($connection);
	}
	$row = mysqli_fetch_assoc($result);
	if(isset($row['price']))
	{
		$price = $row['price'];
	}
	mysqli_free_result($result);
	//return $price;
	return number_format((float)$price, 2, '.', '');

	/*
	$query = 'SELECT price
		FROM project_brand_product_usage_prices
		WHERE
		brand_product_usage_id = ' . $p_usage_id . '
		AND
		pbpu_project_id = ' .$_SESSION['enrollment']['project_id'] . '     
		AND
		price_type_code = "'.$price_type.'"
		AND
		((quantity_low_range <= '.$quantity.' AND (quantity_high_range >= '.$quantity.' OR
		quantity_high_range IS NULL)) OR (quantity_low_range IS NULL AND quantity_high_range IS NULL))';
		
		//echo $query.'<br><br>';
	$result=mysqli_query($connection, $query);
	if(!$result)
	{
		echo mysqli_error($connection);
	}
	$row=mysqli_fetch_assoc($result);
	if(isset($row['price']))
	{
		$price = $row['price'];
	}
	else
	{
		$price = 0.00;
	}
	//if select option array is not null #7	
	if (!is_null($post_select_session))
	{
	//loop through each element in array #7		
		foreach($post_select_session as $arr)
		{
			$query_options ='SELECT price
					 FROM project_brand_product_usage_prices
					 WHERE
					 bpuo_id = ' . $arr[0] .'
					 AND
					 brand_product_usage_id = ' . $p_usage_id . '
					 AND
					 pbpu_project_id = ' .$_SESSION['enrollment']['project_id'] . '  
					 AND
					 price_type_code = "'.$price_type.'" 
					 AND
					((quantity_low_range <= '.$quantity.' AND (quantity_high_range >= '.$quantity.' OR
					quantity_high_range IS NULL)) OR (quantity_low_range IS NULL AND quantity_high_range IS NULL))';
			//echo $query_options;
			$result_query_options=mysqli_query($connection,$query_options);
			$row_options=mysqli_fetch_assoc($result_query_options);
			$price = $row_options['price'];
						 
		}
	}
	return number_format((float)$price, 2, '.', '');
	*/
}

/**
	* Method Name : get_order_total
	* Desc : Used to get total price of all items added to the cart
	* @access public
	* @param Object $connection Mysqli connection object
	* @return float
**/
function get_order_total($connection)
{
		
		
		$max = isset($_SESSION['cart'])?  count($_SESSION['cart']) : 0;
		
		$sum = 0;
		for($i = 0; $i < $max; $i++)
		{
			$pid = $_SESSION['cart'][$i]['productid'];
			$q = $_SESSION['cart'][$i]['qty'];
			$price_type = $_SESSION['cart'][$i]['price_type'];
			$post_select1 = 'prd_'.$pid;
			$last_selected = $_SESSION['cart'][$i]['last_selected'];
			
			if (isset($_SESSION['cart'][$i][$post_select1]) && !empty($_SESSION['cart'][$i][$post_select1]))
			{
				
			   $post_select_session = $_SESSION['cart'][$i][$post_select1];
			   $price=get_price($pid,$q,$price_type,$post_select_session,$connection,$last_selected);
			}
			else
			{
				echo 'second condition record what happened';
			   $price=get_price($pid,$q,$price_type,null,$connection,$last_selected);
			}
		   $sum+=$price*$q;
		}
		$_SESSION['coupon']['product_sub_total_less_discount'] = $sum;
		return number_format((float)$sum, 2, '.', '');
	
}

/**
	* Method Name : remove_product
	* Desc : Used to remove item on cart
	* @access public
	* @param Integer $pid item idex no. on cart
	* @return void
**/
function remove_product($pid){
	//$pid=intval($pid);
	if (isset($_SESSION['cart'][$pid]))
	{
	  unset($_SESSION['cart'][$pid]);
	}
	//start array index from 0 after deletion
	$_SESSION['cart']=array_values($_SESSION['cart']);
}

/**
	* Method Name : remove_product_json
	* Desc : Used to remove item on cart
	* @access public
	* @param Integer $pid item idex no. on cart
	* @return boolean
**/
function remove_product_json($pid)
{
	if (isset($_SESSION['cart'][$pid]))
	{
		unset($_SESSION['cart'][$pid]);
	}
	//start array index from 0 after deletion
	$_SESSION['cart']=array_values($_SESSION['cart']);
	return 1;
}

/**
	* Method Name : update_quantity
	* Desc : Used to update quantity of each item on cart
	* @access public
	* @param Integer $pid item idex no. on cart
	* @param Integer $q quantity of each item on cart
	* @return void
**/
function update_quantity($pid,$q)
{
	$pid=intval($pid);
	$_SESSION['cart'][$pid]['qty']=$q;
	
}

/**
	* Method Name : addtocart
	* Desc : Used to add items on to the cart
	* @access public
	* @param Integer $pid project_brand_product_usage_id
	* @param Integer $q quantity of item
	* @param Array $select_arr select options array
	* @return void
**/	
function addtocart($pid, $q, $select_arr)
{

	//if items are present in session #9
	if (isset($_SESSION['cart']) && is_array($_SESSION['cart']))
	{
		//if product exists ,then do nothing #10
		if (product_exists($pid,$q,$select_arr)) return;
		$max=count($_SESSION['cart']);
		$_SESSION['cart'][$max]['productid']=$pid;
		$_SESSION['cart'][$max]['qty']=$q;
		$post_select = 'prd_'.$pid;
		if (!is_null($select_arr))
		{
			$_SESSION['cart'][$max][$post_select]= $select_arr;
		}
	}
	//Else not present in session
	else
	{
		$_SESSION['cart']=array();
		$_SESSION['cart'][0]['productid']=$pid;
		$_SESSION['cart'][0]['qty'] = $q;
		$post_select = 'prd_'.$pid;
		if (!is_null($select_arr))
		{
			$_SESSION['cart'][0][$post_select]= $select_arr;
		}
		
		
	}
}

/**
	* Method Name : product_exists
	* Desc : Used to check whether added item is already present on the cart  or not
	* @access public
	* @param Integer $pid project_brand_product_usage_id
	* @param Integer $q quantity of item
	* @param Array $select_arr select options array
	* @return boolean
**/	
function product_exists($pid ,$q ,$select_arr)
{
	$pid=intval($pid);
	$max=count($_SESSION['cart']);
	$flag=0;
	for($i=0;$i<$max;$i++)
	{
		//if item product id is present in session #11
		if ($pid == $_SESSION['cart'][$i]['productid'])
		{
			//$_SESSION['cart'][$i]['qty']=$q;
			$post_select = 'prd_'.$pid;
			if (!is_null($select_arr))
			{
				//if same quantity and select options are present in sesssion #12
				if ($_SESSION['cart'][$i]['qty'] == $q && $_SESSION['cart'][$i][$post_select] == $select_arr)
				{
						$flag = 1;
						break;
				}
				//if quantity of item to be added and quantity present in session is not same and select options to be added are same #13
				elseif($_SESSION['cart'][$i]['qty'] != $q && $_SESSION['cart'][$i][$post_select] == $select_arr)
				{
						//Update the quantity #13
						$_SESSION['cart'][$i]['qty']=$q;
						$flag = 1;
						break;
				}
			
			}
			else
			{
				//if quantity of item to be added is same as present in session #14
				if ($_SESSION['cart'][$i]['qty'] == $q)
				{
					$flag = 1;
						break;	
				}
				//Else Update the quantity #14
				else
				{
					$_SESSION['cart'][$i]['qty']=$q;
						$flag = 1;
						break;	
				}
			}
			
		}
	}
	return $flag;
		
}

/**
	* Method Name : get_project_email_data,
	* Desc : Used to get email_header,email_shipping_policy,email_refund_policy,email_footer from projects table
	* @access public
	* @param Integer $project_id project id
	* @param Object $connection Mysqli connection object
	* @return Arrray
**/	
function get_project_email_data($project_id, $connection)
{
	$get_project_email_sql = 'SELECT
							email_header,
							email_shipping_policy,
							email_refund_policy,
							email_footer
							FROM
							projects
							WHERE
							id= '.$project_id.'';
	$result_get_project_email_sql  = mysqli_query($connection, $get_project_email_sql);
	if (! $result_get_project_email_sql)
	{
		echo $get_project_email_sql;
		die("Database get billing sql failed.");
	}
	$row_project_email = mysqli_fetch_assoc($result_get_project_email_sql);
	return $row_project_email;
}
/**
	* Method Name : generate_order_html
	* Desc : Used to generate html template for email receipt being sent to customers as well as for view order page
	* @access public
	* @param Integer $order_id order id
	* @param Object $connection Mysqli connection object
	* @return String
**/	
function generate_order_html($order_id, $connection)
{
    $get_billing_sql = 'SELECT
						*
                        FROM
                        `orders`
                        WHERE id = '.$order_id.'';
    $result_get_billing_sql  = mysqli_query($connection, $get_billing_sql);
    if (! $result_get_billing_sql)
	{
		echo $get_billing_sql;
		die("Database get billing sql failed.");
	}
    $get_item_sql = '
                   SELECT
                   `id`,
                   `brand_product_usage_id`,
                   `quantity`,
                   `unit_price`,
                   `amount`
                    FROM
                    order_items
                    WHERE
                    orders_id = '.$order_id.' ';
    $result_get_item_sql = mysqli_query($connection, $get_item_sql);
    if (! $result_get_item_sql)
	{
		echo $get_item_sql;
		echo mysqli_error($connection);
		die("Database get item sql failed.");
	}
    
	$row_billing = mysqli_fetch_assoc($result_get_billing_sql);
	$product_subtotal_amount = $row_billing['product_subtotal_amount'];
	$discount_amount = $row_billing['discount_amount'];
    $total_tax = $row_billing['total_tax'];
    $total_shipping = $row_billing['shipping_total'];
    if($row_billing['shipping_option_type_name'] == '') $shipping_label = 'Shipping:';
    else $shipping_label = ' ('.$row_billing['shipping_option_type_name'].' - '.$row_billing['shipping_description'].'):';
    $total_amount = $row_billing['total_amount'];
    $project_name = get_project_name($row_billing['order_project_id'], $connection);
	list($header, $shipping_policy, $refund_policy, $footer) = array_values(get_project_email_data($row_billing['order_project_id'], $connection));
	$email = '';
	if ($header != '')
	{
		$email.= $header.'<br/>';
	}
    // $email.= '<font size="2"><strong>Thank you for your new order from '.' '.$project_name.'</strong><br/><br/>';
    $email.= '<div style="padding: 5px; background-color:grey; color: white;">Order Information</div><br/>';
    $email.= 'Order Number:'.'&nbsp;'.$row_billing['invoice_num'].'<br/>'.
            'Transaction ID#:'.'&nbsp;'.$row_billing['transaction_id'].'<br/>'.
            'Date / Time:'.'&nbsp;'.date("l - m/d/Y g:i A T ").'<br>
            Card Number:&nbsp;'.$row_billing['cc_mask'];
    if (!empty($row_billing['comments']))
    {
        $email.= '<br/>'.'Comment:'.'&nbsp;'.$row_billing['comments'];
    }
    if (!empty($row_billing['free_sample']))
    {
        $email.= '<br/>'.'Free Sample:'.'&nbsp;'.$row_billing['free_sample'];
    }

    $email.= 	'<br/>';
    $email.= 	'<table width="97%">
    				<tr>
    					<td>
						    <table style="font-size:12px" border="0" width="100%">
							    <tr><td colspan="2"><br/><strong>Billing Information</strong><hr><br/></td></tr>
								<tr><td width="65px">Name:&nbsp;</td><td>'.$row_billing['first_name'].' '.$row_billing['last_name'].'</td></tr>
								<tr><td>Address:&nbsp;</td><td>'.$row_billing['address'].'</td></tr>
								<tr><td>City:&nbsp;</td><td>'.$row_billing['city'].'</td></tr>
								<tr><td>Zip&nbsp;Code:&nbsp;</td><td>'.$row_billing['zip_code'].'</td></tr>
								<tr><td>State:&nbsp;</td><td>'.$row_billing['state'].'</td></tr>
								<tr><td>Country:&nbsp;</td><td>'.$row_billing['country'].'</td></tr>
								<tr><td>Email:&nbsp;</td><td>'.$row_billing['email'].'</td></tr>
								<tr><td>Phone:&nbsp;</td><td>'.$row_billing['phone'].'</td></tr>
						    </table>
						</td>
						<td style="width: 15px;">&nbsp;</td>
						<td>
						    <table style="font-size:12px" border="0" width="100%">
							    <tr><td colspan="2"><br/><strong>Shipping Information</strong><hr><br/></td></tr>
								<tr><td width="65px">Name:&nbsp;</td><td>'.$row_billing['shipping_first_name'].' '.$row_billing['shipping_last_name'].'</td></tr>
								<tr><td>Address:&nbsp;</td><td>'.$row_billing['shipping_address'].'</td></tr>
								<tr><td>City:&nbsp;</td><td>'.$row_billing['shipping_city'].'</td></tr>
								<tr><td>Zip&nbsp;Code:&nbsp;</td><td>'.$row_billing['shipping_zip_code'].'</td></tr>
								<tr><td>State:&nbsp;</td><td>'.$row_billing['shipping_state'].'</td></tr>
								<tr><td>Country:&nbsp;</td><td>'.$row_billing['shipping_country'].'</td></tr>
								<tr><td>Email:&nbsp;</td><td>'.$row_billing['shipping_email'].'</td></tr>
								<tr><td>Phone:&nbsp;</td><td>'.$row_billing['shipping_phone'].'</td></tr>
						    </table>
						</td>
					</tr>
				</table><br>';
	//echo $email;
    $email.= '<hr><br><table width="90%" align="center" border="0" style="font-size:12px";>'.
	            '<tr>'.
		            '<td style="text-align: center;"><b>Item</b><hr></td>'.
		            '<td style="text-align: center;">&nbsp;</td>'.
		            '<td style="text-align: left;"><b>Description</b><hr></td>'.
		            '<td style="text-align: center;">&nbsp;</td>'.
		            '<td style="text-align: right;"><b>Qty</b><hr></td>'.
		            '<td style="text-align: center;">&nbsp;</td>'.
		            '<td style="text-align: right;"><b>Unit Price</b><hr></td>'.
		            '<td style="text-align: center;">&nbsp;</td>'.
		            '<td style="text-align: right;"><b>Item Total</b><hr></td>'.
	            '</tr>';
    $counter = 1;
    while($row_item = mysqli_fetch_assoc($result_get_item_sql))
    {
        $get_option_sql = 'SELECT
                            `name`,
                            `order_option_type_code`,
                            `order_option_name`
                            FROM
                            order_item_options
                            LEFT JOIN product_option_types ON code = order_option_type_code
                            WHERE
                            order_item_id = '.$row_item['id'].' ';
        
        $result_get_option_sql = mysqli_query($connection, $get_option_sql);
        if (! $result_get_option_sql)
        {
            echo $get_option_sql;
            die("Database get item option failed.");
        }
        $pid = $row_item['brand_product_usage_id'];
        $total_price= $row_item['amount'];
        $quantity = $row_item['quantity'];
        $pname = get_product_name($pid,$connection);
        $taxable = is_taxable($pid,$connection);
        $unit_price = $row_item['unit_price'];
        if ($taxable)
        {
            $is_taxable = 'Y';
			$tax_code = '<tr><td colspan="7" style="text-align: right;">State Sales Tax: </td><td colspan="2" style="text-align: right;">$'.$total_tax.'</td></tr>';
        }
        else
        {
            $is_taxable = 'N';
            $tax_code = '';
        }
        $email.= 	'<tr>
        				<td style="text-align: center;">'.$counter.'</td>
        				<td>&nbsp;</td>
        				<td><b>'.$pname.'</b><br>';
				        
				        while ($row_option = mysqli_fetch_assoc($result_get_option_sql)) {
				            $email.= $row_option['name'].':'.'&nbsp;'.$row_option['order_option_name'].'<br/>';
				        }
		$email.= 		'</td>
						<td>&nbsp;</td>
						<td style="text-align: right;">'.$quantity.'</td>'.
				        '<td>&nbsp;</td>
				        <td style="text-align: right;">$'. $unit_price.'</td>'.
				        '<td>&nbsp;</td>
				        <td style="text-align: right;">$'.$total_price.'</td>
				    </tr>';
				    $counter++;
    }
    
    if($discount_amount > 0) $discount_html = '<tr><td colspan="7" style="text-align: right;">Promotion Discount: </td><td style="text-align: right;" colspan="2">$'.$discount_amount.'</td></tr>';
    else $discount_html = '';
    
    $email.= '<tr><td colspan="9"><hr></td></tr>
    <tr><td colspan="7" style="text-align: right;">Order Subtotal: </td><td  colspan="2" style="text-align: right;">$'.$product_subtotal_amount.'</td></tr>'
    
    .$discount_html.
  
    $tax_code.
    '<tr><td colspan="7" style="text-align: right;">Shipping Option: '.$shipping_label.' </td><td colspan="2" style="text-align: right;">$'.$total_shipping.'</td></tr>
    <tr><td colspan="7" style="text-align: right;"><b>Order Total: </b></td><td colspan="2" style="text-align: right;"><b>$'.$total_amount.'</b></td></tr>';  
    $email.= '</table><br>';
    $email.= '&nbsp;</font>';
	if (isset($shipping_policy) && !empty($shipping_policy))
	{
		 $email.= '<font size="2">'.'<br/><div style="padding: 5px; background-color:grey; color: white;">Shipping Policy</div><br/>';
		 $email.= $shipping_policy.'</font>';
	}
	if (isset($refund_policy) && !empty($refund_policy))
	{
		$email.= '<font size="2">'.'<div style="padding: 5px; background-color:grey; color: white;">Refund Policy</div><br/>';
		$email.= $refund_policy.'</font>'.'<hr/>';
	}
	if (isset($footer) && !empty($footer))
	{
		$email.= '<font size="2">'.$footer.'</font>';
	}
    return $email;
}

/*
function getCustomerNumberOrders($connection, $order_status = '', $user_id = '', $order_created_timestamp = '', $last_name = '', $zip_code = '') {
    
    // Calculate Total # Orders for this Person
    $user_orders = 0;
    $revenue_order = array("1", "2", "3", "4");

    if (in_array($order_status, $revenue_order)) { 
    	// Revenue orders are processed for 1) Total Orders for that Customer, 2) Accounting Summary
    										
    	// #1 Total Customer Orders. Determine Total Orders for this Customer. Can be a Member or a Public User.
    	if ($user_id > 0) {
    		// Member Account
    		// echo $c['user_id'].'<br>';
    		
    		// Determine how many valid Orders this User has prior to and including this Order.	
    		$queryCustomerOrders = '
    			SELECT count(*) AS order_count 
    			FROM orders   
    			WHERE user_id = "'.$user_id.'"
    			AND created_timestamp <= "'.$order_created_timestamp.'"
    			AND order_status in (1,2,3,4)';
    													
    	} else {
    		// Public User (not a Member Account)
    		// echo 'Not a Member order<br>';
    		
    		// Determine how many valid Orders this User has prior to and including this Order.	
    		$queryCustomerOrders = '
    			SELECT count(*) AS order_count 
    			FROM orders   
    			WHERE last_name = "'.$last_name.'" 
    			AND zip_code = "'.$zip_code.'" 
    			AND created_timestamp <= "'.$order_created_timestamp.'"
    			AND order_status in (1,2,3,4)';
    		    		
    	}
    	
        // echo $queryCustomerOrders.'<br>';
        // die();
    			
    	// Execute the query
    	$result_order_count = mysqli_query($connection, $queryCustomerOrders);
    	
    	if (!$result_order_count) {
    		show_mysqli_error_message($queryCustomerOrders, $connection);
    		die;
    	}
    	
    	$oc = mysqli_fetch_assoc($result_order_count);
    	
    	$user_orders = $oc['order_count'];
    	
    	mysqli_free_result($result_order_count);	
    	
    	return $user_orders;
    	
    }
    
    return false;
    
}
*/

/**
	* Method Name : generate_customer_service_order_html
	* Desc : Used to generate html template for email receipt being sent to customer service
	* @access public
	* @param Integer $order_id order id
	* @param Object $connection Mysqli connection object
	* @return String
**/	

function generate_customer_service_order_html($order_id, $connection, $order_failed = false, $response_code = 0, $response_subcode = 0, $response_reason_code = 0)
{
    $get_billing_sql = 'SELECT
						*
                        FROM
                        `orders`
                        WHERE id = '.$order_id.'';
    $result_get_billing_sql  = mysqli_query($connection, $get_billing_sql);
    if (! $result_get_billing_sql)
	{
		echo $get_billing_sql;
		die("Database get billing sql failed.");
	}
    $get_item_sql = '
                   SELECT
                   `id`,
                   `brand_product_usage_id`,
                   `quantity`,
                   `unit_price`,
                   `amount`
                    FROM
                    order_items
                    WHERE
                    orders_id = '.$order_id.' ';
    $result_get_item_sql = mysqli_query($connection, $get_item_sql);
    if (! $result_get_item_sql)
	{
		echo $get_item_sql;
		echo mysqli_error($connection);
		die("Database get item sql failed.");
	}
    
 	$row_billing = mysqli_fetch_assoc($result_get_billing_sql);
 	
 	// Get Total Number Orders for this Customer
 	// Added by Jay 3/21/16
 	
    $user_orders = getCustomerNumberOrders($connection, $row_billing['order_status'], $row_billing['user_id'], $row_billing['created_timestamp'], $row_billing['last_name'], $row_billing['zip_code']);

     // Get User Session data. Add by Jay 12/23/15.
    $user_session_id = $row_billing['user_session_id'];
        
    $get_session_sql = 'SELECT 
            us.http_referer,
            us.remote_address,
            us.browser_platform,
            us.browser_device_type,
            us.browser_name,
            us.browser_version,
            us.login_timestamp, 
            pe.username, 
            pg.name AS target_page   
            FROM user_sessions us 
                LEFT JOIN persons pe ON us.user_id = pe.id 
                LEFT JOIN pages pg ON us.page_id = pg.id 
                WHERE us.id = '.$user_session_id.' ';
                
    $result_get_session_sql = mysqli_query($connection, $get_session_sql);
    if (! $result_get_session_sql)
	{
		echo $get_session_sql;
		echo mysqli_error($connection);
		die("Database get order user session sql failed.");
	}
	
    $row_user_session = mysqli_fetch_assoc($result_get_session_sql);
    
    $session_referring_uri = $row_user_session['http_referer'];
 
    $session_remote_address = $row_user_session['remote_address'];
    $session_browser_platform = $row_user_session['browser_platform'];
    $session_browser_device_type = $row_user_session['browser_device_type'];
    $session_browser_name = $row_user_session['browser_name'];
    $session_browser_version = $row_user_session['browser_version'];
    $session_login_timestamp = $row_user_session['login_timestamp'];
    
    if (strlen($session_login_timestamp) > 0) {
        $session_login_timestamp = date('m/d/y h:i A T', strtotime($session_login_timestamp));
    } else {  
       $session_login_timestamp = null;        
    }
         
    $session_target_page = $row_user_session['target_page'];
    $session_username = $row_user_session['username'];
    
    $queryTargetContent = 'SELECT 
         MIN(sl.log_id), 
         sl.branded_product_id, 
         sl.content_asset_id, 
         bpu.name AS brand_product_name, 
         ca.title AS content_asset_title  
         FROM system_logs sl 
         LEFT JOIN brand_product_usages bpu ON sl.branded_product_id = bpu.id 
         LEFT JOIN content_assets ca ON sl.content_asset_id = ca.id 
         WHERE sl.user_session_id = '.$user_session_id.' 
         LIMIT 1';
             
    // echo $queryTargetContent;
    // die();
    
    $resultTargetContent = mysqli_query($connection, $queryTargetContent);
    
    if (!$resultTargetContent) {
    	show_mysqli_error_message($queryTargetContent, $connection);
    	die;
    }
     
    $rtc = mysqli_fetch_assoc($resultTargetContent);
    
    $session_target_product = $rtc['brand_product_name'];
    $session_target_content = $rtc['content_asset_title'];
        
    mysqli_free_result($resultTargetContent);
 
	$product_subtotal_amount = $row_billing['product_subtotal_amount'];
	$discount_amount = $row_billing['discount_amount'];
    $total_tax = $row_billing['total_tax'];
    $total_shipping = $row_billing['shipping_total'];
    if($row_billing['shipping_option_type_name'] == '') $shipping_label = 'Shipping:';
    else $shipping_label = ' ('.$row_billing['shipping_option_type_name'].' - '.$row_billing['shipping_description'].'):';
    $total_amount = $row_billing['total_amount'];
    $project_name = get_project_name($row_billing['order_project_id'], $connection);
    
	// list($header, $shipping_policy, $refund_policy, $footer) = array_values(get_project_email_data($row_billing['order_project_id'], $connection));
	$email = '';
	
	/*
	if ($header != '')
	{
		$email.= $header.'<br/>';
	}
	*/
	
    // $email.= '<font size="2"><strong>Thank you for your new order from '.' '.$project_name.'</strong><br/><br/>';
	if($order_failed){    
    	$email.= '<div style="padding: 5px; background-color:grey; color: white;">Order <strong>FAILED</strong> Information</div><br/>';
    }else{
    	$email.= '<div style="padding: 5px; background-color:grey; color: white;">Order Information</div><br/>';
    }
    $email.= 'Order Number:'.'&nbsp;'.$row_billing['invoice_num'].'<br/>'.
            'Transaction ID#:'.'&nbsp;'.$row_billing['transaction_id'].'<br/>'.
            'Date / Time:'.'&nbsp;'.date("l - m/d/Y g:i A T ").'<br>
            Card Number:&nbsp;'.$row_billing['cc_mask'];
    
    if($order_failed){
    	$email.= '<br>Response Reason Text: <span style="color:red">'.$row_billing['response_reason_text'].'</span>';
    	$email.= '<br>Response Code: <span style="color:red">'.$response_code.'</span>';
    	$email.= '<br>Response Subcode: <span style="color:red">'.$response_subcode.'</span>';
    	$email.= '<br>Response Reason Code: <span style="color:red">'.$response_reason_code.'</span>';
    }

    if (!empty($row_billing['comments']))
    {
        $email.= '<br/>'.'Comment:'.'&nbsp;'.$row_billing['comments'];
    }
    
    if (!empty($row_billing['free_sample']))
    {
        $email.= '<br/>'.'Free Sample:'.'&nbsp;'.$row_billing['free_sample'];
    }

    $email.= 	'<br/>';
    $email.= 	'<table width="97%">
    				<tr>
    					<td>
						    <table style="font-size:12px" border="0" width="100%">
							    <tr><td colspan="2"><br/><strong>Billing Information</strong><hr><br/></td></tr>
								<tr><td width="65px">Name:&nbsp;</td><td>'.$row_billing['first_name'].' '.$row_billing['last_name'].'</td></tr>
								<tr><td>Address:&nbsp;</td><td>'.$row_billing['address'].'</td></tr>
								<tr><td>City:&nbsp;</td><td>'.$row_billing['city'].'</td></tr>
								<tr><td>Zip&nbsp;Code:&nbsp;</td><td>'.$row_billing['zip_code'].'</td></tr>
								<tr><td>State:&nbsp;</td><td>'.$row_billing['state'].'</td></tr>
								<tr><td>Country:&nbsp;</td><td>'.$row_billing['country'].'</td></tr>
								<tr><td>Email:&nbsp;</td><td>'.$row_billing['email'].'</td></tr>
								<tr><td>Phone:&nbsp;</td><td>'.$row_billing['phone'].'</td></tr>
						    </table>
						</td>
						<td style="width: 15px;">&nbsp;</td>
						<td>
						    <table style="font-size:12px" border="0" width="100%">
							    <tr><td colspan="2"><br/><strong>Shipping Information</strong><hr><br/></td></tr>
								<tr><td width="65px">Name:&nbsp;</td><td>'.$row_billing['shipping_first_name'].' '.$row_billing['shipping_last_name'].'</td></tr>
								<tr><td>Address:&nbsp;</td><td>'.$row_billing['shipping_address'].'</td></tr>
								<tr><td>City:&nbsp;</td><td>'.$row_billing['shipping_city'].'</td></tr>
								<tr><td>Zip&nbsp;Code:&nbsp;</td><td>'.$row_billing['shipping_zip_code'].'</td></tr>
								<tr><td>State:&nbsp;</td><td>'.$row_billing['shipping_state'].'</td></tr>
								<tr><td>Country:&nbsp;</td><td>'.$row_billing['shipping_country'].'</td></tr>
								<tr><td>Email:&nbsp;</td><td>'.$row_billing['shipping_email'].'</td></tr>
								<tr><td>Phone:&nbsp;</td><td>'.$row_billing['shipping_phone'].'</td></tr>
						    </table>
						</td>
					</tr>
				</table><br>';
	//echo $email;
    $email.= '<hr><br><table width="90%" align="center" border="0" style="font-size:12px";>'.
	            '<tr>'.
		            '<td style="text-align: center;"><b>Item</b><hr></td>'.
		            '<td style="text-align: center;">&nbsp;</td>'.
		            '<td style="text-align: left;"><b>Description</b><hr></td>'.
		            '<td style="text-align: center;">&nbsp;</td>'.
		            '<td style="text-align: right;"><b>Qty</b><hr></td>'.
		            '<td style="text-align: center;">&nbsp;</td>'.
		            '<td style="text-align: right;"><b>Unit Price</b><hr></td>'.
		            '<td style="text-align: center;">&nbsp;</td>'.
		            '<td style="text-align: right;"><b>Item Total</b><hr></td>'.
	            '</tr>';
    $counter = 1;
    while($row_item = mysqli_fetch_assoc($result_get_item_sql))
    {
        $get_option_sql = 'SELECT
                            `name`,
                            `order_option_type_code`,
                            `order_option_name`
                            FROM
                            order_item_options
                            LEFT JOIN product_option_types ON code = order_option_type_code
                            WHERE
                            order_item_id = '.$row_item['id'].' ';
        
        $result_get_option_sql = mysqli_query($connection, $get_option_sql);
        if (! $result_get_option_sql)
        {
            echo $get_option_sql;
            die("Database get item option failed.");
        }
        $pid = $row_item['brand_product_usage_id'];
        $total_price= $row_item['amount'];
        $quantity = $row_item['quantity'];
        $pname = get_product_name($pid,$connection);
        $taxable = is_taxable($pid,$connection);
        $unit_price = $row_item['unit_price'];
        if ($taxable)
        {
            $is_taxable = 'Y';
			$tax_code = '<tr><td colspan="7" style="text-align: right;">State Sales Tax: </td><td colspan="2" style="text-align: right;">$'.$total_tax.'</td></tr>';
        }
        else
        {
            $is_taxable = 'N';
            $tax_code = '';
        }
        $email.= 	'<tr>
        				<td style="text-align: center;">'.$counter.'</td>
        				<td>&nbsp;</td>
        				<td><b>'.$pname.'</b><br>';
				        
				        while ($row_option = mysqli_fetch_assoc($result_get_option_sql)) {
				            $email.= $row_option['name'].':'.'&nbsp;'.$row_option['order_option_name'].'<br/>';
				        }
		$email.= 		'</td>
						<td>&nbsp;</td>
						<td style="text-align: right;">'.$quantity.'</td>'.
				        '<td>&nbsp;</td>
				        <td style="text-align: right;">$'. $unit_price.'</td>'.
				        '<td>&nbsp;</td>
				        <td style="text-align: right;">$'.$total_price.'</td>
				    </tr>';
				    $counter++;
    }
    
    if($discount_amount > 0) $discount_html = '<tr><td colspan="7" style="text-align: right;">Promotion Discount: </td><td style="text-align: right;" colspan="2">$'.$discount_amount.'</td></tr>';
    else $discount_html = '';
    
    $email.= '<tr><td colspan="9"><hr></td></tr>
    <tr><td colspan="7" style="text-align: right;">Order Subtotal: </td><td  colspan="2" style="text-align: right;">$'.$product_subtotal_amount.'</td></tr>'
    
    .$discount_html.
  
    $tax_code.
    '<tr><td colspan="7" style="text-align: right;">Shipping Option: '.$shipping_label.' </td><td colspan="2" style="text-align: right;">$'.$total_shipping.'</td></tr>
    <tr><td colspan="7" style="text-align: right;"><b>Order Total: </b></td><td colspan="2" style="text-align: right;"><b>$'.$total_amount.'</b></td></tr>';  
    $email.= '</table><br>';
    // $email.= '&nbsp;</font>';
    
    /*
	if (isset($shipping_policy) && !empty($shipping_policy))
	{
		 $email.= '<font size="2">'.'<br/><div style="padding: 5px; background-color:grey; color: white;">Shipping Policy</div><br/>';
		 $email.= $shipping_policy.'</font>';
	}
	if (isset($refund_policy) && !empty($refund_policy))
	{
		$email.= '<font size="2">'.'<div style="padding: 5px; background-color:grey; color: white;">Refund Policy</div><br/>';
		$email.= $refund_policy.'</font>'.'<hr/>';
	}
	if (isset($footer) && !empty($footer))
	{
		$email.= '<font size="2">'.$footer.'</font>';
	}
	*/
		
	$email.= 
	'<span style="font-size:14px">
    <hr><br>
    Total Orders: '.$user_orders.'<br>
    Remote Address: '.$session_remote_address.'<br>
	Referring URI: '.$session_referring_uri.'<br>
    Target Page: '.$session_target_page.'<br>
    Product: '.$session_target_product.'<br>
    Content: '.$session_target_content.'<br>
    Browser Platform: '.$session_browser_platform.'<br>
	Browser Device: '.$session_browser_device_type.'<br>
	Browser Name: '.$session_browser_name.'<br>
	Browser Version: '.$session_browser_version.'<br>
	Username: '.$session_username.'<br>
	Login: '.$session_login_timestamp.'<br>
	</span>';
	
    return $email;
}

/**
	* Method Name : safe_data
	* Desc : Used to secure input data
	* @access public
	* @param String $param input data
	* @return string
**/
/*
function safe_data($param)
{
		return stripslashes(htmlspecialchars(trim($param)));
		
}
*/

/**
	* Method Name : safe_sql_data
	* Desc : Used to escape input data for preventing sql injection
	* @access public
	* @param Object $connection Mysqli connection object
	* @param String $param input data
	* @return string
**/

/*
function safe_sql_data($connection, $param)
{
		return mysqli_real_escape_string($connection, stripslashes(htmlspecialchars(trim($param))));
		
}
*/

/**
	* Method Name : get_shipping_cost
	* Desc : Used to get shipping amount for Checkout 
	* @access public
	* @return string
**/
function get_shipping_cost($id, $connection)
{ 
	if($id != '') {
		$zq = 'SELECT * FROM project_shipping_options WHERE id = '.$id.' AND project_id = '.$_SESSION['enrollment']['project_id'].' AND active = 1 AND effective_date <= CURDATE() AND (end_date is NULL or end_date > CURDATE()) AND price_type_code = "'.$_SESSION['enrollment']['user_price_type'].'"';
		$zr = mysqli_query($connection, $zq);
		$zrow = mysqli_fetch_assoc($zr);
		if($_SESSION['coupon']['product_sub_total_less_discount'] <= $zrow['minimum_purchase_amount']) {
			$_SESSION['checkout']['shipping_total'] = $zrow['alternative_charge_amount'];
			return $zrow['alternative_charge_amount'];
		}else{
			$_SESSION['checkout']['shipping_total'] = $zrow['charge_amount'];
			return $zrow['charge_amount'];
		}
	
	}else{
		$_SESSION['checkout']['shipping_total'] = 0;
		return 0;
	}
}

function pFormat($number)
{
	return number_format((float)$number,2, '.', '');
}

function cc_mask($number)
{
	$str = $number;
	$masked = preg_replace("/(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})/", "XXXXXXXX", $str);
	return $masked;
}


function get_coupon_code_discount($connection)
{ 
	$_SESSION['coupon']['associatedProducts']['products'] = "";
	$_SESSION['coupon']['coupon_code_message'] = "";
	if(empty($_SESSION['coupon']['coupon_code'])) {
		$_SESSION['coupon']['coupon_code_id'] = '';
		$_SESSION['coupon']['coupon_code_message'] = '<div class="alert-box alert radius">You must enter a Promo/Coupon Code first.</div>';
		return 0;
	}
	
	
	//$productId = get_product_id_from_cart();

	/*$zq = 
		'SELECT 
		pscp.project_brand_product_usage_id, 
		pscp.project_shopping_code_id,
		bpu.id AS bpu_id,
		psc.* 
		FROM project_shopping_code_products pscp 
		JOIN project_brand_product_usages pbpu 
		ON pscp.project_brand_product_usage_id = pbpu.id 
		JOIN brand_product_usages bpu
		ON pbpu.brand_product_usage_id = bpu.id 
		JOIN project_shopping_codes psc 
		ON  pscp.project_shopping_code_id = psc.id
		WHERE bpu.id IN '.$productId.
		'AND psc.shopping_code = "'.$_SESSION['coupon']['coupon_code'].
		'" AND psc.project_id = '.$_SESSION['enrollment']['project_id'].
		' AND psc.active = 1 
		AND psc.effective_date <= CURDATE() 
		AND (psc.end_date is NULL or psc.end_date > CURDATE()) 
		AND psc.price_type_code = "'.$_SESSION['enrollment']['user_price_type'].'"';

		$zr = mysqli_query($connection, $zq);
		
		$discount = 0;
		if(mysqli_num_rows($zr) > 0)
		{
			while($usageId = mysqli_fetch_assoc($zr))
			{
				$usageId_minimum_purchase_amount = $usageId['minimum_purchase_amount'];
				foreach ($_SESSION['cart'] as $key2 => $cart) 
				{ 
					if($usageId['bpu_id'] == $cart['productid'])
					{
						if($usageId['absolute_amount']>0 && ($_SESSION['coupon']['product_sub_total_less_discount']>=$usageId['minimum_purchase_amount']) )
						{
								$discount = $discount + ($usageId['absolute_amount'] * $cart['qty']);
						}
						else if( $usageId['percentage_amount'] > 0 && 
								($_SESSION['coupon']['product_sub_total_less_discount'] >= $usageId['minimum_purchase_amount'])) 
						{
							$pid = $cart['productid'];
							$q = $cart['qty'];
							$price_type = $cart['price_type'];
							$post_select1 = 'prd_'.$pid;
							$last_selected = $cart['last_selected'];
							
							if (isset($cart[$post_select1]) && !empty($cart[$post_select1]))
							{
								
								$post_select_session = $_cart[$post_select1];
							    $price=get_price($pid,$q,$price_type,$post_select_session,$connection,$last_selected);
							}
							else
							{
								echo 'second condition record what happened';
							    $price=get_price($pid,$q,$price_type,null,$connection,$last_selected);
							}
						    $sum=$price*$q;
						    $discount_total = round($sum * $usageId['percentage_amount'], 2);
							$discount = $discount + $discount_total;
						}
						break;
					}
				}
			}

			if ($discount > 0)
			{
				$_SESSION['coupon']['coupon_code_discount'] = $discount;
				$_SESSION['coupon']['coupon_code_message'] = '<div class="alert-box success radius">Discount has been applied successfully</div>';	
				return $discount;
			}
			else
			{
				$_SESSION['coupon']['coupon_code_discount'] = 0;
				$_SESSION['coupon']['coupon_code_message'] = '<div class="alert-box alert radius">Product Subtotal does not meet minimum purchase amount of <strong>$'.pFormat($usageId_minimum_purchase_amount).'</strong> to qualify for discount.</div>';
				return 0;
			}
		}
		else
		{
			$_SESSION['coupon']['coupon_code_id'] = '';
			$_SESSION['coupon']['coupon_code_discount'] = 0;
			$_SESSION['coupon']['coupon_code_message'] = '<div class="alert-box alert radius">Invalid Promo/Coupon Code. Try again.</div>';
			return 0;
		}
	*/

	$zq = 
		'SELECT 
		psc.id, psc.product_specific, psc.absolute_amount, psc.percentage_amount, psc.minimum_purchase_amount,
		sct.code 
		FROM project_shopping_codes psc 
		LEFT JOIN shopping_code_types sct 
		ON psc.shopping_code = sct.code 
		WHERE psc.shopping_code = "'.$_SESSION['coupon']['coupon_code'].
		'" AND psc.project_id = '.$_SESSION['enrollment']['project_id'].
		' AND psc.active = 1 
		AND psc.effective_date <= CURDATE() AND (psc.end_date is NULL or psc.end_date > CURDATE()) 
		AND psc.price_type_code = "'.$_SESSION['enrollment']['user_price_type'].'" 
		LIMIT 1';
		
	$zr = mysqli_query($connection, $zq);

	$discount = 0;
	$tempDiscount = 0;
	$totalPriceOfQualifyingProducts = 0;
	if(mysqli_num_rows($zr) == 1) 
	{
		$flag = 0;
		$usageId = mysqli_fetch_assoc($zr);
		
		if ($usageId['product_specific'] == 1) 
		{
			$query =
			'SELECT 
			project_brand_product_usage_id
			FROM project_shopping_code_products
			WHERE active = 1 
			AND effective_timestamp <= CURDATE() 
			AND (end_timestamp is NULL or end_timestamp >= CURDATE())
			AND project_shopping_code_id = '.$usageId['id'];

			$fetchQuery = mysqli_query($connection, $query);

			while($resultQuery = mysqli_fetch_assoc($fetchQuery))
			{
				foreach ($_SESSION['cart'] as $key => $cart) 
				{
					if($resultQuery['project_brand_product_usage_id'] == $cart['productid'])
					{
						$flag = 1;
						$pid = $cart['productid'];
						$q = $cart['qty'];
						$price_type = $cart['price_type'];
						$post_select1 = 'prd_'.$pid;
						$last_selected = $cart['last_selected'];
						$post_select_session = $_cart[$post_select1];
					    $price=get_price($pid,$q,$price_type,$post_select_session,$connection,$last_selected);
						$totalPriceOfQualifyingProducts += ($price*$q);
						

						if($usageId['absolute_amount'] > 0 )
						{
								$tempDiscount = $tempDiscount + ($usageId['absolute_amount'] * $cart['qty']);
						}
						else if( $usageId['percentage_amount'] > 0 ) 
						{
							$tempDiscount = round(($totalPriceOfQualifyingProducts * $usageId['percentage_amount']), 2);
						}

						break;

					}
				}
			}
			if($totalPriceOfQualifyingProducts >= $usageId['minimum_purchase_amount'])
			{
				/*$fetchQuery2 = mysqli_query($connection, $query);
				while($resultsQuery = mysqli_fetch_assoc($fetchQuery2))
				{
					foreach ($_SESSION['cart'] as $key => $cart) 
					{
						if($resultsQuery['project_brand_product_usage_id'] == $cart['productid'])
						{
							if($usageId['absolute_amount'] > 0 )
							{
									$discount = $discount + ($usageId['absolute_amount'] * $cart['qty']);
							}
							else if( $usageId['percentage_amount'] > 0 ) 
							{
								$discount = round(($totalPriceOfQualifyingProducts * $usageId['percentage_amount']), 2);
							}
							break;
						}
					}
				}*/
				$discount = $tempDiscount;
			}
			if ( ($flag == 0) && ($discount == 0) )
			{
				$_SESSION['coupon']['coupon_code_id'] = '';
				$_SESSION['coupon']['coupon_code_discount'] = 0;
			
				$associatedProducts = get_associated_products($connection);
				$_SESSION['coupon']['coupon_code_message'] = 
					'<div class="alert-box alert radius">
						<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
						Promotion Code is for qualifying products including '.$associatedProducts.'. Please add at least one of these items to your shopping cart.</div>';				
				return 0;
			}
		} 
		else 
		{
			$flag = 1;
			if($usageId['absolute_amount'] > 0 && 
				($_SESSION['coupon']['product_sub_total_less_discount'] >= $usageId['minimum_purchase_amount']) )
			{
					foreach ($_SESSION['cart'] as $key => $cart) 
					{
						$discount = $discount + ($usageId['absolute_amount'] * $cart['qty']);
					}
			}
			else if( $usageId['percentage_amount'] > 0 && 
					($_SESSION['coupon']['product_sub_total_less_discount'] >= $usageId['minimum_purchase_amount'])) 
			{
				$discount = round(($_SESSION['coupon']['product_sub_total_less_discount'] * $usageId['percentage_amount']), 2);
			}
						
		}

		if ($discount > 0)
		{
			$_SESSION['coupon']['coupon_code_discount'] = $discount;
			$_SESSION['coupon']['coupon_code_message'] = '<div class="alert-box success radius"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Discount has been applied successfully.</div>';
			return $discount;
		}
		else
		{
			$_SESSION['coupon']['coupon_code_discount'] = 0;
			$_SESSION['coupon']['coupon_code_message'] = '<div class="alert-box alert radius"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
		 	&nbsp;&nbsp;Product Subtotal does not meet minimum purchase amount of <strong>$'.pFormat($usageId['minimum_purchase_amount']).
		 	'</strong> to qualify for discount.</div>';
			return 0;
		}

	}
	else
	{
		$_SESSION['coupon']['coupon_code_id'] = '';
		$_SESSION['coupon']['coupon_code_discount'] = 0;
		$_SESSION['coupon']['coupon_code_message'] = '<div class="alert-box alert radius"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;&nbsp;Invalid Promotion/Coupon Code. Please try again.</div>';
		return 0;
	}

}

function get_associated_products($connection)
{
	$zq = 
		'SELECT 
		bpu.id AS bpu_id,
		bpu.name AS productName
		FROM project_shopping_code_products pscp 
		JOIN project_brand_product_usages pbpu 
		ON pscp.project_brand_product_usage_id = pbpu.id 
		JOIN brand_product_usages bpu
		ON pbpu.brand_product_usage_id = bpu.id 
		JOIN project_shopping_codes psc 
		ON  pscp.project_shopping_code_id = psc.id
		WHERE psc.shopping_code = "'.$_SESSION['coupon']['coupon_code'].
		'" AND psc.project_id = '.$_SESSION['enrollment']['project_id'].
		' AND psc.active = 1 
		AND psc.effective_date <= CURDATE() 
		AND (psc.end_date is NULL or psc.end_date > CURDATE()) 
		AND psc.price_type_code = "'.$_SESSION['enrollment']['user_price_type'].'"';
	$fetchQuery = mysqli_query($connection, $zq);
	$associatedProducts = "";
	if(mysqli_num_rows($fetchQuery) > 0)
	{
		$key = 0;
		while($result = mysqli_fetch_assoc($fetchQuery))
		{
			if($key == 0)
			{
				$key = $key + 1;
				$associatedProducts .= "<b>".$result['productName']."</b>";

			}
			else
			{
				$associatedProducts .= ", "."<b>".$result['productName']."</b>";
			}
		}
	}
	return $associatedProducts;
}

/**
	* Method Name : get_product_id_from_cart
	* Desc : fetches all the product ids from the cart
	* @access public
	* @return string eg.-(productId1,productId2...)
**/

/*function get_product_id_from_cart()
{

	$productId = "(";
	foreach ($_SESSION['cart'] as $key => $cart) {
		if(!$key)
		{
			$productId .= $cart['productid'];	
		}
		else
		{
			$productId .= ','.$cart['productid'];
		}
	}
	$productId .= ")";
	return $productId;
}*/

/**
	* Method Name : log_sql_error
	* Desc : 
	* @access public
	* @param String $msg
	* @param String $sql
	* @return void
**/

function log_sql_error($msg,$sql,$connection){

	$log_entry  = "\n$msg - ".date('Y-m-d H:i:s');
	$log_entry .= "\nSQL: ".$sql;
	$log_entry .= "\nError Message: ".mysqli_error($connection)."\n";
	$e = new Exception;
	$log_entry .= $e->getTraceAsString();

	switch(SQL_ERROR_ACTION)
	{
		case 'log':
			$logfile = dirname(__FILE__).STORE_LOG_FILE;
			file_put_contents( $logfile , $log_entry,FILE_APPEND);
		break;
		case 'email':
			$customer_service_to = CUSTOMER_SERVICE_EMAIL_FAIL;
			$from = get_from_email($_SESSION['enrollment']['project_id'], $connection);
			$msg = str_replace("\n", "<br>", $msg);

			send_email($from, CUSTOMER_SERVICE_EMAIL_FAIL, $msg, $log_entry, '');
		break;
	}
}



































/**
	* Method Name : add_to_cart
	* Desc : Used to add item in to session and generate html for shopping cart
	* @access public
	* @param Object $connection Mysqli connection object
	* @param $arr array
	* @return String
	*
	*  Nececessary indexes of $arr :
	* Integer 'p_usage_id'  brand product usage id
	* Integer 'quantity' value of quantity
	* Array 'select options' array of item options
	* Integer 'read_only' (1/0)(check whether textbox will be visible or not and it will stored in session)
**/
function add_to_cart($connection, $arr = array())
{
	if(array_key_exists('p_usage_id', $arr) &&
	   array_key_exists('quantity', $arr) &&
	   array_key_exists('select_options', $arr) &&
	   array_key_exists('read_only', $arr) &&
	   array_key_exists('p_last_selected', $arr) )
	{
		$pid = $arr['p_usage_id'];
		$q = $arr['quantity'];
		$select_arr = $arr['select_options'];
		$read_only = $arr['read_only'];
		$p_last_selected = $arr['p_last_selected'];
	}
	if(!empty($pid) && !empty($q))
	{
		add_item_session(array(
			'p_usage_id' => $pid,
			'quantity' => $q,
			'select_array' => $select_arr,
			'read_only' => $read_only,
			'p_last_selected' => $p_last_selected
		));
	}
    $cart_html = generate_cart_html($connection);
    echo  $cart_html;
}

/**
	* Method Name : generate_cart_html
	* Desc : Used to  generate html for shopping cart
	* @access public
	* @param Object $connection Mysqli connection object
	* @param Flag variable $redirect decides the redirection path
	* @return String
**/
function generate_cart_html($connection, $redirect = false)
{
	$continue_shopping_url = $redirect == false ? "#" : '/'.ROOT_DIRECTORY.'numedica/index.php';
    $cart_html = '';
	$cart_html .= '<div style="text-align:center" ><strong>My Cart</strong></div><br>';
    $cart_html .= '<div class="show-for-large-up" >'.
					'<form name="cart_form" id="cart_form" method="post">'.
						'<input type="hidden" name="pid" />'.
						'<input type="hidden" name="command" />'.
						'<div class="panel" >';
						if (isset($_SESSION['cart']) && !empty($_SESSION['cart']))
						{
							$max=count($_SESSION['cart']);
							
							for($i=0; $i<$max; $i++)
							{
								$pid=$_SESSION['cart'][$i]['productid'];
								$q=$_SESSION['cart'][$i]['qty'];
								$read_only = $_SESSION['cart'][$i]['read_only'];
								$price_type = $_SESSION['cart'][$i]['price_type'];
								$post_select1 = 'prd_'.$pid;
								
								$last_selected = $_SESSION['cart'][$i]['last_selected'];
								if(isset($_SESSION['cart'][$i][$post_select1]) && !empty($_SESSION['cart'][$i][$post_select1]))
								 {
									$post_select_session = $_SESSION['cart'][$i][$post_select1];

									$price_type = $_SESSION['cart'][$i]['price_type'];
									$p_price=get_price($pid, $q , $price_type, $post_select_session, $connection, $last_selected);
									$p_option_name = get_option_name($pid,$post_select_session,$connection);
								 }
								 else
								 {
									$p_price=get_price($pid, $q, $price_type,  null, $connection,$last_selected);
									$p_option_name = '';
								 }
								$p_image=get_product_image($pid,$connection);
								$p_name=get_product_name($pid,$connection);
								if($q ==0 ) continue;
								$cart_html .= '<div class="row" >
									<div class="medium-2 large-1 columns">
										<center>
											<p>
												<img src="'.$_SESSION['application']['root_media_host_url'].$p_image.'" style="width: 50px;">
											</p>
										</center>
									</div>
									<div class="medium-10 large-3 columns">
										<p><b>'.$p_name.'</b><br/>
										<font size = "2"><cite>'.$p_option_name.'</cite></font></p>
									</div>
									<div class="small-4 medium-2 large-1 columns left" style="text-align:center" >';
									if(!empty($read_only) && $read_only == 1)
									{
										$cart_html .= "1";
									}
									else
									{
										$cart_html .= '<input type="text" name="product_'.$i.'" id="product_'.$i.'" value="'.$q.'" maxlength="3" size="1">';
									}	
                                    $cart_html	.=		'</div>
									<div class=" small-8 medium-6 large-3 columns">
										<p class="price_medium"><b> X $'.$p_price.'&nbsp;'.'='.'&nbsp;'.'$'.number_format((float)$p_price*$q, 2, '.', '').'</b></p>
									</div>
									<div class="small-6 medium-4 large-4 columns">
										<a href="javascript:del('.$i.')" class="del_medium_button small button radius fa fa-trash-o fa-1x">&nbsp;Remove</a>';
										if(!empty($read_only) && $read_only == 1)
										{
											$cart_html .= '';
										}
										else
										{
											$cart_html .= '&nbsp;<a href="javascript:update_cart('.$i.')" class="update_medium_button small button radius fa fa-refresh fa-1x">&nbsp;Update</a>';
										}
									   
		$cart_html .= 				'</div>	
								</div>';
								
								if( $i < ($max-1))
								{
									$cart_html .= '<hr>';
								}
							}
			
		$cart_html .= 	'</div>
						<div class="div_footer_cart">
							<div class="row">
								<div class="medium-4 large-4 columns" >
									<a href="'.$continue_shopping_url.'" class="continue_shopping radius button_shopping expand button">Continue Shopping</a>
								</div>
								<div class="medium-4 large-4 columns" >
									<div class="button_cursor radius button_order_total expand button right"><b>Order Total:</b> $'.get_order_total($connection).'</div>
								</div>
								<div class="medium-4 large-4 columns" >
									<a href="/'.ROOT_DIRECTORY.'store/php/checkout.php" class="radius button_check_out expand button success right">Checkout</a>
								</div>
							</div>
						</div>';
		
						}
						else
						{
		
					$cart_html .='<h3>You have not selected any products yet.</h3>
						</div>
						<div class="div_footer_cart">
							<div class="row">
								<div class="medium-4 large-4 columns" >
									<a href="'.$continue_shopping_url.'" class="continue_shopping radius button_shopping expand button">Continue Shopping</a>
								</div>
								<div class="medium-4 large-4 columns" >
									<div class="button_cursor radius button_order_total expand button right"><b>Order Total:</b> $'.get_order_total($connection).'</div>
								</div>
								<div class="medium-4 large-4 columns" >
									<div  class="disabled radius button_check_out expand button success right">Checkout</div>
								</div>
							</div>
						</div>';	
						}
		
	$cart_html .= '</form>
				</div>';
	$cart_html .= '<div class="show-for-medium-only" >
                    <form name="medium_cart_form" id="medium_cart_form" method="post">
                        <input type="hidden" name="medium_pid" />
                        <input type="hidden" name="medium_command" />
                        <div class="panel">';
                            
                            if (isset($_SESSION['cart']) && !empty($_SESSION['cart']))
                            {
                                $max=count($_SESSION['cart']);
                                
                                for($i=0;$i<$max;$i++)
                                {
                                    $pid=$_SESSION['cart'][$i]['productid'];
                                    $q=$_SESSION['cart'][$i]['qty'];
									$read_only = $_SESSION['cart'][$i]['read_only'];
									$price_type = $_SESSION['cart'][$i]['price_type'];
                                    $post_select1 = 'prd_'.$pid;
                                    $last_selected = $_SESSION['cart'][$i]['last_selected'];
                                    
                                    if(isset($_SESSION['cart'][$i][$post_select1]) && !empty($_SESSION['cart'][$i][$post_select1]))
                                    {
                                        $post_select_session = $_SESSION['cart'][$i][$post_select1];
                                        $p_price=get_price($pid,$q,$price_type ,$post_select_session,$connection,$last_selected);
                                        $p_option_name = get_option_name($pid,$post_select_session,$connection);
                                    }
                                    else
                                    {
                                       $p_price=get_price($pid,$q,$price_type ,null,$connection,$last_selected);
                                       $p_option_name = '';
                                    }
                                    $p_image=get_product_image($pid,$connection);
                                    $p_name=get_product_name($pid,$connection);
                                    if($q ==0 ) continue;
                                    $cart_html .= '<div class="row">
                                                        <div class="medium-3 columns" >
                                                            <center>
                                                                <p>
                                                                    <img src="'.$_SESSION['application']['root_media_host_url'].$p_image.'" style="width: 50px;">
                                                                </p>
                                                            </center>
                                                        </div>
                                                        <div class="medium-9 columns" >
                                                            <div class="row">
                                                                <div class="medium-12 columns" >
                                                                    <p><b>'.$p_name.'</b><br/>
                                                                    <font size = "2"><cite>'.$p_option_name.'</cite></p></font>
                                                                </div>
                                                            </div>
                                                            <div class="row" >
                                                                <div class="medium-4 columns" style="text-align:center" >';
																if(!empty($read_only) && $read_only == 1)
																{
																	$cart_html .= "1";
																}
																else
																{
																	$cart_html .= '<input type="text" name="medium_product_'.$i.'" id="medium_product_'.$i.'" value="'.$q.'" maxlength="3" size="1" style="text-align: right">';
																}  
                                            $cart_html .=      '</div>
                                                                <div class="medium-8 columns" >
                                                                    <p class="price_medium" style="text-align: left"><b> X $'.$p_price.'&nbsp;'.'='.'&nbsp;'.'$'.number_format((float)$p_price*$q, 2, '.', '').'</b></p>
                                                                </div>
                                                            </div>
                                                            <div class="row" >
                                                                <div class="medium-5 columns" >
                                                                    <a href="javascript:medium_del('.$i.')" class="del_medium_button small button radius fa fa-trash-o fa-1x" style="text-align: right">&nbsp;Remove</a>
                                                                </div>
                                                                <div class="medium-1 columns" >
                                                                    &nbsp;
                                                                </div>
                                                                <div class="medium-5 columns" >';
																if(!empty($read_only) && $read_only == 1)
																{
																	$cart_html .= '';
																}
																else
																{
																	$cart_html .= '<a href="javascript:medium_update_cart('.$i.')" class="update_medium_button small button radius fa fa-refresh fa-1x" style="text-align: left">&nbsp;Update</a>';
																}
                                                                    
                                         $cart_html .=         '</div>
                                                            </div>
                                                        </div>
                                                    </div>';
				
                                    if( $i < ($max-1))
                                    {
                                       $cart_html .= '<hr>';
                                    }
                                }
				
		  $cart_html .='</div>
                        <div class="div_footer_cart">
                            <div class="row">
                                <div class="medium-4 large-4 columns" >
                                    <a href="'.$continue_shopping_url.'" class="continue_shopping radius button_shopping expand button">Continue Shopping</a>
                                </div>
                                <div class="medium-4 large-4 columns" >
                                    <div class="button_cursor radius button_order_total expand button right"><b>Order Total:</b> $'.get_order_total($connection).'</div>
                                </div>
                                <div class="medium-4 large-4 columns" >
                                    <a href="/'.ROOT_DIRECTORY.'store/php/checkout.php" class="radius button_check_out expand button success right">Checkout</a>
                                </div>
                            </div>
                        </div>';
			
                            }
                            else
                            {

		 $cart_html .= '<h3>You have not selected any products yet.</h3>
		 	            </div>
                        <div class="div_footer_cart">
                            <div class="row">
                                <div class="medium-4  columns" >
                                    <a href="'.$continue_shopping_url.'" class="continue_shopping radius button_shopping expand button">Continue Shopping</a>
                                </div>
                                <div class="medium-4 columns" >
                                    <div class="button_cursor radius button_order_total expand button right"><b>Order Total:</b> $'.get_order_total($connection).'</div>
                                </div>
                                <div class="medium-4 columns" >
                                    <div  class="disabled radius button_check_out expand button success right">Checkout</div>
                                </div>
                            </div>
                        </div>';		
			               }
	$cart_html .=	'</form>
                </div>';
	$cart_html .= '<div class="show-for-small-only" >
                <form name="mobile_cart_form" id="mobile_cart_form" >
                    <input type="hidden" name="mob_pid" />
                    <input type="hidden" name="mob_command" />
                    <div class="panel">';
                    if (isset($_SESSION['cart']) && is_array($_SESSION['cart']) && !empty($_SESSION['cart'])) 
                    { 
                        $max=count($_SESSION['cart']);
                        for ($i = 0 ; $i < $max ; $i++)
                        {
                            $pid=$_SESSION['cart'][$i]['productid'];
                            $q=$_SESSION['cart'][$i]['qty'];
							$read_only = $_SESSION['cart'][$i]['read_only'];
							$price_type = $_SESSION['cart'][$i]['price_type'];
                            $post_select1 = 'prd_'.$pid;
                            $last_selected = $_SESSION['cart'][$i]['last_selected'];
                            
                           if(isset($_SESSION['cart'][$i][$post_select1]) && !empty($_SESSION['cart'][$i][$post_select1]))
                            {
                                $post_select_session = $_SESSION['cart'][$i][$post_select1];
                                $p_price=get_price($pid,$q,$price_type,$post_select_session,$connection,$last_selected);
                                $p_option_name = get_option_name($pid,$post_select_session,$connection);
                            }
                            else
                            {
                                $p_price=get_price($pid,$q,$price_type,null,$connection,$last_selected);
                                $p_option_name = '';
                            }
                            $p_image=get_product_image($pid,$connection);
                            $p_name=get_product_name($pid,$connection);
                            if($q ==0 ) continue;
		
                            $cart_html .= '<div class="row">
                                                <div>
                                                    <center><p><img src="'.$_SESSION['application']['root_media_host_url'].$p_image.'" style="width: 50px;"></p></center>
                                                </div>
                                                <div>
                                                  <p style="text-align: center;"><b>'.$p_name.'</b><br/>
                                                  <font size = "2"><cite>'.$p_option_name.'</cite></font></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                  <div>
                                                    <center><strong>Quantity</strong></center>
                                                  </div>
                                            </div>
                                            <div class="row">
                                                <div class="small-4 columns">&nbsp;</div>
                                                <div class="small-4 columns" style="text-align:center">';
												if(!empty($read_only) && $read_only == 1)
												{
													$cart_html .= '1';
												}
												else
												{
													$cart_html .= '<input type="text" name="mob_product_'.$i.'" id="mob_product_'.$i.'" value="'.$q.'" maxlength="3" size="2" style="text-align: center;"/>';
												} 
            $cart_html .=                      '</div>
                                                <div class="small-4 columns">&nbsp;</div>
                                            </div>
                                            <div class="row">
                                                <div style="text-align: center">
                                                  <p><b> X $'.$p_price.'&nbsp;'.'='.'&nbsp;'.'$'.number_format((float)$p_price*$q, 2, '.', '').'</b></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div>
                                                  <a href="javascript:mob_del('.$i.')" class="small button radius expand fa fa-trash-o fa-1x">&nbsp;Remove</a>
                                                </div>
                                                <div>';
												if(!empty($read_only) && $read_only == 1)
												{
													$cart_html .= '';
												}
												else
												{
													$cart_html .= '<a href="javascript:mob_update_cart('.$i.')" class="small button radius expand fa fa-refresh fa-1x">&nbsp;Update</a>';
												}
                                                  
								 $cart_html .='</div>	
                                            </div><hr>';
                        }
			
                            $cart_html .= 	'<div class="row">
                                                <div class="button_cursor radius full_width expand button mob_order_total"><b>Order Total:</b> $'.get_order_total($connection).'</div>
                                            </div>
                                            <div class="row">
                                                <a href="/'.ROOT_DIRECTORY.'store/php/checkout.php" class="radius full_width expand button success">Checkout</a>
                                            </div>
                                            <div class="row">
                                                <a href="'.$continue_shopping_url.'" class="continue_shopping radius full_width expand button">Continue Shopping</a>
                                            </div>
                    </div>';
                
                    }
                    else
                    {
		
                        $cart_html .=   '<div class="panel" >
                                            <b>You have not selected any products yet.</b>
                                            <br><br>
                                             <div class="row">
                                                <div class="button_cursor radius full_width button mob_order_total"><b>Order Total:</b> $'.get_order_total($connection).'</div>
                                            </div>
                                            <div class="row">
                                                <div class="disabled radius full_width button success ">Checkout</div>
                                            </div>
                                            <div class="row">
                                                <a href="'.$continue_shopping_url.'" class="continue_shopping radius full_width button">Continue Shopping</a>
                                            </div>
                                        </div>';
                    }
			
	$cart_html .=	'</form>
            </div>';
	$cart_html .=  '<div id="confirm_modal"  style="display:none; cursor: default">
			<div class="row" >
				<div class="small-12 medium-12 large-12 columns" style="padding-top:20px" >
					<p>Are you sure you want to remove this?</p>
				</div>
			</div>
			<div class="row collapse" >
				<div class="small-12 medium-12 large-12 columns" >
					<input class="tiny button radius" type="button" id="yes" value="Yes" />&nbsp;&nbsp;&nbsp;
					<input class="tiny button radius" type="button" id="no" value="No" /> 
				</div>	
			</div>
        </div>';
    
    return $cart_html;
}

/**
	* Method Name : add_item_session
	* Desc : Used to  add item to the session
	* @access public
	* @param Array $arr array of parameters
	* @return void
	*
	*  Nececessary indexes of $arr :
	* Integer 'p_usage_id'  brand product usage id
	* Integer 'quantity' value of quantity
	* Array 'select_array' array of item options
	* Integer 'read_only' (1/0)(check whether textbox will be visible or not and it will stored in session)
**/
function add_item_session($arr = array())
{
	if(array_key_exists('p_usage_id', $arr) &&
	   array_key_exists('quantity', $arr) &&
	   array_key_exists('select_array', $arr) &&
	   array_key_exists('read_only', $arr))
	{
		$pid = $arr['p_usage_id'];
		$q = $arr['quantity'];
		$select_arr = $arr['select_array'];
		$read_only = $arr['read_only'];
		$p_last_selected = $arr['p_last_selected'];
	}
	//if items are present in session #9
	if (isset($_SESSION['cart']) && is_array($_SESSION['cart']))
	{
		//if product exists ,then do nothing #10
		if (item_exists($pid,$q,$select_arr)) return;
		$max=count($_SESSION['cart']);
		$_SESSION['cart'][$max]['productid']=$pid;
		$_SESSION['cart'][$max]['qty']=$q;
		$_SESSION['cart'][$max]['read_only'] = $read_only;
		$_SESSION['cart'][$max]['price_type'] = $_SESSION['enrollment']['user_price_type'];
		$_SESSION['cart'][$max]['last_selected'] = $p_last_selected;
		$post_select = 'prd_'.$pid;
		if (!is_null($select_arr))
		{
			$_SESSION['cart'][$max][$post_select]= $select_arr;
		}
	}
	//Else not present in session
	else
	{
		$_SESSION['cart']=array();
		$_SESSION['cart'][0]['productid']=$pid;
		$_SESSION['cart'][0]['qty'] = $q;
		$_SESSION['cart'][0]['read_only'] = $read_only;
		$_SESSION['cart'][0]['price_type'] = $_SESSION['enrollment']['user_price_type'];
		$_SESSION['cart'][0]['last_selected'] = $p_last_selected;
		$post_select = 'prd_'.$pid;
		if (!is_null($select_arr))
		{
			$_SESSION['cart'][0][$post_select]= $select_arr;
		}
		
		
	}
}

/**
	* Method Name : item_exists
	* Desc : Used to check whether item already exists on session or not 
	* @access public
	* @param Integer $pid product id
	* @param Integer $q value of quantity
	* @param Array $select_arr array of select options
	* @return boolean
**/
function item_exists($pid ,$q ,$select_arr)
{
	$pid=intval($pid);
	$max=count($_SESSION['cart']);
	$flag=0;
	for($i=0;$i<$max;$i++)
	{
		//if item product id is present in session #11
		if ($pid == $_SESSION['cart'][$i]['productid'])
		{
			//$_SESSION['cart'][$i]['qty']=$q;
			$post_select = 'prd_'.$pid;
			if (!is_null($select_arr))
			{
				//if same quantity and select options are present in sesssion #12
				if ($_SESSION['cart'][$i]['qty'] == $q && $_SESSION['cart'][$i][$post_select] == $select_arr)
				{
						$flag = 1;
						break;
				}
				//if quantity of item to be added and quantity present in session is not same and select options to be added are same #13
				elseif($_SESSION['cart'][$i]['qty'] != $q && $_SESSION['cart'][$i][$post_select] == $select_arr)
				{
						//Update the quantity #13
						$_SESSION['cart'][$i]['qty']=$q;
						$flag = 1;
						break;
				}
			
			}
			else
			{
				//if quantity of item to be added is same as present in session #14
				if ($_SESSION['cart'][$i]['qty'] == $q)
				{
					$flag = 1;
						break;	
				}
				//Else Update the quantity #14
				else
				{
					$_SESSION['cart'][$i]['qty']=$q;
						$flag = 1;
						break;	
				}
			}
			
		}
	}
	return $flag;
}

/**
	* Method Name : get_retail_option_price
	* Desc : Used to generate html code for displaying item options 
	* @access public
	* @param Object $connection Mysqli connection object
	* @param $arr array
	* @return String
	*
	* Nececessary indexes of $arr :
	* Integer 'p_usage_id'  brand product usage id
	* Integer 'pid' product id 
**/
function get_retail_option_price($connection, $project_id, $pbpup_id, $bpup_id)
{
	if(!isset($project_id) || !empty($pbpup_id))
	{
		return false;
	}
	
	if(!empty($bpup_id)) $add_where = ' AND bpuo_id = '.$bpup_id;
	
	$q_options = 'SELECT price FROM project_brand_product_usage_prices WHERE brand_product_usage_id = '.$pbpup_id.' AND pbpu_project_id = '.$project_id.$add_where;
	$res_options = mysqli_query($connection, $q_options);
	$row_options = mysqli_fetch_assoc($res_options);
	return $price;
}


/**
	* Method Name : generate_item_option_html
	* Desc : Used to generate html code for displaying item options 
	* @access public
	* @param Object $connection Mysqli connection object
	* @param $arr array
	* @return String
	*
	* Nececessary indexes of $arr :
	* Integer 'p_usage_id'  brand product usage id
	* Integer 'pid' product id 	
**/
function generate_item_option_html($connection, $arr=array(), $parent_id = null, $level = 0, $showPrice = false)
{
	if(isset($arr['pid']) &&
	   !empty($arr['pid']) &&
	   isset($arr['p_usage_id']) &&
	   !empty($arr['p_usage_id']))
	{
		$p_usage_id = $arr['p_usage_id'];
		$pid = $arr['pid'];
		$retail_price = isset($arr['retail_price'])?$arr['retail_price']:0;
		$price_type = $arr['price_type'];
		if(isset($arr['selections']))
			$selections = explode("|", $arr['selections']);
		else
			$selections = explode("|", '0|0|0|0|0|0|0|0|0|0|0|0|0');
		$quantity = isset($arr['quantity'])?$arr['quantity']:1;
		$parent_price = isset($arr['parent_price'])?$arr['parent_price']:null;
	}
	else
	{
		return '';
	}
	$options_html = '';
	$parent_sentence = ' AND bpuo.brand_product_usage_option_parent_id';
	if($parent_id == null){
		$parent_sentence .= ' is null ';
	}else{
		$parent_sentence .= ' = '.$parent_id;
	}

	$qty_price_fn = ' ';
	if($quantity > 1){
		$qty_price_fn = 'getPrice(bpuo.brand_product_usage_id, '.$quantity.', bpuo.id, '.$_SESSION['enrollment']['project_id'].',\''.$price_type.'\') as qty_price,';
	}

	$q_options = 
      'SELECT bpuo.*,
 	  	getPrice(bpuo.brand_product_usage_id, '.$quantity.', bpuo.id, '.$_SESSION['enrollment']['project_id'].',\''.$price_type.'\') as price,
	    pot.name AS pOption,
	    (SELECT count(*) 
	    	FROM brand_product_usage_options o 
	    	WHERE o.brand_product_usage_id = bpuo.brand_product_usage_id 
	    	AND o.brand_product_usage_option_parent_id = bpuo.id 
	    	AND (o.effective_date <= CURDATE() AND (o.end_date is NULL or o.end_date >= CURDATE() )) 
	    ) AS child_options

	    FROM brand_product_usage_options bpuo

	    LEFT JOIN product_option_types pot ON bpuo.product_option_type_code = pot.code

	    WHERE bpuo.brand_product_usage_id =  '.$p_usage_id.$parent_sentence.' 
	    AND (bpuo.effective_date <= CURDATE() AND (bpuo.end_date is NULL or bpuo.end_date >= CURDATE() )) 
	    GROUP BY bpuo.id
	    ORDER BY bpuo.value_sort ASC';

	$res_options = mysqli_query($connection, $q_options);
	
	$cnt_options = mysqli_num_rows($res_options);

	if ($cnt_options > 0)
	{
	   $option_name = '';
	   $options_html = '';
	   $counter = '';
	   $first_item = true;

	   $next_parent_id = 0;
	   $next_parent_price = null;

	   while($row_options = mysqli_fetch_assoc($res_options))
	   {
			$selected = '';
			if($selections[$level]  == 0 && $first_item){
				$selections[$level] = $row_options['id'];
				$next_parent_price = $row_options['price'];
			}
			
			if( $selections[$level]  > 0 && $selections[$level] == $row_options['id']){
				$selected = 'selected';
				$next_parent_id = $row_options['id'];
				$next_parent_price = $row_options['price'];
			}

			if($first_item){

				if($showPrice == true){
					$str_price_type = 'Unit Price';
					$retail_price = isset($arr['retail_price'])?$arr['retail_price']:$arr['parent_price'];
					$options_html .= '<div id="productPrice'.$p_usage_id.'" class="panel">
									<p>'.$str_price_type.': <b class="product-price">$'.$retail_price.'</b></p>
						  </div>';
				}
				$arr['selections'] = implode('|', $selections);
				$options_html .= '<div id="option_level'.$level.'_'.$p_usage_id.'"><label>'.$row_options['pOption'].':</label>'
				.'<select class="recurse_option" data-selections="'.$arr['selections'].'" data-level='.$level
					.' data-usageid="'.$p_usage_id
					.'"  data-pid="'.$pid.'" name="prd_'.$p_usage_id."['".$row_options['product_option_type_code']."'][ ]".'" id="'.$row_options['product_option_type_code'].'_'.$pid.'">';

				$first_item = false;
				$next_parent_id = $row_options['id'];
			}

			$price = '';
			if($parent_id == null){
				$price = ' ($'.($row_options['price']).')';
			}else{

				if($row_options['price'] != $parent_price){
					$price = ' ($'.($row_options['price']).')';	
				}
			}
			
			$price_text = $price;

			$options_html .= '<option '.$selected.' value="'.$row_options['id'].'" data-childs="'.$row_options['child_options'].'" data-price="'.$row_options['price'].'"  >'.$row_options['option_value'].$price_text.' '.($row_options['child_options'] > 0?'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...':'' ).'   </option>';

		}

	   $options_html .= '</select></div>'; 
	   
	   $arr['parent_price'] = $next_parent_price;
		
	   return $options_html.generate_item_option_html($connection,$arr,$next_parent_id,$level + 1,false);
	}
	return '';
}

function get_option_value($connection, $id) {
	$q = 'SELECT option_value FROM brand_product_usage_options WHERE id = '.$id;
	$r = mysqli_query($connection, $q);
	$row = mysqli_fetch_assoc($r);
	return $row['option_value'];
}

/**
	* Desc : Used to create offer button
	* @access public
	* @param Object $connection Mysqli connection object
	* @param $arr array
	* @return String
	*
	* Nececessary indexes of $arr :
	* Integer 'p_usage_id'  brand product usage id
	* Integer 'project_id' project id
	* String 'p_name' product name
**/

function generate_offer_html($connection, $arr=array())
{
	if(isset($arr['p_usage_id']) &&
	   isset($arr['project_id']) &&
	   isset($arr['p_name']) 	 &&
	   isset($arr['price_type']) &&
	   !empty($arr['p_usage_id']) &&
	   !empty($arr['project_id']) &&
	   !empty($arr['p_name']) &&
	    !empty($arr['price_type'])
	   )
	{
		$p_usage_id = $arr['p_usage_id'];
		$project_id = $arr['project_id'];
		$p_name = $arr['p_name'];
		$price_type = $arr['price_type'];
	}
	else
	{
		return false;
	}
	$query_offer = 'SELECT pbpup.* 

		FROM project_brand_product_usage_prices pbpup
		LEFT JOIN brand_product_usage_options bpuo ON pbpup.`bpuo_id` = bpuo.`id` 
		WHERE pbpup.brand_product_usage_id = ' . $p_usage_id . ' AND 
		pbpup.pbpu_project_id = '. $project_id .' AND 
		pbpup.price_type_code = "'.$price_type.'" AND 
		pbpup.quantity_low_range IS NOT NULL 
		AND ( bpuo.effective_date <= CURDATE() AND (bpuo.end_date is NULL or bpuo.end_date >= CURDATE()) ) 
		GROUP BY pbpup.id
		ORDER BY pbpup.`bpuo_id` ASC, pbpup.quantity_range_seq ASC';
		
	$result_offer = mysqli_query($connection, $query_offer);
	$cnt_offer = mysqli_num_rows($result_offer);
	
	if (!$result_offer)
	{
	   echo $query_offer;
       echo  mysqli_error($connection);
	}
	if ($cnt_offer > 1)
	{
		$option = '';
		$html = '';
		$counter = '';
		$no_option = '';
		$html .= '
		<a data-reveal-id="reveal-' . $p_usage_id . '" href="#" ><img src="../'.STORE_FOLDER_NAME.'/store_img/offer.png" width="50px" height="50px" /></a>'.
		'<div id="reveal-'.$p_usage_id.'" class="reveal-modal small" data-reveal>';
			
			$option_id = '';
			
			while ($price = mysqli_fetch_assoc($result_offer))
			{
			
				if($price['bpuo_id'] != $option_id) {
					if($option_id != '') $html .= '</table>';
					$html .= '
					<ul class="pricing-table">
					    <li class="title">
					        <b>'.$p_name.'</b><br>'
                            .get_option_value($connection, $price['bpuo_id']).'
					    </li>
                    </ul>
                    <table align="center">';
					$counter = '';
				}
				
				if ($price['product_type_code'] != '' && $price['product_type_code'] != $option) {
					$html .= '
					<table align="center">
					    <tr colspan="2" align="center"><strong>'.$price['product_type_code'].'</strong></tr>';
					$option = $price['product_type_code'];
					$counter = '';
				}
				if(!isset($price['product_type_code']))
					$price['product_type_code'] = '';

				if ( $price['product_type_code'] == '' && $no_option == '') {
					$html .= '<table align="center">';
					$option = $price['product_type_code'];
					$counter = '';
					$no_option = 1;
				}
				if ($counter == '') {
					$html .= '
					<tr>
					    <td align-"center"><strong>Quantity Range</strong></td>
					    <td align-"center"><strong>Price</strong></td>
                    </tr>';
					$counter = 1;
				}
				if ($price['price'] != '0.00' && !empty($price['price']))
				{
					$html .= '<tr><td>';
					if ($price['quantity_high_range'] == '') $html .= $price['quantity_low_range']."+";
					else $html .= $price['quantity_low_range'].'-'.$price['quantity_high_range'];
					$html .= '</td><td>$'.$price['price'].'</td></tr>';
				}
				$option_id = $price['bpuo_id'];
			}
			$html .= '
			</table>
            <a class="close-reveal-modal">&#215;</a>					
		</div>';
	return $html;
	}
	else
	{
		$html = '';
		return $html;
	}
}

/**
	* Method Name : integrate_add_to_cart_button
	* Desc : Used to integrate "add to cart" button along with item options and offer button.
	* @access public
	* @param Object $connection Mysqli connection object
	* @param $arr array
	* @return String
	*
	* Nececessary indexes of $arr :
	* Integer 'p_usage_id'  brand product usage id
	* Integer 'project_id' project id
	* String 'p_name' product name
	* Integer 'pid' product name
	* String 'form_name' name of form (It must be unique for each item)
	* Boolean 'qty_visible' visibility of quantity text box (Optional)
	*String 'button_html_content' html content of link (Optional)
**/
function integrate_add_to_cart_button($connection, $arr=array())
{
	
	if(isset($arr['p_usage_id']) &&
	   isset($arr['project_id']) &&
	   isset($arr['p_name']) &&
	   isset($arr['pid']) &&
	   isset($arr['form_name']) &&
	   !empty($arr['p_usage_id']) &&
	   !empty($arr['project_id']) &&
	   !empty($arr['p_name']) &&
	   !empty($arr['pid']) &&
	   !empty($arr['form_name'])
	   )
	{
		$p_usage_id = $arr['p_usage_id']; 
		$project_id = $arr['project_id'];
		$_SESSION['enrollment']['project_id'] = $project_id;
		$p_name = $arr['p_name']; 
		$pid = $arr['pid']; 
		$form_name = $arr['form_name'];
		$retail_price = $arr['retail_price'];
		
	}
	else
	{
		return false;
	}
	
	if (array_key_exists('price_type', $arr) && ! empty($arr['price_type']))
	{
		$price_type = $arr['price_type'];
	}
	else
	{
		$price_type = USER_PRICE_TYPE;
	}

	$add_to_cart_html = '';
	$add_to_cart_html .= '<form id="'.$form_name.'_'.$pid.'" >';
							
	
	$flag = 0;
	if(isset($_GET['option_id']))
	{
		$parent_price 			= 	null;
		$parent_id 				=	null;
		$selections 			=	'0|0|0|0|0|0|0|0|0|0|0|0|0|0';
		$resultFromExactOrder 	= 	exact_product($connection, $_GET['option_id'], $price_type, $p_usage_id);
		if($resultFromExactOrder)
		{
			$flag 					=	1;
			$selections 			= 	$resultFromExactOrder['selection'];
			$parent_price 			= 	$resultFromExactOrder['parent_price'];
			$parent_id 				=	$resultFromExactOrder['parent_id'];
			$add_to_cart_html .= generate_item_option_html($connection, array(
								'pid' => $pid,
								'p_usage_id' => $p_usage_id,
								'selections' => $selections,
								'price_type' => $price_type,
								'parent_price' => $parent_price
								),null,0,true);
		}
	}
	if($flag == 0)
	{
		$add_to_cart_html .= generate_item_option_html($connection, array(
							'pid' => $pid,
							'p_usage_id' => $p_usage_id,
							'retail_price' => $retail_price,
							'price_type' => $price_type
						),null,0,true);
	}
	
	if(array_key_exists('qty_visible', $arr))
	{
		
		if($arr['qty_visible'])
		{
			$add_to_cart_html .= "Quantity:"."<br/>";
			$add_to_cart_html .= '<input class="quantity_item" type="number" min="1" step="1" value="1" name="q" id="qty_'.$pid.'" data-id="'.$pid.'" data-usageid="'.$p_usage_id.'">';				
		}
		else
		{
			
			$add_to_cart_html .= '<input type="hidden" value="1" name="q" id="qty_'.$pid.'">'.'<br>'.
									'<input type="hidden" value="1" name="read_only" />';
		}
	}
	else
	{
		$add_to_cart_html .= "Quantity:"."<br/>";
		$add_to_cart_html .= '<input type="number" min="1" step="1" value="1" name="q" id="qty_'.$pid.'">'.'<br>';
	}
	
	if(array_key_exists("button_html_content", $arr) && $arr['button_html_content'] != '')
	{
		$add_to_cart_html .= "<a class='add_to_cart' href='#' data-id='{$pid}' data-usage_id='{$p_usage_id}' data-reveal-id='cart_modal' data-form='".$form_name.'_'.$pid."'  >".
		$arr['button_html_content'].'</a>';
	}
	else
	{
		
		$offer = generate_offer_html($connection, array(
							'p_usage_id' => $p_usage_id,
							'project_id' => $project_id,
							'p_name' => $p_name,
							'price_type' => $price_type
						));
					
		if(strlen($offer) > 0) {
			$bwidth = '75%';
			$margin_bottom = '15px';
		} else {
			$bwidth = '100%';
			$margin_bottom = '-7px;';
		}
		if(array_key_exists("checkout_page", $arr))
		{
			$checkout_page = $arr['checkout_page'];	
		}
		else
		{
			$checkout_page = 0;
		}
		$add_to_cart_html .= "<button class='add_to_cart small button radius success fa fa-shopping-cart icon-white fa-3x' style=\"width: ".$bwidth.";margin-bottom: ".$margin_bottom.";height: 60px;font-size: 1.0em;\" href='#' data-id='{$pid}' data-checkout_page='{$checkout_page}' data-usage_id='{$p_usage_id}' data-reveal-id='cart_modal' data-form='".$form_name.'_'.$pid."'  >&nbsp;&nbsp;Add to Cart</button>&nbsp;&nbsp;".$offer;		
	}

	$add_to_cart_html .= '<input type="hidden" value="'.$price_type.'" name="price_type" id="price_type_'.$pid.'" />';
	
	$add_to_cart_html .= 
					'</form>';
	if($arr['divAdded']==0)
	{
		$add_to_cart_html .= '<div id="cart_modal" class="reveal-modal" data-reveal></div>';	
	}
	return $add_to_cart_html;
}

/**
	* Method Name : integrate_view_cart_button
	* Desc : Used to integrate "view cart" button .
	* @access public
	* @param $arr array
	* @return String
	*
	* Nececessary indexes of $arr :
	* String 'store_path' root directory of store folder
	* String 'button_html_content' html content of view cart button that will be present inbetween '<a></a>'(Optional)
	* Inorder to show default view cart button ,you need to pass 'button_html_content' => ''
**/
function integrate_view_cart_button($arr = array())
{
	$store_path = '../store/';//$arr['store_path'];
	$button_html_content = $arr['button_html_content'];
	$view_cart_html = '<a title="View Cart" href="';
	if(array_key_exists('store_path', $arr) && !empty($arr['store_path']))
	{
		$view_cart_html .= $arr['store_path'].'php/cart.php"'.'data-reveal-id="cart_modal" data-reveal-ajax="true" ';
	}
	if(array_key_exists('button_html_content', $arr) && !empty($arr['button_html_content']))
	{
		$view_cart_html .= '>'.$button_html_content.'</a>';
	}
	else
	{
		$view_cart_html .= ' class="button small radius"><i class="fa fa-shopping-cart icon-white fa-1x"></i>&nbsp;&nbsp;VIEW CART</a>';
	}
	return $view_cart_html;
}

/**
	* Method Name : reorder
	* Desc : Used to reorder an existing order .
	* @access public
	* @param Object $connection Mysqli connection object
	* @param $order_id int
	* @param $order_item_id int
	* @return String
**/
function reorder($connection, $order_id, $order_item_id = false)
{
	$item_option_id = [];
	$queryPart 		= '';
	$navigation 	= true;
	if($order_item_id)
	{
		$queryPart 	= ' AND id = '.$order_item_id;
		$navigation = false;
	}
	$query = 'SELECT id,brand_product_usage_id AS p_usage_id, quantity FROM order_items WHERE orders_id = '.$order_id.$queryPart;
	$result = mysqli_query($connection, $query);
	while ($r = mysqli_fetch_assoc($result)) 
	{
		$select_arr = [];
		$query_item = 'SELECT order_option_type_code, item_option_id FROM order_item_options WHERE order_item_id = '.$r['id'];
		$result_item = mysqli_query($connection, $query_item);
		while ($r_item = mysqli_fetch_assoc($result_item)) 
		{
			$item_option_id[0] = $r_item['item_option_id'];
			$order_option_type_code = "'".$r_item['order_option_type_code']."'";
			$select_arr[$order_option_type_code] = $item_option_id;
		}
		$p_usage_id = $r['p_usage_id'];
		$quantity   = $r['quantity'];
		if(!empty($p_usage_id) && !empty($quantity))
		{
			add_item_session(array(
				'p_usage_id' => $p_usage_id,
				'quantity' => $quantity,
				'select_array' => $select_arr,
				'read_only' => 0,
				'p_last_selected' => $item_option_id[0],
			));
		}
	}
	$cart_html = generate_cart_html($connection, $navigation);
	echo $cart_html;
}

/**
	* Method Name : exact_order
	* Desc : Used to get the exact product from option_id
	* @access public
	* @param Object $connection Mysqli connection object
	* @param $option_id int
	* @param $price_type string
	* @param $p_usage_id int
	* @return String
**/
function exact_product($connection, $option_id, $price_type, $p_usage_id)
{
	$query = 
	'SELECT brand_product_usage_option_parent_id 
	from brand_product_usage_options 
	where id = '.$option_id.
	' AND brand_product_usage_id = '.$p_usage_id;	
	$fetch_query 	= 	mysqli_query($connection,$query);
	if(mysqli_num_rows($fetch_query) > 0)
	{
		$result 		= 	mysqli_fetch_assoc($fetch_query);

		$selection 		= 	$option_id;
		$level 			=	0;
		$parent_price 	= 	null;
		$parent_id 		= 	$option_id;
		$flag 			= 	0;

		$queryPrice = 
		'SELECT price from project_brand_product_usage_prices where bpuo_id = '.$option_id.' AND price_type_code = "'.$price_type.'"';

		$fetch_queryPrice 	= 	mysqli_query($connection,$queryPrice);
		
		if(mysqli_num_rows($fetch_queryPrice)>0)
		{
			$resultPrice 		= 	mysqli_fetch_assoc($fetch_queryPrice);
			$parent_price 		= 	$resultPrice['price'];
			$flag = 1;
		}

		while($result['brand_product_usage_option_parent_id'])
		{
			$selection = $result['brand_product_usage_option_parent_id'].'|'.$selection;	
			$option_id = $result['brand_product_usage_option_parent_id'];

			$query = 'SELECT brand_product_usage_option_parent_id from brand_product_usage_options where id = '.$option_id;
			$fetch_query 	= 	mysqli_query($connection,$query);
			$result 		= 	mysqli_fetch_assoc($fetch_query);

			if($flag == 0)
			{
				$queryPrice = 
				'SELECT price from project_brand_product_usage_prices where bpuo_id = '.$option_id.' AND price_type_code = "'.$price_type.'"';
				$fetch_queryPrice 	= 	mysqli_query($connection,$queryPrice);
				$resultPrice 		= 	mysqli_fetch_assoc($fetch_queryPrice);
				$parent_price 		= 	$resultPrice['price'];
				$flag 				= 	1;
			}

		}

		$countPipe 	= 	substr_count($selection, '|');
		$level 		=	$countPipe + 1;
		for($i = $countPipe; $i < 13; $i++) 
		{ 
			$selection .= "|0"; 
		}

		$resultFromExactOrder 					= 	[];
		$resultFromExactOrder['selection'] 		= 	$selection;
		$resultFromExactOrder['parent_price'] 	= 	$parent_price;
		$resultFromExactOrder['parent_id'] 		= 	$parent_id;
		$resultFromExactOrder['level'] 			=	$level;

		return $resultFromExactOrder;
	}
	else
	{
		return false;
	}
}
