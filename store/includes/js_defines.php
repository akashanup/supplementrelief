<!-- Define global variable for js -->
<script type="text/javascript" >
    // Define url to cart.php used for update and delete functionality on shopping cart
    var cart_url = "<?php echo ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://'), $_SERVER['HTTP_HOST'].'/' .ROOT_DIRECTORY.STORE_FOLDER_NAME .'/php/cart.php';?>";
    // Define url for Continue Shopping button on Checkout Cart
    var home_page_url = "<?php echo ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://'), $_SERVER['HTTP_HOST'].$_SESSION['store_home_page_path']; ?>";
    var  progress_wheel = "<img src='../<?php echo STORE_FOLDER_NAME; ?>/store_img/ld.gif' />"
</script>
