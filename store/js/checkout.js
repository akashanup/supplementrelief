/**
	* Method Name : checkout_del
	* Desc : Used to delete item on checkout cart for medium and large screen devices
	* @access public
	* @param integer pid item index no.
	* @return boolean
**/ 
function checkout_del(pid)
{
	//Shows confirmation modal
    $.blockUI({ message: $('#confirm_modal'),css: { top: '3%', border: '0px' }, baseZ: 1005 });
	//Making confirmation modal responsive
    $('.blockUI.blockMsg.blockPage').addClass("block_ui_confirm");
    //if yes will be clicked 
    $('#yes').off('click').on('click',function() {
        document.cart_form.pid.value = pid;
        document.cart_form.command.value = 'delete';
       
        $.blockUI({ message: "<img src='../store_img/ld.gif' />",css: {border: '0px',backgroundColor:'#616161',top: '25%' }, baseZ: 1005 }); 
        
        $.ajax({
            type:"POST",
            url:"./checkout.php",
            data: {
					command : 'delete',
					pid     : pid
					},
            dataType:'html',
            
            success:function(r)
            {
				var a = $(r);
				$('#checkout_cart_container').html(a.find('#checkout_cart_container').html());
				$('#check_out_block').html(a.find('#check_out_block').html());
				var checkCoupon = $('#x_coupon_code').val();
				var totalProductCost = $("#totalProductCost").val();
				if(checkCoupon)
				$('#coupon_message').html(a.find('#session_coupon_message').val());console.log(a.find('#session_coupon_message').val());
				if(!totalProductCost)
				$('#coupon_message').html("");
            },
            complete: function()
            { 
                $.unblockUI(); 
            } 
            
        });
    });
	//If no will be clicked
    $('#no').click(function() { 
        $.unblockUI(); 
        return false; 
    });
}

/**
	* Method Name : checkout_update_cart
	* Desc : Used to update item on checkout cart for medium and large screen devices
	* @access public
	* @param Integer pid Item index no.
	* @return void 
**/
function checkout_update_cart(pid)
{
    document.cart_form.pid.value=pid;
    document.cart_form.command.value='update';
	var qant = 'product_'+pid;
	var parameter = {
						command : 'update',
						pid     :  pid
					};
	var select_quant = '#quant_'+pid;
	var quantity = $(select_quant).val();
	//Checks for numeric value #1
	if (is_number(quantity))
	{
		$.ajax({
			type:"POST",
			url:"./checkout.php",
			data: $.param(parameter) + '&'+qant+'='+quantity,
			dataType:'html',
			success:function(response)
			{
				//console.log(response);
				var a = $(response);
				$('#checkout_cart_container').html(a.find('#checkout_cart_container').html());
				$('#order_total_checkout').html(a.find('#order_total_checkout').html());
				var checkCoupon = $('#x_coupon_code').val();
				if(checkCoupon)
				$('#coupon_message').html(a.find('#session_coupon_message').val());console.log(a.find('#session_coupon_message').val());
			}
		});
	}
	//Else show alert modal #1
	else
	{
		var message = 'Please Enter Valid data';
		alert_modal(message);
	}
}

/**
	* Method Name : checkout_mob_update_cart
	* Desc : Used to update item on checkout cart for small screen devices
	* @access public
	* @param Integer pid item index no.
	* @return void 
**/
function checkout_mob_update_cart(pid)
{
    document.mobile_cart_form.mob_pid.value = pid;
    document.mobile_cart_form.mob_command.value = 'update';
	var qant = 'mob_product_'+pid;
	var parameter = {
						mob_command : 'update',
						mob_pid     :  pid
							 
					};
	var select_quant = '#mob_quant_'+pid;
	var quantity = $(select_quant).val();
	//Checks for numeric value #2
	if (is_number(quantity))
	{
		$.ajax({
			type:"POST",
				url:"./checkout.php",
				data: $.param(parameter) + '&'+qant+'='+quantity,
				dataType:'html',
				
				success:function(response)
				{
					//console.log(response);
					var a = $(response);
					$('#checkout_cart_container').html(a.find('#checkout_cart_container').html());
					$('#order_total_checkout').html(a.find('#order_total_checkout').html());
				}
				
			});
	}
	//Else show alert modal #2
	else
	{
		var message = 'Please Enter Valid data';
		alert_modal(message);
	}
}

/**
	* Method Name : checkout_mob_del
	* Desc : Used to delete item on checkout cart for small screen devices
	* @access public
	* @param integer pid item index no.
	* @return boolean
**/
function checkout_mob_del(pid)
{
	//Show confirmation modal
    $.blockUI({ message: $('#confirm_modal'),css: { top: '3%', border: '0px' }, baseZ: 1005 });
	//Making confirmation modal responsive
    $('.blockUI.blockMsg.blockPage').addClass("block_ui_confirm");
	//If yes will be clicked
    $('#yes').click(function() { 
    document.mobile_cart_form.mob_pid.value=pid;
    document.mobile_cart_form.mob_command.value='delete';
    $.blockUI({ message: "<img src='../store_img/ld.gif' />",css: {border: '0px',backgroundColor:'#616161',top: '36%' }, baseZ: 1005 });
    $.ajax({
				type:"POST",
                url:"./checkout.php",
                data: {
						mob_command : 'delete',
						mob_pid     :  pid
					},
                dataType:'html',
                
                success:function(r)
                {
					var a = $(r);
					$('#checkout_cart_container').html(a.find('#checkout_cart_container').html());
					$('#check_out_block').html(a.find('#check_out_block').html());
                },
                complete: function()
                { 
                // unblock when remote call returns 
                  $.unblockUI(); 
                }
                
            });
    });
    //If no will be clicked
    $('#no').click(function() { 
        $.unblockUI(); 
        return false; 
    });        
}


		
