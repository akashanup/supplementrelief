//When the document will be ready
 
var delay_check = Array();
$(document).ready (function(){
	//For scroll to top arrow
	// This (scrollUp) is already being done in the standard app.js so does not need to be done again here. However, the Store as currently configured does not use the standard app.js but should. 
    /*$.scrollUp({
        
        scrollImg: true,
        scrollDistance: 100
        
    });*/
	 //For browser window width greater than 40.063em #1
	if (window.matchMedia('(min-width:  40.063em)').matches)
    {
		//Setting height of "Why choose Professional Supplements?" paragraph for adding read more option #1
		$('#txt').readmore({maxHeight: 134});
		//Adding top spacing to the sticky div having id as "stick-content" #1
        $("#stick-content").sticky({ topSpacing: 0,
                                   responsiveWidth:true});
		//Adding top spacing to the sticky div having id as "check_out_block" #1
		 $("#check_out_block").sticky({ topSpacing: 3,
                                   responsiveWidth:true});
		//Set width of sticky content #1
		var check_out_block_width =  $('#check_out_block-sticky-wrapper').width()+'px';
		$('#check_out_block').css("width", check_out_block_width);
		var product_table_width = $('#projectBrandSupplementsTable').width()+'px';
        $('#stick-content').css("width", product_table_width);
		set_header_width();
	}
	//If browser window width will lie in between 40.063em and 44em or it will lie below 23.8em #2
	if ((window.matchMedia('(min-width:  40.063em)').matches && window.matchMedia('(max-width:  44em)').matches) ||
		window.matchMedia('(max-width:  23.8em)').matches )
	{
		//Adding class "tiny" to 'add to cart' buttons so that it will make it smaller. #2
	   $(document).find('.add_to_cart').addClass("tiny");
	}
	//This will be fired when the user releases a key on the keyboard. 
	$('.billing_text,.address_text_area').keyup(function(){
		//if error class will be present ,it will add "disabled" class to "place order" button. #3
		if ($('.error').is(":visible"))
		{
			$('#checkout_submit').addClass('disabled');
		}
		//removes "disabled" class from the "place order" button. #3
		else
		{
			$('#checkout_submit').removeClass('disabled');
		}
	});
	
	
	//If state, country,exp.month ,exp.year dropdown selected option value will be empty. #4
	if ($('#x_country option:selected,#x_shipping_country option:selected,#x_state option:selected,#x_shipping_state option:selected,#exp_month option:selected,#exp_year option:selected').val() == '')
	{
		//Adding grey color to options. #4
		$('#x_country,#x_shipping_country,#x_state,#x_shipping_state,#exp_month,#exp_year').css('color','#999');
	}
	//This event will be fired when user will select any options from the SHIPPING country dropdown.
	
	$('#x_shipping_state').on('change',function(){
   		$('#x_shipping_state').css('color','#000');
	})
	
	$('#x_country').on('change',function(){
		//if any option value will be empty #5
		if ($('#x_country').val() == '')
		{
		$('#x_country').css('color','#999');
		}
		//Else add a specific color to country dropdown #5
		else
		{
			$('#x_country').css('color','rgba(0, 0, 0, 0.75)');
		}
	});
	
	//This event will be fired when user will select any options from the state dropdown
	$(document.body).on('change','#x_shipping_state',function( ){
	
		//if any option value will be empty #6
		if ($('#x_shipping_state').val() == '')
		{
		$('#x_shipping_state').css('color','#999');
		}
		//Else add a specific color to state dropdown #6
		else
		{
			$('#x_shipping_state').css('color','rgba(0, 0, 0, 0.75)');
		}
		//removes error message 
		$('#x_shipping_state').parent().removeClass("error");
		//Gets tax from get_tax.php as per state and replace check_out_block content through ajax
		var state_abbr = $('#x_shipping_state option:selected').val();
		var state_param = 'state_abbr='+state_abbr;
		$.ajax({
		type:"POST",
		async: "false",
		url:"./get_tax.php",
		data: state_param,
		dataType:'html',
		success:function(response)
		{
			//console.log(response);
			//$('#state_div').html(response.state_option);
			var a = $(response);
			$('#check_out_block').html(a.filter('#check_out_block').html());
			
		}
		});
	});
	
	//This event will be fired when user will select any options from the SHIPPING METHOD
	$(document.body).on('change','#x_shipping_type',function( ){
		//if any option value will be empty #6
		if ($('#x_shipping_type').val() == '') {
			$('#x_shipping_type').css('color','#999');
		} else /*Else add a specific color to shipping method dropdown #6 */ {
			$('#x_shipping_type').css('color','rgba(0, 0, 0, 0.75)');
		}
		//removes error message 
		$('#x_shipping_type').parent().removeClass("error");
		//Gets shipping from get_shipping.php as per shipping_type and replace check_out_block content through ajax
		var shipping_type = $('#x_shipping_type option:selected').val();
		var shipping_type_param = 'shipping_type='+shipping_type;
		$.ajax({
		type:"POST",
		async: "false",
		cache:false,
		url:"./get_shipping.php",
		data: shipping_type_param,
		dataType:'html',
		success:function(response)
		{
			////console.log(response);
			//$('#state_div').html(response.state_option);
			var a = $(response);
			$('#check_out_block').html('');
			$('#check_out_block').append(a.filter('#check_out_block').html());
			
		}
		});
	});
	
	//This event will be fired when user will select any options from the country dropdown.
	$('#x_shipping_country').on('change',function(){
		//if any option value will be empty #5
		if ($('#x_shipping_country').val() == '')
		{
		$('#x_shipping_country').css('color','#999');
		}
		//Else add a specific color to country dropdown #5
		else
		{
			$('#x_shipping_country').css('color','rgba(0, 0, 0, 0.75)');
		}
	});
	
	//This will be fired when user will click on "continue shopping button" on cart modal
	$(document.body).off('click').on('click','.continue_shopping',function(){
		//Closes the cart modal
		$('#cart_modal').foundation('reveal', 'close');
	});
	//This event will be fired when user will select any options from the month dropdown
	$('#exp_month').on('change',function(){
		//if any option value will be empty #7
		if ($('#exp_month').val() == '')
		{
			//Add grey color. #7
			$('#exp_month').css('color','#999');
		}
		//Else add a specific color to month dropdown #7
		else
		{
			$('#exp_month').css('color','rgba(0, 0, 0, 0.75)');
		}
		
		//if year dropdown will not be selected #8
		if ($('#exp_year option:selected').val() == '')
		{
			//Shows the error message #8
			$(this).parent().addClass("error");
		}
	});
	
	//This event will be fired when user will select any options from the year dropdown
	$('#exp_year').on('change',function(){
		//if any option value will be empty #9
		if ($('#exp_year').val() == '')
		{
			//Add grey color. #9
			$('#exp_year').css('color','#999');
		}
		//Else add a specific color to year dropdown #9
		else
		{
			$('#exp_year').css('color','rgba(0, 0, 0, 0.75)');
		}
		//if month dropdown will not be selected #10
		if ($('#exp_month option:selected').val() == '')
		{
			//Shows error message #10
			$(this).parent().addClass("error");
		}
	});
	//This event will be fired whenever the browser window's size is changed
    $(window).resize(function() {
		//if browser window width lies in between 22em and 40em #11
		if (window.matchMedia('(max-width: 22em)').matches && window.matchMedia('(max-width: 40em)').matches)
		{
		   //Makes icon size of icon bar smaller #11
		   $(".item i").removeClass("fa-2x");
		   $(".item i").addClass("fa-1g");
	   
		}
		//if browser window width lies in between 40.063em and 44em or below 23.8em #12
	   if ((window.matchMedia('(min-width:  40.063em)').matches && window.matchMedia('(max-width:  44em)').matches) ||
		   window.matchMedia('(max-width:  23.8em)').matches )
		{
			//Makes "add to cart" button smaller #12
		   $(document).find('.add_to_cart').addClass("tiny");
		}
		//Else retain its original size #12
		else
		{
		   $(document).find('.add_to_cart').removeClass("tiny");
		}
		//if browser window size is above 40.063em #13
		if (window.matchMedia('(min-width:  40.063em)').matches)
		{
			//Setting height of "Why choose Professional Supplements?" paragraph for adding read more option #13
		   $('#txt').readmore({maxHeight: 134});
		   //if html element having id "stick-content-sticky-wrapper" does not exist #14
		   if ( $( "#stick-content-sticky-wrapper" ).length < 1)
		   {
				//Adding top spacing to the sticky div having id as "stick-content" #14
			   $("#stick-content").sticky({ topSpacing: 0,
									 responsiveWidth:true});
		   }
		   //Setting stick-content div width which will be equal to projectBrandSupplementsTable width #13
		   var product_table_width = $('#projectBrandSupplementsTable').width()+'px';
		   $('#stick-content').css("width", product_table_width);
		   //if html element having id "check_out_block-sticky-wrapper" does not exist #15
		   if ( $( "#check_out_block-sticky-wrapper" ).length < 1)
		   {
				//Adding top spacing to the sticky div having id as "check_out_block" #15
			   $("#check_out_block").sticky({ topSpacing: 3});
		   }
		   //Setting "check_out_block" div width which will be equal to width of div having id as "check_out_block-sticky-wrapper" #13
		   var check_out_block_width =  $('#check_out_block-sticky-wrapper').width()+'px';
		   $('#check_out_block').css("width", check_out_block_width);
		   //setting product listing table header width #13
		   set_header_width();
		}
		//if browser window width is below 40em #16
		if (window.matchMedia('(max-width:  40em)').matches)
		{
		  //for chrome and safari browser #17
		   if (navigator.userAgent.search("Chrome") >= 0 || navigator.userAgent.search("Safari") >= 0 )
		   {
				//Set height of mobile sticky content(small screen) #17
				$("#mobile_stick_content-sticky-wrapper").css("height","145px");
		   }
		   //Set width of check_out_block div #16
		   var check_out_block_width =  $('.checkout_block').width()+'px';
		   $('#check_out_block').css("width", check_out_block_width);
		   //Remove sticky attribute of checkout block for small screen devices #16
		   $('#check_out_block-sticky-wrapper').removeClass('is-sticky');
		   $('#check_out_block').css('position','absolute');
		}
    });
	//This event will be fired when user will select any options from the country dropdown
	$('#x_country').on('change',function() {
	   var country_abbr = $('#x_country option:selected').val();
	   var country_param = 'country_abbr='+country_abbr;
	   //Get state list as per country
	   $.ajax({
		   type:"POST",
		   async: "false",
		   url:"./get_state_list.php",
		   data: country_param,
		   dataType:'json',
		   success:function(response)
		   {
			   //console.log(response);
			   $('#state_div').html(response.state_option);
			   
		   }
	   });
	});
	
	//This event will be fired when user will select any options from the SHIPPING country dropdown
	$('#x_shipping_country').on('change',function() {
	   var country_abbr = $('#x_shipping_country option:selected').val();
	   var country_param = 'country_abbr='+country_abbr;
	   //Get state list as per country
	   $.ajax({
		   type:"POST",
		   async: "false",
		   url:"./get_shipping_state_list.php",
		   data: country_param,
		   dataType:'json',
		   success:function(response)
		   {
			   //console.log(response);
			   $('#shipping_state_div').html(response.state_option);
			   
		   }
	   });
	});
		
	// This event will be fired when user will click on "Place Order" button.
	$(document.body).on('click','#checkout_submit',function( ) {
	   // Submit checkout form.
	   //alert('User selected the Place Order button.');
	   //console.log('User selected the Place Order button.');
	   $('#checkout_form').submit();
	});
	
	// This event will be fired when checkout_form will be submitted and all fields are validated properly.
	// $('#checkout_form').off('valid.fndtn.abide').on('valid.fndtn.abide', function () {
	$('#checkout_form').on('valid.fndtn.abide', function () {
		
		//alert('User selected Place Order button AND Order Form passed ALL validations.');
		//console.log('User selected Place Order button AND Order Form passed ALL validations.');
	  	if($("#x_shipping_state").val() === ""){
		    alert('Shipping State is required');
		    return false;
	  	}

	  	if($("#x_state").val() === ""){
		    alert('Billing State is required');
		    return false;
	  	}

		$('#checkout_submit').removeClass('disabled');
		
		/*
		We are currently doing the processing below in a UI Modal where the transaction status message will be displayed. This needs to become a separate page for a successful transaction called ../order_success/index.php because we need to use javascript tracking code on the page to communicate to third party marketing services such as Google Adwords and Bing Ads that track advertising (pay-per-click) to successful outcomes i.e. a completed sale. With the successful outcome being a modal on the ../checkout.php page every hit on this page is triggering a "successful sale" to these outside services even if the user never Places the Order.		
		*/
		
		// Appends checkout_modal div which will contain the contents of checkout modal.
		$('#checkout_submit_div').append('<div id="checkout_modal" class="reveal-modal" data-reveal ></div>');
		// Opens modal showing a specific message.
		$.blockUI({ message: "<h3>Your Transaction is processing.<br/>Please wait..</h3><div class='row' id='response_message'></div>",baseZ: 1005 });
		$('.blockUI.blockMsg.blockPage').addClass("block_ui_confirm");
		
		// Opens checkout modal.

		$.ajax({
			type:"POST",
			async: "false",
			url: './checkout_ajax.php',
			data: $('#checkout_form').serialize(),
			dataType:'json',
			cache: false,
			success:function(response_checkout)
			{
				var first_name = $('#x_first_name').val();
				var last_name = $('#x_last_name').val();
				var address = $('#x_address').val();
				var city = $('#x_city').val();
				var country = $('#x_country option:selected').val();
				var state = $('#x_state option:selected').val();
				var zip = $('#x_zip').val();
				var email = $('#x_email').val();
				var phone = $('#x_phone').val();
				var shipping_same = $('#x_shipping_same').val();
				var shipping_type = $('#x_shipping_type option:selected').val();
				var shipping_first_name = $('#x_shipping_first_name').val();
				var shipping_last_name = $('#x_shipping_last_name').val();
				var shipping_address = $('#x_shipping_address').val();
				var shipping_city = $('#x_shipping_city').val();
				var shipping_country = $('#x_shipping_country option:selected').val();
				var shipping_state = $('#x_shipping_state option:selected').val();
				var shipping_zip = $('#x_shipping_zip').val();
				var shipping_email = $('#x_shipping_email').val();
				var shipping_phone = $('#x_shipping_phone').val();
				
				$.ajax({
					type:"POST",
					async: "false",
					url:"./save_checkout_session.php",
					data: {
							fname  : first_name,
							lname  : last_name,
							address: address,
							city   : city,
							country: country,
							state  : state,
							zip    : zip,
							email  : email,
							phone  : phone,
							shipping_same : shipping_same,
							shipping_type : shipping_type,
							shipping_first_name  : shipping_first_name,
							shipping_last_name  : shipping_last_name,
							shipping_address: shipping_address,
							shipping_city   : shipping_city,
							shipping_country: shipping_country,
							shipping_state  : shipping_state,
							shipping_zip    : shipping_zip,
							shipping_email  : shipping_email,
							shipping_phone  : shipping_phone
						  },
					dataType:'json',
					success:function(response)
					{
						$('#response_message').html('Redirecting... please wait...');
						window.location.replace(response_checkout.url);
					}
				});
			}
		});
		
		/*
		$('#checkout_modal').foundation('reveal', 'open', {
		   type:"POST",
		   async: "false",
		   url: './checkout_ajax.php',
		   data: $('#checkout_form').serialize(),
		   complete: function()
		   { 

				// Store all form data into session through ajax AFTER CC PROCESS.
				var first_name = $('#x_first_name').val();
				var last_name = $('#x_last_name').val();
				var address = $('#x_address').val();
				var city = $('#x_city').val();
				var country = $('#x_country option:selected').val();
				var state = $('#x_state option:selected').val();
				var zip = $('#x_zip').val();
				var email = $('#x_email').val();
				var phone = $('#x_phone').val();
				var shipping_same = $('#x_shipping_same').val();
				var shipping_type = $('#x_shipping_type option:selected').val();
				var shipping_first_name = $('#x_shipping_first_name').val();
				var shipping_last_name = $('#x_shipping_last_name').val();
				var shipping_address = $('#x_shipping_address').val();
				var shipping_city = $('#x_shipping_city').val();
				var shipping_country = $('#x_shipping_country option:selected').val();
				var shipping_state = $('#x_shipping_state option:selected').val();
				var shipping_zip = $('#x_shipping_zip').val();
				var shipping_email = $('#x_shipping_email').val();
				var shipping_phone = $('#x_shipping_phone').val();
				
				$.ajax({
					type:"POST",
					async: "false",
					url:"./save_checkout_session.php",
					data: {
							fname  : first_name,
							lname  : last_name,
							address: address,
							city   : city,
							country: country,
							state  : state,
							zip    : zip,
							email  : email,
							phone  : phone,
							shipping_same : shipping_same,
							shipping_type : shipping_type,
							shipping_first_name  : shipping_first_name,
							shipping_last_name  : shipping_last_name,
							shipping_address: shipping_address,
							shipping_city   : shipping_city,
							shipping_country: shipping_country,
							shipping_state  : shipping_state,
							shipping_zip    : shipping_zip,
							shipping_email  : shipping_email,
							shipping_phone  : shipping_phone
						  },
					dataType:'json',
					success:function(response)
					{
						// //console.log(response);
						// window.location.replace("./");
												
					}
				});

			   $.unblockUI(); 
		   } 
		});
		*/	
			
		// This event will be fired when checkout modal will be closed.
		$(document).on('close.fndtn.reveal', '[data-reveal]', function (){
		   window.location.reload(true);
		   
		 });
	});
	
	// This event will be fired when checkout form has invalid field data.
	$('#checkout_form').on('invalid.fndtn.abide', function () {
		$('#checkout_submit').removeAttr( "data-reveal-id" );
		$('#checkout_modal').remove();
		// Opens a modal showing specific message.
		$.blockUI({ message: "<h5>Please fill all required fields </h5>",timeout: 700,baseZ: 1005 });
		$('#checkout_submit').addClass('disabled');	 
	});
	
	// This event will be fired when user will click on "continue shopping" button on checkout cart.
	$(document.body).on('click','.check_out_button_shopping',function(){
	   // Store all form data into session through AJAX. 
		var first_name = $('#x_first_name').val();
		var last_name = $('#x_last_name').val();
		var address = $('#x_address').val();
		var city = $('#x_city').val();
		var country = $('#x_country option:selected').val();
		var state = $('#x_state option:selected').val();
		var zip = $('#x_zip').val();
		var email = $('#x_email').val();
		var phone = $('#x_phone').val();
		var shipping_same = $('#x_shipping_same').val(); // type same?
		var shipping_type = $('#x_shipping_type option:selected').val();
		var shipping_first_name = $('#x_shipping_first_name').val();
		var shipping_last_name = $('#x_shipping_last_name').val();
		var shipping_address = $('#x_shipping_address').val();
		var shipping_city = $('#x_shipping_city').val();
		var shipping_country = $('#x_shipping_country option:selected').val();
		var shipping_state = $('#x_shipping_state option:selected').val();
		var shipping_zip = $('#x_shipping_zip').val();
		var shipping_email = $('#x_shipping_email').val();
		var shipping_phone = $('#x_shipping_phone').val();
				
		$.ajax({
			type:"POST",
			async: "false",
			url:"./save_checkout_session.php",
			data: {
					fname  : first_name,
					lname  : last_name,
					address: address,
					city   : city,
					country: country,
					state  : state,
					zip    : zip,
					email  : email,
					phone  : phone,
					shipping_same  : shipping_same,
					shipping_type  : shipping_type,
					shipping_first_name  : shipping_first_name,
					shipping_last_name  : shipping_last_name,
					shipping_address: shipping_address,
					shipping_city   : shipping_city,
					shipping_country: shipping_country,
					shipping_state  : shipping_state,
					shipping_zip    : shipping_zip,
					shipping_email  : shipping_email,
					shipping_phone  : shipping_phone
				  },
			dataType:'json',
			success:function(response)
			{
				//console.log(response);
				 window.location.replace(home_page_url);	
			}
			
		});
   });

	$(document).find('.add_to_cart').parent().find('select').on('change', function(){
		var selected_value = $(this).val();
		var select_id = $(this).attr('id');
		$(document).find('.add_to_cart').parent().find('select').each(function(){
			if ($(this).attr('id') == select_id)
			{
				$(this).val(selected_value).attr('selected', 'selected');
			}
		});	
	});
	
	$(document).find('.add_to_cart').parent().find('[name =q]').on('keyup',function(){
	  var value = $(this).val();
	  var id = $(this).attr('id');

      var start = this.selectionStart,
        end = this.selectionEnd;

	  $(document).find('.add_to_cart').parent().find('[name =q]').each(function(){
	    if($(this).attr('id') == id)
	    {
	      $(this).val(value);
	       this.setSelectionRange(start, end);
	    }
	  });
	});
	
	// This event will be fired when user will click on "add to cart" button.
	//$(document).find('.add_to_cart').off('click').on('click',function(){alert();
	$(document).on('click', '.add_to_cart', function(){
		//Getting data from "data" attribute	  
		var a=$(this).data('id');
		var b=$(this).data('usage_id');
		var c = $(this).data('form');
		var checkout_page = $(this).data('checkout_page');
		var select_form = "#"+c;
		var select_qty = select_form+' '+ '#qty_'+a;
		var quantity = $(select_qty).val();
		//Check if quantity is number or not #18
		if (is_number(quantity))
		{
			//getting the last selected option
			var lastSelect = -1;

			for(var i = 0 ; i < 10 ; i++ ){
				if($('#option_level' + i + '_' + b).length > 0){
					lastSelect = i;
				}else{
					break;
				}
			}

			//if(lastSelect >=0 ){
			var selected = $('#option_level' + lastSelect + '_' + b + ' select');
			var last_id =  selected.val();
			//}


			var parameter = {
						pid : a,
						p_usage_id : b,
						p_last_selected : last_id		
					   };
		//var selector_quantity = "#prd-qty_"+a;
		//var qant=$(selector_quantity).val();
		//var url="cart.php?pid="+a+"&p_usage_id="+b+"&q="+qant+"&action=add";
	   
		//$(document).find('.add_to_cart').attr("href",url);
		
		//$(document).find('.add_to_cart').attr("data-reveal-id","cart_modal");
		//$(select_form).append('<div id="cart_modal" class="reveal-modal" data-reveal></div>');
		// $(document).find('.add_to_cart').attr("data-reveal-ajax","true");
			if(checkout_page)
			{
				$.ajax({
					type 	: 	"POST",
					async	: 	"false",
					url 	: 	cart_url,
					data 	: 	$(select_form).serialize() + '&' + $.param(parameter),
					success :   function(response)
					{
						 location.reload();
					}
				});
				return false;
			}
			else
			{
				$('#cart_modal').foundation('reveal', 'open', {
					type:"POST",
					async: "false",
					url: cart_url,
					data:$(select_form).serialize() + '&' + $.param(parameter)
				});
				$(document).on('closed.fndtn.reveal', '[data-reveal]', function (){
					//$('#cart_modal').remove();
					//$(document).find('.add_to_cart').removeAttr( "data-reveal-id" );
				});
			}
		}
		//Else show alert modal #18
		else
		{
			var message = 'Please Enter Valid data';
			alert_modal(message);
		}
		return false;
			   
	});
	
	// This event will be fired when user will click on learn more link.
	$('.learn_more').on('click',function(){
		//$(this).removeClass('open');
		var pro_id = $(this).data('project_id');
		var pbrand_product_usage_id = $(this).data('brand_product_usage_id');
		var pid = $(this).data('id');
		var parameter = {
			                project_id : pro_id,
							brand_product_usage_id : pbrand_product_usage_id,
							id : pid
							
			             };
		//opens learn more modal
		$('#learn_modal').foundation('reveal','open',{
			type: "POST",
			async: "false",
			url: './product.php',
			data: $.param(parameter)
			});
		return false;
		
	});
	
	// Checks for touchscreen devices.
	function is_touch_device() {
	return (('ontouchstart' in window)
      || (navigator.MaxTouchPoints > 0)
      || (navigator.msMaxTouchPoints > 0));
	}
	//if touch screen device will be present #19
	if (is_touch_device()) {
		//removes the tooltips #19
		$('.learn_more').removeAttr('data-selector');
	}

	//catch all options for securse option search	
	rebindOptions(true);

	$('.quantity_item').on('keyup',function(){
		////console.log($(this).data('id'));

		var group = parseInt($(this).data('id'));
  
  		//var ctrl = this; 		
   		//var xstart = this.selectionStart,
        //xend = this.selectionEnd;
        ////console.log(this.selectionStart);
		if(typeof delay_check[group] == 'undefined'){
			delay_check[group] = {running: false, qty:1};
		}
		
		if(delay_check[group].running){
			//this.setSelectionRange(xstart, xend);
			return;
		}else{
			var q = parseInt($(this).val());
			if(isNaN(q)) return;
			if(q <= 0) return;

			delay_check[group].running = true;
			//delay_check[group].qty = q;
			////console.log('Runnig : ' + group);

			setTimeout(function(){
				////console.log('tic');
				////console.log(group);
				refreshPrice(group);
		   		//var start = ctrl.selectionStart,
		        //end = ctrl.selectionEnd;
				//ctrl.setSelectionRange(start, end);
				delay_check[group].running = false;
			},500);
			
			//return false;
		}

	});
  
});

/**
	* Method Name : is_number
	* Desc : Used to check whether parameter is a positive number or not(not fractional)
	* @access public
	* @param Integer n parameter
	* @return Integer
**/
function is_number(n)
{
	
	if (isFinite(n) && n > 0 && n % 1 === 0)
	{
		return 1;
	}
	
	else
	{
		return 0;
	}
}

/**
	* Method Name : alert_modal
	* Desc : Used to show message on alert modal
	* @access public
	* @param String message message displayed on alert modal
	* @return void
**/
function alert_modal(message)
{
	var alert_msg = '<div class="row" >'+
			'<div class="small-12 medium-12 large-12 columns" style="padding-top:20px" >'+
				'<p>'+message+'</p>'+
			'</div>'+
		'</div>'+
		'<div class="row collapse" >'+
			'<div class="small-12 medium-12 large-12 columns" >'+
				'<input class="tiny button radius" type="button" id="ok" value="Ok" />'+
			'</div>'+	
		'</div>';
	$.blockUI({ message: alert_msg,css: { top: '3%', border: '0px' }, baseZ: 1005 });
	
	$('.blockUI.blockMsg.blockPage').addClass("block_ui_confirm");
	$('#ok').click(function() { 
		$.unblockUI(); 
		return false; 
	})
}
	/*
	 *cart javascript starts here
	 */

/**
	* Method Name : del
	* Desc : Used to delete item on shopping cart(Large screen)
	* @access public
	* @param Integer pid item index no.(not product id but index no. of item on cart)
	* @return boolean
**/
function del(pid)
{
	$.blockUI({ message: $('#confirm_modal'),css: { top: '3%', border: '0px' }, baseZ: 1005 });
	$('.blockUI.blockMsg.blockPage').addClass("block_ui_confirm");
	$(document).on("click","#yes",function() { 
	$.blockUI({ message: progress_wheel,css: {border: '0px',backgroundColor:'#616161',top: '25%' }, baseZ: 1005 }); 
	$.ajax({
			type:"POST",
			async: "false",
				url: cart_url,
				data: {
							command : 'delete',
							pid     : pid
					   },
				dataType:'html',
				
				success:function(r)
				{
					var a = $(r);
					$('#shopping_cart_container').html(a.filter('#shopping_cart_container').html());
				},
				complete: function()
				{ 
					   $.unblockUI(); 
				
				} 
				
			});
	});
	$('#no').click(function() { 
		$.unblockUI(); 
		return false; 
	});
}

/**
	* Method Name : update_cart
	* Desc : Used to update quantity on shopping cart(For large screen)
	* @access public
	* @param Integer pid item index no.(not product id but index no. of item on cart)
	* @return void
**/
function update_cart(pid)
{
	var qant = 'product_'+pid;
	var select_qant = '#product_'+pid;
	var quantity = $(select_qant).val();
	if (is_number(quantity))
	{
		
		var parameter = {
						command : 'update',
						pid     :  pid
					};
		$.ajax({
					type:"POST",
					async: "false",
						url: cart_url,
						data:  $.param(parameter) + '&'+qant+'='+quantity,
						dataType:'html',
						
						success:function(r)
						{
							 var a = $(r);
							 $('#shopping_cart_container').html(a.filter('#shopping_cart_container').html());						 
						}
						
				});
	}
	else
	{
		var message = 'Please Enter Valid data';
		 alert_modal(message);
		
	}
}

/**
	* Method Name : mob_update_cart
	* Desc : Used to update quantity on shopping cart(For small screen)
	* @access public
	* @param Integer pid item index no.(not product id but index no. of item on cart)
	* @return void
**/
function mob_update_cart(pid)
{
	var qant = 'mob_product_'+pid;
	var select_qant = '#mob_product_'+pid;
	var quantity = $(select_qant).val();
	var parameter = {
						mob_command : 'update',
						mob_pid     :  pid
							 
					};
	if (is_number(quantity))
	{
		$.ajax({
					type:"POST",
					async: "false",
					url: cart_url,
					data: $.param(parameter) + '&'+qant+'='+quantity,
					dataType:'html',
					
					success:function(r)
					{
						  var a = $(r);
						 $('#shopping_cart_container').html(a.filter('#shopping_cart_container').html());
					}
						
			  });
	}
	else
	{
		var message = 'Please Enter Valid data';
		alert_modal(message);
	}
}

/**
	* Method Name : mob_del
	* Desc : Used to delete item on shopping cart(small screen)
	* @access public
	* @param Integer pid item index no.(not product id but index no. of item on cart)
	* @return boolean
**/
function mob_del(pid){
	$.blockUI({ message: $('#confirm_modal'),css: { top: '3%', border: '0px' }, baseZ: 1005 });
	$('.blockUI.blockMsg.blockPage').addClass("block_ui_confirm");
	$('#yes').click(function() { 
	$.blockUI({ message: progress_wheel,css: {border: '0px',backgroundColor:'#616161',top: '36%' }, baseZ: 1005 });
	$.ajax({
			type:"POST",
			async: "false",
				url: cart_url,
				data: {
							mob_command : 'delete',
							mob_pid : pid
					   },
				dataType:'html',
				
				success:function(r)
				{
					var a = $(r);
					$('#shopping_cart_container').html(a.filter('#shopping_cart_container').html());
				},
				complete: function()
				{ 
				
				  $.unblockUI(); 
				}
				
			});
	});
	
	$('#no').click(function() { 
		$.unblockUI(); 
		return false; 
	});
}

/**
	* Method Name : medium_update_cart
	* Desc : Used to update quantity on shopping cart(For medium screen)
	* @access public
	* @param Integer pid item index no.(not product id but index no. of item on cart)
	* @return void
**/
function medium_update_cart(pid)
{
	var parameter = {
						medium_command : 'update',
						medium_pid     :  pid		 
					};
	var qant = 'medium_product_'+pid;
	var select_qant = '#medium_product_'+pid;
	var quantity = $(select_qant).val();
	
	if (is_number(quantity))
	{
		$.ajax({
			type:"POST",
			async: "false",
				url: cart_url,
				data: $.param(parameter) + '&'+qant+'='+quantity,
				dataType:'html',
				
				success:function(r)
				{
					var a = $(r);
					$('#shopping_cart_container').html(a.filter('#shopping_cart_container').html());
				}
		});
	}
	else
	{
		var message = 'Please Enter Valid data';
		alert_modal(message);
	}
}

/**
	* Method Name : medium_del
	* Desc : Used to delete item on shopping cart(medium screen)
	* @access public
	* @param Integer pid item index no.(not product id but index no. of item on cart)
	* @return boolean
**/
function medium_del(pid){
	
	$.blockUI({ message: $('#confirm_modal'),css: { top: '3%', border: '0px' }, baseZ: 1005 });
	$('.blockUI.blockMsg.blockPage').addClass("block_ui_confirm");
	
	$('#yes').click(function() {
		
		$.blockUI({ message: progress_wheel,css: {border: '0px',backgroundColor:'#8C8C8C',top: '36%' }, baseZ: 1005 });
		$.ajax({
				type:"POST",
				async: "false",
					url: cart_url,
					data: {
								medium_command : 'delete',
								medium_pid : pid
						   },
					dataType:'html',
					success:function(r)
					{
						var a = $(r);
						$('#shopping_cart_container').html(a.filter('#shopping_cart_container').html());
					},
					complete: function()
					{ 
					
					  $.unblockUI(); 
					}
		});
	});
	
	$('#no').click(function() { 
		$.unblockUI(); 
		return false; 
	});
}

	
	 /*
	 *cart javascript ends here
	 */
	 
/**
	* Method Name : set_header_width
	* Desc : Used to set width of header of product listing table 
	* @access public
	* @param void
	* @return void
**/	 
function set_header_width()
{
	var image_width = $('#projectBrandSupplementsTable td:first').width()+'px';
	var desc_width = $('#projectBrandSupplementsTable td:nth-child(2)').width()+'px';
	var purchase_width = $('#projectBrandSupplementsTable td:nth-child(3)').width()+'px';
	$('#projectBrandSupplementsTableHeader th:first').css("width", image_width);
	$('#projectBrandSupplementsTableHeader th:nth-child(2)').css("width", desc_width);
	$('#projectBrandSupplementsTableHeader th:nth-child(3)').css("width", purchase_width);
}

function FillBilling(f) {
	if(f.shipping_same.checked == true) {
		f.x_first_name.value = f.x_shipping_first_name.value;
		f.x_last_name.value = f.x_shipping_last_name.value;
		f.x_address.value = f.x_shipping_address.value;
		f.x_city.value = f.x_shipping_city.value;
		var country_abbr = $('#x_shipping_country option:selected').val();
		var state_abbr = '';
		if(country_abbr == 'US' || country_abbr == 'CA') {
			state_abbr = $('#x_shipping_state option:selected').val();
		}else{
			state_abbr = $('#x_shipping_state').val();
		}
		var country_param = 'country_abbr='+country_abbr+'&state_abbr='+state_abbr;
		//Get state list as per country
		$.ajax({
		   type:"POST",
		   async: "false",
		   url:"./get_state_list.php",
		   data: country_param,
		   dataType:'json',
		   success:function(response)
		   {
			   //console.log(response);
			   $('#state_div').html(response.state_option);

		   }
		});
		$("#x_country").val($('#x_shipping_country option:selected').val());
		if ($('#x_country').val() == '') {
			$('#x_country').css('color','#999');
		}else{
			$('#x_country').css('color','rgba(0, 0, 0, 0.75)');
		}
		f.x_zip.value = f.x_shipping_zip.value;
		f.x_phone.value = f.x_shipping_phone.value;
		f.x_email.value = f.x_shipping_email.value;
	}else{
		f.x_first_name.value = '';
		f.x_last_name.value = '';
		f.x_address.value = '';
		f.x_city.value = '';
		//$("#x_country").val('');
		//$('#x_country').css('color','#999');
		$("#x_country").val('US');
		var country_param = 'country_abbr=US&state_abbr=';
		//Get state list as per country
		$.ajax({
		   type:"POST",
		   async: "false",
		   url:"./get_state_list.php",
		   data: country_param,
		   dataType:'json',
		   success:function(response)
		   {
			   //console.log(response);
			   $('#state_div').html(response.state_option);
		   }
		});
		$("#x_country").val('');
		$('#x_country').css('color','#999');
		$("#x_state").val('');
		$('#x_state').css('color','#999');
		f.x_zip.value = '';
		f.x_phone.value = '';
		f.x_email.value = '';
	}
	}
	
	// This event will be fired when the user selects the APPLY BUTTON of Coupon Code
	function get_coupon_discount(f) {
		//if any option value will be empty #6
		if ($('#x_coupon_code').val() == '') {
			$('#x_coupon_code').css('color','#999');
		} else /*Else add a specific color to shipping method dropdown #6 */ {
			$('#x_coupon_code').css('color','rgba(0, 0, 0, 0.75)');
		}
		//removes error message 
		$('#x_coupon_code').parent().removeClass("error");
		//Gets shipping from get_shipping.php as per shipping_type and replace check_out_block content through ajax
		var coupon_code = $('#x_coupon_code').val();
		var coupon_code_param = 'coupon_code='+coupon_code;
		$.ajax({
			type:"POST",
			async: "false",
			url:"./get_coupon_discount.php",
			data: coupon_code_param,
			dataType:'html',
			success:function(response)
			{
				var r = jQuery.parseJSON(response);
				//console.log(r);
				//var a = $(response);
				//$('#check_out_block').html(a.filter('#check_out_block').html());
				if(response.ok ) {
				//
				}
				$('#check_out_block').html(r.result_html);
				$('#coupon_message').html(r.message);								
			}
		});
	}

//for recursive options for product
function rebindOptions(rebindAll,group,formgroup){

	////console.log('rebindAll = ' + rebindAll + '; group = ' + group);
	var selectQry = '';
	if(rebindAll){
		$('.recurse_option').unbind('change');
		selectQry = '.recurse_option';
	}else{
		$('#form_large_' + formgroup).find('select').unbind('change');
		selectQry = '#form_large_' + formgroup + ' .recurse_option';

	}
	////console.log(selectQry + ' : ' + $(selectQry).length);
	////console.log(selectQry);

	$(selectQry).on('change',function(e,h){

		////console.log(e.currentTarget.val());
		////console.log();
		////console.log();
		////console.log(); cart_url
		var p_pid = $(this).data('pid');
		var p_usage_id = $(this).data('usageid');
		var p_parent_id = $(this).val();
		
		var p_level =  parseInt($(this).data('level'));
		
		var p_price_type = $('#price_type_' + p_pid).val();

		var selected = $(this).find('option:selected');
		var p_parent_price = selected.data('price');
		var p_childs = selected.data('childs');
		
		var q = parseInt($('#qty_' + p_pid).val());
		
		if(isNaN(q) || q <= 0)
			q = 1;
 
		var p_selections = $(this).attr('data-selections').split('|');
		p_selections[p_level] = $(this).val();
		$(this).attr('data-selections',p_selections.join('|'));

		if(parseInt(p_childs) <= 0 ){
			for(var i = p_level + 1;i < 10; i++ ){
				$('#option_level' + i + '_' + p_usage_id).remove();
			}

		   setTimeout(function(){
		   		updatePriceFromLastSelected(p_usage_id,p_pid);
		   },10);
			

			return;
		}
		
		$('#form_large_' + p_usage_id).find('input,select,button,a').attr('disabled',true);
		

		//$.blockUI(); 

		$.ajax({
			type:"POST",
			async: "false",
				url: '../product/options.php',
				data: {
							pid : p_pid,
							p_usage_id : p_usage_id,
							parent_id : p_parent_id,
							level : p_level + 1,
							quantity : q,
							selections : p_selections.join('|'),
							price_type : p_price_type,
							parent_price : p_parent_price
					   },
				dataType:'html',
				
				success:function(p_response)
				{
					////console.log(r);

					for(var i = p_level + 1;i < 10; i++ ){
						$('#option_level' + i + '_' + p_usage_id).remove();
					}

					$('#option_level' + p_level + '_' + p_usage_id).after(p_response);



					//var a = $(r);
					//$('#shopping_cart_container').html(a.filter('#shopping_cart_container').html());
				},
				complete: function()
				{
				   $('#form_large_' + p_usage_id).find('input,select,button,a').attr('disabled',false);

				   setTimeout(function(){
				   		rebindOptions(false,p_usage_id,p_pid);
				   		updatePriceFromLastSelected(p_usage_id,p_pid);
				   },100);

				}
		});
		
	});

}

function updatePriceFromLastSelected(group,formgroup){
	var lastSelect = -1;
	for(var i = 0 ; i < 10 ; i++ ){
		if($('#option_level' + i + '_' + group).length > 0){
			lastSelect = i;
		}else{
			break;
		}
	}

	if(lastSelect >=0 ){
		var selected = $('#option_level' + lastSelect + '_' + group).find('.recurse_option option:selected');
		var price = selected.data('price');
		$('#productPrice' + group).find('.product-price').html('$' + price);
	}
}

function refreshPrice(group){
	
	delay_check[group].running = false;

	var q = parseInt($('#qty_' + group).val());
	var p_usage_id = $('#qty_' + group).attr('data-usageid');

	if(isNaN(q)) return;
	if(q <= 0) return;

	if(q == delay_check[group].qty) return;

	//delay_check[group].running = true;
	

	////console.log('Updating : ' + group + ', quantity : ' + q);

	var lastSelect = -1;

	for(var i = 0 ; i < 10 ; i++ ){
		if($('#option_level' + i + '_' + p_usage_id).length > 0){
			lastSelect = i;
		}else{
			break;
		}
	}

	// //console.log('last ' + lastSelect);
	if(lastSelect >=0 ){
		var selected = $('#option_level' + lastSelect + '_' + p_usage_id + ' select');
		var p_first = $('#option_level0_' + p_usage_id + ' select');
		var p_price_type = $('#price_type_' + group).val();
		var p_selections = selected.attr('data-selections');
		var p_parent_price = p_first.find('option:selected').data('price');
//		//console.log('#option_level' + lastSelect + '_' + group + ' select')
//		//console.log(p_selections);
//		return;

		var p_pid = p_first.data('pid');
		//var p_usage_id = p_first.data('usageid');
		var p_parent_id = p_first.val();
		
		//var p_level =  parseInt(p_first.data('level'));

		//var selected = p_first.find('option:selected');
		//var p_childs = selected.data('childs');

		//var p_selections = $(this).data('selections').split('|');
		//p_selections[p_level] = $(this).val();
		//$(this).attr('data-selections',p_selections.join('|'));


		$.ajax({
			type:"POST",
			async: "false",
				url: '../product/options.php',
				data: {
							pid : p_pid,
							p_usage_id : p_usage_id,
							parent_id : p_parent_id,
							level : 0,
							selections : p_selections, //.join('|')
							quantity: q,
							price_type: p_price_type,
							parent_price: p_parent_price
					   },
				dataType:'html',
				
				success:function(p_response)
				{
					////console.log(r);

					for(var i = 0;i < 10; i++ ){
						$('#option_level' + i + '_' + p_usage_id).remove();
					}

					$('#productPrice'  + p_usage_id ).after(p_response);

					delay_check[group].qty = q;
				},
				complete: function()
				{
				   //$('#form_large_' + p_usage_id).find('input,select,button,a').attr('disabled',false);

				   setTimeout(function(){
				   		rebindOptions(false,p_usage_id,p_pid);
				   		updatePriceFromLastSelected(p_usage_id,p_pid);
				   },200);

					   //$.unblockUI();
				} 
				
		});




	}

	
}
