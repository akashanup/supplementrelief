<?php

include_once('../includes/store_page_header.php');

// Added by Jay 8/29/15 to populate system_logs with page access metrics.
getPage('Checkout', $connection);
// include_once('../../includes/authorization.php');
$_SESSION['page']['name'] = 'Order Checkout';	
include('../../php/system_log_page_access.php'); 
if (isset($_POST['command']) && isset($_POST['pid']) && $_POST['command']=='delete' && $_POST['pid'] >= 0)
{
	remove_product(trim($_POST['pid']));
	
}
else if (isset($_POST['command']) && isset($_POST['pid']) &&  $_POST['command']=='update' && $_POST['pid'] >= 0){
	$pid=$_POST['pid'];
	$q=intval($_POST['product_'.$pid]);
	update_quantity($_POST['pid'],$q);
}

if (isset($_POST['mob_command']) && isset($_POST['mob_pid']) && $_POST['mob_command']=='delete' && $_POST['mob_pid'] >= 0)
{
	remove_product($_POST['mob_pid']);
}
else if (isset($_POST['mob_command']) && isset($_POST['mob_pid']) &&  $_POST['mob_command']=='update' && $_POST['mob_pid'] >= 0){
	$pid=$_POST['mob_pid'];
	$q=intval($_POST['mob_product_'.$pid]);
	update_quantity($_POST['mob_pid'],$q);
}

// Query for getting Country list used for Shipping and Billing select lists

// Puts United States and Canada at top of list for user convenience.
$sql_get_country_list = "SELECT country_abbr,
						country_name
						FROM country
						ORDER BY country_name asc";
$sql_get_top_country_list = 'SELECT 
							country_name,
							country_abbr
							FROM country
							WHERE country_abbr = "US"
							OR
							country_abbr = "CA"
							ORDER BY country_name desc';
$result_get_country_list = mysqli_query($connection, $sql_get_country_list);
$result_get_top_country_list = mysqli_query($connection, $sql_get_top_country_list );
if (!$result_get_country_list)
{
	die("Database get country list query failed.");
}
if (!$result_get_top_country_list)
{
	die("Database get top country list query failed.");
}

$sql_get_country_list = "SELECT country_abbr,
						country_name
						FROM country
						ORDER BY country_name asc";
$sql_get_top_country_list = 'SELECT 
							country_name,
							country_abbr
							FROM country
							WHERE country_abbr = "US"
							OR
							country_abbr = "CA"
							ORDER BY country_name desc';
$result_get_country_list2 = mysqli_query($connection, $sql_get_country_list);
$result_get_top_country_list2 = mysqli_query($connection, $sql_get_top_country_list );
if (!$result_get_country_list2)
{
	die("Database get country list query failed.");
}
if (!$result_get_top_country_list2)
{
	die("Database get top country list query failed.");
}

if(isset($_SESSION['enrollment']['project_id']) && $_SESSION['enrollment']['project_id'] > 0 ) $project_id = $_SESSION['enrollment']['project_id'];
else $project_id = public_project_id; // /store/includes/defines.php

// Currently used to get Order Terms of Sale. Could be extended to get Project Cart graphic.
$pq = 'SELECT * FROM projects WHERE id = '.$project_id;
$pr = mysqli_query($connection, $pq);
$prow = mysqli_fetch_assoc($pr);

if(empty($_SESSION['enrollment']['user_price_type'])) $_SESSION['enrollment']['user_price_type'] = USER_PRICE_TYPE; // /store/includes/defines.php

$sql_get_shipping_method_list = "SELECT   
pso.id, pso.shipping_option_type_code, 
pso.charge_amount,
pso.default_option, 
sot.name AS shipping_option_type_name,
sot.description AS shipping_option_type_description 
FROM project_shipping_options pso 
LEFT JOIN shipping_option_types sot ON pso.shipping_option_type_code = sot.code 
WHERE pso.project_id = ".$project_id." 
AND pso.price_type_code = '".$_SESSION['enrollment']['user_price_type']."' 
AND pso.effective_date <= CURDATE() 
AND (pso.end_date IS NULL OR pso.end_date >= CURDATE())  
AND pso.active = 1 
ORDER BY pso.seq ASC";

$result_shipping_method_list = mysqli_query($connection, $sql_get_shipping_method_list);
if (!$result_shipping_method_list)
{
	die("Database get shipping method list query failed.");
}

// Query to see if we display a Coupon Code field
$zq = 'SELECT count(*) as cCount FROM project_shopping_codes psc LEFT JOIN shopping_code_types sct ON psc.shopping_code = sct.code WHERE psc.project_id = '.$_SESSION['enrollment']['project_id'].' AND psc.active = 1 AND psc.effective_date <= CURDATE() AND (psc.end_date is NULL or psc.end_date > CURDATE()) AND psc.price_type_code = "'.$_SESSION['enrollment']['user_price_type'].'"';
$zr = mysqli_query($connection, $zq);
$zrow = mysqli_fetch_assoc($zr);
$_SESSION['coupon']['cCount'] = $zrow['cCount'];

// cleaning user credit card data from last failed transaction (kept so they can verify what they entered)

if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 300) /*5 mins*/) {
    unset($_SESSION['LAST_ACTIVITY']);
    unset($_SESSION['_INFO']);
}

?><!DOCTYPE html>
<html class="no-js <?php echo $ie9?'lt-ie9':'';   ?>" lang="en">

	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Order Checkout | SupplementRelief.com</title>
		<link rel="stylesheet" href="<?php echo '../'.STORE_APP_CSS; ?>" />
		<link rel="stylesheet" href="<?php echo '../'.STORE_STORE_CSS; ?>" />
		<script src="<?php echo '../'.STORE_MODERNIZR_JS; ?>"></script>
		<!--[if lt IE 9]>
		  <script src="<?php echo '../'.STORE_IE8_JS; ?>"></script>
		<![endif]-->
		<script src="<?php echo '../'.STORE_FOUNDATION_JQUERY_JS;?>"></script>
		<script src="<?php echo '../'.STORE_JQUERY_STICKY_JS; ?>"></script>
		<script src="<?php echo '../'.STORE_READMORE_JS; ?>"></script>
		<?php include_once('../'.STORE_JS_DEFINE); ?>
		<script src="<?php echo '../'.STORE_STORE_JS; ?>"></script>
		<script src="<?php echo '../'.STORE_MOBILE_JS; ?>"></script>
		<script src="<?php echo '../'.STORE_SCROLLUP_JS; ?>"></script>
		<script src="<?php echo '../'.STORE_BLOCKUI_JS; ?>"></script>
		<script src="<?php echo '../'.STORE_CHECKOUT_JS; ?>"></script>
		<!-- Google Analytics --> 
    	<script>
    	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    	
    	  ga('create', 'UA-19307539-4', 'auto');
    	  ga('send', 'pageview');
    	</script>		
	</head>
	
    <body class="check_out_body"> <!-- top container - sets background color only -->
    
	    <link href='https://fonts.googleapis.com/css?family=Open%20Sans:300,600,400' rel='stylesheet' type='text/css'>
		<!-- <link href='<?php echo FONT_AWESOME_CSS; ?>' rel='stylesheet' type='text/css'> -->
		<script src="<?php echo FONT_AWESOME_CSS?>"></script> 
	    <!--[if lt IE 9]><meta http-equiv="refresh" content="0; url=../legacy_browser/"><![endif]-->
	
	    <!-- 
	    <style>
	    	.myclass { color:#222; }
	    </style>
	    -->
     
    <div id="checkout_container" > <!-- parent check_out_body div - top margin 0, width 100%, center -->
    
      <div class="checkout_site_logo">
      		<!-- *JT images.host_url should be used here to get the Project's Checkout Logo for top of page branding -->
  	  		<!-- <img src="./store_img/checkout_site_logo.png" /> -->
      </div>
        
      <div id="check_bill" class="row collapse" > <!-- check_bill does not appear to be used in css or js -->

        <div class="small-12 medium-8 columns">
          	<div class="checkout_block">
	          
	        	<div class="row" id="header_checkout_div">
		            
		            <div class="small-12 medium-6 columns BBBLogo">
			            <h3><b>CHECKOUT</b><small>&nbsp;&nbsp;<i class="fa fa-asterisk fa-1g icon-redcart"></i><em> required items </em></small></h3>
            		</div>
	
					<!-- BBB Logo -->	
					<div class="small-6 medium-3 columns BBBLogo">
						
						<center><?php echo $_SESSION['application']['bbb_seal_code']; ?></center>
										
					</div>
					
					<!-- Authorize.net Merchant Seal -->			
					<div class="small-6 medium-3 columns">
					
						<?php echo $_SESSION['application']['authorize_net_seal_code']; ?>
						
					</div>
					
		        </div>
	                       
				<div class="checkout_block_body">
	            
	            	<div class="checkout_cart"> <!-- not used -->
	              
						<div id="checkout_cart_container"> <!-- for js only -->
							
							<?php
							//$pid=$_GET['pid'];
							
							?>
										
							<!-- Cart for Tablet and Desktop -->			
							<div class="show-for-medium-up">
								
								<form name="cart_form" id="cart_form" method="post">
									
									<input type="hidden" name="pid" />
									<input type="hidden" name="command" />
									<div class="panel" id="cart_medium_up_panel"> <!-- don't think id is used -->
									
									<?php
										// 1. See if user has added any items to shopping cart. If so display the cart items.
										if (isset($_SESSION['cart']) && !empty($_SESSION['cart']))
										{
											$max=count($_SESSION['cart']);
											// Looping through all items present in session #1
											for($i=0;$i<$max;$i++)
											{
												$pid=$_SESSION['cart'][$i]['productid'];
												$q=$_SESSION['cart'][$i]['qty'];
												$read_only = $_SESSION['cart'][$i]['read_only'];
												$price_type = $_SESSION['cart'][$i]['price_type'];
												$post_select1 = 'prd_'.$pid;
												$last_selected = $_SESSION['cart'][$i]['last_selected'];
												//Checking for select options #2
												if (isset($_SESSION['cart'][$i][$post_select1]) && !empty($_SESSION['cart'][$i][$post_select1]))
												 {
													$post_select_session = $_SESSION['cart'][$i][$post_select1];
													$p_price=get_price($pid,$q,$price_type,$post_select_session,$connection,$last_selected);
													$p_option_name = get_option_name($pid,$post_select_session,$connection);
												 }
												 else
												 {
													$p_price=get_price($pid,$q,$price_type,null,$connection,$last_selected);
													$p_option_name = '';
												 }
												$p_image=get_product_image($pid,$connection);
												$p_name=get_product_name($pid,$connection);
												if ($q ==0 ) continue;
											?>
									
											<div class="row" id="item_<?php echo $i; ?>" >
												<div class="medium-3 large-3 columns" >
													<center>
														<p>
															<img src="<?php echo $_SESSION['application']['root_media_host_url'].$p_image;  ?>" class="cart_image">
														</p>
													</center>
												</div>
												<div class="medium-9 large-9 columns" >
													<div class="row">
														<div class="medium-12 large-12 columns" >
															<p><b><?php echo $p_name; ?></b><br/>
															 <font size = "2"><cite><?php echo $p_option_name ?></cite></font></p>
														</div>
													</div>
													<div class="row" >
														<div class="medium-4 large-4 columns" style="text-align: center" >
															<?php
															if(!empty($read_only) && $read_only == 1)
															{
																echo "1";
															}
															else
															{
															?>
															<input id="quant_<?php echo $i; ?>" type="text" name="product_<?php echo $i; ?>" value="<?php echo $q; ?>" maxlength="3" size="1" style="text-align: right">
															<?php
															}
															?>															
														</div>
														<div class="medium-8 large-8 columns" >
															<p class="price_medium" id="price_<?php echo $i; ?>" style="text-align: left"><b> X <?php echo '$'.$p_price.'&nbsp;'.'='.'&nbsp;'.'$'.number_format((float)$p_price*$q, 2, '.', ''); ?></b></p>
														</div>
													</div>
													<div class="row" >
														<div class="medium-5 large-5 columns" >
															<a href="javascript:checkout_del(<?php echo $i; ?>)" class="expand del_checkout_button small button radius fa fa-trash-o fa-1x" style="text-align: center">&nbsp;Remove</a>
														</div>
														<div class="medium-1 large-1 columns" >
															&nbsp;
														</div>
														<div class="medium-5 large-5 columns" >
															<?php
															if(!empty($read_only) && $read_only == 1)
															{
																echo '';
															}
															else
															{
															?>
															<a href="javascript:checkout_update_cart(<?php echo $i; ?>)" class="expand update_checkout_button small button radius fa fa-refresh fa-1x" style="text-align: center">&nbsp;Update</a>
															<?php
															}
															?>																
														</div>
													</div>
													
												</div>
												<?php
												if ( $i < ($max-1))
												{
													echo '<hr>';
												}
												?>
											</div>
											<?php
											}
											?>
										</div>
										
										<div class="div_footer_cart"> <!-- div_footer_cart not used -->
											<div class="row">
												<div class="medium-12 columns">
													<div class="radius button_shopping expand success button check_out_button_shopping" >Continue Shopping</div>
												</div>
											</div>
										</div>
									
									<?php
										}
										else
										// 2. User has not added any items to cart so display user message.
										{
									?>
										<h3>You have not selected any products yet.</h3>
									</div>
									
									<div class="div_footer_cart"> <!-- div_footer_cart not used -->
										<div class="row">
											<div class="medium-12">
												<div class="radius button_shopping expand success button check_out_button_shopping" >Continue Shopping</div>
											</div>
										</div>
									</div>
											
									<?php
									  }
									  ?>
								</form>
											
							</div>
									
							<!-- Mobile Cart -->
							<div class="show-for-small-only">
								
								<form name="mobile_cart_form" id="mobile_cart_form" >
									
									<input type="hidden" name="mob_pid" />
									<input type="hidden" name="mob_command" />
									
									<?php
									// 3. See if user has added any items to shopping cart. If so display the cart items.

									if (is_array($_SESSION['cart']) && !empty($_SESSION['cart']))
									{
										echo '<div class="panel" id="cart_small_panel">'; // id cart_small_panel not used
										$max=count($_SESSION['cart']);
										// Looping through all items in the shopping cart
										for ($i = 0 ; $i < $max ; $i++)
										{
											$pid=$_SESSION['cart'][$i]['productid'];
											$q=$_SESSION['cart'][$i]['qty'];
											$read_only = $_SESSION['cart'][$i]['read_only'];
											$price_type = $_SESSION['cart'][$i]['price_type'];
											$post_select1 = 'prd_'.$pid;
											$last_selected = $_SESSION['cart'][$i]['last_selected'];
											//Checking for select options #4
											if (isset($_SESSION['cart'][$i][$post_select1]) && !empty($_SESSION['cart'][$i][$post_select1]))
											 {
												$post_select_session = $_SESSION['cart'][$i][$post_select1];
												$p_price=get_price($pid,$q,$price_type,$post_select_session,$connection,$last_selected);
												$p_option_name = get_option_name($pid,$post_select_session,$connection);
											 }
											 else
											 {
												$p_price=get_price($pid,$q,$price_type,null,$connection,$last_selected);
												$p_option_name = '';
											 }
											$p_image=get_product_image($pid,$connection);
											$p_name=get_product_name($pid,$connection);
											
											if ($q ==0 ) continue;
										
											?>
									        <div id="small_item_<?php echo $i;  ?>">
												<div class="row ">
													<div>
														<center><p><img src="<?php echo $_SESSION['application']['root_media_host_url'].$p_image;  ?>" class="cart_image"></p></center>
													</div>
													<div>
													  <p style="text-align: center;"><b><?php echo $p_name; ?></b><br/>
														<font size = "2"><cite><?php echo $p_option_name ?></cite></font>
													  </p>
													</div>
											    </div>
												<div class="row">
												  <div>
													<center><strong>Quantity</strong></center>
												  </div>
												</div>
												<div class="row">
													<div class="small-4 columns">&nbsp;</div>
													<div class="small-4 columns" style="text-align: center">
														<?php
														if(!empty($read_only) && $read_only == 1)
														{
															echo "1";
														}
														else
														{
														?>
														<input id="mob_quant_<?php echo $i; ?>" type="text" name="mob_product_<?php echo $i; ?>" value="<?php echo $q; ?>" maxlength="3" size="2" style="text-align: center;"/>
														<?php
														}
														?>
													</div>
													<div class="small-4 columns">&nbsp;</div>
												</div>
												<div class="row">
													<div style="text-align: center">
														<p id="small_price_<?php echo $i; ?>"><b> X <?php echo '$'.$p_price.'&nbsp;'.'='.'&nbsp;'.'$'.number_format((float)$p_price*$q, 2, '.', ''); ?></b></p>
													</div>
												</div>
												<div class="row">
													<div>
														<a href="javascript:checkout_mob_del(<?php echo $i; ?>)" class="small button radius expand fa fa-trash-o fa-1x">&nbsp;Remove</a>
													</div>
													<div>
														<?php
														if(!empty($read_only) && $read_only == 1)
														{
															echo '';
														}
														else
														{
														?>
														<a href="javascript:checkout_mob_update_cart(<?php echo $i; ?>)" class="small button radius expand fa fa-refresh fa-1x">&nbsp;Update</a>
														<?php
														}
														?>
													</div>
												</div>
											</div>
											<hr>
										<?php
										}
										?>
										<div class="row">
											<div class="radius full_width button check_out_button_shopping">Continue Shopping</div>
										</div>
									</div>
									<?php
									}
									else
									{
									// 4. User has not added any items to cart so display user message.
									?>
										<div class="panel">
											<b>You have not selected any products yet.</b>
											<div class="row">
												<div class="radius full_width button check_out_button_shopping">Continue Shopping</div>
											</div>
										</div>
										<?php
										}
										?>
								</form>
								
							</div>
							
						</div>
					
						<div id="confirm_modal" style="display:none; cursor: default">
							<div class="row">
								<div class="small-12 columns" style="padding-top:20px">
									<p>Are you sure you want to remove this?</p>
								</div>
							</div>
					
							<div class="row collapse">
								<div class="small-12 columns">
									<input class="tiny button radius" type="button" id="yes" value="Yes" />&nbsp;&nbsp;&nbsp;
									<input class="tiny button radius" type="button" id="no" value="No" /> 
								</div>										
							</div>
							
						</div>
						         
					</div>
													
					<div class="billing">
	            
						<form id="checkout_form" data-abide="ajax" data-options="timeout : 0" >
							
						<?php if(CHECK_OUT_FREE_SAMPLES == 'yes') { 
							
							// Build the Select List for Eligible Free Samples
                            
                            $current_timestamp = date('Y/m/d H:i:s');
                            $promotion_type_code = 'FREESAMP';
							$queryProjectProgramPromotions	= 'SELECT 
								pr.name, 
								pr.description, 
								pr.destination_url, 
								pr.link_text 
								/* im.host_url, 
								im.caption, 
								im.alt_text */
								FROM project_program_promotions ppp 
								LEFT JOIN promotions pr ON ppp.promotion_id = pr.id   
								/* LEFT JOIN images im ON 
								    (pr.image_id = im.content_asset_id AND 
								     im.size = "Promotion" AND 
								     im.usage_size = "Small" AND 
								     im.usage_shape = "Block" AND 
								     im.format = "JPG") */
								WHERE ppp.project_program_id = "'.$_SESSION['enrollment']['project_program_id'].'" 
								AND pr.project_id = "'.$_SESSION['enrollment']['project_id'].'"  
								AND ppp.active = "1" 
								AND ppp.effective_timestamp <= "'.$current_timestamp.'" 
								AND (ppp.end_timestamp IS NULL OR ppp.end_timestamp > "'.$current_timestamp.'")  
								AND ppp.active = "1" 
								AND pr.promotion_type_code = "'.$promotion_type_code.'" 
								AND pr.active = "1" 
								AND pr.effective_timestamp <= "'.$current_timestamp.'" 
								AND (pr.end_timestamp IS NULL OR pr.end_timestamp > "'.$current_timestamp.'")  
								ORDER BY pr.name ASC';
										
							// echo $queryProjectProgramPromotions . '<br /><hr />';

							$result_promotion = mysqli_query($connection, $queryProjectProgramPromotions);
							
							if (!$result_promotion) {
								show_mysqli_error_message($queryProjectProgramPromotions, $connection);
								die;
							}
														
							$promotion_options = '<option value="0" selected>Choose Free NuMedica Sample ...</option>';
							
							while($promotion_row = mysqli_fetch_array($result_promotion)) {
								$selected = '';
								if(($_SESSION['checkout']['free_sample'] ? $_SESSION['checkout']['free_sample'] : $row1['p']) == $promotion_row['name']) $selected = ' selected';
								$promotion_options .= '<option value="'.$promotion_row['name'].'" '.$selected.'>'.$promotion_row['name'].' - '.$promotion_row['description'].'</option>';
							}
											
							mysqli_free_result($result_promotion);
							
							?>

							<!-- #2 Free Sample -->

							<div class="billing_header">
								<h3>Free NuMedica Product Sample</h3>
							</div>
															
							<div class="row">
				                <div class="small-12 columns">
				                    
				                    <select name="free_sample" id="free_sample">
					    		        <?php echo $promotion_options; ?>
					    		  	</select>
				                    
				                </div>
				            </div>
            
                        <?php } ?>				            
       
	                     
		               	<?php if($_SESSION['coupon']['cCount'] > 0 ) { ?>
		               			               	
		               		<?php if(CHECK_OUT_PROMOTION_CODE == 'yes') { ?>
		               		
                                <!-- #1 Promotion Code 
                                   If Promotion Codes for the User Price Plan AND Promotions enabled - display the Promotion section 
						        -->   	
	
								<div class="billing_header">
									<h3>Promotion Code</h3>
								</div>
															
								<div class="row">
									<div class="small-12 columns">
										<p>If you have a valid Promotion Code, enter it then select the Apply button:</p>
									</div>
								</div>
																
								<div class="row">
								
								   <div class="small-12 columns">
								     <div class="row collapse">
								       <div class="small-3 columns">
								         <input class="billing_text" name="x_coupon_code" id="x_coupon_code" maxlength="10" value="<?php if (isset($_SESSION['coupon']['coupon_code'])){ echo $_SESSION['coupon']['coupon_code']; } ?>" placeholder="Code"></input>
								       </div>
								       <div class="small-2 columns end">
								         <a class="button postfix" onclick="get_coupon_discount(this.form);">Apply</a>     
								       </div>
								     </div>
								   </div>
								   								
									<div class="small-12 columns">
										<div id="coupon_message">
											<?php 
												if (isset($_SESSION['coupon']['coupon_code']) && ($_SESSION['coupon']['coupon_code'] != '') )
												{ echo $_SESSION['coupon']['coupon_code_message']; } 
											?>
										</div>
									</div>
									
								</div>
							
							<?php } ?>	
 								
						<?php }
					
						// #3 Populate Billing & Shipping Information for User if User is logged in and has Billing and/or Shipping data
						
						// show_array($_SESSION);
						
						if ( isset($_SESSION['user']['user_id']) && $_SESSION['user']['user_id'] > 0 ) {
						
							// Check for a Shipping Address to pre-populate checkout data for user
							
							$query_shipping_address = '
							SELECT 
							pe.first_name, 
							pe.last_name, 
							pe.email, 
							pe.phone,    
							ad.address, 
							ad.city, 
							st.state_abbr,
							ad.postal_code, 
							co.country_abbr   
							FROM locations lo 
							LEFT JOIN persons pe ON lo.person_id = pe.id 
							LEFT JOIN addresses ad ON lo.address_id = ad.id 
							LEFT JOIN states st ON ad.state_id = st.state_id 
							LEFT JOIN country co ON ad.country_id = co.country_id 
							WHERE pe.id = '.$_SESSION['user']['user_id'].'  
							AND lo.address_type_code = "SHIP" 
							AND lo.effective_date <= CURRENT_DATE 
							AND (lo.end_date is NULL or lo.end_date >= CURRENT_DATE)  
							LIMIT 1';
							
							// echo $query_shipping_address;
							// die();
																
							$result_shipping_address = mysqli_query($connection, $query_shipping_address);
							
							if (!$result_shipping_address) {
								show_mysqli_error_message($query_shipping_address, $connection);
								die;
							}
														
							$rsa = mysqli_fetch_array($result_shipping_address);
							
							// show_array($result_shipping_address);
																													
							$_SESSION['checkout']['shipping_first_name'] = $rsa['first_name'];
							$_SESSION['checkout']['shipping_last_name'] = $rsa['last_name'];
							$_SESSION['checkout']['shipping_address'] = $rsa['address'];
							$_SESSION['checkout']['shipping_city'] = $rsa['city'];
							$_SESSION['checkout']['shipping_zip'] = $rsa['postal_code'];
							$_SESSION['checkout']['shipping_state'] = $rsa['state_abbr'];
							$sales_tax = get_sales_tax($_SESSION['enrollment']['project_id'], $_SESSION['checkout']['shipping_state'], $connection);
							
							if (isset($sales_tax) && !empty($sales_tax)) $_SESSION['checkout']['state_tax'] = $sales_tax;
							else $_SESSION['checkout']['state_tax'] = 0.00;
							
							$_SESSION['checkout']['shipping_country'] = $rsa['country_abbr'];
							
							mysqli_free_result($result_shipping_address);				
							
							// Check for a Billing Address to pre-populate checkout data for user

							$query_billing_address = '
							SELECT 
							pe.first_name, 
							pe.last_name, 
							pe.email, 
							pe.phone,     
							ad.address, 
							ad.city, 
							st.state_abbr,
							ad.postal_code, 
							co.country_abbr  
							FROM locations lo 
							LEFT JOIN persons pe ON lo.person_id = pe.id 
							LEFT JOIN addresses ad ON lo.address_id = ad.id 
							LEFT JOIN states st ON ad.state_id = st.state_id 
							LEFT JOIN country co ON ad.country_id = co.country_id 
							WHERE pe.id = '.$_SESSION['user']['user_id'].' 
							AND lo.address_type_code = "BILL" 
							AND lo.effective_date <= CURRENT_DATE 
							AND (lo.end_date is NULL or lo.end_date >= CURRENT_DATE)  
							LIMIT 1';
							
							// echo $query_billing_address;
							// die();
																
							$result_billing_address = mysqli_query($connection, $query_billing_address);
							
							if (!$result_billing_address) {
								show_mysqli_error_message($query_billing_address, $connection);
								die;
							}							
							
							$rba = mysqli_fetch_array($result_billing_address);
							
							// show_array($rba);
																													
							$_SESSION['checkout']['first_name'] = $rba['first_name'];
							$_SESSION['checkout']['lname'] = $rba['last_name'];
							$_SESSION['checkout']['address'] = $rba['address'];
							$_SESSION['checkout']['city'] = $rba['city'];
							$_SESSION['checkout']['zip'] = $rba['postal_code'];
							$_SESSION['checkout']['state'] = $rba['state_abbr'];
							$_SESSION['checkout']['country'] = $rba['country_abbr'];
							$_SESSION['checkout']['email'] = $rba['email'];
							$_SESSION['checkout']['phone'] = $rba['phone'];										
							
							mysqli_free_result($result_billing_address);						
																																	
						}
					
						?>  
	                            
						<div class="billing_header">
			            	<h3>Shipping Information</h3> 
			         	</div>
	          
					 	<div class="row">
					 		<div class="small-12 columns">
	
					 			<span class="label">Choose Shipping Method</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br/>
					 			<select name="x_shipping_type" id="x_shipping_type" required>
									<?php
									//echo '<option value="" ';
									// Check if country is present in session or not #5
									if (empty($_SESSION['checkout']['shipping_type']))
									{
										echo '  selected';
									}
									echo '  disabled style="color: #999">Select Shipping Method</option>';
									?>
									   
									<?php
									// Get country list 
										while($shipping_method_array = mysqli_fetch_assoc($result_shipping_method_list))
										{
											echo '<option value="' . $shipping_method_array['id'] . '"';
											// Check for non-empty country in session #7
											if (!empty($_SESSION['checkout']['shipping_type']))
											{
												if ($_SESSION['checkout']['shipping_type'] == $shipping_method_array['id'])
												{
													echo ' selected="selected"';
												}
												
											}else{
												if ($shipping_method_array['default_option'] == 1)
												{
													echo ' selected="selected"';
													$_SESSION['checkout']['shipping_type'] = $shipping_method_array['id'];
												}
											}
											if($shipping_method_array['charge_amount'] > 0) $sname = ' ($'.$shipping_method_array['charge_amount'].' - '.$shipping_method_array['shipping_option_type_description'].')';
											else $sname = ' ('.$shipping_method_array['shipping_option_type_description'].')';
											echo '>'.$shipping_method_array['shipping_option_type_name'].$sname.'</option>';
										}
									?>
								</select>
								<small class="error" >Please Select Shipping Method</small>
								
							</div>
					 	</div>
	            
					 	<div class="row">
	          
				          	<div class="small-12 medium-6 columns" >
				              <span class="label">First Name</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br>
				              <input class="" type="text" required maxlength="50" name="x_shipping_first_name" id="x_shipping_first_name" value="<?php if (isset($_SESSION['checkout']['shipping_first_name'])){ echo $_SESSION['checkout']['shipping_first_name']; } ?>" placeholder="Shipping First Name"></input>
				              <small class="error">Invalid Shipping First Name</small>       
				            </div>
				            
				            <div class="small-12 medium-6 columns">
				            	<span class="label">Last Name</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br/>
				              <input class="" type="text" required maxlength="50" name="x_shipping_last_name" id="x_shipping_last_name" value="<?php if (isset($_SESSION['checkout']['shipping_last_name'])){ echo $_SESSION['checkout']['shipping_last_name']; } ?>" placeholder="Shipping Last Name"></input>
				              <small class="error">Invalid Shipping Last Name</small>
				            </div>
				            
				            <!--
				          	<div class="small-12 medium-6 columns" >
				              <span class="label">First Name</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br>
				              <input class="billing_text" type="text" required size="49" maxlength="50" name="x_shipping_first_name" id="x_shipping_first_name" value="<?php //if (isset($_SESSION['checkout']['shipping_first_name'])){ echo $_SESSION['checkout']['shipping_first_name']; } ?>" placeholder="Shipping First Name"></input>
				              <small class="error">Invalid Shipping First Name</small>       
				            </div>
				            
				            <div class="small-12 medium-6 columns">
				            	<span class="label">Last Name</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br/>
				              <input class="billing_text" type="text" required size="49" maxlength="50" name="x_shipping_last_name" id="x_shipping_last_name" value="<?php //if (isset($_SESSION['checkout']['shipping_last_name'])){ echo $_SESSION['checkout']['shipping_last_name']; } ?>" placeholder="Shipping Last Name"></input>
				              <small class="error">Invalid Shipping Last Name</small>
				            </div>
							-->
   
				        </div>
				        	          
					 	<div class="row">
	          
				            <div class="small-12 medium-6 columns">
				            	<span class="label">Address</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br/>
								<textarea class="" required  rows="2" name="x_shipping_address" id="x_shipping_address" placeholder="Shipping Address"><?php if (isset($_SESSION['checkout']['shipping_address'])){ echo $_SESSION['checkout']['shipping_address'];}?></textarea>			
								<small class="error">Invalid Shipping Address</small>
				            </div>
				           
					        <div class="small-12 medium-6 columns">
					        	<span class="label">City</span><i class="fa fa-asterisk fa-1g icon-redcart"></i>
					            <input class="" type="text" required name="x_shipping_city" id="x_shipping_city" value="<?php if (isset($_SESSION['checkout']['shipping_city'])){ echo $_SESSION['checkout']['shipping_city']; } ?>" placeholder="Shipping City"></input>
					            <small class="error">Invalid Shipping City</small>
					        </div>
					        
					        <!--
					        <div class="small-12 medium-6 columns">
				            	<span class="label">Address</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br/>
								<textarea class="address_text_area"  required  rows="2" name="x_shipping_address" id="x_shipping_address"  placeholder="Shipping Address"><?php //if (isset($_SESSION['checkout']['shipping_address'])){ echo $_SESSION['checkout']['shipping_address'];}?></textarea>			
								<small class="error">Invalid Shipping Address</small>
				            </div>
				          
					        <div class="small-12 medium-6 columns">
					        	<span class="label">City</span><i class="fa fa-asterisk fa-1g icon-redcart"></i>
					            <input class="billing_text" type="text" required size="15" name="x_shipping_city" id="x_shipping_city" value="<?php //if (isset($_SESSION['checkout']['shipping_city'])){ echo $_SESSION['checkout']['shipping_city']; } ?>" placeholder="Shipping City"></input>
					            <small class="error">Invalid Shipping City</small>
					        </div>
							-->
				           
				        </div>
	          
					 	<div class="row">
						 	
					 		<div class="small-12 medium-4 columns">		
						 		<span class="label">Country</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br/>
						 		<select name="x_shipping_country" id="x_shipping_country" required>

									<?php
									echo '<option value="" ';
									//Check if country is present in session or not #5
									if (empty($_SESSION['checkout']['shipping_country']))
									{
										echo '  selected';
									}
									echo '  disabled style="color: #999">Select Country</option>';
									?>
									   
									<?php
									if (empty($_SESSION['checkout']['shipping_country']))
									{
										$_SESSION['checkout']['shipping_country'] = 'US';
									}
									// Get Country list 
									    while($country_top_array = mysqli_fetch_assoc($result_get_top_country_list2))
										{
											/*
											echo '<pre>';
											print_r($country_top_array);
											echo '</pre>';
											*/ 
											
											echo '<option value="'.$country_top_array['country_abbr'].'"';
											// Check for non-empty country in session #6
											if (!empty($_SESSION['checkout']['shipping_country']))
											{
												if ($_SESSION['checkout']['shipping_country'] == $country_top_array['country_abbr'])
												{
													echo ' selected="selected"';
												}
												
											}
											
											echo '>' . $country_top_array['country_name'] . '</option>';
										}
										echo '<option>.........</option>';
										while($country_array = mysqli_fetch_assoc($result_get_country_list2))
										{
											echo '<option value="' . $country_array['country_abbr'] . '"';
											//Check for non-empty country in session #7
											if (!empty($_SESSION['checkout']['shipping_country']))
											{
												if ($_SESSION['checkout']['shipping_country'] == $country_array['country_abbr'])
												{
													echo ' selected="selected"';
												}
												
											}
											echo '>'.$country_array['country_name'].'</option>';
										}
									?>
								</select>
								<small class="error" >Please Select Shipping Country</small>
							</div>
	             
					 		<div class="small-12 medium-4 columns">
					 			<span class="label">State</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br/>
								<div id="shipping_state_div">
											
									<?php
									// If country will be already present in session #8
									if (isset($_SESSION['checkout']['shipping_country']) && !empty($_SESSION['checkout']['shipping_country']))
									{
										// get state list #8
										$sql_get_state_list = 'SELECT state_name,
																state_abbr
																FROM states
																WHERE country_abbr= '.'"'.$_SESSION['checkout']['shipping_country'].'"';
										$result_get_state_list = mysqli_query($connection, $sql_get_state_list);
										$total_rows = mysqli_num_rows($result_get_state_list);
										if ($total_rows > 0)
										{
											echo '<select name="x_shipping_state" id="x_shipping_state" required >';
											echo '<option value=""' ;
											if (empty($_SESSION['checkout']['shipping_state']))
											{
												echo ' selected="selected"';
											}
											echo '  disabled style="color: #999">Select State</option>';
											$state_option = '';
											while($state_array = mysqli_fetch_assoc($result_get_state_list))
											{
												$state_option .= '<option value="'.$state_array['state_abbr'].'"';
												if (!empty($_SESSION['checkout']['shipping_state']))
												{
													if ($_SESSION['checkout']['shipping_state'] == $state_array['state_abbr'])
													{
														$state_option .= ' selected="selected"';
													}
												}
												$state_option .= '>'.$state_array['state_name'].'</option>';
											}
											$state_option .= '</select>';
											echo $state_option;
										}
										else
										{
											$state_option = 
											'<input type="alpha" class="" required name="x_shipping_state" id="x_shipping_state" value=""></input>
											 <!-- <input type="alpha" class="billing_text" required size="4" name="x_shipping_state" id="x_shipping_state" value="CA"></input> -->
											 <small class="error">Shipping State is required</small>';
											echo $state_option;
										}
									}
									else
									{
										echo '<select name="x_shipping_state" id="x_shipping_state" required>';
									    echo '<option value=" " selected disabled style="color: #999">Select State</option>';
										echo '</select>';
										echo '<small class="error" >Shipping State is required</small>';
									}
									?>
									
								</div>
							</div>
	             
					 		<div class="small-12 medium-4 columns">
					 			<span class="label">Postal Code</span><i class="fa fa-asterisk fa-1g icon-redcart"></i>
					 			<input class="billing_text1" type="text" required size="11" name="x_shipping_zip" id="x_shipping_zip" value="<?php if (isset($_SESSION['checkout']['shipping_zip'])){ echo $_SESSION['checkout']['shipping_zip']; } ?>" placeholder="Shipping Postal Code" />
					 			<small class="error">Invalid Shipping Postal Code</small>
					 		</div>
	            
					 	</div>
	          
					 	<div class="row">
						 	
						    <div class="small-12 medium-6 columns">
						       <span class="label">E-mail</span><i class="fa fa-asterisk fa-1g icon-redcart"></i>
						       <input class="billing_text1" type="email" required name="x_shipping_email" id="x_shipping_email" value="<?php if (isset($_SESSION['checkout']['email'])){ echo $_SESSION['checkout']['email']; } ?>" placeholder="someone@domain.com"/>
						       <small class="error">Invalid E-mail Address</small>
						    </div>
						    
						    <div class="small-12 medium-6 columns">
						       <span class="label">Phone</span><i class="fa fa-asterisk fa-1g icon-redcart"></i>
						       <input class="" type="text" required name="x_shipping_phone" id="x_shipping_phone" value="<?php if (isset($_SESSION['checkout']['phone'])){ echo $_SESSION['checkout']['phone']; } ?>" placeholder="555-555-5555 or (555) 555-5555"/>
						       <!-- <input class="billing_text" type="shipping_phone" required name="x_shipping_phone" id="x_shipping_phone" value="<?php if (isset($_SESSION['checkout']['phone'])){ echo $_SESSION['checkout']['phone']; } ?>" placeholder="555-555-5555 or (555) 555-5555"/> -->
						       <small class="error">Invalid Phone Number</small>
						    </div>
						    
						</div>
			
					 	<div class="billing_header" >
					 		<h3>Billing Information</h3>
					 	</div>
								             
					 	<div class="row">
	              
			                <div class="small-12 columns" >
					          	<input id="shipping_same" name="x_shipping_same" onclick="FillBilling(this.form)" type="checkbox">
					            <!-- <span class="label">Check if Shipping Info is the same as Billing Info</span>-->
					            Check if Billing is the same as Shipping
					        </div>
	                
			                <div class="small-12 medium-6 columns" >
			                    <span class="label">First Name</span><i class="fa fa-asterisk fa-1x icon-redcart"></i><br>
			                    <input class="" type="text" required maxlength="50" name="x_first_name" id="x_first_name" value="<?php if (isset($_SESSION['checkout']['first_name'])){ echo $_SESSION['checkout']['first_name']; } ?>" placeholder="Billing First Name">
			                    </input>
			                    <small class="error">Invalid Billing First Name</small>       
			                </div>
			                
			                <div class="small-12 medium-6 columns">
			                  <span class="label">Last Name</span><i class="fa fa-asterisk icon-redcart"></i><br/>
			                  <input class="" type="text" required maxlength="50" name="x_last_name" id="x_last_name" value="<?php if (isset($_SESSION['checkout']['lname'])){ echo $_SESSION['checkout']['lname']; } ?>" placeholder="Billing Last Name">
			                  </input>
			                  <small class="error">Invalid Billing Last Name</small>
			              	</div>
	              	
						</div>
	                                                            
					 	<div class="row">
	              
			                <div class="small-12 medium-6 columns">
			                  <span class="label">Address</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br/>
			                  <textarea class="" required  rows="2" name="x_address" id="x_address" placeholder="Billing Address"><?php if (isset($_SESSION['checkout']['address'])){ echo $_SESSION['checkout']['address'];}?></textarea>
			                  <small class="error">Invalid Billing Address</small>
			                </div>
			                 
			                <div class="small-12 medium-6 columns">
			                  <span class="label">City</span><i class="fa fa-asterisk fa-1g icon-redcart"></i>
			                  <input class="" type="text" required name="x_city" id="x_city" value="<?php if (isset($_SESSION['checkout']['city'])){ echo $_SESSION['checkout']['city']; } ?>" placeholder="Billing City"></input>
			                   <small class="error">Invalid Billing City</small>
			                </div>
			                
			            </div>
	               
					 	<div class="row">
								
					 		<div class="small-12 medium-4 columns">			
						 		<span class="label">Country</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br/>
						 		<select name="x_country" id="x_country" required>
									<?php
									echo '<option value="" ';
									//Check if country is present in session or not #5
									if (empty($_SESSION['checkout']['country']))
									{
										echo '  selected';
									}
									echo '  disabled style="color: #999">Select Country</option>';
									?>
									   
									 <?php
									 if (empty($_SESSION['checkout']['country']))
									 {
										$_SESSION['checkout']['country'] = 'US';
									 }
									 //Get country list 
									    while($country_top_array = mysqli_fetch_assoc($result_get_top_country_list))
										{
											echo '<option value="'.$country_top_array['country_abbr'].'"';
											// Check for non-empty country in session #6
											if (!empty($_SESSION['checkout']['country']))
											{
												if ($_SESSION['checkout']['country'] == $country_top_array['country_abbr'])
												{
													echo ' selected="selected"';
												}
												
											}
											
											echo '>' . $country_top_array['country_name'] . '</option>';
										}
										echo '<option>.........</option>';
										while($country_array = mysqli_fetch_assoc($result_get_country_list))
										{
											echo '<option value="' . $country_array['country_abbr'] . '"';
											//Check for non-empty country in session #7
											if (!empty($_SESSION['checkout']['country']))
											{
												if ($_SESSION['checkout']['country'] == $country_array['country_abbr'])
												{
													echo ' selected="selected"';
												}
												
											}
											echo '>'.$country_array['country_name'].'</option>';
										}
									 ?>
								</select>
						 		<small class="error" >Please Select Country</small>
						 	</div>
	                 
					 		<div class="small-12 medium-4 columns">
	                  			<span class="label">State</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br/>
					  			<div id="state_div" >
							
									<?php
									// If country will be already present in session #8
									if (isset($_SESSION['checkout']['country']) && !empty($_SESSION['checkout']['country']))
									{
										//get state list #8
										$sql_get_state_list = 'SELECT state_name,
																state_abbr
																FROM states
																WHERE country_abbr= '.'"'.$_SESSION['checkout']['country'].'"';
										$result_get_state_list = mysqli_query($connection, $sql_get_state_list);
										$total_rows = mysqli_num_rows($result_get_state_list);
										if ($total_rows > 0)
										{
											echo '<select name="x_state" id="x_state" required>';
											echo '<option value=""' ;
											if (empty($_SESSION['checkout']['state']))
											{
												echo ' selected="selected"';
											}
											echo '  disabled style="color: #999">Select State</option>';
											$state_option = "";
											while($state_array = mysqli_fetch_assoc($result_get_state_list))
											{
												$state_option.= '<option value="'.$state_array['state_abbr'].'"';
												if (!empty($_SESSION['checkout']['state']))
												{
													if ($_SESSION['checkout']['state'] == $state_array['state_abbr'])
													{
														$state_option.= ' selected="selected"';
													}
												}
												$state_option.= '>'.$state_array['state_name'].'</option>';
											}
											$state_option.= '</select>';
											echo $state_option;
										}
										else
										{
											$state_option = '<input type="alpha" class="billing_text" required size="4" name="x_state" id="x_state" value="CA"></input>
															 <small class="error">State can not be blank</small>';
											echo $state_option;
										}
									}
									else
									{
										echo '<select name="x_state" id="x_state" required>';
									        echo '<option value=" " selected disabled style="color: #999">Select State</option>';
										echo '</select>';
										echo '<small class="error" >Select state dropdown</small>';
									}
									?>
							
								</div>
					  		</div>
	               
					 		<div class="small-12 medium-4 columns">
			                	<span class="label">Postal Code</span><i class="fa fa-asterisk fa-1g icon-redcart"></i>
								<input class="" type="text" required size="11" name="x_zip" id="x_zip" value="<?php if (isset($_SESSION['checkout']['zip'])){ echo $_SESSION['checkout']['zip']; } ?>" placeholder="Billing Postal Code" />
								<small class="error">Invalid Billing Postal Code</small>
								</div>
					 		
			           </div>
	                    
					 	<div class="row">
						 	
				            <div class="small-12 medium-6 columns">
				               <span class="label">E-mail</span><i class="fa fa-asterisk fa-1g icon-redcart"></i>
				               <input class="billing_text" type="email" required name="x_email" id="x_email" value="<?php if (isset($_SESSION['checkout']['email'])){ echo $_SESSION['checkout']['email']; } ?>" placeholder="someone@domain.com"/>
				               <small class="error">Invalid Billing E-mail Address</small>
				            </div>
				            
				            <div class="small-12 medium-6 columns">
				               <span class="label">Phone</span><i class="fa fa-asterisk fa-1g icon-redcart"></i>
				               <input class="" type="text" required name="x_phone" id="x_phone" value="<?php if (isset($_SESSION['checkout']['phone'])){ echo $_SESSION['checkout']['phone']; } ?>" placeholder="555-555-5555 or (555) 555-5555"/>
				               <small class="error">Invalid Billing Phone Number</small>
				            </div>
				            
			          	</div>
	            
					 	<div class="billing_header">
					 		<h3>Payment</h3>
					 	</div>
					 	
					 	<div class="row">		
					 		<div class="small-12 medium-3 columns">
					 			<b>Payment Options</b>&nbsp;<i class="fa fa-asterisk fa-1g icon-redcart"></i>&nbsp;
					 		</div>
					 		<div class="small-12 medium-9 columns">
					 			<input type="radio" name="credit_card" value="credit_card" id="checkout_credit_card" checked>
					 			<label for="checkout_credit_card"><i class="fa fa-credit-card fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;(Visa, Mastercard, Discover, American Express)
					 				<!-- <img src="./store_img/creditcard.png"/>&nbsp;&nbsp;(Visa, Mastercard, Discover, American Express)-->
					 			</label>
					 		</div>
					 	</div>
					 	
					 	<!--
					 	<b>Payment Options</b>&nbsp;<i class="fa fa-asterisk fa-1g icon-redcart"></i>&nbsp;
					 	<input type="radio" name="credit_card" value="credit_card" id="checkout_credit_card" checked>
					 	<label for="checkout_credit_card">
			           		<img src="./store_img/creditcard.png"/>&nbsp;&nbsp;(Visa, Mastercard, Discover, American Express)
					   	</label>
					   	-->
	                                
					   <div class="row panel">
	          
					   		<div class="small-12 medium-4 columns">
	            
						   		<span class="label panel_content">Credit Card #</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br/>
						   		<input class="" type="text" required  maxlength="16" name="x_card_num" 
						   		value="<?php
					              if(isset($_SESSION['_INFO']['card_num'])){
					              	echo $_SESSION['_INFO']['card_num'];
					              } ?>"  
								  placeholder="XXXXXXXXXXXX9999"/>
								  <small class="error">Invalid Credit Card Number</small>
			              
							</div>
	           	
					   		<div class="small-12 medium-4 columns">
											
								<span class="label panel_content">Expiration Date<font size="1"></font></span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br/>
								<select name="exp_month" id="exp_month" style="width: 60px;" required>
									<option value="" default selected disabled style="color: #999">MM</option>
									  <?php
									  // get Month list
									  $exp_month = array( '01','02','03','04','05','06','07','08','09','10','11','12');
									  $sel_month = "";
									  if(isset($_SESSION['_INFO']['exp_month'])){
									  	$sel_month = $_SESSION['_INFO']['exp_month'];
									  }
									  foreach($exp_month as $value)
									  {
										  echo "<option ".($sel_month == $value?'selected':'')." value=\"$value\">$value</option>\n"; 
									  }
									  ?>
								</select>
								
								/
								
								<select name="exp_year" id="exp_year" style="width: 84px;" required>
								  <option value="" default selected disabled style="color: #999">YYYY</option>
								  <?php
								  // Get Year list
									  $cur_year = date("Y");
									  $last_year = $cur_year + 8;
									  $years = range($cur_year , $last_year);
									  $sel_year = "";
									  if(isset($_SESSION['_INFO']['exp_year'])){
									  	$sel_year = $_SESSION['_INFO']['exp_year'];
									  }
	
									  foreach($years as $value)
									  {
										  echo "<option ".($sel_year == $value?'selected':'')." value=\"$value\">$value</option>\n"; 
									  }
								  ?>
								</select>
								<small class="error">Month and Year required</small>
							
							</div>
	            
					   		<div class="small-12 medium-4 columns">
	            
				            	<span class="label panel_content">CCV</span><i class="fa fa-asterisk fa-1g icon-redcart"></i><br/>
								<input class="billing_text" type="cvv" required size="4" name="x_card_code" 
									value="<?php
										  if(isset($_SESSION['_INFO']['ccv'])){
										  	echo $_SESSION['_INFO']['ccv'];
										  } ?>" 
										  placeholder="999"/>
								<small class="error">CCV required</small>
				              
				           </div>
	              
					   	</div>
					   	<br />
	            
					   	<?php if(CHECK_OUT_TERMS_CHECK_BOX == 'yes') { ?>
	
						<div class="billing_header">
							<h3>Terms</h3>
						</div>
					
						<div class="row">
							<div class="small-12 columns">
								<span class="label">Accept Terms of Sale</span><i class="fa fa-asterisk fa-1g icon-redcart"></i>
								<input id="terms" name="x_terms" type="checkbox" required ><label for="terms"><a href="#" data-tooltip aria-haspopup="true" class="has-tip" title="Please read terms and click accept checkbox prior to proceeding." data-reveal-id="terms_modal">I agree to these terms</a></label>
								<small class="error">Please review and accept the Terms and Conditions</small>
								<div id="terms_modal" class="reveal-modal small" data-reveal>
									<p class="p1">
										<?php echo $prow['order_terms_of_sale']; ?>
									</p>
									<a class="close-reveal-modal">&#215;</a> 
								</div>
							</div>
						</div>
				
						<?php } ?>
							
						<div class="billing_header">
							<h3>Comments</h3>
						</div>
			
						<div class="row">
							<div class="small-12 columns">
								Message:<textarea name="x_message" rows="4" id="checkout_comments"  placeholder="Optional customer message..." ></textarea>
								<!-- <i class="fa fa-asterisk fa-1g icon-redcart"></i><em> required items </em> -->
							</div>
						</div>
									
						</form>
						
					</div>
	                        	                        
	            </div>
	            
	        </div>
	    </div>
            
        <!-- Begin right column of Checkout that has ORDER SUMMARY -->
        
        <div class="small-12 medium-4 columns">
	        
            <div class="checkout_block" id="check_out_block">
	            
                <div id="header_checkout_div" style="text-align: center">
                    <h6><strong>ORDER SUMMARY</strong></h6>
                </div>
                
				<div id="order_total_checkout" style="padding-top: 20px; padding-left: 20px; padding-right: 20px;" >
					<?php
						if (!empty($_SESSION['cart'])) {
							
							$project_id = safe_data($_SESSION['enrollment']['project_id']);
							// $sales_tax = get_sales_tax($project_id, $connection);
							// Check if tax is present in sesssion #9
							 if (isset($_SESSION['checkout']['state_tax']) && !empty($_SESSION['checkout']['state_tax']))
							 {
								$sales_tax = $_SESSION['checkout']['state_tax'];
							 }
							 else
							 {
								$sales_tax = 0.00;
							 }
							$total_tax = total_tax($sales_tax, $connection);
							$sub_total = get_order_total($connection);
							if($_SESSION['enrollment']['user_price_type'] == '') $_SESSION['enrollment']['user_price_type'] = USER_PRICE_TYPE;
							if($_SESSION['checkout']['shipping_type'] == '') $shipping_total = '0.00';
							else $shipping_total = get_shipping_cost($_SESSION['checkout']['shipping_type'], $connection);
							$coupon_code_discount = get_coupon_code_discount($connection);
							$grand_total = ($sub_total - $coupon_code_discount) + $total_tax + $shipping_total;
							$grand_total = pFormat($grand_total);
							echo "<input type='hidden' id ='session_coupon_message' value='".$_SESSION['coupon']['coupon_code_message']."'>";
							echo "<input type='hidden' id ='totalProductCost' value='".$sub_total."'>";
							echo '<div class="panel" >';
								echo '<table width="100%" style="background-color:#f2f2f2" >
										<tbody>
											<tr>
												<td style="text-align:right">Product Subtotal:</td>
												<td style="text-align:right">$'.pFormat($sub_total).'</td>
											</tr>';
											
										if(CHECK_OUT_PROMOTION_CODE == 'yes') {
											echo '
											<tr>
												<td style="text-align:right">Promotion Discount:</td>
												<td style="text-align:right">$'.pFormat($coupon_code_discount).'</td>
											</tr>';
										}
										
										echo '
											<tr>
												<td style="text-align:right">Sales Tax: </td>
												<td style="text-align:right">$'.pFormat($total_tax).'</td>
											</tr>
											<tr>
												<td style="text-align:right">Shipping:</td>
												<td style="text-align:right">$'.pFormat($shipping_total).'</td>
											</tr>
											<tr>
												<td style="text-align:right"><font size="4" color="RED">Order Total: </font></td>
												<td style="text-align:right"><font size="4" color="RED">$'.$grand_total.'</font></td>
										   </tr>
									   </tbody>
									</table>';
							echo "</div>";
							//echo '<div class="radius expand button right buttton_grand_total">'.'<b>Order Total: </b>&nbsp; $'.$grand_total.'</div>';
						} else {
							echo '<div class="panel" >';
							echo '<table width="100%" style="background-color:#f2f2f2" >
									<tbody>
										<tr>
											<td style="text-align:right">Product Subtotal:</td>
											<td style="text-align:right">$0.00</td>
										</tr>';
										
									if(CHECK_OUT_PROMOTION_CODE == 'yes') {
										echo '
										<tr>
											<td style="text-align:right">Promotion Discount: </td>
											<td style="text-align:right">$0.00</td>
										</tr>';
									}
									
								echo 	'<tr>
											<td style="text-align:right">Sales Tax: </td>
											<td style="text-align:right">$0.00</td>
										</tr>
										<tr>
											<td style="text-align:right">Shipping: </td>
											<td style="text-align:right">$0.00</td>
										</tr>
										<tr>
											<td style="text-align:right"><font size="4" color="RED">Order Total: </font></td>
											<td style="text-align:right"><font size="4" color="RED">$0.00</font></td>
										</tr>
									</tbody>
								</table>';
							echo "</div>";
							//echo '<div class="radius expand button right buttton_grand_total">'.'<b>Order Total: </b>&nbsp; $'.'0.00'.'</div>';
						}	
					?>
				</div>
				
				<?php
				if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])) {
					?>
         			<div style="padding-left: 20px; padding-right: 20px;" id="checkout_submit_div">
						<input type="button" value="Place Order" id="checkout_submit" class="submit button large radius success expand" >
					</div>
					<div>
						<center><img id="checkoutSRLogo" src="https://cdn-manager.net/media/images/supplementrelief-logo-blue-landscape-large.jpg" width="70%"></center>
						<br />
					</div>

					<?php
				} else {
				  	?>
				  	<div style="padding-left: 20px; padding-right: 20px;" >
						<input type="button" value="Place Order" class="disabled submit button large radius success expand" >
					</div>
					<div>
						<center><img id="checkoutSRLogo" src="https://cdn-manager.net/media/images/supplementrelief-logo-blue-landscape-large.jpg" width="70%"></center>
						<br />
					</div>
					<?php 
				} 
				?>
                   
            </div>
            
        </div>
                         
        </div>
        
    </div>

    <script src="<?php echo '../'.STORE_FOUNDATION_MIN_JS; ?>"></script>
	
	<script> $(document).foundation(); </script>

	<script type="text/javascript">
		$(document).ready(function () {
			// Retrieve
			if(sessionStorage.getItem("free_sampleValue"))
			$("#free_sample").val(sessionStorage.getItem("free_sampleValue"));
			$(document).on('change','#free_sample',function(){
				var free_sampleValue = $('#free_sample').val();
				// Check browser support
				if (typeof(Storage) !== "undefined") 
				{
				    // Store
				    sessionStorage.setItem("free_sampleValue", free_sampleValue);
				}
			});

			if(sessionStorage.getItem("checkout_comments"))
			$("#checkout_comments").html(sessionStorage.getItem("checkout_comments"));
			$(document).on('keypress keyup keydown keypress paste','#checkout_comments',function(){
				var checkout_comments = $('#checkout_comments').val();
				// Check browser support
				if (typeof(Storage) !== "undefined") 
				{
				    // Store
				    sessionStorage.setItem("checkout_comments", checkout_comments);
				}	
			});
			
		});
	</script>
	
	<?php  if(isset($_SESSION['_INFO'])): ?>
		<script type="text/javascript">
		var idleTime = 0;
		$(document).ready(function () {
		    //Increment the idle time counter every minute.
		    var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

		    //Zero the idle timer on mouse movement.
		    $(this).mousemove(function (e) {
		        idleTime = 0;
		    });
		    $(this).keypress(function (e) {
		        idleTime = 0;
		    });
		});

		function timerIncrement() {
		    idleTime = idleTime + 1;
		    if (idleTime > 4) { // 5 minutes
		    	idleTime = 0;
		        $("input[name='x_card_num']").val('');
		        $("#exp_month").val('');
		        $("#exp_year").val('');
		        $("input[name=x_card_code]").val('');
		    }
		}

		</script> 
	<?php  
		endif;
		if(STORE_CHECKOUT_MODAL == "yes")
		{
			if(!isset($_SESSION['store']['checkout_popup']))
			{
				$_SESSION['store']['checkout_popup'] = true;
				include_once('../php/checkout_promotions_modal.php');
			}
		}
	 ?>
	 <?php
	 	if (isset($_SESSION['coupon']['coupon_code']) && ($_SESSION['coupon']['coupon_code'] != '') ):
	 ?>
		 <script type="text/javascript">
		 	$(document).ready(function () {
		 		$("#coupon_message").html($("#session_coupon_message").val());
		 	});
		 </script>
	 <?php
		endif;
	 ?>
	 <?php
	 	if (!$sub_total):
	 ?>
		 <script type="text/javascript">
		 	$(document).ready(function () {
		 		$("#coupon_message").html("");
		 	});
		 </script>
	 <?php
		endif;
	 ?>

    </body>
</html>