<?php

/*
function getallheaders() { 
  foreach($_SERVER as $K=>$V){$a=explode('_' ,$K); 
      if(array_shift($a)=='HTTP'){ 
           array_walk($a,function(&$v){$v=ucfirst(strtolower($v));});
       $retval[join('-',$a)]=$V;} 
    } return $retval; }

$headers  = "";
foreach (getallheaders() as $name => $value) {
    $headers .= "$name: $value\n";
}
error_log('headers: '.$headers);
*/
include_once('../includes/store_page_header.php');

$country_abbr = safe_sql_data($connection, $_POST['country_abbr']);
$state_abbr = safe_sql_data($connection, $_POST['state_abbr']);
//get state list
$sql_get_state_list = 'SELECT state_name,
					   state_abbr
					   FROM states
					   WHERE country_abbr= '.'"'.$country_abbr.'"';


$result_get_state_list = mysqli_query($connection, $sql_get_state_list);
if (!$result_get_state_list)
{
	die("Database Get State List query failed.");
}
$total_rows = mysqli_num_rows($result_get_state_list);
if($total_rows > 0)
{
	$state_option = '<select name="x_state" id="x_state" required><option value=" " default selected disabled style="color: #999">Select State</option>';
	
	if (empty($_SESSION['checkout']['state']))
	{
		$state_option .= ' selected="selected"';
	}
	$state_option .= '  disabled style="color: #999">Select State</option>';
	
	while($state_array = mysqli_fetch_assoc($result_get_state_list))
	{
		if($state_abbr == $state_array['state_abbr']) $selected = ' selected';
		else $selected = '';
		$state_option.= '<option value='.$state_array['state_abbr'].$selected.'>'.$state_array['state_name'].'</option>';
	}
	$state_option.= '</select><small class="error" >Please select state</small>';
}
else
{
	$state_option = '<input type="alpha" class="billing_text" required size="4" name="x_state"  id="x_state" value="'.$state_abbr.'"></input>
					 <small class="error">State can not be blank</small>';
}

$state_json = array(
						'state_option' => $state_option
					);
echo json_encode($state_json);

    
?>
