<?php
if ( ! session_id())
{
	//sec_session_start();
}
include_once('../includes/store_page_header.php');
//Saving all parameters in to session #1
$_SESSION['checkout'] = array();

if (isset($_POST['fname']))
{
    $first_name = safe_data($_POST['fname']);
    $_SESSION['checkout']['first_name'] = $first_name;
}

if (isset($_POST['lname']))
{
    $last_name = safe_data($_POST['lname']);
    $_SESSION['checkout']['lname'] = $last_name;
}

if(isset($_POST['address']))
{
    $address = safe_data($_POST['address']);
    $_SESSION['checkout']['address'] = $address;
}

if (isset($_POST['city']))
{
    $city = safe_data($_POST['city']);
    $_SESSION['checkout']['city'] = $city;
}

if (isset($_POST['country']))
{
    $country = safe_data($_POST['country']);
    $_SESSION['checkout']['country'] = $country;
}

if (isset($_POST['state']))
{
    $state = safe_data($_POST['state']);
    $_SESSION['checkout']['state'] = $state;
}

if (isset($_POST['zip']))
{
    $zip = safe_data($_POST['zip']);
    $_SESSION['checkout']['zip'] = $zip;
}

if(isset($_POST['email']))
{
    $email = safe_data($_POST['email']);
    $_SESSION['checkout']['email'] = $email;
}

if(isset($_POST['phone']))
{
    $phone = safe_data($_POST['phone']);
    $_SESSION['checkout']['phone'] = $phone;
}

if (isset($_POST['shipping_same']))
{
    $shipping_same = safe_data($_POST['shipping_same']);
    $_SESSION['checkout']['shipping_same'] = $shipping_same;
}

if (isset($_POST['shipping_type']))
{
    $shipping_type = safe_data($_POST['shipping_type']);
    $_SESSION['checkout']['shipping_type'] = $shipping_type;
}

if (isset($_POST['shipping_first_name']))
{
    $shipping_first_name = safe_data($_POST['shipping_first_name']);
    $_SESSION['checkout']['shipping_first_name'] = $shipping_first_name;
}

if (isset($_POST['shipping_last_name']))
{
    $shipping_last_name = safe_data($_POST['shipping_last_name']);
    $_SESSION['checkout']['shipping_last_name'] = $shipping_last_name;
}

if(isset($_POST['shipping_address']))
{
    $shipping_address = safe_data($_POST['shipping_address']);
    $_SESSION['checkout']['shipping_address'] = $shipping_address;
}

if (isset($_POST['shipping_city']))
{
    $shipping_city = safe_data($_POST['shipping_city']);
    $_SESSION['checkout']['shipping_city'] = $shipping_city;
}

if (isset($_POST['shipping_country']))
{
    $shipping_country = safe_data($_POST['shipping_country']);
    $_SESSION['checkout']['shipping_country'] = $shipping_country;
}

if (isset($_POST['shipping_state']))
{
    $shipping_state = safe_data($_POST['shipping_state']);
    $_SESSION['checkout']['shipping_state'] = $shipping_state;
}

if (isset($_POST['shipping_zip']))
{
    $shipping_zip = safe_data($_POST['shipping_zip']);
    $_SESSION['checkout']['shipping_zip'] = $shipping_zip;
}

if(isset($_POST['shipping_email']))
{
    $shipping_email = safe_data($_POST['shipping_email']);
    $_SESSION['checkout']['shipping_email'] = $shipping_email;
}

if(isset($_POST['shipping_phone']))
{
    $shipping_phone = safe_data($_POST['shipping_phone']);
    $_SESSION['checkout']['shipping_phone'] = $shipping_phone;
}

if(isset($_POST['free_sample']))
{
    $free_sample = safe_data($_POST['free_sample']);
    $_SESSION['checkout']['free_sample'] = $free_sample;
}

if(isset($_SESSION['checkout']) && !empty($_SESSION['checkout']))
{
    $session = array( 'session' => '1');
    echo json_encode($session);
}
else
{
    $session = array( 'session' => '0');
    echo json_encode($session);
}

