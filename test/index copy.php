<?php 

// echo 'Begin Test Page.<br />';
// die;

error_reporting(E_ALL);

include_once('../includes/header.php');

getPage('Test', $connection);

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

// include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section">

// phpinfo();
// show_session();
// show_array($_SERVER);

// echo '<br>';

	/*
	include('../store/includes/functions.php');
	include('../store/includes/new_functions.php');
	include('../store/includes/defines.php');
	
	echo '<br>';

	// include('../php/e-commerce-header-products.php');

	include('../php/search_products.php');
	include('../php/search_products_form.php');
	include('../php/search_products_results_footer.php');
	
	lorem_ipsum(5);
	*/

// unset($_SESSION['assessment']);
// unset($_SESSION['assessment_item']);

?>

<br />

<div class="row">		
	<div class="small-12 columns">
			<a href="#" data-reveal-id="freeSample">Click Me For A Free Sample</a>
			<br>
			<br>	
	</div>
</div>


<div id="freeSample" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	
  <h2 id="modalTitle">Choose Free NuMedica Sample</h2>
  
  <p class="lead">Included with your order today!</p>
  
  <hr>
  
  <div class="row">
	  
	<div class="small-12 medium-3 columns">
		<center>
	  		<img src="https://cdn-manager.net/media/images/numedica-adrenamed-gl50-small-portrait.jpg"><br>
	  		<span class="caption">AdrenaMed GL50</span>
	  		<div class="modalButton"><a href="" class="button tiny radius">Choose</a></div>
		</center>
  	</div>
  
	<div class="small-12 medium-3 columns">
		<center>
	  		<img src="https://cdn-manager.net/media/images/numedica-Methyl-B-12-Rx-60loz-small.jpg" ><br>
	  		<span class="caption">Methyl B-12 Rx</span>
	  		<div class="modalButton"><a href="" class="button tiny radius">Choose</a></div>
		</center>
  	</div>
  		
  	<div class="small-12 medium-3 columns">
	  	<center>
	  		<img src="https://cdn-manager.net/media/images/numedica-power-greens-berry-1-serving-small.jpg"><br>
	  		<span class="caption">Power Greens Berry</span>
	  		<div class="modalButton"><a href="" class="button tiny radius">Choose</a></div>
	  	</center>					
  	</div>
  	
  	<div class="small-12 medium-3 columns">
	  	<center>
	  		<img src="https://cdn-manager.net/media/images/numedica-Power-Greens-Chocolate-1svg-small.jpg"><br>
	  		<span class="caption">Power Greens Chocolate</span>
	  		<div class="modalButton"><a href="" class="button tiny radius">Choose</a></div>
	  	</center>
  	</div>

  </div>
    
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
  
</div>


<div class="row">
	<div class="small-12  columns">
		
		<div id="featureVideo">
			
			<video id="myVideo" controls style="width:100%;height:auto;" poster="https://cdn-manager.net/media/images/people-of-different-ethniticity-pondering-question-large-landscape.jpg">
				<source src="https://cdn-manager.net/media/videos/supplementrelief-membership-promotion.mp4" type="video/mp4">  
			    <source src="https://cdn-manager.net/media/videos/supplementrelief-membership-promotion.webm" type="video/webm">    
		    Your browser does not support the video tag.
		  	</video>
		  	
		  	<div id="output"></div>
		  	
		  	<!-- 
			  	https://developer.mozilla.org/en-US/docs/Web/Guide/Events/Media_events 
			  	http://stackoverflow.com/questions/8599076/detect-if-html5-video-element-is-playing
			-->
		  	
		  	<script>
			  	
		        var media = document.getElementById('myVideo');
		        
		        // Playing event
		        var isPlaying = function(e) {
		            $("#output").html("Playing event triggered");
		            // alert('Video is playing.');
		        };
		
		        // Playing event
		        var didEnd = function(e) {
			        
		            console.log("Ended event triggered");
		            
		            $.ajax({
			            url: "../includes/TEST_VIDEO_AJAX.php",
						method: "POST",
						data: {test_data: "Video finished playing"}
		            }).success(function(response) {
			            alert(response);
		            })
		            
		            
		        };
		        // Pause event
		        var onPause = function(e) { 
		            console.log("Pause event triggered"); 
		        };
		        // Volume changed event
		        var onVolumechange = function(e) { 
		            console.log("Volume change event triggered"); 
		        };
		        // Seeking event
		        var onSeeking = function(e) { 
		            console.log("Seeking event triggered"); 
		        };
		        
		        media.addEventListener("playing", isPlaying, false);  
		        media.addEventListener("ended", didEnd, false);    
		        media.addEventListener("pause", onPause, false);
		        media.addEventListener("seeking", onSeeking, false);
		        media.addEventListener("volumechange", onVolumechange, false);
	        
	    	</script>   
		</div>
	</div>
</div>


<div class="row">		
	<div class="small-12 columns">
			<a href="#" data-reveal-id="myModal">Click Me For A Modal</a>
			<br>
			<br>	
	</div>
</div>


<div id="myModal" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <h2 id="modalTitle">Awesome. I have it.</h2>
  <p class="lead">Your couch.  It is mine.</p>
  <p>I'm a cool paragraph that lives inside of an even cooler modal. Wins!</p>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>


<!-- 
<div class="row">		
	<div class="small-12 columns">
		<form id="assessmentItemForm" data-abide action="../assessment/" method="post" />

			<input type="hidden" id="assessment-item-action" name="action" value="grade-item"/>
			<input type="hidden" name="assessment_session_id" value="<?php echo ($_SESSION['assessment']['assessment_session_id'] ? $_SESSION['assessment']['assessment_session_id'] : ''); ?>"/>				
			<input type="hidden" name="assessment_id" value="<?php echo ($_SESSION['assessment']['assessment_id'] ? $_SESSION['assessment']['assessment_id'] : ''); ?>"/>
			<input type="hidden" name="assessment_item_id" value="<?php echo ($assessment_item_id ? $assessment_item_id : ''); ?>"/>
			<input type="hidden" name="assessment_item_type_code" value="<?php echo ($assessment_item_type_code ? $assessment_item_type_code : ''); ?>"/>
			
			<fieldset>
						  
				<legend>Question 1 of 5 - Multiple Choice/Multiple Answer</legend>
				
				<div class="row">		
					<div class="small-12 columns">
						<div class="assessmentResponseImage">
							<ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-5">
								<li>
									<center>
										<img width="" src="https://cdn-manager.net/media/images/stressed-cartoon-head-illustration-small-block.jpg" alt="">
										<br>
										<input type="checkbox" name="checkbox_response[]" value="1" id="1">&nbsp;&nbsp;First Choice
									</center>
								</li>
								<li>
									<center>
										<img width="" src="https://cdn-manager.net/media/images/stressed-cartoon-head-illustration-small-block.jpg" alt="">
										<br>
										<input type="checkbox" name="checkbox_response[]" value="2" id="2">&nbsp;&nbsp;Second Choice
									</center>
								</li>
								<li>
									<center>
										<img width="" src="https://cdn-manager.net/media/images/stressed-cartoon-head-illustration-small-block.jpg" alt="">
										<br>
										<input type="checkbox" name="checkbox_response[]" value="3" id="3">&nbsp;&nbsp;Third Choice
									</center>
								</li>
								<li>
									<center>
										<img width="" src="https://cdn-manager.net/media/images/stressed-cartoon-head-illustration-small-block.jpg" alt="">
										<br>
										<input type="checkbox" name="checkbox_response[]" value="4" id="4">&nbsp;&nbsp;Fourth Choice
									</center>
								</li>
								<li>
									<center>
										<img width="" src="https://cdn-manager.net/media/images/stressed-cartoon-head-illustration-small-block.jpg" alt="">
										<br>
										<input type="checkbox" name="checkbox_response[]" value="5" id="5">&nbsp;&nbsp;Fifth Choice
									</center>
								</li>
							</ul>
						</div>					
					</div>
				</div>
				
				<hr>	
				
				<div class="row">
		    	 	<div class="small-12 columns">
		    	    	
		    	    	<input id="button" class="button large radius success" type="submit" value="Submit">			    	   
		    	      	    	      	    	      	    	  				    	    
		    	  	</div>
				</div>
				
			</fieldset>
					
		</form>
					
	</div>
</div>
-->

<!--
<form action="/echo/html/" method="POST" data-abide>
    <div class="row">
        <div class="small-12 columns checkbox-group" data-abide-validator-limit="1,3">
            <label>Check some boxes</label>
            <small class="error">You have checked an invalid number of boxes.</small>
            <ul class="small-block-grid-3">
                <li>
                    <label>
                        <input type="checkbox" data-abide-validator="checkbox_limit" value="1" /> 1
                    </label>
                </li>
                <li>
                    <label>
                        <input type="checkbox" data-abide-validator="checkbox_limit" value="2" /> 2
                    </label>
                </li>
                <li>
                    <label>
                        <input type="checkbox" data-abide-validator="checkbox_limit" value="3" /> 3
                    </label>
                </li>
                <li>
                    <label>
                        <input type="checkbox" data-abide-validator="checkbox_limit" value="4" /> 4
                    </label>
                </li>
                <li>
                    <label>
                        <input type="checkbox" data-abide-validator="checkbox_limit" value="5" /> 5
                    </label>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <button type="submit">Submit</button>
        </div>
    </div>
</form>
-->

<?php
// include('../php/google_ecommerce_analytics_tracking_test.php');

// include('../php/show_project_promotions.php');

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

// include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php'); 

?>

<script type="text/javascript">
	/*
	$(document).ready(function(){
		window.setTimeout(function() {$('#freeSample').foundation('reveal', 'open');}, <?php echo 3000; ?>);	
	});
	*/
</script>

<!--
<script type="text/javascript" src="../js/slick.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
        $(".promotion-slider").show();
        $('.promotion-slider').slick({
         autoplay: true,
         autoplaySpeed: 7000,	
         dots: true, 
         slidesToShow: 1,
         slidesToScroll: 1
        });
	});
</script>
-->