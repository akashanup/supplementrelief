<?php 

// echo 'Begin Test page.<br />';

error_reporting(E_ALL);

include_once('../includes/header.php');

getPage('Test', $connection);

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

// show_session();

if ($_SESSION['page']['ecommerce']) { 
	include_once('../'.STORE_FOLDER_NAME.'/includes/store_header.php');
	// $_SESSION['store_current_path'] = $_SERVER['QUERY_STRING'];
}

include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

// echo '<br>';

/*
include('../php/search_products.php');
include('../php/search_products_form.php');
include('../php/search_products_results_footer.php');
include('../php/show_project_brand_products.php');
*/

?>

<br />

<div class="row">		
	<div class="small-12 columns">
			<a href="#" data-reveal-id="freeSample">Click Me For A Free Sample</a>
			<br>
			<br>	
	</div>
</div>

<?php // show_array($_SESSION); ?>

<div id="freeSample" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	
	<h2 id="modalTitle">Choose Free NuMedica Sample</h2>
  
	<p class="lead">Included with your order today!</p>
  
	<hr>
  
	<div class="row">
				
		<div class="small-12 medium-3 columns">
		
		  	<div id="productAddtoCart">
					
				<?php
				/*								 
				echo integrate_add_to_cart_button($connection, array(
					'p_usage_id' => $product['brand_product_usage_id'],
					'pid' => $product['id'],
					'project_id' => $project_id,
					'p_name' => $product['project_brand_product_name'],
					'form_name' => 'form_large',
					'qty_visible' => TRUE,
					'button_html_content' => '',
					'retail_price' => $product['price'],
					'price_type' => $user_price_type
				));
				*/
				
				
				echo integrate_add_to_cart_button($connection, array(
					'p_usage_id' => 4,
					'pid' => 264,
					'project_id' => 1,
					'p_name' => 'NuMedica Absolute Protein Gorilla',
					'form_name' => 'form_large',
					'qty_visible' => TRUE,
					'button_html_content' => '',
					'retail_price' => 159.95,
					'price_type' => 'RETL'
				));
				
				
				/*
				$t = array(
					'p_usage_id' => $product['brand_product_usage_id'],
					'pid' => $product['id'],
					'project_id' => $_GET['project_id'],
					'p_name' => $product['project_brand_product_name'],
					'form_name' => 'form_large',
					'qty_visible' => TRUE,
					'button_html_content' => '',
					'retail_price' => $product['price'],			
					'price_type' => $user_price_type 
				);
									
				echo '<pre>';
				print_r($t);
				echo '</pre>';
				*/
						
				?>
				
			</div>
							
		</div>
	
		<div class="small-12 medium-9 columns">
			<?php lorem_ipsum(3); ?>
		</div>

	</div>

	<div class="row">
	  
		<div class="small-12 medium-3 columns">
			<center>
		  		<img src="https://cdn-manager.net/media/images/numedica-adrenamed-gl50-small-portrait.jpg"><br>
		  		<span class="caption">AdrenaMed GL50</span>
		  		<div class="modalButton"><a href="" class="button tiny radius">Choose</a></div>
		  	</center>
  		</div>
  
		<div class="small-12 medium-3 columns">
			<center>
		  		<img src="https://cdn-manager.net/media/images/numedica-Methyl-B-12-Rx-60loz-small.jpg" ><br>
		  		<span class="caption">Methyl B-12 Rx</span>
		  		<div class="modalButton"><a href="" class="button tiny radius">Choose</a></div>
			</center>
	  	</div>
	  		
	  	<div class="small-12 medium-3 columns">
		  	<center>
		  		<img src="https://cdn-manager.net/media/images/numedica-power-greens-berry-1-serving-small.jpg"><br>
		  		<span class="caption">Power Greens Berry</span>
		  		<div class="modalButton"><a href="" class="button tiny radius">Choose</a></div>
		  	</center>					
	  	</div>
	  	
	  	<div class="small-12 medium-3 columns">
		  	<center>
		  		<img src="https://cdn-manager.net/media/images/numedica-Power-Greens-Chocolate-1svg-small.jpg"><br>
		  		<span class="caption">Power Greens Chocolate</span>
		  		<div class="modalButton"><a href="" class="button tiny radius">Choose</a></div>
		  	</center>
	  	</div>

  	</div>
    
  	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
  
</div>

<?php

// include('../php/show_project_program_resource.php'); 

include('../php/system_log_page_access.php'); 

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php');

if ($_SESSION['page']['ecommerce']) { 
	include_once('../'.STORE_FOLDER_NAME.'/'.STORE_FOOTER);
	$_SESSION['store_current_path'] = $_SERVER['QUERY_STRING'];
}

?>

<script type="text/javascript">
	
	$(document).ready(function(){
		window.setTimeout(function() {$('#freeSample').foundation('reveal', 'open');}, <?php echo 3000; ?>);	
	});
	
</script>