<?php
    
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    
// echo 'Begin /url-traffic-manager/index.php<br />';
// die;
// This script will only execute if the page in the requested URL DOES NOT physically exist on the server.
// This can happen for TWO different reasons:
// 1. The page requested really doesn't exist so in that case we redirect to a custom 404 Page Not Found page: /page_not_found/.
// 2. The URL is "SEO optimized" for a multipurpose php page within the system that requires URL parameters to uniquely identify content such as /product/, /recipe/, /resource/, /show_blog_post/.
    // 2a. .htaccess in ROOT Directory will intercept ALL inbound traffic where the URL requested is NOT found on the server and redirect it to /url-traffic-manager/.
        // 2a1. FallbackResource /url-traffic-manager/index.php
    // 2b. Each page within the system (already indexed with query parameters) that needs an "SEO optimized" URL MUST have a 301 to redirect the currently indexed URL (with params) to the "SEO optimized" URL.
        // 2b1. Redirect 301 /product/?p_id=1&bpu_id=3 https://supplementrelief.com/numedica-absolute-protein/
        // 2b2. If the page is new and has NOT been indexed yet then the 301 is NOT needed.
    // 2c. The "SEO optimized" URL i.e. numedica-absolute-protein MUST EXIST and be properly defined in tables urls and url_usages. These tables are auto-populated from CDN Admin forms for the respective Blog Post, Product, Recipe, Resource, etc.

include_once('../includes/header.php');
 
// The first thing it does is log the user session. 
 
if ( getMarketingReferralURI() ) {	
	createUserSessionLog($connection); // Log what we know about this initial User Request into the database.	
}
       
if (!$url) { $url = $_SERVER['REQUEST_URI']; }

$parsedURL = explode("/", $url);
// show_array($parsedURL); 
// die;
// $parsedURL[1] should be used if the project is installed at the root of the domain i.e. supplementrelief.com
// $parsedURL[2] should be used if the project is installed at the first subdirectory of the root of the domain i.e. supplementrelief.com/development

// if (strlen($parsedURL[1]) > 0) { 
if (strlen($parsedURL[2]) > 0) { 
    // We are NOT on the Home (root) page.
    // We have either: an SEO URL like numedica-absolute-protein OR understanding-celiac-disease-and-the-relationship-to-gluten-sensitivity OR a bad URL (real 404).
    // We got here using root directory .htaccess FallbackResource /development/url-traffic-manager/index.php

    // $seo_url = $parsedURL[1];
    $seo_url = $parsedURL[2]; 
    // echo 'SEO URL for Database Lookup is: '.$parsedURL[2].'<br>';
    // show_array($parsedURL); 
	// die;
    // echo '<br>SEO URL for Database Lookup is: '.$seo_url.'<br>';

    // 1. Determine if this is a valid URL or if it is a 404.
    // Query the URL
    $query = 'SELECT 
    	url.id AS url_id,
    	url.url, 		
    	urlu.id AS url_usage_id, 
    	urlu.blog_id, 
    	urlu.blog_post_id, 
    	urlu.recipe_id,  
    	urlu.resource_id, 
    	urlu.course_id, 
    	urlu.topic_id, 
    	urlu.web_page_id, 
    	pg.name AS page_name, 
    	pg.relative_url,
    	pbpu.project_id, 
    	pbpu.brand_product_usage_id,  
    	pbpu.page_meta_title, 
    	bpu.name AS brand_product_name, 
    	ppca.id AS ppca_id  
    	FROM urls url 
    	LEFT JOIN url_usages urlu ON url.id = urlu.url_id 
    	LEFT JOIN pages pg ON urlu.page_id = pg.id 
    	LEFT JOIN project_brand_product_usages pbpu ON urlu.pbpu_id = pbpu.id 
    	LEFT JOIN brand_product_usages bpu ON pbpu.brand_product_usage_id = bpu.id 
    	LEFT JOIN project_program_content_assets ppca ON urlu.ppca_id = ppca.id 
    	WHERE url.url = "'.$seo_url.'" 
    	AND urlu.effective_date <= CURRENT_DATE 
        AND (urlu.end_date is NULL or urlu.end_date >= CURRENT_DATE)';  

    // echo $query;
    // die();
    
    $result = mysqli_query($connection, $query);
    
    if (!$result) {
    	show_mysqli_error_message($query, $connection);
    	die;
    }
    
    $r = mysqli_fetch_assoc($result);                
    
    if ($r['url_id']) { 
        
        // This is a VALID SEO URL so take us to the right php page to display the content.
        // URL may look like:
        // a. numedica-absolution-protein (goes to ../product/?p_id=1&bpu_id=3)
        // b. understanding-celiac-disease-and-the-relationship-to-gluten-sensitivity (goes to ../resource/?ppca_id=393&ca_id=2358)
        // c. african-peanut-chicken-recipe (goes to ../recipe/?ppca_id=308&ca_id=1280&re_id=1349)
        // d. in-case-you-wonder-if-good-things-still-happen (goes to ../show_blog_post/?bp_id=2348)
        
        // show_array($r);
        // die;
        
        // Determine which php script to use to display the content for the URL based upon $r['page_name'].
        
        // 1. If this is a Blog page include ../show_blog_posts/ with the correct correct parameters.        
        if ($r['page_name'] == 'Show Blog Posts') { 
            // https://supplementrelief.com/show_blog_posts/?blog_id=1299
            // echo 'Page Name: Your Healthy Life Concierge';
            // show_array($r);  
            $_SESSION['blog']['url'] = $seo_url;
            $_GET['blog_id'] = $r['blog_id'];
            $target_uri = '../'.$r['relative_url'].'/index.php'; 
            // echo $target_uri;
            include $target_uri;
            // unset($_SESSION['seo_url']); 
            exit;                  	 
        }
        
        // 2. If this is a Blog Post page include ../show_blog_post/ with the correct correct parameters.        
        if ($r['page_name'] == 'Blog Post') { 
            // https://supplementrelief.com/show_blog_post/?bp_id=2348
            // echo 'Page Name: Blog Post';
            // show_array($r);  
            $_SESSION['blog_post']['url'] = $seo_url;
            $_GET['bp_id'] = $r['blog_post_id'];
             $target_uri = '../'.$r['relative_url'].'/index.php'; 
            // echo $target_uri;
            include $target_uri;
            // unset($_SESSION['seo_url']); 
            exit;           
        }  
        
        // 3. If this is a Product page include ../product/ with the correct correct parameters.
        if ($r['page_name'] == 'Product') { 
            // https://supplementrelief.com/product/?p_id=1&bpu_id=3
            // echo 'Page Name: Product';
            // show_array($r);  
		    $_SESSION['brand_product_usage']['url'] = $seo_url;
            $_GET['p_id'] = $r['project_id'];
            $_GET['bpu_id'] = $r['brand_product_usage_id'];
            $target_uri = '../'.$r['relative_url'].'/index.php'; 
            // echo $target_uri;
            include $target_uri;
            // unset($_SESSION['seo_url']); 
            exit;              
        }
        
        // 4. If this is a Recipe page include ../recipe/ with the correct correct parameters.
        if ($r['page_name'] == 'Recipe') { 
            // https://supplementrelief.com/recipe/?ppca_id=308&ca_id=1280&re_id=1971    
            // echo 'Page Name: Recipe';
            // show_array($r); 
            $_SESSION['recipe']['url'] = $seo_url;
            $_GET['re_id'] = $r['recipe_id'];
            // echo $target_uri;
            $target_uri = '../'.$r['relative_url'].'/index.php'; 
            // echo $target_uri;
            include $target_uri;
            // unset($_SESSION['seo_url']); 
            exit;        
        }
        
        // 5. If this is a Resource page include ../resource/ with the correct correct parameters.
        if ($r['page_name'] == 'Resource') {  
            // https://supplementrelief.com/resource/?ppca_id=393&ca_id=2358     
            // echo 'Page Name: Resource';
            // show_array($r);  
            $_SESSION['resource']['url'] = $seo_url;
            $_GET['ppca_id'] = $r['ppca_id'];
            $_GET['ca_id'] = $r['resource_id'];         
            $target_uri = '../'.$r['relative_url'].'/index.php';
            // echo $target_uri;
            include $target_uri;
            // unset($_SESSION['seo_url']); 
            exit;        
        }
        
        // 6. If this is a Cookbook page include ../cookbook/ with the correct correct parameters.
        if ($r['page_name'] == 'Cookbook') {  
            // https://supplementrelief.com/cookbook/?ppca_id=393&ca_id=2358     
            // echo 'Page Name: Cookbook';
            // show_array($r);  
            $_SESSION['resource']['url'] = $seo_url;
            $_GET['ppca_id'] = $r['ppca_id'];
            $_GET['ca_id'] = $r['resource_id'];         
            $target_uri = '../'.$r['relative_url'].'/index.php';
            // echo $target_uri;
            include $target_uri;
            // unset($_SESSION['seo_url']); 
            exit;        
        }

        // 7. If this is a Wellness Education page include ../education/ with the correct correct parameters.
        if ($r['page_name'] == 'Wellness Education') {  
            // https://supplementrelief.com/education/?c_id=1772&t_id=1773&wp_id=1776     
            // echo 'Page Name: Wellness Education';
            // show_array($r);  
            $_SESSION['education']['url'] = $seo_url;
            $_GET['c_id'] = $r['course_id'];
            $_GET['t_id'] = $r['topic_id'];
            $_GET['wp_id'] = $r['web_page_id'];       
            $target_uri = '../'.$r['relative_url'].'/index.php';
            // echo $target_uri;
            include $target_uri;
            // unset($_SESSION['seo_url']); 
            exit;        
        }
                     
    } else {
        // 1. SEO URL was not found. Not in our database.
        // 2. This means we have a true 404.
         
        // echo 'SEO URL was NOT found. This is a TRUE 404.<br>';
        // echo 'Server Request URI: '.$_SERVER['REQUEST_URI'].'<br>';
        // die;    

        // Redirect to our custom 404 page /page-not-found/                
        // $_SESSION['USER_REQUEST_URI'] = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $_SESSION['USER_REQUEST_URI'] = $_SERVER['REQUEST_URI'];
        $target_uri = '../page-not-found/';
        header("location: ".$target_uri);
        exit();	            
    }
                
} else {
    
    echo 'There was no URL when there should have been one. Get screenshot of page and email to support@supplementrelief.com.<br>';
    show_array($parsedURL);
    die;
 
    $target_uri = '../';
	// $target_uri = '../login/?msg=1&u_id='.$si;
	// echo 'Target URI: '.$target_uri.'<br /><hr />';
	// die;
	header("location: $target_uri"); // Redirect user from this script to original URL.
	exit;
   
}

?>