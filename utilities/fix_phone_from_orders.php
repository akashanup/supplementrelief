<?php

// Query the Orders
$query = 'SELECT 
	MAX(ord.id), 
	ord.user_id,
	ord.phone, 
	ord.authorization_code  
	FROM orders ord  
	LEFT JOIN persons pe ON ord.user_id = pe.id 	
	WHERE ord.user_id IS NOT NULL 
	AND ord.transaction_id IS NOT NULL 
	AND authorization_code IS NOT NULL  
	AND ord.order_project_id = 1  
	GROUP by ord.user_id 
	ORDER BY ord.user_id ASC';

// echo $query;
// die();

$result = mysqli_query($connection, $query);

if (!$result) {
	show_mysqli_error_message($query, $connection);
	die;
}

$total_records = mysqli_num_rows($result);

// echo 'Total Orders: '.$total_records.'<br />';

while($r = mysqli_fetch_assoc($result)) {
	
    $queryUpdate = '
	UPDATE persons SET 
	phone = "'.$r['phone'].'" 
    	WHERE id = "'.$r['user_id'].'" 
    	AND phone IS NULL';	
	
	// echo $queryUpdate;	
	// die();
		
	$result_update = mysqli_query($connection, $queryUpdate);
	
	if (!$result_update) {
		show_mysqli_error_message($queryUpdate, $connection);
		die;
	}
	
	// show_array($r);
    
}

mysqli_free_result($result)

?>
