<?php

echo 'user_session_update.php<br /><hr />';

include_once('../includes/core.php');
    
error_reporting(E_ERROR | E_WARNING | E_PARSE);

// Query the User Sessions
$query = 'SELECT 
    id, 
    http_user_agent 
    FROM user_sessions 
    ORDER BY id'; 

// echo $query;
// die();

$result = mysqli_query($connection, $query);

if (!$result) {
	show_mysqli_error_message($query, $connection);
	die;
}

$total_records = mysqli_num_rows($result);

// Set autocommit to off
// mysqli_autocommit($connection, FALSE);

// $http_user_agent = 100;

while($r = mysqli_fetch_assoc($result)) { 
        
    $user_session_id = $r['id'];
    $http_user_agent = $r['http_user_agent'];
    
    if (strlen($http_user_agent) > 1) {
        $u_agent = $r['http_user_agent'];   
    } else {
        $u_agent = ''; 			
    }
   
    // echo $user_session_id.'<br>';
    
    $browser=getBrowser($u_agent);
    
    /*
    echo '<pre>';
    print_r($browser);
    echo '</pre>';
    */
    
    $queryUpdate = '
	UPDATE user_sessions SET 
    browser_device_type = "'.safe_data($browser['deviceType']).'" /* 
	browser_platform = "'.safe_data($browser['platform']).'", 
    browser_name = "'.safe_data($browser['name']).'", 
	browser_version = "'.safe_data($browser['version']).'" */ 
	WHERE id = "'.$user_session_id.'"';	
	
	// echo $queryUpdateUsername;	
	// die();
		
	$result_update = mysqli_query($connection, $queryUpdate);
	
	if (!$result_update) {
		show_mysqli_error_message($queryUpdate, $connection);
		die;
	}
	    
}


echo 'Total User Sessions: '.$total_records.'<br />';


// mysqli_free_result($result)

echo 'Done';





?>
