<?php 

// echo 'Begin Why Choose NuMedica? page.<br />';

include_once('../includes/header.php');

getPage('Why Choose NuMedica?', $connection);

include_once('../includes/authorization.php');

include('../includes/page_header.php');

include('../includes/menu.php');

include('../includes/off_page_canvas_1.php');

// Begin body content that will appear inside of the Off Canvas <section class="main-section"> 

if (!empty($global_page_content_summary)) {
	
	echo $global_page_content_summary;
		
}

// include('php/home_promotion_slider.php');

if (!empty($global_page_content)) {
	
	echo $global_page_content;
		
}

include('../php/system_log_page_access.php'); 

// End body content that will appear inside of the Off Canvas <section class="main-section"> -->

include('../includes/off_page_canvas_2.php');

// Begin rest of body content for page.

include('../includes/page_footer.php');

include('../includes/disclaimer.php');

include('../includes/foundation_footer.php'); 

?>